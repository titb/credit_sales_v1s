-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Feb 20, 2019 at 12:03 PM
-- Server version: 5.7.19
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `credit_sale_db4`
--

-- --------------------------------------------------------

--
-- Table structure for table `account_type`
--

DROP TABLE IF EXISTS `account_type`;
CREATE TABLE IF NOT EXISTS `account_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `account_type`
--

INSERT INTO `account_type` (`id`, `name`, `description`, `deleted`, `created_at`, `updated_at`) VALUES
(1, 'Sana', 'Cutee', NULL, NULL, '2018-06-06 09:23:14');

-- --------------------------------------------------------

--
-- Table structure for table `cart_storage`
--

DROP TABLE IF EXISTS `cart_storage`;
CREATE TABLE IF NOT EXISTS `cart_storage` (
  `id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `cart_data` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cart_storage`
--

INSERT INTO `cart_storage` (`id`, `cart_data`, `created_at`, `updated_at`) VALUES
('50_cart_items', 'a:0:{}', '2018-08-15 01:16:09', '2019-01-18 10:05:52'),
('53_cart_items', 'a:0:{}', '2018-11-19 23:22:08', '2018-12-28 08:43:48');

-- --------------------------------------------------------

--
-- Table structure for table `cs-cds-request-item`
--

DROP TABLE IF EXISTS `cs-cds-request-item`;
CREATE TABLE IF NOT EXISTS `cs-cds-request-item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cds-request-id` int(11) DEFAULT NULL,
  `product-id` int(11) DEFAULT NULL,
  `barcode` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `price-total-of-pro` decimal(30,0) DEFAULT NULL,
  `price-of-deposit-product` decimal(30,0) DEFAULT NULL,
  `price-total-for-pay` decimal(30,0) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cs-co-give-item-client`
--

DROP TABLE IF EXISTS `cs-co-give-item-client`;
CREATE TABLE IF NOT EXISTS `cs-co-give-item-client` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `schedule_id` int(11) DEFAULT '0',
  `currency_id` int(11) DEFAULT '0',
  `staff_id` int(11) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL COMMENT 'status [ 1 => Give to Client , 2 =. Give to Co , ''0'' => Client not Take]',
  `active` int(11) DEFAULT NULL,
  `approve_by` int(11) DEFAULT NULL,
  `give_by` int(11) DEFAULT '0',
  `staff_give_id` int(11) DEFAULT '0',
  `num_give_to_client` varchar(255) DEFAULT NULL,
  `date_give_to_client` date DEFAULT NULL,
  `date_give` date DEFAULT NULL,
  `date_approval` date DEFAULT NULL,
  `comment` text,
  `comment_give_to_client` text,
  `deleted` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cs-co-give-item-client`
--

INSERT INTO `cs-co-give-item-client` (`id`, `user_id`, `schedule_id`, `currency_id`, `staff_id`, `client_id`, `branch_id`, `status`, `active`, `approve_by`, `give_by`, `staff_give_id`, `num_give_to_client`, `date_give_to_client`, `date_give`, `date_approval`, `comment`, `comment_give_to_client`, `deleted`, `created_at`, `updated_at`) VALUES
(4, 50, 11, 1, 50, 8, 2, 1, 1, 50, 50, 50, '220120191e', '2019-01-23', '2019-01-22', '2018-07-16', 'just  update request', 'Just update for now', 1, '2019-01-22 02:01:33', '2019-01-22 15:36:59'),
(5, 50, 13, 1, 52, 10, 2, 1, 1, 50, 53, 52, '2345', '2018-06-06', '2019-02-11', '2018-07-16', 'hello word', 'hello world', 1, '2019-02-11 03:02:05', '2019-02-11 16:16:38'),
(6, 50, 8, 1, 53, 1, 2, 1, 1, 52, 50, 53, '4567', '2018-06-06', '2019-02-11', '2018-06-06', 'hw', 'hello world', 1, '2019-02-11 04:02:33', '2019-02-11 16:19:44');

-- --------------------------------------------------------

--
-- Table structure for table `cs-deposit-setting`
--

DROP TABLE IF EXISTS `cs-deposit-setting`;
CREATE TABLE IF NOT EXISTS `cs-deposit-setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `number` decimal(30,0) DEFAULT NULL,
  `num_of_percens` decimal(10,0) DEFAULT NULL,
  `note` text CHARACTER SET latin1,
  `deleted` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cs-history-logs`
--

DROP TABLE IF EXISTS `cs-history-logs`;
CREATE TABLE IF NOT EXISTS `cs-history-logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `ip_log` varchar(50) DEFAULT NULL,
  `active` varchar(250) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `what_id` int(11) DEFAULT NULL,
  `method` varchar(30) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=170 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cs-history-logs`
--

INSERT INTO `cs-history-logs` (`id`, `user_id`, `ip_log`, `active`, `status`, `what_id`, `method`, `create_date`) VALUES
(1, 50, '::1', 'កែប្រែសិទ្ធិអ្នកប្រើប្រាស់', 3, 8, 'user group', '2018-04-07 03:04:18'),
(2, 50, '::1', 'លុបសិទ្ធិអ្នកប្រើប្រាស់', 2, 5, 'user group', '2018-04-07 03:04:04'),
(3, 50, '::1', 'លុបសិទ្ធិអ្នកប្រើប្រាស់', 2, 4, 'user group', '2018-04-07 03:04:06'),
(4, 50, '::1', 'លុបសិទ្ធិអ្នកប្រើប្រាស់', 2, 3, 'user group', '2018-04-07 03:04:09'),
(5, 50, '::1', 'លុបសិទ្ធិអ្នកប្រើប្រាស់', 2, 6, 'user group', '2018-04-07 03:04:11'),
(6, 50, '::1', 'បង្កើតអតិថិជនជាលក្ខណៈបុគ្គល', 2, 1, 'customer personal', '2018-04-19 08:04:07'),
(7, 50, '::1', 'បង្កើតអតិថិជនជាលក្ខណៈបុគ្គល', 2, 2, 'customer personal', '2018-04-19 09:04:33'),
(8, 50, '::1', 'បង្កើតអតិថិជនជាលក្ខណៈបុគ្គល', 2, 9, 'customer personal', '2018-04-20 10:04:06'),
(9, 50, '::1', 'បង្កើតអតិថិជនជាលក្ខណៈបុគ្គល', 2, 10, 'customer personal', '2018-04-20 10:04:49'),
(10, 50, '::1', 'កែប្រែសិទ្ធិអ្នកប្រើប្រាស់', 3, 8, 'user group', '2018-04-24 10:04:41'),
(11, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 9, 'customer personal', '2018-04-26 10:04:44'),
(12, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 9, 'customer personal', '2018-04-26 10:04:44'),
(13, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 9, 'customer personal', '2018-04-26 10:04:51'),
(14, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 9, 'customer personal', '2018-04-26 10:04:51'),
(15, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 9, 'customer personal', '2018-04-26 10:04:53'),
(16, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 9, 'customer personal', '2018-04-26 10:04:53'),
(17, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 9, 'customer personal', '2018-04-26 12:04:26'),
(18, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 9, 'customer personal', '2018-04-26 12:04:26'),
(19, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 9, 'customer personal', '2018-04-26 12:04:50'),
(20, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 9, 'customer personal', '2018-04-26 12:04:50'),
(21, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 9, 'customer personal', '2018-04-26 12:04:51'),
(22, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 9, 'customer personal', '2018-04-26 12:04:51'),
(23, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 9, 'customer personal', '2018-04-26 12:04:12'),
(24, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 9, 'customer personal', '2018-04-26 12:04:12'),
(25, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 10, 'customer personal', '2018-04-26 12:04:41'),
(26, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 10, 'customer personal', '2018-04-26 01:04:38'),
(27, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 9, 'customer personal', '2018-04-26 01:04:22'),
(28, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 9, 'customer personal', '2018-04-26 01:04:23'),
(29, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 10, 'customer personal', '2018-04-26 01:04:38'),
(30, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 10, 'customer personal', '2018-04-26 01:04:00'),
(31, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 9, 'customer personal', '2018-04-26 01:04:26'),
(32, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 9, 'customer personal', '2018-04-26 01:04:27'),
(33, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 9, 'customer personal', '2018-04-26 01:04:51'),
(34, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 10, 'customer personal', '2018-04-26 01:04:56'),
(35, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 10, 'customer personal', '2018-04-26 01:04:57'),
(36, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 10, 'customer personal', '2018-04-26 01:04:40'),
(37, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 10, 'customer personal', '2018-04-26 01:04:41'),
(38, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 10, 'customer personal', '2018-04-26 01:04:42'),
(39, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 9, 'customer personal', '2018-04-26 01:04:32'),
(40, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 9, 'customer personal', '2018-04-26 01:04:48'),
(41, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 9, 'customer personal', '2018-04-26 01:04:33'),
(42, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 10, 'customer personal', '2018-04-26 01:04:38'),
(43, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 10, 'customer personal', '2018-04-26 01:04:39'),
(44, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 9, 'customer personal', '2018-04-26 01:04:54'),
(45, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 9, 'customer personal', '2018-04-26 01:04:55'),
(46, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 10, 'customer personal', '2018-04-26 01:04:30'),
(47, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 9, 'customer personal', '2018-04-26 01:04:41'),
(48, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 9, 'customer personal', '2018-04-26 02:04:17'),
(49, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 10, 'customer personal', '2018-04-26 02:04:25'),
(50, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 9, 'customer personal', '2018-04-26 02:04:07'),
(51, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 9, 'customer personal', '2018-04-26 02:04:46'),
(52, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 10, 'customer personal', '2018-04-26 02:04:05'),
(53, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 9, 'customer personal', '2018-04-26 02:04:10'),
(54, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 10, 'customer personal', '2018-04-26 02:04:04'),
(55, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 2, 'customer personal', '2018-04-27 08:04:02'),
(56, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 9, 'customer personal', '2018-04-27 08:04:48'),
(57, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 2, 'customer personal', '2018-04-27 08:04:07'),
(58, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 1, 'customer personal', '2018-04-27 08:04:18'),
(59, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 1, 'customer personal', '2018-04-27 08:04:58'),
(60, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 3, 'customer personal', '2018-04-27 09:04:32'),
(61, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 5, 'customer personal', '2018-04-27 09:04:08'),
(62, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 4, 'customer personal', '2018-04-27 09:04:32'),
(63, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 6, 'customer personal', '2018-04-27 09:04:57'),
(64, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 7, 'customer personal', '2018-04-27 09:04:44'),
(65, 50, '::1', 'បង្កើតសាខារថ្មី', 2, 17, 'user', '2018-04-30 01:04:36'),
(66, 50, '::1', 'បង្កើតសាខារថ្មី', 2, 18, 'user', '2018-04-30 01:04:38'),
(67, 50, '::1', 'បង្កើតសាខារថ្មី', 2, 19, 'user', '2018-04-30 01:04:36'),
(68, 50, '::1', 'បង្កើតសាខារថ្មី', 2, 20, 'user', '2018-04-30 01:04:30'),
(69, 50, '::1', 'បង្កើតសាខារថ្មី', 2, 21, 'user', '2018-05-01 04:05:06'),
(70, 50, '::1', 'បង្កើតសាខារថ្មី', 2, 22, 'user', '2018-05-01 04:05:21'),
(71, 50, '::1', 'កែរប្រែរព័ត៌មានសាខា', 2, 3, 'brand', '2018-05-01 04:05:18'),
(72, 50, '::1', 'កែរប្រែរព័ត៌មានសាខា', 2, 4, 'brand', '2018-05-01 04:05:07'),
(73, 50, '::1', 'កែរប្រែរព័ត៌មានសាខា', 2, 5, 'brand', '2018-05-01 04:05:31'),
(74, 50, '::1', 'បង្កើតសាខារថ្មី', 2, 23, 'user', '2018-05-01 04:05:55'),
(75, 50, '::1', 'លុបព័ត៌មានសាខា', 2, 3, 'brand', '2018-05-01 05:05:26'),
(76, 50, '::1', 'លុបព័ត៌មានសាខា', 2, 19, 'brand', '2018-05-01 05:05:41'),
(77, 50, '::1', 'លុបព័ត៌មានសាខា', 2, 20, 'brand', '2018-05-01 05:05:46'),
(78, 50, '::1', 'លុបព័ត៌មានសាខា', 2, 15, 'brand', '2018-05-01 05:05:55'),
(79, 50, '::1', 'បង្កើតប្រភេទរបស់ផលិតផល', 2, 1, 'Category', '2018-05-01 06:05:42'),
(80, 50, '::1', 'បង្កើតប្រភេទរបស់ផលិតផល', 2, 2, 'Category', '2018-05-01 06:05:10'),
(81, 50, '::1', 'បង្កើតប្រភេទរបស់ផលិតផល', 2, 3, 'Category', '2018-05-01 06:05:03'),
(82, 50, '::1', 'បង្កើតប្រភេទរបស់ផលិតផល', 2, 4, 'Category', '2018-05-01 06:05:43'),
(83, 50, '::1', 'បង្កើតប្រភេទរបស់ផលិតផល', 2, 5, 'Category', '2018-05-01 06:05:28'),
(84, 50, '::1', 'បង្កើតប្រភេទរបស់ផលិតផល', 2, 6, 'Category', '2018-05-01 06:05:57'),
(85, 50, '::1', 'បង្កើតប្រភេទរបស់ផលិតផល', 2, 7, 'Category', '2018-05-01 06:05:18'),
(86, 50, '::1', 'បង្កើតប្រភេទរបស់ផលិតផល', 2, 8, 'Category', '2018-05-01 06:05:47'),
(87, 50, '::1', 'បង្កើតប្រភេទរបស់ផលិតផល', 2, 9, 'Category', '2018-05-01 06:05:57'),
(88, 50, '::1', 'លុបព័ត៌មានប្រភេទរបស់ផលិតផល', 2, 9, 'Category', '2018-05-01 06:05:01'),
(89, 50, '::1', 'កែរប្រែរព័ត៌មានប្រភេទរបស់ផលិតផល', 2, 8, 'Category', '2018-05-01 06:05:18'),
(90, 50, '::1', 'ការបង្កើតអ្នកផ្គត់ផ្គង់', 2, 1, 'Category', '2018-05-01 12:05:31'),
(91, 50, '::1', 'ការបង្កើតអ្នកផ្គត់ផ្គង់', 2, 1, 'Category', '2018-05-01 12:05:03'),
(92, 50, '::1', 'ការបង្កើតអ្នកផ្គត់ផ្គង់', 2, 2, 'Category', '2018-05-01 12:05:02'),
(93, 50, '::1', 'ការបង្កើតអ្នកផ្គត់ផ្គង់', 2, 3, 'Category', '2018-05-01 12:05:21'),
(94, 50, '::1', 'ការបង្កើតអ្នកផ្គត់ផ្គង់', 2, 4, 'Category', '2018-05-01 12:05:46'),
(95, 50, '::1', 'ការបង្កើតអ្នកផ្គត់ផ្គង់', 2, 5, 'Category', '2018-05-01 12:05:11'),
(96, 50, '::1', 'ការបង្កើតអ្នកផ្គត់ផ្គង់', 2, 6, 'Category', '2018-05-01 12:05:38'),
(97, 50, '::1', 'ការបង្កើតអ្នកផ្គត់ផ្គង់', 2, 7, 'Category', '2018-05-01 12:05:04'),
(98, 50, '::1', 'ការកែប្រែរព័ត៌មានអ្នកផ្គត់ផ្គង់', 2, 4, 'Category', '2018-05-01 01:05:23'),
(99, 50, '::1', 'លុបព័ត៌មានអ្នកផ្គត់ផ្គង់', 2, 7, 'brand', '2018-05-01 01:05:52'),
(100, 50, '::1', 'លុបព័ត៌មានអ្នកផ្គត់ផ្គង់', 2, 6, 'brand', '2018-05-01 01:05:15'),
(101, 50, '::1', 'ការកែប្រែរព័ត៌មានអ្នកផ្គត់ផ្គង់', 2, 5, 'Category', '2018-05-01 01:05:11'),
(102, 50, '::1', 'ការបង្កើតអ្នកផ្គត់ផ្គង់', 2, 8, 'Category', '2018-05-01 01:05:04'),
(103, 50, '::1', 'កែរប្រែរព័ត៌មានប្រភេទរបស់ផលិតផល', 2, 8, 'Category', '2018-05-18 09:05:35'),
(104, 50, '::1', 'ការបង្កើតផលិតផលថ្មី', 2, 3, 'items', '2018-05-18 09:05:42'),
(105, 50, '::1', 'ការកែប្រែរព័ត៌មានផលិតផល', 2, 3, 'items', '2018-05-18 10:05:46'),
(106, 50, '::1', 'ការកែប្រែរព័ត៌មានផលិតផល', 2, 3, 'items', '2018-05-18 10:05:23'),
(107, 50, '::1', 'ការបង្កើតផលិតផលថ្មី', 2, 4, 'items', '2018-05-18 10:05:34'),
(108, 50, '::1', 'កែប្រែមុខតំណែង', 3, 1, 'position', '2018-06-06 09:06:25'),
(109, 50, '::1', 'លុបមុខតំណែង', 4, 1, 'position', '2018-06-06 09:06:37'),
(110, 50, '::1', 'បង្កើតមុខតំណែង', 2, 14, 'position', '2018-06-06 09:06:46'),
(111, 50, '::1', 'លុបមុខតំណែង', 4, 14, 'position', '2018-06-06 09:06:56'),
(112, 50, '::1', 'ការបង្កើតផលិតផលថ្មី', 2, 5, 'items', '2018-07-15 05:07:50'),
(113, 50, '::1', 'បង្កើតប្រភេទរបស់ផលិតផល', 2, 9, 'Category', '2018-07-15 06:07:03'),
(114, 50, '::1', 'ការបង្កើតផលិតផលថ្មី', 2, 6, 'items', '2018-07-15 06:07:17'),
(115, 50, '::1', 'ការបង្កើតផលិតផលថ្មី', 2, 7, 'items', '2018-07-15 06:07:49'),
(116, 50, '::1', 'បង្កើតប្រភេទរបស់ផលិតផល', 2, 10, 'Category', '2018-07-15 06:07:33'),
(117, 50, '::1', 'បង្កើតប្រភេទរបស់ផលិតផល', 2, 11, 'Category', '2018-07-15 06:07:48'),
(118, 50, '::1', 'បង្កើតប្រភេទរបស់ផលិតផល', 2, 12, 'Category', '2018-07-15 06:07:50'),
(119, 50, '::1', 'បង្កើតប្រភេទរបស់ផលិតផល', 2, 13, 'Category', '2018-07-15 06:07:15'),
(120, 50, '::1', 'កែរប្រែរព័ត៌មានប្រភេទរបស់ផលិតផល', 2, 13, 'Category', '2018-07-15 06:07:24'),
(121, 50, '::1', 'បង្កើតប្រភេទរបស់ផលិតផល', 2, 14, 'Category', '2018-07-15 06:07:59'),
(122, 50, '::1', 'ការបង្កើតផលិតផលថ្មី', 2, 8, 'items', '2018-07-15 06:07:18'),
(123, 50, '::1', 'ការបង្កើតផលិតផលថ្មី', 2, 9, 'items', '2018-07-15 06:07:41'),
(124, 50, '::1', 'ការបង្កើតអ្នកផ្គត់ផ្គង់', 2, 9, 'Suppliers', '2018-08-14 02:08:04'),
(125, 50, '::1', 'ការបង្កើតអ្នកផ្គត់ផ្គង់', 2, 10, 'Suppliers', '2018-08-14 02:08:20'),
(126, 50, '::1', 'លុបព័ត៌មានអ្នកផ្គត់ផ្គង់', 2, 8, 'Suppliers', '2018-08-14 03:08:53'),
(127, 50, '::1', 'លុបព័ត៌មានអ្នកផ្គត់ផ្គង់', 2, 5, 'Suppliers', '2018-08-14 03:08:05'),
(128, 50, '::1', 'លុបព័ត៌មានអ្នកផ្គត់ផ្គង់', 2, 10, 'Suppliers', '2018-08-14 03:08:22'),
(129, 50, '::1', 'ការបង្កើតអ្នកផ្គត់ផ្គង់', 2, 11, 'Suppliers', '2018-08-14 03:08:44'),
(130, 50, '::1', 'លុបព័ត៌មានអ្នកផ្គត់ផ្គង់', 2, 11, 'Suppliers', '2018-08-14 03:08:08'),
(131, 50, '::1', 'ការបង្កើតអ្នកផ្គត់ផ្គង់', 2, 12, 'Suppliers', '2018-08-14 03:08:24'),
(132, 50, '::1', 'លុបព័ត៌មានអ្នកផ្គត់ផ្គង់', 2, 12, 'Suppliers', '2018-08-14 03:08:45'),
(133, 50, '::1', 'ការបង្កើតអ្នកផ្គត់ផ្គង់', 2, 13, 'Suppliers', '2018-08-14 04:08:43'),
(134, 50, '::1', 'លុបព័ត៌មានអ្នកផ្គត់ផ្គង់', 2, 13, 'Suppliers', '2018-08-14 04:08:02'),
(135, 50, '::1', 'ការបង្កើតអ្នកផ្គត់ផ្គង់', 2, 14, 'Suppliers', '2018-08-14 04:08:12'),
(136, 50, '::1', 'លុបព័ត៌មានអ្នកផ្គត់ផ្គង់', 2, 14, 'Suppliers', '2018-08-14 04:08:48'),
(137, 50, '::1', 'ការបង្កើតអ្នកផ្គត់ផ្គង់', 2, 15, 'Suppliers', '2018-08-14 04:08:55'),
(138, 50, '::1', 'លុបព័ត៌មានអ្នកផ្គត់ផ្គង់', 2, 15, 'Suppliers', '2018-08-14 04:08:10'),
(139, 50, '::1', 'ការបង្កើតអ្នកផ្គត់ផ្គង់', 2, 16, 'Suppliers', '2018-08-14 04:08:54'),
(140, 50, '::1', 'លុបព័ត៌មានអ្នកផ្គត់ផ្គង់', 2, 16, 'Suppliers', '2018-08-14 04:08:06'),
(141, 50, '::1', 'ការបង្កើតអ្នកផ្គត់ផ្គង់', 2, 17, 'Suppliers', '2018-08-14 04:08:39'),
(142, 50, '::1', 'លុបព័ត៌មានអ្នកផ្គត់ផ្គង់', 2, 17, 'Suppliers', '2018-08-14 04:08:10'),
(143, 50, '::1', 'ការបង្កើតអ្នកផ្គត់ផ្គង់', 2, 18, 'Suppliers', '2018-08-14 04:08:11'),
(144, 50, '::1', 'លុបព័ត៌មានអ្នកផ្គត់ផ្គង់', 2, 18, 'Suppliers', '2018-08-14 04:08:27'),
(145, 50, '::1', 'ការបង្កើតអ្នកផ្គត់ផ្គង់', 2, 19, 'Suppliers', '2018-08-14 04:08:39'),
(146, 50, '::1', 'លុបព័ត៌មានអ្នកផ្គត់ផ្គង់', 2, 19, 'Suppliers', '2018-08-14 04:08:20'),
(147, 50, '::1', 'ការបង្កើតអ្នកផ្គត់ផ្គង់', 2, 20, 'Suppliers', '2018-08-14 04:08:28'),
(148, 50, '::1', 'លុបព័ត៌មានអ្នកផ្គត់ផ្គង់', 2, 20, 'Suppliers', '2018-08-14 04:08:24'),
(149, 50, '::1', 'ការបង្កើតអ្នកផ្គត់ផ្គង់', 2, 21, 'Suppliers', '2018-08-14 04:08:21'),
(150, 50, '::1', 'ការបង្កើតអ្នកផ្គត់ផ្គង់', 2, 22, 'Suppliers', '2018-08-14 05:08:03'),
(151, 50, '::1', 'លុបព័ត៌មានអ្នកផ្គត់ផ្គង់', 2, 22, 'Suppliers', '2018-08-14 05:08:28'),
(152, 50, '::1', 'លុបព័ត៌មានអ្នកផ្គត់ផ្គង់', 2, 21, 'Suppliers', '2018-08-14 05:08:29'),
(153, 50, '::1', 'ការបង្កើតអ្នកផ្គត់ផ្គង់', 2, 23, 'Suppliers', '2018-08-14 05:08:11'),
(154, 50, '::1', 'លុបព័ត៌មានអ្នកផ្គត់ផ្គង់', 2, 23, 'Suppliers', '2018-08-14 05:08:44'),
(155, 50, '::1', 'ការបង្កើតអ្នកផ្គត់ផ្គង់', 2, 24, 'Suppliers', '2018-08-14 05:08:05'),
(156, 50, '::1', 'ការបង្កើតផលិតផលថ្មី', 2, 10, 'items', '2018-08-14 05:08:24'),
(157, 50, '::1', 'បង្កើតអ្នកប្រើប្រាស់', 2, 52, 'user', '2018-09-06 07:09:15'),
(158, 50, '::1', 'កែប្រែរអ្នកប្រើប្រាស់', 3, 50, 'user', '2018-09-06 07:09:30'),
(159, 52, '::1', 'កែប្រែរអ្នកប្រើប្រាស់', 3, 50, 'user', '2018-09-06 07:09:31'),
(160, 50, '::1', 'កែប្រែរអ្នកប្រើប្រាស់', 3, 52, 'user', '2018-09-06 07:09:58'),
(161, 50, '::1', 'ការបង្កើតផលិតផលថ្មី', 2, 11, 'items', '2018-10-18 06:10:02'),
(162, 50, '::1', 'បង្កើតអ្នកប្រើប្រាស់', 2, 53, 'user', '2018-11-19 10:11:29'),
(163, 50, '::1', 'បង្កើតការកំណត់ពិន័យ', 2, 2, 'penaty', '2019-02-06 04:02:37'),
(164, 50, '::1', 'កែប្រែការកំណត់ពិន័យ', 3, 1, 'penaty', '2019-02-06 04:02:55'),
(165, 50, '::1', 'បង្កើតការកំណត់ពិន័យ', 2, 3, 'penaty', '2019-02-08 02:02:13'),
(166, 50, '::1', 'បង្កើតការកំណត់ពិន័យ', 2, 4, 'penaty', '2019-02-08 02:02:07'),
(167, 50, '::1', 'បង្កើតការកំណត់ពិន័យ', 2, 5, 'penaty', '2019-02-08 02:02:52'),
(168, 50, '::1', 'បង្កើតការកំណត់ពិន័យ', 2, 6, 'penaty', '2019-02-08 02:02:47'),
(169, 50, '::1', 'បង្កើតអតិថិជនជាលក្ខណៈបុគ្គល', 2, 11, 'customer personal', '2019-02-17 06:02:29');

-- --------------------------------------------------------

--
-- Table structure for table `cs-instocks`
--

DROP TABLE IF EXISTS `cs-instocks`;
CREATE TABLE IF NOT EXISTS `cs-instocks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `size` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `qty` decimal(30,0) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  `date_include` date DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cs-interest-crdit-sell`
--

DROP TABLE IF EXISTS `cs-interest-crdit-sell`;
CREATE TABLE IF NOT EXISTS `cs-interest-crdit-sell` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `rate-name` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `size_money_from` decimal(30,0) DEFAULT NULL,
  `size_money_to` decimal(30,0) DEFAULT NULL,
  `day_rate_villige` decimal(30,0) DEFAULT NULL,
  `weekly_rate_villige` decimal(30,0) DEFAULT NULL,
  `two_weekly_rate_villige` decimal(30,0) DEFAULT NULL,
  `monthly_rate_villige` decimal(30,0) DEFAULT NULL,
  `day_rate_brand` decimal(30,0) DEFAULT NULL,
  `weekly_rate_brand` decimal(30,0) DEFAULT NULL,
  `two_weekly_brand` decimal(30,0) DEFAULT NULL,
  `monthly_rate_brand` decimal(30,0) DEFAULT NULL,
  `duration` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `methot description` text CHARACTER SET latin1,
  `deleted` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cs-inventorys`
--

DROP TABLE IF EXISTS `cs-inventorys`;
CREATE TABLE IF NOT EXISTS `cs-inventorys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) DEFAULT '0',
  `branch_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `comment` text,
  `status_product` int(11) DEFAULT NULL,
  `status_transation` varchar(11) DEFAULT NULL,
  `spacifies` int(11) DEFAULT '0',
  `qty` float(30,2) DEFAULT '0.00',
  `qty_free` double DEFAULT '0',
  `size` varchar(60) DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cs-inventorys`
--

INSERT INTO `cs-inventorys` (`id`, `item_id`, `branch_id`, `user_id`, `comment`, `status_product`, `status_transation`, `spacifies`, `qty`, `qty_free`, `size`, `deleted`, `created_at`, `updated_at`) VALUES
(1, 3, 2, 50, NULL, NULL, 'add new', 0, 5.00, 0, NULL, NULL, NULL, '2018-05-18 10:34:22'),
(2, 4, 2, 50, NULL, NULL, 'add new', 0, 10.00, 0, NULL, NULL, NULL, NULL),
(5, 3, 2, 50, NULL, NULL, 'Sale', 8, -1.00, 0, NULL, NULL, NULL, NULL),
(6, 4, 2, 50, NULL, NULL, 'Sale', 8, -1.00, 0, NULL, NULL, NULL, NULL),
(9, 4, 2, 50, NULL, NULL, 'Sale', 11, -2.00, 0, NULL, NULL, NULL, NULL),
(10, 5, 2, 50, NULL, NULL, 'add new', 0, 50.00, 0, NULL, NULL, NULL, NULL),
(11, 6, 2, 50, NULL, NULL, 'add new', 0, 10.00, 0, NULL, NULL, NULL, NULL),
(12, 7, 2, 50, NULL, NULL, 'add new', 0, 10.00, 0, NULL, NULL, NULL, NULL),
(13, 8, 2, 50, NULL, NULL, 'add new', 0, 10.00, 0, NULL, NULL, NULL, NULL),
(14, 9, 2, 50, NULL, NULL, 'add new', 0, 10.00, 0, NULL, NULL, NULL, NULL),
(15, 6, 2, 50, NULL, NULL, 'Sale', 12, -1.00, 0, NULL, NULL, NULL, NULL),
(16, 8, 2, 50, NULL, NULL, 'Sale', 12, -1.00, 0, NULL, NULL, NULL, NULL),
(17, 7, 2, 50, NULL, NULL, 'Sale', 12, -1.00, 0, NULL, NULL, NULL, NULL),
(18, 8, 2, 50, NULL, NULL, 'Sale', 13, -1.00, 0, NULL, NULL, NULL, NULL),
(19, 9, 2, 50, NULL, NULL, 'Sale', 13, -1.00, 0, NULL, NULL, NULL, NULL),
(20, 10, 2, 50, NULL, NULL, 'add new', 0, 10.00, 0, NULL, NULL, NULL, NULL),
(34, 5, 2, 50, NULL, NULL, 'receiving', 20, 15.00, 5, NULL, NULL, '2018-09-06 05:09:18', '2018-09-06 05:09:18'),
(35, 3, 2, 50, NULL, NULL, 'receiving', 20, 6.00, 0, NULL, NULL, '2018-09-06 05:09:18', '2018-09-06 05:09:18'),
(36, 3, 2, 53, NULL, NULL, 'receiving', 22, 10.00, 0, NULL, NULL, '2018-12-28 03:12:24', '2018-12-28 03:12:24'),
(37, 11, 2, 53, NULL, NULL, 'receiving', 22, 10.00, 0, NULL, NULL, '2018-12-28 03:12:24', '2018-12-28 03:12:24'),
(38, 8, 2, 53, NULL, NULL, 'receiving', 22, 10.00, 0, NULL, NULL, '2018-12-28 03:12:24', '2018-12-28 03:12:24'),
(42, 5, 2, 50, NULL, NULL, 'Sale', 14, -1.00, 0, NULL, NULL, NULL, NULL),
(43, 3, 2, 50, NULL, NULL, 'Sale', 14, -1.00, 0, NULL, NULL, NULL, NULL),
(47, 3, 2, 50, NULL, NULL, 'Sale', 9, -10.00, 0, NULL, NULL, NULL, NULL),
(48, 5, 2, 50, NULL, NULL, 'Sale', 9, -10.00, 0, NULL, NULL, NULL, NULL),
(49, 8, 2, 50, NULL, NULL, 'Sale', 9, -2.00, 0, NULL, NULL, NULL, NULL),
(50, 3, 2, 50, NULL, NULL, 'Sale', 15, 1.00, 0, NULL, NULL, NULL, NULL),
(51, 4, 2, 50, NULL, NULL, 'Sale', 15, 1.00, 0, NULL, NULL, NULL, NULL),
(52, 5, 2, 50, NULL, NULL, 'Sale', 16, 1.00, 0, NULL, NULL, NULL, NULL),
(53, 3, 2, 50, NULL, NULL, 'Sale', 16, 1.00, 0, NULL, NULL, NULL, NULL),
(63, 3, 2, 50, NULL, NULL, 'Sale', 21, 10.00, 0, NULL, NULL, NULL, NULL),
(64, 5, 2, 50, NULL, NULL, 'Sale', 21, 10.00, 0, NULL, NULL, NULL, NULL),
(65, 8, 2, 50, NULL, NULL, 'Sale', 21, 2.00, 0, NULL, NULL, NULL, NULL),
(66, 8, 2, 50, NULL, NULL, 'Sale', 22, 1.00, 0, NULL, NULL, NULL, NULL),
(67, 9, 2, 50, NULL, NULL, 'Sale', 22, 1.00, 0, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cs-item-approval`
--

DROP TABLE IF EXISTS `cs-item-approval`;
CREATE TABLE IF NOT EXISTS `cs-item-approval` (
  `cs_approval_id` int(11) NOT NULL DEFAULT '0',
  `product_id` int(11) DEFAULT '0',
  `sell_item_id` int(11) DEFAULT '0',
  `total_price` float DEFAULT '0',
  `qty` float DEFAULT '0',
  `sell_price` float DEFAULT '0',
  `cost_price` float DEFAULT '0',
  `total_tax` float DEFAULT '0',
  `commission` float DEFAULT '0',
  `other_price` float DEFAULT '0',
  `discount` float DEFAULT '0',
  `total_price_payment` float DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cs-item-approval`
--

INSERT INTO `cs-item-approval` (`cs_approval_id`, `product_id`, `sell_item_id`, `total_price`, `qty`, `sell_price`, `cost_price`, `total_tax`, `commission`, `other_price`, `discount`, `total_price_payment`) VALUES
(13, 8, 11, 0, 1, 150, 80, 0, 0, 0, 0, 150),
(13, 9, 12, 0, 1, 1000, 700, 0, 0, 0, 0, 1000),
(11, 4, 7, 1.2, 2, 200, 117, 1.2, 0.24, 0, 0, 196),
(8, 3, 3, 2.6, 1, 400, 300, 2.6, 3, 0, 0, 396),
(8, 4, 4, 0.6, 1, 200, 117, 0.6, 0.12, 0, 0, 198);

-- --------------------------------------------------------

--
-- Table structure for table `cs-manager-branch-approval`
--

DROP TABLE IF EXISTS `cs-manager-branch-approval`;
CREATE TABLE IF NOT EXISTS `cs-manager-branch-approval` (
  `id` int(11) NOT NULL,
  `sale_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) DEFAULT '0',
  `staff_id` int(11) DEFAULT '0',
  `client_id` int(11) DEFAULT '0',
  `cds_request_id` int(11) DEFAULT '0',
  `interest_of_owne_precent` float DEFAULT '0',
  `num_of_pro` double DEFAULT '0',
  `is_agree` int(11) DEFAULT '0',
  `deposit_precent` float DEFAULT '0',
  `deposit_fixed` float DEFAULT '0',
  `deposit_fixed_word` varchar(255) DEFAULT NULL,
  `prices_total_num` double DEFAULT '0',
  `prices_totalword` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `money_owne` double DEFAULT '0',
  `money_owne_word` text,
  `currency` int(11) DEFAULT '0',
  `duration_pay_money` float DEFAULT '0',
  `duration_pay_money_type` varchar(60) DEFAULT NULL,
  `date_for_payments` date DEFAULT NULL,
  `date_approval` date DEFAULT NULL,
  `date_give_product` date DEFAULT NULL,
  `type_of_fomula` int(11) DEFAULT '1',
  `method` varchar(60) DEFAULT NULL,
  `place_for_pay` int(11) DEFAULT '0',
  `status` int(11) DEFAULT '0',
  `comment_manager` text,
  `manager_id1` int(11) DEFAULT '0',
  `manager_id2` int(11) DEFAULT '0',
  `manager_id3` int(11) DEFAULT '0',
  `is_finish` int(11) DEFAULT '0',
  `description` text,
  `deleted` int(11) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cs-manager-branch-approval`
--

INSERT INTO `cs-manager-branch-approval` (`id`, `sale_id`, `user_id`, `staff_id`, `client_id`, `cds_request_id`, `interest_of_owne_precent`, `num_of_pro`, `is_agree`, `deposit_precent`, `deposit_fixed`, `deposit_fixed_word`, `prices_total_num`, `prices_totalword`, `money_owne`, `money_owne_word`, `currency`, `duration_pay_money`, `duration_pay_money_type`, `date_for_payments`, `date_approval`, `date_give_product`, `type_of_fomula`, `method`, `place_for_pay`, `status`, `comment_manager`, `manager_id1`, `manager_id2`, `manager_id3`, `is_finish`, `description`, `deleted`, `created_at`, `updated_at`) VALUES
(8, 8, 50, 50, 1, 8, 0.112, 0, 1, 30, 180.96, 'one hundred eighty point ninety six', 603.2, 'six hundred zero three point twenty', 422.24, 'four hundred twenty two point twenty four', 1, 6, 'month', '2018-07-06', '2018-06-06', '2018-06-06', 1, 'sale_by_credit', 1, 1, 'hello world', 50, 53, 52, 0, 'hello word', 1, '2019-02-11 03:02:41', '2019-02-11 15:55:59'),
(11, 11, 50, 50, 8, 11, 0.112, 0, 1, 30, 120.36, 'one hundred  twenty', 401.2, 'four hundred zero one', 280.84, 'two hundred eighty one', 1, 6, 'month', '2018-07-28', '2018-07-16', '2018-07-20', 1, 'sale_by_credit', 1, 1, 'just two of create', 50, 50, 50, 0, 'hello world', 1, '2018-07-16 07:07:30', '2018-07-25 04:29:27'),
(13, 13, 50, 50, 10, 13, 0.112, 0, 1, 30, 345, 'three hundred forty five us', 1150, 'eleven hundred fifty us', 805, 'eight hundred zero five', 1, 6, 'month', '2018-07-24', '2018-07-16', '2018-07-19', 1, 'sale_by_credit', 1, 1, 'hello because', 50, 50, 50, 0, 'hello word', 1, '2018-07-16 07:07:25', '2018-08-01 08:11:24');

-- --------------------------------------------------------

--
-- Table structure for table `cs-payments`
--

DROP TABLE IF EXISTS `cs-payments`;
CREATE TABLE IF NOT EXISTS `cs-payments` (
  `id` int(11) NOT NULL,
  `sale_id` int(11) DEFAULT '0',
  `place_for_pay` int(11) DEFAULT '0',
  `payment_amount` double(30,3) DEFAULT '0.000',
  `sub_payment_amount` double(30,3) DEFAULT '0.000',
  `total_payment` double(30,3) DEFAULT '0.000',
  `discount_payment` float DEFAULT '0',
  `discount_reason` text,
  `dute_payment_amount` double(30,3) DEFAULT '0.000',
  `remaining_defualt` float DEFAULT '0',
  `remaining_change` float DEFAULT '0',
  `payback_defualt` float DEFAULT '0',
  `payback_change` float DEFAULT '0',
  `comment` text,
  `payment_date` date DEFAULT NULL,
  `payment_for_date` date DEFAULT NULL,
  `penalty` double(30,3) DEFAULT '0.000',
  `remaining` varchar(255) DEFAULT NULL,
  `total_tax` float DEFAULT '0',
  `total_qty` float DEFAULT '0',
  `payment_shipping` double DEFAULT '0',
  `payment_penalty` double DEFAULT '0',
  `return_back_type` int(11) DEFAULT NULL,
  `currency` int(11) DEFAULT '0',
  `status` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cs-payments`
--

INSERT INTO `cs-payments` (`id`, `sale_id`, `place_for_pay`, `payment_amount`, `sub_payment_amount`, `total_payment`, `discount_payment`, `discount_reason`, `dute_payment_amount`, `remaining_defualt`, `remaining_change`, `payback_defualt`, `payback_change`, `comment`, `payment_date`, `payment_for_date`, `penalty`, `remaining`, `total_tax`, `total_qty`, `payment_shipping`, `payment_penalty`, `return_back_type`, `currency`, `status`) VALUES
(9, 9, 1, 4526.000, 5000.000, 5000.000, 10, 'Spacail Price', 4526.000, 0, 0, 0, 0, 'Just Sale By Cash', '2019-01-06', NULL, 0.000, 'pay_on_direct_sale', 26, 22, 0, 0, NULL, 1, 1),
(12, 12, 1, 2165.000, 2165.000, 2165.000, 0, NULL, 2165.000, 0, 0, 0, 0, 'Sale By Cash', '2018-07-15', NULL, 0.000, 'pay_on_direct_sale', 0, 3, 0, 0, NULL, 1, 1),
(14, 14, 1, 472.610, 470.000, 470.000, 0, NULL, 472.600, NULL, NULL, NULL, NULL, 'new payment', '2019-01-06', NULL, 0.000, 'pay_on_direct_sale', 2.6, 2, 0, 0, NULL, 1, 1),
(15, 15, 1, -603.200, -600.000, -600.000, 0, NULL, -603.200, NULL, NULL, NULL, NULL, 'Return tosale_by_credit ID: 8', '2019-01-08', NULL, 0.000, 'pay_for_owned', -3.2, -2, 0, 0, NULL, 1, 1),
(16, 16, 1, -472.590, -470.000, -470.000, 0, NULL, -472.600, NULL, NULL, NULL, NULL, 'Return tosale_by_cash ID: 14', '2019-01-08', NULL, 0.000, 'pay_for_owned', -2.6, -2, 0, 0, NULL, 1, 1),
(21, 21, 1, -4073.400, -5000.000, -5000.000, 10, 'Spacail Price', -4073.400, 452.6, 1855700, 452, 2500, 'Return tosale_by_cash ID: 9', '2019-01-17', NULL, 0.000, 'pay_for_owned', -26, -22, 0, 0, 2, 1, 1),
(22, 22, 1, -1035.000, -1150.000, -1150.000, 0, NULL, -1035.000, 0, 0, 0, 0, 'Return tosale_by_credit ID: 13', '2019-01-18', NULL, 0.000, 'pay_for_owned', 0, -2, 0, 0, 2, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `cs-sale-item`
--

DROP TABLE IF EXISTS `cs-sale-item`;
CREATE TABLE IF NOT EXISTS `cs-sale-item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sale_id` int(11) DEFAULT '0',
  `barcode` varchar(255) DEFAULT NULL,
  `product_id` int(11) DEFAULT '0',
  `total_price` double(30,3) DEFAULT '0.000',
  `total_of_deposit` double(10,3) DEFAULT '0.000',
  `total_price_payment` double(10,3) DEFAULT '0.000',
  `qty` float(11,3) DEFAULT '0.000',
  `sell_price` double DEFAULT '0',
  `cost_price` double DEFAULT '0',
  `discount` float DEFAULT '0',
  `total_tax` float DEFAULT '0',
  `commission` float DEFAULT '0',
  `other_price` float DEFAULT '0',
  `coment_on_item` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cs-sale-item`
--

INSERT INTO `cs-sale-item` (`id`, `sale_id`, `barcode`, `product_id`, `total_price`, `total_of_deposit`, `total_price_payment`, `qty`, `sell_price`, `cost_price`, `discount`, `total_tax`, `commission`, `other_price`, `coment_on_item`) VALUES
(3, 8, '0000000003', 3, 400.000, 0.000, 396.000, 1.000, 400, 300, 0, 2.6, 3, 0, NULL),
(4, 8, '0000000004', 4, 200.000, 0.000, 198.000, 1.000, 200, 117, 0, 0.6, 0.12, 0, NULL),
(7, 11, '0000000004', 4, 400.000, 0.000, 196.000, 2.000, 200, 117, 0, 1.2, 0.24, 0, NULL),
(8, 12, '0000000006', 6, 1215.000, 0.000, 1202.850, 1.000, 1215, 1000, 0, 0, 0, 0, NULL),
(9, 12, '0000000008', 8, 150.000, 0.000, 148.500, 1.000, 150, 80, 0, 0, 0, 0, NULL),
(10, 12, '0000000007', 7, 800.000, 0.000, 792.000, 1.000, 800, 700, 0, 0, 0, 0, NULL),
(11, 13, '0000000008', 8, 150.000, 0.000, 150.000, 1.000, 150, 80, 0, 0, 0, 0, NULL),
(12, 13, '0000000009', 9, 1000.000, 0.000, 1000.000, 1.000, 1000, 700, 0, 0, 0, 0, NULL),
(16, 14, '0000000005', 5, 70.000, 0.000, 69.300, 1.000, 70, 50, 0, 0, 0, 0, NULL),
(17, 14, '0000000003', 3, 400.000, 0.000, 396.000, 1.000, 400, 300, 0, 2.6, 3, 0, NULL),
(21, 9, '0000000003', 3, 4000.000, 0.000, 360.000, 10.000, 400, 300, 0, 26, 30, 0, NULL),
(22, 9, '0000000005', 5, 700.000, 0.000, 63.000, 10.000, 70, 50, 0, 0, 0, 0, NULL),
(23, 9, '0000000008', 8, 300.000, 0.000, 147.000, 2.000, 150, 80, 0, 0, 0, 0, NULL),
(24, 15, '0000000003', 3, -400.000, 0.000, 400.000, -1.000, 400, 300, 0, -2.6, -3, 0, NULL),
(25, 15, '0000000004', 4, -200.000, 0.000, 200.000, -1.000, 200, 117, 0, -0.6, -0.12, 0, NULL),
(26, 16, '0000000005', 5, -70.000, 0.000, 70.000, -1.000, 70, 50, 0, -0, -0, 0, NULL),
(27, 16, '0000000003', 3, -400.000, 0.000, 400.000, -1.000, 400, 300, 0, -2.6, -3, 0, NULL),
(28, 18, '0000000003', 3, -4000.000, 0.000, 400.000, -10.000, 400, 300, 0, -26, -30, 0, NULL),
(29, 18, '0000000005', 5, -700.000, 0.000, 70.000, -10.000, 70, 50, 0, -0, -0, 0, NULL),
(30, 18, '0000000008', 8, -300.000, 0.000, 150.000, -2.000, 150, 80, 0, -0, -0, 0, NULL),
(31, 19, '0000000003', 3, -4000.000, 0.000, 400.000, -10.000, 400, 300, 0, -26, -30, 0, NULL),
(32, 19, '0000000005', 5, -700.000, 0.000, 70.000, -10.000, 70, 50, 0, -0, -0, 0, NULL),
(33, 19, '0000000008', 8, -300.000, 0.000, 150.000, -2.000, 150, 80, 0, -0, -0, 0, NULL),
(34, 20, '0000000003', 3, -4000.000, 0.000, 400.000, -10.000, 400, 300, 0, -26, -30, 0, NULL),
(35, 20, '0000000005', 5, -700.000, 0.000, 70.000, -10.000, 70, 50, 0, -0, -0, 0, NULL),
(36, 20, '0000000008', 8, -300.000, 0.000, 150.000, -2.000, 150, 80, 0, -0, -0, 0, NULL),
(37, 21, '0000000003', 3, -4000.000, 0.000, 400.000, -10.000, 400, 300, 0, -26, -30, 0, NULL),
(38, 21, '0000000005', 5, -700.000, 0.000, 70.000, -10.000, 70, 50, 0, -0, -0, 0, NULL),
(39, 21, '0000000008', 8, -300.000, 0.000, 150.000, -2.000, 150, 80, 0, -0, -0, 0, NULL),
(40, 22, '0000000008', 8, -150.000, 0.000, 150.000, -1.000, 150, 80, 0, -0, -0, 0, NULL),
(41, 22, '0000000009', 9, -1000.000, 0.000, 1000.000, -1.000, 1000, 700, 0, -0, -0, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cs-sales`
--

DROP TABLE IF EXISTS `cs-sales`;
CREATE TABLE IF NOT EXISTS `cs-sales` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) DEFAULT '0',
  `num_invoice` varchar(60) DEFAULT NULL,
  `user_id` int(11) DEFAULT '0',
  `staff_id` int(11) DEFAULT '0',
  `branch_id` int(11) DEFAULT '0',
  `currency_id` int(11) DEFAULT '0',
  `sale_status_for` varchar(60) DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `is_oncredit` int(11) DEFAULT '0',
  `is_approve` int(11) DEFAULT '0',
  `status` int(11) DEFAULT '0',
  `active` int(11) DEFAULT '0',
  `method` varchar(255) DEFAULT NULL,
  `description` text,
  `deleted` int(11) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cs-sales`
--

INSERT INTO `cs-sales` (`id`, `client_id`, `num_invoice`, `user_id`, `staff_id`, `branch_id`, `currency_id`, `sale_status_for`, `due_date`, `is_oncredit`, `is_approve`, `status`, `active`, `method`, `description`, `deleted`, `created_at`, `updated_at`, `deleted_at`) VALUES
(8, 1, 'PP201807068', 50, 50, 2, 1, 'close', '2018-07-06', 1, 1, 1, 1, 'sale_by_credit', NULL, 1, '2018-07-06 08:07:35', '2019-02-11 15:27:41', NULL),
(9, 1, 'PP201807099', 50, 50, 2, 1, 'close', '2019-01-06', 0, 0, 1, 1, 'sale_by_cash', NULL, 1, '2018-07-09 08:07:25', '2019-01-17 18:20:45', NULL),
(11, 8, 'PP2018071011', 50, 50, 2, 1, 'padding', '2018-07-10', 1, 1, 1, 1, 'sale_by_credit', NULL, 1, '2018-07-10 07:07:37', '2018-07-10 07:13:37', NULL),
(12, 9, 'PP2018071512', 50, 50, 2, 1, 'padding', '2018-07-15', 0, 0, 1, 1, 'sale_by_cash', NULL, 1, '2018-07-15 06:07:50', '2018-07-15 06:26:50', NULL),
(13, 10, 'PP2018071513', 50, 50, 2, 1, 'close', '2018-07-15', 1, 1, 1, 1, 'sale_by_credit', NULL, 1, '2018-07-15 06:07:33', '2019-01-18 16:27:10', NULL),
(14, 2, 'PP2018122814', 50, 50, 2, 1, 'close', '2019-01-06', 0, 0, 1, 1, 'sale_by_cash', NULL, 1, '2018-12-28 03:12:47', '2019-01-08 17:19:21', NULL),
(15, 1, 'PP2019010815', 50, 50, 2, 1, 'close', '2019-01-08', 1, 0, 1, 1, 'sale_recieving', NULL, 1, '2019-01-08 05:01:36', '2019-01-08 17:00:36', NULL),
(16, 2, 'PP2019010816', 50, 50, 2, 1, 'close', '2019-01-08', 1, 0, 1, 1, 'sale_recieving', NULL, 1, '2019-01-08 05:01:20', '2019-01-08 17:19:21', NULL),
(21, 1, 'PP2019011721', 50, 50, 2, 1, 'close', '2019-01-17', 1, 0, 1, 1, 'sale_recieving', NULL, 1, '2019-01-17 06:01:45', '2019-01-17 18:20:45', NULL),
(22, 10, 'PP2019011822', 50, 50, 2, 1, 'close', '2019-01-18', 1, 0, 1, 1, 'sale_recieving', NULL, 1, '2019-01-18 04:01:10', '2019-01-18 16:27:10', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cs-schedules`
--

DROP TABLE IF EXISTS `cs-schedules`;
CREATE TABLE IF NOT EXISTS `cs-schedules` (
  `id` int(11) NOT NULL DEFAULT '0',
  `schedule_number` varchar(60) DEFAULT NULL,
  `client_id` int(11) DEFAULT '0',
  `sale_id` int(11) DEFAULT '0',
  `branch_id` int(11) DEFAULT '0',
  `approval_id` int(11) DEFAULT '0',
  `user_id` int(11) DEFAULT '0',
  `staff_id` int(11) DEFAULT '0',
  `currency_id` int(11) DEFAULT '0',
  `money_owne_cost` double(30,2) DEFAULT '0.00',
  `money_owne_interest` double(30,2) DEFAULT '0.00',
  `money_owne_total_pay` double(30,2) DEFAULT '0.00',
  `status` int(11) DEFAULT '0',
  `status_for_pay` int(11) DEFAULT '0',
  `is_give` int(11) DEFAULT '0',
  `active` int(11) DEFAULT '0',
  `is_finish` int(11) DEFAULT '0',
  `deleted` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cs-schedules`
--

INSERT INTO `cs-schedules` (`id`, `schedule_number`, `client_id`, `sale_id`, `branch_id`, `approval_id`, `user_id`, `staff_id`, `currency_id`, `money_owne_cost`, `money_owne_interest`, `money_owne_total_pay`, `status`, `status_for_pay`, `is_give`, `active`, `is_finish`, `deleted`, `created_at`, `updated_at`) VALUES
(11, 'PP2018071011', 8, 11, 2, 11, 50, 50, 1, 280.84, 188.70, 469.54, 1, 2, 1, 1, 0, 1, '2018-07-25 04:07:28', '2019-02-18 17:20:17'),
(13, 'PP2018071513', 10, 13, 2, 13, 50, 50, 1, 805.00, 540.96, 1345.96, 1, 0, 1, 1, 0, 1, '2018-08-01 08:08:25', '2019-02-11 16:16:38'),
(8, 'PN201902118', 1, 8, 2, 8, 50, 50, 1, 422.22, 283.74, 705.96, 1, 0, 1, 1, 0, 1, '2019-02-11 03:02:50', '2019-02-11 16:19:44');

-- --------------------------------------------------------

--
-- Table structure for table `cs-schedules-repayment-timesheet`
--

DROP TABLE IF EXISTS `cs-schedules-repayment-timesheet`;
CREATE TABLE IF NOT EXISTS `cs-schedules-repayment-timesheet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cs_schedules_id` int(11) DEFAULT '0',
  `branch_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `staff_id` int(11) DEFAULT NULL,
  `schedules_titmesheet_id` int(11) DEFAULT NULL,
  `num_invoice_payment` varchar(60) DEFAULT NULL,
  `available_total_pay_cost` double(30,2) DEFAULT '0.00',
  `available_total_pay_interest` double(30,2) DEFAULT '0.00',
  `available_total_payment` double(30,2) DEFAULT '0.00',
  `available_total_due_payment` double(32,3) DEFAULT '0.000',
  `available_total_pay_cost_owe` double(30,2) DEFAULT '0.00',
  `other_pay_reason` text,
  `other_payment` double(30,2) DEFAULT '0.00',
  `date_payment` date DEFAULT NULL,
  `available_date_payment` date DEFAULT NULL,
  `lavel_payment` int(11) DEFAULT '0',
  `date_late_pay` float DEFAULT NULL,
  `is_penalty_pay` int(11) DEFAULT '0',
  `panalty` double(32,3) DEFAULT '0.000',
  `available_remain` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `status_for_pay` int(11) DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  `note` text,
  `deleted_at` datetime DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cs-schedules-repayment-timesheet`
--

INSERT INTO `cs-schedules-repayment-timesheet` (`id`, `cs_schedules_id`, `branch_id`, `user_id`, `staff_id`, `schedules_titmesheet_id`, `num_invoice_payment`, `available_total_pay_cost`, `available_total_pay_interest`, `available_total_payment`, `available_total_due_payment`, `available_total_pay_cost_owe`, `other_pay_reason`, `other_payment`, `date_payment`, `available_date_payment`, `lavel_payment`, `date_late_pay`, `is_penalty_pay`, `panalty`, `available_remain`, `status`, `status_for_pay`, `active`, `note`, `deleted_at`, `deleted`, `created_at`, `updated_at`) VALUES
(1, 11, 2, 50, 50, 37, '345', 46.81, 31.45, 78.26, 78.260, 234.03, NULL, 0.00, '2018-07-31', '2019-02-09', 1, 193, 0, 0.000, '0', 1, 2, 1, 'hello just set wold', NULL, 1, '2019-10-02 03:02:36', '2019-10-02 03:02:36'),
(2, 11, 2, 50, 50, 38, '4ft', 18.55, 31.45, 50.00, 78.260, 187.22, NULL, 0.00, '2018-08-28', '2018-08-28', 1, 0, 0, 0.000, '0', 2, 2, 1, 'last gone', NULL, 1, '2019-02-18 05:02:17', '2019-02-18 05:02:17');

-- --------------------------------------------------------

--
-- Table structure for table `cs-schedules-timesheet`
--

DROP TABLE IF EXISTS `cs-schedules-timesheet`;
CREATE TABLE IF NOT EXISTS `cs-schedules-timesheet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cs_schedules_id` int(11) DEFAULT '0',
  `total_pay_cost` double(30,2) DEFAULT '0.00',
  `total_pay_interest` double(30,2) DEFAULT '0.00',
  `total_payment` double(30,2) DEFAULT '0.00',
  `total_pay_cost_owe` double(30,2) DEFAULT '0.00',
  `date_payment` date DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `status_for_pay` int(11) DEFAULT '0',
  `active` int(11) DEFAULT '0',
  `is_finish` int(11) DEFAULT '0',
  `deleted` int(11) DEFAULT '1',
  `description` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=175 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cs-schedules-timesheet`
--

INSERT INTO `cs-schedules-timesheet` (`id`, `cs_schedules_id`, `total_pay_cost`, `total_pay_interest`, `total_payment`, `total_pay_cost_owe`, `date_payment`, `status`, `status_for_pay`, `active`, `is_finish`, `deleted`, `description`, `created_at`, `updated_at`) VALUES
(37, 11, 46.81, 31.45, 78.26, 234.03, '2018-07-31', 1, 2, 1, 0, 1, NULL, NULL, '2019-02-10 15:06:36'),
(38, 11, 46.81, 31.45, 78.26, 187.22, '2018-08-28', 2, 2, 1, 0, 1, NULL, NULL, '2019-02-18 17:20:17'),
(39, 11, 46.81, 31.45, 78.26, 140.41, '2018-09-28', 0, 0, 1, 0, 1, NULL, NULL, NULL),
(40, 11, 46.81, 31.45, 78.26, 93.60, '2018-10-29', 0, 0, 1, 0, 1, NULL, NULL, NULL),
(41, 11, 46.81, 31.45, 78.26, 46.79, '2018-11-28', 0, 0, 1, 0, 1, NULL, NULL, NULL),
(42, 11, 46.79, 31.45, 78.24, 0.00, '2018-12-28', 0, 0, 1, 0, 1, NULL, NULL, NULL),
(139, 13, 134.17, 90.16, 224.33, 670.83, '2018-07-24', 0, 0, 1, 0, 1, NULL, NULL, NULL),
(140, 13, 134.17, 90.16, 224.33, 536.66, '2018-08-24', 0, 0, 1, 0, 1, NULL, NULL, NULL),
(141, 13, 134.17, 90.16, 224.33, 402.49, '2018-09-25', 0, 0, 1, 0, 1, NULL, NULL, NULL),
(142, 13, 134.17, 90.16, 224.33, 268.32, '2018-10-24', 0, 0, 1, 0, 1, NULL, NULL, NULL),
(143, 13, 134.17, 90.16, 224.33, 134.15, '2018-11-24', 0, 0, 1, 0, 1, NULL, NULL, NULL),
(144, 13, 134.15, 90.16, 224.31, 0.00, '2018-12-24', 0, 0, 1, 0, 1, NULL, NULL, NULL),
(169, 8, 70.37, 47.29, 117.66, 351.87, '2018-07-06', 0, 0, 1, 0, 1, NULL, NULL, NULL),
(170, 8, 70.37, 47.29, 117.66, 281.50, '2018-08-06', 0, 0, 1, 0, 1, NULL, NULL, NULL),
(171, 8, 70.37, 47.29, 117.66, 211.13, '2018-09-06', 0, 0, 1, 0, 1, NULL, NULL, NULL),
(172, 8, 70.37, 47.29, 117.66, 140.76, '2018-10-06', 0, 0, 1, 0, 1, NULL, NULL, NULL),
(173, 8, 70.37, 47.29, 117.66, 70.39, '2018-11-06', 0, 0, 1, 0, 1, NULL, NULL, NULL),
(174, 8, 70.37, 47.29, 117.66, 0.02, '2018-12-06', 0, 0, 1, 0, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cs_brand`
--

DROP TABLE IF EXISTS `cs_brand`;
CREATE TABLE IF NOT EXISTS `cs_brand` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `code` varchar(30) DEFAULT NULL,
  `description` text CHARACTER SET utf8,
  `deleted` int(11) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cs_brand`
--

INSERT INTO `cs_brand` (`id`, `name`, `code`, `description`, `deleted`, `created_at`, `updated_at`) VALUES
(4, 'HP', 'HP', 'professional hey guy are you ok', 0, NULL, '2018-05-01 04:38:07'),
(5, 'Apple', 'APL', 'NPRO dadse', 0, NULL, '2018-05-01 04:44:31'),
(6, 'SAMSUNG', 'SS', 'Live for life', 0, NULL, NULL),
(7, 'LG', 'LG', 'Good Life', 0, NULL, NULL),
(8, 'Vivo', 'VV', 'Smart Left', 0, NULL, NULL),
(9, 'Huwie', 'HW', 'good', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cs_categorys`
--

DROP TABLE IF EXISTS `cs_categorys`;
CREATE TABLE IF NOT EXISTS `cs_categorys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `description` text CHARACTER SET latin1,
  `status` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cs_categorys`
--

INSERT INTO `cs_categorys` (`id`, `name`, `description`, `status`, `user_id`, `deleted`, `created_at`, `updated_at`) VALUES
(1, 'Coffe', 'With ICE', 1, 50, 0, '2018-05-01 06:05:42', NULL),
(2, 'Drink', 'with ICE', 1, 50, 0, '2018-05-01 06:05:10', NULL),
(3, 'Dinner', 'Food', 1, 50, 0, '2018-05-01 06:05:03', NULL),
(4, 'Lunch', 'Food for Lunch Time', 1, 50, 0, '2018-05-01 06:05:43', NULL),
(5, 'Breakfirst', 'food for break first', 1, 50, 0, '2018-05-01 06:05:28', NULL),
(6, 'Supper', 'for midnight', 1, 50, 0, '2018-05-01 06:05:56', NULL),
(7, 'snack', 'cake', 1, 50, 0, '2018-05-01 06:05:18', NULL),
(8, 'Phone', 'Phone', 1, 50, 0, '2018-05-18 09:05:35', '2018-05-18 09:23:35'),
(9, 'Headphones', 'Headphones', 1, 50, 0, '2018-07-15 06:07:02', NULL),
(10, 'Computers', 'Computers', 1, 50, 0, '2018-07-15 06:07:32', NULL),
(11, 'TV', 'TV', 1, 50, 0, '2018-07-15 06:07:48', NULL),
(12, 'KITCHEN  APPLIANCES', 'KITCHEN  APPLIANCES', 1, 50, 0, '2018-07-15 06:07:50', NULL),
(13, 'WASHERS AND DRYERS', 'WASHERS AND DRYERS', 1, 50, 0, '2018-07-15 06:07:24', '2018-07-15 06:20:24'),
(14, 'AIR CONDITIONING AND HEATING', 'AIR CONDITIONING AND HEATING', 1, 50, 0, '2018-07-15 06:07:59', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cs_clients`
--

DROP TABLE IF EXISTS `cs_clients`;
CREATE TABLE IF NOT EXISTS `cs_clients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_code` varchar(60) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_type_id` int(11) NOT NULL DEFAULT '0',
  `kh_name_first` varchar(60) DEFAULT NULL,
  `kh_name_last` varchar(60) DEFAULT NULL,
  `en_name_first` varchar(60) DEFAULT NULL,
  `en_name_last` varchar(60) DEFAULT NULL,
  `kh_username` varchar(60) DEFAULT NULL,
  `en_username` varchar(60) DEFAULT NULL,
  `gender` varchar(60) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `nationality` varchar(60) DEFAULT NULL,
  `identify_num` varchar(60) DEFAULT NULL,
  `identify_type` varchar(60) DEFAULT NULL,
  `identify_by` varchar(60) DEFAULT NULL,
  `account_number` varchar(255) DEFAULT NULL,
  `home_num` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `group_num` varchar(60) DEFAULT NULL,
  `street_num` varchar(60) DEFAULT NULL,
  `vilige` varchar(60) DEFAULT NULL,
  `commune` varchar(60) DEFAULT NULL,
  `district` varchar(60) DEFAULT NULL,
  `province` varchar(60) DEFAULT NULL,
  `phone` varchar(60) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `job` varchar(60) DEFAULT NULL,
  `place_job` varchar(60) DEFAULT NULL,
  `upload_relate_document` varchar(60) DEFAULT NULL,
  `staff_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `client_status` int(11) DEFAULT NULL,
  `description` text,
  `latitude` varchar(255) DEFAULT NULL,
  `longitude` varchar(255) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  `larvel_client` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cs_clients`
--

INSERT INTO `cs_clients` (`id`, `client_code`, `branch_id`, `user_id`, `client_type_id`, `kh_name_first`, `kh_name_last`, `en_name_first`, `en_name_last`, `kh_username`, `en_username`, `gender`, `dob`, `nationality`, `identify_num`, `identify_type`, `identify_by`, `account_number`, `home_num`, `group_num`, `street_num`, `vilige`, `commune`, `district`, `province`, `phone`, `email`, `job`, `place_job`, `upload_relate_document`, `staff_id`, `status`, `client_status`, `description`, `latitude`, `longitude`, `deleted`, `larvel_client`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, NULL, 3, 50, 2, 'ឈឿន', 'សុជាតិ', 'Chhoeun', 'Socheat', 'ឈឿន សុជាតិ', 'Chhoeun Socheat', 'F', '1992-01-01', '1', '125896374', '1', '1', NULL, '45', '77', '88', 'ភូមិ ១', 'កំពង់ស្វាយ', '៧មករា', 'ខេត្តកំពង់ធំ', '016-707-044', NULL, 'Web Developer', 'Phnom Penh', 'kitchen_adventurer_caramel.jpg', NULL, NULL, 0, 'JUST FOR TESTING', NULL, NULL, 0, 0, '2018-04-27 08:04:58', '2018-04-27 08:25:58', NULL),
(2, NULL, 2, 50, 1, 'ការិ  Provin', 'រិវិកា', 'Kari Provin', 'Rivika', 'ការិ  Provin រិវិកា', 'Kari Provin Rivika', 'F', '1945-07-05', '1', '025975214', '1', '1', NULL, '258', '89', 'លំ', 'ភូមិ ១', 'កំពង់ស្វាយ', '៧មករា', 'ខេត្តកំពង់ធំ', '016-957-852', NULL, 'គ្រូបង្រៀន', 'ភ្នំពេញ', 'SP4_Typecover_Blue_AngleView_V2.jpg', NULL, NULL, 0, 'Just for test', NULL, NULL, 0, 0, '2018-04-27 08:04:07', '2018-04-27 08:23:07', NULL),
(8, NULL, 2, 50, 1, 'ការិ', 'រិវិកា6', 'Kari', 'Rivika', 'ការិ រិវិកា', 'Kari Rivika', 'F', '1945-07-05', '1', '025975214', '1', '1', NULL, '258', '47', 'លំ', 'តាប៉ោង', 'កំពង់ស្វាយ', 'កំពង់ស្វាយ', 'ខេត្តកំពង់ធំ', '016-957-852', NULL, 'គ្រូបង្រៀន', 'ភ្នំពេញ', 'SP4_Typecover_Blue_AngleView_V2.jpg', NULL, NULL, 0, 'Just for test', NULL, NULL, 0, 0, '2018-04-19 09:04:33', NULL, NULL),
(9, NULL, 3, 50, 2, 'រ័ត្ន', 'ប្រតិកា', 'Roth', 'Brotika', 'រ័ត្ន ប្រតិកា', 'Roth Brotika', 'F', '1992-07-11', '1', '598', '4', '1', NULL, '96', '23', '85', 'ភូមិ ១', 'កំពង់ស្វាយ', '៧មករា', 'ខេត្តកំពង់ធំ', '023-647-000', NULL, 'Web Developer', 'Phnom Penh', 'blue-flower.jpg', NULL, NULL, 0, 'This is version for edit', '2596', '1475', 0, 0, '2018-04-27 08:04:48', '2018-04-27 08:17:48', NULL),
(10, NULL, 3, 50, 2, 'រិទ', 'និនួន', 'Rit', 'Ninoun', 'រិទ និនួន', 'Rit Ninoun', 'F', '1992-06-11', '2', '0136699', 'protocol', '2', NULL, '14', '8', '2002', 'ភូមិ ១', 'អូរឫស្សីទី ៣', '៧មករា', 'រាជធានីភ្នំពេញ', '026-974-244', NULL, 'Web Developer', 'Phnom Penh', '8.jpg', NULL, NULL, 0, 'just test for you  FGDSGDFG', '7', '8', 0, 0, '2018-04-26 02:04:04', '2018-04-26 14:35:04', NULL),
(11, NULL, 2, 50, 1, 'ឈឿន', 'សុផល', 'Chhoeun', 'Sophal', 'ឈឿន សុផល', 'Chhoeun Sophal', 'F', '1990-10-10', '1', '097367473', '1', '1', NULL, '156', NULL, '2002', 'ភូមិ ១', 'ទឹកថ្លា', 'សែនសុខ', 'រាជធានីភ្នំពេញ', '098-738-382', NULL, 'Teacher', 'Phnom Penh', 'hh2.png', NULL, NULL, 0, 'hello world', NULL, NULL, 0, 0, '2019-02-17 06:02:29', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cs_clients_type`
--

DROP TABLE IF EXISTS `cs_clients_type`;
CREATE TABLE IF NOT EXISTS `cs_clients_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `dispay_name` varchar(60) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cs_clients_type`
--

INSERT INTO `cs_clients_type` (`id`, `name`, `dispay_name`) VALUES
(1, 'pay_by_credit', 'Pay by Credit'),
(2, 'direct_payment', 'Direct Payment');

-- --------------------------------------------------------

--
-- Table structure for table `cs_client_andsub_clients`
--

DROP TABLE IF EXISTS `cs_client_andsub_clients`;
CREATE TABLE IF NOT EXISTS `cs_client_andsub_clients` (
  `client_id` int(11) DEFAULT '0',
  `sub_client_id` int(11) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cs_currency`
--

DROP TABLE IF EXISTS `cs_currency`;
CREATE TABLE IF NOT EXISTS `cs_currency` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `value_option` varchar(255) DEFAULT NULL,
  `value_key` varchar(11) DEFAULT NULL,
  `currency_status` int(11) NOT NULL,
  `note` text,
  `deleted` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cs_currency`
--

INSERT INTO `cs_currency` (`id`, `name`, `value_option`, `value_key`, `currency_status`, `note`, `deleted`, `created_at`, `updated_at`) VALUES
(1, 'DOLAR ', 'US', '$', 1, NULL, 1, NULL, NULL),
(2, 'KHMER', 'KHM', '៛', 1, 'good', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cs_exchange_rate`
--

DROP TABLE IF EXISTS `cs_exchange_rate`;
CREATE TABLE IF NOT EXISTS `cs_exchange_rate` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `real` int(11) NOT NULL,
  `us` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cs_history_log`
--

DROP TABLE IF EXISTS `cs_history_log`;
CREATE TABLE IF NOT EXISTS `cs_history_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip_log` varchar(60) DEFAULT NULL,
  `active` text,
  `user_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL COMMENT '1 => view , 2 => create , 3 => update , 4 => delete , 5 => search',
  `create_date` datetime DEFAULT NULL,
  `what_id` int(11) DEFAULT NULL,
  `method` varchar(60) DEFAULT NULL,
  `date_login` datetime DEFAULT NULL,
  `date_logout` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cs_history_log`
--

INSERT INTO `cs_history_log` (`id`, `ip_log`, `active`, `user_id`, `status`, `create_date`, `what_id`, `method`, `date_login`, `date_logout`) VALUES
(1, '::1', 'លុបការកំណត់ថ្ងៃឈប់សំរាក់', 50, 4, '2018-06-06 08:06:32', 3, 'public holiday', NULL, NULL),
(2, '::1', 'លុបការកំណត់ថ្ងៃឈប់សំរាក់', 50, 4, '2018-07-24 05:07:10', 2, 'public holiday', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cs_image_data`
--

DROP TABLE IF EXISTS `cs_image_data`;
CREATE TABLE IF NOT EXISTS `cs_image_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image_value` varchar(255) DEFAULT NULL,
  `meta_value` varchar(255) DEFAULT NULL,
  `dis` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cs_image_data`
--

INSERT INTO `cs_image_data` (`id`, `image_value`, `meta_value`, `dis`) VALUES
(4, '4.jpg', 'cs_client_id', 2),
(5, '5.jpg', 'cs_client_id', 2),
(6, '101-1024x683.jpg', 'cs_client_id', 2),
(8, 'blue-flower.jpg', 'cs_client_id', 0),
(12, 'raindrops.jpg', 'cs_client_id', 0),
(26, '3.jpg', 'cs_client_id', 9),
(27, '2.jpg', 'cs_client_id', 9),
(28, '7.jpg', 'cs_client_id', 9),
(29, '3.jpg', 'cs_client_id', 9),
(30, '9.jpg', 'cs_client_id', 9),
(31, '7.jpg', 'cs_client_id', 9),
(32, '9.jpg', 'cs_client_id', 9),
(33, '11.jpg', 'cs_client_id', 10),
(34, 'b1.jpg', 'cs_client_id', 10),
(35, 'b2.jpg', 'cs_client_id', 10),
(36, 'kitchen_adventurer_caramel.jpg', 'cs_client_id', 1),
(37, 'kitchen_adventurer_cheesecake_brownie.jpg', 'cs_client_id', 1),
(38, 'kitchen_adventurer_donut.jpg', 'cs_client_id', 1),
(39, 'kitchen_adventurer_lemon.jpg', 'cs_client_id', 1),
(40, 'hh8.png', 'cs_client_id', 11),
(41, 'hh9.png', 'cs_client_id', 11);

-- --------------------------------------------------------

--
-- Table structure for table `cs_items`
--

DROP TABLE IF EXISTS `cs_items`;
CREATE TABLE IF NOT EXISTS `cs_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) DEFAULT NULL,
  `add_number_id` int(11) DEFAULT NULL,
  `item_bacode` varchar(60) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `brand_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT '0',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `qty` double(30,2) DEFAULT '0.00',
  `tax_include` double(30,2) DEFAULT '0.00',
  `commission_fixed` double(30,2) DEFAULT '0.00',
  `override_default_tax` int(11) DEFAULT '0',
  `override_default_commission` int(11) DEFAULT '0',
  `commission_give` double(30,2) DEFAULT '0.00',
  `cost_price` double(30,2) DEFAULT '0.00',
  `sell_price` double(30,2) DEFAULT '0.00',
  `promo_price` double(30,2) DEFAULT '0.00',
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `discount` double(30,2) DEFAULT '0.00',
  `pro_image` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `size` varchar(60) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `is_service` int(11) DEFAULT NULL,
  `quality` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `recode_level` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cs_items`
--

INSERT INTO `cs_items` (`id`, `item_id`, `add_number_id`, `item_bacode`, `category_id`, `brand_id`, `branch_id`, `user_id`, `supplier_id`, `name`, `qty`, `tax_include`, `commission_fixed`, `override_default_tax`, `override_default_commission`, `commission_give`, `cost_price`, `sell_price`, `promo_price`, `start_date`, `end_date`, `discount`, `pro_image`, `size`, `description`, `is_service`, `quality`, `status`, `recode_level`, `deleted`, `created_at`, `updated_at`) VALUES
(3, NULL, 3, '0000000003', 8, 6, 2, 50, 1, 'Samsung Galaxy S', 0.00, NULL, 3.00, 1, 1, 0.00, 300.00, 400.00, 350.00, '2018-05-18', '2018-06-09', NULL, 'i5.jpg', NULL, 'Samsung Galaxy S', NULL, NULL, 1, NULL, 0, '2018-05-18 10:05:21', '2018-05-18 10:34:21'),
(4, NULL, 4, '0000000004', 8, 7, 2, 50, 3, 'LG G3 D885', 0.00, NULL, 0.00, 1, 1, 0.10, 117.00, 200.00, 150.00, '2018-05-18', '2018-06-09', NULL, 'i2.jpg', NULL, 'LG G3 D885', NULL, NULL, 1, NULL, 0, '2018-05-18 10:05:34', '2018-05-18 10:37:34'),
(5, NULL, 5, '0000000005', 6, 5, 2, 50, NULL, 'Stretchy Push-up Jeans', 0.00, NULL, 0.00, NULL, NULL, 0.00, 50.00, 70.00, NULL, NULL, NULL, NULL, '0004888401_1_1_3.jpg', NULL, 'Stretchy Push-up Jeans\r\nScore a wardrobe win no matter the dress code with our Flinto Collection own-label collection. From polished prom to the after party, our London-based design team scour the globe to nail your new-season fashion goals with need-right-now dresses, outerwear, shoes and denim in the coolest.\r\nNeque porro\r\nquisquam est qui dolorem ipsum quia.\r\nI am text block. Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo. Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit…', NULL, NULL, 1, NULL, 0, '2018-07-15 05:07:50', '2018-07-15 05:59:50'),
(6, NULL, 6, '0000000006', 9, 4, 2, 50, NULL, 'Ultra Wireless S50 Headphones S50 with Bluetooth', 0.00, NULL, 0.00, NULL, NULL, 0.00, 1000.00, 1215.00, NULL, NULL, NULL, NULL, 's1-1.jpg', NULL, 'Perfectly Done\r\nPraesent ornare, ex a interdum consectetur, lectus diam sodales elit, vitae egestas est enim ornare nisl. Nullam in lectus nec sem semper viverra. In lobortis egestas massa. Nam nec massa nisi. Suspendisse potenti. Quisque suscipit vulputate dui quis volutpat. Ut id elit facilisis, feugiat est in, tempus lacus. Ut ultrices dictum metus, a ultricies ex vulputate ac. Ut id cursus tellus, non tempor quam. Morbi porta diam nisi, id finibus nunc tincidunt eu.\r\n\r\nWireless\r\nFusce vitae nibh mi. Integer posuere, libero et ullamcorper facilisis, enim eros tincidunt orci, eget vestibulum sapien nisi ut leo. Cras finibus vel est ut mollis. Donec luctus condimentum ante et euismod.\r\n\r\nFresh Design\r\nInteger bibendum aliquet ipsum, in ultrices enim sodales sed. Quisque ut urna vitae lacus laoreet malesuada eu at massa. Pellentesque nibh augue, pellentesque nec dictum vel, pretium a arcu. Duis eu urna suscipit, lobortis elit quis, ullamcorper massa.\r\n\r\nFabolous Sound\r\nCras rutrum, nibh a sodales accumsan, elit sapien ultrices sapien, eget semper lectus ex congue elit. Nullam dui elit, fermentum a varius at, iaculis non dolor. In hac habitasse platea dictumst.', NULL, NULL, 1, NULL, 0, '2018-07-15 06:07:16', '2018-07-15 06:10:16'),
(7, NULL, 7, '0000000007', 8, 6, 2, 50, NULL, 'Samsung Galaxy S4 32GB White', 0.00, NULL, 0.00, NULL, NULL, 0.00, 700.00, 800.00, NULL, NULL, NULL, NULL, '3.jpg', NULL, 'Suspendisse posuere arcu diam, id accumsan eros pharetra ac. Nulla enim risus, facilisis bibendum gravida eget, lacinia id purus. Suspendisse posuere arcu diam, id accumsan eros pharetra ac. Nulla enim risus, facilisis bibendum gravida eget, lacinia id purus. Susp endisse posuere arcu diam, id accumsan eros pharetra ac.', NULL, NULL, 1, NULL, 0, '2018-07-15 06:07:49', '2018-07-15 06:12:49'),
(8, NULL, 8, '0000000008', 11, 7, 2, 50, NULL, 'LG Electronics 32LN520B', 0.00, NULL, 0.00, NULL, NULL, 0.00, 80.00, 150.00, NULL, NULL, NULL, NULL, 'LG-Electronics-32LN520B-32-Inch-720p-60Hz-LED-TV_03-90x90.png', NULL, 'Comfort is a very important thing nowadays because it is a condition of satisfaction and calmness. It is clear that our way of life must be as comfortable as possible. Home electronics satisfy our wishes and make our life more pleasant. We must admit that our way of life depends on quality of different goods of popular brands. Many of our clients were surprised by the incredible assortment of products in our store. You know, we have many devoted customers all over the world, and this fact proves that we sell only quality commodities. Recipe of our success is a fair price and premium quality. We understand that it is very complicated to amaze present clients, they are so whimsical, but our products are very flexible and reliable.\r\nHere you can find something more than just home electronics; you can find real comfort and satisfaction here! Our goods are the combination of perfect design and an ideal functionality. We have a tremendous variety of different models. Nowadays clients’ claims become so scrupulous that sometimes it is very hard to satisfy them. But we provide only real bestsellers and our products have a great number of options that can really help you. You’ll be amazed with its simplicity and durability.\r\nOur manufacturers and vendors provide only new technologies and it is very important because nowadays we see a furious development of electronics industry. We also provide different economical, social and even technological researches. The main goal of their analysis is to find out the changes of clients’ demands and other useful data. We are trying to introduce positive results of our explorations.\r\nIf you want to know more information about our goods, terms, guarantees and other features, you can address our superb 24/7 support system. Also you can save some money at our store because we always provide different promos and you can get good discount and other benefits.\r\nRemember that only in our store you can buy fashionable, solid and very functional home electronics.', NULL, NULL, 1, NULL, 0, '2018-07-15 06:07:18', '2018-07-15 06:23:18'),
(9, NULL, 9, '0000000009', 14, 6, 2, 50, NULL, 'Frigidaire Energy Star', 0.00, NULL, 0.00, NULL, NULL, 0.00, 700.00, 1000.00, NULL, NULL, NULL, NULL, 'Frigidaire-Energy-Star-6.000-BTU-115V-Window-Mounted-Low-Profile-Air-Conditioner-w-Full-Function-Remote-Control-2-90x90.png', NULL, 'Comfort is a very important thing nowadays because it is a condition of satisfaction and calmness. It is clear that our way of life must be as comfortable as possible. Home electronics satisfy our wishes and make our life more pleasant. We must admit that our way of life depends on quality of different goods of popular brands. Many of our clients were surprised by the incredible assortment of products in our store. You know, we have many devoted customers all over the world, and this fact proves that we sell only quality commodities. Recipe of our success is a fair price and premium quality. We understand that it is very complicated to amaze present clients, they are so whimsical, but our products are very flexible and reliable.\r\nHere you can find something more than just home electronics; you can find real comfort and satisfaction here! Our goods are the combination of perfect design and an ideal functionality. We have a tremendous variety of different models. Nowadays clients’ claims become so scrupulous that sometimes it is very hard to satisfy them. But we provide only real bestsellers and our products have a great number of options that can really help you. You’ll be amazed with its simplicity and durability.\r\nOur manufacturers and vendors provide only new technologies and it is very important because nowadays we see a furious development of electronics industry. We also provide different economical, social and even technological researches. The main goal of their analysis is to find out the changes of clients’ demands and other useful data. We are trying to introduce positive results of our explorations.\r\nIf you want to know more information about our goods, terms, guarantees and other features, you can address our superb 24/7 support system. Also you can save some money at our store because we always provide different promos and you can get good discount and other benefits.\r\nRemember that only in our store you can buy fashionable, solid and very functional home electronics.', NULL, NULL, 1, NULL, 0, '2018-07-15 06:07:41', '2018-07-15 06:24:41'),
(10, NULL, 10, '0000000010', 8, 5, 2, 50, 24, 'iBall Slide Brace-X1 Tablet (Classic Silver)', 0.00, 1.00, 0.00, NULL, NULL, 0.00, 200.00, 265.84, 228.92, '2018-08-15', '2018-09-01', NULL, '1.jpg', NULL, '2 GB RAM | 16 GB ROM\r\n13 MP Primary Camera | 5 MP Front\r\nAndroid 4.4 (KitKat)\r\nBattery: 7800 mAh, Lithium - Polymer\r\nVoice Call (Dual Sim, GSM + WCDMA)', NULL, NULL, 1, NULL, 0, '2018-08-14 05:08:24', '2018-08-14 05:38:24'),
(11, NULL, 11, '0000000011', 4, NULL, 2, 50, NULL, 'ddd', 0.00, NULL, 0.00, NULL, NULL, 0.00, 200.00, 230.00, NULL, NULL, NULL, NULL, 'img2.jpg', NULL, NULL, NULL, NULL, 1, NULL, 0, '2018-10-18 06:10:02', '2018-10-18 06:22:02');

-- --------------------------------------------------------

--
-- Table structure for table `cs_items_tax`
--

DROP TABLE IF EXISTS `cs_items_tax`;
CREATE TABLE IF NOT EXISTS `cs_items_tax` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `name` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `percent` double(10,2) DEFAULT '0.00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cs_items_tax`
--

INSERT INTO `cs_items_tax` (`id`, `item_id`, `name`, `percent`) VALUES
(20, 3, 'Tax 5', 0.15),
(19, 3, 'Tax 4', 0.14),
(18, 3, 'Tax 3', 0.13),
(17, 3, 'Tax 2', 0.12),
(16, 3, 'Tax 1', 0.11),
(21, 4, 'Tax 1', 0.10),
(22, 4, 'Tax 2', 0.20);

-- --------------------------------------------------------

--
-- Table structure for table `cs_it_images`
--

DROP TABLE IF EXISTS `cs_it_images`;
CREATE TABLE IF NOT EXISTS `cs_it_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `image` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cs_payments_transactions`
--

DROP TABLE IF EXISTS `cs_payments_transactions`;
CREATE TABLE IF NOT EXISTS `cs_payments_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sale_id` int(11) DEFAULT '0',
  `payment_id` int(11) DEFAULT '0',
  `account_mount` float DEFAULT '0',
  `currecy_type` varchar(60) DEFAULT NULL,
  `payment_pay` varchar(60) DEFAULT NULL,
  `schedule_timesheet_pay_id` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cs_payments_transactions`
--

INSERT INTO `cs_payments_transactions` (`id`, `sale_id`, `payment_id`, `account_mount`, `currecy_type`, `payment_pay`, `schedule_timesheet_pay_id`) VALUES
(4, 12, 0, 2165, '1', 'cash', 0),
(13, 15, 0, -800, '2', 'cash', 0),
(6, 14, 0, 472, '1', 'cash', 0),
(7, 14, 0, 2500, '2', 'cash', 0),
(12, 15, 0, -603, '1', 'cash', 0),
(11, 9, 0, 4526, '1', 'cash', 0),
(14, 16, 0, -472, '1', 'cash', 0),
(15, 16, 0, -2400, '2', 'cash', 0),
(17, 21, 0, -4073.4, '1', 'cash', 0),
(18, 22, 0, -1035, '1', 'cash', 0),
(20, 11, 0, 78.26, '1', 'cash', 1),
(21, 11, 0, 50, '1', 'cash', 2),
(22, 11, 0, 50, '1', 'cash', 3),
(23, 11, 0, 50, '1', 'cash', 2);

-- --------------------------------------------------------

--
-- Table structure for table `cs_payment_type`
--

DROP TABLE IF EXISTS `cs_payment_type`;
CREATE TABLE IF NOT EXISTS `cs_payment_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  `display_name` varchar(60) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`id`),
  KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cs_penaty`
--

DROP TABLE IF EXISTS `cs_penaty`;
CREATE TABLE IF NOT EXISTS `cs_penaty` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `late_form` int(11) DEFAULT NULL,
  `at_late` int(11) DEFAULT NULL,
  `color` varchar(50) DEFAULT NULL,
  `note` text,
  `user_id` int(11) DEFAULT NULL,
  `percent_of_payment` float DEFAULT '0',
  `status` int(11) DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cs_penaty`
--

INSERT INTO `cs_penaty` (`id`, `name`, `late_form`, `at_late`, `color`, `note`, `user_id`, `percent_of_payment`, `status`, `created_at`, `updated_at`) VALUES
(1, 'P001', 1, 10, '#f27f7f', 'Late from 1 one until 10 day', 50, 0.5, 1, '2019-02-06 04:02:47', '2019-02-06 04:02:55'),
(3, 'P002', 11, 50, '#d63f3f', 'Bad Load', 50, 1, 1, '2019-02-08 02:02:13', NULL),
(4, 'P003', 51, 150, '#e81a1a', 'Loan Very Bad in problem load', 50, 2, 1, '2019-02-08 02:02:07', NULL),
(5, 'P004', 151, 365, '#960909', 'loan Has problem', 50, 5, 1, '2019-02-08 02:02:52', NULL),
(6, 'P005', 366, 700, '#ff1010', 'loan lost', 50, 10, 1, '2019-02-08 02:02:47', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cs_purchase_order`
--

DROP TABLE IF EXISTS `cs_purchase_order`;
CREATE TABLE IF NOT EXISTS `cs_purchase_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `supplier_id` int(11) DEFAULT '0',
  `branch_id` int(11) DEFAULT '0',
  `user_id` int(11) DEFAULT '0',
  `stuff_order_id` int(11) DEFAULT '0',
  `stuff_receiving_id` int(11) DEFAULT '0',
  `invoice_num` varchar(60) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `qty_order` double(16,2) DEFAULT '0.00',
  `qty_receiving` double DEFAULT '0',
  `active` int(11) DEFAULT '0',
  `order_date` date DEFAULT NULL,
  `order_modify_date` date DEFAULT NULL,
  `receiving_date` date DEFAULT NULL,
  `to_from_to` varchar(255) DEFAULT '0',
  `deposit_price` double(16,2) DEFAULT '0.00',
  `remain_price` double(16,2) DEFAULT '0.00',
  `total_price` double(15,2) DEFAULT '0.00',
  `sup_total_price` double(15,2) DEFAULT '0.00',
  `dute_total_price` double(15,2) DEFAULT '0.00',
  `currency` int(11) DEFAULT '0',
  `discount_percent` double(15,2) DEFAULT '0.00',
  `status_payment` varchar(60) DEFAULT NULL,
  `status_for_purchase` varchar(255) DEFAULT NULL,
  `command_purchase` text,
  `receiving_command` text,
  `is_receiving` int(11) DEFAULT '0',
  `supspend` int(11) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cs_purchase_order`
--

INSERT INTO `cs_purchase_order` (`id`, `supplier_id`, `branch_id`, `user_id`, `stuff_order_id`, `stuff_receiving_id`, `invoice_num`, `status`, `qty_order`, `qty_receiving`, `active`, `order_date`, `order_modify_date`, `receiving_date`, `to_from_to`, `deposit_price`, `remain_price`, `total_price`, `sup_total_price`, `dute_total_price`, `currency`, `discount_percent`, `status_payment`, `status_for_purchase`, `command_purchase`, `receiving_command`, `is_receiving`, `supspend`, `created_at`, `updated_at`, `deleted`) VALUES
(20, 3, 2, 50, 0, 50, 'PO2018090620', 0, 0.00, 21, 0, NULL, '2018-09-06', '2018-09-06', '0', 0.00, 2056.40, 2550.00, 2120.00, 2056.40, 1, 3.00, 'receiving', 'done', NULL, 'hello world', 1, 1, NULL, '2018-09-06 05:10:18', 1),
(21, 3, 2, 53, 53, 0, 'PO2018122821', 1, 2.00, 0, 1, '2018-12-19', '2018-12-19', NULL, '0', 0.00, 1700.00, 1700.00, 1700.00, 1700.00, 1, 0.00, 'purchas_order', 'pending', 'hello word', NULL, 0, 1, '2018-12-28 02:12:09', '2018-12-28 14:58:09', 1),
(22, 24, 2, 53, 0, 53, 'PO2018122822', 0, 0.00, 30, 0, NULL, '2018-12-28', '2018-12-28', '0', 0.00, 5800.00, 5800.00, 5800.00, 5800.00, 1, 0.00, 'receiving', 'done', NULL, 'just receiving', 1, 1, NULL, '2018-12-28 15:26:24', 1);

-- --------------------------------------------------------

--
-- Table structure for table `cs_purchase_order_item`
--

DROP TABLE IF EXISTS `cs_purchase_order_item`;
CREATE TABLE IF NOT EXISTS `cs_purchase_order_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) DEFAULT '0',
  `cs_purchase_order_id` int(11) DEFAULT '0',
  `qty_order` double(16,2) DEFAULT '0.00',
  `status` int(11) DEFAULT '0',
  `cost_price` double(16,2) DEFAULT '0.00',
  `qty_receiving` double(16,2) DEFAULT '0.00',
  `qty_free` double DEFAULT '0',
  `is_purchas` int(11) DEFAULT '0',
  `sub_total` double(16,2) DEFAULT '0.00',
  `total` double(16,2) DEFAULT '0.00',
  `tax` double(16,2) DEFAULT '0.00',
  `discount` double(16,2) DEFAULT '0.00',
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cs_purchase_order_item`
--

INSERT INTO `cs_purchase_order_item` (`id`, `item_id`, `cs_purchase_order_id`, `qty_order`, `status`, `cost_price`, `qty_receiving`, `qty_free`, `is_purchas`, `sub_total`, `total`, `tax`, `discount`, `updated_at`) VALUES
(45, 5, 20, 0.00, 0, 50.00, 15.00, 5, 0, 500.00, 500.00, 0.00, 0.00, NULL),
(46, 3, 20, 0.00, 0, 300.00, 6.00, 0, 0, 1800.00, 1620.00, 0.00, 10.00, NULL),
(47, 6, 21, 1.00, 0, 1000.00, 0.00, 0, 0, 1000.00, 990.00, 0.00, 0.00, NULL),
(48, 7, 21, 1.00, 0, 700.00, 0.00, 0, 0, 700.00, 693.00, 0.00, 0.00, NULL),
(49, 3, 22, 0.00, 0, 300.00, 10.00, 0, 0, 3000.00, 3000.00, 0.00, 0.00, NULL),
(50, 11, 22, 0.00, 0, 200.00, 10.00, 0, 0, 2000.00, 2000.00, 0.00, 0.00, NULL),
(51, 8, 22, 0.00, 0, 80.00, 10.00, 0, 0, 800.00, 800.00, 0.00, 0.00, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cs_purchase_order_payment`
--

DROP TABLE IF EXISTS `cs_purchase_order_payment`;
CREATE TABLE IF NOT EXISTS `cs_purchase_order_payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cs_purchase_order_id` int(11) DEFAULT '0',
  `total_payment_price` double(16,2) DEFAULT '0.00',
  `status_payment` varchar(60) DEFAULT '0',
  `currency` int(11) DEFAULT '0',
  `payment_type` varchar(60) DEFAULT '0',
  `status` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cs_purchase_order_payment`
--

INSERT INTO `cs_purchase_order_payment` (`id`, `cs_purchase_order_id`, `total_payment_price`, `status_payment`, `currency`, `payment_type`, `status`) VALUES
(30, 22, 5800.00, 'receiving', 1, 'store_account', 'pay'),
(29, 21, 1700.00, 'purchas_order', 1, 'store_account', 'pay'),
(28, 20, 2056.40, 'receiving', 1, 'store_account', 'pay');

-- --------------------------------------------------------

--
-- Table structure for table `cs_request_form`
--

DROP TABLE IF EXISTS `cs_request_form`;
CREATE TABLE IF NOT EXISTS `cs_request_form` (
  `id` int(11) NOT NULL,
  `client_id` int(11) DEFAULT NULL,
  `sale_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `staff_id` int(11) DEFAULT NULL,
  `barcord` varchar(255) DEFAULT NULL,
  `place_for_pay` int(11) DEFAULT '0',
  `prices_total_num` double(30,3) DEFAULT NULL,
  `prices_totalword` varchar(255) DEFAULT NULL,
  `deposit_precent` float DEFAULT '0',
  `deposit_fixed` float DEFAULT '0',
  `deposit_fixed_word` varchar(255) DEFAULT NULL,
  `money_owne` float DEFAULT '0',
  `money_owne_word` varchar(255) DEFAULT NULL,
  `interest_of_owne_precent` float DEFAULT '0',
  `currency` int(11) DEFAULT NULL,
  `discounted` float DEFAULT '0',
  `discount_reason` varchar(255) DEFAULT NULL,
  `method` varchar(60) DEFAULT NULL,
  `duration_pay_money` int(11) DEFAULT NULL,
  `duration_pay_money_type` varchar(60) DEFAULT NULL,
  `date_for_payments` date DEFAULT NULL,
  `date_create_request` date DEFAULT NULL,
  `upload_relate_doc` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `total_qty` int(11) DEFAULT '0',
  `is_agree` int(11) DEFAULT NULL,
  `is_give` int(11) DEFAULT NULL,
  `is_break` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cs_request_form`
--

INSERT INTO `cs_request_form` (`id`, `client_id`, `sale_id`, `branch_id`, `user_id`, `staff_id`, `barcord`, `place_for_pay`, `prices_total_num`, `prices_totalword`, `deposit_precent`, `deposit_fixed`, `deposit_fixed_word`, `money_owne`, `money_owne_word`, `interest_of_owne_precent`, `currency`, `discounted`, `discount_reason`, `method`, `duration_pay_money`, `duration_pay_money_type`, `date_for_payments`, `date_create_request`, `upload_relate_doc`, `status`, `total_qty`, `is_agree`, `is_give`, `is_break`, `deleted`, `created_at`, `updated_at`, `deleted_at`) VALUES
(8, 1, 8, 2, 50, 50, NULL, 1, 603.200, 'six hundred zero three point twenty', 0.3, 180.96, 'one hundred eighty point ninety six', 422.24, 'four hundred twenty two point twenty four', 0.112, 1, 0, NULL, 'sale_by_credit', 6, 'month', '2018-07-06', '2018-07-30', NULL, 1, 2, 1, 0, 0, 1, '2018-07-06 08:07:35', '2019-02-11 15:27:41', NULL),
(9, 1, 9, 2, 50, 50, NULL, 1, 0.000, NULL, 0.3, 0, NULL, 0, NULL, 0.112, 1, 10, 'Spacail Price', 'sale_by_cash', 0, 'none', '1970-01-01', '1970-01-01', NULL, 1, 22, 0, 0, 0, 1, '2019-01-06 08:01:50', '2019-01-06 08:01:50', NULL),
(11, 8, 11, 2, 50, 50, NULL, 1, 401.200, 'four hundred zero one', 0.3, 120.36, 'one hundred  twenty', 280.84, 'two hundred eighty one', 0.112, 1, 0, NULL, 'sale_by_credit', 6, 'month', '2018-07-30', '2018-07-10', NULL, 1, 2, 1, 0, 0, 1, '2018-07-10 07:07:37', '2018-07-16 07:19:30', NULL),
(12, 9, 12, 2, 50, NULL, NULL, 1, 0.000, NULL, 0.3, 649.5, NULL, 1515.5, NULL, 0.112, 1, 0, NULL, 'sale_by_cash', 0, 'none', '1970-01-01', '1970-01-01', NULL, 1, 3, 0, 0, 0, 1, '2018-07-15 06:07:50', '2018-07-15 06:07:50', NULL),
(13, 10, 13, 2, 50, 50, NULL, 1, 1150.000, 'eleven hundred fifty us', 0.3, 345, 'three hundred forty five us', 805, 'eight hundred zero five', 0.112, 1, 0, NULL, 'sale_by_credit', 12, 'month', '2018-07-30', '2018-07-16', NULL, 1, 2, 1, 0, 0, 1, '2018-07-15 06:07:33', '2018-07-16 07:05:25', NULL),
(14, 2, 14, 2, 50, 50, NULL, 1, 0.000, NULL, 0.3, 0, NULL, 0, NULL, 0.112, 2, 0, NULL, 'sale_by_cash', 0, 'none', '1970-01-01', '1970-01-01', NULL, 1, 2, 0, 0, 0, 1, '2019-01-06 08:01:08', '2019-01-06 08:01:08', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cs_settings`
--

DROP TABLE IF EXISTS `cs_settings`;
CREATE TABLE IF NOT EXISTS `cs_settings` (
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cs_settings`
--

INSERT INTO `cs_settings` (`key`, `value`) VALUES
('1', '0');

-- --------------------------------------------------------

--
-- Table structure for table `cs_settings_tax`
--

DROP TABLE IF EXISTS `cs_settings_tax`;
CREATE TABLE IF NOT EXISTS `cs_settings_tax` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tax_name` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tax_precent` float DEFAULT '0',
  `cumulative` int(11) DEFAULT '0',
  `cs_settings_id` int(11) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cs_sub_clients`
--

DROP TABLE IF EXISTS `cs_sub_clients`;
CREATE TABLE IF NOT EXISTS `cs_sub_clients` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `mfi_client_id` int(10) UNSIGNED NOT NULL,
  `client_first_name_kh` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `client_second_name_kh` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `client_name_kh` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `client_first_name_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `client_second_name_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `client_name_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `client_gender` enum('F','M') COLLATE utf8_unicode_ci DEFAULT NULL,
  `client_dob` date DEFAULT NULL,
  `client_phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `client_nationality` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `b_status_live` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `client_idcard_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `client_type_idcard` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `client_create_date_idcard` date DEFAULT NULL,
  `client_aprovel_idcard_by` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `client_num_st_ho_gr` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `client_house_num` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `client_group_num` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `client_st_num` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `client_village` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `client_commune` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `client_district` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `client_province` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `client_job` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `client_address_job` text COLLATE utf8_unicode_ci NOT NULL,
  `client_time_job` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `client_job_profit` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `client_lavel` int(11) NOT NULL,
  `client_note` text COLLATE utf8_unicode_ci NOT NULL,
  `client_upload_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image_uploade_id_familly` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `client_lutidued` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `client_longitidute` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mfi_including_guarantees_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mfi_including_guarantees_id_card` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mfi_witnesses_one_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mfi_witnesses_two_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mfi_including_borrower_one_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mfi_including_borrower_two_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mfi_including_borrower_gender` enum('F','M') COLLATE utf8_unicode_ci DEFAULT NULL,
  `mfi_including_borrower_dob` date DEFAULT NULL,
  `mfi_including_borrower_national` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mfi_including_borrower_id_card` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mfi_including_borrower_type_id_card` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mfi_including_borrower_date_id_card` date DEFAULT NULL,
  `mfi_including_borrower_id_card_leave_by` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `relationship` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `client_status` int(11) NOT NULL,
  `mif_sub_client_status` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cs_suppliers`
--

DROP TABLE IF EXISTS `cs_suppliers`;
CREATE TABLE IF NOT EXISTS `cs_suppliers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(255) DEFAULT NULL,
  `name` varchar(60) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `description` text,
  `images` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `address1` text,
  `address2` text,
  `city` varchar(255) DEFAULT NULL,
  `state_or_province` varchar(255) DEFAULT NULL,
  `zip_code` varchar(255) DEFAULT NULL,
  `country` varchar(60) DEFAULT NULL,
  `account_number` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cs_suppliers`
--

INSERT INTO `cs_suppliers` (`id`, `company_name`, `name`, `email`, `phone`, `description`, `images`, `user_id`, `status`, `address1`, `address2`, `city`, `state_or_province`, `zip_code`, `country`, `account_number`, `created_at`, `updated_at`, `deleted`) VALUES
(1, 'TITB', 'socheat chhoeun', 'socheatit1992@gmail.com', '085218806', 'hello Cambodia', 'error_img.png', 50, 1, 'st.2002 , No .156  teok thla , sensok', '2002', 'Phnom Penh', 'Phnom Penh', '12102', 'Cambodia', '013265478', '2018-05-01 12:05:03', NULL, 0),
(2, 'TITB', 'Rot Tana', 'rottana@gmail.com', '01236974', 'hwlloe', '001.jpg', 50, 1, 'st.2002 , No .156  teok thla , sensok', '2002', 'Phnom Penh', 'Phnom Penh', '12102', 'Cambodia', '1455887774', '2018-05-01 12:05:02', NULL, 0),
(3, 'TITB', 'Rata vasa', 'ratavasa@gmail.com', '0125885336', 'monthpro', '004.jpg', 50, 1, 'st.2002 , No .156  teok thla , sensok', '2002', 'Phnom Penh', 'Phnom Penh', '12102', 'Cambodia', '0362584788', '2018-05-01 12:05:21', NULL, 0),
(4, 'LG Brono', 'VS LF', 'vslf@gmail.com', '0258476300', 'nonthteg ddd', '009.jpg', 50, 1, 'st.2002 , No .156  teok thla , sensok', '2002', 'Phnom Penh', 'Phnom Penh', '12102', 'Cambodia', '365898900', '2018-05-01 01:05:22', '2018-05-01 13:22:22', 0),
(9, 'Sellacios', 'Sellacios', 'noreply@sellacious.com', '+123 456 78910', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer adipiscing erat eget risus sollicitudin pellentesque et non erat. Maecenas nibh dolor, malesuada et bibendum a, sagittis accumsan ipsum. Pellentesque ultrices ultrices sapien, nec tincidunt nunc posuere ut. Lorem ipsum dolor sit amet, consectetur adipiscing elit.', '7518315800_1_1_4.jpg', 50, 1, '250 Bedford Park Blvd W, Bronx New York, NY, 10468', '250 Bedford Park Blvd W, Bronx New York, NY, 10468', 'New York', 'New York', '10468', 'American', NULL, '2018-08-14 02:08:03', NULL, 0),
(24, 'Protect', 'Nin Visa', 'sisninvisa@gmail.com', '098765432', 'daf', '0004888401_1_1_3.jpg', 50, 1, 'st.2002 , No .156  teok thla , sensok', '2002', 'Phnom Penh', 'Phnom Penh', '12102', 'Cambodia', NULL, '2018-08-14 05:08:05', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
CREATE TABLE IF NOT EXISTS `groups` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `groups_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'administractor', 'Administractor', 'Can Manage In this App ', NULL, '2018-01-02 15:17:58'),
(2, 'sub-administractor', 'Sub Administractor', 'Can Create and Edite and View', NULL, '2017-05-18 14:38:26'),
(7, 'Supper User', 'Supper User', 'all', NULL, '2017-07-13 16:03:43'),
(8, 'developer', 'Developer', 'Developer', NULL, '2018-04-24 03:44:39');

-- --------------------------------------------------------

--
-- Table structure for table `mfi_branch`
--

DROP TABLE IF EXISTS `mfi_branch`;
CREATE TABLE IF NOT EXISTS `mfi_branch` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `brand_name` varchar(60) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `brand_name_short` varchar(255) NOT NULL,
  `brand_phone` varchar(60) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `brand_email` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `brand_upload_image` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `brand_dis` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `brand_address` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `website` varchar(60) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mfi_branch`
--

INSERT INTO `mfi_branch` (`id`, `brand_name`, `brand_name_short`, `brand_phone`, `brand_email`, `brand_upload_image`, `brand_dis`, `brand_address`, `website`, `deleted`, `created_at`, `updated_at`) VALUES
(2, 'ភំ្នពេញ', 'PN', '010 35 77 22 / 031 482 7777', 'info@kasp.finance', '', 'ភ្នំពេញ', '#62R, St 62 Toul kok village, Sang khat Toul Sangke, Rousey Keo, PhnomPenh', '', 0, '2017-10-04 01:05:50', '0000-00-00 00:00:00'),
(3, 'អង្គតាសោម', 'TK', '010 36 77 22 / 031 592 777 7', 'info@kasp.finance', '', 'អង្គតាសោម', 'Angtasom Commune, Trangkork District, Takeo Privince', '', 0, '2017-12-15 01:38:42', '2016-11-03 03:31:39'),
(4, 'ស្គន់', 'KN', '010 57 77 22 / 088 219 777 7', 'info@kasp.finance', '', 'សាខា ស្គន់', 'Skun Village, So Tip Commune, Cheoung Prey District, Kampong Cham Province', '', 1, '2018-04-30 06:20:46', '2018-01-18 22:15:09');

-- --------------------------------------------------------

--
-- Table structure for table `mfi_group_permission`
--

DROP TABLE IF EXISTS `mfi_group_permission`;
CREATE TABLE IF NOT EXISTS `mfi_group_permission` (
  `group_id` int(10) DEFAULT NULL,
  `permission_id` int(10) UNSIGNED DEFAULT NULL,
  KEY `group_id` (`group_id`,`permission_id`),
  KEY `group_id_2` (`group_id`),
  KEY `permission_id` (`permission_id`),
  KEY `group_id_3` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mfi_group_permission`
--

INSERT INTO `mfi_group_permission` (`group_id`, `permission_id`) VALUES
(8, 25),
(8, 26),
(8, 27),
(8, 28),
(8, 29),
(8, 30),
(8, 31),
(8, 32),
(8, 33),
(8, 34),
(8, 49),
(8, 50),
(8, 51),
(8, 52),
(8, 53),
(8, 55),
(8, 56),
(8, 57),
(8, 58),
(8, 59),
(8, 64),
(8, 65),
(8, 66),
(8, 68),
(8, 69),
(8, 70),
(8, 71),
(8, 72),
(8, 73),
(8, 74),
(8, 75),
(8, 76),
(8, 77),
(8, 78);

-- --------------------------------------------------------

--
-- Table structure for table `mfi_interest_loan`
--

DROP TABLE IF EXISTS `mfi_interest_loan`;
CREATE TABLE IF NOT EXISTS `mfi_interest_loan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `module_interest_id` int(11) NOT NULL,
  `rate_name` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `size_money_from` double(20,2) DEFAULT NULL,
  `size_money_to` double(20,2) DEFAULT NULL,
  `day_rate_villige` float(20,2) DEFAULT NULL,
  `weekly_rate_villige` float(20,2) DEFAULT NULL,
  `two_weekly_rate_villige` float(20,2) DEFAULT NULL,
  `monthly_rate_villige` float(20,2) DEFAULT NULL,
  `day_rate_brand` float(20,2) DEFAULT NULL,
  `weekly_rate_brand` float(20,2) DEFAULT NULL,
  `two_weekly_brand` float(20,2) DEFAULT NULL,
  `monthly_rate_brand` float(20,2) DEFAULT NULL,
  `duration` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `repayment_disction` text COLLATE utf8_unicode_ci,
  `note` text COLLATE utf8_unicode_ci,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `discrition` text COLLATE utf8_unicode_ci,
  `status` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `id_2` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mfi_interest_loan`
--

INSERT INTO `mfi_interest_loan` (`id`, `user_id`, `module_interest_id`, `rate_name`, `size_money_from`, `size_money_to`, `day_rate_villige`, `weekly_rate_villige`, `two_weekly_rate_villige`, `monthly_rate_villige`, `day_rate_brand`, `weekly_rate_brand`, `two_weekly_brand`, `monthly_rate_brand`, `duration`, `repayment_disction`, `note`, `created_at`, `updated_at`, `discrition`, `status`, `deleted`) VALUES
(1, 50, 2, 'table_1', 200000.00, 700000.00, 0.90, 2.90, 5.80, 11.60, 0.80, 2.80, 5.60, 11.20, '3-6', 'សងទាំងដើមនិងការប្រាក់', 'Repayment All ', '2017-09-27 10:09:33', '2017-09-27 10:09:33', 'Repayment All ', 1, 0),
(2, 50, 2, 'table_1', 1000000.00, 1600000.00, 0.90, 3.00, 6.00, 12.00, 0.80, 2.90, 5.80, 11.60, '2-6', 'សងទាំងដើមនិងការប្រាក់', '', '2017-09-27 10:09:41', '2017-09-27 10:09:41', '', 1, 0),
(3, 50, 2, 'table_1', 1600000.00, 2000000.00, 0.00, 2.80, 5.60, 11.20, 0.00, 2.70, 5.40, 10.80, '2-6', 'សងទាំងដើមនិងការប្រាក់', '', '2017-09-27 10:09:47', '2017-09-27 10:09:47', '', 1, 0),
(4, 50, 3, 'table_2', 200000.00, 700000.00, 0.90, 2.90, 5.80, 11.60, 0.80, 2.80, 5.60, 11.20, '3 - 6', 'សងតែការប្រាក់ និងរំលស់ដើមឬសងផ្តាច់នៅចុងវគ្គ ឬសងទាំងដើមនិងការប្រាក់', 'hello world ', '2017-09-27 10:09:00', '2017-09-27 10:09:00', '', 1, 0),
(5, 50, 3, 'table_2', 1000000.00, 1600000.00, 0.90, 3.00, 6.00, 12.00, 0.80, 2.90, 5.80, 11.60, '2 - 6', 'សងតែការប្រាក់ និងរំលស់ដើមឬសងផ្តាច់នៅចុងវគ្គ ឬសងទាំងដើមនិងការប្រាក់', 'សងតែការប្រាក់ និងរំលស់ដើមឬសងផ្តាច់នៅចុងវគ្គ ឬសងទាំងដើមនិងការប្រាក់', '2017-09-27 10:09:09', '2017-09-27 10:09:09', '', 1, 0),
(6, 50, 3, 'table_2', 1600000.00, 2000000.00, 0.00, 2.80, 5.60, 11.20, 0.00, 2.70, 5.40, 10.80, '2 - 6', 'សងតែការប្រាក់ និងរំលស់ដើមឬសងផ្តាច់នៅចុងវគ្គ ឬសងទាំងដើមនិងការប្រាក់', 'សងតែការប្រាក់ និងរំលស់ដើមឬសងផ្តាច់នៅចុងវគ្គ ឬសងទាំងដើមនិងការប្រាក់', '2017-09-27 10:09:18', '2017-09-27 10:09:18', '', 1, 0),
(7, 50, 2, 'table_1', 2000000.00, 4000000.00, 0.00, 2.70, 5.40, 10.80, 0.00, 2.60, 5.20, 10.40, '2 - 6', 'សងតែការប្រាក់ និងរំលស់ដើមឬសងផ្តាច់នៅចុងវគ្គ ឬសងទាំងដើមនិងការប្រាក់', 'សងតែការប្រាក់ និងរំលស់ដើមឬសងផ្តាច់នៅចុងវគ្គ ឬសងទាំងដើមនិងការប្រាក់', '2017-09-27 10:09:33', '2017-09-27 10:09:33', '', 1, 0),
(8, 50, 4, 'table_3', 200000.00, 700000.00, 0.90, 2.90, 5.80, 11.60, 0.80, 2.80, 5.60, 11.20, '3-6', 'សងទាំងដើមនិងការប្រាក់', 'សងទាំងដើមនិងការប្រាក់', '2017-09-27 10:09:55', '2017-09-27 10:09:55', '', 1, 0),
(9, 50, 4, 'table_3', 1000000.00, 1600000.00, 0.90, 3.00, 6.00, 12.00, 0.80, 2.90, 5.80, 11.60, '2-6', 'សងទាំងដើមនិងការប្រាក់', 'សងទាំងដើមនិងការប្រាក់', '2017-09-27 10:09:11', '2017-09-27 10:09:11', '', 1, 0),
(10, 50, 4, 'table_3', 1600000.00, 2000000.00, 0.00, 2.80, 5.60, 11.20, 0.00, 2.70, 5.40, 10.80, '2-6', 'សងទាំងដើមនិងការប្រាក់', 'សងទាំងដើមនិងការប្រាក់', '2017-09-27 10:09:22', '2017-09-27 10:09:22', '', 1, 0),
(11, 50, 3, 'table_2', 2000000.00, 4000000.00, 0.00, 2.70, 5.40, 10.80, 0.00, 2.60, 5.20, 10.40, '2-6', 'សងទាំងដើមនិងការប្រាក់', 'សងទាំងដើមនិងការប្រាក់', '2017-09-27 10:09:25', '2017-09-27 10:09:25', '', 1, 0),
(12, 50, 4, 'table_3', 4000000.00, 20000000.00, 0.00, 0.00, 0.00, 0.00, 0.00, 2.20, 4.40, 8.80, '2-6', 'សងទាំងដើមនិងការប្រាក់', 'សងទាំងដើមនិងការប្រាក់', '2017-09-27 10:09:40', '2017-09-27 10:09:40', '', 1, 0),
(13, 50, 5, 'table_4', 50.00, 175.00, 0.90, 2.90, 5.80, 11.60, 0.80, 2.80, 5.60, 11.20, '3-6', 'សងទាំងដើមនិងការប្រាក់', 'សងទាំងដើមនិងការប្រាក់', '2017-09-27 10:09:56', '2017-09-27 10:09:56', '', 1, 0),
(14, 50, 5, 'table_4', 250.00, 400.00, 0.90, 3.00, 6.00, 12.00, 0.80, 2.90, 5.80, 11.60, '2-6', 'សងទាំងដើមនិងការប្រាក់', 'សងទាំងដើមនិងការប្រាក់', '2017-09-27 10:09:06', '2017-09-27 10:09:06', '', 1, 0),
(15, 50, 5, 'table_4', 400.00, 500.00, 0.00, 2.80, 5.60, 11.20, 0.00, 2.70, 5.40, 10.80, '2-6', 'សងទាំងដើមនិងការប្រាក់', 'សងទាំងដើមនិងការប្រាក់', '2017-09-27 10:09:23', '2017-09-27 10:09:23', '', 1, 0),
(16, 50, 6, 'table_5', 50.00, 175.00, 0.90, 2.90, 5.80, 11.60, 0.80, 2.80, 5.60, 11.20, '3-6', 'សងទាំងដើមនិងការប្រាក់', 'សងទាំងដើមនិងការប្រាក់', '2017-09-27 10:09:44', '2017-09-27 10:09:44', '', 1, 0),
(17, 50, 6, 'table_5', 250.00, 400.00, 0.90, 3.00, 6.00, 12.00, 0.80, 2.90, 5.80, 11.60, '2-6', 'សងទាំងដើមនិងការប្រាក់', 'សងទាំងដើមនិងការប្រាក់', '2017-09-27 10:09:54', '2017-09-27 10:09:54', '', 1, 0),
(18, 50, 6, 'table_5', 400.00, 500.00, 0.00, 2.80, 5.60, 11.20, 0.00, 2.70, 5.40, 10.80, '2-6', 'សងទាំងដើមនិងការប្រាក់', 'សងទាំងដើមនិងការប្រាក់', '2017-09-27 10:09:05', '2017-09-27 10:09:05', '', 1, 0),
(19, 50, 6, 'table_5', 500.00, 1000.00, 0.00, 2.70, 5.40, 10.80, 0.00, 2.60, 5.20, 10.40, '2-6', 'សងទាំងដើមនិងការប្រាក់', 'សងទាំងដើមនិងការប្រាក់', '2017-09-27 10:09:15', '2017-09-27 10:09:15', '', 1, 0),
(20, 50, 7, 'table_6', 50.00, 175.00, 0.90, 2.90, 5.80, 11.60, 0.80, 2.80, 5.60, 11.20, '3-6', 'សងទាំងដើមនិងការប្រាក់', 'សងទាំងដើមនិងការប្រាក់', '2017-09-27 10:09:33', '2017-09-27 10:09:33', '', 1, 0),
(21, 50, 7, 'table_6', 250.00, 400.00, 0.90, 3.00, 6.00, 12.00, 0.80, 2.90, 5.80, 11.60, '2-6', 'សងទាំងដើមនិងការប្រាក់', 'សងទាំងដើមនិងការប្រាក់', '2017-09-27 10:09:45', '2017-09-27 10:09:45', '', 1, 0),
(22, 50, 7, 'table_6', 400.00, 500.00, 0.00, 2.80, 5.60, 11.20, 0.00, 2.70, 5.40, 10.80, '2-6', 'សងទាំងដើមនិងការប្រាក់', 'សងទាំងដើមនិងការប្រាក់', '2017-09-27 10:09:59', '2017-09-27 10:09:59', '', 1, 0),
(23, 50, 7, 'table_6', 500.00, 1000.00, 0.00, 2.70, 5.40, 10.80, 0.00, 2.60, 5.20, 10.40, '2-6', 'សងទាំងដើមនិងការប្រាក់', 'សងទាំងដើមនិងការប្រាក់', '2017-09-27 10:09:12', '2017-09-27 10:09:12', '', 1, 0),
(24, 50, 7, 'table_6', 1000.00, 5000.00, 0.00, 0.00, 0.00, 0.00, 0.00, 2.20, 4.40, 8.80, '2-6', 'សងទាំងដើមនិងការប្រាក់', 'សងទាំងដើមនិងការប្រាក់', '2017-09-27 10:09:23', '2017-09-27 10:09:23', '', 1, 0),
(25, 50, 8, 'table_7', 100000.00, 700000.00, 0.90, 2.90, 5.80, 11.60, 0.80, 2.80, 5.60, 11.20, '3-6', 'សងទាំងដើមនិងការប្រាក់', 'សងទាំងដើមនិងការប្រាក់', '2017-09-27 10:09:00', '2017-09-27 10:09:00', '', 1, 0),
(26, 50, 8, 'table_7', 700000.00, 1000000.00, 0.90, 3.00, 6.00, 12.00, 0.80, 2.90, 5.80, 11.60, '2-6', 'សងទាំងដើមនិងការប្រាក់', 'សងទាំងដើមនិងការប្រាក់', '2017-09-27 10:09:11', '2017-09-27 10:09:11', '', 1, 0),
(27, 50, 9, 'table_8', 100000.00, 700000.00, 0.00, 2.90, 5.80, 11.60, 0.00, 2.80, 5.60, 11.20, '3-6', 'សងតែការប្រាក់ និងរំលស់ដើមឬសងផ្តាច់នៅចុងវគ្គ ឬសងទាំងដើមនិងការប្រាក់', 'សងតែការប្រាក់ និងរំលស់ដើមឬសងផ្តាច់នៅចុងវគ្គ ឬសងទាំងដើមនិងការប្រាក់', '2017-09-27 10:09:21', '2017-09-27 10:09:21', '', 1, 0),
(28, 50, 9, 'table_8', 700000.00, 1000000.00, 0.00, 3.00, 6.00, 12.00, 0.00, 2.90, 5.80, 11.60, '3-6', 'សងតែការប្រាក់ និងរំលស់ដើមឬសងផ្តាច់នៅចុងវគ្គ ឬសងទាំងដើមនិងការប្រាក់', 'សងតែការប្រាក់ និងរំលស់ដើមឬសងផ្តាច់នៅចុងវគ្គ ឬសងទាំងដើមនិងការប្រាក់', '2017-09-27 10:09:32', '2017-09-27 10:09:32', '', 1, 0),
(29, 50, 10, 'table_9', 100000.00, 700000.00, 0.90, 2.90, 5.80, 11.60, 0.80, 2.80, 5.60, 11.20, '3-6', 'សងទាំងដើមនិងការប្រាក់', 'សងទាំងដើមនិងការប្រាក់', '2017-09-27 10:09:43', '2017-09-27 10:09:43', '', 1, 0),
(30, 50, 10, 'table_9', 700000.00, 1000000.00, 0.90, 3.00, 6.00, 12.00, 0.80, 2.90, 5.80, 11.60, '3-6', 'សងទាំងដើមនិងការប្រាក់', 'សងទាំងដើមនិងការប្រាក់', '2017-09-27 10:09:56', '2017-09-27 10:09:56', '', 1, 0),
(31, 50, 11, 'table_10', 50.00, 175.00, 0.90, 2.90, 5.80, 11.60, 0.80, 2.80, 5.60, 11.20, '3-6', 'សងទាំងដើមនិងការប្រាក់', 'សងទាំងដើមនិងការប្រាក់', '2017-09-27 10:09:08', '2017-09-27 10:09:08', '', 1, 0),
(32, 50, 11, 'table_10', 175.00, 250.00, 0.90, 3.00, 6.00, 12.00, 0.80, 2.90, 5.80, 11.60, '2-6', 'សងទាំងដើមនិងការប្រាក់', 'សងទាំងដើមនិងការប្រាក់', '2017-09-27 10:09:21', '2017-09-27 10:09:21', '', 1, 0),
(33, 50, 12, 'table_11', 50.00, 175.00, 0.00, 2.90, 5.80, 11.60, 0.00, 2.80, 5.60, 11.20, '3-6', 'សងទាំងដើមនិងការប្រាក់', 'សងទាំងដើមនិងការប្រាក់', '2017-09-27 10:09:43', '2017-09-27 10:09:43', '', 1, 0),
(34, 50, 12, 'table_11', 175.00, 250.00, 0.00, 3.00, 6.00, 12.00, 0.00, 2.90, 5.80, 11.60, '3-6', 'សងទាំងដើមនិងការប្រាក់', 'សងទាំងដើមនិងការប្រាក់', '2017-09-27 10:09:02', '2017-09-27 10:09:02', '', 1, 0),
(35, 50, 13, 'table_12', 50.00, 175.00, 0.90, 2.90, 5.80, 11.60, 0.80, 2.80, 5.60, 11.20, '3-6', 'សងទាំងដើមនិងការប្រាក់', 'សងទាំងដើមនិងការប្រាក់', '2017-09-27 10:09:13', '2017-09-27 10:09:13', '', 1, 0),
(36, 50, 13, 'table_12', 175.00, 250.00, 0.90, 3.00, 6.00, 12.00, 0.80, 2.90, 5.80, 11.60, '3-6', 'សងទាំងដើមនិងការប្រាក់', 'សងទាំងដើមនិងការប្រាក់', '2017-09-27 10:09:24', '2017-09-27 10:09:24', '', 1, 0),
(37, 50, 14, 'table_13', 0.00, 0.00, 0.00, 1.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 'unlimit', NULL, NULL, '2018-07-11 09:07:16', '2018-07-11 09:07:16', ' Spcail Interest ', 1, 0),
(38, 50, 14, 'table_13', 0.00, 0.00, 0.00, 0.00, 0.00, 1.00, 0.00, 0.00, 0.00, 0.00, 'unlimit', NULL, NULL, '2018-07-11 09:07:55', '2018-07-11 09:07:55', ' Spcail Interest ', 1, 0),
(39, 50, 14, 'table_13', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 1.00, 'unlimit', NULL, NULL, '2018-07-11 09:07:15', '2018-07-11 09:07:15', ' Spcail Interest ', 1, 0),
(40, 50, 14, 'table_13', 0.00, 0.00, 0.00, 0.00, 0.00, 1.00, 0.00, 0.00, 0.00, 0.00, 'unlimit', NULL, NULL, '2018-07-11 09:07:29', '2018-07-11 09:07:29', ' Spcail Interest ', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `mfi_new_interest_rate`
--

DROP TABLE IF EXISTS `mfi_new_interest_rate`;
CREATE TABLE IF NOT EXISTS `mfi_new_interest_rate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_dura` int(11) NOT NULL,
  `villige` decimal(20,0) NOT NULL,
  `branch` decimal(20,0) NOT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mfi_new_interest_rate`
--

INSERT INTO `mfi_new_interest_rate` (`id`, `type_dura`, `villige`, `branch`, `created_at`, `updated_at`) VALUES
(3, 1, '9', '8', '2018-06-06', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mfi_permission`
--

DROP TABLE IF EXISTS `mfi_permission`;
CREATE TABLE IF NOT EXISTS `mfi_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `display_name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `module` varchar(66) NOT NULL,
  `app_setting` varchar(30) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mfi_permission`
--

INSERT INTO `mfi_permission` (`id`, `name`, `display_name`, `description`, `module`, `app_setting`, `updated_at`) VALUES
(25, 'list-user-groups', 'List User Group', 'View List User groups', 'user-group', NULL, NULL),
(26, 'create-user-groups', 'Create User Group', 'You Can Create User groups', 'user-group', NULL, NULL),
(27, 'edit-user-groups', 'Edit User Group', 'You Can Edit User groups', 'user-group', NULL, NULL),
(28, 'Delete-user-groups', 'Delete User Group', 'You Can Delete User groups', 'user-group', NULL, NULL),
(29, 'list-client-groups', 'List Client Group', 'View List all groups', 'client-group', NULL, NULL),
(30, 'create-client-group', 'Create Client Group', 'Can Create Client Group', 'client-group', NULL, NULL),
(31, 'edit-client-group', 'Edit CLient Group', 'Can Edit CLient Group', 'client-group', NULL, NULL),
(32, 'deleted-client-group', 'Delete Client Group', 'Can Delete Client Group', 'client-group', NULL, NULL),
(33, 'list-client', 'List Client', 'View List all Client', 'client', NULL, NULL),
(34, 'create-client', 'Create Client', 'Can Create Client', 'client', NULL, NULL),
(49, 'credit-committy-list', 'Credit Committy View', '', 'c-committy', NULL, NULL),
(50, 'credit-committy-create', 'Credit Committy Create', '', 'c-committy', NULL, NULL),
(51, 'credit-committy-edit', 'Credit Committy Edit', '', 'c-committy', NULL, NULL),
(52, 'credit-committy-delete', 'Credit Committy Delete', '', 'c-committy', NULL, NULL),
(53, 'sch-generate', 'Generate Schedule', '', 'sch-gener', NULL, NULL),
(55, 'sch-show', 'Show Schedule', '', 'sch-gener', NULL, NULL),
(56, 'list-stuff-cashier', 'List Cashier to Staff ', '', 'staff-to-cashier', NULL, NULL),
(57, 'create-stuff-cashier', 'Create Cashier to Staff ', '', 'staff-to-cashier', NULL, NULL),
(58, 'edit-stuff-cashier', 'Edit Cashier to Staff ', '', 'staff-to-cashier', NULL, NULL),
(59, 'delete-stuff-cashier', 'Delete Cashier to Staff ', '', 'staff-to-cashier', NULL, NULL),
(64, 'list-interest_rate', 'List Interest Rate', '', 'interest_rate', NULL, NULL),
(65, 'create-interest_rate', 'Create Interest Rate', '', 'interest_rate', NULL, NULL),
(66, 'edit-interest_rate', 'Edit Interest Rate', '', 'interest_rate', NULL, NULL),
(68, 'delete-interest_rate', 'Delete Interest Rate', '', 'interest_rate', NULL, NULL),
(69, 'list-brand', 'List Brand', '', 'brand', NULL, NULL),
(70, 'create-brand', 'Create Brand', '', 'brand', NULL, NULL),
(71, 'deletd-brand', 'Delete Brand', '', 'brand', NULL, NULL),
(72, 'edit-brand', 'Edit Brand', '', 'brand', NULL, NULL),
(73, 'exchange-rate', 'Exchange', '', 'exchange_rate', NULL, NULL),
(74, 'edit-exchange-rate', 'Edit Exchange ', '', 'exchange_rate', NULL, NULL),
(75, 'Delete-exchange-rate', 'Delete Exchange ', '', 'exchange_rate', NULL, NULL),
(76, 'developer', 'Developer', 'developer', 'developer', NULL, '2017-07-13 12:02:50'),
(77, 'edit-client', 'Edit Client', 'Can Create Client', 'client', 'credit_sale', NULL),
(78, 'deleted-client', 'Delete Client', 'Can Create Client', 'client', 'credit_sale', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mfi_positions`
--

DROP TABLE IF EXISTS `mfi_positions`;
CREATE TABLE IF NOT EXISTS `mfi_positions` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mfi_positions`
--

INSERT INTO `mfi_positions` (`id`, `name`, `deleted`, `created_at`, `updated_at`) VALUES
(1, 'ប្រធានសាខាc', 1, NULL, NULL),
(2, 'មន្រ្តីប្រតិបត្តិ', 0, NULL, NULL),
(3, 'មន្រ្តីឥណទាន', 0, NULL, NULL),
(4, 'គណនេយ្យ', 1, NULL, NULL),
(5, 'គណនេយ្យ', 0, NULL, NULL),
(6, 'ជំនួយការរដ្ឋបាល និងហិរញ្ញវត្ថុ', 0, NULL, NULL),
(7, 'បេឡាករ', 0, NULL, NULL),
(8, 'ប្រធានក្រុមឥណទាន', 0, NULL, NULL),
(9, 'ប្រធានក្រុមប្រឹក្សាភិបាល និងជាអគ្គនាយក', 0, NULL, NULL),
(10, 'អនុប្រធានក្រុមប្រឹក្សាភិបាល និងជាអគ្គនាយករង', 0, NULL, NULL),
(11, 'ក្រុមប្រឹក្សាភិបាល', 0, NULL, NULL),
(12, 'Developer', 0, NULL, NULL),
(13, 'ជំនួយការរដ្ឋបាល និងគណនេយ្យ', 0, NULL, NULL),
(14, 'Sana', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mfi_public_holiday`
--

DROP TABLE IF EXISTS `mfi_public_holiday`;
CREATE TABLE IF NOT EXISTS `mfi_public_holiday` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `discription` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mfi_public_holiday`
--

INSERT INTO `mfi_public_holiday` (`id`, `title`, `duration`, `start_date`, `end_date`, `discription`, `user_id`, `deleted`, `created_at`, `updated_at`) VALUES
(1, 'Khmer New Year Day', 3, '2018-04-14', '2018-04-16', 'Khmer New Year Day', 50, NULL, '2018-05-28 04:05:26', '2018-07-24 05:07:25'),
(4, 'Constitutional Day', 1, '2018-09-24', '2018-09-24', 'Constitutional Day', 50, NULL, '2018-07-23 10:07:14', NULL),
(5, 'Pchum Ben Day', 3, '2018-10-08', '2018-10-10', 'Pchum Ben Day', 50, NULL, '2018-07-23 10:07:38', '2018-07-24 05:07:28'),
(6, 'Commemoration Day of King\'s Father, Norodom Sihanouk', 1, '2018-10-15', '2018-10-15', 'Commemoration Day of King\'s Father, Norodom Sihanouk', 50, NULL, '2018-07-24 05:07:09', NULL),
(7, 'Anniversary of the Paris Peace Accord', 1, '2018-10-23', '2018-10-23', 'Anniversary of the Paris Peace Accord', 50, NULL, '2018-07-24 05:07:57', NULL),
(8, 'King\'s Coronation Day, Norodom Sihamoni', 1, '2018-10-29', '2018-10-29', 'King\'s Coronation Day, Norodom Sihamoni', 50, NULL, '2018-07-24 05:07:23', NULL),
(9, 'Independence Day', 1, '2018-11-09', '2018-11-09', 'Independence Day', 50, NULL, '2018-07-24 05:07:48', NULL),
(10, 'Water Festival Ceremony', 3, '2018-11-21', '2018-11-23', 'Water Festival Ceremony', 50, NULL, '2018-07-24 05:07:24', NULL),
(11, 'International Human Rights Day', 1, '2018-12-10', '2018-12-10', 'International Human Rights Day', 50, NULL, '2018-07-24 05:07:03', NULL),
(12, 'International New Year Day', 1, '2018-01-01', '2018-01-01', 'International New Year Day', 50, NULL, '2018-07-24 05:07:55', NULL),
(13, 'Victory over Genocide Day', 1, '2018-01-07', '2018-01-07', 'Victory over Genocide Day', 50, NULL, '2018-07-24 05:07:34', NULL),
(14, 'Meak Bochea Day', 1, '2018-01-31', '2018-01-31', 'Meak Bochea Day', 50, NULL, '2018-07-24 05:07:01', NULL),
(15, 'International Women\'s Day', 1, '2018-03-08', '2018-03-08', 'International Women\'s Day', 50, NULL, '2018-07-24 05:07:34', NULL),
(16, 'Visak Bochea Day', 1, '2018-04-29', '2018-04-29', 'Visak Bochea Day', 50, NULL, '2018-07-24 05:07:03', NULL),
(17, 'International Labor Day', 1, '2018-05-01', '2018-05-01', 'International Labor Day', 50, NULL, '2018-07-24 05:07:30', NULL),
(18, 'Royal Plowing Ceremony', 1, '2018-05-03', '2018-05-03', 'Royal Plowing Ceremony', 50, NULL, '2018-07-24 05:07:59', NULL),
(19, 'King\'s Birthday, Norodom Sihamoni', 3, '2018-05-13', '2018-05-15', 'King\'s Birthday, Norodom Sihamoni', 50, NULL, '2018-07-24 05:07:26', NULL),
(20, 'National Day of Remembrance', 1, '2018-05-20', '2018-05-20', 'National Day of Remembrance', 50, NULL, '2018-07-24 05:07:54', NULL),
(21, 'International Children Day', 1, '2018-06-01', '2018-06-01', 'International Children Day', 50, NULL, '2018-07-24 05:07:25', NULL),
(22, 'King\'s Mother Birthday, Norodom Monineath Sihanouk', 1, '2018-06-18', '2018-06-18', 'King\'s Mother Birthday, Norodom Monineath Sihanouk', 50, NULL, '2018-07-24 05:07:51', NULL),
(23, 'Election Day', 3, '2018-07-28', '2018-07-30', 'Election Day', 50, NULL, '2018-07-24 05:07:05', NULL),
(24, 'International New Year Day', 1, '2019-01-01', '2019-01-01', 'International New Year Day', 50, NULL, '2018-07-24 05:07:35', '2018-07-24 05:07:52'),
(25, 'Meak Bochea Day', 1, '2019-02-19', '2019-02-19', 'Meak Bochea Day', 50, NULL, '2018-07-24 05:07:04', NULL),
(26, 'International Women\'s Day', 1, '2019-03-08', '2019-03-08', 'International Women\'s Day', 50, NULL, '2018-07-24 05:07:45', NULL),
(27, 'Khmer New Year Day', 3, '2019-04-14', '2019-04-17', 'Khmer New Year Day', 50, NULL, '2018-07-24 05:07:54', '2018-07-24 05:07:16'),
(28, 'Victory over Genocide Day', 1, '2019-01-07', '2019-01-07', 'Victory over Genocide Day', 50, NULL, '2018-07-24 05:07:21', NULL),
(29, 'International Labor Day', 1, '2019-05-01', '2019-05-01', 'International Labor Day', 50, NULL, '2018-07-24 05:07:02', '2018-07-24 05:07:35'),
(30, 'King\'s Birthday, Norodom Sihamoni', 3, '2019-05-13', '2019-05-15', 'King\'s Birthday, Norodom Sihamoni', 50, NULL, '2018-07-24 05:07:19', NULL),
(31, 'Visak Bochea Day', 1, '2019-05-18', '2019-05-18', 'Visak Bochea Day', 50, NULL, '2018-07-24 05:07:34', '2018-07-24 05:07:45'),
(32, 'National Day of Remembrance', 1, '2019-05-20', '2019-05-20', 'National Day of Remembrance', 50, NULL, '2018-07-24 05:07:34', NULL),
(33, 'Royal Plowing Ceremony', 1, '2019-05-22', '2019-05-22', 'Royal Plowing Ceremony', 50, NULL, '2018-07-24 05:07:52', NULL),
(34, 'International Children Day', 1, '2019-06-01', '2019-06-01', 'International Children Day', 50, NULL, '2018-07-24 05:07:37', NULL),
(35, 'King\'s Mother Birthday, Norodom Monineath Sihanouk', 1, '2019-06-18', '2019-06-18', 'King\'s Mother Birthday, Norodom Monineath Sihanouk', 50, NULL, '2018-07-24 05:07:16', NULL),
(36, 'Constitutional Day', 24, '2019-09-24', '2019-09-24', 'King\'s Mother Birthday, Norodom Monineath Sihanouk', 50, NULL, '2018-07-24 06:07:10', NULL),
(37, 'Pchum Ben Day', 3, '2019-09-27', '2019-09-30', 'Pchum Ben Day', 50, NULL, '2018-07-24 06:07:25', '2018-07-24 06:07:14'),
(38, 'Commemoration Day of King\'s Father, Norodom Sihanouk', 1, '2019-10-15', '2019-10-15', 'Commemoration Day of King\'s Father, Norodom Sihanouk', 50, NULL, '2018-07-24 06:07:52', NULL),
(39, 'Anniversary of the Paris Peace Accord', 1, '2019-10-23', '2019-10-23', 'Anniversary of the Paris Peace Accord', 50, NULL, '2018-07-24 06:07:54', NULL),
(40, 'King\'s Coronation Day, Norodom Sihamoni', 1, '2019-10-29', '2019-10-29', 'King\'s Coronation Day, Norodom Sihamoni', 50, NULL, '2018-07-24 06:07:26', NULL),
(41, 'Independence Day', 1, '2019-11-09', '2019-11-09', 'Independence Day', 50, NULL, '2018-07-24 06:07:56', NULL),
(42, 'Water Festival Ceremony', 3, '2019-11-10', '2019-11-13', 'Water Festival Ceremony', 50, NULL, '2018-07-24 06:07:41', NULL),
(43, 'International Human Rights Day', 1, '2019-12-10', '2019-12-10', 'International Human Rights Day', 50, NULL, '2018-07-24 06:07:22', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mfi_public_holiday_reset`
--

DROP TABLE IF EXISTS `mfi_public_holiday_reset`;
CREATE TABLE IF NOT EXISTS `mfi_public_holiday_reset` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mfi_public_holiday_id` int(11) DEFAULT NULL,
  `date_for_holiday` date DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=93 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mfi_public_holiday_reset`
--

INSERT INTO `mfi_public_holiday_reset` (`id`, `mfi_public_holiday_id`, `date_for_holiday`, `note`) VALUES
(35, 13, '2018-01-07', NULL),
(34, 12, '2018-01-01', NULL),
(33, 1, '2018-04-16', NULL),
(32, 1, '2018-04-15', NULL),
(31, 1, '2018-04-14', NULL),
(9, 4, '2018-09-24', NULL),
(22, 5, '2018-10-10', NULL),
(21, 5, '2018-10-09', NULL),
(20, 5, '2018-10-08', NULL),
(23, 6, '2018-10-15', NULL),
(24, 7, '2018-10-23', NULL),
(25, 8, '2018-10-29', NULL),
(26, 9, '2018-11-09', NULL),
(27, 10, '2018-11-21', NULL),
(28, 10, '2018-11-22', NULL),
(29, 10, '2018-11-23', NULL),
(30, 11, '2018-12-10', NULL),
(36, 14, '2018-01-31', NULL),
(37, 15, '2018-03-08', NULL),
(38, 16, '2018-04-29', NULL),
(39, 17, '2018-05-01', NULL),
(40, 18, '2018-05-03', NULL),
(41, 19, '2018-05-13', NULL),
(42, 19, '2018-05-14', NULL),
(43, 19, '2018-05-15', NULL),
(44, 20, '2018-05-20', NULL),
(45, 21, '2018-06-01', NULL),
(46, 22, '2018-06-18', NULL),
(47, 23, '2018-07-28', NULL),
(48, 23, '2018-07-29', NULL),
(49, 23, '2018-07-30', NULL),
(52, 24, '2019-01-01', NULL),
(53, 25, '2019-02-19', NULL),
(54, 26, '2019-03-08', NULL),
(62, 27, '2019-04-17', NULL),
(61, 27, '2019-04-16', NULL),
(60, 27, '2019-04-15', NULL),
(59, 27, '2019-04-14', NULL),
(63, 28, '2019-01-07', NULL),
(65, 29, '2019-05-01', NULL),
(66, 30, '2019-05-13', NULL),
(67, 30, '2019-05-14', NULL),
(68, 30, '2019-05-15', NULL),
(70, 31, '2019-05-18', NULL),
(71, 32, '2019-05-20', NULL),
(72, 33, '2019-05-22', NULL),
(73, 34, '2019-06-01', NULL),
(74, 35, '2019-06-18', NULL),
(75, 36, '2019-09-24', NULL),
(84, 37, '2019-09-30', NULL),
(83, 37, '2019-09-29', NULL),
(82, 37, '2019-09-28', NULL),
(81, 37, '2019-09-27', NULL),
(80, 38, '2019-10-15', NULL),
(85, 39, '2019-10-23', NULL),
(86, 40, '2019-10-29', NULL),
(87, 41, '2019-11-09', NULL),
(88, 42, '2019-11-10', NULL),
(89, 42, '2019-11-11', NULL),
(90, 42, '2019-11-12', NULL),
(91, 42, '2019-11-13', NULL),
(92, 43, '2019-12-10', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `module_interest`
--

DROP TABLE IF EXISTS `module_interest`;
CREATE TABLE IF NOT EXISTS `module_interest` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name_eng` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name_kh` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` int(11) NOT NULL,
  `loan_type` int(11) NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `module_interest`
--

INSERT INTO `module_interest` (`id`, `user_id`, `name`, `display_name_eng`, `display_name_kh`, `active`, `loan_type`, `description`, `created_at`, `updated_at`) VALUES
(2, 50, 'table_1', 'តារាងទី ១៖ កម្ចីសម្រាប់ការប្រើប្រាស់ផ្សេងៗ ជាប្រាក់រៀល', 'តារាងទី ១៖ កម្ចីសម្រាប់ការប្រើប្រាស់ផ្សេងៗ ជាប្រាក់រៀល', 1, 1, 'Nothing Permission', '2017-09-05 17:00:00', '2017-09-27 04:00:00'),
(3, 50, 'table_2', 'តារាងទី ២៖ កម្ចីសម្រាប់កសិកម្ម ជាប្រាក់រៀល', 'តារាងទី ២៖ កម្ចីសម្រាប់កសិកម្ម ជាប្រាក់រៀល', 1, 1, '', '2017-09-26 17:00:00', NULL),
(4, 50, 'table_3', 'តារាងទី ៣៖ កម្ចីសម្រាប់ពាណិជ្ជកម្ម សេវាកម្ម ឬផលិតកម្មផ្សេងៗ ជាប្រាក់រៀល', 'តារាងទី ៣៖ កម្ចីសម្រាប់ពាណិជ្ជកម្ម សេវាកម្ម ឬផលិតកម្មផ្សេងៗ ជាប្រាក់រៀល', 1, 1, '', '2017-09-26 17:00:00', NULL),
(5, 50, 'table_4', 'តារាងទី ៤៖ កម្ចីប្រើប្រាស់ផ្សេងៗ ជាប្រាក់ដុល្លារ', 'តារាងទី ៤៖ កម្ចីប្រើប្រាស់ផ្សេងៗ ជាប្រាក់ដុល្លារ', 1, 1, '', '2017-09-26 17:00:00', NULL),
(6, 50, 'table_5', 'តារាងទី ៥៖ កម្ចីសម្រាប់កសិកម្ម ជាប្រាក់ដុល្លារ', 'តារាងទី ៥៖ កម្ចីសម្រាប់កសិកម្ម ជាប្រាក់ដុល្លារ', 1, 1, '', '2017-09-26 17:00:00', NULL),
(7, 50, 'table_6', 'តារាងទី ៦៖ កម្ចីសម្រាប់ពាណិជ្ជកម្ម សេវាកម្ម ឬផលិតកម្មផ្សេងៗ ជាប្រាក់ដុល្លារ', 'តារាងទី ៦៖ កម្ចីសម្រាប់ពាណិជ្ជកម្ម សេវាកម្ម ឬផលិតកម្មផ្សេងៗ ជាប្រាក់ដុល្លារ', 1, 1, '', '2017-09-26 17:00:00', NULL),
(8, 50, 'table_7', 'តារាងទី ៧៖ កម្ចីសម្រាប់ការប្រើប្រាស់ផ្សេងៗ ជាប្រាក់រៀល', 'តារាងទី ៧៖ កម្ចីសម្រាប់ការប្រើប្រាស់ផ្សេងៗ ជាប្រាក់រៀល', 1, 2, '', '2017-09-26 17:00:00', NULL),
(9, 50, 'table_8', 'តារាងទី ៨៖ កម្ចីសម្រាប់កសិកម្ម ជាប្រាក់រៀល', 'តារាងទី ៨៖ កម្ចីសម្រាប់កសិកម្ម ជាប្រាក់រៀល', 1, 2, '', '2017-09-26 17:00:00', NULL),
(10, 50, 'table_9', 'តារាងទី ៩៖ កម្ចីសម្រាប់ពាណិជ្ជកម្ម សេវាកម្ម ឬផលិតកម្មផ្សេងៗ ជាប្រាក់រៀល', 'តារាងទី ៩៖ កម្ចីសម្រាប់ពាណិជ្ជកម្ម សេវាកម្ម ឬផលិតកម្មផ្សេងៗ ជាប្រាក់រៀល', 1, 2, '', '2017-09-26 17:00:00', NULL),
(11, 50, 'table_10', 'តារាងទី ១០៖ កម្ចីប្រើប្រាស់ផ្សេងៗ ជាប្រាក់ដុល្លារ', 'តារាងទី ១០៖ កម្ចីប្រើប្រាស់ផ្សេងៗ ជាប្រាក់ដុល្លារ', 1, 2, '', '2017-09-26 17:00:00', NULL),
(12, 50, 'table_11', 'តារាងទី ១១៖ កម្ចីសម្រាប់កសិកម្ម ជាប្រាក់ដុល្លារ', 'តារាងទី ១១៖ កម្ចីសម្រាប់កសិកម្ម ជាប្រាក់ដុល្លារ', 1, 2, '', '2017-09-26 17:00:00', NULL),
(13, 50, 'table_12', 'តារាងទី ១២៖ កម្ចីសម្រាប់ពាណិជ្ជកម្ម សេវាកម្ម ឬផលិតកម្មផ្សេងៗ ជាប្រាក់ដុល្លារ', 'តារាងទី ១២៖ កម្ចីសម្រាប់ពាណិជ្ជកម្ម សេវាកម្ម ឬផលិតកម្មផ្សេងៗ ជាប្រាក់ដុល្លារ', 1, 2, '', '2017-09-26 17:00:00', NULL),
(14, 50, 'table_13', 'តារាងទី ១៣៖ កម្ចីសម្រាប់បុគ្គលិកជាបុគ្គល', 'តារាងទី ១៣៖ កម្ចីសម្រាប់បុគ្គលិកជាបុគ្គល', 1, 0, '', '2017-09-26 17:00:00', NULL),
(15, 50, 'table_14', 'តារាងទី ១៤៖ កម្ចីសម្រាប់បុគ្គលិកជាក្រុម', 'តារាងទី ១៤៖ កម្ចីសម្រាប់បុគ្គលិកជាក្រុម', 1, 0, '', '2017-09-26 17:00:00', '2017-09-26 17:00:00'),
(16, 50, 'table_15', 'តារាងទី ១៥៖ កម្ចីផ្សេងៗជាបុគ្កល', 'តារាងទី ១៥៖ កម្ចីផ្សេងៗជាបុគ្កល', 1, 0, '', '2017-09-26 17:00:00', NULL),
(17, 50, 'table_16', 'តារាងទី ១៦៖ កម្ចីផ្សេងៗជាក្រុម', 'តារាងទី ១៦៖ កម្ចីផ្សេងៗជាក្រុម', 1, 0, '', '2017-09-26 17:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `user_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_user_email_index` (`user_email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rate_percent`
--

DROP TABLE IF EXISTS `rate_percent`;
CREATE TABLE IF NOT EXISTS `rate_percent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `meta_key` text,
  `value_option` char(255) DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rate_percent`
--

INSERT INTO `rate_percent` (`id`, `meta_key`, `value_option`, `deleted`, `created_at`, `updated_at`) VALUES
(1, 'money', 'khmer', NULL, NULL, '2019-01-06 04:28:01'),
(4, 'money', 'us', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_kh` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_code` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position_id` int(11) DEFAULT NULL,
  `user_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_gender` enum('F','M') COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_last_login` datetime DEFAULT NULL,
  `user_last_logout` datetime DEFAULT NULL,
  `user_status` int(11) DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `photo` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deteted` int(11) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `api_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_user_email_unique` (`user_email`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `branch_id`, `username`, `name_kh`, `user_code`, `position_id`, `user_email`, `user_phone`, `user_gender`, `user_last_login`, `user_last_logout`, `user_status`, `password`, `photo`, `deteted`, `remember_token`, `api_token`, `created_at`, `updated_at`) VALUES
(50, 2, 'socheat', 'សុជាតិ', '', 12, 'socheat@titb.biz', '016-707-044', 'F', '2019-02-20 11:02:57', '2018-09-06 07:09:13', 1, '$2y$10$t1tYFydr.jgaDJNjodr6he9g0dULeXRM9SI4Z3OxZnFIw0p1Tieju', NULL, 1, '3W4jAb2Y0e61TjNHyuxEv9THEjmOckx36AWMwQcu8gqeo1BI2MxbZCkLHMtD', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjUwLCJpc3MiOiJodHRwOi8vbG9jYWxob3N0L2NyZWRpdC1zYWxlL21hcGkxL3B1YmxpYy9hcGkvbG9naW4iLCJpYXQiOjE1MjI5MjQ5MDAsImV4cCI6MTUyMjkyODUwMCwibmJmIjoxNTIyOTI0OTAwLCJqdGkiOiIzYnVLWTR5c1hlcTVnanlwIn0.OR_ZvpYLfrrRlpBnvKTbGF', '2017-07-13 16:07:09', '2018-09-06 00:09:31'),
(52, 2, 'socheat1', 'socheat chhoeun', NULL, 12, 'socheatit1991@gmail.com', '+85-585-218-806', 'M', '2018-09-06 07:09:08', '2018-09-06 07:09:37', 1, '$2y$10$HqX3wrGTn91/YYvqHTZii.DZsGiWlUKs7YIWjKuz7ZiP2.Pg5oIMS', '0026220250_1_1_4.jpg', 1, 'NGfLgo7YeLQNbNcXDkRM3ZPb9mAUBRTUJwfGZ1BRggqEcejkhoEJBW3pijS3', NULL, '2018-09-06 00:09:15', '2018-09-06 00:09:58'),
(53, 2, 'developer', 'អ្នកបង្កើត', NULL, 12, 'socheatit1995@gmail.com', '016-707-044', 'F', '2018-12-28 02:12:16', NULL, 1, '$2y$10$BB3tpao1vnSPR68HiKb6t.RJ0064JSlGnPk1YDkJKVTJNEQVaDZh6', '91d6028c23b501f5fdb08194139b52e4.jpg', 1, NULL, NULL, '2018-11-19 03:11:28', '2018-12-28 07:53:58');

-- --------------------------------------------------------

--
-- Table structure for table `user_groups`
--

DROP TABLE IF EXISTS `user_groups`;
CREATE TABLE IF NOT EXISTS `user_groups` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `group_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`user_id`,`group_id`),
  KEY `user_groups_group_id_foreign` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_groups`
--

INSERT INTO `user_groups` (`user_id`, `group_id`) VALUES
(52, 1),
(50, 8),
(53, 8);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `user_groups`
--
ALTER TABLE `user_groups`
  ADD CONSTRAINT `user_groups_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_groups_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
