-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Mar 24, 2018 at 04:48 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.0.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `credit_sale_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `cs-cds-request-item`
--

CREATE TABLE `cs-cds-request-item` (
  `id` int(11) NOT NULL,
  `cds-request-id` int(11) DEFAULT NULL,
  `product-id` int(11) DEFAULT NULL,
  `barcode` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `price-total-of-pro` decimal(30,0) DEFAULT NULL,
  `price-of-deposit-product` decimal(30,0) DEFAULT NULL,
  `price-total-for-pay` decimal(30,0) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cs-co-give-product-client`
--

CREATE TABLE `cs-co-give-product-client` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `sale_id` int(11) NOT NULL DEFAULT '0',
  `cds_request_id` int(11) DEFAULT NULL,
  `manager-branch-approval-id` int(11) DEFAULT NULL,
  `staff_id` int(11) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  `approve_by` int(11) DEFAULT NULL,
  `date_give` date DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cs-deposit-setting`
--

CREATE TABLE `cs-deposit-setting` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `number` decimal(30,0) DEFAULT NULL,
  `num_of_percens` decimal(10,0) DEFAULT NULL,
  `note` text CHARACTER SET latin1,
  `deleted` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cs-history-logs`
--

CREATE TABLE `cs-history-logs` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `ip_address` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `note` text CHARACTER SET latin1,
  `action` text CHARACTER SET latin1,
  `date_created` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cs-instocks`
--

CREATE TABLE `cs-instocks` (
  `id` int(11) NOT NULL,
  `item_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `size` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `qty` decimal(30,0) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  `date_include` date DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cs-interest-crdit-sell`
--

CREATE TABLE `cs-interest-crdit-sell` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `rate-name` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `size_money_from` decimal(30,0) DEFAULT NULL,
  `size_money_to` decimal(30,0) DEFAULT NULL,
  `day_rate_villige` decimal(30,0) DEFAULT NULL,
  `weekly_rate_villige` decimal(30,0) DEFAULT NULL,
  `two_weekly_rate_villige` decimal(30,0) DEFAULT NULL,
  `monthly_rate_villige` decimal(30,0) DEFAULT NULL,
  `day_rate_brand` decimal(30,0) DEFAULT NULL,
  `weekly_rate_brand` decimal(30,0) DEFAULT NULL,
  `two_weekly_brand` decimal(30,0) DEFAULT NULL,
  `monthly_rate_brand` decimal(30,0) DEFAULT NULL,
  `duration` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `methot description` text CHARACTER SET latin1,
  `deleted` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cs-inventorys`
--

CREATE TABLE `cs-inventorys` (
  `id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `comment` text CHARACTER SET latin1,
  `status_product` int(11) DEFAULT NULL,
  `status_transation` int(11) DEFAULT NULL,
  `qty` decimal(30,0) DEFAULT NULL,
  `size` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cs-manager-branch-approval`
--

CREATE TABLE `cs-manager-branch-approval` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `staff_id` int(11) DEFAULT NULL,
  `manager_brand_id` int(11) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `cds_request_id` int(11) DEFAULT NULL,
  `interest_cds_id` int(11) DEFAULT NULL,
  `num_of_pro` decimal(30,0) DEFAULT NULL,
  `is_agree` int(11) DEFAULT NULL,
  `discount` decimal(30,0) DEFAULT NULL,
  `deposit_pro_percen_of_price_product` decimal(30,0) DEFAULT NULL,
  `deposit_pro_num` decimal(30,0) DEFAULT NULL,
  `deposit_pro_word` int(11) DEFAULT NULL,
  `price_total_num` decimal(30,0) DEFAULT NULL,
  `price_total_word` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `price_for_pay` decimal(30,0) DEFAULT NULL,
  `money_type` int(11) DEFAULT NULL,
  `duration_pay_money` int(11) DEFAULT NULL,
  `duration_pay_money_type` int(11) DEFAULT NULL,
  `date_for_payments` date DEFAULT NULL,
  `date_approval` date DEFAULT NULL,
  `date_give_product` date DEFAULT NULL,
  `created_approval_date` date DEFAULT NULL,
  `update_approval_date` date DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cs-payments`
--

CREATE TABLE `cs-payments` (
  `id` int(11) NOT NULL,
  `sale_id` int(11) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `is_direct_payment` int(11) DEFAULT NULL,
  `cds_request_id` int(11) DEFAULT NULL,
  `manager-branch-approval-id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `staff_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `place_for_pay` int(11) DEFAULT NULL,
  `payment_amount` decimal(30,0) DEFAULT NULL,
  `sub_payment_amount` decimal(30,0) DEFAULT NULL,
  `dute__payment_amount` decimal(30,0) DEFAULT NULL,
  `payment_date` date DEFAULT NULL,
  `payment_for_date` date DEFAULT NULL,
  `penalty` decimal(30,0) DEFAULT NULL,
  `remaining` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `date_created` date DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cs-purchase-order-detail`
--

CREATE TABLE `cs-purchase-order-detail` (
  `id` int(11) NOT NULL,
  `product-id` int(11) DEFAULT NULL,
  `cs-purchase_order_id` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cs-purchase_order`
--

CREATE TABLE `cs-purchase_order` (
  `id` int(11) NOT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `branch-id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `stuff_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  `order-date` date DEFAULT NULL,
  `order-modify-date` date DEFAULT NULL,
  `created-at` datetime DEFAULT NULL,
  `update-at` datetime DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cs-sale-item`
--

CREATE TABLE `cs-sale-item` (
  `id` int(11) NOT NULL,
  `sale_id` int(11) DEFAULT NULL,
  `barcode` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `total_price` decimal(30,0) DEFAULT NULL,
  `total_of_deposit` decimal(10,0) DEFAULT NULL,
  `total_price_payment` decimal(10,0) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cs-sales`
--

CREATE TABLE `cs-sales` (
  `id` int(11) NOT NULL,
  `client_id` int(11) DEFAULT NULL,
  `cds_request_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `staff_id` int(11) DEFAULT NULL,
  `manager-branch-approval-id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `total_due_amout` decimal(30,0) DEFAULT NULL,
  `total_amount` decimal(30,0) DEFAULT NULL,
  `sale_status_for` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `discount` decimal(30,0) DEFAULT NULL,
  `is_oncredit` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  `remaining` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `method` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `description` text CHARACTER SET latin1,
  `deleted` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cs-schedules`
--

CREATE TABLE `cs-schedules` (
  `id` int(11) NOT NULL,
  `client-id` int(11) DEFAULT NULL,
  `sale_id` int(11) DEFAULT '0',
  `branch-id` int(11) DEFAULT NULL,
  `cds-request-id` int(11) DEFAULT NULL,
  `manager-branch-approval-id` int(11) DEFAULT NULL,
  `user-id` int(11) DEFAULT NULL,
  `staff-id` int(11) DEFAULT NULL,
  `total-price-of-pro` decimal(30,0) DEFAULT NULL,
  `total-price-deposit-of-pro` decimal(30,0) DEFAULT NULL,
  `total-price-payment-pro` decimal(30,0) DEFAULT NULL,
  `money_type` int(11) DEFAULT NULL,
  `interest-cds-id` int(11) DEFAULT NULL,
  `qty-pro` int(11) DEFAULT NULL,
  `date_for_payments` date DEFAULT NULL,
  `date_approval` date DEFAULT NULL,
  `date_give_product` date DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `status-for-pay` int(11) DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  `is_give` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cs-schedules-repayment-timesheet`
--

CREATE TABLE `cs-schedules-repayment-timesheet` (
  `id` int(11) NOT NULL,
  `schedule-id` int(11) DEFAULT NULL,
  `branch-id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `staff_id` int(11) DEFAULT NULL,
  `schedules-titmesheet-id` int(11) DEFAULT NULL,
  `available-total-pay-cost` decimal(30,0) DEFAULT NULL,
  `available-total-pay-interest` decimal(30,0) DEFAULT NULL,
  `available-total-payment` decimal(30,0) DEFAULT NULL,
  `available-total-pay-cost-owe` decimal(30,0) DEFAULT NULL,
  `date-payment` date DEFAULT NULL,
  `available-date-payment` date DEFAULT NULL,
  `lavel-payment` int(11) DEFAULT NULL,
  `date-late-pay` date DEFAULT NULL,
  `penalty-pay` int(11) DEFAULT NULL,
  `available-remain` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `status-for-pay` int(11) DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  `note` text,
  `deleted` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cs-schedules-timesheet`
--

CREATE TABLE `cs-schedules-timesheet` (
  `id` int(11) NOT NULL,
  `schedule-id` int(11) DEFAULT NULL,
  `total-pay-cost` decimal(30,0) DEFAULT NULL,
  `total-pay-interest` decimal(30,0) DEFAULT NULL,
  `total-payment` decimal(30,0) DEFAULT NULL,
  `total-pay-cost-owe` decimal(30,0) DEFAULT NULL,
  `date-payment` date DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `status-for-pay` int(11) DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  `description` text CHARACTER SET latin1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cs_brand`
--

CREATE TABLE `cs_brand` (
  `id` int(11) NOT NULL,
  `name` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `description` text CHARACTER SET latin1,
  `status` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cs_categorys`
--

CREATE TABLE `cs_categorys` (
  `id` int(11) NOT NULL,
  `name` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `description` text CHARACTER SET latin1,
  `status` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cs_clients`
--

CREATE TABLE `cs_clients` (
  `id` int(11) NOT NULL,
  `client_code` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `brand_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `kh_name_first` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `kh_name_last` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `en_name_first` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `en_name_last` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `kh_username` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `en_username` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `gender` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `nationality` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `identify_num` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `identify_type` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `identify_by` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `home_num` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `group_num` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `street_num` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `vilige` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `commune` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `district` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `province` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `phone` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `job` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `place_job` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `upload_relate_document` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `staff_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `client_status` int(11) DEFAULT NULL,
  `description` text CHARACTER SET latin1,
  `deleted` int(11) DEFAULT NULL,
  `larvel_client` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cs_currency`
--

CREATE TABLE `cs_currency` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `value_option` varchar(255) DEFAULT NULL,
  `note` text,
  `deleted` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cs_exchange_rate`
--

CREATE TABLE `cs_exchange_rate` (
  `id` int(10) UNSIGNED NOT NULL,
  `real` int(11) NOT NULL,
  `us` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cs_history_log`
--

CREATE TABLE `cs_history_log` (
  `id` int(11) NOT NULL,
  `ip_log` varchar(60) DEFAULT NULL,
  `active` text,
  `user_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL COMMENT '1 => view , 2 => create , 3 => update , 4 => delete , 5 => search',
  `create_date` datetime DEFAULT NULL,
  `what_id` int(11) DEFAULT NULL,
  `method` varchar(60) DEFAULT NULL,
  `date_login` datetime DEFAULT NULL,
  `date_logout` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cs_items`
--

CREATE TABLE `cs_items` (
  `id` int(11) NOT NULL,
  `item_id` int(11) DEFAULT NULL,
  `add_number_id` int(11) DEFAULT NULL,
  `item_bacode` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `brand_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `qty` decimal(30,0) DEFAULT NULL,
  `tax_include` decimal(30,0) DEFAULT NULL,
  `commission_fixed` decimal(30,0) DEFAULT NULL,
  `commission_give` decimal(30,0) DEFAULT NULL,
  `cost_price` decimal(30,0) DEFAULT NULL,
  `sell_price` decimal(30,0) DEFAULT NULL,
  `promo_price` decimal(30,0) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `discount` decimal(30,0) DEFAULT NULL,
  `pro_image` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `size` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `description` text CHARACTER SET latin1,
  `is_service` int(11) DEFAULT NULL,
  `quality` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `recode_level` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cs_it_images`
--

CREATE TABLE `cs_it_images` (
  `id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `image` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cs_payment_type`
--

CREATE TABLE `cs_payment_type` (
  `id` int(11) NOT NULL,
  `name` varchar(60) NOT NULL,
  `display_name` varchar(60) DEFAULT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cs_penaty`
--

CREATE TABLE `cs_penaty` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `late_form` int(11) NOT NULL,
  `at_late` int(11) NOT NULL,
  `color` varchar(50) NOT NULL,
  `note` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cs_request_form`
--

CREATE TABLE `cs_request_form` (
  `id` int(11) NOT NULL,
  `client_id` int(11) DEFAULT NULL,
  `sale_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `staff_id` int(11) DEFAULT NULL,
  `barcord` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `prices_total_num` decimal(30,0) DEFAULT NULL,
  `prices_totalword` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `price_for_pay` decimal(30,0) DEFAULT NULL,
  `money_type` int(11) DEFAULT NULL,
  `duration_pay_money` int(11) DEFAULT NULL,
  `duration_pay_money_type` int(11) DEFAULT NULL,
  `date_for_payments` date DEFAULT NULL,
  `date_create_request` date DEFAULT NULL,
  `upload_relate_doc` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `is_agree` int(11) DEFAULT NULL,
  `is_give` int(11) DEFAULT NULL,
  `is_break` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cs_sub_clients`
--

CREATE TABLE `cs_sub_clients` (
  `id` int(11) NOT NULL,
  `cs_client_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `staff_id` int(11) DEFAULT NULL,
  `kh_username_r` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `en_username_r` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `gender_r` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `dob_r` date DEFAULT NULL,
  `national_r` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `identify_card_num_r` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `identify_card_type_r` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `identify_card_dob_r` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `identify_card_by_r` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `kh_username_g` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `en_username_g` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `gender_g` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `dob_g` date DEFAULT NULL,
  `national_g` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `identify_card_num_g` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `identify_card_type_g` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `identify_card_dob_g` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `identify_card_by_g` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `kh_username_gr` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `en_username_gr` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `gender_gr` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `dob_gr` date DEFAULT NULL,
  `national_gr` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `identify_card_num_gr` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `identify_card_type_gr` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `identify_card_dob_gr` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `identify_card_by_gr` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `home_num` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `group_num` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `street_num` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `vilige` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `commune` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `district` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `province` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `upload_relate_doc` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cs_suppliers`
--

CREATE TABLE `cs_suppliers` (
  `id` int(11) NOT NULL,
  `name` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `email` varchar(150) CHARACTER SET latin1 DEFAULT NULL,
  `phone` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  `address` text CHARACTER SET latin1,
  `description` text CHARACTER SET latin1,
  `images` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cs-cds-request-item`
--
ALTER TABLE `cs-cds-request-item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cs-co-give-product-client`
--
ALTER TABLE `cs-co-give-product-client`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cs-deposit-setting`
--
ALTER TABLE `cs-deposit-setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cs-history-logs`
--
ALTER TABLE `cs-history-logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cs-instocks`
--
ALTER TABLE `cs-instocks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cs-interest-crdit-sell`
--
ALTER TABLE `cs-interest-crdit-sell`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cs-inventorys`
--
ALTER TABLE `cs-inventorys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cs-manager-branch-approval`
--
ALTER TABLE `cs-manager-branch-approval`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cs-payments`
--
ALTER TABLE `cs-payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cs-purchase-order-detail`
--
ALTER TABLE `cs-purchase-order-detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cs-purchase_order`
--
ALTER TABLE `cs-purchase_order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cs-sale-item`
--
ALTER TABLE `cs-sale-item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cs-sales`
--
ALTER TABLE `cs-sales`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cs-schedules`
--
ALTER TABLE `cs-schedules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cs-schedules-repayment-timesheet`
--
ALTER TABLE `cs-schedules-repayment-timesheet`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cs-schedules-timesheet`
--
ALTER TABLE `cs-schedules-timesheet`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cs_brand`
--
ALTER TABLE `cs_brand`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cs_categorys`
--
ALTER TABLE `cs_categorys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cs_clients`
--
ALTER TABLE `cs_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cs_currency`
--
ALTER TABLE `cs_currency`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cs_exchange_rate`
--
ALTER TABLE `cs_exchange_rate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cs_history_log`
--
ALTER TABLE `cs_history_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cs_items`
--
ALTER TABLE `cs_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cs_it_images`
--
ALTER TABLE `cs_it_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cs_payment_type`
--
ALTER TABLE `cs_payment_type`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `cs_penaty`
--
ALTER TABLE `cs_penaty`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cs_request_form`
--
ALTER TABLE `cs_request_form`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cs_suppliers`
--
ALTER TABLE `cs_suppliers`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cs-cds-request-item`
--
ALTER TABLE `cs-cds-request-item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cs-co-give-product-client`
--
ALTER TABLE `cs-co-give-product-client`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cs-deposit-setting`
--
ALTER TABLE `cs-deposit-setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cs-history-logs`
--
ALTER TABLE `cs-history-logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cs-instocks`
--
ALTER TABLE `cs-instocks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cs-interest-crdit-sell`
--
ALTER TABLE `cs-interest-crdit-sell`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cs-inventorys`
--
ALTER TABLE `cs-inventorys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cs-manager-branch-approval`
--
ALTER TABLE `cs-manager-branch-approval`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cs-payments`
--
ALTER TABLE `cs-payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cs-purchase-order-detail`
--
ALTER TABLE `cs-purchase-order-detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cs-purchase_order`
--
ALTER TABLE `cs-purchase_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cs-sale-item`
--
ALTER TABLE `cs-sale-item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cs-sales`
--
ALTER TABLE `cs-sales`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cs-schedules`
--
ALTER TABLE `cs-schedules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cs-schedules-repayment-timesheet`
--
ALTER TABLE `cs-schedules-repayment-timesheet`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cs-schedules-timesheet`
--
ALTER TABLE `cs-schedules-timesheet`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cs_brand`
--
ALTER TABLE `cs_brand`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cs_categorys`
--
ALTER TABLE `cs_categorys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cs_clients`
--
ALTER TABLE `cs_clients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cs_currency`
--
ALTER TABLE `cs_currency`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cs_exchange_rate`
--
ALTER TABLE `cs_exchange_rate`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cs_history_log`
--
ALTER TABLE `cs_history_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cs_items`
--
ALTER TABLE `cs_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cs_it_images`
--
ALTER TABLE `cs_it_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cs_payment_type`
--
ALTER TABLE `cs_payment_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cs_penaty`
--
ALTER TABLE `cs_penaty`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cs_request_form`
--
ALTER TABLE `cs_request_form`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cs_suppliers`
--
ALTER TABLE `cs_suppliers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
