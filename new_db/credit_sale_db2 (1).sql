-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: May 21, 2018 at 04:20 AM
-- Server version: 5.7.19
-- PHP Version: 7.0.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `credit_sale_db2`
--

-- --------------------------------------------------------

--
-- Table structure for table `cs-cds-request-item`
--

DROP TABLE IF EXISTS `cs-cds-request-item`;
CREATE TABLE IF NOT EXISTS `cs-cds-request-item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cds-request-id` int(11) DEFAULT NULL,
  `product-id` int(11) DEFAULT NULL,
  `barcode` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `price-total-of-pro` decimal(30,0) DEFAULT NULL,
  `price-of-deposit-product` decimal(30,0) DEFAULT NULL,
  `price-total-for-pay` decimal(30,0) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cs-co-give-item-client`
--

DROP TABLE IF EXISTS `cs-co-give-item-client`;
CREATE TABLE IF NOT EXISTS `cs-co-give-item-client` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `sale_id` int(11) NOT NULL DEFAULT '0',
  `cds_request_id` int(11) DEFAULT NULL,
  `manager-branch-approval-id` int(11) DEFAULT NULL,
  `staff_id` int(11) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  `approve_by` int(11) DEFAULT NULL,
  `date_give` date DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cs-deposit-setting`
--

DROP TABLE IF EXISTS `cs-deposit-setting`;
CREATE TABLE IF NOT EXISTS `cs-deposit-setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `number` decimal(30,0) DEFAULT NULL,
  `num_of_percens` decimal(10,0) DEFAULT NULL,
  `note` text CHARACTER SET latin1,
  `deleted` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cs-history-logs`
--

DROP TABLE IF EXISTS `cs-history-logs`;
CREATE TABLE IF NOT EXISTS `cs-history-logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `ip_log` varchar(50) DEFAULT NULL,
  `active` varchar(250) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `what_id` int(11) DEFAULT NULL,
  `method` varchar(30) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=108 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cs-history-logs`
--

INSERT INTO `cs-history-logs` (`id`, `user_id`, `ip_log`, `active`, `status`, `what_id`, `method`, `create_date`) VALUES
(1, 50, '::1', 'កែប្រែសិទ្ធិអ្នកប្រើប្រាស់', 3, 8, 'user group', '2018-04-07 03:04:18'),
(2, 50, '::1', 'លុបសិទ្ធិអ្នកប្រើប្រាស់', 2, 5, 'user group', '2018-04-07 03:04:04'),
(3, 50, '::1', 'លុបសិទ្ធិអ្នកប្រើប្រាស់', 2, 4, 'user group', '2018-04-07 03:04:06'),
(4, 50, '::1', 'លុបសិទ្ធិអ្នកប្រើប្រាស់', 2, 3, 'user group', '2018-04-07 03:04:09'),
(5, 50, '::1', 'លុបសិទ្ធិអ្នកប្រើប្រាស់', 2, 6, 'user group', '2018-04-07 03:04:11'),
(6, 50, '::1', 'បង្កើតអតិថិជនជាលក្ខណៈបុគ្គល', 2, 1, 'customer personal', '2018-04-19 08:04:07'),
(7, 50, '::1', 'បង្កើតអតិថិជនជាលក្ខណៈបុគ្គល', 2, 2, 'customer personal', '2018-04-19 09:04:33'),
(8, 50, '::1', 'បង្កើតអតិថិជនជាលក្ខណៈបុគ្គល', 2, 9, 'customer personal', '2018-04-20 10:04:06'),
(9, 50, '::1', 'បង្កើតអតិថិជនជាលក្ខណៈបុគ្គល', 2, 10, 'customer personal', '2018-04-20 10:04:49'),
(10, 50, '::1', 'កែប្រែសិទ្ធិអ្នកប្រើប្រាស់', 3, 8, 'user group', '2018-04-24 10:04:41'),
(11, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 9, 'customer personal', '2018-04-26 10:04:44'),
(12, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 9, 'customer personal', '2018-04-26 10:04:44'),
(13, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 9, 'customer personal', '2018-04-26 10:04:51'),
(14, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 9, 'customer personal', '2018-04-26 10:04:51'),
(15, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 9, 'customer personal', '2018-04-26 10:04:53'),
(16, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 9, 'customer personal', '2018-04-26 10:04:53'),
(17, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 9, 'customer personal', '2018-04-26 12:04:26'),
(18, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 9, 'customer personal', '2018-04-26 12:04:26'),
(19, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 9, 'customer personal', '2018-04-26 12:04:50'),
(20, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 9, 'customer personal', '2018-04-26 12:04:50'),
(21, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 9, 'customer personal', '2018-04-26 12:04:51'),
(22, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 9, 'customer personal', '2018-04-26 12:04:51'),
(23, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 9, 'customer personal', '2018-04-26 12:04:12'),
(24, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 9, 'customer personal', '2018-04-26 12:04:12'),
(25, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 10, 'customer personal', '2018-04-26 12:04:41'),
(26, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 10, 'customer personal', '2018-04-26 01:04:38'),
(27, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 9, 'customer personal', '2018-04-26 01:04:22'),
(28, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 9, 'customer personal', '2018-04-26 01:04:23'),
(29, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 10, 'customer personal', '2018-04-26 01:04:38'),
(30, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 10, 'customer personal', '2018-04-26 01:04:00'),
(31, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 9, 'customer personal', '2018-04-26 01:04:26'),
(32, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 9, 'customer personal', '2018-04-26 01:04:27'),
(33, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 9, 'customer personal', '2018-04-26 01:04:51'),
(34, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 10, 'customer personal', '2018-04-26 01:04:56'),
(35, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 10, 'customer personal', '2018-04-26 01:04:57'),
(36, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 10, 'customer personal', '2018-04-26 01:04:40'),
(37, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 10, 'customer personal', '2018-04-26 01:04:41'),
(38, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 10, 'customer personal', '2018-04-26 01:04:42'),
(39, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 9, 'customer personal', '2018-04-26 01:04:32'),
(40, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 9, 'customer personal', '2018-04-26 01:04:48'),
(41, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 9, 'customer personal', '2018-04-26 01:04:33'),
(42, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 10, 'customer personal', '2018-04-26 01:04:38'),
(43, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 10, 'customer personal', '2018-04-26 01:04:39'),
(44, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 9, 'customer personal', '2018-04-26 01:04:54'),
(45, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 9, 'customer personal', '2018-04-26 01:04:55'),
(46, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 10, 'customer personal', '2018-04-26 01:04:30'),
(47, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 9, 'customer personal', '2018-04-26 01:04:41'),
(48, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 9, 'customer personal', '2018-04-26 02:04:17'),
(49, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 10, 'customer personal', '2018-04-26 02:04:25'),
(50, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 9, 'customer personal', '2018-04-26 02:04:07'),
(51, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 9, 'customer personal', '2018-04-26 02:04:46'),
(52, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 10, 'customer personal', '2018-04-26 02:04:05'),
(53, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 9, 'customer personal', '2018-04-26 02:04:10'),
(54, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 10, 'customer personal', '2018-04-26 02:04:04'),
(55, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 2, 'customer personal', '2018-04-27 08:04:02'),
(56, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 9, 'customer personal', '2018-04-27 08:04:48'),
(57, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 2, 'customer personal', '2018-04-27 08:04:07'),
(58, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 1, 'customer personal', '2018-04-27 08:04:18'),
(59, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 1, 'customer personal', '2018-04-27 08:04:58'),
(60, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 3, 'customer personal', '2018-04-27 09:04:32'),
(61, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 5, 'customer personal', '2018-04-27 09:04:08'),
(62, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 4, 'customer personal', '2018-04-27 09:04:32'),
(63, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 6, 'customer personal', '2018-04-27 09:04:57'),
(64, 50, '::1', 'កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល', 2, 7, 'customer personal', '2018-04-27 09:04:44'),
(65, 50, '::1', 'បង្កើតសាខារថ្មី', 2, 17, 'user', '2018-04-30 01:04:36'),
(66, 50, '::1', 'បង្កើតសាខារថ្មី', 2, 18, 'user', '2018-04-30 01:04:38'),
(67, 50, '::1', 'បង្កើតសាខារថ្មី', 2, 19, 'user', '2018-04-30 01:04:36'),
(68, 50, '::1', 'បង្កើតសាខារថ្មី', 2, 20, 'user', '2018-04-30 01:04:30'),
(69, 50, '::1', 'បង្កើតសាខារថ្មី', 2, 21, 'user', '2018-05-01 04:05:06'),
(70, 50, '::1', 'បង្កើតសាខារថ្មី', 2, 22, 'user', '2018-05-01 04:05:21'),
(71, 50, '::1', 'កែរប្រែរព័ត៌មានសាខា', 2, 3, 'brand', '2018-05-01 04:05:18'),
(72, 50, '::1', 'កែរប្រែរព័ត៌មានសាខា', 2, 4, 'brand', '2018-05-01 04:05:07'),
(73, 50, '::1', 'កែរប្រែរព័ត៌មានសាខា', 2, 5, 'brand', '2018-05-01 04:05:31'),
(74, 50, '::1', 'បង្កើតសាខារថ្មី', 2, 23, 'user', '2018-05-01 04:05:55'),
(75, 50, '::1', 'លុបព័ត៌មានសាខា', 2, 3, 'brand', '2018-05-01 05:05:26'),
(76, 50, '::1', 'លុបព័ត៌មានសាខា', 2, 19, 'brand', '2018-05-01 05:05:41'),
(77, 50, '::1', 'លុបព័ត៌មានសាខា', 2, 20, 'brand', '2018-05-01 05:05:46'),
(78, 50, '::1', 'លុបព័ត៌មានសាខា', 2, 15, 'brand', '2018-05-01 05:05:55'),
(79, 50, '::1', 'បង្កើតប្រភេទរបស់ផលិតផល', 2, 1, 'Category', '2018-05-01 06:05:42'),
(80, 50, '::1', 'បង្កើតប្រភេទរបស់ផលិតផល', 2, 2, 'Category', '2018-05-01 06:05:10'),
(81, 50, '::1', 'បង្កើតប្រភេទរបស់ផលិតផល', 2, 3, 'Category', '2018-05-01 06:05:03'),
(82, 50, '::1', 'បង្កើតប្រភេទរបស់ផលិតផល', 2, 4, 'Category', '2018-05-01 06:05:43'),
(83, 50, '::1', 'បង្កើតប្រភេទរបស់ផលិតផល', 2, 5, 'Category', '2018-05-01 06:05:28'),
(84, 50, '::1', 'បង្កើតប្រភេទរបស់ផលិតផល', 2, 6, 'Category', '2018-05-01 06:05:57'),
(85, 50, '::1', 'បង្កើតប្រភេទរបស់ផលិតផល', 2, 7, 'Category', '2018-05-01 06:05:18'),
(86, 50, '::1', 'បង្កើតប្រភេទរបស់ផលិតផល', 2, 8, 'Category', '2018-05-01 06:05:47'),
(87, 50, '::1', 'បង្កើតប្រភេទរបស់ផលិតផល', 2, 9, 'Category', '2018-05-01 06:05:57'),
(88, 50, '::1', 'លុបព័ត៌មានប្រភេទរបស់ផលិតផល', 2, 9, 'Category', '2018-05-01 06:05:01'),
(89, 50, '::1', 'កែរប្រែរព័ត៌មានប្រភេទរបស់ផលិតផល', 2, 8, 'Category', '2018-05-01 06:05:18'),
(90, 50, '::1', 'ការបង្កើតអ្នកផ្គត់ផ្គង់', 2, 1, 'Category', '2018-05-01 12:05:31'),
(91, 50, '::1', 'ការបង្កើតអ្នកផ្គត់ផ្គង់', 2, 1, 'Category', '2018-05-01 12:05:03'),
(92, 50, '::1', 'ការបង្កើតអ្នកផ្គត់ផ្គង់', 2, 2, 'Category', '2018-05-01 12:05:02'),
(93, 50, '::1', 'ការបង្កើតអ្នកផ្គត់ផ្គង់', 2, 3, 'Category', '2018-05-01 12:05:21'),
(94, 50, '::1', 'ការបង្កើតអ្នកផ្គត់ផ្គង់', 2, 4, 'Category', '2018-05-01 12:05:46'),
(95, 50, '::1', 'ការបង្កើតអ្នកផ្គត់ផ្គង់', 2, 5, 'Category', '2018-05-01 12:05:11'),
(96, 50, '::1', 'ការបង្កើតអ្នកផ្គត់ផ្គង់', 2, 6, 'Category', '2018-05-01 12:05:38'),
(97, 50, '::1', 'ការបង្កើតអ្នកផ្គត់ផ្គង់', 2, 7, 'Category', '2018-05-01 12:05:04'),
(98, 50, '::1', 'ការកែប្រែរព័ត៌មានអ្នកផ្គត់ផ្គង់', 2, 4, 'Category', '2018-05-01 01:05:23'),
(99, 50, '::1', 'លុបព័ត៌មានអ្នកផ្គត់ផ្គង់', 2, 7, 'brand', '2018-05-01 01:05:52'),
(100, 50, '::1', 'លុបព័ត៌មានអ្នកផ្គត់ផ្គង់', 2, 6, 'brand', '2018-05-01 01:05:15'),
(101, 50, '::1', 'ការកែប្រែរព័ត៌មានអ្នកផ្គត់ផ្គង់', 2, 5, 'Category', '2018-05-01 01:05:11'),
(102, 50, '::1', 'ការបង្កើតអ្នកផ្គត់ផ្គង់', 2, 8, 'Category', '2018-05-01 01:05:04'),
(103, 50, '::1', 'កែរប្រែរព័ត៌មានប្រភេទរបស់ផលិតផល', 2, 8, 'Category', '2018-05-18 09:05:35'),
(104, 50, '::1', 'ការបង្កើតផលិតផលថ្មី', 2, 3, 'items', '2018-05-18 09:05:42'),
(105, 50, '::1', 'ការកែប្រែរព័ត៌មានផលិតផល', 2, 3, 'items', '2018-05-18 10:05:46'),
(106, 50, '::1', 'ការកែប្រែរព័ត៌មានផលិតផល', 2, 3, 'items', '2018-05-18 10:05:23'),
(107, 50, '::1', 'ការបង្កើតផលិតផលថ្មី', 2, 4, 'items', '2018-05-18 10:05:34');

-- --------------------------------------------------------

--
-- Table structure for table `cs-instocks`
--

DROP TABLE IF EXISTS `cs-instocks`;
CREATE TABLE IF NOT EXISTS `cs-instocks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `size` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `qty` decimal(30,0) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  `date_include` date DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cs-interest-crdit-sell`
--

DROP TABLE IF EXISTS `cs-interest-crdit-sell`;
CREATE TABLE IF NOT EXISTS `cs-interest-crdit-sell` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `rate-name` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `size_money_from` decimal(30,0) DEFAULT NULL,
  `size_money_to` decimal(30,0) DEFAULT NULL,
  `day_rate_villige` decimal(30,0) DEFAULT NULL,
  `weekly_rate_villige` decimal(30,0) DEFAULT NULL,
  `two_weekly_rate_villige` decimal(30,0) DEFAULT NULL,
  `monthly_rate_villige` decimal(30,0) DEFAULT NULL,
  `day_rate_brand` decimal(30,0) DEFAULT NULL,
  `weekly_rate_brand` decimal(30,0) DEFAULT NULL,
  `two_weekly_brand` decimal(30,0) DEFAULT NULL,
  `monthly_rate_brand` decimal(30,0) DEFAULT NULL,
  `duration` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `methot description` text CHARACTER SET latin1,
  `deleted` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cs-inventorys`
--

DROP TABLE IF EXISTS `cs-inventorys`;
CREATE TABLE IF NOT EXISTS `cs-inventorys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) DEFAULT '0',
  `branch_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `comment` text,
  `status_product` int(11) DEFAULT NULL,
  `status_transation` varchar(11) DEFAULT NULL,
  `qty` float(30,2) DEFAULT '0.00',
  `size` varchar(60) DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cs-inventorys`
--

INSERT INTO `cs-inventorys` (`id`, `item_id`, `branch_id`, `user_id`, `comment`, `status_product`, `status_transation`, `qty`, `size`, `deleted`, `created_at`, `updated_at`) VALUES
(1, 3, 2, 50, NULL, NULL, 'add new', 5.00, NULL, NULL, NULL, '2018-05-18 10:34:22'),
(2, 4, 2, 50, NULL, NULL, 'add new', 10.00, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cs-manager-branch-approval`
--

DROP TABLE IF EXISTS `cs-manager-branch-approval`;
CREATE TABLE IF NOT EXISTS `cs-manager-branch-approval` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `staff_id` int(11) DEFAULT NULL,
  `manager_brand_id` int(11) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `cds_request_id` int(11) DEFAULT NULL,
  `interest_cds_id` int(11) DEFAULT NULL,
  `num_of_pro` decimal(30,0) DEFAULT NULL,
  `is_agree` int(11) DEFAULT NULL,
  `discount` decimal(30,0) DEFAULT NULL,
  `deposit_pro_percen_of_price_product` decimal(30,0) DEFAULT NULL,
  `deposit_pro_num` decimal(30,0) DEFAULT NULL,
  `deposit_pro_word` int(11) DEFAULT NULL,
  `price_total_num` decimal(30,0) DEFAULT NULL,
  `price_total_word` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `price_for_pay` decimal(30,0) DEFAULT NULL,
  `money_type` int(11) DEFAULT NULL,
  `duration_pay_money` int(11) DEFAULT NULL,
  `duration_pay_money_type` int(11) DEFAULT NULL,
  `date_for_payments` date DEFAULT NULL,
  `date_approval` date DEFAULT NULL,
  `date_give_product` date DEFAULT NULL,
  `created_approval_date` date DEFAULT NULL,
  `update_approval_date` date DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cs-payments`
--

DROP TABLE IF EXISTS `cs-payments`;
CREATE TABLE IF NOT EXISTS `cs-payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sale_id` int(11) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `is_direct_payment` int(11) DEFAULT NULL,
  `cds_request_id` int(11) DEFAULT NULL,
  `manager-branch-approval-id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `staff_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `place_for_pay` int(11) DEFAULT NULL,
  `payment_amount` decimal(30,0) DEFAULT NULL,
  `sub_payment_amount` decimal(30,0) DEFAULT NULL,
  `dute__payment_amount` decimal(30,0) DEFAULT NULL,
  `payment_date` date DEFAULT NULL,
  `payment_for_date` date DEFAULT NULL,
  `penalty` decimal(30,0) DEFAULT NULL,
  `remaining` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `date_created` date DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cs-purchase-order-detail`
--

DROP TABLE IF EXISTS `cs-purchase-order-detail`;
CREATE TABLE IF NOT EXISTS `cs-purchase-order-detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product-id` int(11) DEFAULT NULL,
  `cs-purchase_order_id` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cs-purchase_order`
--

DROP TABLE IF EXISTS `cs-purchase_order`;
CREATE TABLE IF NOT EXISTS `cs-purchase_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `supplier_id` int(11) DEFAULT NULL,
  `branch-id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `stuff_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  `order-date` date DEFAULT NULL,
  `order-modify-date` date DEFAULT NULL,
  `created-at` datetime DEFAULT NULL,
  `update-at` datetime DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cs-sale-item`
--

DROP TABLE IF EXISTS `cs-sale-item`;
CREATE TABLE IF NOT EXISTS `cs-sale-item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sale_id` int(11) DEFAULT NULL,
  `barcode` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `total_price` decimal(30,0) DEFAULT NULL,
  `total_of_deposit` decimal(10,0) DEFAULT NULL,
  `total_price_payment` decimal(10,0) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cs-sales`
--

DROP TABLE IF EXISTS `cs-sales`;
CREATE TABLE IF NOT EXISTS `cs-sales` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) DEFAULT NULL,
  `cds_request_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `staff_id` int(11) DEFAULT NULL,
  `manager-branch-approval-id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `total_due_amout` decimal(30,0) DEFAULT NULL,
  `total_amount` decimal(30,0) DEFAULT NULL,
  `sale_status_for` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `discount` decimal(30,0) DEFAULT NULL,
  `is_oncredit` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  `remaining` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `method` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `description` text CHARACTER SET latin1,
  `deleted` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cs-schedules`
--

DROP TABLE IF EXISTS `cs-schedules`;
CREATE TABLE IF NOT EXISTS `cs-schedules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client-id` int(11) DEFAULT NULL,
  `sale_id` int(11) DEFAULT '0',
  `branch-id` int(11) DEFAULT NULL,
  `cds-request-id` int(11) DEFAULT NULL,
  `manager-branch-approval-id` int(11) DEFAULT NULL,
  `user-id` int(11) DEFAULT NULL,
  `staff-id` int(11) DEFAULT NULL,
  `total-price-of-pro` decimal(30,0) DEFAULT NULL,
  `total-price-deposit-of-pro` decimal(30,0) DEFAULT NULL,
  `total-price-payment-pro` decimal(30,0) DEFAULT NULL,
  `money_type` int(11) DEFAULT NULL,
  `interest-cds-id` int(11) DEFAULT NULL,
  `qty-pro` int(11) DEFAULT NULL,
  `date_for_payments` date DEFAULT NULL,
  `date_approval` date DEFAULT NULL,
  `date_give_product` date DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `status-for-pay` int(11) DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  `is_give` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cs-schedules-repayment-timesheet`
--

DROP TABLE IF EXISTS `cs-schedules-repayment-timesheet`;
CREATE TABLE IF NOT EXISTS `cs-schedules-repayment-timesheet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `schedule-id` int(11) DEFAULT NULL,
  `branch-id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `staff_id` int(11) DEFAULT NULL,
  `schedules-titmesheet-id` int(11) DEFAULT NULL,
  `available-total-pay-cost` decimal(30,0) DEFAULT NULL,
  `available-total-pay-interest` decimal(30,0) DEFAULT NULL,
  `available-total-payment` decimal(30,0) DEFAULT NULL,
  `available-total-pay-cost-owe` decimal(30,0) DEFAULT NULL,
  `date-payment` date DEFAULT NULL,
  `available-date-payment` date DEFAULT NULL,
  `lavel-payment` int(11) DEFAULT NULL,
  `date-late-pay` date DEFAULT NULL,
  `penalty-pay` int(11) DEFAULT NULL,
  `available-remain` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `status-for-pay` int(11) DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  `note` text,
  `deleted` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cs-schedules-timesheet`
--

DROP TABLE IF EXISTS `cs-schedules-timesheet`;
CREATE TABLE IF NOT EXISTS `cs-schedules-timesheet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `schedule-id` int(11) DEFAULT NULL,
  `total-pay-cost` decimal(30,0) DEFAULT NULL,
  `total-pay-interest` decimal(30,0) DEFAULT NULL,
  `total-payment` decimal(30,0) DEFAULT NULL,
  `total-pay-cost-owe` decimal(30,0) DEFAULT NULL,
  `date-payment` date DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `status-for-pay` int(11) DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  `description` text CHARACTER SET latin1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cs_brand`
--

DROP TABLE IF EXISTS `cs_brand`;
CREATE TABLE IF NOT EXISTS `cs_brand` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `code` varchar(30) DEFAULT NULL,
  `description` text CHARACTER SET utf8,
  `deleted` int(11) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cs_brand`
--

INSERT INTO `cs_brand` (`id`, `name`, `code`, `description`, `deleted`, `created_at`, `updated_at`) VALUES
(4, 'HP', 'HP', 'professional hey guy are you ok', 0, NULL, '2018-05-01 04:38:07'),
(5, 'Apple', 'APL', 'NPRO dadse', 0, NULL, '2018-05-01 04:44:31'),
(6, 'SAMSUNG', 'SS', 'Live for life', 0, NULL, NULL),
(7, 'LG', 'LG', 'Good Life', 0, NULL, NULL),
(8, 'Vivo', 'VV', 'Smart Left', 0, NULL, NULL),
(9, 'Huwie', 'HW', 'good', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cs_categorys`
--

DROP TABLE IF EXISTS `cs_categorys`;
CREATE TABLE IF NOT EXISTS `cs_categorys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `description` text CHARACTER SET latin1,
  `status` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cs_categorys`
--

INSERT INTO `cs_categorys` (`id`, `name`, `description`, `status`, `user_id`, `deleted`, `created_at`, `updated_at`) VALUES
(1, 'Coffe', 'With ICE', 1, 50, 0, '2018-05-01 06:05:42', NULL),
(2, 'Drink', 'with ICE', 1, 50, 0, '2018-05-01 06:05:10', NULL),
(3, 'Dinner', 'Food', 1, 50, 0, '2018-05-01 06:05:03', NULL),
(4, 'Lunch', 'Food for Lunch Time', 1, 50, 0, '2018-05-01 06:05:43', NULL),
(5, 'Breakfirst', 'food for break first', 1, 50, 0, '2018-05-01 06:05:28', NULL),
(6, 'Supper', 'for midnight', 1, 50, 0, '2018-05-01 06:05:56', NULL),
(7, 'snack', 'cake', 1, 50, 0, '2018-05-01 06:05:18', NULL),
(8, 'Phone', 'Phone', 1, 50, 0, '2018-05-18 09:05:35', '2018-05-18 09:23:35');

-- --------------------------------------------------------

--
-- Table structure for table `cs_clients`
--

DROP TABLE IF EXISTS `cs_clients`;
CREATE TABLE IF NOT EXISTS `cs_clients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_code` varchar(60) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_type_id` int(11) NOT NULL DEFAULT '0',
  `kh_name_first` varchar(60) DEFAULT NULL,
  `kh_name_last` varchar(60) DEFAULT NULL,
  `en_name_first` varchar(60) DEFAULT NULL,
  `en_name_last` varchar(60) DEFAULT NULL,
  `kh_username` varchar(60) DEFAULT NULL,
  `en_username` varchar(60) DEFAULT NULL,
  `gender` varchar(60) DEFAULT NULL,
  `dob` date DEFAULT '0000-00-00',
  `nationality` varchar(60) DEFAULT NULL,
  `identify_num` varchar(60) DEFAULT NULL,
  `identify_type` varchar(60) DEFAULT NULL,
  `identify_by` varchar(60) DEFAULT NULL,
  `home_num` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `group_num` varchar(60) DEFAULT NULL,
  `street_num` varchar(60) DEFAULT NULL,
  `vilige` varchar(60) DEFAULT NULL,
  `commune` varchar(60) DEFAULT NULL,
  `district` varchar(60) DEFAULT NULL,
  `province` varchar(60) DEFAULT NULL,
  `phone` varchar(60) DEFAULT NULL,
  `job` varchar(60) DEFAULT NULL,
  `place_job` varchar(60) DEFAULT NULL,
  `upload_relate_document` varchar(60) DEFAULT NULL,
  `staff_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `client_status` int(11) DEFAULT NULL,
  `description` text,
  `latitude` varchar(255) DEFAULT NULL,
  `longitude` varchar(255) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  `larvel_client` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cs_clients`
--

INSERT INTO `cs_clients` (`id`, `client_code`, `branch_id`, `user_id`, `client_type_id`, `kh_name_first`, `kh_name_last`, `en_name_first`, `en_name_last`, `kh_username`, `en_username`, `gender`, `dob`, `nationality`, `identify_num`, `identify_type`, `identify_by`, `home_num`, `group_num`, `street_num`, `vilige`, `commune`, `district`, `province`, `phone`, `job`, `place_job`, `upload_relate_document`, `staff_id`, `status`, `client_status`, `description`, `latitude`, `longitude`, `deleted`, `larvel_client`, `created_at`, `updated_at`) VALUES
(1, NULL, 3, 50, 2, 'ឈឿន', 'សុជាតិ', 'Chhoeun', 'Socheat', 'ឈឿន សុជាតិ', 'Chhoeun Socheat', 'F', '1992-01-01', '1', '125896374', '1', '1', '45', '77', '88', 'ភូមិ ១', 'កំពង់ស្វាយ', '៧មករា', 'ខេត្តកំពង់ធំ', '016-707-044', 'Web Developer', 'Phnom Penh', 'kitchen_adventurer_caramel.jpg', NULL, NULL, 0, 'JUST FOR TESTING', NULL, NULL, 0, 0, '2018-04-27 08:04:58', '2018-04-27 08:25:58'),
(2, NULL, 2, 50, 1, 'ការិ  Provin', 'រិវិកា', 'Kari Provin', 'Rivika', 'ការិ  Provin រិវិកា', 'Kari Provin Rivika', 'F', '1945-07-05', '1', '025975214', '1', '1', '258', '89', 'លំ', 'ភូមិ ១', 'កំពង់ស្វាយ', '៧មករា', 'ខេត្តកំពង់ធំ', '016-957-852', 'គ្រូបង្រៀន', 'ភ្នំពេញ', 'SP4_Typecover_Blue_AngleView_V2.jpg', NULL, NULL, 0, 'Just for test', NULL, NULL, 0, 0, '2018-04-27 08:04:07', '2018-04-27 08:23:07'),
(8, NULL, 2, 50, 1, 'ការិ', 'រិវិកា6', 'Kari', 'Rivika', 'ការិ រិវិកា', 'Kari Rivika', 'F', '1945-07-05', '1', '025975214', '1', '1', '258', '47', 'លំ', 'តាប៉ោង', 'កំពង់ស្វាយ', 'កំពង់ស្វាយ', 'ខេត្តកំពង់ធំ', '016-957-852', 'គ្រូបង្រៀន', 'ភ្នំពេញ', 'SP4_Typecover_Blue_AngleView_V2.jpg', NULL, NULL, 0, 'Just for test', NULL, NULL, 0, 0, '2018-04-19 09:04:33', NULL),
(9, NULL, 3, 50, 2, 'រ័ត្ន', 'ប្រតិកា', 'Roth', 'Brotika', 'រ័ត្ន ប្រតិកា', 'Roth Brotika', 'F', '1992-07-11', '1', '598', '4', '1', '96', '23', '85', 'ភូមិ ១', 'កំពង់ស្វាយ', '៧មករា', 'ខេត្តកំពង់ធំ', '023-647-000', 'Web Developer', 'Phnom Penh', 'blue-flower.jpg', NULL, NULL, 0, 'This is version for edit', '2596', '1475', 0, 0, '2018-04-27 08:04:48', '2018-04-27 08:17:48'),
(10, NULL, 3, 50, 2, 'រិទ', 'និនួន', 'Rit', 'Ninoun', 'រិទ និនួន', 'Rit Ninoun', 'F', '1992-06-11', '2', '0136699', 'protocol', '2', '14', '8', '2002', 'ភូមិ ១', 'អូរឫស្សីទី ៣', '៧មករា', 'រាជធានីភ្នំពេញ', '026-974-244', 'Web Developer', 'Phnom Penh', '8.jpg', NULL, NULL, 0, 'just test for you  FGDSGDFG', '7', '8', 0, 0, '2018-04-26 02:04:04', '2018-04-26 14:35:04');

-- --------------------------------------------------------

--
-- Table structure for table `cs_clients_type`
--

DROP TABLE IF EXISTS `cs_clients_type`;
CREATE TABLE IF NOT EXISTS `cs_clients_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `dispay_name` varchar(60) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cs_clients_type`
--

INSERT INTO `cs_clients_type` (`id`, `name`, `dispay_name`) VALUES
(1, 'pay_by_credit', 'Pay by Credit'),
(2, 'direct_payment', 'Direct Payment');

-- --------------------------------------------------------

--
-- Table structure for table `cs_currency`
--

DROP TABLE IF EXISTS `cs_currency`;
CREATE TABLE IF NOT EXISTS `cs_currency` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `value_option` varchar(255) DEFAULT NULL,
  `note` text,
  `deleted` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cs_exchange_rate`
--

DROP TABLE IF EXISTS `cs_exchange_rate`;
CREATE TABLE IF NOT EXISTS `cs_exchange_rate` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `real` int(11) NOT NULL,
  `us` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cs_history_log`
--

DROP TABLE IF EXISTS `cs_history_log`;
CREATE TABLE IF NOT EXISTS `cs_history_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip_log` varchar(60) DEFAULT NULL,
  `active` text,
  `user_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL COMMENT '1 => view , 2 => create , 3 => update , 4 => delete , 5 => search',
  `create_date` datetime DEFAULT NULL,
  `what_id` int(11) DEFAULT NULL,
  `method` varchar(60) DEFAULT NULL,
  `date_login` datetime DEFAULT NULL,
  `date_logout` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cs_image_data`
--

DROP TABLE IF EXISTS `cs_image_data`;
CREATE TABLE IF NOT EXISTS `cs_image_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image_value` varchar(255) DEFAULT NULL,
  `meta_value` varchar(255) DEFAULT NULL,
  `dis` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cs_image_data`
--

INSERT INTO `cs_image_data` (`id`, `image_value`, `meta_value`, `dis`) VALUES
(4, '4.jpg', 'cs_client_id', 2),
(5, '5.jpg', 'cs_client_id', 2),
(6, '101-1024x683.jpg', 'cs_client_id', 2),
(8, 'blue-flower.jpg', 'cs_client_id', 0),
(12, 'raindrops.jpg', 'cs_client_id', 0),
(26, '3.jpg', 'cs_client_id', 9),
(27, '2.jpg', 'cs_client_id', 9),
(28, '7.jpg', 'cs_client_id', 9),
(29, '3.jpg', 'cs_client_id', 9),
(30, '9.jpg', 'cs_client_id', 9),
(31, '7.jpg', 'cs_client_id', 9),
(32, '9.jpg', 'cs_client_id', 9),
(33, '11.jpg', 'cs_client_id', 10),
(34, 'b1.jpg', 'cs_client_id', 10),
(35, 'b2.jpg', 'cs_client_id', 10),
(36, 'kitchen_adventurer_caramel.jpg', 'cs_client_id', 1),
(37, 'kitchen_adventurer_cheesecake_brownie.jpg', 'cs_client_id', 1),
(38, 'kitchen_adventurer_donut.jpg', 'cs_client_id', 1),
(39, 'kitchen_adventurer_lemon.jpg', 'cs_client_id', 1);

-- --------------------------------------------------------

--
-- Table structure for table `cs_items`
--

DROP TABLE IF EXISTS `cs_items`;
CREATE TABLE IF NOT EXISTS `cs_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) DEFAULT NULL,
  `add_number_id` int(11) DEFAULT NULL,
  `item_bacode` varchar(60) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `brand_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT '0',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `qty` double(30,2) DEFAULT '0.00',
  `tax_include` double(30,2) DEFAULT '0.00',
  `commission_fixed` double(30,2) DEFAULT '0.00',
  `override_default_tax` int(11) DEFAULT '0',
  `override_default_commission` int(11) DEFAULT '0',
  `commission_give` double(30,2) DEFAULT '0.00',
  `cost_price` double(30,2) DEFAULT '0.00',
  `sell_price` double(30,2) DEFAULT '0.00',
  `promo_price` double(30,2) DEFAULT '0.00',
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `discount` double(30,2) DEFAULT '0.00',
  `pro_image` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `size` varchar(60) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `is_service` int(11) DEFAULT NULL,
  `quality` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `recode_level` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cs_items`
--

INSERT INTO `cs_items` (`id`, `item_id`, `add_number_id`, `item_bacode`, `category_id`, `brand_id`, `branch_id`, `user_id`, `supplier_id`, `name`, `qty`, `tax_include`, `commission_fixed`, `override_default_tax`, `override_default_commission`, `commission_give`, `cost_price`, `sell_price`, `promo_price`, `start_date`, `end_date`, `discount`, `pro_image`, `size`, `description`, `is_service`, `quality`, `status`, `recode_level`, `deleted`, `created_at`, `updated_at`) VALUES
(3, NULL, 3, '0000000003', 8, 6, 2, 50, 1, 'Samsung Galaxy S', 0.00, NULL, 3.00, 1, 1, 0.00, 300.00, 400.00, 350.00, '2018-05-18', '2018-06-09', NULL, 'i5.jpg', NULL, 'Samsung Galaxy S', NULL, NULL, 1, NULL, 0, '2018-05-18 10:05:21', '2018-05-18 10:34:21'),
(4, NULL, 4, '0000000004', 8, 7, 2, 50, 3, 'LG G3 D885', 0.00, NULL, 0.00, 1, 1, 0.10, 117.00, 200.00, 150.00, '2018-05-18', '2018-06-09', NULL, 'i2.jpg', NULL, 'LG G3 D885', NULL, NULL, 1, NULL, 0, '2018-05-18 10:05:34', '2018-05-18 10:37:34');

-- --------------------------------------------------------

--
-- Table structure for table `cs_items_tax`
--

DROP TABLE IF EXISTS `cs_items_tax`;
CREATE TABLE IF NOT EXISTS `cs_items_tax` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `name` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `percent` double(10,2) DEFAULT '0.00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cs_items_tax`
--

INSERT INTO `cs_items_tax` (`id`, `item_id`, `name`, `percent`) VALUES
(20, 3, 'Tax 5', 0.15),
(19, 3, 'Tax 4', 0.14),
(18, 3, 'Tax 3', 0.13),
(17, 3, 'Tax 2', 0.12),
(16, 3, 'Tax 1', 0.11),
(21, 4, 'Tax 1', 0.10),
(22, 4, 'Tax 2', 0.20);

-- --------------------------------------------------------

--
-- Table structure for table `cs_it_images`
--

DROP TABLE IF EXISTS `cs_it_images`;
CREATE TABLE IF NOT EXISTS `cs_it_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `image` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cs_payment_type`
--

DROP TABLE IF EXISTS `cs_payment_type`;
CREATE TABLE IF NOT EXISTS `cs_payment_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  `display_name` varchar(60) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`id`),
  KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cs_penaty`
--

DROP TABLE IF EXISTS `cs_penaty`;
CREATE TABLE IF NOT EXISTS `cs_penaty` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `late_form` int(11) NOT NULL,
  `at_late` int(11) NOT NULL,
  `color` varchar(50) NOT NULL,
  `note` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cs_request_form`
--

DROP TABLE IF EXISTS `cs_request_form`;
CREATE TABLE IF NOT EXISTS `cs_request_form` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) DEFAULT NULL,
  `sale_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `staff_id` int(11) DEFAULT NULL,
  `barcord` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `prices_total_num` decimal(30,0) DEFAULT NULL,
  `prices_totalword` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `price_for_pay` decimal(30,0) DEFAULT NULL,
  `money_type` int(11) DEFAULT NULL,
  `duration_pay_money` int(11) DEFAULT NULL,
  `duration_pay_money_type` int(11) DEFAULT NULL,
  `date_for_payments` date DEFAULT NULL,
  `date_create_request` date DEFAULT NULL,
  `upload_relate_doc` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `is_agree` int(11) DEFAULT NULL,
  `is_give` int(11) DEFAULT NULL,
  `is_break` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cs_sub_clients`
--

DROP TABLE IF EXISTS `cs_sub_clients`;
CREATE TABLE IF NOT EXISTS `cs_sub_clients` (
  `id` int(11) NOT NULL,
  `cs_client_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `staff_id` int(11) DEFAULT NULL,
  `kh_username_r` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `en_username_r` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `gender_r` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `dob_r` date DEFAULT NULL,
  `national_r` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `identify_card_num_r` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `identify_card_type_r` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `identify_card_dob_r` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `identify_card_by_r` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `kh_username_g` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `en_username_g` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `gender_g` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `dob_g` date DEFAULT NULL,
  `national_g` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `identify_card_num_g` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `identify_card_type_g` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `identify_card_dob_g` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `identify_card_by_g` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `kh_username_gr` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `en_username_gr` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `gender_gr` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `dob_gr` date DEFAULT NULL,
  `national_gr` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `identify_card_num_gr` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `identify_card_type_gr` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `identify_card_dob_gr` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `identify_card_by_gr` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `home_num` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `group_num` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `street_num` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `vilige` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `commune` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `district` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `province` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `upload_relate_doc` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cs_suppliers`
--

DROP TABLE IF EXISTS `cs_suppliers`;
CREATE TABLE IF NOT EXISTS `cs_suppliers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(255) DEFAULT NULL,
  `name` varchar(60) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `description` text,
  `images` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `address1` text,
  `address2` text,
  `city` varchar(255) DEFAULT NULL,
  `state_or_province` varchar(255) DEFAULT NULL,
  `zip_code` varchar(255) DEFAULT NULL,
  `country` varchar(60) DEFAULT NULL,
  `account_number` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cs_suppliers`
--

INSERT INTO `cs_suppliers` (`id`, `company_name`, `name`, `email`, `phone`, `description`, `images`, `user_id`, `status`, `address1`, `address2`, `city`, `state_or_province`, `zip_code`, `country`, `account_number`, `created_at`, `updated_at`, `deleted`) VALUES
(1, 'TITB', 'socheat chhoeun', 'socheatit1992@gmail.com', '085218806', 'hello Cambodia', 'error_img.png', 50, 1, 'st.2002 , No .156  teok thla , sensok', '2002', 'Phnom Penh', 'Phnom Penh', '12102', 'Cambodia', '013265478', '2018-05-01 12:05:03', NULL, 0),
(2, 'TITB', 'Rot Tana', 'rottana@gmail.com', '01236974', 'hwlloe', '001.jpg', 50, 1, 'st.2002 , No .156  teok thla , sensok', '2002', 'Phnom Penh', 'Phnom Penh', '12102', 'Cambodia', '1455887774', '2018-05-01 12:05:02', NULL, 0),
(3, 'TITB', 'Rata vasa', 'ratavasa@gmail.com', '0125885336', 'monthpro', '004.jpg', 50, 1, 'st.2002 , No .156  teok thla , sensok', '2002', 'Phnom Penh', 'Phnom Penh', '12102', 'Cambodia', '0362584788', '2018-05-01 12:05:21', NULL, 0),
(4, 'LG Brono', 'VS LF', 'vslf@gmail.com', '0258476300', 'nonthteg ddd', '009.jpg', 50, 1, 'st.2002 , No .156  teok thla , sensok', '2002', 'Phnom Penh', 'Phnom Penh', '12102', 'Cambodia', '365898900', '2018-05-01 01:05:22', '2018-05-01 13:22:22', 0),
(5, 'TITLB', 'profile', 'profile@gmail.com', '085218806', 'dsaeae', '013.jpg', 50, 1, 'st.2002 , No .156  teok thla , sensok', '2002', 'Phnom Penh', 'Phnom Penh', '12102', 'Cambodia', '0', '2018-05-01 01:05:11', '2018-05-01 13:29:11', 0),
(8, 'TITB1', 'NO', 'notitb@gmail.com', '85218806', 'ho', '', 50, 1, 'st.2002 , No .156  teok thla , sensok', '2002', 'Phnom Penh', 'Phnom Penh', '12102', 'Cambodia', '25963457', '2018-05-01 01:05:04', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
CREATE TABLE IF NOT EXISTS `groups` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `groups_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'administractor', 'Administractor', 'Can Manage In this App ', NULL, '2018-01-02 15:17:58'),
(2, 'sub-administractor', 'Sub Administractor', 'Can Create and Edite and View', NULL, '2017-05-18 14:38:26'),
(7, 'Supper User', 'Supper User', 'all', NULL, '2017-07-13 16:03:43'),
(8, 'developer', 'Developer', 'Developer', NULL, '2018-04-24 03:44:39');

-- --------------------------------------------------------

--
-- Table structure for table `mfi_branch`
--

DROP TABLE IF EXISTS `mfi_branch`;
CREATE TABLE IF NOT EXISTS `mfi_branch` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `brand_name` varchar(60) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `brand_name_short` varchar(255) NOT NULL,
  `brand_phone` varchar(60) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `brand_email` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `brand_upload_image` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `brand_dis` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `brand_address` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `website` varchar(60) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mfi_branch`
--

INSERT INTO `mfi_branch` (`id`, `brand_name`, `brand_name_short`, `brand_phone`, `brand_email`, `brand_upload_image`, `brand_dis`, `brand_address`, `website`, `deleted`, `created_at`, `updated_at`) VALUES
(2, 'ភំ្នពេញ', 'PN', '010 35 77 22 / 031 482 7777', 'info@kasp.finance', '', 'ភ្នំពេញ', '#62R, St 62 Toul kok village, Sang khat Toul Sangke, Rousey Keo, PhnomPenh', '', 0, '2017-10-04 01:05:50', '0000-00-00 00:00:00'),
(3, 'អង្គតាសោម', 'TK', '010 36 77 22 / 031 592 777 7', 'info@kasp.finance', '', 'អង្គតាសោម', 'Angtasom Commune, Trangkork District, Takeo Privince', '', 0, '2017-12-15 01:38:42', '2016-11-03 03:31:39'),
(4, 'ស្គន់', 'KN', '010 57 77 22 / 088 219 777 7', 'info@kasp.finance', '', 'សាខា ស្គន់', 'Skun Village, So Tip Commune, Cheoung Prey District, Kampong Cham Province', '', 1, '2018-04-30 06:20:46', '2018-01-18 22:15:09');

-- --------------------------------------------------------

--
-- Table structure for table `mfi_group_permission`
--

DROP TABLE IF EXISTS `mfi_group_permission`;
CREATE TABLE IF NOT EXISTS `mfi_group_permission` (
  `group_id` int(10) DEFAULT NULL,
  `permission_id` int(10) UNSIGNED DEFAULT NULL,
  KEY `group_id` (`group_id`,`permission_id`),
  KEY `group_id_2` (`group_id`),
  KEY `permission_id` (`permission_id`),
  KEY `group_id_3` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mfi_group_permission`
--

INSERT INTO `mfi_group_permission` (`group_id`, `permission_id`) VALUES
(8, 25),
(8, 26),
(8, 27),
(8, 28),
(8, 29),
(8, 30),
(8, 31),
(8, 32),
(8, 33),
(8, 34),
(8, 49),
(8, 50),
(8, 51),
(8, 52),
(8, 53),
(8, 55),
(8, 56),
(8, 57),
(8, 58),
(8, 59),
(8, 64),
(8, 65),
(8, 66),
(8, 68),
(8, 69),
(8, 70),
(8, 71),
(8, 72),
(8, 73),
(8, 74),
(8, 75),
(8, 76),
(8, 77),
(8, 78);

-- --------------------------------------------------------

--
-- Table structure for table `mfi_permission`
--

DROP TABLE IF EXISTS `mfi_permission`;
CREATE TABLE IF NOT EXISTS `mfi_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `display_name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `module` varchar(66) NOT NULL,
  `app_setting` varchar(30) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mfi_permission`
--

INSERT INTO `mfi_permission` (`id`, `name`, `display_name`, `description`, `module`, `app_setting`, `updated_at`) VALUES
(25, 'list-user-groups', 'List User Group', 'View List User groups', 'user-group', NULL, NULL),
(26, 'create-user-groups', 'Create User Group', 'You Can Create User groups', 'user-group', NULL, NULL),
(27, 'edit-user-groups', 'Edit User Group', 'You Can Edit User groups', 'user-group', NULL, NULL),
(28, 'Delete-user-groups', 'Delete User Group', 'You Can Delete User groups', 'user-group', NULL, NULL),
(29, 'list-client-groups', 'List Client Group', 'View List all groups', 'client-group', NULL, NULL),
(30, 'create-client-group', 'Create Client Group', 'Can Create Client Group', 'client-group', NULL, NULL),
(31, 'edit-client-group', 'Edit CLient Group', 'Can Edit CLient Group', 'client-group', NULL, NULL),
(32, 'deleted-client-group', 'Delete Client Group', 'Can Delete Client Group', 'client-group', NULL, NULL),
(33, 'list-client', 'List Client', 'View List all Client', 'client', NULL, NULL),
(34, 'create-client', 'Create Client', 'Can Create Client', 'client', NULL, NULL),
(49, 'credit-committy-list', 'Credit Committy View', '', 'c-committy', NULL, NULL),
(50, 'credit-committy-create', 'Credit Committy Create', '', 'c-committy', NULL, NULL),
(51, 'credit-committy-edit', 'Credit Committy Edit', '', 'c-committy', NULL, NULL),
(52, 'credit-committy-delete', 'Credit Committy Delete', '', 'c-committy', NULL, NULL),
(53, 'sch-generate', 'Generate Schedule', '', 'sch-gener', NULL, NULL),
(55, 'sch-show', 'Show Schedule', '', 'sch-gener', NULL, NULL),
(56, 'list-stuff-cashier', 'List Cashier to Staff ', '', 'staff-to-cashier', NULL, NULL),
(57, 'create-stuff-cashier', 'Create Cashier to Staff ', '', 'staff-to-cashier', NULL, NULL),
(58, 'edit-stuff-cashier', 'Edit Cashier to Staff ', '', 'staff-to-cashier', NULL, NULL),
(59, 'delete-stuff-cashier', 'Delete Cashier to Staff ', '', 'staff-to-cashier', NULL, NULL),
(64, 'list-interest_rate', 'List Interest Rate', '', 'interest_rate', NULL, NULL),
(65, 'create-interest_rate', 'Create Interest Rate', '', 'interest_rate', NULL, NULL),
(66, 'edit-interest_rate', 'Edit Interest Rate', '', 'interest_rate', NULL, NULL),
(68, 'delete-interest_rate', 'Delete Interest Rate', '', 'interest_rate', NULL, NULL),
(69, 'list-brand', 'List Brand', '', 'brand', NULL, NULL),
(70, 'create-brand', 'Create Brand', '', 'brand', NULL, NULL),
(71, 'deletd-brand', 'Delete Brand', '', 'brand', NULL, NULL),
(72, 'edit-brand', 'Edit Brand', '', 'brand', NULL, NULL),
(73, 'exchange-rate', 'Exchange', '', 'exchange_rate', NULL, NULL),
(74, 'edit-exchange-rate', 'Edit Exchange ', '', 'exchange_rate', NULL, NULL),
(75, 'Delete-exchange-rate', 'Delete Exchange ', '', 'exchange_rate', NULL, NULL),
(76, 'developer', 'Developer', 'developer', 'developer', NULL, '2017-07-13 12:02:50'),
(77, 'edit-client', 'Edit Client', 'Can Create Client', 'client', 'credit_sale', NULL),
(78, 'deleted-client', 'Delete Client', 'Can Create Client', 'client', 'credit_sale', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mfi_positions`
--

DROP TABLE IF EXISTS `mfi_positions`;
CREATE TABLE IF NOT EXISTS `mfi_positions` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mfi_positions`
--

INSERT INTO `mfi_positions` (`id`, `name`, `deleted`, `created_at`, `updated_at`) VALUES
(1, 'ប្រធានសាខា', 0, NULL, NULL),
(2, 'មន្រ្តីប្រតិបត្តិ', 0, NULL, NULL),
(3, 'មន្រ្តីឥណទាន', 0, NULL, NULL),
(4, 'គណនេយ្យ', 1, NULL, NULL),
(5, 'គណនេយ្យ', 0, NULL, NULL),
(6, 'ជំនួយការរដ្ឋបាល និងហិរញ្ញវត្ថុ', 0, NULL, NULL),
(7, 'បេឡាករ', 0, NULL, NULL),
(8, 'ប្រធានក្រុមឥណទាន', 0, NULL, NULL),
(9, 'ប្រធានក្រុមប្រឹក្សាភិបាល និងជាអគ្គនាយក', 0, NULL, NULL),
(10, 'អនុប្រធានក្រុមប្រឹក្សាភិបាល និងជាអគ្គនាយករង', 0, NULL, NULL),
(11, 'ក្រុមប្រឹក្សាភិបាល', 0, NULL, NULL),
(12, 'Developer', 0, NULL, NULL),
(13, 'ជំនួយការរដ្ឋបាល និងគណនេយ្យ', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `user_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_user_email_index` (`user_email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name_kh` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_code` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `position_id` int(11) NOT NULL,
  `user_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_gender` enum('F','M') COLLATE utf8_unicode_ci NOT NULL,
  `user_last_login` datetime NOT NULL,
  `user_last_logout` datetime NOT NULL,
  `user_status` int(11) NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `photo` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deteted` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `api_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_user_email_unique` (`user_email`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `branch_id`, `username`, `name_kh`, `user_code`, `position_id`, `user_email`, `user_phone`, `user_gender`, `user_last_login`, `user_last_logout`, `user_status`, `password`, `photo`, `deteted`, `remember_token`, `api_token`, `created_at`, `updated_at`) VALUES
(50, 2, 'socheat', 'សុជាតិ', '', 12, 'socheat@titb.biz', '016-707-044', 'F', '2018-05-21 04:05:12', '2018-01-02 10:01:48', 1, '$2y$10$t1tYFydr.jgaDJNjodr6he9g0dULeXRM9SI4Z3OxZnFIw0p1Tieju', NULL, 0, 'ybxxfErUjfxKLe7WftoRAvDQgBltV3MBHYlFrD7RMyBIS7h272WJfu6dqVOT', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjUwLCJpc3MiOiJodHRwOi8vbG9jYWxob3N0L2NyZWRpdC1zYWxlL21hcGkxL3B1YmxpYy9hcGkvbG9naW4iLCJpYXQiOjE1MjI5MjQ5MDAsImV4cCI6MTUyMjkyODUwMCwibmJmIjoxNTIyOTI0OTAwLCJqdGkiOiIzYnVLWTR5c1hlcTVnanlwIn0.OR_ZvpYLfrrRlpBnvKTbGF', '2017-07-13 16:07:09', '2018-04-05 10:41:40');

-- --------------------------------------------------------

--
-- Table structure for table `user_groups`
--

DROP TABLE IF EXISTS `user_groups`;
CREATE TABLE IF NOT EXISTS `user_groups` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `group_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`user_id`,`group_id`),
  KEY `user_groups_group_id_foreign` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_groups`
--

INSERT INTO `user_groups` (`user_id`, `group_id`) VALUES
(50, 8);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `user_groups`
--
ALTER TABLE `user_groups`
  ADD CONSTRAINT `user_groups_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_groups_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
