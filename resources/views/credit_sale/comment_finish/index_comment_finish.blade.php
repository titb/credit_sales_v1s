@extends('credit_sale.layout.master')
@section('contend')
<style>
ul.typeahead.dropdown-menu {
    margin-top: -10px;
}
</style>
<div class="container-fluid">
    <div class="row-fluid">
            <!-- validation -->
        <div class="row-fluid">
                <!-- block -->
            <div class="block">
                <div class="navbar navbar-inner block-header">
                    <center>
                        <h3 class="cen_title text-center khmer_Moul">ការបញ្ចប់ឥណទាន</h3>
                    </center>
                </div>

                <div class="block-content collapse in">
                    <div class="span12" style="margin: 0 auto !important;float: none;">								
                        <ul class="nav nav-tabs">
                            <li class="<?php if(Request::segment(1) == 'comment_finish'){echo "active";} ?>"><a href="{{url('comment_finish')}}">ការបញ្ចប់ឥណទាន</a></li>
                            <li class="<?php if(Request::segment(1) == 'comment_error'){echo "active";} ?>"><a href="{{url('comment_error')}}">ការបញ្ចប់ឥណទានមិនត្រឹមត្រូវ</a></li>
                        </ul>								
                    </div>

                    @include('errors.error')
                    <table class="table table-bordered">

                        <thead style="background: rgb(251, 205, 205);">

                            <tr class="header">

                                <th>ល.រ</th>
                                <th>សំណើ​រ ល.រ</th>
                                <th>ឈ្មេាះអតិថិជន</th>
                                <th>ចំនួនទំនិញ</th>
                                <th>តម្លៃសរុប</th>
                                <th>ការបរិច្ឆេតយល់ព្រម</th>
                                <th>ប្រភេទនៃការបញ្ចប់</th>
                                <th>សកម្មភាព</th>

                            </tr>
                        </thead>
                        <tbody class="item_list">
                            @foreach($cre_data as $key=>$val)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $val->approval_id }}</td>
                                    <td>{{ $val->cs_client->kh_username }}</td>
                                    <td>{{ $val->cs_approvalcredit->approval_item->sum('qty') }}</td>
                                    <td>{{ $val->money_owne_total_pay }}</td>
                                    <td>{{ date("d-m-Y", strtotime($val->cs_approvalcredit->date_approval)) }}</td>
                                    <td>
                                        <?php 
                                            if($val->comment_finish != null){
                                                if($val->comment_finish->status_of_finish == 1){
                                                    echo "ល្អ";
                                                }elseif($val->comment_finish->status_of_finish == 2){
                                                    echo "ល្អបង្គួរ";
                                                }elseif($val->comment_finish->status_of_finish == 3){
                                                    echo "មធ្យម";
                                                }elseif($val->comment_finish->status_of_finish == 4){
                                                    echo "មិនល្អ";
                                                }else{
                                                    echo "មិនមាន";
                                                }
                                            }else{
                                                echo "មិនទាន់បង្កើត";
                                            }
                                        ?>
                                    </td>
                                    <td class="edit">
                                        <?php if($val->comment_finish != null){$btn="btn-primary";$na="ការបញ្ចប់";}else{$btn="btn-success";$na="បង្កើត";} ?>
                                        <a href="{{url('comment_finish/create?request_id='.$val->approval_id)}}" id="btn_edit" class="btn {{$btn}} btn_edit">{{$na}}</a>
                                        <!-- <button  class="btn btn-primary btn_edit" id="btn_edit" value="">កែប្រែ</button> -->
                                        <button class="btn btn-danger btn_deleted" id="btn_deleted" value="{{$val->approval_id}}">លុប</button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                            <tr>
                                <td colspan="7">
                                    <b class="pull-right">សរុប:</b>
                                </td>
                                <td>
                                    <b id="total_all">{{count($cre_data)}}</b>
                                </td>
                            </tr>
                        </table>
                        <!-- Pagination -->
                        <div class="pagination text-right"></div>
                    </div>
                </div>
                <!-- /block -->
            </div>
            <!-- /validation -->
        </div>
    </div>
</div>

<meta name="_token" content="{{ csrf_token() }}" />
<p id="model_in"></p>
<script>
    // $.ajax({
    //     url: "{{url('comment_finish/json')}}",
    //     type: 'get',
    //     success: function(result){
    //         console.log(result);
    //         $.each(result, function(k,v){
    //             if(v.comment_finish !== null){
    //                 var url = "{{url('comment_finish/create?request_id=" + v.approval_id + "')}}"
    //                 $(".edit").append("<a href='' id='btn_edit' class='btn btn-primary btn_edit'>ការបញ្ចប់</a>");
    //             }else{
    //                 $(".edit").append("<a href='{{url('comment_finish/create?request_id=" + v.approval_id + "')}}' id='btn_edit' class='btn btn-success btn_edit'>បង្កើត</a>");
    //             }
                
    //         });
    //     },
    //     error: function(error){
    //         console.log(error.responseText);
    //     }
    // });
    $(".btn_deleted").click(function(e){
        // alert("Hello");
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })
        e.preventDefault();
        var request_id = $(this).val();
        var url = "{{url('delete/comment/')}}";
        $.ajax({
            url: url + "/" + request_id,
            type: 'post',
            data: {
                'id' : request_id,
            },
            success: function(result,status,xhr){
                location.reload();
            },
            error: function(error){
                console.log(error.responseText);
            }
        });
    });
</script>          
@stop()
