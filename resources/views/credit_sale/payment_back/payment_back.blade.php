@extends('credit_sale.layout.master')
@section('contend')
@include('credit_sale.autocompleted_script')
<style>
    p#payment_back {
        color: #08c;
        margin-top: 10px;
    }

</style>
<meta name="csrf-token" content="{{ csrf_token() }}">
<script src="{{ URL::to('assets/accounting.min.js')}}"></script>
<!-- container-fluid  -->
<div class="container-fluid content_top_mar" >

<!-- validation -->
<div class="row-fluid"> 
    <!-- block -->
    <div class="block">

        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left"><a href="{{url('list_payment_back')}}">ការទូទាត់សំណងអតិថិជន</a></div>
            <div class="muted pull-right add_payment_pre"><b >ប្រាក់ការនៅខែនេះ</b>  :  <b class="interest_this_month">  </b> || <b >ប្រាក់ដើមដែលនៅសល់</b>  :  <b class="cost_own_payment">  </b><span class='your_late_payment'>|| <b >ប្រាក់ផាកពិន័យ</b>  :  <b class='payment_late'> </b></span> </div>
        </div>
        @include('errors.error')	
        <div class="block-content collapse in">
            <form role="form" id="form_payment_back" name="form_payment_back" method="POST"  enctype="multipart/form-data">
                {{ csrf_field() }}  
                    <div class="span12" style="margin-left: 0;">
                                <div class="span4">
                                    <label class="control-label">លេខកូដអតិថិជន<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name="payment_client_id" data-required="1" class="span12 m-wrap payment_client_id" placeholder="បញ្ចូលលេខសម្គាល់តារាងទូទាត់" autocomplete="off"/>
                                        <input type="hidden" name="client_id" data-required="1" class="span12 m-wrap client_id" />
                                    </div>
                                </div>
                                <div class="span4">
                                    <label class="control-label">ឈ្មោះអតិថិជន<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name="client_name" data-required="1" class="span12 m-wrap client_name " placeholder="ឈ្មោះអតិថិជន" readonly/>
                                    </div>
                                </div>
                                <div class="span4">
                                    <label class="control-label">ចំនួនទឹកប្រាក់សរុបដែលត្រូវសង<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name="total_payment" data-required="1" class="span12 m-wrap total_payment" placeholder="ចំនួនទឹកប្រាក់សរុបដែលត្រូវសង" readonly/>
                                    </div>
                                </div>
                                <div class="span4" style="margin-left: 0px;">
                                    <label class="control-label">ថ្ងៃត្រូវសងប្រាក់<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name="date_payment" data-required="1" class="span12 m-wrap date_payment" placeholder="ថ្ងៃត្រូវសងប្រាក់" readonly/>
                                    </div>
                                </div>
                                <div class="span4">
                                    <label class="control-label">លេខប័ណ្ណ<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name="num_invoice_payment" data-required="1" class="span12 m-wrap num_invoice_payment" placeholder="លេខប័ណ្ណ" disabled="disabled"/>
                                    </div>
                                </div>
                                <div class="span4">
                                    <label class="control-label">អ្នកទទួលប្រាក់<span class="required">*</span></label>
                                    <div class="controls">
                                        <select class="span12 m-wrap staff_id" name="staff_id" disabled="disabled">
                                                <option value=""> ជ្រើសអ្នកទទួលប្រាក់ </option>
                                                @foreach($user as $us)
                                                <option value="{{$us->id}}">{{ $us->name_kh }}</option>
                                                @endforeach
                                        </select>
                                    </div> 
                                </div>
                            <div class="old_payment_remaining" style="display:none;"> 
                                <div class="span4" style="margin-left: 0px;">
                                    <label class="control-label">ចំនួនប្រាក់ដែលបានសងលើកមុន	<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name="old_payment" data-required="1" class="span12 m-wrap old_payment" placeholder="ចំនួនប្រាក់ដែលបានសងលើកមុន" readonly/>
                                    </div>
                                </div>
                                <div class="span4">
                                    <label class="control-label">ចំនួនប្រាក់ដែលត្រូវសង<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name="payment_own" data-required="1" class="span12 m-wrap payment_own" placeholder="ចំនួនប្រាក់ដែលត្រូវសង" readonly/>
                                    </div>
                                </div>
                                <div class="span4" >
                                    <label class="control-label">ថ្ងៃដែលសងលើកមុន		<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name="date_last_payment" data-required="1" class="span12 m-wrap date_last_payment" placeholder="ថ្ងៃដែលសងលើកមុន" readonly/>
                                    </div>
                                </div>
                            </div>
                                <div class="span4 "  style="margin-left: 0px;">
                                    <label class="control-label">ថ្ងៃដែលសង<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name="available_date_payment" data-required="1" class="span12 m-wrap input-xlarge datepicker available_date_payment" placeholder="ថ្ងៃដែលសង" autocomplete="off" disabled="disabled"/>
                                    </div>
                                </div>
                                <div class="span4" >
                                    <label class="control-label">ចំនួនប្រាក់ដែលសង<span class="required">*</span></label>
                                    <div class="controls">
				  							<input type="text" class="span10 m-wrap number-format available_total_payment" name="available_total_payment" placeholder="ចំនួនប្រាក់ដែលសង" autocomplete="off" disabled="disabled">
											<input type="text" class="span2 m-wrap number-format lavel_payment" name="lavel_payment" value="1" disabled="disabled">

	                                </div>
                                </div>
                                
                                <div class="span4" style="margin-top: 13px;">
                                    <div class="span6">
                                        <!-- <label class="control-label">&nbsp;</label>     -->
                                        <div class="controls">
                                            <label class="uniform">
                                              <input type="radio" name="pay_all" value="2" checked="checked" class="pay_this_month pay_all" disabled="disabled">
                                              សងប្រាក់បន្ត
                                            </label>
                                            <label class="uniform">
                                              <input type="radio" name="pay_all" value="1" class="pay_complet_this_month pay_all"  disabled="disabled">
                                              សងប្រាក់ផ្តាច់
                                            </label>
                                        </div>
                                    </div>
                                    <div class="span6">
                                        <div class="controls">
                                                <label class="uniform">
                                                <input type="checkbox" class="is_penalty_pay" name="is_penalty_pay" value="1"  disabled="disabled">
                                                ប្រសិនបើពិន័យ
                                                </label>
                                        </div>
                                    </div>
                                    <br/>
                                   
                                </div>
                                <div class="span12 no_left">
                                            <div class="controls">
                                                <label class="control-label" >​ បរិយាយ </label>
                                                <textarea name="comment" data-required="1" rows="5" class="span12 m-wrap comment" disabled="disabled"></textarea>
                                            </div>
                                 </div>
                                <div class="span12"  style="margin-left:0px;">
                                    <input type="hidden" name="old_total_pay_cos" class="old_total_pay_cos" value="0">
                                    <input type="hidden" name="num_of_payment" class="num_of_payment">
                                    <input type="hidden" name="old_total_pay_interest" class="old_total_pay_interest" value="0">
                                    <input type="hidden" name="old_total_panalty" class="old_total_panalty" value="0">
                                    <input type="hidden" name="sc_timesheet_id" class="sc_timesheet_id" value="0">
                                    <input type="hidden" name="total_pay_cost" class="total_pay_cost" value="0">
                                    <input type="hidden" name="total_pay_cost_owe" class="total_pay_cost_owe" value="0">
                                    <input type="hidden" name="total_pay_interest" class="total_pay_interest" value="0">
                                    <input type="hidden" name="status_for_pay" class="status_for_pay" value="0">
                                       <a href="#sub_schedule_back" data-toggle="modal" class="btn btn-danger pull-right sub_schedule_back">បោះបង់</a>
                                       <button type="button" class="btn btn-primary pull-right sub_schedule" style="margin-right: 10px;" value=""> រក្សាទុក</button>
                                </div>
                    </div>
            </form>
            </div>
        </div>
    <!-- /block -->
</div>
<!-- /validation -->

</div>
<div id="sub_schedule_back" class="modal hide">
    <div class="modal-header">
        <button data-dismiss="modal" class="close" type="button">&times;</button>
        <h3>Confirm Cancel Payment </h3>
    </div>
    <div class="modal-body">
        <p>Are you sure you want to cancel this transation?</p>
    </div>
    <div class="modal-footer" style=" padding: 10px;">
        <button data-dismiss="modal" class="btn btn-primary modal_click" value="1" >Confirm</button>
        <button data-dismiss="modal" class="btn btn-danger modal_click" value="0">Cancel</button>
    </div>
</div>

<meta name="_token" content="{{ csrf_token() }}" />
<script type="text/javascript">
    $('.number-format').keyup(function(event) {

        // skip for arrow keys
        if(event.which >= 37 && event.which <= 40){
        event.preventDefault();
        }

        $(this).val(function(index, value) {
            value = value.replace(/,/g,'');
            return numberWithCommas(value);
        });
    });

    function numberWithCommas(x) {
        var parts = x.toString().split(".");
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        return parts.join(".");
    }
    $(".payment_client_id").autocomplete({

        source : '{{ URL::route('client_payment_back') }}',

        minlenght :1,

        autoFocuse : true,

        select:function(e,ui){
           // get_data_schedule(ui.item.id);
           get_date_payment(ui.item.id);
           $(".num_invoice_payment, .available_total_payment , .lavel_payment, .available_date_payment, .pay_all, .is_penalty_pay, .comment, .staff_id").removeAttr('disabled');
           
        }
    });

    function get_date_payment(schedule_id){

        $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
        $(".loading_file").removeClass("active");
        var url_now = "{{url('schedule_check_timesheet_pay')}}/"+schedule_id;
            $.ajax({
                
                type: "POST",
                url: url_now,
                dataType: 'json',
                // async: false,
                //data: formData1,
                success: function(result){
                    console.log(result);
                 $(".loading_file").addClass("active");
                    $(".payment_client_id").attr('readonly','readonly');

                    if(result.schedule.cs_schedule){
                            $(".client_name").val(result.schedule.cs_schedule.cs_client.kh_username);
                    }
                    if(result.schedule.total_payment){
                        $(".total_payment").val(result.schedule.total_payment);
                    }
                    if(result.schedule.date_payment){
                        $(".date_payment").val(day_format_show(result.schedule.date_payment));
                    }
                    if(result.schedule.cs_schedule){
                        $(".staff_id").find("[value='"+result.schedule.cs_schedule.staff_id+"']").attr("selected","selected"); 
                    }
                    if(result.schedule.cs_schedule.client_id){
                        $(".client_id").find("[value='"+result.schedule.cs_schedule.client_id+"']").attr("selected","selected"); 
                    }
                    
                    if(result.schedule.status_for_pay == 2){
                        $(".old_payment_remaining").removeAttr('style');
                    }else if(result.schedule.status_for_pay == 0){
                        $(".old_payment_remaining").css('display','none');
                    }
                    if(result.old_payment != 0){
                        $('.old_payment').val(result.old_payment);
                    }
                    if(result.date_last_payment != 0){
                        $('.date_last_payment').val(result.date_last_payment);
                    }
                    if(result.panalty != 0){
                        $('.old_total_panalty').val(result.panalty);
                    }
                    if(result.payment_own != 0){
                        $('.payment_own').val(result.payment_own);
                    } 
                    if(result.num_of_payment != 0){
                        $('.num_of_payment').val(result.num_of_payment);
                    }    
                    
                    if(result.available_total_pay_interest != 0){
                        $('.old_total_pay_interest').val(result.available_total_pay_interest);
                    }
                    if(result.available_total_pay_cos != 0){
                        $('.old_total_pay_cos').val(result.available_total_pay_cos);
                    }
                    
                    $(".sc_timesheet_id").val(result.schedule.id);
                    $(".status_for_pay").val(result.schedule.status_for_pay);
                    $(".total_pay_cost").val(result.schedule.total_pay_cost);
                    $(".total_pay_cost_owe").val(result.schedule.total_pay_cost_owe);
                    $(".total_pay_interest").val(result.schedule.total_pay_interest);
                    
                    if(result.schedule.total_pay_cost_owe == 0){
                        $(".pay_complet_this_month").attr("checked","checked");
                        $(".pay_this_month").removeAttr("checked");
                    }
                    show_payment_banner(2); 
                }
            });
    }


    $(".modal_click").click(function(e){
        e.preventDefault();
        var modale_cancel = $(this).val();
        if(modale_cancel == 1){
            $(".payment_client_id").removeAttr('readonly');
        }else {
            alert("You Chose Continuce transaction ");  
        }
       
    });

    $(".sub_schedule").click(function(e){
        $(".loading_file").removeClass("active");
        e.preventDefault();
        var url_now = "{{ url('client_payment_back') }}";
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }

        });
      var item_id = $(this).val();
        var form = document.forms.namedItem("form_payment_back");
        var formDatas = new FormData(form);   
        $.ajax({
            type:'POST', 
            url: url_now,
            dataType: 'json',
            contentType: false,
            data: formDatas, 
            processData: false,
            success: function (result) {
                $(".loading_file").addClass("active");
                console.log(result);
                window.location = "{{url('payment_credit_back')}}"; 
            },
            error: function (result) {
                console.log('Error:', result);
            }
        });
        

    });

    $(".pay_all").click(function(){
        var payment = $(this).val();
        show_payment_banner(payment);
    });
function show_payment_banner(payment){
    var total_pay_cost = Number($(".total_pay_cost").val());
    var total_pay_cost_owe = Number($(".total_pay_cost_owe").val());
    var total_pay_interest = Number($(".total_pay_interest").val());
    var total_pay = total_pay_cost + total_pay_cost_owe;
    $(".cost_own_payment").text(total_pay.toFixed(2));
    $(".interest_this_month").text(total_pay_interest);
    if(payment == "2"){
        $(".pay_complete").remove();
    }else if(payment == 1){
        var total_pay = total_pay_cost + total_pay_cost_owe + total_pay_interest;
        var text = "<span class='pay_complete'>|| <b >ប្រាក់ត្រូវបងបញ្ចប់</b>  :  <b class='cost_own_payment'>  "+ total_pay.toFixed(2) +"</b></span>";
        $(".add_payment_pre").append(text);
    }
}
// $(document).on('change','.datepicker-dropdown', function(){
//$(".available_date_payment").change(function(){
// $(".available_date_payment").on('focusout', function() {
//     var schedul_id = $(".payment_client_id").val();

//     if(schedul_id != ''){
//         var adp = $(".available_date_payment").val();
//         var status_for_pay = $(".status_for_pay").val();
//         if(status_for_pay == 0){
//             var date_payment = convert_back_date($(".date_payment").val()); 
//         }else{
//             var date_payment = convert_back_date($(".date_last_payment").val());
//         }
//     var dat_e = date_between(date_payment , adp);

//     alert(adp);   
//     }
// });
function date_cal(){
    var schedul_id = $(".payment_client_id").val();
var alv_get = $(".available_date_payment").val();
    if(schedul_id != ''){
        if(alv_get != ''){
            var adp = $(".available_date_payment").datepicker("getDate");
            
                var status_for_pay = $(".status_for_pay").val();
                if(status_for_pay == 0){
                    var date_payment = convert_back_date($(".date_payment").val()); 
                }else{
                    var date_payment = convert_back_date($(".date_last_payment").val());
                }
                var dat_e = date_between(date_payment , adp);
                
                var interest = $(".total_pay_interest").val();
                var  dat_total_payment = 0;
            if(dat_e > 0){
                if(interest != 0) {
                
                    var dateinmonth = DateInMonth(date_payment);
                    var dat_total_payment1 = Number(interest)/30;  
                    var dat_total_payment = dat_total_payment1 * dat_e;
                }
            }else{
                dat_e = 0
            }
                var data = {
                    date_late:dat_e,
                    dat_total_payment: dat_total_payment
                };
            
            return data;
        }
    }
}   

// $('.is_penalty_pay').click(function(){
//     if($(this).prop('checked') == true){
//         date_cal();
//     }else{
//         alert("no penalty");
//     }
// });
$(".available_date_payment").focusout(function(){
    var data = date_cal();
    var dm = (data.dat_total_payment).toFixed(2);
    $(".payment_late").text(dm);
   // alert("You Late "+ data.date_late +" Your Late Payment is  " +data.dat_total_payment);
});
// });
// $(".available_date_payment").datepicker({
//     minDate: '0',
//     maxDate: '+1Y+6M',
//     onSelect: function (dateStr) {
//          var status_for_pay = $(".status_for_pay").val();
//         if(status_for_pay == 0){
//             var date_payment = convert_back_date($(".date_payment").val()); 
//         }else{
//             var date_payment = convert_back_date($(".date_last_payment").val());
//         }
//         var max = $(this).datepicker('getDate'); // Get selected date
//         $('#datepicker').datepicker('option', 'maxDate', max || '+1Y+6M'); // Set other max, default to +18 months
//         var start = new Date(date_payment);
//         var end = $(".available_date_payment").datepicker("getDate");
//         var days = (end - start) / (1000 * 60 * 60 * 24);
//         // $("#TextBox3").val(days);
//         alert(days);
//     }
// });
</script>
@endsection
