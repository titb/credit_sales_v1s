@extends('credit_sale.layout.master')
@section('contend')
@include('credit_sale.autocompleted_script')
<style>
    p#payment_back {
        color: #08c;
        margin-top: 10px;
    }

</style>
<meta name="csrf-token" content="{{ csrf_token() }}">
<script src="{{ URL::to('assets/accounting.min.js')}}"></script>
<!-- container-fluid  -->
<div class="container-fluid content_top_mar" >

<!-- validation -->
<div class="row-fluid">
    <!-- block -->
    <div class="block">
    @include('errors.error')	
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left">ការទូទាត់សំណងអតិថិជន</div>
        </div>
        <div class="block-content collapse in">
            <div class="span12">
                    <!-- BEGIN FORM-->
                        <fieldset>
                            <span class="span11">
                            <div class="control-group">
                                <label class="control-label">បញ្ចូលលេខសម្គាល់តារាងទូទាត់<span class="required">*</span></label>
                                <div class="controls">
                                    <input type="text" name="payment_client_id" data-required="1" class="span6 m-wrap payment_client_id" placeholder="បញ្ចូលលេខសម្គាល់តារាងទូទាត់"/>
                                    <!-- <button type="buttom" class="btn btn-primary sub_schedule" style="margin-top:-10px;"> Submit</button> -->
                                </div>
                            </div>
                            </span>
                           
                            </div>
                        </fieldset>
                    <!-- END FORM-->
                </div>
            </div>
        </div>
    <!-- /block -->
</div>
<!-- /validation -->

</div>
<meta name="_token" content="{{ csrf_token() }}" />
<script type="text/javascript">
    $(".payment_client_id").autocomplete({

        source : '{{ URL::route('client_payment_back') }}',

        minlenght :1,

        autoFocuse : true,

        select:function(e,ui){
           // get_data_schedule(ui.item.id);
        }
    });
	// $(document).ready(function(){
   
	// 		 $.ajaxSetup({
    //             headers: {
    //                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //             }
    //         });			      
			

	// });
</script>
@endsection
