@extends('credit_sale.layout.master')
@section('contend')
@include('credit_sale.autocompleted_script')
<style>
    p#payment_back {
        color: #08c;
        margin-top: 10px;
    }

</style>
<script src="{{ URL::to('assets/accounting.min.js')}}"></script>
<!-- container-fluid  -->
<div class="container-fluid content_top_mar" >

<!-- validation -->
<div class="row-fluid">
    <!-- block -->
    <div class="block">
    @include('errors.error')	
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left" style="margin-top: -10px;"><a href="{{url('payment_credit_back')}}">កាលវិភាគនៃការសងប្រាក់របស់អតិថិជន</a></div>
        </div>
        <div class="block-content collapse in">
            <div class="span12">
                    <!-- BEGIN FORM-->
                        <fieldset>
                            <span class="span11">
                         
                                <div class="control-group">
                            
                                    <label class="control-label">លេខសម្គាល់តារាង</label>
                                    <div class="controls">
                                        <input type="text" name="payment_client_id" data-required="1" class="span6 m-wrap payment_client_id" placeholder="បញ្ចូលលេខសម្គាល់តារាងទូទាត់"  autocomplete="off"/>
                                        <button type="buttom" class="btn btn-primary b_search" id="b_search" name="submit_search" value="b_search" style="margin-top:-10px;"> Search</button>
                                    </div>
                                </div>
                            </span>

                            </div>
                        </fieldset>
                    <!-- END FORM-->
                    <div class="span12" style="margin-left: 0px;">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>ល.រ</th>
                                    <th>លេខតារាង</th>
                                    <th>លេខប័ណ្</th>
                                    <th>ឈ្មោះអតិថិជន</th>
                                    <th>ប្រាក់ដើម</th>
                                    <th>ប្រាក់ការ</th>
                                    <th>ប្រាក់សរុបដែលបានសង</th>
                                    <th>ថ្ងៃដែលបានសង់</th>
                                    <th>បរិយាយ</th>
                                    <th>មន្ត្រីឥណទាន</th>
                                    <th>អ្នកបញ្ចូល</th>
                                    <th>ថ្ងៃអ្នកបញ្ចូល</th>
                                    <th class="status_payment">ស្ថានភាព</th>
                                </tr>
                            </thead>
                            <tbody id="list_item">
                            
                            </tbody>
                        </table>
                        <div class="pagination text-right"></div>

                    </div>
                </div>

            </div>
        </div>
    <!-- /block -->
</div>
<!-- /validation -->

</div>
<meta name="_token" content="{{ csrf_token() }}" />
<script type="text/javascript">
       
            var numpage = 1;
            var url_edit = "{{route('list_schedule_timesheet_pay_json')}}";
            get_page(url_edit.numpage);

            $(".payment_client_id").autocomplete({

                source : '{{ URL::route('search_schedule_id_ajax') }}',

                minlenght :1,

                autoFocuse : true,

                select:function(e,ui){ }
            });
            // $(document).ajaxComplete(function(){
                $(".b_search").click(function(){
                    var submit_search = $(this).val();
                    var n = 1;
                    
                    var url_index2 = submit_search; 		
                    get_page(url_index2,numpage = n);
                });
                $(document).on('click','.pag',function(){
                    var numpage = $(this).text();   
                    get_page(url_edit,numpage);
                });

                $(".pre").click(function(){
                    var numpage = $(this).find(".pre_in").val();
                    get_page(url_edit,numpage);
                });
            // });
            $(document).on('click','.btn_deleted',function(e){   
                if (confirm("Are you sure you wish to delete  this Row?")) {
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                            }
                        })
                            var item_id = $(this).val();
                        
                        var url_index = "{{ url('delete_payment_timesheet') }}/"+item_id;
                            e.preventDefault();
                            $.ajax({
                                type: "POST",
                                url: url_index, 
                                dataType: "json",
                                success: function(result){
                                    $('.msg_show').html(result.msg_show); 
                                            var numpage = 1 ;
                                            var url_index1 = "{{ route('get_sale_by_wating_credit_json') }}";
                                            get_page(url_index1,numpage);
                                },
                                error: function (result ,status, xhr) {
                                    console.log(result.responseText);
                                }                   
                            });
                }else{
                    return false;
                }
             });
                function get_page(url,n){
                    $.ajaxSetup({
							headers: {
								'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
							}
						})
 

                        var payment_client_id = $(".payment_client_id").val();

                        if(url === "b_search"){
                            var url_i = "{{route('list_schedule_timesheet_pay_json')}}";
                            var forData = {
                                payment_client_id: payment_client_id,
                                submit_search: $(".b_search").val()
                            }                
                            var url_index = url_i+"?payment_client_id="+payment_client_id+"&submit_search=b_search&page="+n;     
                        }else{
                            var forData = {};
                            var url_index = url_edit+"?page="+n;
                        }

						// alert(url_edit);
						var client;
						// var forData = {};
                        var out = "";
                        $.ajax({
                                type: "GET",
                                url: url_index, 
                                dataType: "json",
                                data: forData,
                                success: function(result ,xhr){
                                    console.log(result);
                                    var myJSON = JSON.stringify(result); 
                                    var text ="";
                                    // var i = 0;
                                    $.each(result.data, function(index, field){
                                        var il = result.from  + index;
                                        var url_edit = "{{url('payment_credit_back?item_id=')}}"+field.id+"&item=edite";
                                        text +=  "<tr>";
                                        text += " <td>"+ il +"</td>";
                                        text += " <td>"+ field.cs_schedules_id+"</td>";
                                        text += " <td>"+ field.num_invoice_payment+"</td>";
                                        text += " <td>"+ field.cs_schedule.cs_client.kh_username+"</td>";
                                        text += " <td>"+ accounting.formatMoney(field.available_total_pay_cost) +"</td>";
                                        text += " <td>"+ accounting.formatMoney(field.available_total_pay_interest) +"</td>"; 
                                        text += " <td>"+ accounting.formatMoney(field.available_total_payment) +"</td>";
                                        text += " <td>"+ day_format_show(field.available_date_payment) +"</td>";
                                        text += " <td>"+ field.note+"</td>";
                                        text += " <td>"+ field.cs_staff.name_kh +"</td>";
                                        text += " <td>"+ field.user.name_kh +"</td>";
                                        text += " <td>"+ field.created_at +"</td>";
                                        text += "<td>";
                                        // text += "<a href='"+url_edit+"' class='btn btn-primary btn_edit' id='btn_edit'>Edit</a>  ";
                                        text += "<button  class='btn btn-danger btn_deleted'  value='"+ field.id +"'>Delete</button>";
                                        text += "</td>";
                                        text += "</tr>";
                                    });
  
                                    $("#list_item").html(text);

                                    var page = "";
                                        if(result.prev_page_url === null){
                                            var pr_url = result.current_page;
                                        }else{
                                            var pr_url = result.current_page -1;
                                        }
                                        page += "<a class='pre'>&laquo;<input type='hidden' class='pre_in' value='"+pr_url+"' ></a>";
                                        for(var x = 1; x <= result.last_page; x ++  ) {
                                            if(result.current_page === x){
                                                page += "<a class='pag active' >"+x+"</a>";
                                            }else{
                                                page += "<a class='pag' >"+x+"</a>";
                                            }
                                        }
                                        if(result.next_page_url === null){
                                            var ne_url = result.current_page;
                                        }else{
                                            var ne_url = result.current_page +1;
                                        }
                                        page += "<a class='pre'>&raquo;<input type='hidden' class='pre_in' value='"+ne_url+"' ></a>";
                                        $(".pagination").html(page );
                                },
                                error: function (result ) {
                                    console.log(result.stutus);
                                }

                        });
                }


			 $(document).on('click','.btn_deleted',function(e){   
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                            }
                        })
                            var item_id = $(this).val();
                           var url_index = "{{ url('delete_payment_timesheet') }}/"+item_id; 
                            e.preventDefault();
                            $.ajax({
                                type: "POST",
                                url: url_index, 
                                dataType: "json",
                                success: function(result){
                                    $('.msg_show').html(result.msg_show); 
                                            var numpage = 1 ;
                                            var url_index1 = "{{ route('list_schedule_timesheet_pay_json') }}";
                                            get_page(url_index1,numpage);
                                },
                                error: function (result ,status, xhr) {
                                    console.log(result.responseText);
                                }                   
                            });
                    });	

</script>
@endsection
