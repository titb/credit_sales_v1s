@extends('credit_sale.layout.master')
@section('contend')

<div class="container-fluid">
    <div class="row-fluid">
        <!-- validation -->
		<div class="row-fluid">
			<!-- block -->
			<div class="block">
				<div class="navbar navbar-inner block-header">
					<div class="muted pull-left"><a href="{!! url('penaty_date') !!}">ការកំណត់ពិន័យ</a> </div>
						 <div class="muted pull-right" style="padding-top: 0px;"><a href="{{ url('penaty_date/create') }}" class="btn btn-success pull-right"><i class="icon-plus icon-white"></i> បង្កើតថ្មី</a></div>

                </div>
                <div class="block-content collapse in">
                	@if ($message = Session::get('success'))
						<div class="alert alert-success">
							<p>{{ $message }}</p>
						</div>
					@endif
					<center>

                        <h3 class="cen_title text-center khmer_Moul">ការកំណត់ពិន័យ</h3> 

                    </center>

                            	<legend></legend>
					<table class="table table-bordered" style="text-align:center">
						<thead style="background: rgb(251, 205, 205);">
							<tr>
								<th>#</th>
								<th>ឈ្មោះ</th>
								<th>ថ្ងៃចាប់ផ្តើម</th>
								<th>ថ្ងៃបញ្ចប់</th>
								<th>ពណ័ </th>
								<th>ចំនួនពិន័យ</th>
								<th>សេចក្តីលម្អិត</th>
								<th>អ្នកប្រើប្រាស់</th>
								<th style="width: 131px;">សកម្មភាព</th>

								</tr>
						</thead>
						<tbody>
						<?php $i =1; ?>
						@foreach($data as $value)
							<tr>
								<td>{{ $i++ }}</td>
								<td>{{ $value->name }} </td>
								<td>{{ $value->late_form }}​ ថ្ងៃ</td>
								<td>{{ $value->at_late }} ថ្ងៃ</td>
								<td >  <p style="background: {{ $value->color  }}; width:20px;  height: 20px;"></p> </td>
								<td>{{ $value->percent_of_payment }} %</td>
								<td>{{ $value->note }}</td>
								<td>
									{{$value->user->name_kh}}
								</td>
								<td>
									<a class="btn btn-primary" href="{{ url('penaty_date/'.$value->id.'/edit') }}">កែប្រែ</a>
									<!-- <a class="btn btn-danger" href="{{ url('penaty_date/'.$value->id.'/delete') }}">Delete</a> -->
								</td>
							</tr>
						@endforeach
						</tbody>
					</table>
                </div>
            </div>
        </div>
    </div>
</div>


@stop()