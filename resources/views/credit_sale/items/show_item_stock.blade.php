@extends('credit_sale.layout.master')
@section('contend')
<div class="container-fluid">
    <div class="row-fluid">
        <div class="block">
            <div class="navbar navbar-inner block-header">
                @if($form != 'report')
                <div class="muted pull-left"><a href="{!! url('accounts') !!}"> គណនី</a> <span class="divider">|</span>
                    {{$title}} </div>
                @else
                <div class="muted pull-left"><a href="{!! url('report/item/report_item_list') !!}"> របាយការណ៍ទំនិញ</a>
                    <span class="divider">|</span> {{$title}} </div>
                @endif
            </div>
            <div class="block-content collapse in">
                <ul class="nav nav-tabs">
                    <li class="<?php echo(Request::segment(3) == $data_id?'active':'');  ?>">
                        <a href="{{ url('products/products/'.$data_id.'/show') }}">ព័ត៌មានផលិតផល</a>
                    </li>
                    <li class="<?php echo(last(Request::segments()) == 'stock'?'active':'');  ?>">
                        <a href="{{url('products/products/show/'.$data_id.'/stock')}}">ក្នុងស្តុក</a>
                    </li>
                </ul>
                @include('errors.error')
                <div class="span8" style="margin: 0 auto !important;float: none;">
                    <center>
                        <h3 class="cen_title"> ការបង្ហាញព័ត៌មានស្តុក </h3>
                    </center>
                </div>
                <div class="span8" style="margin: 0 auto !important;float: none;">
                    <!-- <div class="span3"  style="float: right;">
                            <a href="#" class="btn btn-primary">កែប្រែរ</a>
                          </div> -->
                    <table class="table table-bordered table-striped">
                        <thead style="background: rgb(251, 205, 205);">
                            <tr>
                                <th>#</th>
                                <th>បរិយាយ</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><b>ពន្ធ</b></td>
                                <td><p class="tax"></p></td>
                              </tr>
                            <tr>
                                <td><b>ចំនួននាំចូល</b></td>
                                <td>
                                    <p class="qty_add"></p>
                                </td>
                            </tr>
                            <tr>
                                <td><b>ចំនួនលក់ចេញ</b></td>
                                <td>
                                    <p class="qty_sale"></p>
                                </td>
                            </tr>
                            <tr>
                                <td><b>ចំនួនត្រលប់វិញ</b></td>
                                <td>
                                    <p class="qty_receive"></p>
                                </td>
                            </tr>
                            <tr>
                                <td><b>ចំនួននៅសល់</b></td>
                                <td>
                                    <p class="qty_stock"></p>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<meta name="_token" content="{{ csrf_token() }}" />

<script type="text/javascript">
    $(document).ready(function () {
        var sup_id = "{{$data_id}}";
        var url = "{{ url('products/products') }}";
        var url_index1 = url + "/" + sup_id + "/json_edit";
        $.ajax({
            type: "GET",
            url: url_index1,
            dataType: "json",
            // data: forData,
            // async: false,
            success: function (result) {
                // console.log(result);
                if(result.cs_item_tax !== null){
                    $.each(result.cs_item_tax, function(tk, tv){
                        if(tk == result.cs_item_tax.length - 1){
                            $(".tax").append(tv.percent);
                        }else{
                            $(".tax").append(tv.percent + " , ");
                        }
                    });
                }
                var add_new = 0;
                var sale = 0;
                var receive = 0;
                $.each(result.inventorys, function(ik, iv){
                    if(iv.status_transation == "add new"){
                        add_new += iv.qty;
                    }
                    if(iv.status_transation == "Sale"){
                        sale += (iv.qty * -1);
                    }
                    if(iv.status_transation == "receiving"){
                        receive += iv.qty;
                    }
                });
                $(".qty_add").html(add_new);
                $(".qty_sale").html(sale);
                $(".qty_receive").html(receive);
                $(".qty_stock").html(result.qty_re.qty);
            },
            error: function (result, status, xhr) {
                console.log(result.responseText);
            }
        });
    });
</script>

@endsection