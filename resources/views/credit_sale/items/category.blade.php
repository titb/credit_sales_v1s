@extends('credit_sale.layout.master')
@section('contend')

<div class="container-fluid">
            <div class="row-fluid">
                     <!-- validation -->
                    <div class="row-fluid">
                         <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                            					<?php $items = DB::table('cs_categorys')->get();?>
											<input type="text" name="item_search" placeholder="Name" data-required="1" class="span3 m-wrap item_search" style="margin-top:5px;" id="typeahead" data-provide="typeahead" data-source='[@foreach($items as $row)"{{$row->name}}",@endforeach""]' autocomplete="off">
                                            <button type="submit" class="btn btn-primary b_search" id="b_search" name="submit_search" style="margin-top: -5px;" value="b_search">Search</button>
											
                            </div>

                            <div class="block-content collapse in">
                            @include('errors.error')	
                             	<center>
			                        <h3 class="cen_title text-center khmer_Moul">ការគ្រប់គ្រង់ប្រភេទរបស់ផលិតផល</h3>

			                    	<div class="muted span3 pull-right" style="margin-bottom:5px;"><button class="btn btn-success btn_edit pull-right" value="add"><i class="icon-plus icon-white"></i> បង្កើតថ្មី</button></div>
			                    </center>
                            	<legend></legend>
                           
							    <table class="table table-bordered">

							      <thead style="background: rgb(251, 205, 205);">

							        <tr class="header">

							            <th>ល.រ</th>

							            <th>ឈ្មេាះ</th>

							            <th>បរិយារ</th>

							            <th>សកម្មភាព</th>

							        </tr>
							      </thead>
							      <tbody class="item_list">
                                       
                                  </tbody>
                                    <tr>
                                        <td colspan="3">
                                            <b class="pull-right">សរុប:</b>
                                        </td>
                                        <td>
                                            <b id="total_all"></b>
                                        </td>
                                    </tr>
							    </table>
                            	<!-- Pagination -->
								<div class="pagination text-right"></div>
			    			</div>
                        <!-- Edit Category -->
                            <div id="edit_me" class="modal hide">
                                    <div class="modal-header">
                                        <br/>
                                        <button class="close btn_close" type="button">&times;</button>
                                        <center><h3 class="cen_title"><span id="ch_title" style="font-size: 24.5px !important;">ការបង្កើត</span>ប្រភេទរបស់ផលិតផល</h3></center>
                                    </div>
                                    <div class="modal-body">
                                        <form role="form" id="form_insert_category" name="form_insert_category" method="POST" enctype="multipart/form-data">
                                        {{ csrf_field() }}  
                                            <div class="span8"  style="margin: 0 auto !important;float: none;">
                                                    <div class="span12" style="margin-left:0;">
                                                        <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">​ ឈ្មេាះ <span class="required" title="This place you must be put data">*</span></label>
                                                        <div class="controls">
                                                            <input type="text" id="name" class="span12 m-wrap name" name="name" data-required="1" autocomplete="off"/>
                                                        </div>
                                                        <p class="alert-danger">{{$errors->first('name')}}</p>
                                                    </div>
                                                    <div class="span12" style="margin-left:0;">
                                                        <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">​ សេចក្ដីពិពណ៌នា <span class="required" title="This place you must be put data">*</span></label>
                                                        <div class="controls">
                                                            <textarea name="description"  data-required="1" rows="5" class="span12 m-wrap description"></textarea>
                                                        </div>
                                                        <p class="alert-danger">{{$errors->first('discription')}}</p>  
                                                    </div>
                                                    <div class="span12" style="margin-left:0;">
                                                        <br/>
                                                            <center>
                                                                    <button class="btn btn-success tbn_add" id="btn-save" name="btn_save" value="add">Submit</button>
                                                                    <input type="hidden" id="item_id" name="item_id" value="0">
                                                                    <button  class="btn btn-danger get_back">Back</button>
                                                            </center>  
                                                        <br/>
                                                    
                                                    </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            <!-- end Category -->
						</div>
                     	<!-- /block -->
		    		</div>
                     <!-- /validation -->
                </div>

            </div>

<meta name="_token" content="{{ csrf_token() }}" />
<p id="model_in"></p>

<script>
$(document).ready(function(){
        var numpage = 1;
		var url_index1 = "{{route('products/categorys_json')}}";
		get_page(url_index1,numpage);
   $(document).ajaxComplete(function(){    
        $(".b_search").click(function(){
            var submit_search = $(this).val();
            var n = 1;
            var url_index2 = submit_search; 		
            get_page(url_index2,numpage = n);
        });
        
        $(".pag").click(function(){
            var numpage = $(this).text();   
            get_page(url_index1,numpage);
        });

        $(".pre").click(function(){
            var numpage = $(this).find(".pre_in").val();
            get_page(url_index1,numpage);
        });

        $('.btn_edit').click(function(){
           var btn_edit = $(this).val();
            if(btn_edit === "add"){
               $("#category_id").val("0");
               $("#ch_title").text("ការបង្កើត");
            }else{
                var name = $(this).parents('tr').find('#name_b').text();
                var desp = $(this).parents('tr').find('#desp_b').text();
                var item_id = btn_edit;
                $(".name").val(name);
                $(".description").val(desp);
                $("#item_id").val(item_id);
                $("#btn-save").val("edit");
                $("#ch_title").text("ការកែប្រែ");
            }
                $("#edit_me").addClass("in");
                $("#edit_me").css({'display':'block'});
                $("#edit_me").attr("aria-hidden","false");
                $("#model_in").html("<div class='modal-backdrop  in'></div>");
        });

         $(".btn_deleted").click(function(e){
            
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })
                var item_id = $(this).val();
                var url1 = "{{ route('products/categorys') }}";
                var url_index = url1+"/"+item_id+"/deleted";
                e.preventDefault();
                $.ajax({
                    type: "POST",
                    url: url_index, 
                    dataType: "json",
                    success: function(result){
                        $('.msg_show').html(result.msg_show); 
                                var numpage = 1 ;
                                var url_index1 = "{{ route('products/categorys_json') }}";
                                get_page(url_index1,numpage);
                    },
                    error: function (result ,status, xhr) {
                        console.log(result.responseText);
                    }                   
                });
        });

   });
        function get_page(url,n){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                })
                var item_search = $(".item_search").val(); 
                if(url === "b_search" || item_search !== ""){
                    var url_i = "{{route('products/categorys_json')}}";
                    var forData = {
                                    item_search: $(".item_search").val(),
                                    submit_search: $(".b_search").val()
                                }                
                    var url_index = url_i+"?item_search="+item_search+"&submit_search=b_search&page="+n;     
                }else{
                    var forData = {};
                    var url_index = url+"?page="+n;
                }
			    var client;
                    $.ajax({
                            type: "GET",
                            url: url_index, 
                            dataType: "json",
                            data: forData,
                            // async: false,
                            success: function(result){
                                console.log(result);
                                $("#total_all").text(result.total);
                                        $.each(result.data, function(i, field){
                                                var il = result.from  + i;
                                        
                                                client += "<tr>";
                                                    client += "<td>"+ il +"</td>";
                                                    client += "<td id='name_b'>"+ field.name +"</td>";
                                                    client += "<td id='desp_b'> "+ field.description +"</td>";
                                                    client += "<td>";
                                                    client += "<button  class='btn btn-primary btn_edit' id='btn_edit'  value='"+ field.id +"'>Edit</button>  ";
                                                    client += "<button  class='btn btn-danger btn_deleted' ' value='"+ field.id +"'>Delete</button>";
                                                    client += "</td>";
                                                client += "</tr>";
                                        });
                                    $(".item_list").html(client);	
                                    var page = "";
                                    if(result.prev_page_url === null){
                                        var pr_url = result.current_page;
                                    }else{
                                        var pr_url = result.current_page -1;
                                    }
                                    page += "<a class='pre'>&laquo;<input type='hidden' class='pre_in' value='"+pr_url+"' ></a>";
                                    for(var x = 1; x <= result.last_page; x ++  ) {
                                        if(result.current_page === x){
                                            page += "<a class='pag active' >"+x+"</a>";
                                        }else{
                                            page += "<a class='pag' >"+x+"</a>";
                                        }
                                    }
                                    if(result.next_page_url === null){
                                        var ne_url = result.current_page;
                                    }else{
                                        var ne_url = result.current_page +1;
                                    }
                                    page += "<a class='pre'>&raquo;<input type='hidden' class='pre_in' value='"+ne_url+"' ></a>";
                                    $(".pagination").html(page );

                        },
                        error: function (result ,status, xhr) {
                            console.log(result.responseText);
                        }
                    
                    });
                   
		}    
        
       
        $('#btn-save').click(function(e){

             var btn_edit = $(this).val();
             $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                })
            // item_id   
                if(btn_edit == "edit"){
                    var item_id  = $("#item_id").val();
                    var url1 = "{{ route('products/categorys') }}";
                    var url_index = url1+"/"+item_id+"/edit";
                 
                }else{
                    var item_id  =  0; 
                    var url_index = "{{ route('products/categorys/create') }}";  
                  
                }
                
                var forData = {
                                name: $(".name").val(),
                                description_get: $(".description").val()
                              }

                 e.preventDefault();
                
                $.ajax({
					type: "POST",
					url: url_index, 
					dataType: "json",
					data: forData,
					success: function(result){
                        console.log(result);
                        $("#edit_me").removeClass("in");
                        $("#edit_me").css({'display':'none'});
                        $("#edit_me").attr("aria-hidden","true");
                        $("#model_in").html("");
                        $("#item_id").val(0);
                        $("#btn-save").val("add");
                        $("#ch_title").text("ការបង្កើត");
                        
                        $('#form_insert_category').trigger("reset");
								var numpage = 1 ;
								var url_index1 = "{{ route('products/categorys_json') }}";
								get_page(url_index1,numpage);
                       $('.msg_show').html(result.msg_show); 

                    },
                    error: function (result ,status, xhr) {
                        console.log(result.responseText);
                    }                   
                });
            });
    
        $(".btn_close,.get_back").click(function(event){
            event.preventDefault()
            $("#edit_me").removeClass("in");
            $("#edit_me").css({'display':'none'});
            $("#edit_me").attr("aria-hidden","true");
            $("#model_in").html("");
            $("#item_id").val(0);
            $("#btn-save").val("add");
            $("#ch_title").text("ការបង្កើត");
            $('#form_insert_category').trigger("reset");

        });


 });       
</script>            
@stop()
