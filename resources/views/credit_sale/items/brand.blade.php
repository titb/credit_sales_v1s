@extends('credit_sale.layout.master')
@section('contend')

<div class="container-fluid">
            <div class="row-fluid">
                     <!-- validation -->
                    <div class="row-fluid">
                         <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                            		<!-- <form class="form-search brand_products" style="margin-bottom:2px;" method="GET" > -->
											<?php $brands = DB::table('cs_brand')->get();?>
											<input type="text" name="brand_products" placeholder="Name" data-required="1" class="span3 m-wrap brand_products" style="margin-top:5px;" id="typeahead" data-provide="typeahead" data-source='[@foreach($brands as $row)"{{$row->name}}",@endforeach""]' autocomplete="off">
                                            <button type="submit" class="btn btn-primary b_search" id="b_search" name="submit_search" style="margin-top: -5px;" value="b_search">Search</button>
											<!-- <button type="submit" class="btn btn-success"><i class="icon-search icon-white"></i> Search</button> -->
                            		<!-- </form> -->
                            </div>

                            <div class="block-content collapse in">
                            @include('errors.error')	
                             	<center>
			                        <h3 class="cen_title text-center khmer_Moul">ការគ្រប់គ្រង់ម៉ាករបស់ផលិតផល</h3>

			                    	<div class="muted span3 pull-right" style="margin-bottom:5px;"><button class="btn btn-success btn_edit pull-right" value="add"><i class="icon-plus icon-white"></i> បង្កើតថ្មី</button></div>
			                    </center>
                            	<legend></legend>
                           
							    <table class="table table-bordered">

							      <thead style="background: rgb(251, 205, 205);">

							        <tr class="header">

							            <th>ល.រ</th>

							            <th>ឈ្មេាះ</th>

							            <th>បរិយារ</th>

							            <th>សកម្មភាព</th>

							        </tr>
							      </thead>
							      <tbody class="bread_list">
                                       
                                  </tbody>
                                    <tr>
                                        <td colspan="3">
                                            <b class="pull-right">សរុប:</b>
                                        </td>
                                        <td>
                                            <b id="total_all"></b>
                                        </td>
                                    </tr>
							    </table>
                            	<!-- Pagination -->
								<div class="pagination text-right"></div>
			    			</div>
                        <!-- Edit Brand -->
                            <div id="edit_me" class="modal hide">
                                    <div class="modal-header">
                                        <br/>
                                        <button class="close btn_close" type="button">&times;</button>
                                        <center><h3 class="cen_title">ម៉ាករបស់ផលិតផល</h3></center>
                                    </div>
                                    <div class="modal-body">
                                        <form role="form" id="form_insert_brands" name="form_insert_brands" method="POST" enctype="multipart/form-data">
                                        {{ csrf_field() }}  
                                            <div class="span8"  style="margin: 0 auto !important;float: none;">
                                                    <div class="span12" style="margin-left:0;">
                                                        <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">​ ឈ្មេាះ <span class="required" title="This place you must be put data">*</span></label>
                                                        <div class="controls">
                                                            <input type="text" id="name_brand" class="span12 m-wrap name_brand" name="name" data-required="1" autocomplete="off"/>
                                                        </div>
                                                        <p class="alert-danger">{{$errors->first('name_brand')}}</p>
                                                    </div>
                                                    <div class="span12" style="margin-left:0;">
                                                        <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">​ សេចក្ដីពិពណ៌នា <span class="required" title="This place you must be put data">*</span></label>
                                                        <div class="controls">
                                                            <textarea name="discription"  data-required="1" rows="5" class="span12 m-wrap discription"></textarea>
                                                        </div>
                                                        <p class="alert-danger">{{$errors->first('discription')}}</p>  
                                                    </div>
                                                    <div class="span12" style="margin-left:0;">
                                                        <br/>
                                                            <center>
                                                                    <button class="btn btn-success tbn_add" id="btn-save" name="btn_save" value="add">Submit</button>
                                                                    <input type="hidden" id="brand_id" name="brand_id" value="0">
                                                                    <button  class="btn btn-danger get_back">Back</button>
                                                            </center>  
                                                        <br/>
                                                    
                                                    </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            <!-- end Brand -->
						</div>
                     	<!-- /block -->
		    		</div>
                     <!-- /validation -->
                </div>

            </div>

<meta name="_token" content="{{ csrf_token() }}" />
<p id="model_in"></p>

<script>
$(document).ready(function(){
        var numpage = 1;
		var url_index1 = "{{route('products/brands_json')}}";
		get_page(url_index1,numpage);
   $(document).ajaxComplete(function(){    
        $(".b_search").click(function(){
            var submit_search = $(this).val();
            var n = 1;
            var url_index2 = submit_search; 		
            get_page(url_index2,numpage = n);
        });
        
        $(".pag").click(function(){
            var numpage = $(this).text();   
            get_page(url_index1,numpage);
        });

        $(".pre").click(function(){
            var numpage = $(this).find(".pre_in").val();
            get_page(url_index1,numpage);
        });

        $('.btn_edit').click(function(){
           var btn_edit = $(this).val();
            if(btn_edit === "add"){
               $("#brand_id").val("0");
            }else{
                var name = $(this).parents('tr').find('#name_b').text();
                var desp = $(this).parents('tr').find('#desp_b').text();
                var brand_id = btn_edit;
                $(".name_brand").val(name);
                $(".discription").val(desp);
                $("#brand_id").val(brand_id);
                $("#btn-save").val("edit");
            }
                $("#edit_me").addClass("in");
                $("#edit_me").css({'display':'block'});
                $("#edit_me").attr("aria-hidden","false");
                $("#model_in").html("<div class='modal-backdrop  in'></div>");
        });

         $(".btn_deleted").click(function(e){
            
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })
                var brand_id = $(this).val();
                var url1 = "{{ route('products/brands') }}";
                var url_index = url1+"/"+brand_id+"/deleted";
                e.preventDefault();
                $.ajax({
                    type: "POST",
                    url: url_index, 
                    dataType: "json",
                    success: function(result){
                        $('.msg_show').html(result.msg_show); 
                                var numpage = 1 ;
                                var url_index1 = "{{ route('products/brands_json') }}";
                                get_page(url_index1,numpage);
                    },
                    error: function (result ,status, xhr) {
                        console.log(result.responseText);
                    }                   
                });
        });

   });
        function get_page(url,n){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                })
                var brand_products = $(".brand_products").val(); 
                if(url === "b_search" || brand_products !== ""){
                    var url_i = "{{route('products/brands_json')}}";
                    var forData = {
                                    brand_products: $(".brand_products").val(),
                                    submit_search: $(".b_search").val()
                                }                
                    var url_index = url_i+"?brand_products="+brand_products+"&submit_search=b_search&page="+n;     
                }else{
                    var forData = {};
                    var url_index = url+"?page="+n;
                }
			    var client;
                    $.ajax({
                            type: "GET",
                            url: url_index, 
                            dataType: "json",
                            data: forData,
                            // async: false,
                            success: function(result){
                                console.log(result);
                                $("#total_all").text(result.total);
                                        $.each(result.data, function(i, field){
                                                var il = result.from  + i;
                                        
                                                client += "<tr>";
                                                    client += "<td>"+ il +"</td>";
                                                    client += "<td id='name_b'>"+ field.name +"</td>";
                                                    client += "<td id='desp_b'> "+ field.description +"</td>";
                                                    client += "<td>";
                                                    client += "<button  class='btn btn-primary btn_edit' id='btn_edit'  value='"+ field.id +"'>Edit</button>  ";
                                                    client += "<button  class='btn btn-danger btn_deleted' ' value='"+ field.id +"'>Delete</button>";
                                                    client += "</td>";
                                                client += "</tr>";
                                        });
                                    $(".bread_list").html(client);	
                                    var page = "";
                                    if(result.prev_page_url === null){
                                        var pr_url = result.current_page;
                                    }else{
                                        var pr_url = result.current_page -1;
                                    }
                                    page += "<a class='pre'>&laquo;<input type='hidden' class='pre_in' value='"+pr_url+"' ></a>";
                                    for(var x = 1; x <= result.last_page; x ++  ) {
                                        if(result.current_page === x){
                                            page += "<a class='pag active' >"+x+"</a>";
                                        }else{
                                            page += "<a class='pag' >"+x+"</a>";
                                        }
                                    }
                                    if(result.next_page_url === null){
                                        var ne_url = result.current_page;
                                    }else{
                                        var ne_url = result.current_page +1;
                                    }
                                    page += "<a class='pre'>&raquo;<input type='hidden' class='pre_in' value='"+ne_url+"' ></a>";
                                    $(".pagination").html(page );

                        },
                        error: function (result ,status, xhr) {
                            console.log(result.responseText);
                        }
                    
                    });
                   
		}    
        
       
        $('#btn-save').click(function(e){

             var btn_edit = $(this).val();
             $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                })
            // brand_id   
                if(btn_edit == "edit"){
                    var brand_id  = $("#brand_id").val();
                    var url1 = "{{ route('products/brands') }}";
                    var url_index = url1+"/"+brand_id+"/edit";
                 
                }else{
                    var brand_id  =  0; 
                    var url_index = "{{ route('products/brands/create') }}";  
                  
                }
                
                var forData = {
                                name_brand: $(".name_brand").val(),
                                discription_get: $(".discription").val()
                              }

                 e.preventDefault();
                
                $.ajax({
					type: "POST",
					url: url_index, 
					dataType: "json",
					data: forData,
					success: function(result){
                        console.log(result);
                        $("#edit_me").removeClass("in");
                        $("#edit_me").css({'display':'none'});
                        $("#edit_me").attr("aria-hidden","true");
                        $("#model_in").html("");
                        $("#brand_id").val(0);
                        $("#btn-save").val("add");
                        $('.msg_show').html(result.msg_show); 
                        $('#form_insert_brands').trigger("reset");
								var numpage = 1 ;
								var url_index1 = "{{ route('products/brands_json') }}";
								get_page(url_index1,numpage);

                    },
                    error: function (result ,status, xhr) {
                        console.log(result.responseText);
                    }                   
                });
            });
    


        $(".btn_close,.get_back").click(function(event){
            event.preventDefault()
            $("#edit_me").removeClass("in");
            $("#edit_me").css({'display':'none'});
            $("#edit_me").attr("aria-hidden","true");
            $("#model_in").html("");
            $("#brand_id").val(0);
            $("#btn-save").val("add");
            $('#form_insert_brands').trigger("reset");

        });
 });       
</script>            
@stop()
