@extends('credit_sale.layout.master')
@section('contend')

<div class="container-fluid">
            <div class="row-fluid">
                     <!-- validation -->
                    <div class="row-fluid">
                         <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                    <div class="muted pull-left"><a href="{!! url('accounts') !!}"> គណនី</a> <span class="divider">|</span> ការបង្ហាញកិច្ចសន្យាទិញបង់រំលស់ </div>		
                                    <!-- <button id="back_to_prev" class="btn btn-danger pull-right back_to_prev" style="font-family: 'Hanuman' !important;" onclick="back_to_prev()"><i class=" icon-arrow-left icon-white"></i> back </button> -->
                            </div>

                            <div class="block-content collapse in">
                            @include('credit_sale.client_management.menu_client')
                            <button class="btn btn-success pull-left" id="print_hide" style=" margin-right: 10px; font-family: 'Hanuman' !important;" onclick="with_print()"><i class="icon-print icon-white"></i> ព្រីនកិច្ចសន្យាទិញបង់រំលស់ </button>   
                            <!-- <button id="generate_schedule" class="btn btn-info pull-left generate_schedule" style="margin-right: 10px;font-family: 'Hanuman' !important;"  name="generate_schedule"><i class="icon-th-list icon-white"></i> ការបង្កើតកាលវិភាគសងប្រាក់អតិថិជន </button> -->

                            @include('errors.error')	
                             	
			                        <h3 class="cen_title text-center khmer_Moul ">{{$title}}</h3>
			                   
                            	<legend></legend>
                                
                                 
                                <div class="span8"  style="margin: 0 auto !important;float: none;">
                                                    <div class="span12" style="margin-left:0;">
                                                        <table class="table table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th>ឈ្មោះទំនិញ</th>
                                                                <th>លេខ​កូដ​</th>
                                                                <th>ម៉ាក</th>
                                                                <th>ចំនួន</th>
                                                                <th>តម្លៃ​ឯកតា</th>
                                                                <th>តម្លៃសរុប</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody class="item_list">
                                                           
                                                        </tbody>
                                                                <tr>
                                                                    <td colspan="5" style="text-align: right;"> តម្លៃសរុប </td>
                                                                    <td> <b class="prices_total_num_text"></b> </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="5" style="text-align: right;"> ប្រភេទនៃការទិញ </td>
                                                                    <td> <b class="method"></b> </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="5" style="text-align: right;"> ប្រាក់កក </td>
                                                                    <td> <b class="deposit_fixed_text"></b> </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="5" style="text-align: right;">  រយៈពេលបង់ប្រាក់</td>
                                                                    <td> <b class="duration_pay_money_text"></b>  / <b class="duration_pay_money_type_text"></b></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="5" style="text-align: right;">  សុំបង់ថ្ងៃទី</td>
                                                                    <td> <b class="date_for_payments_text"></b></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="5" style="text-align: right;">ចំនួនទឹកប្រាក់</td>
                                                                    <td> <b class="money_owne_text"></b> </td>
                                                                </tr>
                                                            
                                                        </table>  
                                                        <br/>
                                                <legend></legend> 
                                            </div>
                                         </div>
			    			</div>
                        <!-- Edit Approval -->
                        
						</div>
                     	<!-- /block -->
		    		</div>
                     <!-- /validation -->
                </div>

            </div>
        <script>

            function with_print(){
                window.location= "{{ url('accounts/credit_sale/'.$data_id.'/print_contract') }}";
                
            }
            function back_to_prev(){
                window.history.back();
                
            }
        </script>

<meta name="_token" content="{{ csrf_token() }}" />

<script type="text/javascript">
$(document).ready(function(){
    $(window).load(function(){
        get_sale_credit_sale_show();
    });
        function get_sale_credit_sale_show(){
            var url_json =  "{{ url('accounts/aprove_credit_sales/'.$data_id.'/show_detail_json') }}";
                    $.ajax({
                        type: "GET",
                        url: url_json, 
                        dataType: "json",
                        success: function(result){
                            var url_link = "{{ url('accounts/schedule/'.$data_id) }}";
                            if(result.schedule != null){
                                var btn_chachge = " <a href='"+url_link+"' id='show_schedule' class='btn btn-info pull-left show_schedule'  style='font-family: Hanuman !important;' rel='generate_schedule'><i class='icon-eye-open icon-white'></i> ការបង្ហាញកាលវិភាគសងប្រាក់អតិថិជន </a>";
                                $("#generate_schedule").after(btn_chachge);
                                // $("#generate_schedule").remove();
                              
                            }
                            // else{
                            //     var btn_chachge = " <button id=generate_schedule class=btn btn-info pull-left generate_schedule style=font-family: 'Hanuman' !important; name=generate_schedule><i class=icon-th-list icon-white></i> Generate Schedule </button>";
                            //     $("#generate_schedule").after(btn_chachge);
                            //     $("#generate_schedule").remove();
                                    
                            // }
                            console.log(result);
                            var text = "";
                            $.each(result.approval_item,function(i,da){
                                text += "<tr> " ;
                                // text += "<td>  <button class='btn btn-danger btn-mini remove_item' value='"+da.product_id+"'><i class='icon-remove icon-white ' style='padding-right: 0px;'></i></button>   "+da.item.name+" </td>";
                                text += "<td> "+da.item.name+" </td>";
                                text +=  "<td> "+da.item.item_bacode+"  </td>";
                                text +=  "<td> "+da.item.categorys.name+" </td>"
                                text +=  "<td> "+da.qty+" </td>";
                                text +=  "<td>  "+accounting.formatMoney(da.sell_price)+" </td>";
                                text +=  "<td>  "+accounting.formatMoney(da.total_price_payment)+" </td>";
                                text += "</tr> "; 
                               
                            });

                            $(".item_list").html(text); 
                            if(result.method == "sale_by_credit"){
                                var mothod = "បង់រំលស់";
                            }else{
                                var mothod = "បង់ជាសាច់ប្រាក់";   
                            }
                            var  dpm = result.date_for_payments;
                            var d = new Date(dpm);
                            var year_n = d.getFullYear();
                            var month_n = d.getMonth() + 1;
                            var day_n = d.getDate();
                            if(month_n > 10){
                                month_n = month_n;
                            }else{
                                month_n = "0"+month_n; 
                            }
                            if(day_n > 10){
                                day_n = day_n;
                            }else{
                                day_n = "0"+day_n; 
                            }
                           var  date_for_payments = day_n +"-"+month_n+"-"+year_n;
                           var dpmt = result.duration_pay_money_type;
                            if(dpmt == "month"){
                                dpmt = "ខែ";
                            }else if(dpmt == "2week"){
                                dpmt = "២ សប្តាហ៍";
                            }else if(dpmt == "week"){
                                dpmt = "សប្តាហ៍";
                            }else if(dpmt == "day"){
                                dpmt = "ថ្ងៃ";
                            }  
                            $(".method").text(mothod); 
                            $(".prices_total_num_text").text(accounting.formatMoney(result.prices_total_num));
                            $(".deposit_fixed_text").text(accounting.formatMoney(result.deposit_fixed));
                            $(".duration_pay_money_text").text(result.duration_pay_money);
                            $(".duration_pay_money_type_text").text(dpmt);
                            $(".date_for_payments_text").text(date_for_payments );
                            $(".money_owne_text").text(accounting.formatMoney(result.money_owne));    
                            
                        },
                        error: function (result ,status, xhr) {
                            console.log(result.responseText);
                        }
                    
                    });
        }
   $(".generate_schedule").one("click", function(e){ 
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });
            e.preventDefault();
            var url_sc_gen = "{{ url('aprove_credit_sales/'.$data_id.'/generate_credit_sale') }}"  ;    
            $.ajax({
                type: "POST",
                url: url_sc_gen, 
                dataType: "json",
                success: function(data){
                    console.log(data);
                    $('.msg_show').html(data.msg_show); 
                    window.location="{{ url('accounts/schedule/'.$data_id) }}";        
                },
                error: function (data ,status, xhr) {
                    console.log(data.responseText);
                }                   
            });
    });
 });       
</script>            
@stop()
