<!DOCTYPE html>
<!-- saved from url=(0054)http://mfi.titbserver.com/new_print_contract/1039/show -->
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>កិច្ចសន្យាខ្ចីប្រាក់</title>
	<link href="{{url('printing/css')}}" rel="stylesheet">

	<link rel="stylesheet" type="text/css" href="{{url('printing/form.css')}}">
	<!-- Bootstrap -->
	<link href="{{url('printing/bootstrap.min.css')}}" rel="stylesheet" media="screen">
	<script>
		function myFunction() {
		    window.print();
		}
	</script>
	<style type="text/css">
		@font-face {
			font-family: "Battambang";
			src: local("Battambang"), url("assets/fonts/KhmerOSbattambang.woff") format("woff"), url("assets/fonts/KhmerOSbattambang.woff") format("truetype");
		}

		@media screen {

			header.onlyprint,
			footer.onlyprint {
				display: none;
				/* Hide from screen */
			}

			.content-main {
				margin: 50px;
			}

			body {
				font-family: Battambang;
			}

			ol {
				list-style: none;
			}

		}

		@media print {
			body {
				font-family: Battambang;
				counter-reset: page;
			}

			ol {
				list-style: none;
			}

			header.onlyprint {
				position: fixed;
				/* Display only on print page (each) */
				top: 25px;
				/* Because it's header */
				font-size: 11px;
			}

			footer.onlyprint {
				position: fixed;
				bottom: 25px;
				/* Because it's footer */
				font-size: 11px;
			}

			.content-main {
				margin: 30px 0px;
			}

			.page-break {
				page-break-after: always;
			}

			footer.onlyprint span:after {
				margin-left: 580px;
				/*content: counter(page);
			  counter-increment: page;*/
				counter-increment: page;
				content: counter(page);
			}
		}
	</style>

</head>

<body class="font_load-header">
	@include('credit_sale.client_management.menu_client')
	<div class="container-fluid">
		<div class="row-fluid ">
			<button id="printPageButton" onclick="myFunction()" class="btn btn-primary">Print</button>
			<header class="onlyprint">
				<span>ក្រុមហ៊ុន ខេ.អេ.អេស.ភី. កសិករ ពាណិជ្ជ ឯ.ក</span>
				<span style="margin-left:450px;font-size:11px;">ទម្រង់ទី ០៣</span>
				<hr style="width:1000px;margin-top:0px;">
			</header>
			<div class="content-main">
				<div class="span12 header main-title">
					<center>
						ព្រះរាជាណាចក្រកម្ពុជា<br>
						ជាតិ សាសនា ព្រះមហាក្សត្រ
					</center>
				</div>
				<div class="span12 main-title header_left">
					ខេ.អេ.អេស.ភី. កសិករ ពាណិជ្ជ ឯ.ក<br>
					សាខា: <span>អង្គតាសោម</span>
				</div>
				<div style="clear:both;"></div>
				<div class="main-title span12">
					<center style="margin-top:20px;">
						កិច្ចសន្យាខ្ចីប្រាក់
						<p>ធ្វើនៅ <span>អង្គតាសោម</span> ថ្ងៃទី <span>27 </span>ខែ <span>07 </span>ឆ្នាំ <span>2018</span></p>
						រវាង
					</center>
				</div>
				<div class="span12 content">
					<ol>
						<li>១. ភាគីឲ្យខ្ចីប្រាក់: &nbsp;&nbsp;ខេ.អេ.អេស.ភី. កសិករ ពាណិជ្ជ ឯ.ក &nbsp;សាខា <span>អង្គតាសោម</span>
							&nbsp;តំណាងពេញច្បាប់&nbsp;&nbsp;ដោយលោក / លោកស្រី

							<span>
								ណុប ប៊ុនណាត
							</span>
							តួនាទីជា
							<span>
								ប្រធានក្រុមឥណទាន
							</span>តទៅនេះហៅកាត់ថាភាគី (ក)</li>
					</ol>

				</div>
				<div class="span12 title_center main-title">
					<center>និង</center>
				</div>
				<div class="span12 content li_height">
					<ol>
						<li>
							២. ភាគីសុំខ្ចីប្រាក់: &nbsp;ឈ្មោះ <span>ហេង ថាវី</span>
							ភេទ <span> ស្រី
							</span>
							ថ្ងៃខែឆ្នាំកំណើត <span>03-07-1961</span>
							សញ្ជាតិ <span>
								ខ្មែរ
							</span>
							កាន់ប័ណ្ណសំគាល់អត្តសញ្ញាណលេខ <span>100603273</span>
							ប្រភេទប័ណ្ណ <span>
								អត្តសញ្ញាណប័ណ្ណ

							</span>
							ចុះថ្ងៃទី <span>29-11-2011</span>
							ចេញដោយ <span>
								ក្រសួងមហាផ្ទៃ
							</span>
							និងឈ្មោះ <span>
								................
							</span>
							ភេទ <span>
								.........
							</span>
							ថ្ងៃខែឆ្នាំកំណើត​ <span>
								................
								សញ្ជាតិ <span>
									ខ្មែរ
								</span>
								កាន់ប័ណ្ណសំគាល់អត្តសញ្ញាណលេខ <span>
									................
								</span>
								ប្រភេទប័ណ្ណ <span>
									អត្តសញ្ញាណប័ណ្ណ

								</span>
								ចុះថ្ងៃទី <span>
									................
									ចេញដោយ <span>
										ក្រសួងមហាផ្ទៃ
									</span> ។<br>
									អាសយដ្ឋានផ្ទះលេខ <span>
										........
									</span>
									ក្រុមទី <span>
										........
									</span>
									ផ្លូវលេខ <span>
										........
									</span>
									ភូមិ <span>អង្គតាសោម</span>
									ឃុំ/សង្កាត់ <span>ឃុំអង្គតាសោម</span>
									ស្រុក/ខណ្ឌ <span>ស្រុកត្រាំកក់</span>
									ខេត្ត/ក្រុង <span>ខេត្តតាកែវ</span>
									តទៅហៅកាត់ថាភាគី (ខ)។<br>
								</span></span></li>
					</ol>
					<p style="margin-left: 23px;" class="paragrap_p">
						ភាគីទាំងពីរបានព្រមព្រៀងចុះកិច្ចសន្យាខ្ចីប្រាក់ ដោយអនុវត្តតាមរាល់ប្រការដូចខាងក្រោម៖<br>
					</p>

				</div>
				<div class="main-title span12">
					ប្រការ១៖ អំពីលក្ខខណ្ឌរួម
				</div>
				<div class="span12 content">
					<p style="margin-left: 23px;" class="paragrap_p">
						ឈរលើមូលដ្ឋាននៃពាក្យសុំខ្ចីប្រាក់សេចក្តីណែនាំស្តីពីគោលការណ៍ និង លក្ខខណ្ឌនៃការឲ្យខ្ចីប្រាក់ ភាគី (ក)
						យល់ព្រមឲ្យភាគី(ខ) ខ្ចីប្រាក់ ហើយភាគី (ខ) ក៏យល់ព្រមទទួល និងសន្យាសងមកភាគី (ក) វិញជាដាច់ខាតនូវប្រាក់ខ្ចីនេះតាមចំនួន
						និងរាល់លក្ខខណ្ឌដែលបានព្រមព្រៀងដូចតទៅនេះ
					</p>
					<ul type="square" style="margin-left:43px;">
						<li>
							ប្រាក់ខ្ចីដែលភាគី (ខ) បានទទួលចំនួនជាលេខ
							<span>1,353,600.00
								រៀល

							</span>
							<!-- ជាអក្សរ <span>មួយលានរៀលគត់</span> -->
						</li>
						<li>
							រយៈពេលខ្ចីប្រាក់ <span>6</span>
							គិតជា <span>
								ខែ
							</span>
							គិតចាប់ពីថ្ងៃដែលបានទទួលប្រាក់តទៅ ។
						</li>
						<li>របៀបសងបំណុលៈ អនុវត្តន៍តាមតារាងកាលវិភាគសងប្រាក់</li>
						<li>
							អត្រាការប្រាក់ <span>
								1.4% </span>
							ក្នុងមួយ <span>

								ខែ

							</span>គិតលើប្រាក់ដើមនៅជំពាក់សរុប ។
						</li>
						<li>ប្រាក់សន្សំកាតព្វកិច្ច <span>.......</span> ក្នុងមួយវគ្គនៃការខ្ចីប្រាក់ ដោយអនុលោមតាមតារាងកាលវិភាគសងប្រាក់។
						</li>
						<li class="page-break">ទីកន្លែងបង់ប្រាក់ និងឧបសម្ព័ន្ធ (ក) បង់នៅការិយាល័យ
							ឬនៅតាមភូមិនៃតំបន់ប្រតិបត្តិការរបស់ស្ថាប័ន(ខ)តារាងកាលវិភាគសងប្រាក់ (គ) សក្ខីប័តបើកប្រាក់ (ឃ) កិច្ចសន្យាបញ្ចាំ (ង)
							កិច្ចសន្យាបញ្ចាំជំនួស។
						</li>
					</ul>
				</div>
				<div class="main-title span12" style="margin-top:50px;">
					ប្រការ២៖ អំពីលក្ខខណ្ធពិសេស
				</div>
				<div class="span12 content li_list">
					<p style="margin-left: 23px;" class="paragrap_p">
						២.១ ភាគី (ខ) ត្រូវប្រើប្រាស់ប្រាក់កម្ចីសម្រាប់គោលបំណងដែលបានស្នើសុំក្នុងពាក្យសុំខ្ចី ក្នុងករណីប្រើប្រាស់ខុសភាគី
						(ក) មានសិទ្ធិឲ្យភាគី (ខ) សងបំណុលទាំងអស់ដែលនៅសល់ត្រឡប់មកវីញមុនកាលកំណត់។
					</p>
					<p style="margin-left: 23px; margin-top:20px;" class="paragrap_p">
						២.២ ប្រសិនបើភាគី (ខ) មិនបានគោរពលក្ខខណ្ឌទាំងឡាយនៃកិច្ចសន្យានេះ ហើយមិនបានបង់ប្រាក់រំលោះជាច្រើនលើកច្រើនសារ ភាគី (ក)
						មានសិទ្ធិប្តឹងទៅស្ថាប័នមានសមត្ថកិច្ច ដើម្បីទាមទារឲ្យសងប្រាក់ត្រឡប់មកវីញ ទោះបីមុនកាលកំណត់នៃការសងប្រាក់ក៏ដោយ។ ភាគី
						(ក) មានសិទ្ធិដកប្រាក់សន្សំកាតព្វកិច្ចរបស់ភាគី (ខ) មកទូរទាត់ការបង់ប្រចាំវគ្គក្នុងករណី ភាគី (ខ) បង់យឺត។
					</p>
					<p style="margin-left: 23px;" class="paragrap_p">
						២.៣ ភាគី (ក) មានសិទ្ធិចុះទៅពិនិត្យកន្លែងប្រកបអាជីវកម្ម និងផ្ទះរបស់ភាគី (ខ) ដោយមិនចាំបាច់ជូនដំណឹងជាមុន។
					</p>
					<p style="margin-left: 23px;" class="paragrap_p">
						២.៤ ភាគី (ក) តម្រូវឲ្យភាគី (ខ) តម្កល់ប្រាក់សន្សំកាតព្វកិច្ចចំនួនសរុបមួយវគ្គនៃការខ្ចី បើភាគី(ខ) នៅបន្តការខ្ចី ។
					</p>
					<p style="margin-left: 23px;" class="paragrap_p">
						២.៥ ក្នុងករណី ភាគី (ខ) ខកខានមិនបានសងប្រាក់គ្រប់ចំនួនតាមថ្ងៃកំណត់ដូចមានក្នុងតារាងកាលវិភាគសងប្រាក់នោះ ភាគី (ខ)
						យល់ព្រមបង់ប្រាក់ពិន័យឲ្យ ភាគី (ក)។ ប្រាក់ពិន័យនេះត្រូវគណនាដោយយក ចំនួនទឹកប្រាក់សរុបដែលខកខានមិនបានសងគុណនិងអត្រា
						០.៥% (ភាគរយ) ទៅតាមចំនួនថ្ងៃដែលបានយឺតយ៉ាវ។
					</p>
					<p style="margin-left: 23px" class="paragrap_p">
						២.៦ ក្នុងករណីមរណៈភាព ឬ ក្នុងករណីមិនអាចអនុវត្តកិច្ចសន្យាបានមុនចប់អាណត្តិ អ្នកស្នងមរតកស្របច្បាប់ ទាយាទរបស់ភាគី (ខ)
						ជាអ្នកអនុវត្តកិច្ចសន្យាបន្ត។
					</p>
				</div>
				<div class=" main-title span12">
					ប្រការ៣៖ ការធានាកតព្វកិច្ច
				</div>
				<div class="span12 content li_list">
					<p style="margin-left: 23px;" class="paragrap_p">
						៣.១ ឈ្មោះ <span>នេត ឡេង</span>
						ភេទ <span>
							ស្រី
						</span>
						ថ្ងៃខែឆ្នាំកំណើត​ <span>04-05-1956</span>
						សញ្ជាតិ <span>
							ខ្មែរ
						</span>
						កាន់ប័ណ្ណសំគាល់អត្តសញ្ញាណលេខ <span>3716</span>
						ប្រភេទប័ណ្ណ <span>

							សំបុត្របញ្ជាក់កំណើត



						</span>
						ចុះថ្ងៃទី <span>23-03-2005</span>
						ចេញដោយ <span>

							ក្រសួងមហាផ្ទៃ

						</span>
						និងឈ្មោះ <span>..................</span>
						ភេទ <span>..................</span>
						ថ្ងៃខែឆ្នាំកំណើត <span>..................</span>
						សញ្ជាតិ <span>..................</span>
						កាន់ប័ណ្ណសំគាល់អត្តសញ្ញាណលេខ <span>..................</span>
						ប្រភេទប័ណ្ណ <span>..................</span>
						ចុះថ្ងៃទី <span>..................</span>
						ចេញដោយ <span>..................</span> ។ <br>
						អាសយដ្ឋានផ្ទះលេខ <span>
							........
						</span>
						ក្រុមទី <span>
							........
						</span>
						ផ្លូវលេខ <span>
							........
						</span>
						ភូមិ <span>មឿងចារ</span>
						ឃុំ/សង្កាត់ <span>ជាងទង</span>
						ស្រុក/ខណ្ឌ <span>ត្រាំកក់</span>
						ខេត្ត/ក្រុង <span>ខេត្តតាកែវ</span>តទៅនេះហៅកាត់ថាភាគី (គ) ។
					</p>
					<p style="margin-left: 23px;" class="paragrap_p">
						៣.២ ដើម្បីធានាចំពោះការបំពេញកតព្វកិច្ចរបស់ខ្លួនក្រោមកិច្ចសន្យាខ្ចីប្រាក់នេះ ភាគី (ខ) បានសុខចិត្តដាក់ទ្រព្យសម្បត្តិ
						ផ្ទាល់ខ្លួន និងផលប្រយោជន៍ប្រាតិភោគលើទ្រព្យសម្បត្តិទាំងនេះបញ្ចាំជូន ភាគី (ក) ដូចមានចែង
						និងកំណត់ក្នុងកិច្ចសន្យាបញ្ចាំ។
					</p>
					<p style="margin-left: 23px;" class="paragrap_p page-break">
						៣.៣ ដើម្បីធានាចំពោះការបំពេញកតព្វកិច្ចជំនួសភាគី(ខ) ក្រោមកិច្ចសន្យាខ្ចីប្រាក់នេះភាគី (គ)
						សុខចិត្តដាក់ទ្រព្យសម្បត្តិផ្ទាល់ខ្លួន និងផលប្រយោជន៍ប្រាតិភោគលើទ្រព្យសម្បត្តិទាំងនេះបញ្ចាំជូនភាគី (ក) ដូចមានចែង
						និងកំណត់ក្នុងកិច្ចសន្យាបញ្ចាំ ។
					</p>
					<p style="margin-left: 23px;margin-top:50px;" class="paragrap_p">
						៣.៤ ក្នុងករណីភាគី (ខ) គ្មានលទ្ធភាពសងប្រាក់ជូនភាគី (ក) ទេ នោះភាគី (គ) នឹងមានកាតព្វកិច្ចសងប្រាក់ជំពាក់ទាំងអស់
						ដែលមិនទាន់បានសងជំនួសភាគី (ខ) ឲ្យគ្រប់ចំនួនទាំងដើម ការប្រាក់ និងប្រាក់សន្សំ។
					</p>
				</div>
				<div class=" main-title span12" style="">
					ប្រការ៤៖ អំពីលក្ខខណ្ឌអវសាន
				</div>
				<div class="span12 content li_list">
					<p style="margin-left: 23px;" class="paragrap_p">
						៤.១ ភាគី (ក) ភាគី (ខ) និងភាគី (គ) សន្យាគោរពយ៉ាងមឺងម៉ាត់តាមរាល់ប្រការនៃកិច្ចសន្យានេះ។ ក្នុងករណីមានការអនុវត្តផ្ទុយ
						ឬដោយរំលោភលើលក្ខខណ្ឌ ណាមួយនៃកិច្ចសន្យានេះ ភាគីដែលល្មើសត្រូវទទួល ខុសត្រូវចំពោះមុខច្បាប់ជាធរមាន។
						រាល់សោហ៊ុយចំណាយទាក់ទងក្នុងការដោះស្រាយវិវាទ ជាបន្ទុករបស់ភាគីរំលោភបំពានលើកិច្ចសន្យា។
					</p>
					<p style="margin-left: 23px;" class="paragrap_p">
						៤.២ ភាគីទាំងអស់បានអាន និងយល់យ៉ាងច្បាស់អំពីអត្ថន័យនៃកិច្ចសន្យា
						ហើយស្ម័គ្រចិត្តគោរពយ៉ាងមឺងម៉ាត់រាល់ប្រការទាំងអស់ដែលមានចែងនៅក្នុងកិច្ចសន្យាខ្ចីប្រាក់ប្រាក់នេះដោយគ្មានកាបង្ខិតបង្ខំឡើយ។
					</p>
					<p style="margin-left: 23px;" class="paragrap_p">
						៤.៣ កិច្ចសន្យា និងឧបសម្ព័ន្ធនៃកិច្ចសន្យានេះមានសុពលភាពចាប់ពីថ្ងៃចុះហត្ថលេខា និងផ្តិតស្នាមមេដៃស្តាំតទៅ។
					</p>
					<p style="margin-left: 23px;" class="paragrap_p">
						៤.៤ កិច្ចសន្យានេះត្រូវបានធ្វើឡើងជា ០២ច្បាប់ជាភាសាខ្មែរ (១ច្បាប់ដើមរក្សាទុកនៅស្ថាប័ន
						និង១ច្បាប់ថតចម្លងជូនអតិថិជនរក្សាទុក)
					</p>
				</div>
				<div class="span12 font-title">
					<div class="span4" style="float:left; width:250px" id="column-left">
						<center>
							ស្នាមមេដៃស្តាំភាគីខ្ចីប្រាក់ ភាគី (ខ)
						</center>
						<center>
							<div class="span4 sub_col1_1" style="float: left; ">
								<p>អ្នករួមខ្ចីប្រាក់</p>
							</div>
							<div class="span4" style="float: left;">
								<p>អ្នកសុំខ្ចីប្រាក់</p>
							</div>
						</center>
					</div>
					<div class="span4" style="float:left;width:220px" id="column-center">
						<center>
							ស្នាមមេដៃស្តាំអ្នកធានា
						</center>
					</div>
					<div class="span4" style="float:right; width:200px;" id="column-right">
						<center>
							តំណាងឲ្យអ្នកខ្ចីប្រាក់ ភាគី(ក)
						</center>
						<div class="span4">
							<center>
								<p>ហត្ថលេខា និងឈ្មោះ</p>
							</center>
						</div>
					</div>
				</div>
				<div class="span12 br_line"></div>
				<div class="span12">
					<div class="span6" style="float:left;">
						<div class="span3" style="float:left;padding-right: 3px;margin-right: 60px;">
							<p>ឈ្មោះ
								................
							</p>
						</div>
						<div class="span3" style="float:right;">
							<p>ឈ្មោះ
								ហេង ថាវី
							</p>
						</div>
						<div style="clear:both;"></div>
					</div>
					<div class="span6">
						<div class="span3" style="float:left;margin-left:60px;">
							<p>ឈ្មោះ
								នេត ឡេង
							</p>
						</div>
						<div class="span3" style="float:left;margin-left:100px;">
							<p>ឈ្មោះ

								ណុប ប៊ុនណាត

							</p>
						</div>
					</div>
					<div style="clear:both;"></div>
				</div>
				<!-- =========================== -->
				<div class="span12 font-title">

					<div class="span6 " style="float:left; margin:160px  90px 0 0px; height:189px;">
						<center>
							បានឃើញ និងបញ្ជាក់ថាសេចក្តីបញ្ជាក់របស់<br>
							មេភូមិ...............................................<br>
							<p>ថ្ងៃទី..................ខែ.....................ឆ្នាំ....................</p>
							មេឃុំ/ចៅសង្កាត់.....................................
							<br>ហត្ថលេខា និងត្រា

						</center>
					</div>
					<div class="span5" style="float:right;margin-top:70px;">
						<center>
							បានឃើញ និងទទួលស្គាល់ថា<br>
							<p style=" width:310px; text-align:justify;">កម្មសិទ្ធិស្របច្បាប់របស់ភាគី(ខ) ប្រាកដមែន ហើយទ្រព្យទាំងនេះពុំមាន
								ពាក់ព័ន្ធនិងបញ្ហាអ្វីឡើយ ហើយភាគី(ខ) បានយល់ព្រម ដាក់បញ្ចាំដោយ ស្ម័គ្រចិត្តឲ្យ ក្រុមហ៊ុន ខេ.អេ.អេស.ភី. កសិករ
								ពាណិជ្ជ ឯ.ក ពិតប្រាកដមែន។ <br>
							</p>
							<p>ថ្ងៃទី..................ខែ.....................ឆ្នាំ....................</p>
							មេភូមិ................................................
							<br>ហត្ថលេខា និងឈ្មោះ
							<p></p>
						</center>
						<input type="hidden" name="client_id" value="{{$client_id}}">
					</div>
				</div>
			</div>
			<footer class="onlyprint">
				<hr style="width:1000px;margin-bottom:0px;">កិច្ចសន្យាខ្ចីប្រាក់
				<!-- <span></span> -->
			</footer>

		</div>
	</div>


</body>

</html>