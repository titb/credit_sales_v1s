
<html>
   <head>
      <meta http-equiv=Content-Type content="text/html; charset=utf-8">
      <meta name=ProgId content=Word.Document>
      <meta name=Generator content="Microsoft Word 15">
      <meta name=Originator content="Microsoft Word 15">

      <title>កិច្ចសន្យាទិញបង់រំលស់</title>
      <script src="{{ url('assets/jquery.min.js')}}"></script>
      <script src="{{ URL::to('assets/accounting.min.js')}}"></script>
      <style>
    
        /* Font Definitions */
        @font-face
        {font-family:Wingdings;
        panose-1:5 0 0 0 0 0 0 0 0 0;
        mso-font-charset:2;
        mso-generic-font-family:auto;
        mso-font-pitch:variable;
        mso-font-signature:0 268435456 0 0 -2147483648 0;}
        @font-face
        {font-family:"Cambria Math";
        panose-1:2 4 5 3 5 4 6 3 2 4;
        mso-font-charset:0;
        mso-generic-font-family:roman;
        mso-font-pitch:variable;
        mso-font-signature:-536869121 1107305727 33554432 0 415 0;}
        @font-face
        {font-family:"Khmer OS Battambang";
        panose-1:2 0 5 0 0 0 0 2 0 4;
        mso-font-charset:0;
        mso-generic-font-family:auto;
        mso-font-pitch:variable;
        mso-font-signature:-1610612497 1342185546 65536 0 273 0;}
        @font-face
        {font-family:"Khmer OS";
        panose-1:2 0 5 0 0 0 0 2 0 4;
        mso-font-charset:0;
        mso-generic-font-family:auto;
        mso-font-pitch:variable;
        mso-font-signature:-1610612497 1342185546 65536 0 273 0;}
        @font-face
        {font-family:"Limon S1";
        panose-1:0 0 0 0 0 0 0 0 0 0;
        mso-font-charset:0;
        mso-generic-font-family:auto;
        mso-font-pitch:variable;
        mso-font-signature:3 0 0 0 1 0;}
        @font-face
        {font-family:"Limon S6";
        mso-font-alt:Calibri;
        mso-font-charset:0;
        mso-generic-font-family:auto;
        mso-font-pitch:variable;
        mso-font-signature:3 0 0 0 1 0;}
        @font-face
        {font-family:Tahoma;
        panose-1:2 11 6 4 3 5 4 4 2 4;
        mso-font-charset:0;
        mso-generic-font-family:swiss;
        mso-font-pitch:variable;
        mso-font-signature:-520081665 -1073717157 41 0 66047 0;}
        @font-face
        {font-family:"Khmer OS Muol";
        panose-1:2 0 5 0 0 0 0 2 0 4;
        mso-font-charset:0;
        mso-generic-font-family:auto;
        mso-font-pitch:variable;
        mso-font-signature:-1593824529 1342185546 65536 0 66047 0;}
        @font-face
        {font-family:"Khmer OS Muol Light";
        panose-1:2 0 5 0 0 0 0 2 0 4;
        mso-font-charset:0;
        mso-generic-font-family:auto;
        mso-font-pitch:variable;
        mso-font-signature:-1610612497 1342185546 65536 0 273 0;}
        /* Style Definitions */
        p.MsoNormal, li.MsoNormal, div.MsoNormal
        {mso-style-unhide:no;
        mso-style-qformat:yes;
        mso-style-parent:"";
        margin:0in;
        margin-bottom:.0001pt;
        mso-pagination:widow-orphan;
        font-size:10.0pt;
        font-family:"Times New Roman",serif;
        mso-fareast-font-family:"Times New Roman";
        mso-fareast-language:EN-US;
        mso-bidi-language:AR-SA;}
        h1
        {mso-style-unhide:no;
        mso-style-qformat:yes;
        mso-style-link:"Heading 1 Char";
        mso-style-next:Normal;
        margin:0in;
        margin-bottom:.0001pt;
        text-align:right;
        text-indent:.5in;
        mso-pagination:widow-orphan;
        page-break-after:avoid;
        mso-outline-level:1;
        font-size:22.0pt;
        mso-bidi-font-size:10.0pt;
        font-family:"Limon S1";
        mso-font-kerning:0pt;
        mso-fareast-language:EN-US;
        mso-bidi-language:AR-SA;
        font-weight:normal;}
        h2
        {mso-style-unhide:no;
        mso-style-qformat:yes;
        mso-style-next:Normal;
        margin:0in;
        margin-bottom:.0001pt;
        mso-pagination:widow-orphan;
        page-break-after:avoid;
        mso-outline-level:2;
        font-size:13.0pt;
        mso-bidi-font-size:10.0pt;
        font-family:"Times New Roman",serif;
        mso-bidi-font-family:Arial;
        color:navy;
        mso-fareast-language:EN-US;
        mso-bidi-language:AR-SA;}
        h3
        {mso-style-unhide:no;
        mso-style-qformat:yes;
        mso-style-next:Normal;
        margin:0in;
        margin-bottom:.0001pt;
        mso-pagination:widow-orphan;
        page-break-after:avoid;
        mso-outline-level:3;
        font-size:10.0pt;
        font-family:"Times New Roman",serif;
        mso-bidi-font-family:Arial;
        color:#003300;
        mso-fareast-language:EN-US;
        mso-bidi-language:AR-SA;}
        h4
        {mso-style-unhide:no;
        mso-style-qformat:yes;
        mso-style-link:"Heading 4 Char";
        mso-style-next:Normal;
        margin:0in;
        margin-bottom:.0001pt;
        mso-pagination:widow-orphan;
        page-break-after:avoid;
        mso-outline-level:4;
        font-size:20.0pt;
        font-family:"Limon S6";
        mso-bidi-font-family:Arial;
        mso-fareast-language:EN-US;
        mso-bidi-language:AR-SA;
        font-weight:normal;}
        h5
        {mso-style-unhide:no;
        mso-style-qformat:yes;
        mso-style-link:"Heading 5 Char";
        mso-style-next:Normal;
        margin-top:12.0pt;
        margin-right:0in;
        margin-bottom:3.0pt;
        margin-left:0in;
        mso-pagination:widow-orphan;
        mso-outline-level:5;
        font-size:13.0pt;
        font-family:"Times New Roman",serif;
        mso-fareast-language:EN-US;
        mso-bidi-language:AR-SA;
        font-style:italic;}
        p.MsoHeader, li.MsoHeader, div.MsoHeader
        {mso-style-priority:99;
        mso-style-link:"Header Char";
        margin:0in;
        margin-bottom:.0001pt;
        mso-pagination:widow-orphan;
        tab-stops:center 3.25in right 6.5in;
        font-size:10.0pt;
        font-family:"Times New Roman",serif;
        mso-fareast-font-family:"Times New Roman";
        mso-fareast-language:EN-US;
        mso-bidi-language:AR-SA;}
        p.MsoFooter, li.MsoFooter, div.MsoFooter
        {mso-style-priority:99;
        mso-style-link:"Footer Char";
        margin:0in;
        margin-bottom:.0001pt;
        mso-pagination:widow-orphan;
        tab-stops:center 3.25in right 6.5in;
        font-size:10.0pt;
        font-family:"Times New Roman",serif;
        mso-fareast-font-family:"Times New Roman";
        mso-fareast-language:EN-US;
        mso-bidi-language:AR-SA;}
        a:link, span.MsoHyperlink
        {mso-style-priority:99;
        color:blue;
        mso-themecolor:hyperlink;
        text-decoration:underline;
        text-underline:single;}
        a:visited, span.MsoHyperlinkFollowed
        {mso-style-noshow:yes;
        mso-style-priority:99;
        color:purple;
        mso-themecolor:followedhyperlink;
        text-decoration:underline;
        text-underline:single;}
        p.MsoAcetate, li.MsoAcetate, div.MsoAcetate
        {mso-style-noshow:yes;
        mso-style-priority:99;
        mso-style-link:"Balloon Text Char";
        margin:0in;
        margin-bottom:.0001pt;
        mso-pagination:widow-orphan;
        font-size:8.0pt;
        font-family:"Tahoma",sans-serif;
        mso-fareast-font-family:"Times New Roman";
        mso-fareast-language:EN-US;
        mso-bidi-language:AR-SA;}
        p.MsoNoSpacing, li.MsoNoSpacing, div.MsoNoSpacing
        {mso-style-priority:1;
        mso-style-unhide:no;
        mso-style-qformat:yes;
        mso-style-parent:"";
        margin:0in;
        margin-bottom:.0001pt;
        mso-pagination:widow-orphan;
        font-size:10.0pt;
        font-family:"Times New Roman",serif;
        mso-fareast-font-family:"Times New Roman";
        mso-fareast-language:EN-US;
        mso-bidi-language:AR-SA;}
        p.MsoListParagraph, li.MsoListParagraph, div.MsoListParagraph
        {mso-style-priority:34;
        mso-style-unhide:no;
        mso-style-qformat:yes;
        margin-top:0in;
        margin-right:0in;
        margin-bottom:0in;
        margin-left:.5in;
        margin-bottom:.0001pt;
        mso-add-space:auto;
        mso-pagination:widow-orphan;
        font-size:10.0pt;
        font-family:"Times New Roman",serif;
        mso-fareast-font-family:"Times New Roman";
        mso-fareast-language:EN-US;
        mso-bidi-language:AR-SA;}
        p.MsoListParagraphCxSpFirst, li.MsoListParagraphCxSpFirst, div.MsoListParagraphCxSpFirst
        {mso-style-priority:34;
        mso-style-unhide:no;
        mso-style-qformat:yes;
        mso-style-type:export-only;
        margin-top:0in;
        margin-right:0in;
        margin-bottom:0in;
        margin-left:.5in;
        margin-bottom:.0001pt;
        mso-add-space:auto;
        mso-pagination:widow-orphan;
        font-size:10.0pt;
        font-family:"Times New Roman",serif;
        mso-fareast-font-family:"Times New Roman";
        mso-fareast-language:EN-US;
        mso-bidi-language:AR-SA;}
        p.MsoListParagraphCxSpMiddle, li.MsoListParagraphCxSpMiddle, div.MsoListParagraphCxSpMiddle
        {mso-style-priority:34;
        mso-style-unhide:no;
        mso-style-qformat:yes;
        mso-style-type:export-only;
        margin-top:0in;
        margin-right:0in;
        margin-bottom:0in;
        margin-left:.5in;
        margin-bottom:.0001pt;
        mso-add-space:auto;
        mso-pagination:widow-orphan;
        font-size:10.0pt;
        font-family:"Times New Roman",serif;
        mso-fareast-font-family:"Times New Roman";
        mso-fareast-language:EN-US;
        mso-bidi-language:AR-SA;}
        p.MsoListParagraphCxSpLast, li.MsoListParagraphCxSpLast, div.MsoListParagraphCxSpLast
        {mso-style-priority:34;
        mso-style-unhide:no;
        mso-style-qformat:yes;
        mso-style-type:export-only;
        margin-top:0in;
        margin-right:0in;
        margin-bottom:0in;
        margin-left:.5in;
        margin-bottom:.0001pt;
        mso-add-space:auto;
        mso-pagination:widow-orphan;
        font-size:10.0pt;
        font-family:"Times New Roman",serif;
        mso-fareast-font-family:"Times New Roman";
        mso-fareast-language:EN-US;
        mso-bidi-language:AR-SA;}
        span.Heading1Char
        {mso-style-name:"Heading 1 Char";
        mso-style-unhide:no;
        mso-style-locked:yes;
        mso-style-link:"Heading 1";
        mso-ansi-font-size:22.0pt;
        font-family:"Limon S1";
        mso-ascii-font-family:"Limon S1";
        mso-hansi-font-family:"Limon S1";
        mso-bidi-language:AR-SA;}
        span.Heading4Char
        {mso-style-name:"Heading 4 Char";
        mso-style-unhide:no;
        mso-style-locked:yes;
        mso-style-link:"Heading 4";
        mso-ansi-font-size:20.0pt;
        mso-bidi-font-size:20.0pt;
        font-family:"Limon S6";
        mso-ascii-font-family:"Limon S6";
        mso-hansi-font-family:"Limon S6";
        mso-bidi-font-family:Arial;
        mso-bidi-language:AR-SA;}
        span.Heading5Char
        {mso-style-name:"Heading 5 Char";
        mso-style-unhide:no;
        mso-style-locked:yes;
        mso-style-link:"Heading 5";
        mso-ansi-font-size:13.0pt;
        mso-bidi-font-size:13.0pt;
        mso-bidi-language:AR-SA;
        font-weight:bold;
        font-style:italic;}
        span.BalloonTextChar
        {mso-style-name:"Balloon Text Char";
        mso-style-noshow:yes;
        mso-style-priority:99;
        mso-style-unhide:no;
        mso-style-locked:yes;
        mso-style-link:"Balloon Text";
        mso-ansi-font-size:8.0pt;
        mso-bidi-font-size:8.0pt;
        font-family:"Tahoma",sans-serif;
        mso-ascii-font-family:Tahoma;
        mso-hansi-font-family:Tahoma;
        mso-bidi-font-family:Tahoma;
        mso-bidi-language:AR-SA;}
        span.HeaderChar
        {mso-style-name:"Header Char";
        mso-style-priority:99;
        mso-style-unhide:no;
        mso-style-locked:yes;
        mso-style-link:Header;
        mso-bidi-language:AR-SA;}
        span.FooterChar
        {mso-style-name:"Footer Char";
        mso-style-priority:99;
        mso-style-unhide:no;
        mso-style-locked:yes;
        mso-style-link:Footer;
        mso-bidi-language:AR-SA;}
        .MsoChpDefault
        {mso-style-type:export-only;
        mso-default-props:yes;
        font-size:10.0pt;
        mso-ansi-font-size:10.0pt;
        mso-bidi-font-size:10.0pt;
        mso-fareast-language:EN-US;
        mso-bidi-language:KHM;}
        /* Page Definitions */
        @page
        {mso-footnote-separator:url("F01-Credit%20sale%20contract%20PHN_files/header.html") fs;
        mso-footnote-continuation-separator:url("F01-Credit%20sale%20contract%20PHN_files/header.html") fcs;
        mso-endnote-separator:url("F01-Credit%20sale%20contract%20PHN_files/header.html") es;
        mso-endnote-continuation-separator:url("F01-Credit%20sale%20contract%20PHN_files/header.html") ecs;}
        @page WordSection1
        {size:595.35pt 842.0pt;
        margin:.5in .5in .5in .5in;
        mso-header-margin:.2in;
        mso-footer-margin:5.75pt;
        mso-header:url("F01-Credit%20sale%20contract%20PHN_files/header.html") h1;
        mso-footer:url("F01-Credit%20sale%20contract%20PHN_files/header.html") f1;
        mso-paper-source:0;}
        div.WordSection1
        {page:WordSection1;}
        @page WordSection2
        {size:595.35pt 842.0pt;
        margin:28.35pt 28.05pt 28.35pt 28.05pt;
        mso-header-margin:.5in;
        mso-footer-margin:.5in;
        mso-columns:2 even .2in;
        mso-header:url("F01-Credit%20sale%20contract%20PHN_files/header.html") h1;
        mso-footer:url("F01-Credit%20sale%20contract%20PHN_files/header.html") f1;
        mso-paper-source:0;}
        div.WordSection2
        {page:WordSection2;}
        @page WordSection3
        {size:595.35pt 842.0pt;
        margin:.35in 28.1pt 19.45pt 28.1pt;
        mso-header-margin:.5in;
        mso-footer-margin:.5in;
        mso-header:url("F01-Credit%20sale%20contract%20PHN_files/header.html") h1;
        mso-footer:url("F01-Credit%20sale%20contract%20PHN_files/header.html") f1;
        mso-paper-source:0;}
        div.WordSection3
        {page:WordSection3;}
        /* List Definitions */
        .button {
                background-color: #4CAF50; /* Green */
                border: none;
                color: white;
                padding: 10px 25px;
                text-align: center;
                text-decoration: none;
                display: inline-block;
                font-size: 16px;
                margin: 4px 2px;
                -webkit-transition-duration: 0.4s; /* Safari */
                transition-duration: 0.4s;
                cursor: pointer;
            }

            .button1 {
                background-color: white; 
                color: black; 
                border: 2px solid #4CAF50;
            }

            .button1:hover {
                background-color: #4CAF50;
                color: white;
            }

            .button2 {
                background-color: white; 
                color: black; 
                border: 2px solid #008CBA;
            }

            .button2:hover {
                background-color: #008CBA;
                color: white;
            }
        </style>
    </head>
    <body id=print_me lang=EN-US link=blue vlink=purple style='tab-interval:.5in; margin-left:50px;'>
        <button id="print_hide" class="button button1" onclick="with_print()">print</button>
        <button id="go_back" class="button button2" onclick="go_back()">Go Back</button>
        <script>
            document.getElementById('print_me').style.width = "210mm";    
            function with_print(){
                document.getElementById('print_hide').style.display = "none";
                document.getElementById('go_back').style.display = "none";
                document.getElementById('print_me').removeAttribute("style");
                window.print();
                document.getElementById('print_me').style.width = "210mm"; 
                document.getElementById('print_me').style.marginLeft = "50px";  
                document.getElementById('print_hide').removeAttribute("style");
                document.getElementById('go_back').removeAttribute("style");
            }
            function go_back(){
                window.location = "{{ url('accounts/aprove_credit_sales/'.$cl_data->client_id.'/show_approve/'.$data_id) }}";
            }
        </script>
        <div class=WordSection1>
            <p class=MsoNoSpacing align=center style='text-align:center'>
                <span
                style='font-size:12.0pt;font-family:"Khmer OS Muol";mso-bidi-language:KHM'>
                <o:p>&nbsp;</o:p>
                </span>
            </p>
            <p class=MsoNoSpacing align=center style='text-align:center'>
                <span lang=KHM
                style='font-size:14.0pt;font-family:"Khmer OS Muol";mso-bidi-language:KHM'>ព្រះរាជាណាចក្រកម្ពុជា</span>
                <span
                style='font-size:14.0pt;font-family:"Khmer OS Muol";mso-bidi-language:KHM'>
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing align=center style='text-align:center'>
                <span lang=KHM
                style='font-size:14.0pt;font-family:"Khmer OS Muol";mso-bidi-language:KHM'>ជាតិ
                សាសនា ព្រះមហាក្សត្រ</span>
                <span style='font-size:14.0pt;font-family:"Khmer OS Muol";
                mso-bidi-language:KHM'>
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing align=center style='text-align:center'>
                <b><span
                style='font-size:12.0pt;font-family:Wingdings;mso-ascii-font-family:"Khmer OS Battambang";
                mso-hansi-font-family:"Khmer OS Battambang";mso-bidi-font-family:"Khmer OS Battambang";
                mso-bidi-language:KHM;mso-char-type:symbol;mso-symbol-font-family:Wingdings'><span
                style='mso-char-type:symbol;mso-symbol-font-family:Wingdings'><span
                style='mso-char-type:symbol;mso-symbol-font-family:Wingdings'><span
                style='mso-char-type:symbol;mso-symbol-font-family:Wingdings'></span></span></span></span></b><b><span
                style='font-size:14.0pt;font-family:Wingdings;mso-ascii-font-family:"Khmer OS Battambang";
                mso-hansi-font-family:"Khmer OS Battambang";mso-bidi-font-family:"Khmer OS Battambang";
                mso-bidi-language:KHM;mso-char-type:symbol;mso-symbol-font-family:Wingdings'><span
                style='mso-char-type:symbol;mso-symbol-font-family:Wingdings'>®</span></span></b><b><span
                style='font-size:12.0pt;font-family:Wingdings;mso-ascii-font-family:"Khmer OS Battambang";
                mso-hansi-font-family:"Khmer OS Battambang";mso-bidi-font-family:"Khmer OS Battambang";
                mso-bidi-language:KHM;mso-char-type:symbol;mso-symbol-font-family:Wingdings'><span
                style='mso-char-type:symbol;mso-symbol-font-family:Wingdings'><span
                style='mso-char-type:symbol;mso-symbol-font-family:Wingdings'><span
                style='mso-char-type:symbol;mso-symbol-font-family:Wingdings'></span></span></span></span></b>
                <b>
                <span
                    style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                    KHM'>
                    <o:p></o:p>
                </span>
                </b>
            </p>
            <p class=MsoNoSpacing style='text-align:justify'>
                <b>
                <span style='font-size:
                    12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:KHM'>
                    <o:p>&nbsp;</o:p>
                </span>
                </b>
            </p>
            <p class=MsoNoSpacing style='text-align:justify'>
                <span lang=KHM
                style='font-size:12.0pt;font-family:"Khmer OS Muol";mso-bidi-language:KHM'>ខេ</span>
                <span
                style='font-size:12.0pt;font-family:"Khmer OS Muol";mso-bidi-language:KHM'>
                .<span
                    lang=KHM>អេ</span>.<span lang=KHM>អេស</span>.<span lang=KHM>ភី</span>.<span
                    lang=KHM> កសិករ ពាណិជ្ជ ឯ.ក</span>
                <o:p></o:p>
                </span>
            </p>
            <?php
                $count = 10;
                function str_pad_left($num,$count){
                    if($count > 0){
                        $num_c = (string) $num;
                        $str = "0";
                        $str_num = strlen($num_c);
                        $count1 = $count - $str_num;
                        $str1 = str_repeat($str,$count1);
                        return $str1."".$num_c;
                    }else{
                        return "error right count please it number and bigger then  0";
                    }
                }
                if(!empty($cl_data->cs_sales)){
                    $sale_id = str_pad_left($cl_data->cs_sales->id,$count);
                }else{
                    $sale_id = "..............";
                }
                
            ?>
            <p class=MsoNoSpacing style='text-align:justify'>
                <span lang=KHM
                style='font-family:"Khmer OS Muol Light";mso-bidi-language:KHM'>លេខកិច្ចសន្យាៈ </span><span
                style='font-size:9.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                KHM' class="contract_id">  {{$sale_id}}<span lang=KHM> </span></span>
                <span
                style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                KHM'>
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing style='text-align:justify'>
                <span lang=KHM
                style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                KHM'><span style='mso-spacerun:yes'>            </span></span>
                <span
                style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                KHM'>
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing align=center style='text-align:center'>
                <span lang=KHM
                style='font-size:12.0pt;font-family:"Khmer OS Muol";mso-bidi-language:KHM'>កិច្ចសន្យាទិញបង់រំលស់</span>
                <span
                style='font-size:12.0pt;font-family:"Khmer OS Muol";mso-bidi-language:KHM'>
                <o:p></o:p>
                </span>
            </p>
            <?php
                if(!empty($cl_data->cs_sales->branch)){
                    $brand = $cl_data->cs_sales->branch->brand_name;
                }else{
                    $brand = "";
                }
                if(!empty($cl_data->date_approval)){
                    $date_pay_last = $cl_data->date_approval;
                    $date = new DateTime($date_pay_last);
                    $yea = $date->format('Y');
                    $mon= $date->format('m');
                    $da= $date->format('d');
                }else{
                    $da = "..............";
                    $mon = "..............";
                    $yea = "..............";
                }
                
                
            ?>
            <p class=MsoNoSpacing align=center style='text-align:center'>
                <span lang=KHM
                style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                KHM'>ធ្វើនៅ</span><span style='font-family:"Khmer OS Battambang";mso-bidi-language:
                KHM' class="where_create_contract">..{{$brand}}..</span><span lang=KHM style='font-size:12.0pt;
                font-family:"Khmer OS Battambang";mso-bidi-language:KHM'>ថ្ងៃទី</span><span
                style='font-family:"Khmer OS Battambang";mso-bidi-language:KHM' class="date_create_contract"> {{$da}} </span><span
                lang=KHM style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                KHM'>ខែ</span><span style='font-family:"Khmer OS Battambang";mso-bidi-language:
                KHM' class="month_create_contract"> {{$mon}} </span><span lang=KHM style='font-size:12.0pt;font-family:
                "Khmer OS Battambang";mso-bidi-language:KHM'>ឆ្នាំ</span><span
                style='font-family:"Khmer OS Battambang";mso-bidi-language:KHM' class="year_create_contract"> {{$yea}} </span>
                <span
                style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                KHM'>
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing align=center style='text-align:center'>
                <b><span lang=KHM
                style='font-size:12.0pt;font-family:"Khmer OS Muol";mso-bidi-language:KHM'>រវាង</span></b>
                <b>
                <span
                    style='font-size:12.0pt;font-family:"Khmer OS Muol";mso-bidi-language:KHM'>
                    <o:p></o:p>
                </span>
                </b>
            </p>
            <p class=MsoNoSpacing style='text-align:justify'>
                <span lang=KHM
                style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                KHM'>ក្រុមហ៊ុន </span><span lang=KHM style='font-size:12.0pt;font-family:"Khmer OS Muol";
                mso-bidi-language:KHM'>ខេ.អេ.អេស.ភី. កសិករ ពាណិជ្ជ ឯ.ក</span><span lang=KHM
                style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                KHM'> ដែលមានលោក </span><span lang=KHM style='font-size:12.0pt;font-family:"Khmer OS Muol Light";
                mso-bidi-language:KHM'>ផុន ភារម្យ</span><span lang=KHM style='font-size:12.0pt;
                font-family:"Khmer OS Battambang";mso-bidi-language:KHM'> តួនាទីជា </span><span
                lang=KHM style='font-size:12.0pt;font-family:"Khmer OS Muol Light";mso-bidi-language:
                KHM'>ប្រធានសាខា</span><span lang=KHM style='font-size:12.0pt;font-family:"Khmer OS Battambang";
                mso-bidi-language:KHM'> ដែលមានអាសយដ្ឋាន ផ្ទះលេខ៦២ ផ្លូវលេខ៦២​</span>
                <span
                style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                KHM'>
                R<span lang=KHM> ភូមិទទួលគោក សង្កាត់ទួលសង្កែ ខណ្ឌឬស្សីកែវ រាជធានីភ្នំពេញ
                ជាតំណាងរបស់ក្រុមហ៊ុន តទៅនេះហៅកាត់ថាភាគី </span>(<span lang=KHM>ក</span>)
                <span
                    lang=KHM>
                    ។
                    <o:p></o:p>
                </span>
                </span>
            </p>
            <p class=MsoNoSpacing align=center style='text-align:center'>
                <b><span lang=KHM
                style='font-size:12.0pt;font-family:"Khmer OS Muol";mso-bidi-language:KHM'>និង</span></b>
                <b>
                <span
                    style='font-size:12.0pt;font-family:"Khmer OS Muol";mso-bidi-language:KHM'>
                    <o:p></o:p>
                </span>
                </b>
            </p>
           
            <?php
                // Client
                if(!empty($cl_data->cs_client)){
                    $na = $cl_data->cs_client->kh_username;
                    $ge = $cl_data->cs_client->gender;
                    $dob = $cl_data->cs_client->dob;
                    $id_n = $cl_data->cs_client->identify_num;
                    if($cl_data->cs_client->gender == 'F'){
                        $ge = "ស្រី";
                    }else{
                        $ge = "ប្រុស";
                    }
    
                    if($cl_data->cs_client->nationality == 1){
                        $nationality = "ខ្មែរ";
                    }else{
                        $nationality = $cl_data->cs_client->nationality;
                    }
    
                    if($cl_data->cs_client->identify_type == 1){
                        $id_ty = "អត្តសញ្ញាណប័ណ្ណ";
                    }elseif($cl_data->cs_client->identify_type == 2){
                        $id_ty = "លិខិនឆ្លង់ដែន";
                    }elseif($cl_data->cs_client->identify_type == 3){
                        $id_ty = "សំបុត្របញ្ជាក់កំណើត";
                    }elseif($cl_data->cs_client->identify_type == 4){
                        $id_ty = "សៀវភៅសា្នក់នៅ";
                    }elseif($cl_data->cs_client->identify_type == 5){
                        $id_ty = "សៀវភៅគ្រួសារ";
                    }else{
                        $id_ty = $cl_data->cs_client->identify_type;
                    }
    
                    if($cl_data->cs_client->identify_by == 1){
                        $id_by = "ក្រសួងមហាផ្ទៃ";
                    }else{
                        $id_by = $cl_data->cs_client->identify_by;
                    }
                    $home_num = $cl_data->cs_client->home_num;
                    $group_num = $cl_data->cs_client->group_num;
                    $street_num = $cl_data->cs_client->street_num;
                    $vilige = $cl_data->cs_client->vilige;
                    $commune = $cl_data->cs_client->commune;
                    $district = $cl_data->cs_client->district;
                    $province = $cl_data->cs_client->province;

                    // Sub Client
                    $cl = $cl_data->cs_client;
                    foreach($cl->sub_clients as $key=>$sc){
                        if($key == 0){
                            if($sc->client_name_kh){
                                $cl_name = $sc->client_name_kh;
                            }else{
                                $cl_name = "....................";
                            }
    
                            if($sc->client_dob){
                                $dob = $sc->client_dob;
                            }
    
                            if($sc->client_idcard_no){
                                $cl_id = $sc->client_idcard_no;
                            }
    
                            if($sc->client_gender == 'F'){
                                $c_g = "ស្រី";
                            }else{
                                $c_g = "ប្រុស";
                            }
    
                            if($sc->client_nationality == 1){
                                $c_n = "ខ្មែរ";
                            }else{
                                $c_n = $sc->client_nationality;
                            }
    
                            if($sc->client_type_idcard == 1){
                                $c_id_ty = "អត្តសញ្ញាណប័ណ្ណ";
                            }elseif($sc->identify_type == 2){
                                $c_id_ty = "លិខិនឆ្លង់ដែន";
                            }elseif($sc->identify_type == 3){
                                $c_id_ty = "សំបុត្របញ្ជាក់កំណើត";
                            }elseif($sc->identify_type == 4){
                                $c_id_ty = "សៀវភៅសា្នក់នៅ";
                            }elseif($sc->identify_type == 5){
                                $c_id_ty = "សៀវភៅគ្រួសារ";
                            }else{
                                $c_id_ty = $sc->client_type_idcard;
                            }
    
                            if($sc->client_aprovel_idcard_by == 1){
                                $c_id_by = "ក្រសួងមហាផ្ទៃ";
                            }else{
                                $c_id_by = $sc->client_aprovel_idcard_by;
                            }
    
                            if($sc->client_house_num){
                                $h_num = $sc->client_house_num;
                            }
    
                            if($sc->client_group_num){
                                $gr_num = $sc->client_group_num;
                            }
    
                            if($sc->client_st_num){
                                $st_num = $sc->client_st_num;
                            }
    
                            if($sc->client_village){
                                $vi = $sc->client_village;
                            }
    
                            if($sc->client_commune){
                                $com = $sc->client_commune;
                            }
    
                            if($sc->client_district){
                                $des = $sc->client_district;
                            }
    
                            if($sc->client_province){
                                $prov = $sc->client_province;
                            }
                        }
                        if($key == 1){
                            if($sc->client_name_kh){
                                $k_clname = $sc->client_name_kh;
                            }
    
                            if($sc->client_gender == 'F'){
                                $k_ge = "ស្រី";
                            }else{
                                $k_ge = "ប្រុស";
                            }
    
                            if($sc->client_dob){
                                $k_dob = $sc->client_dob;
                            }
    
                            if($sc->client_nationality == 1){
                                $k_cn = "ខ្មែរ";
                            }else{
                                $k_cn = $sc->client_nationality;
                            }
    
                            if($sc->client_idcard_no){
                                $k_id = $sc->client_idcard_no;
                            }
    
                            if($sc->client_type_idcard == 1){
                                $k_id_ty = "អត្តសញ្ញាណប័ណ្ណ";
                            }elseif($sc->client_type_idcard == 2){
                                $k_id_ty = "លិខិនឆ្លង់ដែន";
                            }elseif($sc->client_type_idcard == 3){
                                $k_id_ty = "សំបុត្របញ្ជាក់កំណើត";
                            }elseif($sc->client_type_idcard == 4){
                                $k_id_ty = "សៀវភៅសា្នក់នៅ";
                            }elseif($sc->client_type_idcard == 5){
                                $k_id_ty = "សៀវភៅគ្រួសារ";
                            }else{
                                $k_id_ty = $sc->client_type_idcard;
                            }
    
                            if($sc->client_aprovel_idcard_by == 1){
                                $k_id_by = "ក្រសួងមហាផ្ទៃ";
                            }else{
                                $k_id_by = $sc->client_aprovel_idcard_by;
                            }
                        }else{
                            $k_clname = "";
                            $k_ge = "";
                            $k_dob = "";
                            $k_cn = "";
                            $k_id = "....................";
                            $k_id_ty = "..............................";
                            $k_id_by = "..........";
                        }
                    }
                    // Item
                    $cs_sa = $cl_data->cs_sales;
                    $name = "";
                    $qty = 0;
                    $brand = "";
                    $item_code = "";
                    foreach($cs_sa->sale_item as $key=>$si){
                        if($key != 0){
                            $name .= ", ".$si->item->name;
                            $brand .= ", ".$si->item->cs_brand->name;
                            $item_code .= ", ".$si->item->item_bacode;
                        }else{
                            $name .= $si->item->name;
                            $brand .= $si->item->cs_brand->name;
                            $item_code .= $si->item->item_bacode;
                        }
                        $qty += $si->qty;
                        
                    }
                }else{
                    $na = "..............";
                    $ge = "..............";
                    $dob = "..............";
                    $nationality = "..............";
                    $id_n = "..............";
                    $id_ty = "..............";
                    $id_by = "..............";
                    $home_num = "..............";
                    $group_num = "..............";
                    $street_num = "..............";
                    $vilige = "..............";
                    $commune = "..............";
                    $district = "..............";
                    $province = "..............";
                    $name = "..............";
                    $qty = "";
                    $item_code = "";
                    $durat_type_week = "";
                    $durat_type_2week = "";
                    $durat_type_month = "";
                    $cl_name = "..............";
                    $c_g = "..............";
                    $c_n = "..............";
                    $cl_id = "..............";
                    $c_id_ty = "..............";
                    $c_id_by = "..............";
                    $k_clname  = "..............";
                    $k_ge = "..............";
                    $k_dob = "..............";
                    $k_cn = "..............";
                    $k_id = "..............";
                    $k_id_ty = "..............";
                    $k_id_by = "..............";
                    $h_num = "..............";
                    $gr_num = "..............";
                    $st_num = "..............";
                    $vi = "..............";
                    $com = "..............";
                    $des = "..............";
                    $prov = "..............";
                }
                if(!empty($cl_data->prices_total_num)){
                    $price = $cl_data->prices_total_num;
                }else{
                    $price = "..............";
                }
                
                if(!empty($cl_data->prices_totalword)){
                    $price_word = $cl_data->prices_totalword;
                }else{
                    $price_word = "..............";
                }
                
                if(!empty($cl_data->deposit_precent)){
                    $price_per = $cl_data->deposit_precent;
                }else{
                    $price_per = "..............";
                }
                
                if(!empty($cl_data->deposit_fixed)){
                    $price_owe = $cl_data->deposit_fixed;
                }else{
                    $price_owe = "..............";
                }
                
                if(!empty($cl_data->deposit_fixed_word)){
                    $price_owe_word = $cl_data->deposit_fixed_word;
                }else{
                    $price_owe_word = "..............";
                }

                if(!empty($cl_data->money_owne_word)){
                    $money_owe_word = $cl_data->money_owne_word;
                }else{
                    $money_owe_word = "..............";
                }
                
                if(!empty($cl_data->duration_pay_money)){
                    $money_owe_duration = $cl_data->duration_pay_money;
                }else{
                    $money_owe_duration = "..............";
                }
                
                if(!empty($cl_data->duration_pay_money_type)){
                    if($cl_data->duration_pay_money_type == 'week'){
                    $durat_type_week = "þ";
                    }else{
                        $durat_type_week = "¨";
                    }
                    if($cl_data->duration_pay_money_type == '2week'){
                        $durat_type_2week = "þ";
                    }else{
                        $durat_type_2week = "¨";
                    }
                    if($cl_data->duration_pay_money_type == 'month'){
                        $durat_type_month = "þ";
                    }else{
                        $durat_type_month = "¨";
                    }
                }
                
                if(!empty($cl_data->schedule)){
                    $cl_sched = $cl_data->schedule;
                    $money_owe = $cl_data->schedule->money_owne_total_pay;
                    $total_pay = $cl_sched->cs_schedule_timesheet->first()->total_payment;
                    $date_pay_first = $cl_sched->cs_schedule_timesheet->first()->date_payment;
                    $d_t = new DateTime($date_pay_first);
                    $years = $d_t->format('Y');
                    $months = $d_t->format('m');
                    $days = $d_t->format('d');
                    $date_pay_last = $cl_sched->cs_schedule_timesheet->last()->date_payment;
                    $d_l = new DateTime($date_pay_last);
                    $years_l = $d_l->format('Y');
                    $months_l = $d_l->format('m');
                    $days_l = $d_l->format('d');
                }else{
                    $money_owe = "...";
                    $total_pay = "...";
                    $days = "...";
                    $months = "...";
                    $years = "...";
                    $d_l = "...";
                    $years_l = "...";
                    $months_l = "...";
                    $days_l = "...";
                }
                
            ?>


            <p class=MsoNoSpacing style='text-align:left'>
                <span lang=KHM
                style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                KHM'>ឈ្មោះ</span>
                <span style='font-size:12.0pt;font-family:"Khmer OS Battambang";
                mso-bidi-language:KHM'><b class="kh_username" lang=KHM> {{$na}} </b><span lang=KHM>ភេទ</span >​<span
                    lang=KHM class="client_gender"> {{$ge}} </span>​<span lang=KHM>ថ្ងៃខែឆ្នាំកំណើត</span>​<span class="client_dob">{{$dob}} ​</span><span
                    lang=KHM>សញ្ជាតិ</span><span class="nationality">.. {{$nationality}}</span>..<span lang=KHM>កាន់ប័ណ្ណ​សម្គាល់អត្តសញ្ញាណលេខ</span><span class="identify_num"> {{$id_n}} </span><span lang=KHM>ប្រភេទប័ណ្ណ</span><span class="identify_type"> {{$id_ty}} </span>
                    <span lang=KHM>ចុះថ្ងៃទី</span><span class="date_create_identify_id">​</span><span lang=KHM>..</span>​    <span
                    lang=KHM>ចេញដោយ</span>​<span lang=KHM class="identify_by"> {{ $id_by }}</span>​<span
                    lang=KHM>និងឈ្មោះ</span><span class="name_with_relate_client">​</span><span lang=KHM >ភេទ</span><span class="name_with_relate_client"> </span>
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing style='text-align:justify'>
                <span lang=KHM
                style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                KHM'>ថ្ងៃខែឆ្នាំកំណើត</span>
                <span style='font-size:12.0pt;font-family:"Khmer OS Battambang";
                mso-bidi-language:KHM'>
                <span lang=KHM> </span> <span
                    lang=KHM>សញ្ជាតិ</span>.<span lang=KHM> </span> <span lang=KHM> កាន់ប័ណ្ណ​សម្គាល់អត្តសញ្ញាណលេខ</span> <span
                    lang=KHM> </span> 
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing style='text-align:justify'>
                <span lang=KHM
                style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                KHM'>ប្រភេទប័ណ្ណ</span>
                <span style='font-size:12.0pt;font-family:"Khmer OS Battambang";
                mso-bidi-language:KHM'>
                 <span lang=KHM>ចុះថ្ងៃទី</span> <span
                    lang=KHM>.</span> <span lang=KHM>ចេញដោយ</span> <span
                    lang=KHM>។</span>
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing style='text-align:justify'>
                <span style='font-size:12.0pt;
                font-family:"Khmer OS Battambang";mso-bidi-language:KHM'>
                <span
                    style='mso-spacerun:yes'> </span><span lang=KHM>អាសយដ្ឋានផ្ទះលេខ</span> {{$home_num}} <span
                    lang=KHM>ក្រុមទី</span> {{$group_num}} <span lang=KHM>ផ្លូវលេខ</span> {{$street_num}} <span
                    lang=KHM> </span> <span lang=KHM>ភូមិ</span> {{$vilige}} <span
                    lang=KHM>ឃុំ</span>/<span lang=KHM>សង្កាត់</span> {{$commune}} 
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing style='text-align:justify'>
                <span lang=KHM
                style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                KHM'>ស្រុក</span>
                <span style='font-size:12.0pt;font-family:"Khmer OS Battambang";
                mso-bidi-language:KHM'>
                /<span lang=KHM>ខណ្ឌ</span> {{$district}} <span
                    lang=KHM>ខេត្ត</span>/<span lang=KHM>ក្រុង</span> <span lang=KHM> </span> <span
                    lang=KHM> </span>{{$province}}<span lang=KHM> </span> <span lang=KHM><span
                    style='mso-spacerun:yes'>  </span>តទៅហៅកាត់ថាភាគី </span>(<span lang=KHM>ខ</span>)<span
                    lang=KHM>។</span>
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing style='text-align:justify'>
                <span style='font-size:12.0pt;
                font-family:"Khmer OS Battambang";mso-bidi-language:KHM'>
                <o:p>&nbsp;</o:p>
                </span>
            </p>
            <p class=MsoNoSpacing align=center style='text-align:center'>
                <span lang=KHM
                style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                KHM'>ភាគីទាំងពីរបានព្រមព្រៀងចុះកិច្ចសន្យា ដោយអនុវត្តតាមប្រការដូចខាងក្រោម​៖</span>
                <span
                style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                KHM'>
                <o:p></o:p>
                </span>
            </p>


            <p class=MsoNoSpacing style='text-align:justify'>
                <b><span lang=KHM
                style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                KHM'>ប្រការ១៖ អំពីលក្ខខណ្ឌរួម</span></b>
                <b>
                <span style='font-size:12.0pt;
                    font-family:"Khmer OS Battambang";mso-bidi-language:KHM'>
                    <o:p></o:p>
                </span>
                </b>
            </p>
            <p class=MsoNoSpacing style='text-align:justify'>
                <span lang=KHM
                style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                KHM'>១.១<span style='mso-tab-count:1'>    </span>ភាគី (ខ) យល់ព្រមទិញ  {{$name}}  
                ចំនួន {{$qty}} គ្រឿង </span>
                <span style='font-size:12.0pt;
                font-family:"Khmer OS Battambang";mso-bidi-language:KHM'>
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing style='margin-left:.75in;text-align:justify;text-indent:
                -.25in'>
                <span lang=KHM style='font-size:12.0pt;font-family:"Khmer OS Battambang";
                mso-bidi-language:KHM'>ម៉ាក  {{$brand}}  
                លេខកូដ  {{$item_code}}  </span>
                <span
                style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                KHM'>
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing style='margin-left:.75in;text-align:justify;text-indent:
                -.25in'>
                <span lang=KHM style='font-size:12.0pt;font-family:"Khmer OS Battambang";
                mso-bidi-language:KHM'>ក្នុងតម្លៃសរុបជាលេខ <span
                style='mso-spacerun:yes'> </span> {{$price}} </span>
                <span
                style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                KHM'>
                (US$)<span lang=KHM> ជាអក្សរ </span>{{$price_word}}<span lang=KHM></span>
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing style='margin-left:.75in;text-align:justify;text-indent:
                -.25in'>
                <span lang=KHM style='font-size:12.0pt;font-family:"Khmer OS Battambang";
                mso-bidi-language:KHM'>ដោយបង់ប្រាក់កក់មុនចំនួន. {{$price_per}} .ភាគរយ ស្មើនឹងចំនួនសរុបជាលេខ
                {{$price_owe}} </span>
                <span style='font-size:12.0pt;
                font-family:"Khmer OS Battambang";mso-bidi-language:KHM'>
                (US$)
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing style='text-align:justify;text-indent:.5in'>
                <span
                lang=KHM style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                KHM'>ជាអក្សរ {{$price_owe_word}} ។</span>
                <span
                style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                KHM'>
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing style='text-align:justify;text-indent:.5in'>
                <span
                lang=KHM style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                KHM'>១.២<span style='mso-tab-count:1'>    </span>ភាគី (ខ) យល់ព្រមបង់រំលស់ប្រាក់ដែលនៅសល់រួមទាំងការប្រាក់ចំនួនជាលេខ </span>
                <span
                style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                KHM'>
                {{$money_owe}}<span lang=KHM> </span>US$
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing style='margin-left:.5in;text-align:justify'>
                <span
                lang=KHM style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                KHM'>ជាអក្សរ  {{$money_owe_word}} </span>
                <span
                style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                KHM'>
                <span style='mso-spacerun:yes'>  </span><span lang=KHM>ក្នុងរយៈពេល </span>{{$money_owe_duration}}<span
                    lang=KHM> ខែ ទៅតាមដំណាក់កាលដូចខាងក្រោមៈ </span>
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing style='text-align:justify;text-indent:.5in'>
                <span
                style='font-size:12.0pt;font-family:Wingdings;mso-ascii-font-family:"Khmer OS Battambang";
                mso-hansi-font-family:"Khmer OS Battambang";mso-bidi-font-family:"Khmer OS Battambang";
                mso-bidi-language:KHM;mso-char-type:symbol;mso-symbol-font-family:Wingdings'><span
                style='mso-char-type:symbol;mso-symbol-font-family:Wingdings'>{{$durat_type_week}}</span></span><span
                lang=KHM style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                KHM'> សបា្តហ៍ <span style='mso-tab-count:1'>     </span></span><span
                style='font-size:12.0pt;font-family:Wingdings;mso-ascii-font-family:"Khmer OS Battambang";
                mso-hansi-font-family:"Khmer OS Battambang";mso-bidi-font-family:"Khmer OS Battambang";
                mso-bidi-language:KHM;mso-char-type:symbol;mso-symbol-font-family:Wingdings'>{{$durat_type_2week}}<span
                style='mso-char-type:symbol;mso-symbol-font-family:Wingdings'></span></span><span
                lang=KHM style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                KHM'> ពីរសបា្តហ៍ <span style='mso-spacerun:yes'>  </span><span
                style='mso-tab-count:1'>         </span></span>
                
                <span style='font-size:12.0pt;
                font-family:Wingdings;mso-ascii-font-family:"Khmer OS Battambang";mso-hansi-font-family:
                "Khmer OS Battambang";mso-bidi-font-family:"Khmer OS Battambang";mso-bidi-language:
                KHM;mso-char-type:symbol;mso-symbol-font-family:Wingdings'><span
                style='mso-char-type:symbol;mso-symbol-font-family:Wingdings'>{{$durat_type_month}}</span></span><span
                lang=KHM style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                KHM'> ខែ</span>
                <span style='font-size:12.0pt;font-family:"Khmer OS Battambang";
                mso-bidi-language:KHM'>
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing style='margin-left:70.9pt;text-align:justify;text-indent:
                -21.25pt;mso-list:l7 level1 lfo30'>
                <![if !supportLists]><span style='font-size:
                12.0pt;font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:
                Symbol;mso-bidi-language:KHM'><span style='mso-list:Ignore'>·<span
                style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                </span></span></span><![endif]><span lang=KHM style='font-size:12.0pt;
                font-family:"Khmer OS Battambang";mso-bidi-language:KHM'>ចំនួនជាលេខ </span>
                <span
                style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                KHM'>
                US$<span lang=KHM>.. {{$total_pay}} ..</span> x .{{$money_owe_duration}}.<span lang=KHM>ដង
                </span>
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing style='margin-left:70.9pt;text-align:justify;text-indent:
                -21.25pt;mso-list:l7 level1 lfo30'>
                <![if !supportLists]><span style='font-size:
                12.0pt;font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:
                Symbol;mso-bidi-language:KHM'><span style='mso-list:Ignore'>·<span
                style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                </span></span></span><![endif]><span lang=KHM style='font-size:12.0pt;
                font-family:"Khmer OS Battambang";mso-bidi-language:KHM'>ទូរទាត់ចាប់ពីថ្ងៃទី​.{{$days}}.ខែ.{{$months}}.ឆ្នាំ.{{$years}}.</span>
                <span
                style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                KHM'>
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing style='margin-left:1.0in;text-align:justify'>
                <span
                lang=KHM style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                KHM'>នឹងបញ្ចប់ថ្ងៃទី​.{{$days_l}}.ខែ.{{$months_l}}.ឆ្នាំ.{{$years_l}}. (តារាងកាលវិភាគសងប្រាក់)។</span>
                <span
                style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                KHM'>
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing style='text-align:justify'>
                <span lang=KHM
                style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                KHM'>១.៣<span style='mso-tab-count:1'>    </span>ភាគី (ខ) យល់ព្រមទូទាត់សំណងថ្លៃដឹកជញ្ជូន
                និងសេវារដ្ឋបាលផ្សេងៗ ដែលមានតម្លៃស្មើ១០% នៃតម្លៃទំ<span style='mso-tab-count:
                1'>          </span>និញ ក្នុងករណីភាគី(ខ) មិនទទួលយកទំនិញដែលបានដឹកជញ្ជូនទីកន្លែង។</span>
                <span
                style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                KHM'>
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing style='text-align:justify'>
                <span lang=KHM
                style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                KHM'>១.៤<span style='mso-tab-count:1'>    </span>ភាគី(ក) ធានាលើលក្ខនៃទំនិញ គុណភាពផលិតផលល្អ១០០%<span
                style='mso-spacerun:yes'>  </span>និងមានប័ណ្ណធានារយៈពេលមួយឆ្នាំ។</span>
                <span
                style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                KHM'>
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing style='text-align:justify'>
                <b><span lang=KHM
                style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                KHM'>ប្រការ២៖ អំពីលក្ខខណ្ឌពិសេស</span></b>
                <span style='font-size:12.0pt;
                font-family:"Khmer OS Battambang";mso-bidi-language:KHM'>
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing style='margin-left:.5in;text-align:justify;text-indent:
                -.5in'>
                <span lang=KHM style='font-size:12.0pt;font-family:"Khmer OS Battambang";
                mso-bidi-language:KHM'>២.១<span style='mso-tab-count:1'>    </span>ភាគី (ខ) មានកាតព្វកិច្ចបង់ប្រាក់ឲ្យបានទៀងទាត់ទៅតារាងកាលវិភាគសងប្រាក់ដែលបានកំណត់។
                ក្នុងករណីភាគី </span>
                <span style='font-size:12.0pt;font-family:"Khmer OS Battambang";
                mso-bidi-language:KHM'>
                (<span lang=KHM>ខ</span>)<span lang=KHM> មិនបានគោរពលក្ខខណ្ឌទាំងឡាយនៃកិច្ចសន្យានេះ
                ភាគី </span>(<span lang=KHM>ខ</span>)
                <span lang=KHM>
                    យល់ព្រមឲ្យភាគី (ក) <span
                        style='mso-spacerun:yes'> </span>រឹបអូសសម្ភារៈដែលបានទិញពីក្រុមហ៊ុន
                    ឬសម្ភារៈផ្សេងៗដែលមានតម្លៃប្រហាក់ប្រហែលគ្នា ដើម្បីលក់ឡាយឡុងសម្រាប់ទូទាត់ប្រាក់បំណុលដែលនៅខ្វះ។
                    <o:p></o:p>
                </span>
                </span>
            </p>
            <p class=MsoNoSpacing style='margin-left:.5in;text-align:justify;text-indent:
                -.5in'>
                <span lang=KHM style='font-size:12.0pt;font-family:"Khmer OS Battambang";
                mso-bidi-language:KHM'>២.២<span style='mso-tab-count:1'>    </span>ក្នុងករណី
                ភាគី </span>
                <span style='font-size:12.0pt;font-family:"Khmer OS Battambang";
                mso-bidi-language:KHM'>
                (<span lang=KHM>ខ</span>)<span lang=KHM> ខកខានមិនបានសងប្រាក់គ្រប់ចំនួនតាមថ្ងៃកំណត់ដូចមានក្នុងតារាងកាលវិភាគសងប្រាក់នោះ
                ភាគី​ </span>(<span lang=KHM>ខ</span>)<span lang=KHM> យល់ព្រមបង់ប្រាក់ពិន័យឲ្យ
                ភាគី (ក)។ ប្រាក់ពិន័យនេះត្រូវគណនាដោយយកចំនួនទឹកប្រាក់ដែលខកខានមិនបានសង
                គុណនឹងអត្រា ១០% ទៅតាមចំនួនថ្ងៃដែលបានយឺត។</span>
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing style='margin-left:.5in;text-align:justify;text-indent:
                -.5in'>
                <span lang=KHM style='font-size:12.0pt;font-family:"Khmer OS Battambang";
                mso-bidi-language:KHM'>២.៣<span style='mso-tab-count:1'>   </span>ក្នុងករណីមិនអាចអនុវត្តកិច្ចសន្យាបាន
                មុនចប់អាណត្តិដូចជា បាត់បង់លទ្ធភាពការងារ ឬមរណៈភាព អ្នកធានារបស់ភា​គី​ </span>
                <span
                style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                KHM'>
                (<span lang=KHM>ខ</span>) <span lang=KHM>ជាអ្នកទទួលខុសត្រូវទាំងស្រុងក្នុងការអនុវត្តកិច្ចសន្យាបន្ត។</span>
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing style='text-align:justify'>
                <span style='font-size:12.0pt;
                font-family:"Khmer OS Battambang";mso-bidi-language:KHM'>
                <o:p>&nbsp;</o:p>
                </span>
            </p>
            <p class=MsoNoSpacing style='text-align:justify'>
                <b><span lang=KHM
                style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                KHM'>ប្រការ៣៖ កាតព្វកិច្ចអ្នកធានា</span></b>
                <b>
                <span style='font-size:12.0pt;
                    font-family:"Khmer OS Battambang";mso-bidi-language:KHM'>
                    <o:p></o:p>
                </span>
                </b>
            </p>
            


            <p class=MsoNoSpacing style='text-align:justify'>
                <span lang=KHM
                style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                KHM'>៣.១<span style='mso-tab-count:1'>    </span>អ្នកធានាឈ្មោះ</span>
                <span
                style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                KHM'>
                  {{ $cl_name }} <span lang=KHM>ភេទ</span>..<span
                    lang=KHM> {{$c_g}}</span>....<span lang=KHM>ថ្ងៃខែឆ្នាំកំណើត</span>.......<span
                    lang=KHM> {{ $dob }}</span>.........<span lang=KHM>សញ្ជាតិ</span> {{ $c_n }} 
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing style='margin-left:.5in;text-align:justify'>
                <span
                lang=KHM style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                KHM'>កាន់ប័ណ្ណសម្គាល់អត្តសញ្ញាណលេខ</span>
                <span style='font-size:12.0pt;
                font-family:"Khmer OS Battambang";mso-bidi-language:KHM'>
                 <span
                    lang=KHM>{{ $cl_id}} </span> <span lang=KHM>ប្រភេទប័ណ្ណ</span>{{ $c_id_ty}} <span
                    lang=KHM>ចុះថ្ងៃទី</span> <span lang=KHM>ចេញដោយ</span> <span
                    lang=KHM>{{ $c_id_by}} </span><span lang=KHM> និងឈ្មោះ </span>....<span lang=KHM>{{ $k_clname}}</span> 
                <o:p></o:p>
                </span>
            </p>
            
            <p class=MsoNoSpacing style='margin-left:.5in;text-align:justify'>
                <span
                lang=KHM style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                KHM'>ភេទ</span>
                <span style='font-size:12.0pt;font-family:"Khmer OS Battambang";
                mso-bidi-language:KHM'>
                 {{ $k_ge}}  <span lang=KHM> ថ្ងៃខែឆ្នាំកំណើត </span> <span
                    lang=KHM>{{$k_dob}}</span> <span lang=KHM> </span> <span lang=KHM> សញ្ជាតិ</span> <span
                    lang=KHM>{{$k_cn}}</span>.<span lang=KHM> កាន់ប័ណ្ណ​សម្គាល់អត្តសញ្ញាណ</span>
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing style='margin-left:.5in;text-align:justify'>
                <span
                lang=KHM style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                KHM'>លេខ</span>
                <span style='font-size:12.0pt;font-family:"Khmer OS Battambang";
                mso-bidi-language:KHM'>
                 {{$k_id}} <span lang=KHM>ប្រភេទប័ណ្ណ</span> {{$k_id_ty}} <span
                    lang=KHM>ចុះថ្ងៃទី</span> <span lang=KHM> </span> 
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing style='margin-left:.5in;text-align:justify'>
                <span
                lang=KHM style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                KHM'>ចេញដោយ</span>
                <span style='font-size:12.0pt;font-family:"Khmer OS Battambang";
                mso-bidi-language:KHM'>
                 {{$k_id_by}}<span lang=KHM>​</span> <span
                    lang=KHM> ។ <span style='mso-spacerun:yes'> </span>អាសយដ្ឋាន ផ្ទះលេខ</span> {{$h_num}} <span
                    lang=KHM>ក្រុមទី</span>..<span lang=KHM>{{$gr_num}}</span> <span lang=KHM>ផ្លូវលេខ</span> <span
                    lang=KHM>{{$st_num}}</span> 
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing style='margin-left:.5in;text-align:justify'>
                <span
                lang=KHM style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                KHM'>ភូមិ</span>
                <span style='font-size:12.0pt;font-family:"Khmer OS Battambang";
                mso-bidi-language:KHM'>
                 <span lang=KHM>{{$vi}}</span> <span
                    lang=KHM>ឃុំ</span>/<span lang=KHM>សង្កាត់</span> <span lang=KHM>{{$com}}</span> <span
                    lang=KHM>ស្រុក</span>/<span lang=KHM>ខណ្ឌ</span> <span lang=KHM>{{$des}}</span> 
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing style='margin-left:.5in;text-align:justify'>
                <span
                lang=KHM style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                KHM'>ខេត្ត</span>
                <span style='font-size:12.0pt;font-family:"Khmer OS Battambang";
                mso-bidi-language:KHM'>
                /<span lang=KHM>ក្រុង</span> {{$prov}} <span
                    lang=KHM>តទៅនេះហៅកាត់ថាភាគី </span>(<span lang=KHM>គ</span>)<span lang=KHM>។</span>
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing style='margin-left:.5in;text-align:justify;text-indent:
                -.5in'>
                <span lang=KHM style='font-size:12.0pt;font-family:"Khmer OS Battambang";
                mso-bidi-language:KHM'>៣.២<span style='letter-spacing:-1.0pt'> </span></span><span
                style='font-size:12.0pt;font-family:"Khmer OS Battambang";letter-spacing:-1.0pt;
                mso-bidi-language:KHM'><span style='mso-tab-count:1'>   </span></span><span
                lang=KHM style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                KHM'>ក្នុងករណីភាគី </span>
                <span style='font-size:12.0pt;font-family:"Khmer OS Battambang";
                mso-bidi-language:KHM'>
                (<span lang=KHM>ខ</span>)<span lang=KHM> គ្មានលទ្ធភាពសងប្រាក់ជូនភាគី
                </span>(<span lang=KHM>ក</span>) <span lang=KHM>ទេ នោះភាគី</span> (<span
                    lang=KHM>គ</span>)<span lang=KHM> នឹងមានកាតព្វកិច្ចសងប្រាក់ជំពាក់ទាំងអស់ ដែលមិនទាន់បានសងជំនួសភាគី
                </span>(<span lang=KHM>ខ</span>)<span lang=KHM> ឲ្យគ្រប់ចំនួន។</span>
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing style='margin-left:.75in;text-align:justify;text-indent:
                -.5in'>
                <span style='font-size:12.0pt;font-family:"Khmer OS Battambang";
                mso-bidi-language:KHM'>
                <o:p>&nbsp;</o:p>
                </span>
            </p>
            <p class=MsoNoSpacing style='text-align:justify'>
                <b><span lang=KHM
                style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                KHM'>ប្រការ៤៖​ អវសានបញ្ញត្តិ</span></b>
                <b>
                <span style='font-size:12.0pt;
                    font-family:"Khmer OS Battambang";mso-bidi-language:KHM'>
                    <o:p></o:p>
                </span>
                </b>
            </p>
            <p class=MsoNoSpacing style='margin-left:.5in;text-align:justify;text-indent:
                -.5in'>
                <span lang=KHM style='font-size:12.0pt;font-family:"Khmer OS Battambang";
                mso-bidi-language:KHM'>៤.១<span style='mso-tab-count:1'>    </span>ភាគី</span>
                <span
                style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                KHM'>
                (<span lang=KHM>ក</span>)<span lang=KHM> ភាគី </span>(<span lang=KHM>ខ</span>)
                <span lang=KHM>និងភាគី</span> (<span lang=KHM>គ</span>)<span lang=KHM> សន្យាគោរពយ៉ាងមឺងម៉ាត់តាមរាល់ប្រការនៃកិច្ចសន្យានេះ។
                ក្នុងករណីមានការអនុវត្តផ្ទុយ ឬដោយរំលោភលើលក្ខខណ្ឌណាមួយនៃកិច្ចសន្យានេះ ភាគីដែលល្មើសត្រូវទទួលខុសត្រូវចំពោះមុខច្បាប់ជាធរមាន។
                រាល់សោហ៊ុយចំណាយទាក់ទងក្នុងការដោះស្រាយ វិវាទ ជាបន្ទុករបស់ភាគីដែលបង្ករការរំលោភបំពានលើកិច្ចសន្យា។</span>
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing style='margin-left:.5in;text-align:justify;text-indent:
                -.5in'>
                <span lang=KHM style='font-size:12.0pt;font-family:"Khmer OS Battambang";
                mso-bidi-language:KHM'>៤.២​ </span>
                <span style='font-size:12.0pt;font-family:
                "Khmer OS Battambang";mso-bidi-language:KHM'>
                <span style='mso-tab-count:1'>   </span><span
                    lang=KHM>ភាគីទាំងអស់បានអាន និងយល់យ៉ាងច្បាស់អំពីអត្ថន័យនៃកិច្ចសន្យា ហើយស្ម័គ្រចិត្តគោរពយ៉ាងម៉ឺងម៉ាត់រាល់ប្រការទាំងអស់ដែលមានចែងនៅក្នុងកិច្ចសន្យានេះដោយគ្មានកាបង្ខិតបង្ខំឡើយ។</span>
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing style='margin-left:.5in;text-align:justify;text-indent:
                -.5in'>
                <span lang=KHM style='font-size:12.0pt;font-family:"Khmer OS Battambang";
                mso-bidi-language:KHM'>៤.៣</span>
                <span style='font-size:12.0pt;font-family:
                "Khmer OS Battambang";mso-bidi-language:KHM'>
                <span style='mso-tab-count:1'>    </span><span
                    lang=KHM>កិច្ចសន្យា និងតារាងកាលវិភាគសងប្រាក់នេះមានសុពលភាពចាប់ពីថ្ងៃចុះហត្ថលេខា
                និងផ្តិតស្នាមមេដៃស្តាំតទៅ។</span>
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing style='margin-left:.5in;text-align:justify;text-indent:
                -.5in'>
                <span lang=KHM style='font-size:12.0pt;font-family:"Khmer OS Battambang";
                mso-bidi-language:KHM'>៤.៤ </span>
                <span style='font-size:12.0pt;font-family:
                "Khmer OS Battambang";mso-bidi-language:KHM'>
                <span style='mso-tab-count:1'>   </span><span
                    lang=KHM>កិច្ចសន្យានេះត្រូវបានធ្វើឡើងជា ០៣ច្បាប់ជាភាសាខ្មែរ ១ច្បាប់ដើមរក្សាទុកភាគី
                (ក) ១ច្បាប់រក្សាទុកនៅភាគី (ខ) និង១ច្បាប់ទៀតរក្សានៅភាគី (គ)។ </span>
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing style='text-align:justify'>
                <span style='font-size:12.0pt;
                font-family:"Khmer OS Battambang";mso-bidi-language:KHM'>
                <o:p>&nbsp;</o:p>
                </span>
            </p>
        </div>
        <span style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-fareast-font-family:
            "Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:EN-US;
            mso-bidi-language:KHM'><br clear=all style='page-break-before:auto;mso-break-type:
            section-break'>
        </span>
        <div class=WordSection2>
            <p class=MsoNoSpacing style='text-align:justify'>
                <b>
                <span style='font-size:
                    12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:KHM'>
                    <o:p>&nbsp;</o:p>
                </span>
                </b>
            </p>
       <div class="" style="width:40%; display:inline-block ;">
            <p class=MsoNoSpacing align=center style='text-align:center'>
                <b><span lang=KHM
                style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                KHM'>ស្នាមមេដៃស្តាំភាគី</span></b>
                <b>
                <span style='font-size:12.0pt;font-family:
                    "Khmer OS Battambang";mso-bidi-language:KHM'>
                    (<span lang=KHM>ខ</span>)
                    <o:p></o:p>
                </span>
                </b>
            </p>
            <p class=MsoNoSpacing align=center style='text-align:center'>
                <b><span lang=KHM
                style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                KHM'>ប្តី</span></b>
                <b>
                <span style='font-size:12.0pt;font-family:"Khmer OS Battambang";
                    mso-bidi-language:KHM'>
                    / <span lang=KHM>ប្រពន្ធ</span>
                    <o:p></o:p>
                </span>
                </b>
            </p>
            <p class=MsoNoSpacing style='text-align:justify'>
                <b>
                <span style='font-size:
                    12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:KHM'>
                    <o:p>&nbsp;</o:p>
                </span>
                </b>
            </p>
            <p class=MsoNoSpacing style='text-align:justify'>
                <b>
                <span style='font-size:
                    12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:KHM'>
                    <o:p>&nbsp;</o:p>
                </span>
                </b>
            </p>
            <p class=MsoNoSpacing style='text-align:justify'>
                <b>
                <span style='font-size:
                    12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:KHM'>
                    <o:p>&nbsp;</o:p>
                </span> 
                </b>
            </p>
            
            
            <p class=MsoNoSpacing  align=center style='text-align:center'>
                <b><span lang=KHM
                style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                KHM'>ឈ្មោះ</span></b>
                <b>
                <span  style='font-size:12.0pt;font-family:"Khmer OS Battambang";
                    mso-bidi-language:KHM'>
                    ..............................<span
                        style='mso-spacerun:yes'>   
                </b>
                
            </p>
            <p class=MsoNoSpacing  align=center style='text-align:center'>
                <b><span lang=KHM
                style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                KHM'>ឈ្មោះ</span></b>
                <b>
                <span  style='font-size:12.0pt;font-family:"Khmer OS Battambang";
                    mso-bidi-language:KHM'>
                    ..............................<span
                        style='mso-spacerun:yes'>   
                </b>
                
            </p>
            <p class=MsoNoSpacing style='text-align:justify'>
                <b>
                <span style='font-size:
                    12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:KHM'>
                    <o:p>&nbsp;</o:p>
                </span>
                </b>
            </p>

        </div>

        <div class="" style="width:28%;  display:inline-block ;">
           
            <p class=MsoNoSpacing align=center style='text-align:center'>
                <b><span lang=KHM
                style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                KHM'>ស្នា​​មមេដៃស្តាំអ្នកធានា</span></b>
                <b>
                <span style='font-size:12.0pt;font-family:
                    "Khmer OS Battambang";mso-bidi-language:KHM'>
                   
                    <o:p></o:p>
                </span>
                </b>
            </p>
            <p class=MsoNoSpacing align=center style='text-align:center'>
                <b><span lang=KHM
                style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                KHM'>ភាគី</span></b>
                <b>
                <span style='font-size:12.0pt;font-family:
                    "Khmer OS Battambang";mso-bidi-language:KHM'>
                    (<span lang=KHM>គ</span>)
                    <o:p></o:p>
                </span>
                </b>
            </p>
            <p class=MsoNoSpacing style='text-align:justify'>
                <b>
                <span style='font-size:
                    12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:KHM'>
                    <o:p>&nbsp;</o:p>
                </span>
                </b>
            </p>
            <p class=MsoNoSpacing style='text-align:justify'>
                <b>
                <span style='font-size:
                    12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:KHM'>
                    <o:p>&nbsp;</o:p>
                </span>
                </b>
            </p>
            <p class=MsoNoSpacing style='text-align:justify'>
                <b>
                <span style='font-size:
                    12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:KHM'>
                    <o:p>&nbsp;</o:p>
                </span> 
                </b>
            </p>
            <p class=MsoNoSpacing style='text-align:justify'>
                <b>
                <span style='font-size:
                    12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:KHM'>
                    <o:p>&nbsp;</o:p>
                </span>
                </b>
            </p>
            <p class=MsoNoSpacing  align=center style='text-align:center'>
                <b><span lang=KHM
                style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                KHM'>ឈ្មោះ</span></b>
                <b>
                <span  style='font-size:12.0pt;font-family:"Khmer OS Battambang";
                    mso-bidi-language:KHM'>
                    .............................<span
                        style='mso-spacerun:yes'>   
                </b>
            </p>
            <p class=MsoNoSpacing style='text-align:justify'>
                <b>
                <span style='font-size:
                    12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:KHM'>
                    <o:p>&nbsp;</o:p>
                </span>
                </b>
            </p>
        </div>
        <div class="" style="width:28%;  display:inline-block ;">
           
            <p class=MsoNoSpacing align=center style='text-align:center'>
                <b><span lang=KHM
                style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                KHM'>តំណាងឲ្យភាគី(</span></b>
                <b>
                <span style='font-size:12.0pt;font-family:
                    "Khmer OS Battambang";mso-bidi-language:KHM'>
                   (<span lang=KHM>ក</span>)
                    <o:p></o:p>
                </span>
                </b>
            </p>
            <p class=MsoNoSpacing align=center style='text-align:center'>
                <b><span lang=KHM
                style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                KHM'>ហត្ថលេខា និងឈ្មោះ</span></b>
                <b>
                <span style='font-size:12.0pt;font-family:
                    "Khmer OS Battambang";mso-bidi-language:KHM'>
                    
                    <o:p></o:p>
                </span>
                </b>
            </p>
            <p class=MsoNoSpacing style='text-align:justify'>
                <b>
                <span style='font-size:
                    12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:KHM'>
                    <o:p>&nbsp;</o:p>
                </span>
                </b>
            </p>
            <p class=MsoNoSpacing style='text-align:justify'>
                <b>
                <span style='font-size:
                    12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:KHM'>
                    <o:p>&nbsp;</o:p>
                </span>
                </b>
            </p>
            <p class=MsoNoSpacing style='text-align:justify'>
                <b>
                <span style='font-size:
                    12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:KHM'>
                    <o:p>&nbsp;</o:p>
                </span> 
                </b>
            </p>
            <p class=MsoNoSpacing style='text-align:justify'>
                <b>
                <span style='font-size:
                    12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:KHM'>
                    <o:p>&nbsp;</o:p>
                </span>
                </b>
            </p>
            <p class=MsoNoSpacing  align=center style='text-align:center'>
                <b><span lang=KHM
                style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                KHM'>ឈ្មោះ</span></b>
                <b>
                <span  style='font-size:12.0pt;font-family:"Khmer OS Battambang";
                    mso-bidi-language:KHM'>
                    .............................<span
                        style='mso-spacerun:yes'>   
                </b>
            </p>
            <p class=MsoNoSpacing style='text-align:justify'>
                <b>
                <span style='font-size:
                    12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:KHM'>
                    <o:p>&nbsp;</o:p>
                </span>
                </b>
            </p>
        </div>
            
        <div class="" style="width:48%;  display:inline-block ;">
                <p class=MsoNoSpacing align=center style='text-align:center'>
                    <b><span lang=KHM
                    style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                    KHM'>បានឃើញ និងបញ្ជាក់ថាសេចក្តីបញ្ជាក់របស់</span></b>
                    <b>
                    <span style='font-size:
                        12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:KHM'>
                        <o:p></o:p>
                    </span>
                    </b>
                </p>
                <p class=MsoNoSpacing align=center style='text-align:center'>
                    <b><span lang=KHM
                    style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                    KHM'>មេភូមិ</span></b>
                    <span style='font-size:12.0pt;font-family:"Khmer OS Battambang";
                    mso-bidi-language:KHM'>
                    ...............................................
                    <o:p></o:p>
                    </span>
                </p>
                <p class=MsoNoSpacing align=center style='text-align:center'>
                    <span lang=KHM
                    style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                    KHM'>ថ្ងៃទី</span>
                    <span style='font-size:12.0pt;font-family:"Khmer OS Battambang";
                    mso-bidi-language:KHM'>
                    ..................<span lang=KHM>ខែ</span>.....................<span
                        lang=KHM>ឆ្នាំ</span>....................
                    <o:p></o:p>
                    </span>
                </p>
                <p class=MsoNoSpacing align=center style='text-align:center'>
                    <b><span lang=KHM
                    style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                    KHM'>មេឃុំ</span></b><b><span style='font-size:12.0pt;font-family:"Khmer OS Battambang";
                    mso-bidi-language:KHM'>/<span lang=KHM>ចៅសង្កាត់</span></span></b>
                    <span
                    style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                    KHM'>
                    .....................................
                    <o:p></o:p>
                    </span>
                </p>
                <p class=MsoNoSpacing align=center style='text-align:center'>
                    <b><span lang=KHM
                    style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                    KHM'>ហត្ថលេខា និងត្រា</span></b>
                    <b>
                    <span style='font-size:12.0pt;font-family:
                        "Khmer OS Battambang";mso-bidi-language:KHM'>
                        <o:p></o:p>
                    </span>
                    </b>
                </p>
                <p class=MsoNoSpacing style='text-align:justify'>
                    <b>
                    <span style='font-size:
                        12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:KHM'>
                        <o:p>&nbsp;</o:p>
                    </span>
                    </b>
                </p>


        </div>
        <div class="" style="width:48%;  display:inline-block ;">
                
                <p class=MsoNoSpacing align=center style='text-align:center'>
                    <b><span lang=KHM
                    style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                    KHM'>បានឃើញ និងទទួលស្គាល់ថា</span></b>
                    <b>
                    <span style='font-size:12.0pt;
                        font-family:"Khmer OS Battambang";mso-bidi-language:KHM'>
                        <o:p></o:p>
                    </span>
                    </b>
                </p>
                <p class=MsoNoSpacing align=center style='text-align:center'>
                    <span lang=KHM
                    style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                    KHM'>កម្មសិទ្ធិស្របច្បាប់របស់ភាគី(ខ)ប្រាកដមែន ហើយទ្រព្យទាំង​នេះពុំមាន<span
                    style='mso-spacerun:yes'>  </span>ពាក់ព័ន្ធនិងបញ្ហាអ្វីឡើយ ហើយភាគី(ខ)
                    បានយល់ព្រមដាក់បញ្ចាំដោយ ស្ម័គ្រចិត្តឲ្យភាគី(ក) ពិតប្រាកដមែន។</span>
                    <span
                    style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                    KHM'>
                    <o:p></o:p>
                    </span>
                </p>
                <p class=MsoNoSpacing align=center style='text-align:center'>
                    <span lang=KHM
                    style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                    KHM'>ថ្ងៃទី</span>
                    <span style='font-size:12.0pt;font-family:"Khmer OS Battambang";
                    mso-bidi-language:KHM'>
                    ..................<span lang=KHM>ខែ</span>.....................<span
                        lang=KHM>ឆ្នាំ</span>....................
                    <o:p></o:p>
                    </span>
                </p>
                <p class=MsoNoSpacing align=center style='text-align:center'>
                    <b><span lang=KHM
                    style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                    KHM'>មេភូមិ</span></b>
                    <span style='font-size:12.0pt;font-family:"Khmer OS Battambang";
                    mso-bidi-language:KHM'>
                    ................................................
                    <o:p></o:p>
                    </span>
                </p>
                <p class=MsoNoSpacing align=center style='text-align:center'>
                    <b><span lang=KHM
                    style='font-size:12.0pt;font-family:"Khmer OS Battambang";mso-bidi-language:
                    KHM'>ហត្ថលេខា និងឈ្មោះ</span></b>
                    <b>
                    <span style='font-size:12.0pt;font-family:
                        "Khmer OS Battambang";mso-bidi-language:KHM'>
                        <o:p></o:p>
                    </span>
                    </b>
                </p>
            </div>
        </div>
        <span lang=KHM style='font-size:12.0pt;font-family:"Khmer OS Battambang";
            mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:
            EN-US;mso-bidi-language:KHM'><br clear=all style='page-break-before:auto;
            mso-break-type:section-break'>
        </span>
        <div class=WordSection3>
            <p class=MsoNoSpacing style='text-align:justify'>
                <span style='font-size:12.0pt;
                font-family:"Khmer OS Battambang";mso-bidi-language:KHM'>
                <o:p>&nbsp;</o:p>
                </span>
            </p>
        </div>
    </body>
    
<meta name="_token" content="{{ csrf_token() }}" />
<!-- ១២៣៤៥៦៧៨៩០ -->
<script type="text/javascript">
$(document).ready(function(){
    $(window).load(function(){
        get_sale_credit_sale_show();
    });

    var count = 10;
        function str_pad_left(num,count){
            if(count > 0){
            var num_c = num.toString();
            var str = "0";
            var str_num = num_c.length;
            var count1 = count - str_num;
            var str1 = str.repeat(count1);
            return str1+""+num_c;
            }else{
                return console.log("error right count please it number and bigger then  0");
            }
        }
        function get_date(get_date){
            var d = new Date(get_date);
            var day_n = d.getDate();
            if(day_n > 10){
                day_n = day_n;
            }else{
                day_n = "0"+day_n; 
            }
            return day_n;
        }
       
        function get_month(get_month){
            var d = new Date(get_month);
            var month_n = d.getMonth() + 1;
            if(month_n > 10){
                month_n = month_n;
            }else{
                month_n = "0"+month_n; 
            }
            return month_n;
        }
        function get_year(get_year){
            var d = new Date(get_year);
            var year_n = d.getFullYear();
            return year_n;
        }
        function get_sale_credit_sale_show(){
            var url_json =  "{{ url('accounts/aprove_credit_sales/'.$data_id.'/show_json') }}";
                    $.ajax({
                        type: "GET",
                        url: url_json, 
                        dataType: "json",
                        success: function(result){
                            console.log(result);
                            var text = "";
                            // $.each(result.approval_item,function(i,da){
                            //     text += "<tr> " ;
                            //     // text += "<td>  <button class='btn btn-danger btn-mini remove_item' value='"+da.product_id+"'><i class='icon-remove icon-white ' style='padding-right: 0px;'></i></button>   "+da.item.name+" </td>";
                            //     text += "<td> "+da.item.name+" </td>";
                            //     text +=  "<td> "+da.item.item_bacode+"  </td>";
                            //     text +=  "<td> "+da.item.categorys.name+" </td>"
                            //     text +=  "<td> "+da.qty+" </td>";
                            //     text +=  "<td>  "+accounting.formatMoney(da.sell_price)+" </td>";
                            //     text +=  "<td>  "+accounting.formatMoney(da.total_price_payment)+" </td>";
                            //     text += "</tr> "; 
                               
                            // });

                        //     $(".item_list").html(text); 
                        //     if(result.method == "sale_by_credit"){
                        //         var mothod = "បង់រំលស់";
                        //     }else{
                        //         var mothod = "បង់ជាសាច់ប្រាក់";   
                        //     }
                        //     var  dpm = result.date_for_payments;
                        //     var d = new Date(dpm);
                        //     var year_n = d.getFullYear();
                        //     var month_n = d.getMonth() + 1;
                        //     var day_n = d.getDate();
                        //     if(month_n > 10){
                        //         month_n = month_n;
                        //     }else{
                        //         month_n = "0"+month_n; 
                        //     }
                        //     if(day_n > 10){
                        //         day_n = day_n;
                        //     }else{
                        //         day_n = "0"+day_n; 
                        //     }
                        //    var  date_for_payments = day_n +"-"+month_n+"-"+year_n;
                        //    var dpmt = result.duration_pay_money_type;
                        //     if(dpmt == "month"){
                        //         dpmt = "ខែ";
                        //     }else if(dpmt == "2week"){
                        //         dpmt = "២ សប្តាហ៍";
                        //     }else if(dpmt == "week"){
                        //         dpmt = "សប្តាហ៍";
                        //     }else if(dpmt == "day"){
                        //         dpmt = "ថ្ងៃ";
                        //     }  
                        //     if(result.date_approval){
                        //         $(".date_create_contract").text("..."+get_date(result.date_approval)+"...");
                        //         $(".month_create_contract").html("..."+get_month(result.date_approval)+"...");
                        //         $(".year_create_contract").text("    "+get_year(result.date_approval));
                        //     }
                        //     if(result.cs_sales.branch.brand_name){
                        //         $(".where_create_contract").text("..."+result.cs_sales.branch.brand_name+"...");
                        //     }
                            
                        //     if(result.cs_sales.cs_client.kh_username){
                        //         $(".kh_username").text("  "+result.cs_sales.cs_client.kh_username+"   ");
                        //     }
                        //     if(result.cs_sales.cs_client.gender == 'F'){
                        //             var gender = "ស្រី";
                        //     }else{
                        //             var gender = "ប្រុស";
                        //     }
                        //     $(".client_gender").text("  "+gender+"   ");
                            
                        //     if(result.cs_sales.cs_client.dob){
                        //           var dob = get_date(result.cs_sales.cs_client.dob)+"-"+get_month(result.cs_sales.cs_client.dob)+"-"+get_year(result.cs_sales.cs_client.dob);
                        //           $(".client_dob").text("  "+dob+"   ");
                                  
                        //     }
                        //     if(result.cs_sales.cs_client.nationality == 1){
                        //         var national = "ខ្មែរ";
                        //     }else{nationality
                        //         var national = result.cs_sales.cs_client.nationality;
                        //     }
                        //     $(".nationality").text("  "+national+"   ");
                            
                        //     if(result.sale_id){
                        //         $(".contract_id").text(str_pad_left(result.sale_id,count));
                        //     }
                            
                        //     $(".method").text(mothod); 
                        //     $(".prices_total_num_text").text(accounting.formatMoney(result.prices_total_num));
                        //     $(".deposit_fixed_text").text(accounting.formatMoney(result.deposit_fixed));
                        //     $(".duration_pay_money_text").text(result.duration_pay_money);
                        //     $(".duration_pay_money_type_text").text(dpmt);
                        //     $(".date_for_payments_text").text(date_for_payments );
                        //     $(".money_owne_text").text(accounting.formatMoney(result.money_owne));    
                            
                        },
                        error: function (result ,status, xhr) {
                            console.log(result.responseText);
                        }
                    
                    });
        }

 });       
</script> 
    </html>