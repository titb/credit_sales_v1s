@extends('credit_sale.layout.master')
@section('contend')
<style>
ul.typeahead.dropdown-menu {
    margin-top: -10px;
}
</style>
<div class="container-fluid">
            <div class="row-fluid">
                     <!-- validation -->
                    <div class="row-fluid">
                         <!-- block -->
                        <div class="block">
                          <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left"><a href="{!! url('accounts') !!}"> គណនី</a> <span class="divider">|</span> {{$title}} </div>
                            </div>  

                            <div class="block-content collapse in">
                            @include('credit_sale.client_management.menu_client')
                            @include('errors.error')
                            	<table class="table table-bordered">

							      <thead style="background: rgb(251, 205, 205);">

							        <tr class="header">

							            <th>ល.រ</th>
							            <th>សំណើ​រ ល.រ</th>
							            <th>ចំនួនទំនិញ</th>
                                        <th>ប្រាក់ដើម</th>
                                        <th>កាប្រាក់</th>
                                        <th>តម្លៃសរុប</th>
                                        <th>សកម្មភាព</th>

							        </tr>
							      </thead>
							      <tbody class="item_list">
                                       
                                  </tbody>
                                    <tr>
                                        <td colspan="6">
                                            <b class="pull-right">សរុប:</b>
                                        </td>
                                        <td>
                                            <b id="total_all"></b>
                                            <input type="hidden" id="client_id" value="{{$client_id}}">
                                        </td>
                                    </tr>
							    </table>
                            	<!-- Pagination -->
								<div class="pagination text-right"></div>
			    			</div>
						</div>
                     	<!-- /block -->
		    		</div>
                     <!-- /validation -->
                </div>

            </div>

<meta name="_token" content="{{ csrf_token() }}" />

<script>
    var client_id = $("#client_id").val();
    var url = '{{ url("accounts/history_payment/sale_credit_json") }}' +"/"+ client_id;
    $.get( url, function( result ) {
        console.log(result);
        if(result != ''){
            var data;
            $.each(result, function(k, v){
                data += '<tr>';
                data += '<td>'+(k+1)+'</td>';
                data += '<td>'+(k+1)+'</td>';
                data += '<tr>';
            });
        }else{
            $(".item_list").append("<tr><td>មិនមានទិន្នន័យ</td></tr>");
        }
        
    });
</script>            
@stop()
