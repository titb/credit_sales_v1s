
<html>

<head>
    <meta http-equiv=Content-Type content="text/html; charset=utf-8">
    <meta name=ProgId content=Word.Document>
    <meta name=Generator content="Microsoft Word 15">
    <meta name=Originator content="Microsoft Word 15">

    <title>កិច្ចសន្យាទិញបង់រំលស់</title>
    <script src="{{ url('assets/jquery.min.js')}}"></script>
    <script src="{{ URL::to('assets/accounting.min.js')}}"></script>
    <style>

        /* Font Definitions */
        @font-face
        {font-family:Wingdings;
        panose-1:5 0 0 0 0 0 0 0 0 0;
        mso-font-charset:2;
        mso-generic-font-family:auto;
        mso-font-pitch:variable;
        mso-font-signature:0 268435456 0 0 -2147483648 0;}
        @font-face
        {font-family:"Cambria Math";
        panose-1:2 4 5 3 5 4 6 3 2 4;
        mso-font-charset:0;
        mso-generic-font-family:roman;
        mso-font-pitch:variable;
        mso-font-signature:-536869121 1107305727 33554432 0 415 0;}
        @font-face
        {font-family:"Battambang";
        panose-1:2 0 5 0 0 0 0 2 0 4;
        mso-font-charset:0;
        mso-generic-font-family:auto;
        mso-font-pitch:variable;
        mso-font-signature:-1610612497 1342185546 65536 0 273 0;}
        @font-face
        {font-family:"Battambang";
        panose-1:2 0 5 0 0 0 0 2 0 4;
        mso-font-charset:0;
        mso-generic-font-family:auto;
        mso-font-pitch:variable;
        mso-font-signature:-1610612497 1342185546 65536 0 273 0;}
        @font-face
        {font-family:"Limon S1";
        panose-1:0 0 0 0 0 0 0 0 0 0;
        mso-font-charset:0;
        mso-generic-font-family:auto;
        mso-font-pitch:variable;
        mso-font-signature:3 0 0 0 1 0;}
        @font-face
        {font-family:"Limon S6";
        mso-font-alt:Calibri;
        mso-font-charset:0;
        mso-generic-font-family:auto;
        mso-font-pitch:variable;
        mso-font-signature:3 0 0 0 1 0;}
        @font-face
        {font-family:Tahoma;
        panose-1:2 11 6 4 3 5 4 4 2 4;
        mso-font-charset:0;
        mso-generic-font-family:swiss;
        mso-font-pitch:variable;
        mso-font-signature:-520081665 -1073717157 41 0 66047 0;}
        @font-face
        {font-family:'Moul', cursive;
        panose-1:2 0 5 0 0 0 0 2 0 4;
        mso-font-charset:0;
        mso-generic-font-family:auto;
        mso-font-pitch:variable;
        mso-font-signature:-1593824529 1342185546 65536 0 66047 0;}
        @font-face
        {font-family:'Moul', cursive;
        panose-1:2 0 5 0 0 0 0 2 0 4;
        mso-font-charset:0;
        mso-generic-font-family:auto;
        mso-font-pitch:variable;
        mso-font-signature:-1610612497 1342185546 65536 0 273 0;}
        /* Style Definitions */
        p.MsoNormal, li.MsoNormal, div.MsoNormal
        {mso-style-unhide:no;
        mso-style-qformat:yes;
        mso-style-parent:"";
        margin:0in;
        margin-bottom:.0001pt;
        mso-pagination:widow-orphan;
        font-size:10.0pt;
        font-family:"Times New Roman",serif;
        mso-fareast-font-family:"Times New Roman";
        mso-fareast-language:EN-US;
        mso-bidi-language:AR-SA;}
        h1
        {mso-style-unhide:no;
        mso-style-qformat:yes;
        mso-style-link:"Heading 1 Char";
        mso-style-next:Normal;
        margin:0in;
        margin-bottom:.0001pt;
        text-align:right;
        text-indent:.5in;
        mso-pagination:widow-orphan;
        page-break-after:avoid;
        mso-outline-level:1;
        font-size:22.0pt;
        mso-bidi-font-size:10.0pt;
        font-family:"Limon S1";
        mso-font-kerning:0pt;
        mso-fareast-language:EN-US;
        mso-bidi-language:AR-SA;
        font-weight:normal;}
        h2
        {mso-style-unhide:no;
        mso-style-qformat:yes;
        mso-style-next:Normal;
        margin:0in;
        margin-bottom:.0001pt;
        mso-pagination:widow-orphan;
        page-break-after:avoid;
        mso-outline-level:2;
        font-size:13.0pt;
        mso-bidi-font-size:10.0pt;
        font-family:"Times New Roman",serif;
        mso-bidi-font-family:Arial;
        color:navy;
        mso-fareast-language:EN-US;
        mso-bidi-language:AR-SA;}
        h3
        {mso-style-unhide:no;
        mso-style-qformat:yes;
        mso-style-next:Normal;
        margin:0in;
        margin-bottom:.0001pt;
        mso-pagination:widow-orphan;
        page-break-after:avoid;
        mso-outline-level:3;
        font-size:10.0pt;
        font-family:"Times New Roman",serif;
        mso-bidi-font-family:Arial;
        color:#003300;
        mso-fareast-language:EN-US;
        mso-bidi-language:AR-SA;}
        h4
        {mso-style-unhide:no;
        mso-style-qformat:yes;
        mso-style-link:"Heading 4 Char";
        mso-style-next:Normal;
        margin:0in;
        margin-bottom:.0001pt;
        mso-pagination:widow-orphan;
        page-break-after:avoid;
        mso-outline-level:4;
        font-size:20.0pt;
        font-family:"Limon S6";
        mso-bidi-font-family:Arial;
        mso-fareast-language:EN-US;
        mso-bidi-language:AR-SA;
        font-weight:normal;}
        h5
        {mso-style-unhide:no;
        mso-style-qformat:yes;
        mso-style-link:"Heading 5 Char";
        mso-style-next:Normal;
        margin-top:12.0pt;
        margin-right:0in;
        margin-bottom:3.0pt;
        margin-left:0in;
        mso-pagination:widow-orphan;
        mso-outline-level:5;
        font-size:13.0pt;
        font-family:"Times New Roman",serif;
        mso-fareast-language:EN-US;
        mso-bidi-language:AR-SA;
        font-style:italic;}
        p.MsoHeader, li.MsoHeader, div.MsoHeader
        {mso-style-priority:99;
        mso-style-link:"Header Char";
        margin:0in;
        margin-bottom:.0001pt;
        mso-pagination:widow-orphan;
        tab-stops:center 3.25in right 6.5in;
        font-size:10.0pt;
        font-family:"Times New Roman",serif;
        mso-fareast-font-family:"Times New Roman";
        mso-fareast-language:EN-US;
        mso-bidi-language:AR-SA;}
        p.MsoFooter, li.MsoFooter, div.MsoFooter
        {mso-style-priority:99;
        mso-style-link:"Footer Char";
        margin:0in;
        margin-bottom:.0001pt;
        mso-pagination:widow-orphan;
        tab-stops:center 3.25in right 6.5in;
        font-size:10.0pt;
        font-family:"Times New Roman",serif;
        mso-fareast-font-family:"Times New Roman";
        mso-fareast-language:EN-US;
        mso-bidi-language:AR-SA;}
        a:link, span.MsoHyperlink
        {mso-style-priority:99;
        color:blue;
        mso-themecolor:hyperlink;
        text-decoration:underline;
        text-underline:single;}
        a:visited, span.MsoHyperlinkFollowed
        {mso-style-noshow:yes;
        mso-style-priority:99;
        color:purple;
        mso-themecolor:followedhyperlink;
        text-decoration:underline;
        text-underline:single;}
        p.MsoAcetate, li.MsoAcetate, div.MsoAcetate
        {mso-style-noshow:yes;
        mso-style-priority:99;
        mso-style-link:"Balloon Text Char";
        margin:0in;
        margin-bottom:.0001pt;
        mso-pagination:widow-orphan;
        font-size:8.0pt;
        font-family:"Tahoma",sans-serif;
        mso-fareast-font-family:"Times New Roman";
        mso-fareast-language:EN-US;
        mso-bidi-language:AR-SA;}
        p.MsoNoSpacing, li.MsoNoSpacing, div.MsoNoSpacing
        {mso-style-priority:1;
        mso-style-unhide:no;
        mso-style-qformat:yes;
        mso-style-parent:"";
        margin:0in;
        margin-bottom:.0001pt;
        mso-pagination:widow-orphan;
        font-size:10.0pt;
        font-family:"Times New Roman",serif;
        mso-fareast-font-family:"Times New Roman";
        mso-fareast-language:EN-US;
        mso-bidi-language:AR-SA;}
        p.MsoListParagraph, li.MsoListParagraph, div.MsoListParagraph
        {mso-style-priority:34;
        mso-style-unhide:no;
        mso-style-qformat:yes;
        margin-top:0in;
        margin-right:0in;
        margin-bottom:0in;
        margin-left:.5in;
        margin-bottom:.0001pt;
        mso-add-space:auto;
        mso-pagination:widow-orphan;
        font-size:10.0pt;
        font-family:"Times New Roman",serif;
        mso-fareast-font-family:"Times New Roman";
        mso-fareast-language:EN-US;
        mso-bidi-language:AR-SA;}
        p.MsoListParagraphCxSpFirst, li.MsoListParagraphCxSpFirst, div.MsoListParagraphCxSpFirst
        {mso-style-priority:34;
        mso-style-unhide:no;
        mso-style-qformat:yes;
        mso-style-type:export-only;
        margin-top:0in;
        margin-right:0in;
        margin-bottom:0in;
        margin-left:.5in;
        margin-bottom:.0001pt;
        mso-add-space:auto;
        mso-pagination:widow-orphan;
        font-size:10.0pt;
        font-family:"Times New Roman",serif;
        mso-fareast-font-family:"Times New Roman";
        mso-fareast-language:EN-US;
        mso-bidi-language:AR-SA;}
        p.MsoListParagraphCxSpMiddle, li.MsoListParagraphCxSpMiddle, div.MsoListParagraphCxSpMiddle
        {mso-style-priority:34;
        mso-style-unhide:no;
        mso-style-qformat:yes;
        mso-style-type:export-only;
        margin-top:0in;
        margin-right:0in;
        margin-bottom:0in;
        margin-left:.5in;
        margin-bottom:.0001pt;
        mso-add-space:auto;
        mso-pagination:widow-orphan;
        font-size:10.0pt;
        font-family:"Times New Roman",serif;
        mso-fareast-font-family:"Times New Roman";
        mso-fareast-language:EN-US;
        mso-bidi-language:AR-SA;}
        p.MsoListParagraphCxSpLast, li.MsoListParagraphCxSpLast, div.MsoListParagraphCxSpLast
        {mso-style-priority:34;
        mso-style-unhide:no;
        mso-style-qformat:yes;
        mso-style-type:export-only;
        margin-top:0in;
        margin-right:0in;
        margin-bottom:0in;
        margin-left:.5in;
        margin-bottom:.0001pt;
        mso-add-space:auto;
        mso-pagination:widow-orphan;
        font-size:10.0pt;
        font-family:"Times New Roman",serif;
        mso-fareast-font-family:"Times New Roman";
        mso-fareast-language:EN-US;
        mso-bidi-language:AR-SA;}
        span.Heading1Char
        {mso-style-name:"Heading 1 Char";
        mso-style-unhide:no;
        mso-style-locked:yes;
        mso-style-link:"Heading 1";
        mso-ansi-font-size:22.0pt;
        font-family:"Limon S1";
        mso-ascii-font-family:"Limon S1";
        mso-hansi-font-family:"Limon S1";
        mso-bidi-language:AR-SA;}
        span.Heading4Char
        {mso-style-name:"Heading 4 Char";
        mso-style-unhide:no;
        mso-style-locked:yes;
        mso-style-link:"Heading 4";
        mso-ansi-font-size:20.0pt;
        mso-bidi-font-size:20.0pt;
        font-family:"Limon S6";
        mso-ascii-font-family:"Limon S6";
        mso-hansi-font-family:"Limon S6";
        mso-bidi-font-family:Arial;
        mso-bidi-language:AR-SA;}
        span.Heading5Char
        {mso-style-name:"Heading 5 Char";
        mso-style-unhide:no;
        mso-style-locked:yes;
        mso-style-link:"Heading 5";
        mso-ansi-font-size:13.0pt;
        mso-bidi-font-size:13.0pt;
        mso-bidi-language:AR-SA;
        font-weight:bold;
        font-style:italic;}
        span.BalloonTextChar
        {mso-style-name:"Balloon Text Char";
        mso-style-noshow:yes;
        mso-style-priority:99;
        mso-style-unhide:no;
        mso-style-locked:yes;
        mso-style-link:"Balloon Text";
        mso-ansi-font-size:8.0pt;
        mso-bidi-font-size:8.0pt;
        font-family:"Tahoma",sans-serif;
        mso-ascii-font-family:Tahoma;
        mso-hansi-font-family:Tahoma;
        mso-bidi-font-family:Tahoma;
        mso-bidi-language:AR-SA;}
        span.HeaderChar
        {mso-style-name:"Header Char";
        mso-style-priority:99;
        mso-style-unhide:no;
        mso-style-locked:yes;
        mso-style-link:Header;
        mso-bidi-language:AR-SA;}
        span.FooterChar
        {mso-style-name:"Footer Char";
        mso-style-priority:99;
        mso-style-unhide:no;
        mso-style-locked:yes;
        mso-style-link:Footer;
        mso-bidi-language:AR-SA;}
        .MsoChpDefault
        {mso-style-type:export-only;
        mso-default-props:yes;
        font-size:10.0pt;
        mso-ansi-font-size:10.0pt;
        mso-bidi-font-size:10.0pt;
        mso-fareast-language:EN-US;
        mso-bidi-language:KHM;}
        /* Page Definitions */
        @page
        {mso-footnote-separator:url("F01-Credit%20sale%20contract%20PHN_files/header.html") fs;
        mso-footnote-continuation-separator:url("F01-Credit%20sale%20contract%20PHN_files/header.html") fcs;
        mso-endnote-separator:url("F01-Credit%20sale%20contract%20PHN_files/header.html") es;
        mso-endnote-continuation-separator:url("F01-Credit%20sale%20contract%20PHN_files/header.html") ecs;}
        @page WordSection1
        {size:595.35pt 842.0pt;
        margin:.5in .5in .5in .5in;
        mso-header-margin:.2in;
        mso-footer-margin:5.75pt;
        mso-header:url("F01-Credit%20sale%20contract%20PHN_files/header.html") h1;
        mso-footer:url("F01-Credit%20sale%20contract%20PHN_files/header.html") f1;
        mso-paper-source:0;}
        div.WordSection1
        {page:WordSection1;}
        @page WordSection2
        {size:595.35pt 842.0pt;
        margin:28.35pt 28.05pt 28.35pt 28.05pt;
        mso-header-margin:.5in;
        mso-footer-margin:.5in;
        mso-columns:2 even .2in;
        mso-header:url("F01-Credit%20sale%20contract%20PHN_files/header.html") h1;
        mso-footer:url("F01-Credit%20sale%20contract%20PHN_files/header.html") f1;
        mso-paper-source:0;}
        div.WordSection2
        {page:WordSection2;}
        @page WordSection3
        {size:595.35pt 842.0pt;
        margin:.35in 28.1pt 19.45pt 28.1pt;
        mso-header-margin:.5in;
        mso-footer-margin:.5in;
        mso-header:url("F01-Credit%20sale%20contract%20PHN_files/header.html") h1;
        mso-footer:url("F01-Credit%20sale%20contract%20PHN_files/header.html") f1;
        mso-paper-source:0;}
        div.WordSection3
        {page:WordSection3;}
        /* List Definitions */
        .button {
                background-color: #4CAF50; /* Green */
                border: none;
                color: white;
                padding: 10px 25px;
                text-align: center;
                text-decoration: none;
                display: inline-block;
                font-size: 16px;
                margin: 4px 2px;
                -webkit-transition-duration: 0.4s; /* Safari */
                transition-duration: 0.4s;
                cursor: pointer;
            }

            .button1 {
                background-color: white; 
                color: black; 
                border: 2px solid #4CAF50;
            }

            .button1:hover {
                background-color: #4CAF50;
                color: white;
            }

            .button2 {
                background-color: white; 
                color: black; 
                border: 2px solid #008CBA;
            }

            .button2:hover {
                background-color: #008CBA;
                color: white;
            }
        </style>
</head>

<!-- <body id=print_me lang=EN-US link=blue vlink=purple style='tab-interval:.5in; margin-left:50px;'> -->
<div id=print_me lang=EN-US link=blue vlink=purple style='tab-interval:.5in; margin-left:50px;'>
    <button id="print_hide" class="button button1" onclick="with_print()">print</button>
    <button id="go_back" class="button button2" onclick="go_back()">Go Back</button>
    <script>
        document.getElementById('print_me').style.width = "210mm";    
            function with_print(){
                document.getElementById('print_hide').style.display = "none";
                document.getElementById('go_back').style.display = "none";
                document.getElementById('print_me').removeAttribute("style");
                window.print();
                document.getElementById('print_me').style.width = "210mm"; 
                document.getElementById('print_me').style.marginLeft = "50px";  
                document.getElementById('print_hide').removeAttribute("style");
                document.getElementById('go_back').removeAttribute("style");
            }
            function go_back(){
                window.location = "{{ url('accounts/sub_client/show_sub_client?client_id='.$data_id) }}";
            }
        </script>
    
    <div class="WordSection1">

        <p class="MsoNormal" align="center" style="text-align:center;tab-stops:9.0pt">
            <div class="WordSection1">

                <p class="MsoNormal" align="center" style="text-align:center;tab-stops:9.0pt"><span lang="KHM" style="font-size:11.0pt;font-family:&quot;Moul&quot;;mso-bidi-language:
KHM">ខេ</span><span
                        style="font-size:11.0pt;font-family:&quot;Moul&quot;;
mso-bidi-language:KHM">.<span
                            lang="KHM">អេ</span>.<span lang="KHM">អេស</span>.<span lang="KHM">ភី</span>.<span lang="KHM">
                            កសិករ ពាណិជ្ជ ឯ.ក</span>
                        <o:p></o:p>
                    </span></p>

                <p class="MsoNormal" align="center" style="text-align:center;tab-stops:9.0pt"><span lang="KHM" style="font-size:11.0pt;font-family:&quot;Moul&quot;;mso-bidi-language:
KHM">
                        <o:p>&nbsp;</o:p>
                    </span></p>

                <p class="MsoNormal" align="center" style="text-align:center;tab-stops:center 255.15pt left 387.75pt"><span
                        lang="KHM" style="font-size:11.0pt;font-family:&quot;Moul&quot;;mso-bidi-language:
KHM">ព័ត៌មានអតិថិជន</span><span
                        lang="KHM" style="font-size:12.0pt;font-family:
&quot;Moul&quot;;mso-bidi-language:KHM">
                        <o:p></o:p>
                    </span></p>
<!-- Client Detail -->
<?php
        if(!empty($cl_data->cs_client)){
            if($cl_data->cs_client->kh_username){
                $na = $cl_data->cs_client->kh_username;
            }else{
                $na = "";
            }
            
            $en_na = $cl_data->en_username;
            $ge = $cl_data->cs_client->gender;
            $dob = $cl_data->cs_client->dob;
            $id_n = $cl_data->cs_client->identify_num;
            $phone = $cl_data->cs_client->phone;
            $job = $cl_data->cs_client->job;
            $place_job = $cl_data->cs_client->place_job;
            if($cl_data->cs_client->gender == 'F'){
                $ge = "ស្រី";
            }else{
                $ge = "ប្រុស";
            }

            if($cl_data->cs_client->nationality == 1){
                $nationality = "ខ្មែរ";
            }else{
                $nationality = $cl_data->cs_client->nationality;
            }

            if($cl_data->cs_client->identify_type == 1){
                $id_ty = "អត្តសញ្ញាណប័ណ្ណ";
            }elseif($cl_data->cs_client->identify_type == 2){
                $id_ty = "លិខិនឆ្លង់ដែន";
            }elseif($cl_data->cs_client->identify_type == 3){
                $id_ty = "សំបុត្របញ្ជាក់កំណើត";
            }elseif($cl_data->cs_client->identify_type == 4){
                $id_ty = "សៀវភៅសា្នក់នៅ";
            }elseif($cl_data->cs_client->identify_type == 5){
                $id_ty = "សៀវភៅគ្រួសារ";
            }else{
                $id_ty = $cl_data->cs_client->identify_type;
            }

            if($cl_data->cs_client->identify_by == 1){
                $id_by = "ក្រសួងមហាផ្ទៃ";
            }else{
                $id_by = $cl_data->cs_client->identify_by;
            }
            $home_num = $cl_data->cs_client->home_num;
            $group_num = $cl_data->cs_client->group_num;
            $street_num = $cl_data->cs_client->street_num;
            $vilige = $cl_data->cs_client->vilige;
            $commune = $cl_data->cs_client->commune;
            $district = $cl_data->cs_client->district;
            $province = $cl_data->cs_client->province;
            // Item
            $cs_sa = $cl_data->cs_sales;
            $name = "";
            $qty = 0;
            $brand = "";
            $item_code = "";
            foreach($cs_sa->sale_item as $key=>$si){
                if($key != 0){
                    $name .= ", ".$si->item->name;
                    $brand .= ", ".$si->item->cs_brand->name;
                    $item_code .= ", ".$si->item->item_bacode;
                }else{
                    $name .= $si->item->name;
                    $brand .= $si->item->cs_brand->name;
                    $item_code .= $si->item->item_bacode;
                }
                $qty += $si->qty;
            }
        }else{
            $na = "";
            $en_na = "";
            $ge = "";
            $dob = "";
            $nationality = "";
            $id_n = "";
            $id_ty = "";
            $id_by = "";
            $home_num = "";
            $group_num = "";
            $street_num = "";
            $vilige = "";
            $commune = "";
            $district = "";
            $province = "";
            $phone = "";
            $job = "";
            $place_job = "";
            $name = "";
            $qty = "";
            $brand = "";
            $item_code = "";
            $total_money = "";
            $total_pay_word = "";
            $deposit_precent = "";
            $deposit_fixed = "";
            $deposit_fixed_word = "";
            $duration_pay_money = "";
            $date_for_payments = "";
        }
        if(!empty($cl_data->prices_total_num)){
            $total_money = $cl_data->prices_total_num;
        }else{
            $total_money = "";
        }
        if(!empty($cl_data->prices_totalword)){
            $total_pay_word = $cl_data->prices_totalword;
        }else{
            $total_pay_word = "";
        }
        if(!empty($cl_data->deposit_precent)){
            $deposit_precent = $cl_data->deposit_precent;
        }else{
            $deposit_precent = "";
        }
        if(!empty($cl_data->deposit_fixed)){
            $deposit_fixed = $cl_data->deposit_fixed;
        }else{
            $deposit_fixed = "";
        }
        if(!empty($cl_data->deposit_fixed_word)){
            $deposit_fixed_word = $cl_data->deposit_fixed_word;
        }else{
            $deposit_fixed_word = "";
        }
        if(!empty($cl_data->duration_pay_money)){
            $duration_pay_money = $cl_data->duration_pay_money;
        }else{
            $duration_pay_money = "";
        }
        if(!empty($cl_data->date_for_payments)){
            $date_for_payments = $cl_data->date_for_payments;
        }else{
            $date_for_payments = "";
        }
        if(!empty($cl_data->schedule)){
            $cl_sched = $cl_data->schedule;
        }else{
            $cl_sched = "";
        }
        if(!empty($cl_sched->cs_schedule_timesheet)){
            $total_pay = $cl_sched->cs_schedule_timesheet->first()->total_payment;
        }else{
            $total_pay = ".....";
        }
        if(!empty($cl_data->duration_pay_money_type)){
            if($cl_data->duration_pay_money_type == 'month'){
                $durat_type = "ខែ";
            }elseif($cl_data->duration_pay_money_type == 'week'){
                $durat_type = "សបា្តហ៍";
            }elseif($cl_data->duration_pay_money_type == '2week'){
                $durat_type = "ពីរសបា្តហ៍";
            }else{
                $durat_type = "";
            }
        }else{
            $durat_type = "";
        }
        if(!empty($cl_data->method)){
            if($cl_data->method == 'sale_by_credit'){
                $method = "þ";
            }else{
                $method = "¨";
            }
        }else{
            $method = "";
        }
        if(!empty($cl_data->duration_pay_money_type)){
            if($cl_data->duration_pay_money_type == 'month'){
                $durat_type_month = "þ";
            }else{
                $durat_type_month = "¨";
            }
        }else{
            $durat_type_month = "";
        }
?>
                <p class="MsoNormal" style="text-align:justify"><span lang="KHM" style="font-size:
12.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">ឈ្មោះ</span><span
                        style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM"> {{$na}} <span
                            lang="KHM"></span><span lang="KHM"></span></span><span
                        lang="KHM" style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">ជាអក្សរឡាតាំង</span><span
                        style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM"><span
                            lang="KHM"></span>​​ {{$en_na}} <span lang="KHM"></span></span><span
                        lang="KHM" style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">ភេទ</span><span
                        style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM"> {{$ge}} </span><span
                        lang="KHM" style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">ថ្ងៃខែឆ្នាំកំណើត</span><span
                        style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM"> <span
                            lang="KHM">{{$dob}}</span> </span><br><span lang="KHM"
                        style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">សញ្ជាតិ</span><span
                        style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM"> {{$nationality}} </span><span
                        lang="KHM" style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">កាន់ប័ណ្ណសំគាល់អត្តសញ្ញាណលេខ</span><span
                        style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM"> {{$id_n}}<span
                            lang="KHM"> </span> </span><span lang="KHM" style="font-size:11.0pt;
font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">ប្រភេទប័ណ្ណ</span><span
                        style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM"> <span
                            lang="KHM">{{$id_ty}}</span> <o:p></o:p></span>
                </p>

                <p class="MsoNormal" style="text-align:justify"><span lang="KHM" style="font-size:
11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">ចុះថ្ងៃទី</span><span
                        style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">......................................................</span><span
                        lang="KHM" style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">ចេញដោយ</span><span
                        style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM"> {{$id_by}} </span><span
                        lang="KHM" style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">។ អាសយដ្ឋានផ្ទះលេខ</span><span style="font-size:11.0pt;font-family:&quot;Battambang&quot;;
mso-bidi-language:KHM"> {{$home_num}} </span><span
                        lang="KHM" style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">ក្រុមទី</span><span
                        style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM"> {{$group_num}} </span><span
                        lang="KHM" style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">ផ្លូវលេខ</span><span
                        style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM"> {{$street_num}} </span><span
                        lang="KHM" style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">ភូមិ</span><span
                        style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM"> {{$vilige}} </span><span
                        lang="KHM" style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">ឃុំ</span><span
                        style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">/<span lang="KHM">សង្កាត់</span></span><span
                        style="font-size:11.0pt;font-family:&quot;Battambang&quot;;
mso-bidi-language:KHM"> {{$commune}} </span><span
                        lang="KHM" style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">ស្រុក</span><span
                        style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">/<span lang="KHM">ខណ្ឌ</span></span><span
                        style="font-size:11.0pt;font-family:&quot;Battambang&quot;;
mso-bidi-language:KHM"> {{$district}} </span><span
                        lang="KHM" style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">ខេត្តក្រុង</span><span
                        style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM"> {{$province}} </span><span
                        lang="KHM" style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">
                    </span><span style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:
KHM">
                        <o:p></o:p>
                    </span></p>

                <p class="MsoNormal" style="text-align:justify"><span lang="KHM" style="font-size:
11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">លេខទូរស័ព្ទទំនាក់ទំនង</span><span
                        style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM"> ..........................................<span
                            lang="KHM"> {{$phone}} </span>....................</span><span
                        lang="KHM" style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">។
                    </span><span style="font-size:6.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:
KHM">
                        <o:p></o:p>
                    </span></p>

                <p class="MsoNormal" style="text-align:justify"><span lang="KHM" style="font-size:
11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">មុខរបរ</span><span
                        style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM"> <span
                            lang="KHM">  {{$job}}  </span></span><span
                        lang="KHM" style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">ទីកន្លែងប្រកបមុខរបរ</span><span
                        style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM"> {{$place_job}} <span
                            lang="KHM">
                            <o:p></o:p>
                        </span></span></p>
                <p class="MsoNormal" style="text-align:justify"><span lang="KHM" style="font-size:
11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">ក្រោយពីបានយល់ច្បាស់ពីគោលការណ៍
                        និងលក្ខខណ្ឌរបស់ក្រុមហ៊ុនខេ.អេ.អេស.ភី. កសិករ ពាណិជ្ជ ឯ.ក ខ្ញុំបាទ
                        នាងខ្ញុំបានយល់ព្រមទិញ</span><span lang="KHM" style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:
KHM">..{{$name}}..</span><span lang="KHM" style="font-size:11.0pt;font-family:&quot;Battambang&quot;;
mso-bidi-language:KHM">ចំនួន</span><span
                        lang="KHM" style="font-size:11.0pt;
font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">..{{$qty}}..</span><span
                        lang="KHM" style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">គ្រឿងម៉ាក</span>
                        <span lang="KHM" style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:
KHM">.. {{$brand}} ..
                    </span><span style="font-size:6.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:
KHM">
                        <o:p></o:p>
                    </span></p>

                <p class="MsoNormal" style="text-align:justify"><span lang="KHM" style="font-size:
11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">លេខកូដ</span><span
                        lang="KHM" style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM"> .. {{$item_code}} ..</span><span
                        lang="KHM" style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">ក្នុងតម្លៃសរុបជាលេខ<span
                            style="mso-spacerun:yes"></span></span><span lang="KHM" style="font-size:11.0pt;
font-family:&quot;Battambang&quot;;mso-bidi-language:KHM"> {{$total_money}} </span><span 
                        style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">(US$)<span
                        lang="KHM">​​ ជាអក្សរ</span></span><span lang="KHM" style="font-size:6.0pt;
font-family:&quot;Battambang&quot;;mso-bidi-language:KHM"> </span><span
                        style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM"><span
                            lang="KHM">{{$total_pay_word}}</span></span><span
                        style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">
                        <o:p></o:p>
                    </span></p>
                    <p class="MsoNormal" style="text-align:justify">
                        <span
                            style='font-size:12.0pt;font-family:Wingdings;mso-ascii-font-family:"Battambang";
                            mso-hansi-font-family:"Battambang";mso-bidi-font-family:"Battambang";
                            mso-bidi-language:KHM;mso-char-type:symbol;mso-symbol-font-family:Wingdings'><span
                            style='mso-char-type:symbol;mso-symbol-font-family:Wingdings'></span></span><span
                            lang=KHM style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                            KHM'> ទិញជាលក្ខណៈ <span style='mso-tab-count:1'>     </span></span><span
                            style='font-size:12.0pt;font-family:Wingdings;mso-ascii-font-family:"Battambang";
                            mso-hansi-font-family:"Battambang";mso-bidi-font-family:"Battambang";
                            mso-bidi-language:KHM;mso-char-type:symbol;mso-symbol-font-family:Wingdings'>¨<span
                            style='mso-char-type:symbol;mso-symbol-font-family:Wingdings'></span></span><span
                            lang=KHM style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                            KHM'> ជាសាច់ប្រាក់ <span style='mso-spacerun:yes'>  </span><span
                            style='mso-tab-count:1'>         </span></span>
                            
                            <span style='font-size:12.0pt;
                            font-family:Wingdings;mso-ascii-font-family:"Battambang";mso-hansi-font-family:
                            "Battambang";mso-bidi-font-family:"Battambang";mso-bidi-language:
                            KHM;mso-char-type:symbol;mso-symbol-font-family:Wingdings'><span
                            style='mso-char-type:symbol;mso-symbol-font-family:Wingdings'>{{$method}}</span></span><span
                            lang=KHM style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                            KHM'> បង់រំលស់</span>
                            <span style='font-size:12.0pt;font-family:"Battambang";
                            mso-bidi-language:KHM'>
                            <o:p></o:p>
                        </span>
                    </p>
                <p class="MsoNormal" style="text-align:justify"><span lang="KHM" style="font-size:
11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">ដោយបង់ប្រាក់កក់មុនចំនួន</span><span
                        lang="KHM" style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM"> {{$deposit_precent}} </span><span
                        lang="KHM" style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">ភាគរយស្មើនឹងចំនួនសរុបជាលេខ</span><span
                        lang="KHM" style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM"> {{$deposit_fixed}} </span><span
                        style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">(US$)<span 
                        lang="KHM">​​ជាអក្សរ</span></span><span lang="KHM" style="font-size:11.0pt;
font-family:&quot;Battambang&quot;;mso-bidi-language:KHM"> {{$deposit_fixed_word}} </span><span
                        lang="KHM" style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">។</span><span
                        style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">
                        <o:p></o:p>
                    </span></p>

                <p class="MsoNormal" style="text-align:justify"><span lang="KHM" style="font-size:
11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">រយៈពេលបង់ប្រាក់</span><span
                        style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM"> <span lang="KHM">{{$duration_pay_money}}</span> </span><span
                        lang="KHM" style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">គិតជា</span><span
                        style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM"> <span
                            lang="KHM">{{$durat_type}}</span> </span><span lang="KHM" style="font-size:11.0pt;
font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">សុំបង់ថ្ងៃទី</span><span
                        style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM"> <span
                            lang="KHM">{{$date_for_payments}}</span> </span><span lang="KHM"
                        style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">ស្មើនឹងចំនួន</span><span
                        lang="KHM" style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">.. {{$total_pay}} ..</span><span
                        lang="KHM" style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">
                    </span><span style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:
KHM">(US$)<o:p></o:p></span></p>
                <p class="MsoNormal" style="text-align:justify"><span lang="KHM" style="font-size:
11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">ខ្ញុំបាទនាងខ្ញុំ
                        សូមសន្យាថា នឹងបំពេញបែបបទបន្ថែមទៀតបើក្រុមហ៊ុនត្រូវការ ហើយនឹងផ្តល់នូវរាល់ការប្រែប្រួល</span><span
                        style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">
                        <o:p></o:p>
                    </span></p>

                <p class="MsoNormal" style="text-align:justify"><span lang="KHM" style="font-size:
11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">ទាក់ទងនិងគ្រួសារទ្រព្យសម្បត្តិ
                        និងរាល់ព្រឹត្តិការណ៍ដទៃទៀតដែលបណ្តាលឲ្យខូចប្រយោជន៍ក្រុមហ៊ុន។ </span><span style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">
                        <o:p></o:p>
                    </span></p>

                <p class="MsoNormal" style="text-align:justify"><span lang="KHM" style="font-size:
11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">បើពុំនោះទេ
                        ខ្ញុំបាទនាងខ្ញុំ
                        សុខចិត្តទទួលខុសត្រូវទាំងស្រុងចំពោះមុខច្បាប់ក្នុងការខាតបង់ដែលបណ្តាលមកពីការធ្វេសប្រហែស<span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;
                        </span><span style="mso-spacerun:yes">&nbsp;</span>និងការផ្តល់ព័ត៍មានផ្ទុយពីការពិតរបស់ខ្ញុំបាទ
                        នាងខ្ញុំ។<span style="mso-spacerun:yes">&nbsp; </span><span style="mso-spacerun:yes">&nbsp;</span></span><span
                        style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">
                        <o:p></o:p>
                    </span></p>

                <p class="MsoNormal" style="text-align:justify"><span lang="KHM" style="font-size:
11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">ដើម្បីជាសក្ខីភាពនៃសំណើខាងលើ
                        ខ្ញុំបាទ នាងខ្ញុំសូមដាក់ជូនភ្ជាប់មកជាមួយនូវៈ<o:p></o:p></span></p>

                <p class="MsoListParagraphCxSpFirst" style="margin-left:.25in;mso-add-space:auto;
text-align:justify;text-indent:-.25in;mso-list:l0 level1 lfo1">
                    <!--[if !supportLists]--><span style="font-size:11.0pt;font-family:Wingdings;mso-fareast-font-family:Wingdings;
mso-bidi-font-family:Wingdings;mso-bidi-language:KHM"><span
                            style="mso-list:
Ignore">v<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;
                            </span></span></span>
                    <!--[endif]--><span lang="KHM" style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">ច្បាប់ថតចំលងឯកសារសម្គាល់
                        អត្តសញ្ញាណរបស់ និងឯកសារដែលពាក់ព័ន្ធ</span><span style="font-size:6.0pt;
font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">................................</span><span
                        lang="KHM" style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">ច្បាប់</span><span
                        style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">
                        <o:p></o:p>
                    </span></p>

                <p class="MsoListParagraphCxSpLast" style="margin-left:.25in;mso-add-space:auto;
text-align:justify;text-indent:-.25in;mso-list:l0 level1 lfo1">
                    <!--[if !supportLists]--><span style="font-size:11.0pt;font-family:Wingdings;mso-fareast-font-family:Wingdings;
mso-bidi-font-family:Wingdings;mso-bidi-language:KHM"><span
                            style="mso-list:
Ignore">v<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;
                            </span></span></span>
                    <!--[endif]--><span lang="KHM" style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">ផ្សេងៗ</span><span
                        style="font-size:6.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">.........................<span
                            lang="KHM">.................................................................................................................................................................................................</span>.......</span><span
                        lang="KHM" style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">ច្បាប់</span><span
                        style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">
                        <o:p></o:p>
                    </span></p>

                <p class="MsoNormal" style="text-align:justify;text-indent:.25in"><span style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">
                        <o:p>&nbsp;</o:p>
                    </span></p>

                <p class="MsoNormal" align="center" style="margin-left:.5in;text-align:center"><span lang="KHM" style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">ថ្ងៃទី</span><span
                        style="font-size:6.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">.......<span
                            lang="KHM">..........</span>..........</span><span lang="KHM" style="font-size:
11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">ខែ</span><span
                        style="font-size:6.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">...................</span><span
                        lang="KHM" style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">ឆ្នាំ</span><span
                        style="font-size:6.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">...................<span
                            lang="KHM">​</span>
                        <o:p></o:p>
                    </span></p>

            </div>

            <span lang="KHM" style="font-size:6.0pt;font-family:&quot;Battambang&quot;;mso-fareast-font-family:
&quot;Times New Roman&quot;;mso-ansi-language:EN-US;mso-fareast-language:EN-US;
mso-bidi-language:KHM"><br
                    clear="all" style="page-break-before:auto;mso-break-type:
section-break">
            </span>

            <div class="WordSection2">

                <p class="MsoNormal" align="left" style="margin-left:8%"><span lang="KHM" style="font-size:11.0pt;font-family:&quot;Moul&quot;;mso-bidi-language:
KHM">ស្នាមមេដៃស្តាំអតិថិជន</span><span
                        style="font-size:11.0pt;font-family:
&quot;Moul&quot;;mso-bidi-language:KHM">
                        <o:p></o:p>
                    </span></p>

                    <p class="MsoNormal" align="right" style="margin-right:8%"><span lang="KHM" style="font-size:11.0pt;font-family:&quot;Moul&quot;;mso-bidi-language:
KHM">ហត្ថលេខា
                        បុគ្គលិក</span><span style="font-size:11.0pt;font-family:&quot;Moul&quot;;
mso-bidi-language:KHM">
                        <o:p></o:p>
                    </span></p>

                <p class="MsoNormal" align="center" style="text-align:center"><span style="font-size:11.0pt;font-family:&quot;Moul&quot;;mso-bidi-language:
KHM">
                        <o:p>&nbsp;</o:p>
                    </span></p>

                <p class="MsoNormal" align="center" style="text-align:center"><span style="font-size:11.0pt;font-family:&quot;Moul&quot;;mso-bidi-language:
KHM">
                        <o:p>&nbsp;</o:p>
                    </span></p>

                <p class="MsoNormal" align="center" style="text-align:center"><span style="font-size:11.0pt;font-family:&quot;Moul&quot;;mso-bidi-language:
KHM">
                        <o:p>&nbsp;</o:p>
                    </span></p>

                <p class="MsoNormal" align="center" style="text-align:center"><span style="font-size:11.0pt;font-family:&quot;Moul&quot;;mso-bidi-language:
KHM">
                        <o:p>&nbsp;</o:p>
                    </span></p>

                <p class="MsoNormal" style="text-align:justify;text-indent:.25in"><span lang="KHM" style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">ឈ្មោះ</span><span
                    lang="KHM" style="font-size:6.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">......................................................................................................</span><span
                    style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">
                    <o:p></o:p>
                </span></p>
                <p class="MsoNormal" style="text-align:right;text-indent:.25in"><span lang="KHM" style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">ឈ្មោះ</span><span
                    lang="KHM" style="font-size:6.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">......................................................................................................</span><span
                    style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">
                    <o:p></o:p>
                </span></p>
                
                

            </div>

            <span lang="KHM" style="font-size:11.0pt;font-family:&quot;Moul&quot;;
mso-fareast-font-family:&quot;Times New Roman&quot;;mso-ansi-language:EN-US;mso-fareast-language:
EN-US;mso-bidi-language:KHM"><br
                    clear="all" style="page-break-before:auto;
mso-break-type:section-break">
            </span>

           

            <span style="font-size:11.0pt;font-family:&quot;Battambang&quot;;mso-fareast-font-family:
&quot;Times New Roman&quot;;mso-ansi-language:EN-US;mso-fareast-language:EN-US;
mso-bidi-language:KHM"><br
                    clear="all" style="page-break-before:auto;mso-break-type:
section-break">
            </span>

            <div class="WordSection4">

                <p class="MsoNormal" style="text-align:justify"><span style="font-size:11.0pt;
font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">
                        <o:p>&nbsp;</o:p>
                    </span></p>

                <p class="MsoNormal" style="text-align:justify"><span style="font-size:11.0pt;
font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">
                        <o:p>&nbsp;</o:p>
                    </span></p>

                <p class="MsoNormal" style="text-align:justify"><span style="font-size:11.0pt;
font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">
                        <o:p>&nbsp;</o:p>
                    </span></p>

                <p class="MsoNormal" style="text-align:justify"><span style="font-size:11.0pt;
font-family:&quot;Battambang&quot;;mso-bidi-language:KHM">
                        <o:p>&nbsp;</o:p>
                    </span></p>

                
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                        <tr>
                            <td>
                                <!--[endif]-->
                                <div style="border: 1px solid #ddd;">
                                    <p class="MsoNormal" align="center" style="line-height: 250%;text-align:center"><span lang="KHM"
                                            style="font-size:11.0pt;font-family:&quot;Battambang&quot;;color:black;mso-themecolor:
     text1;mso-bidi-language:KHM">សូមគូសផែនទីនៃទីតាំងផ្ទះរបស់អ្នកខ្ចីនិង
                                            អ្នកធានាឲ្យបានច្បាស់លាស់នៅទីនេះ។</span><span style="color:black;mso-themecolor:text1">
                                            <o:p></o:p>
                                        </span></p>
                                    <span style="float: right;"><img src="{{url('printing/direction.png')}}" alt=""></span>
                                    <p class="MsoNormal" align="right" style="text-align:right"><span style="font-size:11.0pt;font-family:&quot;Battambang&quot;;color:black;mso-themecolor:
     text1;mso-bidi-language:KHM;mso-no-proof:yes">
                                            <v:shapetype id="_x0000_t75" coordsize="21600,21600" o:spt="75"
                                                o:preferrelative="t" path="m@4@5l@4@11@9@11@9@5xe" filled="f" stroked="f">
                                                <v:stroke joinstyle="miter">
                                                    <v:formulas>
                                                        <v:f eqn="if lineDrawn pixelLineWidth 0">
                                                            <v:f eqn="sum @0 1 0">
                                                                <v:f eqn="sum 0 0 @1">
                                                                    <v:f eqn="prod @2 1 2">
                                                                        <v:f eqn="prod @3 21600 pixelWidth">
                                                                            <v:f eqn="prod @3 21600 pixelHeight">
                                                                                <v:f eqn="sum @0 0 1">
                                                                                    <v:f eqn="prod @6 1 2">
                                                                                        <v:f eqn="prod @7 21600 pixelWidth">
                                                                                            <v:f eqn="sum @8 21600 0">
                                                                                                <v:f eqn="prod @7 21600 pixelHeight">
                                                                                                    <v:f eqn="sum @10 21600 0">
                                                                                                    </v:f>
                                                                                                </v:f>
                                                                                            </v:f>
                                                                                        </v:f>
                                                                                    </v:f>
                                                                                </v:f>
                                                                            </v:f>
                                                                        </v:f>
                                                                    </v:f>
                                                                </v:f>
                                                            </v:f>
                                                        </v:f>
                                                    </v:formulas>
                                                    <v:path o:extrusionok="f" gradientshapeok="t" o:connecttype="rect">
                                                        <o:lock v:ext="edit" aspectratio="t">
                                                        </o:lock>
                                                    </v:path>
                                                </v:stroke>
                                            </v:shapetype>
                                            <v:shape id="Picture_x0020_13" o:spid="_x0000_i1025" type="#_x0000_t75"
                                                style="width:74.4pt;height:60.6pt;visibility:visible;mso-wrap-style:square">
                                                <v:imagedata src="file:///C:/Users/USER/AppData/Local/Temp/msohtmlclip1/01/clip_image001.png"
                                                    o:title="Untitled">
                                                </v:imagedata>
                                            </v:shape>
                                        </span><span style="font-size:11.0pt;font-family:&quot;Battambang&quot;;
     color:black;mso-themecolor:text1;mso-bidi-language:KHM">
                                            <o:p></o:p>
                                        </span></p>
                                    <p class="MsoNormal"><span lang="KHM" style="font-size:11.0pt;font-family:
     &quot;Battambang&quot;;color:black;mso-themecolor:text1;mso-bidi-language:KHM"><span
                                                style="mso-tab-count:2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            </span><span style="mso-spacerun:yes">&nbsp;</span><span style="mso-spacerun:yes">&nbsp;</span></span><span
                                            lang="KHM" style="font-family:&quot;Battambang&quot;;color:black;mso-themecolor:text1;
     mso-bidi-language:KHM">
                                            <o:p></o:p>
                                        </span></p>
                                    <p class="MsoNormal"><span lang="KHM" style="font-size:16.0pt;mso-ansi-font-size:
     10.0pt;font-family:DaunPenh;mso-ascii-font-family:&quot;Times New Roman&quot;;
     mso-hansi-font-family:&quot;Times New Roman&quot;;color:black;mso-themecolor:text1;
     mso-bidi-language:KHM"><span
                                                style="mso-tab-count:12">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            </span></span><span style="mso-bidi-font-size:16.0pt;mso-bidi-font-family:DaunPenh;color:black;
     mso-themecolor:text1;mso-bidi-language:KHM">
                                            <o:p></o:p>
                                        </span></p>
                                    <p class="MsoNormal"><span style="font-size:11.0pt;color:black;mso-themecolor:
     text1">
                                            <o:p>&nbsp;</o:p>
                                        </span></p>
                                    <p class="MsoNormal"><span style="color:black;mso-themecolor:text1">
                                            <o:p>&nbsp;</o:p>
                                        </span></p>
                                    <p class="MsoNormal"><span style="color:black;mso-themecolor:text1">
                                            <o:p>&nbsp;</o:p>
                                        </span></p>
                                    <p class="MsoNormal"><span style="color:black;mso-themecolor:text1">
                                            <o:p>&nbsp;</o:p>
                                        </span></p>
                                    <p class="MsoNormal"><span style="color:black;mso-themecolor:text1">
                                            <o:p>&nbsp;</o:p>
                                        </span></p>
                                    <p class="MsoNormal"><span style="color:black;mso-themecolor:text1">
                                            <o:p>&nbsp;</o:p>
                                        </span></p>
                                    <p class="MsoNormal"><span style="color:black;mso-themecolor:text1">
                                            <o:p>&nbsp;</o:p>
                                        </span></p>
                                    <p class="MsoNormal"><span style="color:black;mso-themecolor:text1">
                                            <o:p>&nbsp;</o:p>
                                        </span></p>
                                    <p class="MsoNormal"><span style="color:black;mso-themecolor:text1">
                                            <o:p>&nbsp;</o:p>
                                        </span></p>
                                    <p class="MsoNormal"><span style="color:black;mso-themecolor:text1">
                                            <o:p>&nbsp;</o:p>
                                        </span></p>
                                    <p class="MsoNormal"><span style="color:black;mso-themecolor:text1">
                                            <o:p>&nbsp;</o:p>
                                        </span></p>
                                    <p class="MsoNormal"><span style="color:black;mso-themecolor:text1">
                                            <o:p>&nbsp;</o:p>
                                        </span></p>
                                    <p class="MsoNormal"><span style="color:black;mso-themecolor:text1">
                                            <o:p>&nbsp;</o:p>
                                        </span></p>
                                    <p class="MsoNormal"><span style="color:black;mso-themecolor:text1">
                                            <o:p>&nbsp;</o:p>
                                        </span></p>
                                    <p class="MsoNormal"><span style="color:black;mso-themecolor:text1">
                                            <o:p>&nbsp;</o:p>
                                        </span></p>
                                    <p class="MsoNormal"><span style="color:black;mso-themecolor:text1">
                                            <o:p>&nbsp;</o:p>
                                        </span></p>
                                    <p class="MsoNormal"><span style="color:black;mso-themecolor:text1">
                                            <o:p>&nbsp;</o:p>
                                        </span></p>
                                    <p class="MsoNormal"><span style="color:black;mso-themecolor:text1">
                                            <o:p>&nbsp;</o:p>
                                        </span></p>
                                    <p class="MsoNormal"><span style="color:black;mso-themecolor:text1">
                                            <o:p>&nbsp;</o:p>
                                        </span></p>
                                    <p class="MsoNormal"><span style="color:black;mso-themecolor:text1">
                                            <o:p>&nbsp;</o:p>
                                        </span></p>
                                    <p class="MsoNormal"><span style="color:black;mso-themecolor:text1">
                                            <o:p>&nbsp;</o:p>
                                        </span></p>
                                    <p class="MsoNormal"><span style="color:black;mso-themecolor:text1">
                                            <o:p>&nbsp;</o:p>
                                        </span></p>
                                    <p class="MsoNormal"><span style="color:black;mso-themecolor:text1">
                                            <o:p>&nbsp;</o:p>
                                        </span></p>
                                    <p class="MsoNormal"><span style="color:black;mso-themecolor:text1">
                                            <o:p>&nbsp;</o:p>
                                        </span></p>
                                    <p class="MsoNormal"><span style="color:black;mso-themecolor:text1">
                                            <o:p>&nbsp;</o:p>
                                        </span></p>
                                    <p class="MsoNormal"><span style="color:black;mso-themecolor:text1">
                                            <o:p>&nbsp;</o:p>
                                        </span></p>
                                    <p class="MsoNormal"><span style="color:black;mso-themecolor:text1">
                                            <o:p>&nbsp;</o:p>
                                        </span></p>
                                    <p class="MsoNormal"><span style="color:black;mso-themecolor:text1">
                                            <o:p>&nbsp;</o:p>
                                        </span></p>
                                    <p class="MsoNormal"><span style="color:black;mso-themecolor:text1">
                                            <o:p>&nbsp;</o:p>
                                        </span></p>
                                    <p class="MsoNormal"><span style="color:black;mso-themecolor:text1">
                                            <o:p>&nbsp;</o:p>
                                        </span></p>
                                    <p class="MsoNormal"><span style="color:black;mso-themecolor:text1">
                                            <o:p>&nbsp;</o:p>
                                        </span></p>
                                    <p class="MsoNormal"><span style="color:black;mso-themecolor:text1">
                                            <o:p>&nbsp;</o:p>
                                        </span></p>
                                    <p class="MsoNormal"><span style="color:black;mso-themecolor:text1">
                                            <o:p>&nbsp;</o:p>
                                        </span></p>
                                    <p class="MsoNormal"><span style="color:black;mso-themecolor:text1">
                                            <o:p>&nbsp;</o:p>
                                        </span></p>
                                    <p class="MsoNormal"><span style="color:black;mso-themecolor:text1">
                                            <o:p>&nbsp;</o:p>
                                        </span></p>
                                    <p class="MsoNormal"><span style="color:black;mso-themecolor:text1">
                                            <o:p>&nbsp;</o:p>
                                        </span></p>
                                    <p class="MsoNormal"><span style="color:black;mso-themecolor:text1">
                                            <o:p>&nbsp;</o:p>
                                        </span></p>
                                    <p class="MsoNormal"><span style="color:black;mso-themecolor:text1">
                                            <o:p>&nbsp;</o:p>
                                        </span></p>
                                    <p class="MsoNormal"><span style="color:black;mso-themecolor:text1">
                                            <o:p>&nbsp;</o:p>
                                        </span></p>
                                    <p class="MsoNormal"><span style="color:black;mso-themecolor:text1">
                                            <o:p>&nbsp;</o:p>
                                        </span></p>
                                </div>
                                <!--[if !mso]-->
                            </td>
                        </tr>
                    </tbody>
                </table>
                <!--[endif]-->
                <span style="font-size:11.0pt;font-family:&quot;Battambang&quot;;
mso-bidi-language:KHM">
                    <o:p>&nbsp;</o:p>
                </span>
                <p></p>

                
        </p>
    </div>
</div>
<!-- </body> -->

<meta name="_token" content="{{ csrf_token() }}" />

</html>
