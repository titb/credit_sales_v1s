@extends('credit_sale.layout.master')

@section('contend')

	

<div class="container-fluid">

            <div class="row-fluid">

               

                     <!-- validation -->

                    <div class="row-fluid">

                         <!-- block -->

                        <div class="block">

                            <div class="navbar navbar-inner block-header">
                            	<!-- === -->
                            </div>

                            <div class="block-content collapse in">

                            @if (count($errors) > 0)

						          <div class="alert alert-danger">

						            <strong>Whoops!</strong> There were some problems with your input.<br><br>

						            <ul>

						              @foreach ($errors->all() as $error)

						                <li>{{ $error }}</li>

						              @endforeach

						            </ul>

						          </div>

						    @endif

                            @if ($message = Session::get('success'))

		                        <div class="alert alert-success">

		                            <p>{{ $message }}</p>

		                        </div>

		                    @endif

		                    @if($message = Session::get('keyerror'))

		                        <div class="alert alert-danger">

		                            <p>{{ $message }}</p>

		                        </div>

		                    @endif



                             	<center>

			                        <h3 class="cen_title text-center khmer_Moul">Module Interest</h3> 

			                    	<div class="muted span3 pull-right" style="margin-bottom:5px;"><a href="{{ url('module_interest/create') }}" class="btn btn-success pull-right"><i class="icon-plus icon-white"></i> បង្កើតថ្មី</a></div>

			                    </center>

                            	<legend></legend>

							    <table class="table table-bordered">

							      <thead style="background: rgb(251, 205, 205);">

							        <tr class="header">

							            <th>No.</th>

							            <th>name</th>

							            <th>display name english</th>

							            <th>display name khmer</th>

							            <th>status</th>

							            <th>description</th>
                                        
							            <th>action</th>

							        </tr>

							      </thead>

							      <tbody>

							        	@foreach($data as $key => $d) 

							        		<tr>
							        			<td>{{$key+1}}</td>
							        			<td>{{$d->name}}</td>
							        			<td>{{$d->display_name_eng}}</td>
							        			<td>{{$d->display_name_kh}}</td>
							        			<td>
							        				@if($d->active == 1)
				                  					  <span class="badge badge-success"> Active </span>
				                  					@else
				                  					  <span class="badge badge-important"> Not Active </span>
				                  					@endif
							        			</td>
							        			<td>{{$d->description}}</td>
							        			<td>
							                  		<a href="{!! url('module_interest/'.$d->id.'/edit') !!}" class="btn btn-primary">កែប្រែ</a>
							                  		{!! Form::open(['method' => 'post','url' => 'module_interest/'.$d->id.'/deleted','style'=>'display:inline']) !!}
										            {!! Form::submit('លុប', ['class' => 'btn btn-danger']) !!}
										        	{!! Form::close() !!}
							        			</td>
							        		</tr>

								        @endforeach

							      </tbody>

							    </table>
                            	<!-- Pagination -->
								<div class="pagination text-right">{!! $data->render() !!}</div>

			    			</div>

						</div>

                     	<!-- /block -->

		    		</div>

                     <!-- /validation -->

                </div>

            </div>	
	

@stop()