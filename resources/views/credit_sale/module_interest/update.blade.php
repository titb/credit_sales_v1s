@extends('credit_sale.layout.master')
@section('contend')

	

<div class="container-fluid">
            <div class="row-fluid">
                     <!-- validation -->
                    <div class="row-fluid">
                         <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">

                            </div>
                            <div class="block-content collapse in">
                            @if (count($errors) > 0)
						          <div class="alert alert-danger">
						            <strong>Whoops!</strong> There were some problems with your input.<br><br>
						            <ul>
						              @foreach ($errors->all() as $error)
						                <li>{{ $error }}</li>
						              @endforeach
						            </ul>
						          </div>
						    @endif
                            @if ($message = Session::get('success'))
		                        <div class="alert alert-success">
		                            <p>{{ $message }}</p>
		                        </div>
		                    @endif
		                     @if($message = Session::get('keyerror'))
		                        <div class="alert alert-danger">
		                            <p>{{ $message }}</p>
		                        </div>
		                    @endif
                            <div class="span12">
                            	<h3 class="cen_title"> កែប្រែ Module Interest </h3>
                            	<legend></legend>
                            </div>
                            	<form action="{{ url('module_interest/'.$data->id.'/edit') }}" method="post" enctype="multipart/form-data">
	                                {{ csrf_field() }}

                                	<div class="span5">
                                		<label class="control-label">ឈ្មោះ</label>
		  								<div class="controls">
		  									<input type="text" name="name" value="{{$data->name}}" data-required="1" class="span12 m-wrap"/>
		  								</div>
                                	</div>

                                	<div class="span5">
                                		<label class="control-label">ឈ្មោះបង្ហាញជាអង់គ្លេស</label>
		  								<div class="controls">
		  									<input type="text" name="display_name_eng" value="{{$data->display_name_eng}}" data-required="1" class="span12 m-wrap"/>
		  								</div>
                                	</div>

                                	<div class="span5">
                                		<label class="control-label">ឈ្មោះបង្ហាញជាភាសាខ្មែរ</label>
		  								<div class="controls">
		  									<input type="text" name="display_name_kh" value="{{$data->display_name_kh}}" data-required="1" class="span12 m-wrap"/>
		  								</div>
                                	</div>

                                	<div class="span5">
                                		<label class="control-label">ស្ថានភាព</label>
		  								<select class="span12 m-wrap" name="active">
		  									<option value="1" <?php if($data->active == 1){ echo "selected";} ?> >active</option>
		  									<option value="2" <?php if($data->active == 2){ echo "selected";} ?> >not active</option>
		  								</select>
                                	</div>
									<div class="span5">
                                		<label class="control-label"> ប្រភេទកម្ចី</label>
		  								<select class="span12 m-wrap" name="loan_type">
										    <option value="0">ទាំងពីរ( Individual /Group )</option>
											  <option value="1" <?php if($data->loan_type == 1){ echo "selected";} ?> >Individual</option>
		  									<option value="2" <?php if($data->loan_type == 2){ echo "selected";} ?> >Group</option>  
		  								</select>
                                	</div>	
                                	<div class="span5">
                                		<label class="control-label">បរិយាយ</label>
		  								<div class="controls">
		  									<input type="text" name="description" value="{{$data->description}}" data-required="1" class="span12 m-wrap"/>
		  								</div>
                                	</div>

									<div class="span12">
										<button type="submit" class="btn btn-success">កែប្រែ</button>
										<a href="{{ url('module_interest') }}" class="btn btn-danger">ត្រលប់</a>
									</div>

								</form>

			    			</div>

						</div>

                     	<!-- /block -->

		    		</div>

                     <!-- /validation -->

                </div>

            </div>	
@stop()