<div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                    </a>
                    <a class="brand" href="{{ URL::to('/home')}}"><img src="{{url('images/logo-no-bg.png')}}" width="50" style="margin-top: -10px;padding: 5px;"></a>
                    <div class="nav-collapse collapse" style="margin-top: 10px;">
                        <ul class="nav pull-right">
                            <li><a>រូបីយ​ប័ណ្ណ <b>US</b></a></li>
                            <li class="dropdown">
                                <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-user"></i>@if(Auth::user()->name_kh) {{Auth::user()->name_kh}}@else {{Auth::user()->username}} @endif <i class="caret"></i>

                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a tabindex="-1" href="{{ url('users/profile') }}"><i class="icon-list"></i> ព័ត៌មានអ្នកប្រើប្រាស់</a>
                                    </li>
                                    <li>
                                        <a tabindex="-1" href="{{ url('change_mypassword_user/change') }}"><i class="icon-wrench"></i> ប្តូរលេខកូដសំងាត់</a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a tabindex="-1" href="{{ url('logout') }}"><i class="icon-share"></i> ចាក់ចេញ</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        <ul class="nav">

                                <li  class="{{ Request::segment(1) == '/' ? 'active' : null }}">
                                        <a href="{{ URL::to('/') }}"><b>ទំព័រដំបូង</b></a>
                                </li>
                                 <li class="dropdown {{Request::segment(1) == 'accounts'  ? 'active' : null }}" >
                                    <a href="#" data-toggle="dropdown" class="dropdown-toggle"><b>ការគ្រប់គ្រងអតិថិជន</b> <b class="caret"></b></a>
                                        <ul class="dropdown-menu">
                                            <li class="{{ Request::segment(2) == 'accounts' ? 'active' : null }}">
                                                <a href="{{ url('accounts') }}">ការគ្រប់គ្រងអតិថិជន</a>
                                            </li> 
                                                <li class="{{ Request::segment(1) == 'account_type' ? 'active' : null }}">
                                                <a href="{{ url('account_type') }}">ការគ្រប់គ្រងប្រភេទអតិថិជន</a>
                                            </li>
                                            </li> 
                                                <li class="{{ Request::segment(2) == 'all_sub_client' ? 'active' : null }}">
                                                <a href="{{ url('accounts/all_sub_client') }}">ការគ្រប់គ្រងអ្នកធានា</a>
                                            </li>
                                        </ul>
                                </li>
                                <!-- <li  class="{{ Request::segment(1) == 'products' ? 'active' : null }}">
                                        <a href="{{ URL::to('products') }}"><b>ផលិតផល</b></a>
                                </li> -->
                                <li  class="{{ Request::segment(1) == 'credit_sales_purchas_order' ? 'active' : null }}">
                                        <a href="{{ URL::to('credit_sales_purchas_order') }}"><b>ការ​បញ្ជា​ទិញ</b></a>
                                </li>

                                <li class="dropdown {{Request::segment(1) == 'products'  ? 'active' : null }}" >
                                    <a href="#" data-toggle="dropdown" class="dropdown-toggle"><b>ផលិតផល</b> <b class="caret"></b></a>
                                        <ul class="dropdown-menu">
                                                <li  class="{{ Request::segment(1) == 'suppliers' ? 'active' : null }}">
                                                        <a href="{{ URL::to('suppliers') }}"><b>អ្នកផ្គត់ផ្គង់</b></a>
                                                </li> 
                                                <li class="{{ Request::segment(2) == 'products' ? 'active' : null }}">
                                                    <a href="{{ url('products/products') }}">កាគ្រប់គ្រង់ផលិតផល</a>
                                                </li> 
                                                 <li class="{{ Request::segment(2) == 'brands' ? 'active' : null }}">
                                                    <a href="{{ url('products/brands') }}">ម៉ាកផលិតផល</a>
                                                </li> 
                                                <li class="{{ Request::segment(2) == 'categorys' ? 'active' : null }}">
                                                    <a href="{{ url('products/categorys') }}">ប្រភេទផលិតផល</a>
                                                </li>     
                                        </ul>
                                </li>
                                
                                <li  class="{{ Request::segment(1) == 'credit_sales' ? 'active' : null }}">
                                        <a href="{{ URL::to('credit_sales') }}?redirect=credit_sales&sales=0"><b>ការទិញបង់រំលស់</b></a>
                                </li>
                               
                                <li  class="{{ Request::segment(1) == 'aprove_credit_sales' ? 'active' : null }}">
                                        <a href="{{ URL::to('aprove_credit_sales') }}"><b>ការអនុម័តឥណទាន</b></a>
                                </li>

                                <li  class="{{ Request::segment(1) == 'comment_finish' ? 'active' : null }}">
                                        <a href="{{ URL::to('comment_finish') }}"><b>មតិបញ្ចប់ឥណទាន</b></a>
                                </li>

                                <li class="dropdown {{Request::segment(1) == 'given_products'  ? 'active' : null }}" >
                                    <a href="#" data-toggle="dropdown" class="dropdown-toggle"><b>ការបើកផលិតផល</b> <b class="caret"></b></a>
                                        <ul class="dropdown-menu">
                                                <li class="{{ Request::segment(2) == 'given_products' ? 'active' : null }}">
                                                    <a href="{{ url('given_products') }}">ការបើកផលិតផលអោយបុគ្គលិក </a>
                                                </li> 
                                                <li class="{{ Request::segment(2) == 'given_products/client' ? 'active' : null }}">
                                                    <a href="{{ url('given_products/client') }}">ការបើកផលិតផលអោយអតិថិជន </a>
                                                </li> 
                                        </ul>
                                </li>

                                @if(Auth::user()->groups->first()->hasPermission(['developer']))
                                <li class="{{ Request::segment(1) == 'payment_credit_back'   ? 'active' : null }}">
                                    <a href="{{URL::to('payment_credit_back')}}"><b>កាលវិភាគនៃការសងប្រាក់របស់អតិថិជន</b></a> <!--Payment Back-->
                                </li>
                                 @endif

                                @if(Auth::user()->groups->first()->hasPermission(['developer']))
                                <li class="dropdown {{Request::segment(1) == 'report'  ? 'active' : null }}" >
                                    <a href="#" data-toggle="dropdown" class="dropdown-toggle"><b>របាយការណ៍</b> <b class="caret"></b></a>
                                        <ul class="dropdown-menu menu-scroll">
                                            <li class="{{ Request::segment(2) == 'item' ? 'active' : null }}">
                                                <a href="{{ url('report/item/report_item_list') }}"> របាយការណ៍សន្និធិ </a>
                                            </li>
                                            <li class="{{ Request::segment(2) == 'clients' ? 'active' : null }}">
                                                <a href="{{ url('report/clients/report_client_list') }}"> របាយការណ៍លម្អិតពីអតិថិជន </a>
                                            </li>
                                            <li class="{{ Request::segment(3) == 'report_sale_by_credit' ? 'active' : null }}">
                                                <a href="{{ url('report/sales/report_sale_by_credit') }}"> របាយការណ៍លម្អិតពីការលក់ដោយឥណទាន </a>
                                            </li>
                                            <li class="{{ Request::segment(3) == 'report_direct_payment' ? 'active' : null }}">
                                                <a href="{{ url('report/sales/report_direct_payment') }}"> របាយការណ៍លម្អិតពីការលក់ដោយរការទូទាត់ដោយផ្ទាល់  </a>
                                            </li>
                                            <li class="{{ Request::segment(3) == 'report_payment_back' ? 'active' : null }}">
                                                <a href="{{ url('report/payment_back/report_payment_back') }}"> របាយការណ៍លម្អិតពីសង់ប្រាក់ត្រលប់  </a>
                                            </li>
                                            <li class="{{ Request::segment(3) == 'report/payment_back/report_summery_payment_back' ? 'active' : null }}">
                                                <a href="{{ url('report/payment_back/report_summery_payment_back') }}"> របាយការណ៍សរុបពីសង់ប្រាក់ត្រលប់  </a>
                                            </li>
                                            <li class="{{ Request::segment(2) == 'report_schedules' ? 'active' : null }}">
                                                <a href="{{ url('report/report_schedules') }}">របាយការណ៍លម្អិតពីកាលវិភាគសងប្រាក់ </a>
                                            </li>
                                            <li class="{{ Request::segment(2) == 'report_repayment_collect' ? 'active' : null }}">
                                                <a href="{{ url('report/report_repayment_collect') }}">របាយការណ៍លម្អិតពីការប្រមូលសាច់ប្រាក់ </a>
                                            </li>
                                            <li class="{{ Request::segment(2) == 'report_sale' ? 'active' : null }}">
                                                <a href="{{ url('report/report_sale') }}">របាយការណ៍លម្អិតពីការទិញជាសាច់ប្រាក់</a>
                                            </li> 
                                            <li class="{{ Request::segment(2) == 'report_sale_by_credit' ? 'active' : null }}">
                                                <a href="{{ url('report/report_sale_by_credit') }}">របាយការណ៍លម្អិតពីការទិញបង់រំលស់</a>
                                            </li>
                                            <li class="{{ Request::segment(2) == 'report_receiving_product' ? 'active' : null }}">
                                                <a href="{{ url('report/report_receiving_product') }}">របាយការណ៍លម្អិតពីការទទួលទំនិញ </a>
                                            </li> 
                                            <li class="{{ Request::segment(2) == 'report_po_product' ? 'active' : null }}">
                                                <a href="{{ url('report/report_po_product') }}">របាយការណ៍លម្អិតពីការបញ្ជារទិញ </a>
                                            </li>
                                            
                                        </ul>
                                        
                                        
                                </li>
                                 @endif
                                <li class="dropdown {{Request::segment(1) == 'history_log' || Request::segment(1) == 'users' || Request::segment(1) == 'users-permission' || Request::segment(1) == 'interest_rate'||Request::segment(1) == 'location' || Request::segment(1) == 'public_holiday'  ? 'active' : null }}" >
                                    <a href="#" data-toggle="dropdown" class="dropdown-toggle"><b>ការកំណត់</b> <b class="caret"></b></a>
                                        <ul class="dropdown-menu">
                                            @if(Auth::user()->groups->first()->hasPermission(['create-user-groups']))
                                                @if(Auth::user()->groups->first()->hasPermission(['list-user-groups']))
                                                    <li class="dropdown-submenu">
                                                        <a href="{{ url('#') }}"> កំណត់អ្នកប្រើប្រាស់</a>
                                                        <ul class="dropdown-menu">
                                                            <li>
                                                                <a tabindex="-1" href="{{ URL::to('users')}}">ការគ្រប់គ្រងអ្នកប្រើប្រាស់</a>
                                                            </li>
                                                            <li>
                                                                <a tabindex="-1" href="{{ URL::to('users-groups')}}">ការគ្រប់គ្រងសិទ្ធិអ្នកប្រើប្រាស់</a>
                                                            </li>
                                                            @if(Auth::user()->groups->first()->hasPermission(['developer']))   
                                                                <li>
                                                                    <a tabindex="-1" href="{{ URL::to('users-permission')}}">ការគ្រប់គ្រងសិទ្ធិ</a>
                                                                </li>
                                                            @endif
                                                        </ul>
                                                    </li>
                                                @endif
                                                <li class="{{ Request::segment(1) == 'branch' ? 'active' : null }}">
                                                    <a href="{{ url('branch') }}">ការកំណត់សាខា</a>
                                                </li>
                                                <li class="{{ Request::segment(1) == 'interest_rate' ? 'active' : null }}">
                                                    <a href="{{ url('interest_rate') }}">ការកំណត់អត្រាការប្រាក់</a>
                                                </li>
                                                <li class="{{ Request::segment(1) == 'new_interest_rate' ? 'active' : null || Request::segment(1) == 'service_interest_rate' ? 'active' : null }}">
                                                    <a href="{{ url('new_interest_rate') }}">ការកំណត់អត្រាការប្រាក់ថ្មី</a>
                                                </li>
                                                <li class="{{ Request::segment(1) == 'public_holiday' ? 'active' : null }}">
                                                    <a href="{{ url('public_holiday') }}">ការកំណត់ថ្ងៃឈប់សំរាក</a>
                                                </li>
                                                <li class="{{ Request::segment(1) == 'exchange_rate' ? 'active' : null }}">
                                                    <a href="{{ url('exchange_rate') }}">កំណត់ការប្តូរប្រាក់</a>
                                                </li>
                                                <li class="{{ Request::segment(1) == 'penaty_date' ? 'active' : null }}">
                                                    <a href="{{ url('penaty_date') }}">ការកំណត់ពិន័យ</a>
                                                </li>
                                                <li class="{{ Request::segment(1) == 'list_position' ? 'active' : null }}">
                                                    <a href="{{ url('list_position') }}">ការកំណត់មុខតំណែង</a>
                                                </li>
                                                <li class="{{ Request::segment(1) == 'module_interest' ? 'active' : null }}">
                                                    <a href="{{ url('module_interest') }}">Module Interest</a>
                                                </li>
                                                <li class="{{ Request::segment(1) == 'history_log' ? 'active' : null }}">
                                                    <a href="{{ url('history_log') }}">សកម្មភាពប្រើប្រាសរបស់អ្នកប្រើប្រាស់</a>
                                                </li>
                                                 <li class="{{ Request::segment(1) == 'currency' ? 'active' : null }}">
                                                    <a href="{{url('currency')}}">ការកំណត់រូបីយប័ណ្ណ</a>
                                                </li>
                                                <li class="{{ Request::segment(1) == 'rate_percent' ? 'active' : null }}">
                                                    <a href="{{url('rate_percent')}}">ការកំណត់ភាគរយនៃការបង់ប្រាក់</a>
                                                </li>
                                                  <li class="{{ Request::segment(1) == 'configure' ? 'active' : null }}">
                                                    <a href="{{url('configure')}}">ការកំណត់</a>
                                                </li>
                                            @endif
                                    </ul>
                            </li>
                                    
                        </ul>
                    </div>
                    <!--/.nav-collapse -->
                </div>
            </div>
          
    </div>