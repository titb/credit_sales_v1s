@extends('credit_sale.layout.master')
@section('contend')
<link rel="stylesheet" href="{{ URL::to('assets/jquery-ui.css')}}">
<script src="{{ URL::to('assets/jquery-1.12.4.js')}}"></script>
<script src="{{ URL::to('assets/jquery-ui.js')}}"></script>

<meta name="csrf-token" content="{{ csrf_token() }}">
<script src="{{ URL::to('assets/accounting.min.js')}}"></script>
<!-- container-fluid  -->
<div class="container-fluid content_top_mar" >
        <!-- row-fluid -->
            <div class="row-fluid">
                         <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                       <a href="{{url('credit_sales_purchas_order')}}" class="btn btn-sm btn-danger pull-right" style="margin-left:10px; margin-right:10px"><i class="icon-arrow-left icon-white"></i> New Purchase Order </a>    
                                        

                            </div>
                            <!-- block-content collapse in     -->
                            <div class="block-content collapse in">
                               @include('errors.error')	
                               <!-- <div class="up_all"> -->
									<div class="header1">
										<div class="title">
											<h3>TITB Receiving Invoice </h3>
										</div>
									</div>
									<div class="border-bottom"></div>
									<div class="header2">
										<div class="row">
											<div class="span4">
												<p>Staff: <span class="staff">Afiyah</span></p>
												<p>Address: <span class="address">Chroy Chongvar</span></p>
											</div>
											<div class="span4">
												<p>P: <span class="phone1">Phone Number</span></p>
												<p>F: <span class="fax1">Fax Number</span></p>
											</div>
											<div class="span4">
												<p>Email : <span class="email1">Afiyah@titb.biz</span></p>
												<p>Website : <span class="website">AD Plus Group</span></p>
											</div>
										</div>
									</div>
									<div class="header3">
										<div class="row">
											<div class="span4">
												<p><b>Company Name:</b>	<span class="company_name"></span></p>
												<p><b>Address:</b> <span class="address1"></span></p>
											</div>
											<div class="span4">
												<p><b>Phone:</b> <span class="phone"></span></p>
												<p><b>Email :</b> <span class="email"></span></p>
											</div>
											<div class="span4">
												<p><b>Invoice #:</b><span class="invoice_num"> </span></p>
												<p><b>Date Receive :</b><span class="invoice_date"> </span></p>
											</div>
										</div>
									</div>
									<div class="table_show">
										<table class="table table-bordered table-striped">
											<thead style="background-color: #f4433642;">
												<tr>
													<th>Item</th>
													<th>Description</th>
													<th>Receive Qty</th>
                                                    <th>Qty Free</th>
													<th>Cost Price</th>
                                                    <th>Discount</th>
													<th>Total</th>
												</tr>
											</thead>

											<tbody class="list_item" id="list_item">
												
											</tbody>
											<table class="table table-bordered table-striped" style="width: 40%; float: right; clear: right;">
												<tr>
													<td colspan="5" class="tfoot-right">Invoice Subtotal</td>
													<td><span class="sup_total_price"> </span></td>
												</tr>
                                                <tr>
													<td colspan="5" class="tfoot-right">Remain Price</td>
													<td><span class="remain_price"> </span></td>
												</tr>
												<tr>
													<td colspan="5" class="tfoot-right">Discount</td>
													<td><span class="discount_percent"> </span></td>
												</tr>
                                                <tr>
													<td colspan="5" class="tfoot-right">Receive Qty</td>
													<td><span class="qty_receiving"> </span></td>
												</tr>
												<tr>
													<td colspan="5" class="tfoot-right">Tax Rate</td>
													<td><span class="tax"></span></td>
												</tr>
												
												<tr>
													<td colspan="5" class="tfoot-right">Deposit Received</td>
													<td><span class="deposit_price"> </span></td>
												</tr>
												<tr>
													<td colspan="5" class="tfoot-right">TOTAL</td>
													<td><span class="dute_total_price"> </span></td>
												</tr>
											</table>
										</table>

									</div>
							   <!-- </div> -->
                            </div>
                        </div>
            </div>
</div>

<script type="text/javascript">

	$(document).ready(function(){
                    $.ajaxSetup({
							headers: {
								'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
							}
						})
						var c_id ="{{$po_id}}";
						
						var url_edit = "{{route('credit_sales_purchas_order')}}/"+c_id+"/invoice_json";

						// alert(url_edit);
						var client;
						var forData = {};
							var out = "";
							$.ajax({
									type: "GET",
									url: url_edit, 
									dataType: "json",
									data: forData,
									success: function(result ,xhr){
										console.log(result);
                                        var myJSON = JSON.stringify(result);
										if(result.cs_purchase_order.suppliers.company_name){
											$('.company_name').html(result.cs_purchase_order.suppliers.company_name);
										}
										if(result.cs_purchase_order.suppliers.address1){
											$('.address1').html(result.cs_purchase_order.suppliers.address1);
										}
										if(result.cs_purchase_order.suppliers.phone){
											$('.phone').html(result.cs_purchase_order.suppliers.phone);
										}
										if(result.cs_purchase_order.suppliers.email){
											$('.email').html(result.cs_purchase_order.suppliers.email);
										}
										if(result.cs_purchase_order.invoice_num){
											$('.invoice_num').html(result.cs_purchase_order.invoice_num);
										}
										if(result.cs_purchase_order.order_date){
											$('.invoice_date').html(result.cs_purchase_order.order_date);
										}
										
   
										// Get Item
										var text ="";
										if(result.cs_purchase_order.cs_purchase_order_item){
											$.each(result.cs_purchase_order.cs_purchase_order_item ,function(k,itm){
												text += "<tr>";
												text += "<td>"+itm.item.item_bacode+"</td>";
												text += "<td>"+itm.item.name+"</td>";
												text += "<td>"+itm.qty_receiving+"</td>";
                                                text += "<td>"+itm.qty_free+"</td>";
												text += "<td>"+accounting.formatMoney(itm.cost_price)+"</td>";
                                                text += "<td>"+itm.discount+"</td>";
												text += "<td>"+accounting.formatMoney(itm.total)+"</td>";
												text += "</tr>";
											});
											$("#list_item").html(text);
										}
                                        

										// Get Purchase Order
										if(result.cs_purchase_order.sup_total_price){
											$('.sup_total_price').text(accounting.formatMoney(result.cs_purchase_order.sup_total_price));
										}
                                        if(result.cs_purchase_order.remain_price){
											$('.remain_price').html(result.cs_purchase_order.remain_price);
										}
										if(result.cs_purchase_order.discount_percent){
											$('.discount_percent').html(result.cs_purchase_order.discount_percent);
										}
                                        if(result.cs_purchase_order.qty_receiving){
											$('.qty_receiving').html(result.cs_purchase_order.qty_receiving);
										}
										
										if(result.cs_purchase_order.cs_purchase_order_item){
											$.each(result.cs_purchase_order.cs_purchase_order_item, function(k1,itm1){
												$('.tax').html(itm1.tax);
											});
										}

										if(result.cs_purchase_order.deposit_price){
											$('.deposit_price').text(accounting.formatMoney(result.cs_purchase_order.deposit_price));
										}
										if(result.cs_purchase_order.dute_total_price){
											$('.dute_total_price').text(accounting.formatMoney(result.cs_purchase_order.dute_total_price));
										}

                                        // $(".header").html(myJSON);
                                    },
									error: function (result ) {
										console.log(result.stutus);
									}

							});
						      
				
	});
</script>
@endsection
