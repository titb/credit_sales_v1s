@extends('credit_sale.layout.master')
@section('contend')
<link rel="stylesheet" href="{{ URL::to('assets/jquery-ui.css')}}">
<script src="{{ URL::to('assets/jquery-1.12.4.js')}}"></script>
<script src="{{ URL::to('assets/jquery-ui.js')}}"></script>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="{{ URL::to('bootstrap/js/bootstrap.min.css') }}" rel="stylesheet" media="screen"> -->
<style>
    /* .sale_me :hover,.sale_me:focus, .sale_me:active, .sale_me.active {
        color: #fff !important;
        background-color: #04c !important;
        
    } */
    .table .total_foot td {
        border-top: 0px solid #ddd;
        padding: 5px;
        line-height: 14px;
        text-align: left;
        vertical-align: bottom;
    }
    .table .total_foot td p{
        margin: 0px;
    }
    h1.in_cart ,h2.in_cart ,h3.in_cart {
        line-height: 0px;
    }
    table.table.table_sadow {
        border: 2px solid #ddd;
       
    }
    
    @media only screen and (min-width: 1023px) {
        .table .total_foot td {
           min-width:100px;
        }
        table.table.table_sadow tbody {
            height: 223px;
        }
    }
    @media only screen and (max-width: 1024px) {
        .table .total_foot td {
           min-width:70px;
        }
        table.table.table_sadow tbody {
            height: 200px;
        }
    }
    /* width */
    .scroller_style1::-webkit-scrollbar {
            width: 3px;
        }

        /* Track */
        .scroller_style1::-webkit-scrollbar-track {
            background: #f1f1f1; 
        }
        
        /* Handle */
        .scroller_style1::-webkit-scrollbar-thumb {
            background: #888; 
        }

        /* Handle on hover */
        .scroller_style1::-webkit-scrollbar-thumb:hover {
            background: #555; 
        }
        table.table.table_sadow thead tr {
            display: block;
            position: relative;
        }
        table.table.table_sadow tbody {
            display: block;
            overflow: auto;
            width: 100%;
        }
        /* table.table.table_sadow th ,  table.table.table_sadow td {
            
        } */
        /* table.table.table_sadow thead tr th {
            width: 7%;  
           padding: 8px;
            line-height: 20px;
            text-align: left;

        }
        table.table.table_sadow td {
            vertical-align: middle;
            width: 10%;  
        } */

        table.table.table_sadow th:first-child {
            /* vertical-align: middle; */
            text-align: left;
            /* width: 50%;   */
        }
        table.table.table_sadow td:first-child {
            text-align: left;
            vertical-align: middle;
            width: 24%;  
        }
        table.table.table_sadow th {
            /* width:10%; */
            text-align: center;
        }
        table.table.table_sadow td {
            width:10%;
            text-align: center;
            vertical-align: middle;
           
        }
        table.table.table_sadow td p{
            margin:0px;
        }
        a.btn.btn-danger.btn-mini.remove_item {
            font-size: 8px !important;
            padding: 0 2px !important;
        }
        .form-horizontal .controls {
            margin-left: 100px !important;
        }
        .form-horizontal .control-label{
            width: 86px !important;
        }
        .control-group.customer {
            margin-bottom: 20px!important;
        }
        .form-horizontal .control-group{
            margin-bottom: 10px;
        }
        .table th, .table td {
    padding: 8px;
    line-height: 20px;
    text-align: center;
    vertical-align: middle;
    border-top: 1px solid #ddd;
}
/* .table-fixed thead {
  width: 97%;
}
.table-fixed tbody {
  height: 230px;
  overflow-y: auto;
  width: 100%;
}
.table-fixed thead, .table-fixed tbody, .table-fixed tr, .table-fixed td, .table-fixed th {
  display: block;
}
.table-fixed tbody td, .table-fixed thead > tr> th {
  float: left;
  border-bottom-width: 0;
}      */
</style>

<!-- container-fluid  -->
<div class="container-fluid content_top_mar" >
        <!-- row-fluid -->
            <div class="row-fluid">
                         <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                        <button  class="btn sale_me purchas_order" id="purchas_order" value="purchas_order">ការ​បញ្ជា​ទិញ</button> 
                                        <button  class="btn sale_me receiving" id="receiving" value="receiving" >ត្រលប់មកវិញ</button>
                                        <button   class="btn pull-right full_screen" id="full_screen"  style="margin-left:5px; margin-right:5px"><i class="icon-fullscreen "></i></button>			
                                        <a href="{{url('post_cancel_all_cart_item_purchas_order')}}" class="btn btn-sm btn-danger pull-right" style="margin-left:10px; margin-right:10px"><i class="icon-remove icon-white"></i> បោះបង់ </a>    
                                        
                                    <div class="pull-right">
                                        <p> អត្រាប្តូរប្រាក់ : <b style="color:blue"> 1$ </b> = <b style="color:red"> 4100 ៛​ </b></p>   
                                    </div>	
                                   
                            </div>
                            <!-- block-content collapse in     -->
                            <div class="block-content collapse in">
                               @include('errors.error')	
                             	
                                <!-- span6 up -->
                                    <div class="span6" style="margin-left:0px;">
                                        <div class="span12">
                                                <!-- <form class="form-horizontal"> -->
                                                <div class="block-content collapse in" style="margin-top:0px;">
                                                    <fieldset>
                                                        <!-- <span class="required">*</span> -->
                                                            <div id="sale_by_cash">
                                                                <!-- Search Item add to cart -->
                                                                    <div class="control-group">
                                                                        <div class="controls">
                                                                                <input name="search_item" type="text" id="search_item"  data-required="1" class="span11 m-wrap" placeholder="បញ្ចុលឈ្មោះ , លេខបាកូដទំនិញ ឬ លេខកូដទំនិញ" autocomplete="off"/>     
                                                                                <button class="btn btn-success add_new_item" style="margin-top: -10px;"><i class="icon-plus icon-white"></i></button>
                                                                        </div>
                                                                        <!-- <legend></legend> -->
                                                                    </div>
                                                                <!-- end  Search Item add to cart -->
                                                                <!-- Item Table -->
                                                                    <div class="control-group">
                                                                        <!-- <table class="table table_sadow"> -->
                                                                        <table class="table table-fixed">
                                                                            <thead style="background-color: #489ee7; color: #fff;">
                                                                                <tr id="po_head">
                                                                                  
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody class="scroller_style1">
                                                                                
                                                                            </tbody>
                                                                            </table>
                                                                        </div>
                                                                 <!--end Item Table -->
                                                                 <!-- total Item     -->
                                                                
    
                                                                        <div class="control-group" style=" border-top: 1px solid #ddd;padding-top: 10px;">
                                                                            <table class="table"> 
                                                                                <tfoot class="total_foot">
                                                                                    <tr>
                                                                                        <td><b>តម្លៃ :  </b></td>
                                                                                      <td> <b class="totla_in_cart in_cart">$0.00</b></td>
                                                                                       <td><b>សរុប : </b></td>
                                                                                     <td><b style="color:red" class="total_sub_total">$0.00</b></td>
                                                                                   </tr>
                                                                                 <tr>
                                                                                   <td><b>បញ្ចុះតម្លៃ (%) :  </b></td>
                                                                                        <td> <input name="dis_all_in_add_cart" type="text" class="span9 m-wrap dis_all_in_add_cart" value="0" style="margin-bottom: 0px;" id="dis_all_in_add_cart">  </td>
                                                                                        <td colspan="2"></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td><b>ពន្ធ : </b></td>
                                                                                        <td> <b class="total_tax_in_cart in_cart">$0.00</b></td>
                                                                                        <td><b>ប្រាក់ត្រូវបង់ : </b></td>
                                                                                        <td><b style="color:red" class="balance_due_in_cart in_cart">$0.00</b></td>
                                                                                    </tr>
                                                                                    <input type="hidden" name="balance_due_in_cart_kh" class="balance_due_in_cart_kh1" >

                                                                                </tfoot>
                                                                            </table>
                                                                        </div>
                                                                 <!-- End total Item -->
                                                                 <!-- buttom Pay -->
                                                                 <!-- <div class="control-group"> -->
                                                                    <!-- <button type="reset" class="btn btn-danger"><i class="icon-remove icon-white"></i></button> -->
                                                                  
                                                                     <!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Pay</button> -->
                                                                     
                                                                    <!-- <button type="submit" class="btn btn-info">Pay</button> -->
                                                                <!-- </div>  -->
                                                                 <!-- End buttom Pay -->
                                                            </div>
                                                        <!-- End Sale By Cash -->
                                                       
                                                    </fieldset>
                                                </div>  
                                                <!-- </form> -->

                                            </div>
                                    </div>

                                <!-- end span6 up  -->
                                <!-- span6 down -->
                                    <div class="span6">
                                        <div class="span12">
                                            <div class="block-content collapse in" style="margin-top:0px;"> 
                                                <fieldset class="form-horizontal">
                                                    <!-- <legend>Form Horizontal</legend> -->
                                                    <div class="control-group customer">
                                                            <label class="span4 m-wrap" style="line-height: 30px; margin-bottom: 0px;">អ្នកផ្គត់ផ្គង់ : <b id="supplier_name"></b></label>
                                                            <input type="hidden" name="supplier_id" id="supplier_id" value="0"/>
                                                            <div class="controls">
                                                                    <input name="search_supplier" id="search_supplier" type="text"  data-required="1" class="span8 m-wrap" placeholder="បញ្ចូលឈ្មោះអ្នកផ្គត់ផ្គង់ ឬ​ លេខកូដអ្នកផ្គត់ផ្គង់" autocomplete="off"/>     
                                                                    <button class="btn btn-success add_new_supplier" ><i class="icon-plus icon-white"></i></button>
                                                            </div> 
                                                    </div>

                                                </fieldset>
                                                <fieldset class="form-horizontal">
                                                    <div class="control-group">
                                                        <center>
                                                            <button class="btn pay_type cash_pay " id="" value="cash">សាច់ប្រាក់</button>
                                                            <!-- <button class="btn pay_type credit_card_pay" value="credit_card">Credit Card</button>
                                                            <button class="btn pay_type debit_card_pay" value="debit_card">Debit Card</button> -->
                                                            <!-- <button class="btn pay_type check_pay" value="check">Check</button> -->
                                                            <!-- <button class="btn pay_type gift_card_pay" value="gift_card">Gift Card</button> -->
                                                            <!-- <button class="btn pay_type coupon_pay" value="coupon">coupon</button> -->
                                                            <button class="btn pay_type store_account" value="store_account">Account Store</button>
                                                        </center>
                                                    </div>
                                                    <div class="control-group">
                                                            <table class="table"> 
                                                                <tfoot class="total_foot">
                                                                    <tr>
                                                                        <td><b>ប្រាក់ត្រូវបង់ជា ដុល្លា :  </b></td>
                                                                        <td><h3 style="color:red" class="balance_due_in_cart1 in_cart">0.00 </h3></td>
                                                                        <td><b>ប្រាក់ត្រូវបង់ជា រៀល : </b></td>
                                                                        <td><h3 style="color:red" class="balance_due_in_cart_kh in_cart">0.00 </h3></td>
                                                                    </tr>
                                                                    
                                                                </tfoot>
                                                               
                                                            </table>
                                                            
                                                        <!-- </div> -->
                                                    </div>
                                                    <div class="control-group">
                                                                <label class="control-label" for="focusedInput" style="text-align: left;">ប្រាក់ត្រូវទូទាត់</label>
                                                                <div class="controls">
                                                                    <input class="span7 m-wrap get_payment_num number-format" id="focusedInput" type="text" value="" placeholder="0.00"  autocomplete="off">
                                                                    <button class="btn get_pay get_pay_us" value="us">ដុល្លា</button>
                                                                    <button class="btn get_pay get_pay_kh" value="kh">រៀល</button>
                                                                    <button class="btn btn-primary add_pay_me pull-right" >ទូទាត់</button>
                                                                </div>
                                                    </div>

                                                            <div class="control-group">
                                                                    <table class="table"> 
                                                                        <tbody class="add_pay_ment">
                                                                        <tbody>
                                                                    </table>
                                                                    <input type="hidden" name="has_pay_tran" id="has_pay_tran">
                                                                
                                                            </div>
                                                            <div class="control-group">
                                                                <label class="control-label" for="focusedInput" style="text-align: left;">ប្រាក់កក់</label>
                                                                <div class="controls">
                                                                    <input name="total_deposit_fixed_in_cart" type="text" class="span10 m-wrap total_deposit_fixed_in_cart number-format" style="margin-bottom: 0px;" placeholder="0.00">
                                                                </div>
                                                            </div> 
                                                            <div class="control-group">
                                                                    <label class="control-label" for="focusedInput" style="text-align: left; width: 100px !important;" id="order_date">ថ្ងៃកម្មង់  </label>
                                                                    
                                                                    <div class="controls">
                                                                        <input type="text" id="date_for_receiving" class="input-xlarge datepicker span10 m-wrap date_for_receiving" name="date_for_receiving" data-required="1" autocomplete="off">
                                                                    </div>
                                                            </div> 
                                                            <div class="control-group">
                                                                    <label class="control-label" for="focusedInput" style="text-align: left;  width: 100px !important;">បុគ្គលិក </label>
                                                                    <div class="controls" style="margin-left: 40px !important;">
                                                                    <?php $staff = App\User::all(); ?>
                                                                        <select name="staff_id" class="span9 m-wrap staff_id" id="staff_id">
                                                                                <option value="">-- ជ្រើសរើស  --</option>
                                                                                @foreach($staff as $st)
                                                                                    <option value="{{$st->id}}" @if($sale_type == "purchas_order")@if(Auth::user()->id == $st->id) selected @endif   @endif>{{$st->name_kh}}</option>
                                                                                @endforeach
                                                                            
                                                                        </select>
                                                                    </div>
                                                            </div>  
                                                            <!-- <div class="control-group">
                                                                <label class="control-label" for="focusedInput" style="text-align: left;" >Payback KH</label>
                                                                <div class="controls">
                                                                    <input class="span11 m-wrap payback_kh" id="focusedInput" type="text" value="0">
                                                                </div>
                                                            </div> -->
                                             <!--End If Not Sale Type Not Credit Sale  -->    
                                                    <div class="control-group">
                                                        <label class="control-label" for="focusedInput" style="text-align: left;">មតិយោបល់</label>
                                                        <div class="controls">
                                                        <textarea class="span10 m-wrap sale_comment"  placeholder="បញ្ចូលមតិយោបល់ ..."></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="control-group">
                                                        <center>
                                                            <button class="btn btn-primary completed_payment">បញ្ចូល</button>
                                                        </center>
                                                    </div>
                                                </fieldset>

                                            </div>
                                        </div>
                                    </div>    
                                <!-- end span6 down  -->
                            </div>  
                            <!-- end block-content collapse in     -->
                         </div>    
                        <!-- end block -->
                   
            </div>
            <!-- end row-fluid -->
</div>

<!-- end container-fluid  -->
<meta name="csrf-token" content="{{ csrf_token() }}">
<script src="{{ URL::to('assets/accounting.min.js')}}"></script>
<script type="text/javascript">
// item search

var default_currency = 1 ;
var currency_us = 1;
var currency_kh = 2 ;
var get_fix_right = 2 ;
var config_cur_1us_to_kh = 4100;
if(default_currency == currency_us){
    var curren = "$";
}else if(default_currency == currency_kh){
    var curren = "៛";
}

function round_up_kh(num){
    var num2 ,num3 ,num4;
    num2 = num /100;
    num3 = Math.ceil(num2);
    num4 = num3 *100;
    return num4;
}
 
function money_exchange_to_kh(bal_due){
    var to_kh_1 = bal_due *  config_cur_1us_to_kh;
     var to_kh = round_up_kh(to_kh_1); 
     return to_kh;
}
function money_exchange_to_us(bal_due){
    var to_us1 = bal_due / config_cur_1us_to_kh
    var to_us_fixed = to_us1.toFixed(get_fix_right);
    var to_us = to_us_fixed; 
     return to_us;
}
// $(".currency_defult").text(curren);
function input_type_currency(){

}
$('.number-format').keyup(function(event) {

	  // skip for arrow keys
	  if(event.which >= 37 && event.which <= 40){
	   event.preventDefault();
	  }

	  $(this).val(function(index, value) {
	      value = value.replace(/,/g,'');
	      return numberWithCommas(value);
	  });
	});

	function numberWithCommas(x) {
	    var parts = x.toString().split(".");
	    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	    return parts.join(".");
	} 
    $(window).load(function(){
        get_all_session();
        get_data_cart_item();
    });

// click buttom type of sale 
$(".sale_me").click(function(){
            $(".sale_me").removeClass("btn-primary sale_type_active"); 
            $(this).addClass("btn-primary sale_type_active");
           var sale_type = $(this).val();

            var formdata = {
                sale_type_po: sale_type 
            }; 
                 
        add_all_session(formdata);
       
      
});
// click buttom pay type of sale 
$(".pay_type").click(function(){
            $(".pay_type").removeClass("btn-primary pay_type_active"); 
            $(this).addClass("btn-primary pay_type_active"); 
           var pay_type = $(this).val();

            var formdata = {
                pay_type: pay_type 
            }; 
        add_all_session(formdata);
        
});

// click buttom pay currency type of sale 
$(".get_pay").click(function(){
            $(".get_pay").removeClass("btn-success pay_active"); 
            $(this).addClass("btn-success pay_active"); 
           var currency_type = $(this).val();

            var formdata = {
                currency_type: currency_type 
            }; 
   // total_function_foot(formdata);
        add_all_session(formdata);
        var get_payment_data = $(".balance_due_in_cart").text();
        var has_pay_tran = $("#has_pay_tran").val();
        var get_data ={
            has_pay_tran: has_pay_tran,
            currency_type: $(this).val(),
            get_payment_data: get_payment_data,
            
        };
        get_payment_total(get_data)
});

    // complete and add payment 
    $(".add_pay_me").click(function(event){
        var pay_active, pay_back_kh, pay_back_us, change_to_kh, change_to_us,
        sale_type_active, pay_type_active, get, get_payment, get_payment1, 
        ba_due_payment, ba_due_payment_kh, ba_due_payment_kh1 ,total_pay_back, total_pay_due;
            
            pay_active  = $(".pay_active").val();
            sale_type_active  = $(".sale_type_active").val();
            pay_type_active = $(".pay_type_active").val();
            //get = "GET PAY:  "+pay_active +"  == PAY TYPE: "+ pay_type_active+" SALE TYPE:  "+sale_type_active; 
            get_payment = accounting.unformat($(".get_payment_num").val());
            ba_due_payment = accounting.unformat($(".balance_due_in_cart1").text());
            ba_due_payment_kh = accounting.unformat($(".balance_due_in_cart_kh").text());

            var formdata = {
                    payment_num: get_payment,
                    pay_type_active: pay_type_active,
                    pay_active: pay_active,

                }; 
            event.preventDefault();
            
            add_all_session(formdata);   
            

    });


//
$("#search_item").autocomplete({

source : '{{ URL::route('search_item_ajax_search_po') }}',

minlenght :1,

autoFocuse : true,

select:function(e,ui){
    var formData = {
            item_id: ui.item.id,
            name: ui.item.value,
            sell_price: ui.item.sell_price
        };
    
        add_to_cart_item(formData);

        $("#search_item").val('');  
        return false;    
    }
});

// customer search 
$("#search_supplier").autocomplete({

source : '{{ URL::route('suppliers_get_ajax_serach') }}',

minlenght :1,

autoFocuse : true,

select:function(e,ui){
        // $('#loadid').val(ui.item.id);
       
        //$('#loan_form').submit(); 
        var formdata = {
            supplier_id: ui.item.id,
            supplier_name: ui.item.value,    
        }; 
        add_all_session(formdata);
        $("#search_supplier").val('');  
        return false;      
}
});



    $(".dis_all_in_add_cart").change(function(){
        var discount = $(this).val();

        var formdata = {
            discount_globle_po: discount 
            }; 
           // total_function_foot(formdata);
                add_all_session(formdata);
                total_function_foot(formdata);
            
    });

     $(".total_deposit_fixed_in_cart").change(function(){
        var deposit_price = accounting.unformat($(this).val());
    //   alert(deposit_price);
        var formdata = {
            deposit_price: deposit_price 
        }; 
           // total_function_foot(formdata);
                add_all_session(formdata);
    });

//   $('.total_deposit_fixed_in_cart').change(function(){
//     var deposit_price = $(this).val();

//         var formdata = {
//                 deposit_price: deposit_price 
//             }; 
      
//                 add_all_session(formdata);
//     });

    function clear_all_session(){
        $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                })
                event.preventDefault();
                var url_remove_sale = "{{route('post_cancel_all_purchas_order_ajax')}}";
                $.ajax({
                    type: "POST",
                    url: url_remove_sale,
                    success: function (data) {
                       // window.location.href ="credit_sales_purchas_order";
                       get_all_session();
                        get_data_cart_item();
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
    }
 
    function add_all_session(formdata){
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var url_now = "{{route('pos_add_all_session_purchas_order')}}";
            $.ajax({
                type: "POST",
                url: url_now,
                dataType: 'json',
                data: formdata,
                success: function(result){
                   console.log(result);
                    var dis = formdata.discount_globle_po;
                    var client = formdata.supplier_name;
                    var client_id = formdata.supplier_id;
                    if(formdata.discount_globle_po){
                        $(".dis_all_in_add_cart").val(dis);
                    }
                    if(formdata.supplier_name){
                        $("#supplier_name").text(client);
                        $("#supplier_id").val(client_id);
                    }
                    if(formdata.deposit_price){
                        $(".total_deposit_fixed_in_cart").val(accounting.formatMoney(formdata.deposit_price));
                    } 
                    if(formdata.sale_type_po){
                        clear_all_session();
                    }
                   
                    // if(result.deposit_price){
                    //     $('.total_deposit_fixed_in_cart').val(result.deposit_price);  
                    // }
                    // if(result.remain_price){
                    //     var gpn = $(".get_payment_num").val();
                    //     var gpn1 = accounting.unformat(gpn);
                    //     var gpm2 = gpn1 - result.remain_price; 
                    //     $('.get_payment_num').val(gpm2);  
                    // }
                    // if(result.date_for_receiving){
                    //     $('.date_for_receiving').val(result.date_for_receiving);  
                    // }
                        
                        if(result.sale_type_po == "receiving"){
                            $("#order_date").text("ថ្ងៃទទួល");
                        }else{
                            $("#order_date").text("ថ្ងៃកម្មង់");
                               
                        }
                        var num_p_us = 0;
                        var num_p_kh = 0;
                        var num_p_pay_type =$(".pay_active").val();
                        var num_p_pay_num = 0;
                        if(result.tran_cash_po){
                            var tran_text = "";
                            $.each(result.tran_cash_po ,function(i,field){
                                if(field.pay_active == "us"){
                                    num_p_us += Number(field.payment_num);
                                }else{
                                    num_p_kh +=  Number(money_exchange_to_us(field.payment_num)); 
                                }  
                                var url_pr = "{{ url('credit_sales_purchas_order') }}/"+i+"/remove_tran_cash";
                                tran_text += "<tr class='parent"+i+"'>";
                                //    tran_text += "<td><button class='btn btn-danger btn-mini remove_add_payment'value='"+i+"'><i class='icon-remove icon-white'></i></td>";
                                //    tran_text += "<td><button class='btn btn-danger btn-mini remove_add_payment'value='"+i+"'><i class='icon-remove icon-white'></i></td>";
                                tran_text += "<td><a href='#remove_transtation_pay' class='btn btn-danger btn-mini remove_add_payment' rel='"+i+"'><i class='icon-remove icon-white' style='margin-top: 3px;margin-right: -5px;'></i></a></td>";
                                tran_text += "<td><span class='add_payment'>"+field.payment_num+"</span></td>";
                                tran_text += "<td><span class='add_payment_curren'>"+field.pay_type_active+"</span></td>";
                                tran_text += "<td><span class='add_payment_type'>"+field.pay_active+"</span></td>";
                                tran_text += "</tr>";
                                num_p_pay_type = field.pay_active;
                                num_p_pay_num = field.payment_num;
                            });
                            $(".add_pay_ment").html(tran_text);
                        } 
                    //}
                    $("#has_pay_tran").val(num_p_us + num_p_kh);
                   var get_data ={
                            has_pay_tran: num_p_us + num_p_kh,
                            currency_type: $(".pay_active").val(),
                            get_payment_data: $(".balance_due_in_cart").text()
                        };
                        get_payment_total(get_data);
                    // if(formdata.sale_type == "sale_by_cash"){
                    //     $("#sale_by_cash").addClass("active");
                    // }else if(formdata.sale_type == "purchas_order"){
                    //     $("#purchas_order").addClass("active");
                    // }
                    
                  

                   
                },
                error: function(){} 	        
            });
    }

// change total function
    function total_function_foot(formdata){
        
        var deposit_setting = 0.3;
        var sale_type_active = $(".sale_type_active").val();
        var sub_total = accounting.unformat($(".total_sub_total").text());
        var dis = formdata.discount_globle_po;
        var tax_to =  accounting.unformat($(".total_tax_in_cart").text());
        if(dis == 0) {
            var totl= sub_total;
        }else{
            var totl= sub_total - (sub_total*(dis/100));
        }
       
        var total_dut = totl + tax_to;
        $(".balance_due_in_cart").text(accounting.formatMoney(total_dut));
        
       

        $(".balance_due_in_cart1").text(accounting.formatMoney(total_dut));

        var total_due_kh = money_exchange_to_kh(total_dut);
        $(".balance_due_in_cart_kh1").val(accounting.formatMoney(total_due_kh));
        var get_data ={
            has_pay_tran: $("#has_pay_tran").val(),
            currency_type: $(".pay_active").val(),
            get_payment_data: $(".balance_due_in_cart1").text() 
        };
        get_payment_total(get_data);
    } 
    

// endchange 
    function get_all_session(){
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        //    alert(formData.item_id);
         
            var url_now = "{{route('get_add_all_session_purchas_order')}}";
            $.ajax({
                
                type: "POST",
                url: url_now,
                dataType: 'json',
                // async: false,
                //data: formData1,
                success: function(result){

                    console.log(result);
                    if(result.supplier_name) {
                        $('#supplier_name').html(result.supplier_name);
                        $('#supplier_id').val(result.supplier_id);
                    }
                    if(result.discount_globle_po){
                        $('.dis_all_in_add_cart').val(result.discount_globle_po);
                    }
                    if(result.sale_type_po == "purchas_order"){
                        $(".purchas_order").addClass("btn-primary sale_type_active");
                        $(".show_sale_by_credit").css("display","none");
                        $(".payment_by_cash_hide").removeAttr("style");
                        // $(".add_pay_me").removeAttr("style");
                        $(".dis_all_in_add_cart").attr('disabled','disabled');
                        $(".total_deposit_fixed_in_cart").removeAttr('disabled');
                        var po_head = '<th colspan="2">ឈ្មោះ</th>';  
                            po_head += '<th>បរិមាណ</th>'; 
                            po_head += '<th >តម្លៃដើម</th>'; 
                            po_head += '<th>បញ្ចុះតម្លៃ(%)</th>'; 
                            po_head += '<th>សរុប</th>';
                        
                        $('#po_head').html(po_head);    
                    }else if(result.sale_type_po == "receiving"){
                        $(".receiving").addClass("btn-primary sale_type_active");
                        $(".show_sale_by_credit").removeAttr("style");
                        $(".payment_by_cash_hide").css("display","none");
                        $(".total_deposit_fixed_in_cart").attr('disabled','disabled');
                        // $(".add_pay_me").css("display","none");
                        $(".dis_all_in_add_cart").removeAttr('disabled');

                        var po_head = '<th colspan="2">ឈ្មោះ</th>';  
                            po_head += '<th>បរិមាណ</th>'; 
                            po_head += '<th>Qty Free</th>';
                            po_head += '<th >តម្លៃដើម</th>'; 
                            po_head += '<th>បញ្ចុះតម្លៃ(%)</th>'; 
                            po_head += '<th>សរុប</th>';
                        
                        $('#po_head').html(po_head);
                    }
                   
                        if(result.sale_type_po == "purchas_order"){
                                $("#order_date").text("ថ្ងៃកម្មង់");
                        }else{
                                $("#order_date").text("ថ្ងៃទទួល");
                        }
                        // if(result.date_for_receiving){
                        //     $('.date_for_receiving').val(result.date_for_receiving);  
                        // }
                    if(result.pay_type_po == "cash"){
                        $(".cash_pay").addClass("btn-primary pay_type_active");
                    }else if(result.pay_type_po == "credit_card"){
                        $(".credit_card_pay").addClass("btn-primary pay_type_active");
                    }else if(result.pay_type_po == "debit_card"){
                        $(".debit_card_pay").addClass("btn-primary pay_type_active");
                    }else if(result.pay_type_po == "check"){
                        $(".check_pay").addClass("btn-primar pay_type_activey");
                    }else if(result.pay_type_po == "gift_card"){
                        $(".gift_card_pay").addClass("btn-primary pay_type_active");
                    }else if(result.pay_type_po == "coupon"){
                        $(".coupon_pay").addClass("btn-primary pay_type_active");
                    }else if(result.pay_type_po == "store_account"){
                        $(".store_account").addClass("btn-primary pay_type_active");
                    }
                  
                   if(result.currency_type_po == "us"){
                         $(".get_pay_us").addClass("btn-success pay_active");
                   }else if(result.currency_type_po == "kh"){
                         $(".get_pay_kh").addClass("btn-success pay_active");
                   }  
                   var num_p_us = 0;
                   var num_p_kh = 0;
                   var num_p_pay_type =result.currency_type_po;
                   var num_p_pay_num = 0;
                   if(result.tran_cash_po){
                       var tran_text = "";
                       $.each(result.tran_cash_po ,function(i,field){
                         if(field.pay_active == "us"){
                            num_p_us += Number(field.payment_num);
                         }else{
                            num_p_kh +=  Number(money_exchange_to_us(field.payment_num)); 
                         }  
                          var url_pr = "{{ url('credit_sales_purchas_order') }}/"+i+"/remove_tran_cash";
                           tran_text += "<tr class='parent"+i+"'>";
                        //    tran_text += "<td><button class='btn btn-danger btn-mini remove_add_payment'value='"+i+"'><i class='icon-remove icon-white'></i></td>";
                           tran_text += "<td><a href='#remove_transtation_pay' class='btn btn-danger btn-mini remove_add_payment' rel='"+i+"'><i class='icon-remove icon-white' style='margin-top: 3px;margin-right: -5px;'></i></a></td>";
                           tran_text += "<td><span class='add_payment'>"+field.payment_num+"</span></td>";
                           tran_text += "<td><span class='add_payment_curren'>"+field.pay_type_active+"</span></td>";
                           tran_text += "<td><span class='add_payment_type'>"+field.pay_active+"</span></td>";
                           tran_text += "<td><span class='add_status'>"+field.status+"</span></td>";
                           tran_text += "</tr>";
                           num_p_pay_type = field.pay_active;
                           num_p_pay_num = field.payment_num;
                       });
                       $(".add_pay_ment").html(tran_text);
                   } 
                   if(result.deposit_price){
                        $('.total_deposit_fixed_in_cart').val(accounting.formatMoney(result.deposit_price));  
                   }
                   $("#has_pay_tran").val(num_p_us + num_p_kh);
                   var get_data ={
                            has_pay_tran: num_p_us + num_p_kh,
                            currency_type: result.currency_type_po,
                            get_payment_data: $(".balance_due_in_cart").text()
                        };
                        get_payment_total(get_data);
                },
                error: function(){} 	        
            });
    }

    // add card to table by on click item
    function add_to_cart_item(formData){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
           // alert(formData.item_id);
           var formData1 = {
                    item_id: formData.item_id, 
                    name: formData.name,
                    sell_price: formData.sell_price
            };
            var url_now = "{{route('post_item_add_to_cart_ajax_purchas_order')}}";
            $.ajax({
                type: "POST",
                url: url_now,
                dataType: 'json',
                data: formData1,
                success: function(result){
                    //console.log(result);
                    // $(".scroller_style1").load("{{url('credit_sales_purchas_order')}}");
                    get_data_cart_item();
                    get_all_session();
                    return false;
                },
                error: function(){} 	        
            });

    }

  // update card card one by one  

 // get payment 
        function get_payment_total(get_data){
           
            var deposit_price = $('.total_deposit_fixed_in_cart').val();
            if(deposit_price != ''){
                deposit_price = accounting.unformat(deposit_price);
            }else{
                deposit_price = 0;
            }
             var h_pay = get_data.has_pay_tran;
            
             var cur_cy = get_data.currency_type;
             var get_payment_data = accounting.unformat(get_data.get_payment_data);
             var total_of_pay1 = (get_payment_data - deposit_price ) - h_pay ;
             if(total_of_pay1 > 0 ){
                 var total_of_pay = total_of_pay1;
             }else{
                var total_of_pay = 0 ;
             }
             
             if(get_data.currency_type == "us"){
                $(".get_payment_num").val(accounting.formatMoney(total_of_pay));
             }else if(get_data.currency_type == "kh"){
                var total_due_kh = money_exchange_to_kh(total_of_pay);
                $(".get_payment_num").val(accounting.formatMoney(total_due_kh));
             }

             $(".balance_due_in_cart1").text(accounting.formatMoney(total_of_pay));
                var total_due_kh1 = money_exchange_to_kh(total_of_pay);
             $(".balance_due_in_cart_kh").text(accounting.formatMoney(total_due_kh1));

        }

 // $(document).ajaxComplete(function(event){
    // update qty 

            // $(".qty_in_add_cart").change(function(){
            $(document).on('change','.qty_in_add_cart',function(){
                //event.preventDefault();
                var parent = $(this).parents("tr");
                var formData = {
                    item_card_id: parent.find("#item_id_card").val(),
                    qty_in_add_cart: $(this).val() ,
                    price : parent.find(".cost_price").val(),
                    qty_free : parent.find(".qty_free_in_add_cart").val(),
                    dis_in_add_cart: parent.find(".dis_in_add_cart").val() 
                };
                    update_card_all(formData);
            });

            // update coust price
            $(document).on('change','.cost_price',function(){
                //event.preventDefault();
                var parent = $(this).parents("tr");
                var formData = {
                    item_card_id: parent.find("#item_id_card").val(),
                    qty_in_add_cart: parent.find(".qty_in_add_cart").val(),
                    price : $(this).val(),
                    qty_free : parent.find(".qty_free_in_add_cart").val(),
                    dis_in_add_cart: parent.find(".dis_in_add_cart").val() 
                };
                    update_card_all(formData);
            });
    // update discount

            $(document).on('change','.dis_in_add_cart',function(){
               
                var parent = $(this).parents("tr");
                var formData = {
                    item_card_id: parent.find("#item_id_card").val(),
                    qty_in_add_cart: parent.find(".qty_in_add_cart").val(),
                    price : parent.find(".cost_price").val(),
                    qty_free : parent.find(".qty_free_in_add_cart").val(),
                    dis_in_add_cart: $(this).val() 
                };
                 update_card_all(formData);
                 
            });
        // update discount

            $(document).on('change','.qty_free_in_add_cart',function(){
               
               var parent = $(this).parents("tr");
               var formData = {
                   item_card_id: parent.find("#item_id_card").val(),
                   qty_in_add_cart: parent.find(".qty_in_add_cart").val(),
                   price : parent.find(".cost_price").val(),
                   dis_in_add_cart: parent.find(".dis_in_add_cart").val(), 
                   qty_free: $(this).val() 
                   
               };
                update_card_all(formData);
                
           });
    // remove item 
        $(document).on('click','.remove_item',function(event){
                var cart_id = $(this).attr('rel');
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                })
                event.preventDefault();
                var url_remove_sale = "{{route('credit_sales_purchas_order')}}/"+cart_id+"/remove_card_by_id";
                $.ajax({
                    type: "POST",
                    url: url_remove_sale,
                    success: function (data) {
                        get_data_cart_item();
                        get_all_session();
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
        });
   // end remove item 
        
    // remove transtation pay  
    $(document).on('click','.remove_add_payment',function(event){
                var cart_id = $(this).attr('rel');
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                })
                event.preventDefault();
                var url_remove_sale = "{{route('credit_sales_purchas_order')}}/"+cart_id+"/remove_tran_cash";
                $.ajax({
                    type: "POST",
                    url: url_remove_sale,
                    success: function (data) {
                        get_all_session();
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            });
   // end remove transtation pay  



    function update_card_all(formData){
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
           // alert(formData.item_id);
           var formData1 = {
                    qty: formData.qty_in_add_cart,
                    price: accounting.unformat(formData.price),
                    discount: formData.dis_in_add_cart,
                    qty_free: formData.qty_free
            };
            var item_card_id = formData.item_card_id;
            var url_now = "{{route('credit_sales_purchas_order')}}/"+item_card_id+"/update_card_by_id";
            $.ajax({
                type: "POST",
                url: url_now,
                dataType: 'json',
                data: formData1,
                success: function(result){
                    
                   // $(".scroller_style1").load("{{url('credit_sales_purchas_order')}}");
                   get_data_cart_item();
                   get_all_session();
                  return false;
                },
                error: function(){} 	        
            });
    }

       // show card table 
   function get_data_cart_item(){
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var url_now = "{{route('post_item_cart_ajax_purchas_order')}}";
            $.ajax({
                type: "POST",
                url: url_now,
                async: false,
                //data:  new FormData(this),
                dataType: 'json',
                //data: formData,
                // contentType: false,
                //cache: true,
                 //processData:false,
                success: function(data){
                    console.log(data);
                    var deposit_setting = 0.3;
                    var total_all_dis, item_list, total_tax, total_qty, total_due , dis_total ; 
                  item_list = '';
                   total_all_dis = 0; total_tax = 0 ; total_qty = 0 ; total_due = 0 ;  
                        $.each(data.con_cart ,function(i,field){
                            
                            
                            var id = field.id;
                            var url_one = "{{url('credit_sales_purchas_order')}}/"+id+"/remove_card_by_id";
                            var name = field.name;
                            var qty = field.quantity;
                            if(field.price){
                                var price = Number(field.price);
                            }else{
                                var price = 0;    
                            }
                            
                            
                            if(field.attributes.qty_free){
                                if(field.attributes.qty_free > 0 ){
                                    var discount = field.attributes.discount;
                                    var to_tax = field.attributes.total_tax;
                                    var sub_pric = field.quantity * field.price;
                                    var price_dis = sub_pric * (discount/100 );
                                    var subtotal1 = sub_pric - price_dis ;
                                    var qty_free = field.attributes.qty_free;
                                    var sutotal_qty_free1 = field.price * qty_free;
                                    var sutotal_qty_free =  sub_pric - sutotal_qty_free1;  
                                    var subtotal = subtotal1 - sutotal_qty_free1;
                                    total_all_dis += subtotal; 

                                      
                                }else {
                                    var discount = field.attributes.discount;
                                    var to_tax = field.attributes.total_tax;
                                    var sub_pric = field.quantity * field.price;
                                    var price_dis = sub_pric * (discount/100 );
                                    var subtotal = sub_pric - price_dis ;
                                    total_all_dis += subtotal; 
                                    var qty_free = 0 ;
                                }
                           }else{
                                var discount = field.attributes.discount;
                                var to_tax = field.attributes.total_tax;
                                var sub_pric = field.quantity * field.price;
                                var price_dis = sub_pric * (discount/100 );
                                var subtotal = sub_pric - price_dis ;
                                total_all_dis += subtotal; 
                                var qty_free = 0 ;
                           }
                        if(field.attributes.total_tax){
                            total_tax += to_tax * qty;   
                            //total deposit 
                        }

                      
                            
                            
                        if(data.sale_type_po == 'purchas_order') {  
                            item_list +='<tr>'; 
                            item_list +='<td><a href="#remove '+id+'" class="btn btn-danger btn-mini remove_item" rel="'+id+'"><i class="icon-trash icon-white"></i></a></td>';
                            item_list +='<td>'+  name +'</td>';
                            item_list += '<td><input name="qty_in_add_cart" type="text"  class="span12 m-wrap qty_in_add_cart" value="'+qty+'" style="margin-bottom: 0px; width: 45px; " /></td>';
                            item_list += '<td><input name="cost_price" type="text"  class="span12 m-wrap  cost_price number-format" value="'+accounting.formatMoney(price)+'" style="margin-bottom: 0px; width: 100px;" /><input type="hidden" id="item_id_card" value="'+id+'"/></td>';
                            item_list += '<td><input name="dis_in_add_cart" type="text"  class="span12 m-wrap dis_in_add_cart" value="'+discount+'" style="margin-bottom: 0px; width: 45px; margin-right: 15px;" disabled/></td>';
                            item_list += '<td><p class="total_in_add_cart">'+subtotal.toFixed(2)+'</p></td>';
                            item_list += '</tr>';
                        }else if( data.sale_type_po == 'receiving' ){
                            item_list +='<tr>'; 
                            item_list +='<td><a href="#remove '+id+'" class="btn btn-danger btn-mini remove_item" rel="'+id+'"><i class="icon-trash icon-white"></i></a></td>';
                            item_list +='<td>'+  name +'</td>';
                            item_list += '<td><input name="qty_in_add_cart" type="text"  class="span12 m-wrap qty_in_add_cart" value="'+qty+'" style="margin-bottom: 0px; width: 45px; " /></td>';
                            item_list += '<td><input name="qty_free_in_add_cart" type="text"  class="span12 m-wrap qty_free_in_add_cart" value="'+qty_free+'" style="margin-bottom: 0px; width: 45px; " /></td>';
                            item_list += '<td><input name="cost_price" type="text"  class="span12 m-wrap  cost_price number-format" value="'+accounting.formatMoney(price)+'" style="margin-bottom: 0px; width: 100px;" /><input type="hidden" id="item_id_card" value="'+id+'"/></td>';
                            item_list += '<td><input name="dis_in_add_cart" type="text"  class="span12 m-wrap dis_in_add_cart" value="'+discount+'" style="margin-bottom: 0px; width: 45px; margin-right: 15px;"/></td>';
                            item_list += '<td><p class="total_in_add_cart">'+subtotal.toFixed(2)+'</p></td>';
                            item_list += '</tr>';
                        }
                        });
                        var dis_all = data.discount_globle_po;
                        dis_total = total_all_dis - (total_all_dis*(dis_all/100));
                        total_due = dis_total + total_tax ;
                
                     $(".scroller_style1").html(item_list);
                     $(".totla_in_cart").text(accounting.formatMoney(data.carttotal));
                     $(".total_sub_total").text(accounting.formatMoney(total_all_dis));
                     $(".balance_due_in_cart").text(accounting.formatMoney(total_due));
                     $(".balance_due_in_cart1").text(accounting.formatMoney(total_due));
                    // $(".get_payment_num").val(accounting.formatMoney(total_due));
                     $(".total_tax_in_cart").text(accounting.formatMoney(total_tax));
                     var total_due_kh = money_exchange_to_kh(total_due);
                     $(".balance_due_in_cart_kh").text(accounting.formatMoney(total_due_kh));
                     $(".balance_due_in_cart_kh1").val(accounting.formatMoney(total_due_kh));
                   
                   
                    // $(".totla_in_cart").html(total_all_dis); 
                },
                    error: function (data) {
                        console.log('Error:', data);
                    }	        
            });

    }

    $('.completed_payment').click(function(e){
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            e.preventDefault();  
            var scroller_style1 = $(".scroller_style1").find("tr").length;
            var add_pay_ment = $(".add_pay_ment").find("tr").length;
            var supplier_id = $("#supplier_id").val();
            var staff_id = $("#staff_id").val();
            var sale_type_active = $(".sale_type_active").val();
            var get_payment_num =  $(".get_payment_num").val();
            var deposit_price = $(".total_deposit_fixed_in_cart").val();    
            var deposit_price = $(".total_deposit_fixed_in_cart").val();    
           var date_for_receiving = $(".date_for_receiving").val();
           if(scroller_style1 == 0){
               alert("you don't any Item to Sale");
               return false;
           }
        if(sale_type_active == "purchas_order"){
           if(supplier_id == 0){
                alert("System required Supplier ");
               return false;
           }
        //    if(duration_of_payment == 0){
        //         alert("System required Duration Of Payment ");
        //        return false;
        //    }
           
           if(get_payment_num == 0){
                alert("System required Payment ");
               return false;
           }
           
        }
    
            
            var payment_amount_change = $('.balance_due_in_cart_kh1').val();
            var total_payment = $('.totla_in_cart').text();
            var sub_payment_amount = $('.total_sub_total').text();
            var dute_payment_amount = $('.balance_due_in_cart').text();
            var has_pay_tran = $("#has_pay_tran").val();
            var comment = $('.sale_comment').val();
            var total_tax = $(".total_tax_in_cart").text();
            
            var data_purchas = {
                payment_amount_change: accounting.unformat(payment_amount_change),
                has_pay_tran: has_pay_tran,
                total_payment: accounting.unformat(total_payment),
                sub_payment_amount: accounting.unformat(sub_payment_amount),
                dute_payment_amount: accounting.unformat(dute_payment_amount),
                comment: comment,
                deposit_price: accounting.unformat(deposit_price),
                date_for_receiving: date_for_receiving,
                staff_id: staff_id,
                total_tax: accounting.unformat(total_tax),
                prices_total_num: accounting.unformat(get_payment_num) 
            };
         
            // var myJSON = JSON.stringify(data_purchas);
            // alert(myJSON);
            var url_now = "{{route('completed_purchas_order')}}";
            $.ajax({
                type: "POST",
                url: url_now,
                dataType: 'JSON',
                data: data_purchas,
                success: function(result){
                    if(sale_type_active == "purchas_order"){  
                        var url_refresh = "{{route('credit_sales_purchas_order')}}/"+result.id+"/invoice";
                        window.location.href = url_refresh;
                    }else{
                        var url_refresh = "{{route('credit_sales_purchas_order')}}/"+result.id+"/receive_invoice";
                        window.location.href = url_refresh;
                    }
                
                },
                    error: function (data) {
                        console.log('Error:', data);
                    }	        
            });
        });
    $(".add_new_item").click(function(){
        window.location.href = "{{url('products/products/create')}}?redirect=purchase_order";
    });    
    $(".add_new_supplier").click(function(){
        window.location.href = "{{url('suppliers/create')}}?redirect=purchase_order";
    }); 
</script>
@stop()