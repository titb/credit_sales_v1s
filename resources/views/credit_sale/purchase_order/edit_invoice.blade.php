@extends('credit_sale.layout.master')
@section('contend')
<link rel="stylesheet" href="{{ URL::to('assets/jquery-ui.css')}}">
<script src="{{ URL::to('assets/jquery-1.12.4.js')}}"></script>
<script src="{{ URL::to('assets/jquery-ui.js')}}"></script>

<meta name="csrf-token" content="{{ csrf_token() }}">
<script src="{{ URL::to('assets/accounting.min.js')}}"></script>
<!-- container-fluid  -->
<div class="container-fluid content_top_mar" >
        <!-- row-fluid -->
            <div class="row-fluid">
                         <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                       <a href="{{url('credit_sales_purchas_order')}}" class="btn btn-sm btn-danger pull-right" style="margin-left:10px; margin-right:10px"><i class="icon-arrow-left icon-white"></i> New Purchase Order </a>    
                                        

                            </div>
                            <!-- block-content collapse in     -->
                            <div class="block-content collapse in">
                               @include('errors.error')	
                               <div class="up_all"></div>
                            </div>
                        </div>
            </div>
</div>

<script type="text/javascript">

	$(document).ready(function(){
                    $.ajaxSetup({
							headers: {
								'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
							}
						})
						var c_id ="{{$po_id}}";
						
						var url_edit = "{{route('credit_sales_purchas_order')}}/"+c_id+"/invoice_json";

						// alert(url_edit);
						var client;
						var forData = {};
							var out = "";
							$.ajax({
									type: "GET",
									url: url_edit, 
									dataType: "json",
									data: forData,
									success: function(result ,xhr){
										console.log(result);
                                        var myJSON = JSON.stringify(result);

                                        $(".up_all").html(myJSON);
                                    },
									error: function (result ) {
										console.log(result.stutus);
									}

							});
						      
				
	});
</script>
@endsection
