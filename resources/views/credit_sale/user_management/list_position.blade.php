@extends('credit_sale.layout.master')

@section('contend')



<div class="container-fluid">

            <div class="row-fluid">



                     <!-- validation -->

                    <div class="row-fluid">

                         <!-- block -->

                        <div class="block">

                            <div class="navbar navbar-inner block-header">

                                <!-- <div class="muted pull-left"><a href="{!! url('users') !!}">អ្នកប្រើប្រាស់</a> </div> -->

                            		<!-- <form class="form-search" style="margin-bottom:2px;" action="{{URL::to('users')}}" method="GET">

											<?php $user = DB::table('mfi_positions')->get();?>

											<input type="text" name="search" placeholder="Name" data-required="1" class="span3 m-wrap" style="margin-top:5px;" id="typeahead" data-provide="typeahead" data-source='[@foreach($user as $row)"{{$row->name}}",@endforeach""]'>

											<button type="submit" class="btn btn-success"><i class="icon-search icon-white"></i> Search</button>

                            		</form> -->
                            		<div class="muted span3 pull-right" style="margin-bottom:5px;margin-top: 0px;"><a href="{{ url('position/create') }}" class="btn btn-success pull-right"><i class="icon-plus icon-white"></i> បង្កើតថ្មី</a></div>

                            </div>

                            <div class="block-content collapse in">

                            @if (count($errors) > 0)

						          <div class="alert alert-danger">

						            <strong>Whoops!</strong> There were some problems with your input.<br><br>

						            <ul>

						              @foreach ($errors->all() as $error)

						                <li>{{ $error }}</li>

						              @endforeach

						            </ul>

						          </div>

						    @endif

                            @if ($message = Session::get('success'))

		                        <div class="alert alert-success">

		                            <p>{{ $message }}</p>

		                        </div>

		                    @endif

		                    @if($message = Session::get('keyerror'))

		                        <div class="alert alert-danger">

		                            <p>{{ $message }}</p>

		                        </div>

		                    @endif



                             	<center>

			                        <h3 class="cen_title text-center khmer_Moul">ការគ្រប់គ្រងមុខតំណែង</h3>

			                    </center>

                            	<legend></legend>



                            <style>



                            </style>





							    <table class="table table-bordered">

							      <thead style="background: rgb(251, 205, 205);">

							        <tr class="header">

							            <th>លេខកូដ

							            </th>

							            <th>ឈ្មោះមុខតំណែង

							            </th>

							            <th>សកម្មភាព

							            </th>

							        </tr>

							      </thead>

							      <tbody>

							        	@foreach($position as $key => $d)

								                <tr>

								                <td>
								                  	<?php
											                $invID = $d->id;
											                $invID = str_pad($invID, 6, '0', STR_PAD_LEFT);
											                echo $invID;
													?>
												</td>

								                  <td>{!! $d->name !!}</td>

								                  <td>

								                  		<a href="{!! url('position/'.$d->id.'/edit') !!}" class="btn btn-primary">កែប្រែ</a>

								                  		{!! Form::open(['method' => 'post','url' => 'position/'.$d->id.'/deleted','style'=>'display:inline']) !!}

											            {!! Form::submit('លុប', ['class' => 'btn btn-danger']) !!}

											        	{!! Form::close() !!}

								                  </td>

								                </tr>

								        @endforeach

							      </tbody>

							    </table>
                            	<!-- Pagination -->
								<div class="pagination text-right">{!! $position->render() !!}</div>

			    			</div>

						</div>

                     	<!-- /block -->

		    		</div>

                     <!-- /validation -->

                </div>

            </div>





@stop()
