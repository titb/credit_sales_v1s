@extends('credit_sale.layout.master')

@section('contend')



<div class="container-fluid">

            <div class="row-fluid">

                    <div class="row-fluid">

                         <!-- block -->

                        <div class="block">

                            <div class="navbar navbar-inner block-header">

                                <div class="muted pull-left"><a href="{!! url('users') !!}">អ្នកប្រើប្រាស់</a> <span class="divider">/</span>កែប្រែអ្នកប្រើប្រាស់</div>

                            </div>

                            <div class="block-content collapse in">

                            @if (count($errors) > 0)

						          <div class="alert alert-danger">

						            <strong>Whoops!</strong> There were some problems with your input.<br><br>

						            <ul>

						              @foreach ($errors->all() as $error)

						                <li>{{ $error }}</li>

						              @endforeach

						            </ul>

						          </div>

						    @endif

                            @if ($message = Session::get('success'))

		                        <div class="alert alert-success">

		                            <p>{{ $message }}</p>

		                        </div>

		                    @endif

		                    @if($message = Session::get('keyerror'))

		                        <div class="alert alert-danger">

		                            <p>{{ $message }}</p>

		                        </div>

		                    @endif

                            <div class="span12">

                            	<h3 class="cen_title">កែប្រែអ្នកប្រើប្រាស់</h3>

                            	<legend></legend>

                            </div>

                            	<form action="{{ url('users/'.$data->id.'/edit') }}" method="post" enctype="multipart/form-data">

	                                {{ csrf_field() }}

	                                <div class="span12" style="padding-left: 50px;">



										  <div class="control-group">

										  	<label class="control-label">ជ្រើសរើសសាខា<span class="required">*</span></label>

			  								<div class="controls">
			  									<?php $branch = DB::table('mfi_branch')->get(); ?>
			  									<select class="span9 m-wrap" name="branch_id">
			  										@foreach($branch as $b)
			  										<option value="{{ $b->id }}" <?php if($data->branch_id == $b->id){ echo "selected";} ?> >{{ $b->brand_name }}</option>
			  										@endforeach
			  									</select>

			  								</div>

			  								<label class="control-label">ឈ្មោះអ្នកប្រើប្រាស់<span class="required">*</span></label>

			  								<div class="controls">

			  									<input type="text" name="username" data-required="1" value="{{ $data->username }}" class="span9 m-wrap"/>

			  								</div>

			  								<label class="control-label">ឈ្មោះជាភាសាខ្មែរ<span class="required">*</span></label>

			  								<div class="controls">

			  									<input type="text" name="name_kh" value="{{ $data->name_kh }}" data-required="1" class="span9 m-wrap"/>

			  								</div>

			  								<label class="control-label">ភេទ<span class="required">*</span></label>

			  								<div class="controls">

			  									<select id="selectError"​ name="user_sex" class="span9 m-wrap">

												  <option value="M" <?php if($data->user_gender == "M"){echo "selected";} ?> >ប្រុស</option>

												  <option value="F" <?php if($data->user_gender == "F"){echo "selected";} ?> >ស្រី</option>

												</select>

			  								</div>

			  								<script type="text/javascript">
											   $('.telnumber').keyup(function() {
												foo = $(this).val().split("-").join(""); // remove hyphens
												foo = foo.match(new RegExp('.{1,3}$|.{1,3}', 'g')).join("-");
											        $(this).val(foo);
											   });
											</script>

			  								<label class="control-label">លេខទូរសព្ទ<span class="required">*</span></label>

			  								<div class="controls">

			  									<input type="text" name="user_phone" value="<?php
													$length = strlen($data->user_phone);
													  if ($length == 9) {
													    echo preg_replace("/^1?(\d{3})(\d{3})(\d{3})$/", "$1-$2-$3", $data->user_phone);
													  }
													  else{
													    echo preg_replace("/^1?(\d{3})(\d{3})(\d{3})(\d{1})$/", "$1-$2-$3-$4", $data->user_phone);
													  }
													?>" data-required="1" class="span9 m-wrap telnumber"/>

			  								</div>

			  								<label class="control-label">សារអេឡិចត្រូនិច<span class="required">*</span></label>

			  								<div class="controls">

			  									<input type="text" name="user_email" data-required="1" value="{{ $data->user_email }}" class="span9 m-wrap"/>

			  								</div>

											<label class="control-label">កំណត់សិទ្ធិអ្នកប្រើប្រាស់<span class="required">*</span></label>

			  								<div class="controls">

												 {!! Form::select('group_id', $groups,$user_group, array('class' => 'span9 m-wrap')) !!}

			  								</div>

			  								<label class="control-label">មុខតំណែង<span class="required">*</span></label>
			  									<?php $position = DB::table('mfi_positions')->where('deleted','=',0)->get(); ?>
			  									<select name="position_id" class="span9 m-wrap">
			  										@foreach($position as $p)
			  										<option value="{{ $p->id }}" <?php if($data->position_id == $p->id){echo "selected";} ?> >{{ $p->name }}</option>
			  										@endforeach
			  									</select>

			  								<label class="control-label">រូបថត</label>
			  								<div class="controls">
			  									<input type="file" name="photo" class="input-file uniform_on" value="{{ $data->photo }}">
			  								</div>

			  								<!-- <label class="control-label">លេខសំងាត់<span class="required">*</span></label>

			  								<div class="controls">

			  									<input type="password" name="password" data-required="1" class="span9 m-wrap"/>

			  								</div>

			  								<label class="control-label">បំពេញលេខសំងាត់ម្តងទៀត<span class="required">*</span></label>

			  								<div class="controls">

			  									<input type="password" name="confirm-password" data-required="1" class="span9 m-wrap"/>

			  								</div> -->

	  									  </div>



									</div>



									<div class="span12">

										<center>

											<button type="submit" class="btn btn-success">កែប្រែ</button>
											<a href="{{ url('users') }}" class="btn btn-danger">ត្រលប់</a>

										</center>

									</div>

								</form>

			    			</div>

						</div>

                     	<!-- /block -->

		    		</div>

                     <!-- /validation -->

                </div>

            </div>







@stop()
