@extends('credit_sale.layout.master')

@section('contend')



<div class="container-fluid">

            <div class="row-fluid">



                <!--/span-->

                	<!-- <div class="row-fluid">

		                	<div class="navbar">

		                    	<div class="navbar-inner">

		                            <ul class="breadcrumb">

		                            	<li class="active"> <a href="{{ url('users-groups') }}" class="btn btn-defualt pull-right"><i class="icon-eye-open"></i>  View</a></li>

		                                <li class="active"> <a href="{{ url('users-groups/create') }}" class="btn btn-success pull-right"><i class="icon-pencil icon-white"></i>  Create New</a></li>



		                            </ul>



		                    	</div>

		                	</div>



		            </div> -->

                     <!-- validation -->

                    <div class="row-fluid">

                         <!-- block -->

                        <div class="block">

                            <div class="navbar navbar-inner block-header">

                                <div class="muted pull-left"><a href="{!! url('users-groups') !!}">សិទ្ធិអ្នកប្រើប្រាស់</a> <span class="divider">/</span>បង្កើតសិទ្ធិអ្នកប្រើប្រាស់ថ្មី</div>

                            </div>

                            <div class="block-content collapse in">

                            @if (count($errors) > 0)

						          <div class="alert alert-danger">

						            <strong>Whoops!</strong> There were some problems with your input.<br><br>

						            <ul>

						              @foreach ($errors->all() as $error)

						                <li>{{ $error }}</li>

						              @endforeach

						            </ul>

						          </div>

						    @endif

                            @if ($message = Session::get('success'))

		                        <div class="alert alert-success">

		                            <p>{{ $message }}</p>

		                        </div>

		                    @endif

		                    @if($message = Session::get('keyerror'))

		                        <div class="alert alert-danger">

		                            <p>{{ $message }}</p>

		                        </div>

		                    @endif

                            <div class="12">



                            	<h3 class="cen_title"> បង្កើតសិទ្ធិអ្នកប្រើប្រាស់ថ្មី </h3>

                            	<legend></legend>



                            </div>

                            	<form action="{{ url('users-groups/create') }}" method="post" enctype="multipart/form-data">

	                                {{ csrf_field() }}

	                                <div class="span12" style="padding-left: 50px;">



										  <div class="control-group">

			  								<label class="control-label">Name Function<span class="required" placeholder="Ex:  title-name">*</span></label>

			  								<div class="controls">

			  									<input type="text" name="name" data-required="1" class="span9 m-wrap"/>

			  								</div>

			  								<label class="control-label">Name Display<span class="required" placeholder="Ex: Title Name">*</span></label>

			  								<div class="controls">

			  									<input type="text" name="display_name" data-required="1" class="span9 m-wrap"/>

			  								</div>

			  								<label class="control-label">Discription<span class="required">*</span></label>

			  								<div class="controls">

			  									<input type="text" name="description" data-required="1" class="span9 m-wrap"/>

			  								</div>



	  									  </div>



									</div>

									<div class="span12">

										<div class="controls">



													<input type="checkbox" id="selecctall"/> Selecct All<br/><br/>



												@foreach ($groups as $key => $perm)

												<div>

														<div class="span12" style="margin-left: 0px !important;">

															<b style="color:#08c; font-size: 14px; text-transform: capitalize;"> {{ $key }} </b>

														</div>

													 @foreach($perm as $k => $value)

											                 	<div class="span3" style="margin-left: 0px !important;">

												                	<label>{{ Form::checkbox('permission_id[]', $value->id, false, array('class' => 'checkbox1')) }}

												                	{{ $value->display_name }}</label>

											                	</div>

									                @endforeach

									            </div><br/>

									            @endforeach

			  								</div>

									</div>

									<div class="span12">

										<center>

											<button type="submit" class="btn btn-success">Save</button>
											<a href="{{ url('users-groups') }}" class="btn btn-danger">Back</a>

										</center>

									</div>

								</form>

			    			</div>

						</div>

                     	<!-- /block -->

		    		</div>

                     <!-- /validation -->

                </div>

            </div>





<script type="text/javascript">



	$(document).ready(function() {

    $('#selecctall').click(function(event) {  //on click

        if(this.checked) { // check select status

            $('.checkbox1').each(function() { //loop through each checkbox

                this.checked = true;  //select all checkboxes with class "checkbox1"

            });

        }else{

            $('.checkbox1').each(function() { //loop through each checkbox

                this.checked = false; //deselect all checkboxes with class "checkbox1"

            });

        }

    });





});

</script>



@stop()
