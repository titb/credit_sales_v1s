@extends('credit_sale.layout.master')

@section('contend')
            <div class="row-fluid">
                     <!-- validation -->
                    <div class="row-fluid">
                        <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <!-- <div class="muted pull-left"><a href="{!! url('users') !!}">សិទ្ធិ</a> </div> -->
                            		<form class="form-search" style="margin-bottom:2px;" action="{{URL::to('users-permission')}}" method="GET">
											<?php $user_groups = DB::table('groups')->get();?>
											<input type="text" name="search" data-required="1" class="span3 m-wrap" style="margin-top:5px;" id="typeahead" data-provide="typeahead" data-source='[@foreach($user_groups as $row)"{{$row->name}}",@endforeach""]'>
											<button type="submit" class="btn btn-success"><i class="icon-search icon-white"></i> ស្វែងរក</button>
                            		</form>
                            </div>
                            <div class="block-content collapse in">
                            @if (count($errors) > 0)
						          <div class="alert alert-danger">
						            <strong>Whoops!</strong> There were some problems with your input.<br><br>
						            <ul>
						              @foreach ($errors->all() as $error)
						                <li>{{ $error }}</li>
						              @endforeach
						            </ul>
						          </div>
						    @endif
                            @if ($message = Session::get('success'))
		                        <div class="alert alert-success">
		                            <p>{{ $message }}</p>
		                        </div>
		                    @endif
		                    @if($message = Session::get('keyerror'))
		                        <div class="alert alert-danger">
		                            <p>{{ $message }}</p>
		                        </div>
		                    @endif
                            <center>
                            		<h3 class="cen_title text-center"> ការគ្រប់គ្រងសិទ្ធិ </h3>

                            		<div class="muted span3 pull-right" style="margin-bottom:5px;"><a href="{{ url('users-permission/create') }}" class="btn btn-success pull-right"><i class="icon-plus icon-white"></i> បង្កើតថ្មី</a></div>
                            </center>
					    <table class="table table-bordered">
								      <thead>
								        <tr class="header">
								            <th>លេខសម្គាល់</th>
								            <th>ឈ្មោះនៃសិទិ្ធ</th>
                                            <th>ឈ្មោះនៃសិទិ្ធដែលត្រូវ</th>
								            <th>សេចក្តីបរិយាយ</th>
								            <th>សកម្មភាព </th>
								        </tr>
								      </thead>
								      <tbody>
								        @foreach($data as $key => $d)
							                <tr>
							                  <td>
							                  	<?php
											                $invID = $d->id;
											                $invID = str_pad($invID, 6, '0', STR_PAD_LEFT);
											                echo $invID;
													?>
												</td>
							                  <td>{!! $d->name !!} </td>
							                  <td> {{ $d->display_name }} </td>
							                  <td>{!! $d->description !!} </td>
                                              <td>{!! $d->module !!}</td>
							                  <td>
							                  		<a href="{!! url('users-permission/'.$d->id.'/edit') !!}" class="btn btn-info">កែប្រែ</a>
							                  		{!! Form::open(['method' => 'post','url' => 'users-permission/'.$d->id.'/deleted','style'=>'display:inline']) !!}
										            {!! Form::submit('លុប', ['class' => 'btn btn-danger']) !!}
										        	{!! Form::close() !!}
							                  </td>
							                </tr>
							        	@endforeach
								      </tbody>
								    </table>
								    <!-- Pagination -->
								<div class="pagination text-right">{!! $data->render() !!}</div></div>
						</div>
                     	<!-- /block -->
		    		</div>
                     <!-- /validation -->
                </div>
@stop()
