@extends('credit_sale.layout.master')
@section('contend')
<link rel="stylesheet" href="{{ URL::to('assets/jquery-ui.css')}}">
<script src="{{ URL::to('assets/jquery-1.12.4.js')}}"></script>
<script src="{{ URL::to('assets/jquery-ui.js')}}"></script>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="{{ URL::to('bootstrap/js/bootstrap.min.css') }}" rel="stylesheet" media="screen"> -->
<style>
    /* .sale_me :hover,.sale_me:focus, .sale_me:active, .sale_me.active {
        color: #fff !important;
        background-color: #04c !important;
        
    } */
    .table .total_foot td {
        border-top: 0px solid #ddd;
        padding: 5px;
        line-height: 14px;
        text-align: left;
        vertical-align: bottom;
    }
    .table .total_foot td p{
        margin: 0px;
    }
    h1.in_cart ,h2.in_cart ,h3.in_cart {
        line-height: 0px;
    }
    table.table.table_sadow {
        border: 2px solid #ddd;
       
    }
    
    @media only screen and (min-width: 1023px) {
        .table .total_foot td {
           min-width:100px;
        }
        table.table.table_sadow tbody {
            height: 223px;
        }
    }
    @media only screen and (max-width: 1024px) {
        .table .total_foot td {
           min-width:70px;
        }
        table.table.table_sadow tbody {
            height: 200px;
        }
    }
    /* width */
    .scroller_style1::-webkit-scrollbar {
            width: 3px;
        }

        /* Track */
        .scroller_style1::-webkit-scrollbar-track {
            background: #f1f1f1; 
        }
        
        /* Handle */
        .scroller_style1::-webkit-scrollbar-thumb {
            background: #888; 
        }

        /* Handle on hover */
        .scroller_style1::-webkit-scrollbar-thumb:hover {
            background: #555; 
        }
        table.table.table_sadow thead tr {
            display: block;
            position: relative;
        }
        table.table.table_sadow tbody {
            display: block;
            overflow: auto;
            width: 100%;
        }
        /* table.table.table_sadow th ,  table.table.table_sadow td {
            
        } */
        /* table.table.table_sadow thead tr th {
            width: 7%;  
           padding: 8px;
            line-height: 20px;
            text-align: left;

        }
        table.table.table_sadow td {
            vertical-align: middle;
            width: 10%;  
        } */

        table.table.table_sadow th:first-child {
            /* vertical-align: middle; */
            text-align: left;
            /* width: 50%;   */
        }
        table.table.table_sadow td:first-child {
            text-align: left;
            vertical-align: middle;
            width: 24%;  
        }
        table.table.table_sadow th {
            /* width:10%; */
            text-align: center;
        }
        table.table.table_sadow td {
            width:10%;
            text-align: center;
            vertical-align: middle;
           
        }
        table.table.table_sadow td p{
            margin:0px;
        }
        a.btn.btn-danger.btn-mini.remove_item {
            font-size: 8px !important;
            padding: 0 2px !important;
        }
        .form-horizontal .controls {
            margin-left: 100px !important;
        }
        .form-horizontal .control-label{
            width: 86px !important;
        }
        .control-group.customer {
            margin-bottom: 20px!important;
        }
        .form-horizontal .control-group{
            margin-bottom: 10px;
        }
        
</style>
<!-- container-fluid  -->
<div class="container-fluid">
        <!-- row-fluid -->
            <div class="row-fluid">
                         <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                        <button  class="btn sale_me sale_by_credit" id="sale_by_credit" value="sale_by_credit">Sale By Credit</button> 
                                        <button  class="btn sale_me sale_by_cash" id="sale_by_cash" value="sale_by_cash" >Sale By Cash</button>
                                        <a href="{{url('post_cancel_all_cart_item')}}" class="btn btn-sm btn-danger pull-right" style="margin-left:10px; margin-right:10px"><i class="icon-remove icon-white"></i> Cancel </a>    
                                    <div class="pull-right">
                                        <p> Currency Exchange Rate : <b style="color:blue"> 1$ </b> = <b style="color:red"> 4100 ៛​ </b></p>   
                                    </div>				
                            </div>
                            <!-- block-content collapse in     -->
                            <div class="block-content collapse in">
                               @include('errors.error')	
                             	
                                <!-- span6 up -->
                                    <div class="span6" style="margin-left:0px;">
                                        <div class="span12">
                                                <!-- <form class="form-horizontal"> -->
                                                <div class="block-content collapse in" style="margin-top:0px;">
                                                    <fieldset>
                                                        <!-- <span class="required">*</span> -->
                                                            <div id="sale_by_cash">
                                                                <!-- Search Item add to cart -->
                                                                    <div class="control-group">
                                                                        <div class="controls">
                                                                                <input name="search_item" type="text" id="search_item"  data-required="1" class="span12 m-wrap" placeholder="Enter Name , Barcode Item or Item Number" autocomplete="off"/>     
                                                                        </div>
                                                                        <!-- <legend></legend> -->
                                                                    </div>
                                                                <!-- end  Search Item add to cart -->
                                                                <!-- Item Table -->
                                                                    <div class="control-group">
                                                                        <table class="table table_sadow">
                                                                            <thead style="background-color: #0088cc;color: #fff;">
                                                                                <tr>
                                                                                    <th width="253px">Name</th>
                                                                                    <th width="90px">Qty</th>
                                                                                    <th width="90px">Price</th>
                                                                                    <th width="90px">Dis(%)</th>
                                                                                    <th width="90px">Total</th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody class="scroller_style1">
                                                                                
                                                                            </tbody>
                                                                            </table>
                                                                        </div>
                                                                 <!--end Item Table -->
                                                                 <!-- total Item     -->
                                                                        <div class="control-group">
                                                                            <table class="table"> 
                                                                                <tfoot class="total_foot">
                                                                                    <tr>
                                                                                        <td><b>Sub Total :  </b></td>
                                                                                      <td> <b class="totla_in_cart in_cart">$0.00</b></td>
                                                                                       <td><b>TOTAL : </b></td>
                                                                                     <td><b style="color:red" class="total_sub_total">$0.00</b></td>
                                                                                   </tr>
                                                                                 <tr>
                                                                                   <td><b>Discount(%) :  </b></td>
                                                                                        <td> <input name="dis_all_in_add_cart" type="text" class="span9 m-wrap dis_all_in_add_cart" value="0" style="margin-bottom: 0px;" >  </td>
                                                                                        <td colspan="2"></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td><b>Tax : </b></td>
                                                                                        <td> <b class="total_tax_in_cart in_cart">$0.00</b></td>
                                                                                        <td><b>Balance Due : </b></td>
                                                                                        <td><b style="color:red" class="balance_due_in_cart in_cart">$0.00</b></td>
                                                                                    </tr>
                                                                                    <input type="hidden" name="balance_due_in_cart_kh" class="balance_due_in_cart_kh1" >

                                                                                </tfoot>
                                                                            </table>
                                                                        </div>
                                                                 <!-- End total Item -->
                                                                 <!-- buttom Pay -->
                                                                 <!-- <div class="control-group"> -->
                                                                    <!-- <button type="reset" class="btn btn-danger"><i class="icon-remove icon-white"></i></button> -->
                                                                  
                                                                     <!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Pay</button> -->
                                                                     
                                                                    <!-- <button type="submit" class="btn btn-info">Pay</button> -->
                                                                <!-- </div>  -->
                                                                 <!-- End buttom Pay -->
                                                            </div>
                                                        <!-- End Sale By Cash -->
                                                       
                                                    </fieldset>
                                                </div>  
                                                <!-- </form> -->

                                            </div>
                                    </div>

                                <!-- end span6 up  -->
                                <!-- span6 down -->
                                    <div class="span6">
                                        <div class="span12">
                                            <div class="block-content collapse in" style="margin-top:0px;"> 
                                                <fieldset class="form-horizontal">
                                                    <!-- <legend>Form Horizontal</legend> -->
                                                    <div class="control-group customer">
                                                            <label class="span4 m-wrap" style="line-height: 30px; margin-bottom: 0px;">Customer : <b id="customer_name"></b></label>
                                                            <input type="hidden" name="customer_id" id="customer_id" value="0"/>
                                                            <div class="controls">
                                                                    <input name="search_customer" id="search_customer" type="text"  data-required="1" class="span8 m-wrap" placeholder="Enter Customer Name Or Customer ID" autocomplete="off"/>     
                                                                    <button class="btn btn-success" ><i class="icon-plus icon-white"></i></button>
                                                            </div> 
                                                    </div>

                                                </fieldset>
                                                <fieldset class="form-horizontal">
                                                    <div class="control-group">
                                                        <center>
                                                            <button class="btn pay_type cash_pay " id="" value="cash">Cash</button>
                                                            <!-- <button class="btn pay_type credit_card_pay" value="credit_card">Credit Card</button>
                                                            <button class="btn pay_type debit_card_pay" value="debit_card">Debit Card</button> -->
                                                            <button class="btn pay_type check_pay" value="check">Check</button>
                                                            <button class="btn pay_type gift_card_pay" value="gift_card">Gift Card</button>
                                                            <button class="btn pay_type coupon_pay" value="coupon">coupon</button>
                                                            <button class="btn pay_type store_account" value="store_account">Account Store</button>
                                                        </center>
                                                    </div>
                                                    <div class="control-group">
                                                            <table class="table"> 
                                                                <tfoot class="total_foot">
                                                                    <tr>
                                                                        <td><b>Balance Due US :  </b></td>
                                                                        <td><h3 style="color:red" class="balance_due_in_cart1 in_cart">0.00 </h3></td>
                                                                        <td><b>Balance Due KH : </b></td>
                                                                        <td><h3 style="color:red" class="balance_due_in_cart_kh in_cart">0.00 </h3></td>
                                                                    </tr>
                                                                    
                                                                </tfoot>
                                                               
                                                            </table>
                                                            
                                                        <!-- </div> -->
                                                    </div>
                                                    <div class="control-group">
                                                                <label class="control-label" for="focusedInput" style="text-align: left;">Get Payment</label>
                                                                <div class="controls">
                                                                    <input class="span7 m-wrap get_payment_num number-format" id="focusedInput" type="text" value="" placeholder="0.00"  autocomplete="off">
                                                                    <button class="btn get_pay get_pay_us" value="us">US</button>
                                                                    <button class="btn get_pay get_pay_kh" value="kh">KH</button>
                                                                    <button class="btn btn-primary add_pay_me pull-right" >Add Pay</button>
                                                                </div>
                                                            </div>
                                                <!-- If Sale Type Not Credit Sale  -->
                                                <div class="show_sale_by_credit">
                                                    
                                                    <div class="control-group">
                                                            <label class="control-label" for="focusedInput" style="text-align: left;width: 100px !important;">Payment Letter</label>
                                                            <div class="controls">
                                                                <input class="span10 m-wrap get_payment_num_letter " id="focusedInput" type="text" value="" placeholder="តម្លៃសរុបជាអក្សរ"  autocomplete="off">
                                                            </div>
                                                    </div>
                                                    <div class="control-group">
                                                            <label class="control-label" for="focusedInput" style="text-align: left;">Due Deposit<span style="color:red">* </span> </label>
                                                            <div class="controls">
                                                            <input name="total_deposit_fixed_in_cart" type="text" class="span10 m-wrap total_deposit_fixed_in_cart number-format" style="margin-bottom: 0px;" placeholder="0.00">
                                                            </div>
                                                    </div>  
                                                    <div class="control-group">
                                                            <label class="control-label" for="focusedInput" style="text-align: left; width: 100px !important;">Deposit Letter </label>
                                                            <div class="controls">
                                                            <input name="total_deposit_fixed_letter_in_cart" type="text" class="span10 m-wrap total_deposit_fixed_letter_in_cart" style="margin-bottom: 0px;" placeholder="បង់ប្រាក់កក់មុន ជាអក្សរ ">
                                                            </div>
                                                    </div>  
                                                    <div class="control-group">
                                                            <label class="control-label" for="focusedInput" style="text-align: left;">Owed Due<span style="color:red">* </span> </label>
                                                            <div class="controls">
                                                            <input name="total_credit_due_in_cart" type="text" class="span10 m-wrap total_credit_due_in_cart" style="margin-bottom: 0px;" placeholder="0.00">
                                                            </div>
                                                    </div>  
                                                    <div class="control-group">
                                                            <label class="control-label" for="focusedInput" style="text-align: left; width: 100px !important;">Owed Due Letter  </label>
                                                            <div class="controls">
                                                            <input name="total_credit_due_letter_in_cart" type="text" class="span10 m-wrap total_credit_due_letter_in_cart number-format" style="margin-bottom: 0px;" placeholder="តម្លៃសរុបដែលនៅសល់ ">
                                                            </div>
                                                    </div>
                                                    <div class="span11" style="margin-left:0;">
                                                        <div class="span7 control-group">
                                                            <label class="control-label" for="focusedInput" style="text-align: left;width: 100px!important">រយៈពេលបង់ប្រាក់ <span style="color:red">* </span> </label>
                                                            <div class="controls">
                                                                <input class="span12 m-wrap duration_of_payment number-format" id="focusedInput" type="text" value="0">
                                                            </div>
                                                        </div>
                                                        <div class="span4 control-group">
                                                            <label class="control-label" for="focusedInput" style="text-align: left;  width: 40px !important;">គិតជា </label>
                                                            <div class="controls" style="margin-left: 40px !important;">
                                                                <!-- <input class="span12 m-wrap duration_of_payment_type" id="focusedInput" type="text" value="0"> -->
                                                                <select name="duration_of_payment_type" class="span12 m-wrap duration_of_payment_type">
																		<option value="none">-- ជ្រើសរើស  --</option>
																		<option value="month">ខែ</option>
                                                                        <option value="2week">២ សប្តាហ៍</option>
                                                                        <option value="week">សប្តាហ៍</option>
																		<option value="day">ថ្ងៃ</option>
																</select>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="control-group">
                                                            <label class="control-label" for="focusedInput" style="text-align: left; width: 100px !important;">ថ្ងៃសុុំបង់ប្រាក់  </label>
                                                            
                                                            <div class="controls">
                                                                <input type="text" id="date_request_for_pay" class="input-xlarge datepicker span10 m-wrap date_request_for_pay" name="date_request_for_pay" data-required="1" autocomplete="off">
                                                            </div>
                                                    </div>   
                                                    <div class="control-group">
                                                            <label class="control-label" for="focusedInput" style="text-align: left; width: 100px !important;">ថ្ងៃបង្កើត  </label>
                                                            
                                                            <div class="controls">
                                                                <input type="text" id="date_create_request" class="input-xlarge datepicker span10 m-wrap date_create_request" name="date_create_request" data-required="1" autocomplete="off">
                                                            </div>
                                                    </div> 
                                                    <div class="control-group">
                                                            <label class="control-label" for="focusedInput" style="text-align: left;  width: 100px !important;">CO </label>
                                                            <div class="controls" style="margin-left: 40px !important;">
                                                            <?php $staff = App\User::all(); ?>
                                                              
                                                                <select name="staff_id" class="span9 m-wrap staff_id" id="staff_id">
																		<option value="">-- ជ្រើសរើស  --</option>
                                                                        @foreach($staff as $st)
																		    <option value="{{$st->id}}" @if($sale_type != "return")@if(Auth::user()->id == $st->id) selected @endif  @endif >{{$st->name_kh}}</option>
                                                                        @endforeach
                                                                       
																</select>
                                                            </div>
                                                    </div>            
                                                </div>    
                                                <!-- End If Sale Type Not Credit Sale  -->

                                                <!-- If Not Sale Type Not Credit Sale  -->
                                                <div class="payment_by_cash_hide">
                                                            <div class="control-group">
                                                                    <table class="table"> 
                                                                        <tbody class="add_pay_ment">
                                                                        <tbody>
                                                                    </table>
                                                                    <input type="hidden" name="has_pay_tran" id="has_pay_tran">
                                                                
                                                            </div>
                                                        <div class="span11" style="margin-left:0;">
                                                            <div class="span6 control-group">
                                                                <label class="control-label" for="focusedInput" style="text-align: left;">Remaining US</label>
                                                                <div class="controls">
                                                                    <input class="span12 m-wrap remaining_us" id="focusedInput" type="text" value="0">
                                                                </div>
                                                            </div>
                                                            <div class="span6 control-group">
                                                                <label class="control-label" for="focusedInput" style="text-align: left;">Change </label>
                                                                <div class="controls">
                                                                    <input class="span12 m-wrap remaining_kh" id="focusedInput" type="text" value="0">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="span11" style="margin-left:0;">
                                                            <div class="span6 control-group">
                                                                <label class="control-label" for="focusedInput" style="text-align: left;" >Payback US</label>
                                                                <div class="controls">
                                                                    <input class="span12 m-wrap payback_us" id="focusedInput" type="text" value="0">
                                                                </div>
                                                            </div>
                                                            <div class="span6 control-group">
                                                                <label class="control-label" for="focusedInput" style="text-align: left;" >Payback KH</label>
                                                                <div class="controls">
                                                                    <input class="span12 m-wrap payback_kh" id="focusedInput" type="text" value="0">
                                                                </div>
                                                            </div>
                                                        </div>
                                                            <!-- <div class="control-group">
                                                                <label class="control-label" for="focusedInput" style="text-align: left;" >Payback KH</label>
                                                                <div class="controls">
                                                                    <input class="span11 m-wrap payback_kh" id="focusedInput" type="text" value="0">
                                                                </div>
                                                            </div> -->
                                                          

                                                </div>
                                             <!--End If Not Sale Type Not Credit Sale  -->    
                                                    <div class="control-group">
                                                        <label class="control-label" for="focusedInput" style="text-align: left;">Comment</label>
                                                        <div class="controls">
                                                        <textarea class="span10 m-wrap sale_comment"  placeholder="Enter Comment on sale ..."></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="control-group">
                                                        <center>
                                                            <button class="btn btn-primary completed_payment">Complete Payment</button>
                                                        </center>
                                                    </div>
                                                </fieldset>

                                            </div>
                                        </div>
                                    </div>    
                                <!-- end span6 down  -->
                            </div>  
                            <!-- end block-content collapse in     -->
                         </div>    
                        <!-- end block -->
                   
            </div>
            <!-- end row-fluid -->
</div>

<!-- end container-fluid  -->
<meta name="csrf-token" content="{{ csrf_token() }}">
<script src="{{ URL::to('assets/accounting.min.js')}}"></script>
<script type="text/javascript">
// item search

var default_currency = 1 ;
var currency_us = 1;
var currency_kh = 2 ;
var get_fix_right = 2 ;
var config_cur_1us_to_kh = 4100;
if(default_currency == currency_us){
    var curren = "$";
}else if(default_currency == currency_kh){
    var curren = "៛";
}

function round_up_kh(num){
    var num2 ,num3 ,num4;
    num2 = num /100;
    num3 = Math.ceil(num2);
    num4 = num3 *100;
    return num4;
}
 
function money_exchange_to_kh(bal_due){
    var to_kh_1 = bal_due *  config_cur_1us_to_kh;
     var to_kh = round_up_kh(to_kh_1); 
     return to_kh;
}
function money_exchange_to_us(bal_due){
    var to_us1 = bal_due / config_cur_1us_to_kh
    var to_us_fixed = to_us1.toFixed(get_fix_right);
    var to_us = to_us_fixed; 
     return to_us;
}
// $(".currency_defult").text(curren);
function input_type_currency(){

}
$('.number-format').keyup(function(event) {

	  // skip for arrow keys
	  if(event.which >= 37 && event.which <= 40){
	   event.preventDefault();
	  }

	  $(this).val(function(index, value) {
	      value = value.replace(/,/g,'');
	      return numberWithCommas(value);
	  });
	});

	function numberWithCommas(x) {
	    var parts = x.toString().split(".");
	    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	    return parts.join(".");
	} 
    $(window).load(function(){
        get_all_session();
        get_data_cart_item();
    });

// click buttom type of sale 
$(".sale_me").click(function(){
            $(".sale_me").removeClass("btn-primary sale_type_active"); 
            $(this).addClass("btn-primary sale_type_active");
           var sale_type = $(this).val();

            var formdata = {
                sale_type: sale_type 
            }; 
        add_all_session(formdata);
});
// click buttom pay type of sale 
$(".pay_type").click(function(){
            $(".pay_type").removeClass("btn-primary pay_type_active"); 
            $(this).addClass("btn-primary pay_type_active"); 
           var pay_type = $(this).val();

            var formdata = {
                pay_type: pay_type 
            }; 
        add_all_session(formdata);
});

// click buttom pay currency type of sale 
$(".get_pay").click(function(){
            $(".get_pay").removeClass("btn-success pay_active"); 
            $(this).addClass("btn-success pay_active"); 
           var currency_type = $(this).val();

            var formdata = {
                currency_type: currency_type 
            }; 
   // total_function_foot(formdata);
        add_all_session(formdata);
        var get_payment_data = $(".balance_due_in_cart").text();
        var has_pay_tran = $("#has_pay_tran").val();
        var get_data ={
            has_pay_tran: has_pay_tran,
            currency_type: $(this).val(),
            get_payment_data: get_payment_data,
            
        };
        get_payment_total(get_data)
});

    // complete and add payment 
    $(".add_pay_me").click(function(event){
        var pay_active, pay_back_kh, pay_back_us, change_to_kh, change_to_us,
        sale_type_active, pay_type_active, get, get_payment, get_payment1, 
        ba_due_payment, ba_due_payment_kh, ba_due_payment_kh1 ,total_pay_back, total_pay_due;
            
            pay_active  = $(".pay_active").val();
            sale_type_active  = $(".sale_type_active").val();
            pay_type_active = $(".pay_type_active").val();
            //get = "GET PAY:  "+pay_active +"  == PAY TYPE: "+ pay_type_active+" SALE TYPE:  "+sale_type_active; 
            get_payment = accounting.unformat($(".get_payment_num").val());
            ba_due_payment = accounting.unformat($(".balance_due_in_cart1").text());
            ba_due_payment_kh = accounting.unformat($(".balance_due_in_cart_kh").text());
            
            if(pay_active == "us"){
                if(get_payment > ba_due_payment){
                    pay_back_us = get_payment - ba_due_payment;
                    var nub_of_int = parseInt(pay_back_us);
                    var nub_of_float = pay_back_us - nub_of_int;
                    var remaining_us = pay_back_us.toFixed(2);
                    var remaining_kh = money_exchange_to_kh(pay_back_us);
                    var payback_us = nub_of_int;
                    var payback_kh = money_exchange_to_kh(nub_of_float);
                }else{
                    var remaining_us = 0;
                    var remaining_kh = 0;
                    var payback_us = 0;
                    var payback_kh = 0;

                }
            }else if(pay_active == "kh"){
                if(get_payment > ba_due_payment_kh){
                    pay_back_kh = get_payment - ba_due_payment_kh;
                    var remaining_us = money_exchange_to_us(pay_back_kh);
                    var remaining_kh = pay_back_kh;
                    var payback_us = 0;
                    var payback_kh = pay_back_kh;
                }else{
                    var remaining_us = 0;
                    var remaining_kh = 0;
                    var payback_us = 0;
                    var payback_kh = 0;

                }
            } 
            var alert_me = remaining_us+"=="+remaining_kh +"=="+payback_us+"=="+payback_kh;
              
            var formdata = {
                    payment_num: get_payment,
                    pay_type_active: pay_type_active,
                    pay_active: pay_active,
                    remaining_us: remaining_us,
                    remaining_kh: remaining_kh,
                    payback_us: payback_us,
                    payback_kh: payback_kh

                }; 
            event.preventDefault();
            
            add_all_session(formdata);   
            

    });


//
$("#search_item").autocomplete({

source : '{{ URL::route('search_item_ajax_search') }}',

minlenght :1,

autoFocuse : true,

select:function(e,ui){
    var formData = {
            item_id: ui.item.id,
            name: ui.item.value,
            sell_price: ui.item.sell_price
        };
    
        add_to_cart_item(formData);

        $("#search_item").val('');  
        return false;    
    }
});

// customer search 
$("#search_customer").autocomplete({

source : '{{ URL::route('search_customer_name_ajax') }}',

minlenght :1,

autoFocuse : true,

select:function(e,ui){
        // $('#loadid').val(ui.item.id);
       
        //$('#loan_form').submit(); 
        var formdata = {
            customer_id: ui.item.id,
            customer_name: ui.item.value,    
        }; 
        add_all_session(formdata);
        $("#search_customer").val('');  
        return false;      
}
});



    $(".dis_all_in_add_cart").change(function(){
        var discount = $(this).val();

        var formdata = {
                discount_globle: discount 
            }; 
           // total_function_foot(formdata);
                add_all_session(formdata);
                total_function_foot(formdata);
            
    });

    function add_all_session(formdata){
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var url_now = "{{route('pos_add_all_session_credit_sale')}}";
            $.ajax({
                type: "POST",
                url: url_now,
                dataType: 'json',
                data: formdata,
                success: function(result){
                   console.log(result);
                    var dis = formdata.discount_globle;
                    var client = formdata.customer_name;
                    var client_id = formdata.customer_id;
                    if(formdata.discount_globle){
                        $(".dis_all_in_add_cart").val(dis);
                    }
                    if(result.remaining_us){
                        $(".remaining_us").val(result.remaining_us);
                    }else{
                        $(".remaining_us").val("0");
                    }
                    if(result.remaining_kh){
                        $(".remaining_kh").val(result.remaining_kh);
                    }else{
                        $(".remaining_kh").val("0");
                    }
                    if(result.payback_us){
                        $(".payback_us").val(result.payback_us);
                    }else{
                        $(".payback_us").val("0");
                    }
                    if(result.payback_kh){
                        $(".payback_kh").val(result.payback_kh);
                    }else{
                        $(".payback_kh").val("0");
                    }
                    if(result.sale_type == "sale_by_cash"){
                        $(".show_sale_by_credit").css("display","none");
                        $(".payment_by_cash_hide").removeAttr("style");
                        $(".add_pay_me").removeAttr("style");
                    }else if(result.sale_type == "sale_by_credit"){
                        $(".show_sale_by_credit").removeAttr("style");
                        $(".payment_by_cash_hide").css("display","none");
                        $(".add_pay_me").css("display","none");
                    }
                        var num_p_us = 0;
                        var num_p_kh = 0;
                        var num_p_pay_type =$(".pay_active").val();
                        var num_p_pay_num = 0;
                        if(result.tran_cash){
                            var tran_text = "";
                            $.each(result.tran_cash ,function(i,field){
                                if(field.pay_active == "us"){
                                    num_p_us += Number(field.payment_num);
                                }else{
                                    num_p_kh +=  Number(money_exchange_to_us(field.payment_num)); 
                                }  
                                var url_pr = "{{ url('credit_sales') }}/"+i+"/remove_tran_cash";
                                tran_text += "<tr class='parent"+i+"'>";
                                //    tran_text += "<td><button class='btn btn-danger btn-mini remove_add_payment'value='"+i+"'><i class='icon-remove icon-white'></i></td>";
                                tran_text += "<td><a href='#remove_transtation_pay' class='btn btn-danger btn-mini remove_add_payment' rel='"+i+"'><i class='icon-remove icon-white' style='margin-top: 3px;margin-right: -5px;'></i></a></td>";
                                tran_text += "<td><span class='add_payment'>"+field.payment_num+"</span></td>";
                                tran_text += "<td><span class='add_payment_curren'>"+field.pay_type_active+"</span></td>";
                                tran_text += "<td><span class='add_payment_type'>"+field.pay_active+"</span></td>";
                                tran_text += "</tr>";
                                num_p_pay_type = field.pay_active;
                                num_p_pay_num = field.payment_num;
                            });
                            $(".add_pay_ment").html(tran_text);
                        } 
                    //}
                    $("#has_pay_tran").val(num_p_us + num_p_kh);
                   var get_data ={
                            has_pay_tran: num_p_us + num_p_kh,
                            currency_type: $(".pay_active").val(),
                            get_payment_data: $(".balance_due_in_cart").text()
                        };
                        get_payment_total(get_data);
                    // if(formdata.sale_type == "sale_by_cash"){
                    //     $("#sale_by_cash").addClass("active");
                    // }else if(formdata.sale_type == "sale_by_credit"){
                    //     $("#sale_by_credit").addClass("active");
                    // }
                    
                    if(formdata.customer_name){
                        $("#customer_name").text(client);
                        $("#customer_id").val(client_id);
                    } 

                    return false;
                },
                error: function(){} 	        
            });
    }

// change total function
    function total_function_foot(formdata){
        var deposit_setting = 0.3;
        var sale_type_active = $(".sale_type_active").val();
        var sub_total = accounting.unformat($(".total_sub_total").text());
        var dis = formdata.discount_globle;
        var tax_to =  accounting.unformat($(".total_tax_in_cart").text());
        if(dis == 0) {
            var totl= sub_total;
        }else{
            var totl= sub_total - (sub_total*(dis/100));
        }
       
        var total_dut = totl + tax_to;
        $(".balance_due_in_cart").text(accounting.formatMoney(total_dut));
        
        if(sale_type_active == "sale_by_credit"){
            var total_deposit = total_dut * deposit_setting;
            var total_credit_due = total_dut - total_deposit; 
            $(".total_deposit_fixed_in_cart").val(accounting.formatMoney(total_deposit));
            $(".total_credit_due_in_cart").val(accounting.formatMoney(total_credit_due));   
        }

        $(".balance_due_in_cart1").text(accounting.formatMoney(total_dut));

        var total_due_kh = money_exchange_to_kh(total_dut);
        $(".balance_due_in_cart_kh1").val(accounting.formatMoney(total_due_kh));
        var get_data ={
            has_pay_tran: $("#has_pay_tran").val(),
            currency_type: $(".pay_active").val(),
            get_payment_data: $(".balance_due_in_cart1").text() 
        };
        get_payment_total(get_data);
    } 
    

// endchange 
    function get_all_session(){
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        //    alert(formData.item_id);
         
            var url_now = "{{route('get_add_all_session_credit_sale')}}";
            $.ajax({
                
                type: "POST",
                url: url_now,
                dataType: 'json',
                // async: false,
                //data: formData1,
                success: function(result){
                    console.log(result);
                    if(result.customer_name) {
                        $('#customer_name').html(result.customer_name);
                        $('#customer_id').val(result.customer_id);
                    }
                    if(result.discount_globle){
                        $('.dis_all_in_add_cart').val(result.discount_globle);
                    }
                    if(result.sale_type == "sale_by_cash"){
                        $("#sale_by_cash").addClass("btn-primary sale_type_active");
                        $(".show_sale_by_credit").css("display","none");
                        $(".payment_by_cash_hide").removeAttr("style");
                        $(".add_pay_me").removeAttr("style");
                    }else if(result.sale_type == "sale_by_credit"){
                        $("#sale_by_credit").addClass("btn-primary sale_type_active");
                        $(".show_sale_by_credit").removeAttr("style");
                        $(".payment_by_cash_hide").css("display","none");
                        $(".add_pay_me").css("display","none");
                    }
                    
                    if(result.pay_type == "cash"){
                        $(".cash_pay").addClass("btn-primary pay_type_active");
                    }else if(result.pay_type == "credit_card"){
                        $(".credit_card_pay").addClass("btn-primary pay_type_active");
                    }else if(result.pay_type == "debit_card"){
                        $(".debit_card_pay").addClass("btn-primary pay_type_active");
                    }else if(result.pay_type == "check"){
                        $(".check_pay").addClass("btn-primar pay_type_activey");
                    }else if(result.pay_type == "gift_card"){
                        $(".gift_card_pay").addClass("btn-primary pay_type_active");
                    }else if(result.pay_type == "coupon"){
                        $(".coupon_pay").addClass("btn-primary pay_type_active");
                    }else if(result.pay_type == "store_account"){
                        $(".store_account").addClass("btn-primary pay_type_active");
                    }
                    
                    if(result.remaining_us){
                        $(".remaining_us").val(result.remaining_us);
                    }else{
                        $(".remaining_us").val("0");
                    }
                    if(result.remaining_kh){
                        $(".remaining_kh").val(result.remaining_kh);
                    }else{
                        $(".remaining_kh").val("0");
                    }
                    if(result.payback_us){
                        $(".payback_us").val(result.payback_us);
                    }else{
                        $(".payback_us").val("0");
                    }
                    if(result.payback_kh){
                        $(".payback_kh").val(result.payback_kh);
                    }else{
                        $(".payback_kh").val("0");
                    }

                   if(result.currency_type == "us"){
                         $(".get_pay_us").addClass("btn-success pay_active");
                   }else if(result.currency_type == "kh"){
                         $(".get_pay_kh").addClass("btn-success pay_active");
                   }  
                   var num_p_us = 0;
                   var num_p_kh = 0;
                   var num_p_pay_type =result.currency_type;
                   var num_p_pay_num = 0;
                   if(result.tran_cash){
                       var tran_text = "";
                       $.each(result.tran_cash ,function(i,field){
                         if(field.pay_active == "us"){
                            num_p_us += Number(field.payment_num);
                         }else{
                            num_p_kh +=  Number(money_exchange_to_us(field.payment_num)); 
                         }  
                          var url_pr = "{{ url('credit_sales') }}/"+i+"/remove_tran_cash";
                           tran_text += "<tr class='parent"+i+"'>";
                        //    tran_text += "<td><button class='btn btn-danger btn-mini remove_add_payment'value='"+i+"'><i class='icon-remove icon-white'></i></td>";
                           tran_text += "<td><a href='#remove_transtation_pay' class='btn btn-danger btn-mini remove_add_payment' rel='"+i+"'><i class='icon-remove icon-white' style='margin-top: 3px;margin-right: -5px;'></i></a></td>";
                           tran_text += "<td><span class='add_payment'>"+field.payment_num+"</span></td>";
                           tran_text += "<td><span class='add_payment_curren'>"+field.pay_type_active+"</span></td>";
                           tran_text += "<td><span class='add_payment_type'>"+field.pay_active+"</span></td>";
                           tran_text += "</tr>";
                           num_p_pay_type = field.pay_active;
                           num_p_pay_num = field.payment_num;
                       });
                       $(".add_pay_ment").html(tran_text);
                   } 
                  
                   $("#has_pay_tran").val(num_p_us + num_p_kh);
                   var get_data ={
                            has_pay_tran: num_p_us + num_p_kh,
                            currency_type: result.currency_type,
                            get_payment_data: $(".balance_due_in_cart").text()
                        };
                        get_payment_total(get_data);
                         
                },
                error: function(){} 	        
            });
    }

    // add card to table by on click item
    function add_to_cart_item(formData){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
           // alert(formData.item_id);
           var formData1 = {
                    item_id: formData.item_id,
                    name: formData.name,
                    sell_price: formData.sell_price
            };
            var url_now = "{{route('post_item_add_to_cart_ajax')}}";
            $.ajax({
                type: "POST",
                url: url_now,
                dataType: 'json',
                data: formData1,
                success: function(result){
                    //console.log(result);
                    // $(".scroller_style1").load("{{url('credit_sales')}}");
                    get_data_cart_item();
                    get_all_session();
                    return false;
                },
                error: function(){} 	        
            });

    }

  // update card card one by one  

 // get payment 
        function get_payment_total(get_data){
            
             var h_pay = get_data.has_pay_tran;
             var cur_cy = get_data.currency_type;
             var get_payment_data = accounting.unformat(get_data.get_payment_data);
             var total_of_pay1 = get_payment_data - h_pay;
             if(total_of_pay1 > 0 ){
                 var total_of_pay = total_of_pay1;
             }else{
                var total_of_pay = 0 ;
             }
             
             if(get_data.currency_type == "us"){
                $(".get_payment_num").val(accounting.formatMoney(total_of_pay));
             }else if(get_data.currency_type == "kh"){
                var total_due_kh = money_exchange_to_kh(total_of_pay);
                $(".get_payment_num").val(accounting.formatMoney(total_due_kh));
             }

             $(".balance_due_in_cart1").text(accounting.formatMoney(total_of_pay));
                var total_due_kh1 = money_exchange_to_kh(total_of_pay);
             $(".balance_due_in_cart_kh").text(accounting.formatMoney(total_due_kh1));

        }



 // $(document).ajaxComplete(function(event){
    // update qty 

            // $(".qty_in_add_cart").change(function(){
            $(document).on('change','.qty_in_add_cart',function(){
                //event.preventDefault();
                var parent = $(this).parents("tr");
                var formData = {
                    item_card_id: parent.find("#item_id_card").val(),
                    qty_in_add_cart: $(this).val() ,
                    dis_in_add_cart: parent.find(".dis_in_add_cart").val() 
                };
                    update_card_all(formData);
            });
    // update discount

            $(document).on('change','.dis_in_add_cart',function(){
               
                var parent = $(this).parents("tr");
                var formData = {
                    item_card_id: parent.find("#item_id_card").val(),
                    qty_in_add_cart: parent.find(".qty_in_add_cart").val(),
                    dis_in_add_cart: $(this).val() 
                };
                 update_card_all(formData);
                 
            });
    // remove item 
        $(document).on('click','.remove_item',function(event){
                var cart_id = $(this).attr('rel');
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                })
                event.preventDefault();
                var url_remove_sale = "{{route('credit_sales')}}/"+cart_id+"/remove_card_by_id";
                $.ajax({
                    type: "POST",
                    url: url_remove_sale,
                    success: function (data) {
                        get_data_cart_item();
                        get_all_session();
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
        });
   // end remove item 
        
    // remove transtation pay  
    $(document).on('click','.remove_add_payment',function(event){
                var cart_id = $(this).attr('rel');
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                })
                event.preventDefault();
                var url_remove_sale = "{{route('credit_sales')}}/"+cart_id+"/remove_tran_cash";
                $.ajax({
                    type: "POST",
                    url: url_remove_sale,
                    success: function (data) {
                        get_all_session();
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            });
   // end remove transtation pay  



    function update_card_all(formData){
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
           // alert(formData.item_id);
           var formData1 = {
                    qty: formData.qty_in_add_cart,
                    discount: formData.dis_in_add_cart
            };
            var item_card_id = formData.item_card_id;
            var url_now = "{{route('credit_sales')}}/"+item_card_id+"/update_card_by_id";
            $.ajax({
                type: "POST",
                url: url_now,
                dataType: 'json',
                data: formData1,
                success: function(result){
                    
                   // $(".scroller_style1").load("{{url('credit_sales')}}");
                   get_data_cart_item();
                   get_all_session();
                  return false;
                },
                error: function(){} 	        
            });
    }

       // show card table 
   function get_data_cart_item(){
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var url_now = "{{route('post_item_cart_ajax')}}";
            $.ajax({
                type: "POST",
                url: url_now,
                async: false,
                //data:  new FormData(this),
                dataType: 'json',
                //data: formData,
                // contentType: false,
                //cache: true,
                 //processData:false,
                success: function(data){
                    console.log(data);
                    var deposit_setting = 0.3;
                    var total_all_dis, item_list, total_tax, total_qty, total_due , dis_total ; 
                  item_list = '';
                   total_all_dis = 0; total_tax = 0 ; total_qty = 0 ; total_due = 0 ;  
                        $.each(data.con_cart ,function(i,field){
                            
                            
                            var id = field.id;
                            var url_one = "{{url('credit_sales')}}/"+id+"/remove_card_by_id";
                            var name = field.name;
                            var qty = field.quantity;
                            var price = field.price;
                            var discount = field.attributes.discount;
                            var to_tax = field.attributes.total_tax;
                            var sub_pric = field.quantity * field.price;
                            var price_dis = sub_pric * (discount/100 );
                            var subtotal = sub_pric - price_dis ;
                           
                            total_all_dis += subtotal; 
                            total_tax += to_tax * qty;   
                            //total deposit 
                            

                            item_list +='<tr>'; 
                            item_list +='<td><a href="#remove '+id+'" class="btn btn-danger btn-mini remove_item" rel="'+id+'"><i class="icon-trash icon-white"></i></a>   &nbsp; &nbsp;'+  name +'</td>';
                            item_list += '<td><input name="qty_in_add_cart" type="text"  class="span12 m-wrap qty_in_add_cart" value="'+qty+'" style="margin-bottom: 0px; width: 45px;" /></td>';
                            item_list += '<td>'+price.toFixed(2)+'<input type="hidden" id="item_id_card" value="'+id+'"/></td>';
                            item_list += '<td><input name="dis_in_add_cart" type="text"  class="span12 m-wrap dis_in_add_cart" value="'+discount+'" style="margin-bottom: 0px; width: 45px;"/></td>';
                            item_list += '<td><p class="total_in_add_cart">'+subtotal.toFixed(2)+'</p></td>';
                            item_list += '</tr>';
                        });
                        var dis_all = data.discount_globle;
                        dis_total = total_all_dis - (total_all_dis*(dis_all/100));
                        total_due = dis_total + total_tax ;
                       
                     $(".scroller_style1").html(item_list);
                     $(".totla_in_cart").text(accounting.formatMoney(data.carttotal));
                     $(".total_sub_total").text(accounting.formatMoney(total_all_dis));
                     $(".balance_due_in_cart").text(accounting.formatMoney(total_due));
                     $(".balance_due_in_cart1").text(accounting.formatMoney(total_due));
                    // $(".get_payment_num").val(accounting.formatMoney(total_due));
                     $(".total_tax_in_cart").text(accounting.formatMoney(total_tax));
                     var total_due_kh = money_exchange_to_kh(total_due);
                     $(".balance_due_in_cart_kh").text(accounting.formatMoney(total_due_kh));
                     $(".balance_due_in_cart_kh1").val(accounting.formatMoney(total_due_kh));
                   
                    if(data.sale_type == "sale_by_credit"){
                        var total_deposit = total_due * deposit_setting;
                        var total_credit_due = total_due - total_deposit; 
                        $(".total_deposit_fixed_in_cart").val(accounting.formatMoney(total_deposit));
                        $(".total_credit_due_in_cart").val(accounting.formatMoney(total_credit_due));   
                    }
                    // $(".totla_in_cart").html(total_all_dis); 
                },
                error: function(){} 	        
            });

    }

    $('.completed_payment').click(function(e){
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            
            var scroller_style1 = $(".scroller_style1").find("tr").length;
            var add_pay_ment = $(".add_pay_ment").find("tr").length;
            var customer_id = $("#customer_id").val();
            var staff_id = $("#staff_id").val();
            var sale_type_active = $(".sale_type_active").val();
            var duration_of_payment =  $(".duration_of_payment").val();
            var duration_of_payment_type =  $(".duration_of_payment_type").val();
            var date_request_for_pay =  $(".date_request_for_pay").val();
            var date_create_request =  $(".date_create_request").val();
            var get_payment_num =  $(".get_payment_num").val();
            var get_payment_num_letter  = $(".get_payment_num_letter").val();

            var total_deposit_fixed_in_cart =  $(".total_deposit_fixed_in_cart").val();
            var total_deposit_fixed_letter_in_cart =  $(".total_deposit_fixed_letter_in_cart").val();
            var total_credit_due_in_cart =  $(".total_credit_due_in_cart").val();
            var total_credit_due_letter_in_cart  = $(".total_credit_due_letter_in_cart").val();


           if(scroller_style1 == 0){
               alert("you don't any Item to Sale");
               return false;
           }
        if(sale_type_active == "sale_by_credit"){
           if(customer_id == 0){
                alert("System required Customer ");
               return false;
           }
        //    if(duration_of_payment == 0){
        //         alert("System required Duration Of Payment ");
        //        return false;
        //    }
           
           if(get_payment_num == 0){
                alert("System required Payment ");
               return false;
           }
           if(total_deposit_fixed_in_cart == 0){
                alert("System required Due Deposit ");
               return false;
           }
           if(total_credit_due_in_cart == 0){
                alert("System required Credit Due ");
               return false;
           }
           
           if(duration_of_payment == 0){
                alert("System required Duration Of Payment ");
               return false;
           }
           if(duration_of_payment_type == "none"){
                alert("System required Duration Of Payment Type ");
               return false;
           }

            if(date_request_for_pay == 0){
                alert("System required Date Request for Payment");
               return false;
            }
            if(date_create_request == 0){
                alert("System required Date Request for Payment");
               return false;
            }

            if(staff_id == "none"){
                alert("System required CO  for Payment");
               return false;
            }
           
        }
        if(sale_type_active == "sale_by_cash"){   
           if(add_pay_ment == 0) {
                alert("You are not yet add Payment!");
                return false;
           }
        }   
            var url_complete = "{{route('completed_sale')}}";  
            var payment_amount_change = $('.balance_due_in_cart_kh1').val();
            var total_payment = $('.totla_in_cart').text();
            var sub_payment_amount = $('.total_sub_total').text();
            var dute_payment_amount = $('.balance_due_in_cart').text();
            var has_pay_tran = $("#has_pay_tran").val();
            var comment = $('.sale_comment').val();
            var total_tax = $(".total_tax_in_cart").text();
            var total_tax = $(".total_tax_in_cart").text();
            var formData = {
                payment_amount_change : accounting.unformat(payment_amount_change),
                has_pay_tran : has_pay_tran,
                total_payment : accounting.unformat(total_payment),
                sub_payment_amount : accounting.unformat(sub_payment_amount),
                dute_payment_amount : accounting.unformat(dute_payment_amount),
                comment : comment,
                staff_id : staff_id,
                total_tax : accounting.unformat(total_tax),
                prices_total_num : accounting.unformat(get_payment_num),
                prices_totalword : $(".get_payment_num_letter").val(),
                deposit_fixed : accounting.unformat(total_deposit_fixed_in_cart),
                deposit_fixed_word : $(".total_deposit_fixed_letter_in_cart").val(),
                money_owne : accounting.unformat(total_credit_due_in_cart),
                money_owne_word : $(".total_credit_due_letter_in_cart").val(),
                duration_pay_money : duration_of_payment,
                duration_pay_money_type : duration_of_payment_type,
                date_for_payments : date_request_for_pay,
                date_create_request : date_create_request,
            };
            var url_now = "{{route('completed_sale')}}";
            $.ajax({
                type: "POST",
                url: url_now,
                dataType: 'json',
                data: formData,
                success: function(result){
                  var url_refresh = "{{route('credit_sales')}}/"+result.sale_id+"/invoice";
                  window.location.href = url_refresh;
                },
                error: function(){} 	        
            });
        });
        

</script>
@stop()