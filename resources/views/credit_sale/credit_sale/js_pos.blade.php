<script type="text/javascript" language="javascript">
	
	
	var submitting = false;		
			
	$(document).ready(function(){
		
		var hasSuggestions = false;
		$('.coupon_codes').tokenfield({
			tokens: [],
			autocomplete: {
		 		source: 'https://demo.phppointofsale.com/index.php/sales/search_coupons',
				delay: 100,
		 		autoFocus: true,
		 	 	minLength: 0,
		 		showAutocompleteOnFocus: false,
				create: function() {
				        $(this).data('ui-autocomplete')._renderItem = function (ul, item) {
       					 	return $("<li class='item-suggestions'></li>")
           			 .data("item.autocomplete", item)
           			 .append('<a class="suggest-item">'+

									'<div class="name">' +
									item.label +
									'</div>'
								)
         				 .appendTo(ul);
   					}
				},
				open: function() {
					hasSuggestions = true;
				},
        close: function() {
          hasSuggestions = false;
        }
			}
		});

		$('.coupon_codes').on("change", function() {
			$.post('https://demo.phppointofsale.com/index.php/sales/set_coupons',{
				coupons: $('.coupon_codes').tokenfield('getTokens'),
				},
				function(response)
				{
					$("#register_container").html(response);
				});
		});

		$('.coupon_codes').on('tokenfield:createtoken', function (event) {
		    var existingTokens = $(this).tokenfield('getTokens');
		    $.each(existingTokens, function(index, token) {
		        if (token.value === event.attrs.value)
						{
	            event.preventDefault();
						}
		    });

				var menu = $("#coupons-tokenfield").data("uiAutocomplete").menu.element,
		    focused = menu.find("li:has(a.ui-state-focus)");

				if(focused.length !== 1 || !hasSuggestions)
				{
					event.preventDefault();
				}
		});
		
		$("#ebt_voucher_no").bind('keyup change', function()
		{
			$.post('https://demo.phppointofsale.com/index.php/sales/set_ebt_voucher_no', {ebt_voucher_no: $(this).val()});
		});
		
		$("#ebt_auth_code").bind('keyup change', function()
		{
			$.post('https://demo.phppointofsale.com/index.php/sales/set_ebt_auth_code', {ebt_auth_code: $(this).val()});
		});
	
		$( "#keyboard_toggle" ).click(function(e) {
			e.preventDefault();
			$( "#keyboardhelp" ).toggle();
		});
		
    $.fn.editable.defaults.mode = 'popup';     

	$('.fullscreen').on('click',function (e) {
		e.preventDefault();
		salesRecvFullScreen();
		$.get('https://demo.phppointofsale.com/index.php/home/set_fullscreen/1');
	});

	$('.customer-giftcard-item').on('click',function (e) {
		e.preventDefault();
			$.post('https://demo.phppointofsale.com/index.php/sales/set_selected_payment', {payment: "Gift Card"});

			$('#payment_types').val("Gift Card");

			$('.select-payment').removeClass('active');
			$('a[data-payment="Gift Card"]').addClass('active');
			$("#amount_tendered").focus();

		$("#amount_tendered").val($(this).data('giftcard-number'));

	});


	$('.dismissfullscreen').on('click',function (e) {
		e.preventDefault();
		salesRecvDismissFullscren();
		$.get('https://demo.phppointofsale.com/index.php/home/set_fullscreen/0');
	});
	    
    $('.xeditable').editable({
    	validate: function(value) {
            if ($.isNumeric(value) == '' && $(this).data('validate-number')) {
					return "Only numbers are allowed";
            }
        },
    	success: function(response, newValue) {
			 last_focused_id = $(this).attr('id');
 			 $("#register_container").html(response);
		}
    });

    $('.xeditable').on('shown', function(e, editable) {

		$(this).closest('.table-responsive').css('overflow-x','hidden');

    	editable.input.postrender = function() {
				//Set timeout needed when calling price_to_change.editable('show') (Not sure why)
				setTimeout(function() {
	         editable.input.$input.select();
			}, 200);
	    };
	});
	
	$('.xeditable').on('hidden', function(e, editable) {
		$(this).closest('.table-responsive').css('overflow-x','auto');
	});

	 $('.xeditable').on('hidden', function(e, editable) {
		 last_focused_id = $(this).attr('id');
		 $('#'+last_focused_id).focus();
		 $('#'+last_focused_id).select();
	 });
	
				
				// Look up receipt form handling
		
		$('#look-up-receipt').on('shown.bs.modal', function() {
	          $('#sale_id').focus();
	    });
			
		$('.look-up-receipt-form').on('submit',function(e){
			e.preventDefault();

			$('.look-up-receipt-form').ajaxSubmit({
				success:function(response)
				{
					if(response.success)
					{
						window.location.href = 'https://demo.phppointofsale.com/index.php/sales/receipt/'+response.sale_id;
					}
					else
					{
						$('.look-up-receipt-error').html(response.message);
					}
				},
				dataType:'json'
			});
		});

		//Set Item tier after selection
		$('.item-tiers a').on('click',function(e){
			e.preventDefault();

			$('.selected-tier').html($(this).text());
			$.post('https://demo.phppointofsale.com/index.php/sales/set_tier_id', {tier_id: $(this).data('value')}, function(response)
			{
				$('.item-tiers').slideToggle("fast", function()
				{
					$("#register_container").html(response);					
				});
			});
		});

		//Slide Toggle item tier options
		$('.item-tier').on('click',function(e){
			e.preventDefault();
			$('.item-tiers').slideToggle("fast");
		});

		//Set Item tier after selection
		$('.select-sales-persons a').on('click',function(e){
			e.preventDefault();

			$('.selected-sales-person').html($(this).text());
			$.post('https://demo.phppointofsale.com/index.php/sales/set_sold_by_employee_id', {sold_by_employee_id: $(this).data('value')}, function()
			{
				$('.select-sales-persons').slideToggle("fast");
				$("#register_container").load('https://demo.phppointofsale.com/index.php/sales/reload');
			});
		});

		//Slide Toggle item tier options
		$('.select-sales-person').on('click',function(e){
			e.preventDefault();
			$('.select-sales-persons').slideToggle("fast");
		});
		
		checkPaymentTypes();
		
		$('#toggle_email_receipt').on('click',function(e) {
			e.preventDefault();
	        var checkBox = $("#email_receipt");
	        checkBox.prop("checked", !checkBox.prop("checked")).trigger("change");
	        $(this).toggleClass('checked');
		});
		
		$('#email_receipt').change(function(e) 
		{	
			e.preventDefault();
			$.post('https://demo.phppointofsale.com/index.php/sales/set_email_receipt', {email_receipt: $('#email_receipt').is(':checked') ? '1' : '0'});
		});
		
		$('#delivery').change(function(e) 
		{	
			e.preventDefault();
			$.post('https://demo.phppointofsale.com/index.php/sales/set_delivery', {delivery: $('#delivery').is(':checked') ? '1' : '0'}, function(response) {
				if(!$('#delivery').is(':checked'))
				{
					$("#register_container").html(response);
				}
			});			
		});

		// Customer form script
		$('#item,#customer').click(function()
		{
			$(this).attr('value','');
		});


		// if #mode is changed
		$('.change-mode').click(function(e){
			e.preventDefault();
			if ($(this).data('mode') == "store_account_payment") { // Hiding the category grid
				$('#show_hide_grid_wrapper, #category_item_selection_wrapper').fadeOut();
			}else { // otherwise, show the categories grid
				$('#show_hide_grid_wrapper, #show_grid').fadeIn();
				$('#hide_grid').fadeOut();
			}
			$.post('https://demo.phppointofsale.com/index.php/sales/change_mode', {mode: $(this).data('mode')}, function(response)
			{
				$("#register_container").html(response);
			});
		});
		

									if (last_focused_id && last_focused_id != 'item')
				{
					setTimeout(function(){$('#'+last_focused_id).focus(); $('#'+last_focused_id).select();}, 10);
				}
						$(document).off('focusin');
			$(document).focusin(function(event) 
			{
				last_focused_id = $(event.target).attr('id');
			});
			
			
			$(".pay_store_account_sale_form").ajaxForm({target: "#register_container"});
			
			$('#pay_or_unpay_all').click(function()
			{
				$("#register_container").load("https:\/\/demo.phppointofsale.com\/index.php\/sales\/toggle_pay_all_store_account");	
			});
			
			$( "#item" ).autocomplete({
		 		source: 'https://demo.phppointofsale.com/index.php/sales/item_search',
				delay: 150,
		 		autoFocus: false,
		 		minLength: 0,
		 		select: function( event, ui ) 
		 		{
					$( "#item" ).val(decodeHtml(ui.item.value)+'|FORCE_ITEM_ID|');
		 			$('#add_item_form').ajaxSubmit({target: "#register_container", beforeSubmit: salesBeforeSubmit, success: itemScannedSuccess});

		 		},
			}).data("ui-autocomplete")._renderItem = function (ul, item) {
         return $("<li class='item-suggestions'></li>")
             .data("item.autocomplete", item)
	           .append('<a class="suggest-item"><div class="item-image">' +
							'<img src="' + item.image + '" alt="">' +
						'</div>' +
						'<div class="details">' +
							'<div class="name">' + 
								item.label +
							'</div>' +
							'<span class="attributes">' + 'Category' + ' : <span class="value">' + (item.category ? item.category : "None") + '</span></span>' +
							(typeof item.quantity !== 'undefined' && item.quantity!==null ? '<span class="attributes">' + 'Qty.' + ' <span class="value">'+item.quantity + '</span></span>' : '' )+
							(item.attributes ? '<span class="attributes">' + 'Attributes' + ' : <span class="value">' +  item.attributes + '</span></span>' : '' ) +
							
						'</div>')
             .appendTo(ul);
		     };


			
		

			$( "#customer" ).autocomplete({
		 		source: 'https://demo.phppointofsale.com/index.php/sales/customer_search',
				delay: 150,
		 		autoFocus: false,
		 		minLength: 0,
		 		select: function( event, ui ) 
		 		{
		 			$.post('https://demo.phppointofsale.com/index.php/sales/select_customer', {customer: decodeHtml(ui.item.value) }, function(response)
					{
						$("#register_container").html(response);
					});
		 		},
			}).data("ui-autocomplete")._renderItem = function (ul, item) {
		         return $("<li class='customer-badge suggestions'></li>")
		             .data("item.autocomplete", item)
			           .append('<a class="suggest-item"><div class="avatar">' +
									'<img src="' + item.avatar + '" alt="">' +
								'</div>' +
								'<div class="details">' +
									'<div class="name">' + 
										item.label +
									'</div>' + 
									'<span class="email">' +
										item.subtitle + 
									'</span>' +
								'</div></a>')
		             .appendTo(ul);
		     };
	     
	     		
		$('#change_date_enable').is(':checked') ? $("#change_cart_date_picker").show() : $("#change_cart_date_picker").hide(); 

		$('#change_date_enable').click(function() {
			if( $(this).is(':checked')) {
				$("#change_cart_date_picker").show();
			} else {
				$("#change_cart_date_picker").hide();
			}
		});
		
		$('#comment').change(function() 
		{
			$.post('https://demo.phppointofsale.com/index.php/sales/set_comment', {comment: $('#comment').val()});
		});
						
		$('#show_comment_on_receipt').change(function() 
		{
			$.post('https://demo.phppointofsale.com/index.php/sales/set_comment_on_receipt', {show_comment_on_receipt:$('#show_comment_on_receipt').is(':checked') ? '1' : '0'});
		});
		
				
		date_time_picker_field($("#change_cart_date"), JS_DATE_FORMAT + " "+JS_TIME_FORMAT);
		
      $("#change_cart_date").on("dp.change", function(e) {
			$.post('https://demo.phppointofsale.com/index.php/sales/set_change_cart_date', {change_cart_date: $('#change_cart_date').val()});			
      });
		
		//Input change
		$("#change_cart_date").change(function(){
			$.post('https://demo.phppointofsale.com/index.php/sales/set_change_cart_date', {change_cart_date: $('#change_cart_date').val()});			
		});

		$('#change_date_enable').change(function() 
		{
			$.post('https://demo.phppointofsale.com/index.php/sales/set_change_date_enable', {change_date_enable: $('#change_date_enable').is(':checked') ? '1' : '0'});
		});
		

		$('.delete-item, .delete-payment, #delete_customer').click(function(event)
		{
			event.preventDefault();
			$("#register_container").load($(this).attr('href'));	
		});

		$('.delete-tax').click(function(event)
		{
			event.preventDefault();
			var $that = $(this);
			bootbox.confirm("Are you sure you want to delete this tax?", function(result)
			{
				if(result)
				{
					$("#register_container").load($that.attr('href'));	
				}
			});
		});

		
		$('#redeem_discount,#unredeem_discount').click(function(event)
		{
			event.preventDefault();
			$("#register_container").load($(this).attr('href'));	
		});

		//Layaway Sale
		$("#layaway_sale_button").click(function(e)
		{
			e.preventDefault();
			bootbox.confirm("Are you sure you want to suspend this sale?", function(result)
			{
				if(result)
				{
					$.post('https://demo.phppointofsale.com/index.php/sales/set_comment', {comment: $('#comment').val()}, function() {
														$("#register_container").load('https://demo.phppointofsale.com/index.php/sales/suspend');
											});
				}
			});
		});

		//Estimate Sale
		$("#estimate_sale_button").click(function(e)
		{
			e.preventDefault();
			bootbox.confirm("Are you sure you want to suspend this sale?", function(result)
			{
				if(result)
				{
					$.post('https://demo.phppointofsale.com/index.php/sales/set_comment', {comment: $('#comment').val()}, function() {
													$("#register_container").load('https://demo.phppointofsale.com/index.php/sales/suspend/2');
											});
				}
			});
		});
		
		$(".additional_suspend_button").click(function(e)
		{
			var suspend_index = $(this).data('suspend-index');
			e.preventDefault();
			bootbox.confirm("Are you sure you want to suspend this sale?", function(result)
			{
				if(result)
				{
					$.post('https://demo.phppointofsale.com/index.php/sales/set_comment', {comment: $('#comment').val()}, function() {
													$("#register_container").load('https://demo.phppointofsale.com/index.php/sales/suspend/'+suspend_index);
											});
				}
			});
		});

		//Cancel Sale
		$("#cancel_sale_button").click(function(e)
		{
			e.preventDefault();
			bootbox.confirm("Are you sure you want to clear this sale? All items will cleared.", function(result)
			{
				if (result)
				{
					$('#cancel_sale_form').ajaxSubmit({target: "#register_container", beforeSubmit: salesBeforeSubmit});
				}
			});
		});
		//Select Payment
		$('.select-payment').on('click mousedown',selectPayment);
		
				
				
					
					
					

		function selectPayment(e)
		{
			e.preventDefault();
			$.post('https://demo.phppointofsale.com/index.php/sales/set_selected_payment', {payment: $(this).data('payment')});
			$('#payment_types').val($(this).data('payment'));
						// start_cc_processing
			$('.select-payment').removeClass('active');
			$(this).addClass('active');
			$("#amount_tendered").focus();
			$("#amount_tendered").attr('placeholder','');

			checkPaymentTypes();
			
		}

		//Add payment to the sale 
		$("#add_payment_button").click(function(e)
		{
			e.preventDefault();

			$('#add_payment_form').ajaxSubmit({target: "#register_container", beforeSubmit: addPaymentSalesBeforeSubmit});
		});
		
		$('#select_customer_form').bind('keypress', function(e) {
			if(e.keyCode==13)
			{
				e.preventDefault();
	 			$('#select_customer_form').ajaxSubmit({target: "#register_container", beforeSubmit: salesBeforeSubmit});
			}
		});
		
		$('#add_item_form').ajaxForm({target: "#register_container", beforeSubmit: salesBeforeSubmit, success: itemScannedSuccess});
		
		$('#add_item_form').bind('keypress', function(e) {
			if(e.keyCode==13)
			{
				e.preventDefault();
	 			$('#add_item_form').ajaxSubmit({target: "#register_container", beforeSubmit: salesBeforeSubmit, success: itemScannedSuccess});
			}
		});
		
		//Add payment to the sale when hit enter on amount tendered input
		$('#amount_tendered').bind('keypress', function(e) {
			if(e.keyCode==13)
			{
				e.preventDefault();
				
				//Quick complete possible
				if ($("#finish_sale_alternate_button").is(":visible"))
				{
					$('#add_payment_form').ajaxSubmit({target: "#register_container", beforeSubmit: addPaymentSalesBeforeSubmit, complete: function()
					{
						$('#finish_sale_button').trigger('click');
					}});
				}
				else
				{
					$('#add_payment_form').ajaxSubmit({target: "#register_container", beforeSubmit: addPaymentSalesBeforeSubmit});
				}
			}
		});

		//Select all text in the input when input is clicked
		$("input:text, textarea").not(".description,#comment").click(function() {
			$(this).select();
		});
		
		// Finish Sale button
		$("#finish_sale_button").click(function(e)
		{
			e.preventDefault();
			
			var confirm_messages = [];
			
			//Prevent double submission of form
			$("#finish_sale_button").hide();
			$('#grid-loader').show();
			
			
										confirm_messages.push("Payments do NOT cover total, are you sure you want to continue?");
						
							confirm_messages.push("Are you sure you want to submit this sale? This cannot be undone.");
							
				if (confirm_messages.length)						
				{
					bootbox.confirm(confirm_messages.join("<br />"), function(result)
					{
						if (result)
						{
							finishSale();
						}
						else
						{
							//Bring back submit and unmask if fail to confirm
							$("#finish_sale_button").show();
							$('#grid-loader').hide();
						}
					});
				}
				else
				{
					finishSale();
				}
		});
				
				
			if ($("#payment_types").val() == "Gift Card")
			{
				$('#finish_sale_alternate_button').removeClass('hidden');
				$('#add_payment_button').addClass('hidden');
			}
			else if($("#payment_types").val() == "Points")
			{
				$('#finish_sale_alternate_button').addClass('hidden');
				$('#add_payment_button').removeClass('hidden');
			}
			else
			{
				if ((135.00 >=0 && $('#amount_tendered').val()>=135.00) || (135.00 <0 && $('#amount_tendered').val()<=135.00))
				{
					$('#finish_sale_alternate_button').removeClass('hidden');
					$('#add_payment_button').addClass('hidden');
				}
				else
				{
					$('#finish_sale_alternate_button').addClass('hidden');
					$('#add_payment_button').removeClass('hidden');
				}
			}
		
		
		$('#amount_tendered').on('input',function(){
			if ($("#payment_types").val() == "Gift Card")
			{
				$('#finish_sale_alternate_button').removeClass('hidden');
				$('#add_payment_button').addClass('hidden');
			}
			else if($("#payment_types").val() == "Points")
			{
				$('#finish_sale_alternate_button').addClass('hidden');
				$('#add_payment_button').removeClass('hidden');
			}
			else
			{
				if ((135.00 >=0 && $('#amount_tendered').val()>=135.00) || (135.00 < 0 && $('#amount_tendered').val()<=135.00))
				{
					$('#finish_sale_alternate_button').removeClass('hidden');
					$('#add_payment_button').addClass('hidden');
				}
				else
				{
					$('#finish_sale_alternate_button').addClass('hidden');
					$('#add_payment_button').removeClass('hidden');
				}
			}
			
		});

		$('#finish_sale_alternate_button').on('click',function(e){
			e.preventDefault();
			
			$('#add_payment_form').ajaxSubmit({target: "#register_container", beforeSubmit: salesBeforeSubmit, complete: function()
			{
				$('#finish_sale_button').trigger('click');
			}});
		});		
		
				
		
		
		function show_grid() {
			$("#category_item_selection_wrapper").promise().done(function(){
				$("#category_item_selection_wrapper").slideDown();
				$('.show-grid').addClass('hidden');
				$('.hide-grid').removeClass('hidden');
			});
		}
		
		function hide_grid() {
			$("#category_item_selection_wrapper").promise().done(function(){
				$("#category_item_selection_wrapper").slideUp();
				$('.hide-grid').addClass('hidden');
				$('.show-grid').removeClass('hidden');
			});
		}
		
		// Show or hide item grid
		$("#show_grid, .show-grid").on('click',function(e)
		{
			e.preventDefault();
		  if (!$(':animated').length) {
		  }
			
			show_grid();
		});
		
		$("#hide_grid,#hide_grid_top, .hide-grid").on('click', function(e)
		{
			e.preventDefault();
		  if (!$(':animated').length) {
		  }
			
			hide_grid();
		});
		
		
	
		// Save credit card info
		$('#save_credit_card_info').change(function() 
		{
			$.post('https://demo.phppointofsale.com/index.php/sales/set_save_credit_card_info', {save_credit_card_info:$('#save_credit_card_info').is(':checked') ? '1' : '0'});
		});

		// Use saved cc info
		$('#use_saved_cc_info').change(function() 
		{
			$.post('https://demo.phppointofsale.com/index.php/sales/set_use_saved_cc_info', {use_saved_cc_info:$('#use_saved_cc_info').is(':checked') ? '1' : '0'});
		});

		// Prompt for cc info (EMV integration only)
		$('#prompt_for_card').change(function() 
		{
			$.post('https://demo.phppointofsale.com/index.php/sales/set_prompt_for_card', {prompt_for_card:$('#prompt_for_card').is(':checked') ? '1' : '0'});
		});

		$('#ebt_voucher_toggle').change(function() 
		{
			if($('#ebt_voucher_toggle').is(':checked'))
			{
				$("#ebt_voucher").show();
			}
			else
			{
				$("#ebt_voucher").hide();				
			}
			
			$.post('https://demo.phppointofsale.com/index.php/sales/set_ebt_voucher', {ebt_voucher:$('#ebt_voucher_toggle').is(':checked') ? '1' : '0'});
		});
		
			      	$('.cart-number').html(1);
				  
		
	});
	// end of document ready


// Re-usable Functions 
	
	function checkPaymentTypes()
	{
		var paymentType = $("#payment_types").val();
		switch(paymentType) {
    case "Cash":
				$("#amount_tendered").val("135.00");
				$("#amount_tendered").attr('placeholder',"Enter Cash Amount");
        break;
    case "Check":
				$("#amount_tendered").val("135.00");
				$("#amount_tendered").attr('placeholder',"Enter Check Amount");
        break;
    case "Gift Card":
				$("#amount_tendered").val('');
				$("#amount_tendered").attr('placeholder',"Swipe\/Type gift card #");
									giftcard_swipe_field($("#amount_tendered"));
				        break;
		case "Debit Card":
				$("#amount_tendered").val("135.00");
				$("#amount_tendered").attr('placeholder',"Enter Debit Card Amount");
				break;
		case "Credit Card":
				$("#amount_tendered").val("135.00");
				$("#amount_tendered").attr('placeholder',"Enter Credit Card Amount");
				break;
		case "Store Account":
				$("#amount_tendered").val("135.00");
				$("#amount_tendered").attr('placeholder',"Enter Store Account Amount");
				break;
		case "Points":
				$("#amount_tendered").val("");
				$("#amount_tendered").attr('placeholder',"Enter Points Amount");
				break;
		case "EBT":
				$("#amount_tendered").val("0.00");
				$("#amount_tendered").attr('placeholder',"Enter EBT Amount");
				break;
		case "WIC":
				$("#amount_tendered").val("0.00");
				$("#amount_tendered").attr('placeholder',"Enter WIC Amount");
				break;
		case "EBT Cash":
				$("#amount_tendered").val("135.00");
				$("#amount_tendered").attr('placeholder',"Enter EBT Cash Amount");
				break;
    default:
				$("#amount_tendered").val("135.00");
				$("#amount_tendered").attr('placeholder',"Enter"+' '+paymentType+' '+"Amount");
		}
		
	}
	
	function salesBeforeSubmit(formData, jqForm, options)
	{
		if (submitting)
		{ 	
			return false; 
		}
		submitting = true;
		
					$('.cart-number').html(1);
				 $("#ajax-loader").show();
		 $("#add_payment_button").hide();
		$("#finish_sale_button").hide();
	}
	
	function addPaymentSalesBeforeSubmit(formData, jqForm, options)
	{
		if (submitting)
		{ 	
			return false; 
		}
		submitting = true;
		
		if (135.00 == 0 )
		{
			bootbox.confirm("You are adding a payment and there is not an amount due. Are you sure you want to add this payment?", function(result)
			{
				if(result)
				{
					$('#add_payment_form').ajaxSubmit({target: "#register_container"});
				}
			});
			
			submitting = false;
			//Prevent form form submitting
			return false;
		}
		
					$('.cart-number').html(1);
				 $("#ajax-loader").show();
		 $("#add_payment_button").hide();
		$("#finish_sale_button").hide();		
	}

	function itemScannedSuccess(responseText, statusText, xhr, $form)
	{
		setTimeout(function(){$('#item').focus();}, 10);
	}
	
	function finishSale()
	{
		if ($("#comment").val())
		{
			$.post('https://demo.phppointofsale.com/index.php/sales/set_comment', {comment: $('#comment').val()}, function()
			{
				$('#finish_sale_form').submit();						
			});						
		}
		else
		{
			$('#finish_sale_form').submit();						
		}
		
	}
	
		
	
		
	$("#exchange_to").change(function()
	{
		var rate = $(this).val();
		$.post('https://demo.phppointofsale.com/index.php/sales/exchange_to',{'rate': rate}, function(response)
		{
			$("#register_container").html(response);
		});
	});
	
	$("#delete_sale_button").click(function()
	{
		bootbox.confirm({
		    message: "Are you sure you want to void\/delete this sale?",
		    buttons: {
		        confirm: {
		            label: "Yes",
		            className: 'btn-primary'
		        },
		        cancel: {
		            label: "No",
		            className: 'btn-default'
		        }
		    },
		    callback: function (result) {
				if(result)
				{
					var post_data = [];
					
			    post_data.push({
					'name': 'sales_void_and_refund_credit_card', 
					'value': 0				 });

			    post_data.push({
					'name': 'sales_void_and_cancel_return', 
					'value': 0				 });

			    post_data.push({
					'name': 'do_delete', 
						'value': 1
				 });

			    post_data.push({
					'name': 'clear_sale', 
						'value': 1
				 	});
					
				 post_submit('https://demo.phppointofsale.com/index.php/sales/delete/', 'POST',post_data);
				}
		   }
	
	});
});



$("#customer_facing_display_link").click(function()
{
	$("#customer_facing_display_warning").hide();
	$.get("https:\/\/demo.phppointofsale.com\/index.php\/sales\/opened_customer_facing_display");
});
</script>
</div>
 
</div>

<script type="text/javascript">
$(document).ready(function()
{
				
	$(window).load(function()
	{
		setTimeout(function()
		{
				$('.dismissfullscreen').click();	
				
		}, 0);
	});	
	
			 	var current_category_id = null;
	var current_tag_id = null;
	
  var categories_stack = [{category_id: 0, name: "All"}];
  
  function updateBreadcrumbs(item_name)
  {
     var breadcrumbs = '';
     for(var k = 0; k< categories_stack.length;k++)
     {
       var category_name = categories_stack[k].name;
       var category_id = categories_stack[k].category_id;
   
       breadcrumbs += (k != 0 ? ' &raquo ' : '' )+'<a href="javascript:void(0);"class="category_breadcrumb_item" data-category_id = "'+category_id+'">'+category_name+"</a>";
     }
		 
		 if (typeof item_name != "undefined" && item_name)
		 {
		 	 breadcrumbs +=' &raquo '+item_name;
		 }
 
     $("#grid_breadcrumbs").html(breadcrumbs);		  
  }
  
  $(document).on('click', ".category_breadcrumb_item",function()
  {
      var clicked_category_id = $(this).data('category_id');      
      var categories_size = categories_stack.length;
      current_category_id = clicked_category_id;
      
      for(var k = 0; k< categories_size; k++)
      {
        var current_category = categories_stack[k]
        var category_id = current_category.category_id;
        
        if (category_id == clicked_category_id)
        {
          if (categories_stack[k+1] != undefined)
          {
            categories_stack.splice(k+1,categories_size - k - 1);
          }
          break;
        }
      }
      
      if (current_category_id != 0)
      {
        loadCategoriesAndItems(current_category_id,0);
      }
      else
      {
        loadTopCategories();
      }
  });
  
	function loadTopCategories()
	{    
		$('#grid-loader').show();
		$.get('https://demo.phppointofsale.com/index.php/sales/categories', function(json)
		{
			processCategoriesResult(json);
		}, 'json');	
	}
	
	function loadTags()
	{
		$('#grid-loader').show();
		$.get('https://demo.phppointofsale.com/index.php/sales/tags', function(json)
		{
			processTagsResult(json);
		}, 'json');	
	}
  
  function loadCategoriesAndItems(category_id, offset)
  {
    $('#grid-loader').show();
    current_category_id = category_id;
    //Get sub categories then items
    $.get('https://demo.phppointofsale.com/index.php/sales/categories_and_items/'+current_category_id+'/'+offset, function(json)
    {
        processCategoriesAndItemsResult(json);
    }, "json");
  }
	
	function loadCategoriesAndItemsUrl(category_id, url)
	{
    $('#grid-loader').show();
    current_category_id = category_id;
    //Get sub categories then items
    $.get(url, function(json)
    {
        processCategoriesAndItemsResult(json);
    }, "json");
	}
  
  function loadTagItems(tag_id, offset)
  {
     $('#grid-loader').show();
	  current_tag_id = tag_id;
     //Get sub categories then items
     $.get('https://demo.phppointofsale.com/index.php/sales/tag_items/'+tag_id+'/'+offset, function(json)
     {
         processTagItemsResult(json);
     }, "json");
  }
	
	function loadTagItemsUrl(tag_id, url)
	{
    $('#grid-loader').show();
 	 current_tag_id = tag_id;
    //Get sub categories then items
    $.get(url, function(json)
    {
        processTagItemsResult(json);
    }, "json");
	}

	$(document).on('click', ".pagination.categories a", function(event)
	{
		$('#grid-loader').show();
		event.preventDefault();	
		$.get($(this).attr('href'), function(json)
		{
			processCategoriesResult(json);
      
		}, "json");
	});
	
	$(document).on('click', ".pagination.tags a", function(event)
	{
		$('#grid-loader').show();
		event.preventDefault();
	
		$.get($(this).attr('href'), function(json)
		{
			processTagsResult(json);
      
		}, "json");
	});
  
	$(document).on('click', ".pagination.categoriesAndItems a", function(event)
	{
		$('#grid-loader').show();
		event.preventDefault();
		loadCategoriesAndItemsUrl(current_category_id, $(this).attr('href'));
	});
	 
 	$(document).on('click', ".pagination.items a", function(event)
 	{
 		$('#grid-loader').show();
 		event.preventDefault();
 	  loadTagItemsUrl(current_tag_id, $(this).attr('href'));
 	 });

	$('#category_item_selection_wrapper').on('click','.category_item.category', function(event)
	{
      event.preventDefault();
      current_category_id = $(this).data('category_id');
      var category_obj = {category_id: current_category_id, name: $(this).find('p').text()};
      categories_stack.push(category_obj);
      loadCategoriesAndItems($(this).data('category_id'), 0);
	});
	
	$('#category_item_selection_wrapper').on('click','.category_item.tag', function(event)
	{
      event.preventDefault();
		current_tag_id = $(this).data('tag_id');
      loadTagItems($(this).data('tag_id'), 0);
	});
	
	$('#category_item_selection_wrapper').on('click','#by_category', function(event)
	{
	 	current_category_id = null;
		current_tag_id = null;
		$("#grid_breadcrumbs").html('');
		$('.btn-grid').removeClass('active');
		$(this).addClass('active');
		categories_stack = [{category_id: 0, name: "All"}];
		loadTopCategories();
	});
	
	$('#category_item_selection_wrapper').on('click','#by_tag', function(event)
	{
	 	current_category_id = null;
		current_tag_id = null;
		$('.btn-grid').removeClass('active');
		$(this).addClass('active');
		$("#grid_breadcrumbs").html('');
		loadTags();
	});
	

	$('#category_item_selection_wrapper').on('click','.category_item.item', function(event)
	{		
		$('#grid-loader').show();
		event.preventDefault();
		
		var $that = $(this);
		if($(this).data('has-variations'))
		{
   	 $.getJSON('https://demo.phppointofsale.com/index.php/sales/item_variations/'+$(this).data('id'), function(json)
			{
				$("#category_item_selection").html('');
				$("#category_item_selection_wrapper .pagination").html('');
				
				if (current_category_id)
				{
		    	var back_button = $("<div/>").attr('id', 'back_to_category').attr('class', 'category_item register-holder no-image back-to-categories col-md-2 col-sm-3 col-xs-6 ').append('<p>&laquo; '+"Back"+'</p>');
				}
				else
				{
		    	var back_button = $("<div/>").attr('id', 'back_to_tag').attr('class', 'category_item register-holder no-image back-to-tags col-md-2 col-sm-3 col-xs-6 ').append('<p>&laquo; '+"Back"+'</p>');
					
				}
				
	    	$("#category_item_selection").append(back_button);
				
		    for(var k=0;k<json.length;k++)
				{
	     		var image_src = json[k].image_src;
	       	var prod_image = "";
	       	var image_class = "no-image";
	       	var item_parent_class = "";
	       	if (image_src != '' ) {
	       		var item_parent_class = "item_parent_class";
	        	 var prod_image = '<img src="'+image_src+'" alt="" />';
	         	var image_class = "";
	       	}
			
    
	       var item = $("<div/>").attr('data-has-variations',0).attr('class', 'category_item item col-md-2 register-holder ' + image_class + ' col-sm-3 col-xs-6  '+item_parent_class).attr('data-id', json[k].id).append(prod_image+'<p>'+json[k].name+'<br /> <span class="text-bold">'+(json[k].price ? '('+json[k].price+')' : '')+'</span></p>');
	       $("#category_item_selection").append(item);
				 
				 if (current_category_id)
				 {
					 updateBreadcrumbs($that.text());
				 }
			 }
			 
	 		$('#grid-loader').hide();
			 
			});
		}
		else
		{
			$.post('https://demo.phppointofsale.com/index.php/sales/add', {item: $(this).data('id')+"|FORCE_ITEM_ID|" }, function(response)
			{
				show_feedback('success', "You have successfully added item", "Success");				$('#grid-loader').hide();
				$("#register_container").html(response);
				$('.show-grid').addClass('hidden');
				$('.hide-grid').removeClass('hidden');
			});
		}
	});

	$("#category_item_selection_wrapper").on('click', '#back_to_categories', function(event)
	{ 
		$('#grid-loader').show();
		event.preventDefault();
    
    //Remove element from stack
    categories_stack.pop();
    
    //Get current last element
    var back_category = categories_stack[categories_stack.length - 1];
    
    if (back_category.category_id != 0)
    {
      loadCategoriesAndItems(back_category.category_id,0);
    }
    else 
    {
      loadTopCategories();
    }
  });
  
	$("#category_item_selection_wrapper").on('click', '#back_to_tags', function(event)
	{ 
		$('#grid-loader').show();
		event.preventDefault();
	   loadTags();
	});
	
	$("#category_item_selection_wrapper").on('click', '#back_to_tag', function(event)
	{ 
		$('#grid-loader').show();
		event.preventDefault();
		loadTagItems(current_tag_id,0);
		});

	$("#category_item_selection_wrapper").on('click', '#back_to_category', function(event)
	{ 
		$('#grid-loader').show();		
		event.preventDefault();
    
    //Get current last element
    var back_category = categories_stack[categories_stack.length - 1];
    
    if (back_category.category_id != 0)
    {
      loadCategoriesAndItems(back_category.category_id,0);
    }
    else 
    {
      loadTopCategories();
    }
	});
	
  

	function processCategoriesResult(json)
	{    
		$("#category_item_selection_wrapper .pagination").removeClass('categoriesAndItems').removeClass('tags').removeClass('items').addClass('categories');
		$("#category_item_selection_wrapper .pagination").html(json.pagination);
	
		$("#category_item_selection").html('');
	
		for(var k=0;k<json.categories.length;k++)
		{
			 var category_item = $("<div/>").attr('class', 'category_item category col-md-2 register-holder categories-holder col-sm-3 col-xs-6').css('background-color', json.categories[k].color).data('category_id',json.categories[k].id).append('<p> <i class="ion-ios-folder-outline"></i> '+json.categories[k].name+'</p>');
			 
			 if(json.categories[k].image_id)
			 {
				 category_item.css('background-color', 'white');
				 category_item.css('background-image', 'url(' + SITE_URL +'/app_files/view/' + json.categories[k].image_id +'?timestamp='+json.categories[k].image_timestamp+')');
			 }
			 
			$("#category_item_selection").append(category_item);
		}
    
    	updateBreadcrumbs();
		$('#grid-loader').hide();
	}
	
	function processTagsResult(json)
	{		
		$("#category_item_selection_wrapper .pagination").removeClass('categoriesAndItems').removeClass('categories').removeClass('items').addClass('tags');	
		$("#category_item_selection_wrapper .pagination").html(json.pagination);
	
		$("#category_item_selection").html('');
	
		for(var k=0;k<json.tags.length;k++)
		{
			 var tag_item = $("<div/>").attr('class', 'category_item tag col-md-2 register-holder tags-holder col-sm-3 col-xs-6').data('tag_id',json.tags[k].id).append('<p> <i class="ion-ios-pricetag-outline"></i> '+json.tags[k].name+'</p>');
			$("#category_item_selection").append(tag_item);
		}
    
		$('#grid-loader').hide();
	}
  
  function processCategoriesAndItemsResult(json)
  {	  
	 $("#category_item_selection").html('');
    var back_to_categories_button = $("<div/>").attr('id', 'back_to_categories').attr('class', 'category_item register-holder no-image back-to-categories col-md-2 col-sm-3 col-xs-6 ').append('<p>&laquo; '+"Back To Categories"+'</p>');
    $("#category_item_selection").append(back_to_categories_button);

    for(var k=0;k<json.categories_and_items.length;k++)
    {
		 if (json.categories_and_items[k].type == 'category')
		 {
       	var category_item = $("<div/>").attr('class', 'category_item category col-md-2 register-holder categories-holder col-sm-3 col-xs-6').css('background-color', json.categories_and_items[k].color).css('background-image', 'url(' + SITE_URL +'/app_files/view/'+ json.categories_and_items[k].image_id +'?timestamp='+json.categories_and_items[k].image_timestamp+')').data('category_id',json.categories_and_items[k].id).append('<p> <i class="ion-ios-folder-outline"></i> '+json.categories_and_items[k].name+'</p>');
      	$("#category_item_selection").append(category_item);
		 }
		 else if (json.categories_and_items[k].type == 'item')
		 {
	       var image_src = json.categories_and_items[k].image_src;
      	 var has_variations = json.categories_and_items[k].has_variations ? 1 : 0;
				 
	       var prod_image = "";
	       var image_class = "no-image";
	       var item_parent_class = "";
	       if (image_src != '' ) {
	         var item_parent_class = "item_parent_class";
	         var prod_image = '<img src="'+image_src+'" alt="" />';
	         var image_class = "";
	       }
      
	       var item = $("<div/>").attr('data-has-variations',has_variations).attr('class', 'category_item item col-md-2 register-holder ' + image_class + ' col-sm-3 col-xs-6  '+item_parent_class).attr('data-id', json.categories_and_items[k].id).append(prod_image+'<p>'+json.categories_and_items[k].name+'<br /> <span class="text-bold">'+(json.categories_and_items[k].price ? '('+json.categories_and_items[k].price+')' : '')+'</span></p>');
	       $("#category_item_selection").append(item);
		 	
		 }
	 }
	 	 
    $("#category_item_selection_wrapper .pagination").removeClass('categories').removeClass('tags').removeClass('items').addClass('categoriesAndItems');
    $("#category_item_selection_wrapper .pagination").html(json.pagination);

    updateBreadcrumbs();
    $('#grid-loader').hide();
    
  }
  
  function processTagItemsResult(json)
  {
 	 $("#category_item_selection").html('');
     var back_to_categories_button = $("<div/>").attr('id', 'back_to_tags').attr('class', 'category_item register-holder no-image back-to-categories col-md-2 col-sm-3 col-xs-6 ').append('<p>&laquo; '+"Back to tags"+'</p>');
     $("#category_item_selection").append(back_to_categories_button);

     for(var k=0;k<json.items.length;k++)
     {
 	       var image_src = json.items[k].image_src;
      	 var has_variations = json.items[k].has_variations ? 1 : 0;
 	       var prod_image = "";
 	       var image_class = "no-image";
 	       var item_parent_class = "";
 	       if (image_src != '' ) {
 	         var item_parent_class = "item_parent_class";
 	         var prod_image = '<img src="'+image_src+'" alt="" />';
 	         var image_class = "";
 	       }
      
 	       var item = $("<div/>").attr('data-has-variations',has_variations).attr('class', 'category_item item col-md-2 register-holder ' + image_class + ' col-sm-3 col-xs-6  '+item_parent_class).attr('data-id', json.items[k].id).append(prod_image+'<p>'+json.items[k].name+'<br /> <span class="text-bold">'+(json.items[k].price ? '('+json.items[k].price+')' : '')+'</span></p>');
 	       $("#category_item_selection").append(item);
		 	
 	 }
	 	 
     $("#category_item_selection_wrapper .pagination").removeClass('categories').removeClass('tags').removeClass('categoriesAndItems').addClass('items');
     $("#category_item_selection_wrapper .pagination").html(json.pagination);

     $('#grid-loader').hide();
  }
  
  	loadTopCategories();
	});

	var last_focused_id = null;
	
	setTimeout(function(){$('#item').focus();}, 10);
</script>


<script>
//Keyboard events...only want to load once
$(document).keyup(function(event)
{
	var mycode = event.keyCode;
	
	//tab
	if (mycode == 9)
	{
		var $tabbed_to = $(event.target);
		
		if ($tabbed_to.hasClass('xeditable'))
		{
			$tabbed_to.trigger('click').editable('show');
		}
	}

});

$(document).keydown(function(event)
{	
	var mycode = event.keyCode;
	
	//F2
	if (mycode == 113)
	{
		$("#item").focus();
		return;
	}

	//F4
	if (mycode == 115)
	{
		event.preventDefault();
		$("#finish_sale_alternate_button").click();
		$("#finish_sale_button").click();
		return;
	}

	//F7
	if (mycode == 118)
	{
		event.preventDefault();
		$("#amount_tendered").focus();
		$("#amount_tendered").select();
		return;
	}

	//ESC
	if (mycode == 27)
	{
		event.preventDefault();
		$("#cancel_sale_button").click();
		return;
	}	
	
	
});

</script>
