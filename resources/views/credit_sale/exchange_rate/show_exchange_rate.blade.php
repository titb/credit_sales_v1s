@extends('credit_sale.layout.master')

@section('contend')



   

                    @if ($message = Session::get('success'))

                        <div class="alert alert-success">

                            <p>{{ $message }}</p>

                        </div>

                    @endif

                     @if ($message = Session::get('keyerror'))

                                <div class="alert alert-danger">

                                    <p>{{ $message }}</p>

                                </div>

                     @endif



                    <div class="row-fluid">

                         <!-- block -->

                        <div class="block">

                            <div class="navbar navbar-inner block-header">

                            </div>

                            <div class="block-content collapse in">

                           		<center>

			                        <h3 class="cen_title text-center khmer_Moul">តារាងអត្រាប្តូរប្រាក់</h3> 

			                    	<div class="muted span3 pull-right" style="margin-bottom:5px;"><a href="{{ url('exchange_rate/create') }}" class="btn btn-success pull-right"><i class="icon-plus icon-white"></i> បង្កើតថ្មី</a></div>

			                    </center>

                            	<legend></legend>



                            <style>

                            	table.table.table-bordered thead tr th{
                            		text-align: center;
                            	}
                            	table.table.table-bordered tbody tr td{
                            		text-align: center;
                            	}

                            </style>



							  <div class="container">

							    <table class="table table-bordered">

							      <thead style="background: rgb(251, 205, 205);">

							        <tr>

							            <!-- <th>លេខសម្គាល់

							            <div>លេខសម្គាល់</div>

							            </th> -->

							            <th>ប្រាក់​ (គិតជា រៀល)

							            </th>

							            <th>ប្រាក់​ (គិតជា ដុល្លា)

							            </th>

							            <th>សកម្មភាព

							            </th>

							        </tr>

							      </thead>

							      <tbody>
							      	@foreach($data as $row)
								                <tr>

								                	<!-- <td>{{ $row->id }}</td> -->
								                	<td><a href="{{url('exchange_rate/'.$row->id.'/edit')}}">{{ number_format($row->real,2) }} រៀល</a></td>
								                	<td>{{ $row->us }} ដុល្លា</td>
								                	<td>

								                  		<a href="{{url('exchange_rate/'.$row->id.'/edit')}}" class="btn btn-primary">កែប្រែ</a>

								                  		<!-- {!! Form::open(['method' => 'post','url' => 'exchange_rate/'.$row->id.'/delete','style'=>'display:inline']) !!}

											            {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}

											        	{!! Form::close() !!}
								                	-->
								                	</td> 

								                </tr>

								    @endforeach
							      </tbody>

							    </table>

							  </div>



                            	

			    			</div>

                            </div>

                        </div>

                        <!-- /block -->

                    </div>

                        

@endsection