	
		var SITE_URL= "https://demo.phppointofsale.com/index.php";
		var BASE_URL= "https://demo.phppointofsale.com/";
		var CURRENT_URL = "https://demo.phppointofsale.com/index.php/items/view/-1?redirect=items&progression=1";
		var CURRENT_URL_RELATIVE = "items/view/-1?redirect=items&progression=1";
		var ENABLE_SOUNDS = false;
		var JS_DATE_FORMAT = "MM\/DD\/YYYY";
		var JS_TIME_FORMAT = "hh:mm a";
		var LOCALE =  "en";
		var MONEY_NUM_DECIMALS = 2;
		var IS_MOBILE = false;
		var ENABLE_QUICK_EDIT = false;
        var PER_PAGE = 20;
        
        
		 
		var SCREEN_WIDTH = $(window).width();
		var SCREEN_HEIGHT = $(window).height();
		COMMON_SUCCESS = "Success";
		COMMON_ERROR = "Error";
		
		bootbox.addLocale('ar', {
		    OK : 'حسنا',
		    CANCEL : 'إلغاء',
		    CONFIRM : 'تأكيد'			
		});
		
		bootbox.addLocale('km', {
		    OK :'យល់ព្រម',
		    CANCEL : 'បោះបង់',
		    CONFIRM : 'បញ្ជាក់ការ'			
		});
		bootbox.setLocale(LOCALE);		
		
		$.ajaxSetup ({
			cache: false,
			headers: { "cache-control": "no-cache" }
		});
		
		$(document).on('show.bs.modal','.bootbox.modal', function (e) 
		{
			var isShown = ($(".bootbox.modal").data('bs.modal') || {}).isShown;
			//If we have a dialog already don't open another one
			if (isShown)
			{
				//Cleanup the dialog(s) that was added to dom
				$('.bootbox.modal:not(:first)').remove();
				
				//Prevent double modal from showing up
				return e.preventDefault();
			}
		});
		
		
		toastr.options = {
		  "closeButton": true,
		  "debug": false,
		  "newestOnTop": false,
		  "progressBar": false,
		  "positionClass": "toast-top-right",
		  "preventDuplicates": false,
		  "onclick": null,
		  "showDuration": "300",
		  "hideDuration": "1000",
		  "timeOut": "5000",
		  "extendedTimeOut": "1000",
		  "showEasing": "swing",
		  "hideEasing": "linear",
		  "showMethod": "fadeIn",
		  "hideMethod": "fadeOut"
		}
		
    $.fn.editableform.buttons = 
      '<button tabindex="-1" type="submit" class="btn btn-primary btn-sm editable-submit">'+
        '<i class="icon ti-check"></i>'+
      '</button>'+
      '<button tabindex="-1" type="button" class="btn btn-default btn-sm editable-cancel">'+
        '<i class="icon ti-close"></i>'+
      '</button>';
	  
 	  $.fn.editable.defaults.emptytext = "Empty";
		
		$(document).ready(function()
		{
			
				$(".wrapper.mini-bar .left-bar").hover(
				   function() {
				     $(this).parent().removeClass('mini-bar');
				   }, function() {
				     $(this).parent().addClass('mini-bar');
				   }
				 );
			
			

	    $('.menu-bar').click(function(e){                  
	    	e.preventDefault();
	        $(".wrapper").toggleClass('mini-bar');        
	    }); 
    					
			//Ajax submit current location
			$(".set_employee_current_location_id").on('click',function(e)
			{
				e.preventDefault();

				var location_id = $(this).data('location-id');
				$.ajax({
				    type: 'POST',
				    url: 'https://demo.phppointofsale.com/index.php/home/set_employee_current_location_id',
				    data: { 
				        'employee_current_location_id': location_id, 
				    },
				    success: function(){
				    	window.location.reload(true);	
				    }
				});
				
			});
			
			$(".set_employee_language").on('click',function(e)
			{
				e.preventDefault();

				var language_id = $(this).data('language-id');
				$.ajax({
				    type: 'POST',
				    url: 'https://demo.phppointofsale.com/index.php/employees/set_language',
				    data: { 
				        'employee_language_id': language_id, 
				    },
				    success: function(){
				    	window.location.reload(true);	
				    }
				});
				
			});
			
	});
