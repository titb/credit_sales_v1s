@extends('credit_sale.layout.master')
@section('contend')

<div class="container-fluid">
            <div class="row-fluid">
                     <!-- validation -->
                    <div class="row-fluid">
                         <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                    <div class="muted pull-left"><a href="{!! url('suppliers') !!}"> អ្នកផ្គត់ផ្គង់</a> <span class="divider">|</span> បង្កើតអ្នកផ្គត់ផ្គង់ថ្មី </div>					
                            </div>

                            <div class="block-content collapse in">
                            @include('errors.error')	
                             	<center>
			                        <h3 class="cen_title text-center khmer_Moul">{{$title}}</h3>
			                    </center>
                            	<legend></legend>
                                <form role="form" id="form_insert_suppliers" name="form_insert_suppliers" method="POST"  enctype="multipart/form-data">
                                        {{ csrf_field() }}  
                                            <div class="span8"  style="margin: 0 auto !important;float: none;">
                                                    <div class="span12" style="margin-left:0;">

                                                        <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">​ ឈ្មោះ​ក្រុម​ហ៊ុន <span class="required" title="This place you must be put data">*</span></label>
                                                        <div class="controls">
                                                            <input type="text" id="company_name" class="span12 m-wrap company_name" name="company_name" data-required="1" autocomplete="off"/>
                                                        </div>
                                                      
                                                        <label class="control-label" >​ ឈ្មេាះ </label>
                                                        <div class="controls">
                                                            <input type="text" id="name" class="span12 m-wrap name" name="name" data-required="1" autocomplete="off"/>
                                                        </div>
                                                        <label class="control-label" >​ អ៊ីមែល: </label>
                                                        <div class="controls">
                                                            <input type="text" id="email" class="span12 m-wrap email" name="email" data-required="1" autocomplete="off"/>
                                                        </div>
                                                        <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">​ លេខទូរសព្ទ: <span class="required" title="This place you must be put data">*</span></label>
                                                        <div class="controls">
                                                            <input type="text" id="phone" class="span12 m-wrap phone" name="phone" data-required="1" autocomplete="off"/>
                                                        </div>
                                                        <label class="control-label"> ​ ជ្រើសរើសរូបភាព: </label>
                                                        <div class="controls">
                                                            <input type="file" name="images" class="input-file uniform_on" id="fileInput" multiple title="រូបភាពត្រូវតែជារូបភាពដែលថតចេញមកពីការបើកGPSថត"/>
                                                        
                                                            <p id="images_get"></p>
                                                        <br/>

                                                        </div>
                                                        <label class="control-label" >​ អាសយដ្ឋាន 1: </label>
                                                        <div class="controls">
                                                            <input type="text" id="address1" class="span12 m-wrap address1" name="address1" data-required="1" autocomplete="off"/>
                                                        </div>
                                                        <label class="control-label" >​ អាសយដ្ឋាន 2: </label>
                                                        <div class="controls">
                                                            <input type="text" id="address2" class="span12 m-wrap address2" name="address2" data-required="1" autocomplete="off"/>
                                                        </div>
                                                        <label class="control-label" >​ ទីក្រុង: </label>
                                                        <div class="controls">
                                                            <input type="text" id="city" class="span12 m-wrap city" name="city" data-required="1" autocomplete="off"/>
                                                        </div>
                                                        <label class="control-label" >​ រដ្ឋ / ខេត្ត: </label>
                                                        <div class="controls">
                                                            <input type="text" id="state_or_province" class="span12 m-wrap state_or_province" name="state_or_province" data-required="1" autocomplete="off"/>
                                                        </div>
                                                        <label class="control-label" >​ លេខ​កូដ​តំបន់: </label>
                                                        <div class="controls">
                                                            <input type="text" id="zip_code" class="span12 m-wrap zip_code" name="zip_code" data-required="1" autocomplete="off"/>
                                                        </div>
                                                        <label class="control-label" >​ ប្រទេស: </label>
                                                        <div class="controls">
                                                            <input type="text" id="country" class="span12 m-wrap country" name="country" data-required="1" autocomplete="off"/>
                                                        </div>

                                                    </div>
                                                    <div class="span12" style="margin-left:0;">
                                                        <label class="control-label" >​ សេចក្ដីពិពណ៌នា:</label>
                                                        <div class="controls">
                                                            <textarea name="description"  data-required="1" rows="5" class="span12 m-wrap description"></textarea>
                                                        </div>
                                                        <label class="control-label" >​ គណនី #:</label>
                                                        <div class="controls">
                                                            <input type="text" id="account_number" class="span12 m-wrap account_number" name="account_number" data-required="1" autocomplete="off"/>
                                                        </div>
                                                    </div>
                                                <?php 
                                                    if(isset($_REQUEST["redirect"])){
                                                        if($_REQUEST["redirect"] == "purchase_order"){
                                                            echo ' <input type="hidden" name="redirect"  value="'.$_REQUEST["redirect"].'" />';
                                                        } 
                                                    }
                                                
                                                ?>
                                                   
                                                    <div class="span12" style="margin-left:0;">
                                                        <br/>
                                                            <center>
                                                                    <button type="submit" class="btn btn-success tbn_add" id="btn-save" name="btn_save" value="add">បញ្ចូល</button>
                                                                    <input type="hidden" id="item_id" name="item_id" value="0">
                                                                    <button  class="btn btn-danger get_back">ត្រលប់</button>
                                                            </center>  
                                                        <br/>
                                                    
                                                    </div>
                                            </div>
                                        </form>
			    			</div>
                        <!-- Edit Supplier -->

						</div>
                     	<!-- /block -->
		    		</div>
                     <!-- /validation -->
                </div>

            </div>

<meta name="_token" content="{{ csrf_token() }}" />

<script type="text/javascript">
$(document).ready(function(){

        @if(Request::is('suppliers/create'))
		    var url = "{{ route('suppliers/create') }}";
		@else
            var sup_id ="{{$data_id}}";
		    var url = "{{ url('suppliers') }}";
            var url_index1 = url+"/"+sup_id+"/json_edit";
                $.ajax({
                        type: "GET",
                        url: url_index1, 
                        dataType: "json",
                       // data: forData,
                        // async: false,
                        success: function(result){
                            console.log(result);
                            // $.each(result, function(i, field){
                            //     $(".company_name").val(field.company_name);   
                            // }); 
                            $(".company_name").val(result.company_name); 
                            $(".name").val(result.name);
                            $(".email").val(result.email);    
                            $(".phone").val(result.phone);
                            $(".address1").val(result.address1);
                            $(".address2").val(result.address2);
                            $(".city").val(result.city);
                            $(".state_or_province").val(result.state_or_province);
                            $(".zip_code").val(result.zip_code);
                            $(".country").val(result.country);
                            $(".description").val(result.description);
                            $(".account_number").val(result.account_number);
                          var im = result.images;
                          var url_image = "{{url('/')}}";	
                            if(im !== null){
                            
                                $('#images_get').html("<br/><img class='client_upload_image_respon' > ");
								$(".client_upload_image_respon").attr("src",url_image+"/Account/images/"+im);
						    } 

                             $('#item_id').val(result.id);
                             $('#btn-save').val("edit");

														     
                        },
                        error: function (result ,status, xhr) {
                            console.log(result.responseText);
                        }
                    
                    });
                   
		@endif 



        $('#btn-save').click(function(e){

         
                $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        }
                    })
      
                    e.preventDefault();
                    var btn_id = $(this).val();
                      
                   if(btn_id == "edit"){
                       var item_id = $('#item_id').val();
                       var url = "{{ url('suppliers') }}";
                        var url_index = url+"/"+item_id+"/edit";
                   }else{
                       var url_index = "{{ route('suppliers/create') }}";  
                       var item_id = 0;
                   }
                    var form = document.forms.namedItem("form_insert_suppliers"); 
                    var formData = new FormData(form); 
              
                    $.ajax({
                        type:'POST',
                        url: url_index,
                        dataType: 'json',
                        contentType: false,
                        data: formData,
                        processData: false,
                        success: function(result){
                            
                            if(result.redirect == "purchase_order"){
                                var post_url = "{{ url('pos_add_all_session_purchas_order') }}";
                                   var done = function() {
                                            window.location.href = "{{url('credit_sales_purchas_order')}}";
                                    }; 
                                    var formdata = {
                                        supplier_id: result.datas.id,
                                        supplier_name: result.datas.name   
                                    }; 
                                    $.post(post_url, formdata , done);
                            }else{
                                if(btn_id == "add"){

                                    window.scrollTo(0, 0);
                                    $("#item_id").val(0);
                                    $("#btn-save").val("add");
                                    $('#form_insert_suppliers').trigger("reset");
                                    $('.msg_show').html(result.msg_show); 
                                    $('.filename').text("No file selected");
                                }else{
                                    window.location="{{url('suppliers')}}";   
                                }
                            }
                        },
                        error: function (result ,status, xhr) {
                            console.log(result.responseText);
                        }                   
                    });
            
        }); 


        // var submitting = false;
        // function hashDiff(h1, h2) {
        //     var d = {};
        //     for (k in h2) {
        //         if (h1[k] !== h2[k]) d[k] = h2[k];
        //     }
        //     return d;
        // }

        // function convertSerializedArrayToHash(a) { 
        //     var r = {}; 
        //     for (var i = 0;i<a.length;i++) {
        //             if(a[i].name !== 'btn_save')
        //             {
        //                 r[a[i].name] += a[i].value;
        //             }
        //     }
        //     return r;
        // }
        // var $form = $('#form_insert_suppliers').eq(0);
        // var startItems;
        // $(document).ready(function() {		
        //     if(!startItems)
        //     {
        //         startItems = convertSerializedArrayToHash($form.serializeArray());
        //     }
        // });

            // function item_save_confirm_dialog(args) {
	
            //         bootbox.confirm({
            //             message: "What would you like to do next?",
            //             buttons: {
            //                 confirm: {
            //                     label: args.confirm.label,
            //                     className: 'btn-primary'
            //                 },
            //                 cancel: {
            //                     label: args.cancel.label,
            //                                 className: 'btn-default'
                                
            //                 }
            //             },
            //             callback: function (result) {
            //                     if(result)
            //                     {
                                    
            //                         var done = function() {
            //                                 window.location.href = args.confirm.redirect;
            //                         };
                                    
            //                         if(args.confirm.post_url)
            //                         {
            //                             $.post(args.confirm.post_url, {item: args.id}, done);
            //                         } else {
            //                             done();
            //                         }
                                    
            //                     } else {
            //                         if(args.cancel.redirect)
            //                         {
            //                             window.location.href = args.cancel.redirect.replace(/-1/g, args.id);
            //                         }
                                    
            //                         if(args.reload)
            //                         {
            //                             window.location.reload();
            //                         }
            //                     }
            //             }
            //         });
            //     }	     
 });       
</script>            
@stop()
