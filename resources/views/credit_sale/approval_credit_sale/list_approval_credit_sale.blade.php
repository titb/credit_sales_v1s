@extends('credit_sale.layout.master')
@section('contend')
<style>
ul.typeahead.dropdown-menu {
    margin-top: -10px;
}
</style>
<div class="container-fluid">
            <div class="row-fluid">
                     <!-- validation -->
                    <div class="row-fluid">
                         <!-- block -->
                        <div class="block">
                          <div class="navbar navbar-inner block-header">
                                     <!-- <?php $items = App\Cs_Sale::with(['cs_client','cs_request_form'])->where('method','=','sale_by_credit')->where('deleted','=',1)->get();?>
                                    <input type="text" name="item_search" placeholder="Name" data-required="1" class="span3 m-wrap item_search" style="margin-top:5px;" id="typeahead" data-provide="typeahead" data-source='[@foreach($items as $row)"{{$row->name}}",@endforeach""]' autocomplete="off">
                                    <button type="submit" class="btn btn-primary b_search" id="b_search" name="submit_search" style="margin-top: -5px;" value="b_search">Search</button> -->		
                                    <center>
                                        <h3 class="cen_title text-center khmer_Moul">ការអនុម័តឥណទាន</h3>
                                    </center>
                            </div>  

                            <div class="block-content collapse in">
                            <div class="span12"  style="margin: 0 auto !important;float: none;">								
									<ul class="nav nav-tabs">
                                         <li class="tab_approve @if($tab_approve == 'waiting') active get_active @endif" title="waiting"><a href="{!! url('aprove_credit_sales?redirect=waiting') !!}">រងចាំការអនុម័ត</a></li>
                                         <li class="tab_approve @if($tab_approve == 'agree') active get_active @endif"  title="agree"><a href="{!! url('aprove_credit_sales?redirect=agree') !!}">បានអនុម័ត</a></li>
                                         <li class="tab_approve @if($tab_approve == 'not_yet_agree') active  get_active @endif" title="not_yet_agree"><a href="{!! url('aprove_credit_sales?redirect=not_yet_agree') !!}">ការអនុម័តនៅក្នុងការពិភាក្សា</a></li>
                                         <li class="tab_approve @if($tab_approve == 'not_agree') active get_active @endif" title="not_agree"><a href="{!! url('aprove_credit_sales?redirect=not_agree') !!}">មិនអនុម័ត</a></li>
                                    </ul>	
							</div>
                            @include('errors.error')
                            	 <!-- <legend></legend> -->
                                <input type="text" name="item_search" placeholder="ឈ្មោះអតិថិជន " data-required="1" class="span3 m-wrap item_search" style="margin-top:-10px;" id="typeahead" data-provide="typeahead" data-source='[@foreach($items as $row)"{{$row->cs_client->kh_username}}","{{$row->cs_client->en_username}}","{{$row->id}}",@endforeach""]' autocomplete="off">
                                
                                <button type="submit" class="btn btn-primary b_search" id="b_search" name="submit_search" style="margin-top: -21px;" value="b_search">ស្វែងរក</button>
							    <table class="table table-bordered">

							      <thead style="background: rgb(251, 205, 205);">

							        <tr class="header">

							            <th>ល.រ</th>
							            <th>សំណើ​រ ល.រ</th>
							            <th>ឈ្មេាះអតិថិជន</th>
							            <th>ចំនួនទំនិញ</th>
                                        <th>តម្លៃសរុប</th>
                                        <th>ការបរិច្ឆេតស្នើសុំ</th>
                                        <th>ស្ថានភាព</th>
                                        <th>សកម្មភាព</th>

							        </tr>
							      </thead>
							      <tbody class="item_list">
                                       
                                  </tbody>
                                    <tr>
                                        <td colspan="7">
                                            <b class="pull-right">សរុប:</b>
                                        </td>
                                        <td>
                                            <b id="total_all"></b>
                                        </td>
                                    </tr>
							    </table>
                            	<!-- Pagination -->
								<div class="pagination text-right"></div>
			    			</div>
						</div>
                     	<!-- /block -->
		    		</div>
                     <!-- /validation -->
                </div>

            </div>

<meta name="_token" content="{{ csrf_token() }}" />
<p id="model_in"></p>

<script>
$(document).ready(function(){
     
    $(window).load(function(){
        var numpage = 1;
		var url_index1 = "{{route('get_sale_by_wating_credit_json')}}";
		get_page(url_index1,numpage);
    });

    
                    $(".b_search").click(function(){
                        var submit_search = $(this).val();
                        var n = 1;
                        
                        var url_index2 = submit_search; 		
                        get_page(url_index2,numpage = n);
                    });
                    
                    $(document).on('click','.pag',function(){
                        var url_index1 = "{{route('get_sale_by_wating_credit_json')}}";
                        var numpage = $(this).text();   
                        get_page(url_index1,numpage);
                    });
                    $(document).on('click','.pre',function(){ 
                        var url_index1 = "{{route('get_sale_by_wating_credit_json')}}";
                        var numpage = $(this).find(".pre_in").val();
                        get_page(url_index1,numpage);
                    });
                    $(document).on('click','.btn_deleted',function(e){   
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                            }
                        })
                            var item_id = $(this).val();
                            var tac =  $(".get_active").attr("title");  
                        if(tac != "waiting"){ 
                            var url1 = "{{ route('aprove_credit_sales') }}";
                            var url_index = url1+"/"+item_id+"/deleted";
                        }else{
                            var url1 = "{{ route('credit_sales') }}";
                            var url_index = url1+"/"+item_id+"/delete_database";
                        }
                            e.preventDefault();
                            $.ajax({
                                type: "POST",
                                url: url_index, 
                                dataType: "json",
                                success: function(result){
                                    $('.msg_show').html(result.msg_show); 
                                            var numpage = 1 ;
                                            var url_index1 = "{{ route('get_sale_by_wating_credit_json') }}";
                                            get_page(url_index1,numpage);
                                },
                                error: function (result ,status, xhr) {
                                    console.log(result.responseText);
                                }                   
                            });
                    });

                    $(".tab_approve").click(function(e){ 
                        $(".tab_approve").removeClass("active get_active");
                        $(this).addClass("active get_active"); 
                        var add_session = $(this).attr("title");
                            $.ajaxSetup({
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                                }
                            });
                                
                                var url1 = "{{ route('add_session_new') }}";
                                e.preventDefault();
                                $.ajax({
                                    type: "POST",
                                    url: url1, 
                                    data:{tab_approve : add_session},
                                    dataType: "json",
                                    success: function(data){
                                        console.log(data);
                                    // $('.msg_show').html(result.msg_show); 
                                                var numpage = 1 ;
                                                var url_index1 = "{{ route('get_sale_by_wating_credit_json') }}";
                                                get_page(url_index1,numpage);
                                    },
                                    error: function (data ,status, xhr) {
                                        console.log(data.responseText);
                                    }                   
                                });
                     });
        function get_page(url,n){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                })
                var tab_approve =  $(".get_active").attr("title");  
                var item_search = $(".item_search").val(); 
                //alert(item_search);
                if(url === "b_search" || item_search !== ""){
                    var url_i = "{{route('get_sale_by_wating_credit_json')}}";
                    var forData = {
                                    item_search: $(".item_search").val(),
                                    submit_search: $(".b_search").val(),
                                    tab_approve: tab_approve
                                }                
                    var url_index = url_i+"?item_search="+item_search+"&submit_search=b_search&page="+n;     
                              
                }else{
                    var forData = {
                                    tab_approve: tab_approve
                                };
                    var url_index = url+"?page="+n;
                }    
			    var client;
                    $.ajax({
                            type: "GET",
                            url: url_index, 
                            dataType: "json",
                            data: forData,
                            // async: false,
                            success: function(result){ 
                                console.log(result);
                                $("#total_all").text(result.total);
                                if(result.total > 0){
                                        $.each(result.data, function(i, field){
                                                var il = result.from  + i;  

                                                
                                                var url_create = "{{url('aprove_credit_sales')}}/"+field.id+"/create"; 
                                                var url_show = "{{url('aprove_credit_sales')}}/"+field.id+"/show"; 
                                         var tac =  $(".get_active").attr("title");  
                                           if(tac != "waiting"){ 
                                                var url_edit = "{{url('aprove_credit_sales')}}/"+field.id+"/edit";     
                                                var show_approve =  "<a href='"+url_show+"' class='btn btn-info btn_show' id='btn_show'>លម្អិត</a>  "; 
                                           }else{
                                                var url_edit = "{{url('credit_sales')}}/"+field.id+"/edit"; 
                                                var show_approve =  "<a href='"+url_create+"' class='btn btn-success btn_create' id='btn_create'>ការអនុម័ត</a>  " ;     
                                            }   
                                                //var                                      
                                                client += "<tr>";
                                                    client += "<td>"+ il +"</td>";
                                                    client += "<td>"+ field.cs_request_form.id +"</td>";
                                                    client += "<td>"+ field.cs_client.kh_username +"</td>";
                                                    client += "<td> "+ field.cs_request_form.total_qty +"</td>";
                                                    client += "<td> "+ field.cs_request_form.prices_total_num +"</td>";
                                                    client += "<td> "+ field.cs_request_form.date_create_request +"</td>";
                                                    client += "<td> "+ field.method +"</td>";
                                                    client += "<td>";
                                                    client += show_approve +" ";
                                                    if(tac != 'waiting'){
                                                        client += "<a href='"+url_edit+"' class='btn btn-primary btn_edit' id='btn_edit'>កែប្រែ</a>  ";
                                                    }
                                                    client += "<button  class='btn btn-danger btn_deleted'  value='"+ field.id +"'>លុប</button>";
                                                    client += "</td>";
                                                client += "</tr>";
                                        });
                                    $(".item_list").html(client);	
                                    var page = "";
                                    if(result.prev_page_url === null){
                                        var pr_url = result.current_page;
                                    }else{
                                        var pr_url = result.current_page -1;
                                    }
                                    page += "<a class='pre'>&laquo;<input type='hidden' class='pre_in' value='"+pr_url+"' ></a>";
                                    for(var x = 1; x <= result.last_page; x ++  ) {
                                        if(result.current_page === x){
                                            page += "<a class='pag active' >"+x+"</a>";
                                        }else{
                                            page += "<a class='pag' >"+x+"</a>";
                                        }
                                    }
                                    if(result.next_page_url === null){
                                        var ne_url = result.current_page;
                                    }else{
                                        var ne_url = result.current_page +1;
                                    }
                                    page += "<a class='pre'>&raquo;<input type='hidden' class='pre_in' value='"+ne_url+"' ></a>";
                                    $(".pagination").html(page );
                            }else{
                                var client1 = "<tr><td colspan='8'>  No Record  </td></tr>";
                                $(".item_list").html(client1);
                            }

                        },
                        error: function (result ,status, xhr) {
                            console.log(result.responseText);
                        }
                    
                    });
                   
		}    
        
 
 });       
</script>            
@stop()
