
<html>
   <head>
      <meta http-equiv=Content-Type content="text/html; charset=utf-8">
      <meta name=ProgId content=Word.Document>
      <meta name=Generator content="Microsoft Word 15">
      <meta name=Originator content="Microsoft Word 15">

      <title>កិច្ចសន្យាទិញបង់រំលស់</title>
      <link href="https://fonts.googleapis.com/css?family=Battambang|Moul&display=swap" rel="stylesheet">
      <script src="{{ url('assets/jquery.min.js')}}"></script>
      <script src="{{ URL::to('assets/accounting.min.js')}}"></script>
      <style>
    
        /* Font Definitions */
        @font-face
        {font-family:Wingdings;
        panose-1:5 0 0 0 0 0 0 0 0 0;
        mso-font-charset:2;
        mso-generic-font-family:auto;
        mso-font-pitch:variable;
        mso-font-signature:0 268435456 0 0 -2147483648 0;}
        @font-face
        {font-family:"Cambria Math";
        panose-1:2 4 5 3 5 4 6 3 2 4;
        mso-font-charset:0;
        mso-generic-font-family:roman;
        mso-font-pitch:variable;
        mso-font-signature:-536869121 1107305727 33554432 0 415 0;}
        @font-face
        {font-family:"Battambang";
        panose-1:2 0 5 0 0 0 0 2 0 4;
        mso-font-charset:0;
        mso-generic-font-family:auto;
        mso-font-pitch:variable;
        mso-font-signature:-1610612497 1342185546 65536 0 273 0;}
        @font-face
        {font-family:"Battambang";
        panose-1:2 0 5 0 0 0 0 2 0 4;
        mso-font-charset:0;
        mso-generic-font-family:auto;
        mso-font-pitch:variable;
        mso-font-signature:-1610612497 1342185546 65536 0 273 0;}
        @font-face
        {font-family:"Limon S1";
        panose-1:0 0 0 0 0 0 0 0 0 0;
        mso-font-charset:0;
        mso-generic-font-family:auto;
        mso-font-pitch:variable;
        mso-font-signature:3 0 0 0 1 0;}
        @font-face
        {font-family:"Limon S6";
        mso-font-alt:Calibri;
        mso-font-charset:0;
        mso-generic-font-family:auto;
        mso-font-pitch:variable;
        mso-font-signature:3 0 0 0 1 0;}
        @font-face
        {font-family:Tahoma;
        panose-1:2 11 6 4 3 5 4 4 2 4;
        mso-font-charset:0;
        mso-generic-font-family:swiss;
        mso-font-pitch:variable;
        mso-font-signature:-520081665 -1073717157 41 0 66047 0;}
        @font-face
        {font-family:"Moul";
        panose-1:2 0 5 0 0 0 0 2 0 4;
        mso-font-charset:0;
        mso-generic-font-family:auto;
        mso-font-pitch:variable;
        mso-font-signature:-1593824529 1342185546 65536 0 66047 0;}
        @font-face
        {font-family:"Moul";
        panose-1:2 0 5 0 0 0 0 2 0 4;
        mso-font-charset:0;
        mso-generic-font-family:auto;
        mso-font-pitch:variable;
        mso-font-signature:-1610612497 1342185546 65536 0 273 0;}
        /* Style Definitions */
        p.MsoNormal, li.MsoNormal, div.MsoNormal
        {mso-style-unhide:no;
        mso-style-qformat:yes;
        mso-style-parent:"";
        margin:0in;
        margin-bottom:.0001pt;
        mso-pagination:widow-orphan;
        font-size:10.0pt;
        font-family:"Times New Roman",serif;
        mso-fareast-font-family:"Times New Roman";
        mso-fareast-language:EN-US;
        mso-bidi-language:AR-SA;}
        h1
        {mso-style-unhide:no;
        mso-style-qformat:yes;
        mso-style-link:"Heading 1 Char";
        mso-style-next:Normal;
        margin:0in;
        margin-bottom:.0001pt;
        text-align:right;
        text-indent:.5in;
        mso-pagination:widow-orphan;
        page-break-after:avoid;
        mso-outline-level:1;
        font-size:22.0pt;
        mso-bidi-font-size:10.0pt;
        font-family:"Limon S1";
        mso-font-kerning:0pt;
        mso-fareast-language:EN-US;
        mso-bidi-language:AR-SA;
        font-weight:normal;}
        h2
        {mso-style-unhide:no;
        mso-style-qformat:yes;
        mso-style-next:Normal;
        margin:0in;
        margin-bottom:.0001pt;
        mso-pagination:widow-orphan;
        page-break-after:avoid;
        mso-outline-level:2;
        font-size:13.0pt;
        mso-bidi-font-size:10.0pt;
        font-family:"Times New Roman",serif;
        mso-bidi-font-family:Arial;
        color:navy;
        mso-fareast-language:EN-US;
        mso-bidi-language:AR-SA;}
        h3
        {mso-style-unhide:no;
        mso-style-qformat:yes;
        mso-style-next:Normal;
        margin:0in;
        margin-bottom:.0001pt;
        mso-pagination:widow-orphan;
        page-break-after:avoid;
        mso-outline-level:3;
        font-size:10.0pt;
        font-family:"Times New Roman",serif;
        mso-bidi-font-family:Arial;
        color:#003300;
        mso-fareast-language:EN-US;
        mso-bidi-language:AR-SA;}
        h4
        {mso-style-unhide:no;
        mso-style-qformat:yes;
        mso-style-link:"Heading 4 Char";
        mso-style-next:Normal;
        margin:0in;
        margin-bottom:.0001pt;
        mso-pagination:widow-orphan;
        page-break-after:avoid;
        mso-outline-level:4;
        font-size:20.0pt;
        font-family:"Limon S6";
        mso-bidi-font-family:Arial;
        mso-fareast-language:EN-US;
        mso-bidi-language:AR-SA;
        font-weight:normal;}
        h5
        {mso-style-unhide:no;
        mso-style-qformat:yes;
        mso-style-link:"Heading 5 Char";
        mso-style-next:Normal;
        margin-top:12.0pt;
        margin-right:0in;
        margin-bottom:3.0pt;
        margin-left:0in;
        mso-pagination:widow-orphan;
        mso-outline-level:5;
        font-size:13.0pt;
        font-family:"Times New Roman",serif;
        mso-fareast-language:EN-US;
        mso-bidi-language:AR-SA;
        font-style:italic;}
        p.MsoHeader, li.MsoHeader, div.MsoHeader
        {mso-style-priority:99;
        mso-style-link:"Header Char";
        margin:0in;
        margin-bottom:.0001pt;
        mso-pagination:widow-orphan;
        tab-stops:center 3.25in right 6.5in;
        font-size:10.0pt;
        font-family:"Times New Roman",serif;
        mso-fareast-font-family:"Times New Roman";
        mso-fareast-language:EN-US;
        mso-bidi-language:AR-SA;}
        p.MsoFooter, li.MsoFooter, div.MsoFooter
        {mso-style-priority:99;
        mso-style-link:"Footer Char";
        margin:0in;
        margin-bottom:.0001pt;
        mso-pagination:widow-orphan;
        tab-stops:center 3.25in right 6.5in;
        font-size:10.0pt;
        font-family:"Times New Roman",serif;
        mso-fareast-font-family:"Times New Roman";
        mso-fareast-language:EN-US;
        mso-bidi-language:AR-SA;}
        a:link, span.MsoHyperlink
        {mso-style-priority:99;
        color:blue;
        mso-themecolor:hyperlink;
        text-decoration:underline;
        text-underline:single;}
        a:visited, span.MsoHyperlinkFollowed
        {mso-style-noshow:yes;
        mso-style-priority:99;
        color:purple;
        mso-themecolor:followedhyperlink;
        text-decoration:underline;
        text-underline:single;}
        p.MsoAcetate, li.MsoAcetate, div.MsoAcetate
        {mso-style-noshow:yes;
        mso-style-priority:99;
        mso-style-link:"Balloon Text Char";
        margin:0in;
        margin-bottom:.0001pt;
        mso-pagination:widow-orphan;
        font-size:8.0pt;
        font-family:"Tahoma",sans-serif;
        mso-fareast-font-family:"Times New Roman";
        mso-fareast-language:EN-US;
        mso-bidi-language:AR-SA;}
        p.MsoNoSpacing, li.MsoNoSpacing, div.MsoNoSpacing
        {mso-style-priority:1;
        mso-style-unhide:no;
        mso-style-qformat:yes;
        mso-style-parent:"";
        margin:0in;
        margin-bottom:.0001pt;
        mso-pagination:widow-orphan;
        font-size:10.0pt;
        font-family:"Times New Roman",serif;
        mso-fareast-font-family:"Times New Roman";
        mso-fareast-language:EN-US;
        mso-bidi-language:AR-SA;}
        p.MsoListParagraph, li.MsoListParagraph, div.MsoListParagraph
        {mso-style-priority:34;
        mso-style-unhide:no;
        mso-style-qformat:yes;
        margin-top:0in;
        margin-right:0in;
        margin-bottom:0in;
        margin-left:.5in;
        margin-bottom:.0001pt;
        mso-add-space:auto;
        mso-pagination:widow-orphan;
        font-size:10.0pt;
        font-family:"Times New Roman",serif;
        mso-fareast-font-family:"Times New Roman";
        mso-fareast-language:EN-US;
        mso-bidi-language:AR-SA;}
        p.MsoListParagraphCxSpFirst, li.MsoListParagraphCxSpFirst, div.MsoListParagraphCxSpFirst
        {mso-style-priority:34;
        mso-style-unhide:no;
        mso-style-qformat:yes;
        mso-style-type:export-only;
        margin-top:0in;
        margin-right:0in;
        margin-bottom:0in;
        margin-left:.5in;
        margin-bottom:.0001pt;
        mso-add-space:auto;
        mso-pagination:widow-orphan;
        font-size:10.0pt;
        font-family:"Times New Roman",serif;
        mso-fareast-font-family:"Times New Roman";
        mso-fareast-language:EN-US;
        mso-bidi-language:AR-SA;}
        p.MsoListParagraphCxSpMiddle, li.MsoListParagraphCxSpMiddle, div.MsoListParagraphCxSpMiddle
        {mso-style-priority:34;
        mso-style-unhide:no;
        mso-style-qformat:yes;
        mso-style-type:export-only;
        margin-top:0in;
        margin-right:0in;
        margin-bottom:0in;
        margin-left:.5in;
        margin-bottom:.0001pt;
        mso-add-space:auto;
        mso-pagination:widow-orphan;
        font-size:10.0pt;
        font-family:"Times New Roman",serif;
        mso-fareast-font-family:"Times New Roman";
        mso-fareast-language:EN-US;
        mso-bidi-language:AR-SA;}
        p.MsoListParagraphCxSpLast, li.MsoListParagraphCxSpLast, div.MsoListParagraphCxSpLast
        {mso-style-priority:34;
        mso-style-unhide:no;
        mso-style-qformat:yes;
        mso-style-type:export-only;
        margin-top:0in;
        margin-right:0in;
        margin-bottom:0in;
        margin-left:.5in;
        margin-bottom:.0001pt;
        mso-add-space:auto;
        mso-pagination:widow-orphan;
        font-size:10.0pt;
        font-family:"Times New Roman",serif;
        mso-fareast-font-family:"Times New Roman";
        mso-fareast-language:EN-US;
        mso-bidi-language:AR-SA;}
        span.Heading1Char
        {mso-style-name:"Heading 1 Char";
        mso-style-unhide:no;
        mso-style-locked:yes;
        mso-style-link:"Heading 1";
        mso-ansi-font-size:22.0pt;
        font-family:"Limon S1";
        mso-ascii-font-family:"Limon S1";
        mso-hansi-font-family:"Limon S1";
        mso-bidi-language:AR-SA;}
        span.Heading4Char
        {mso-style-name:"Heading 4 Char";
        mso-style-unhide:no;
        mso-style-locked:yes;
        mso-style-link:"Heading 4";
        mso-ansi-font-size:20.0pt;
        mso-bidi-font-size:20.0pt;
        font-family:"Limon S6";
        mso-ascii-font-family:"Limon S6";
        mso-hansi-font-family:"Limon S6";
        mso-bidi-font-family:Arial;
        mso-bidi-language:AR-SA;}
        span.Heading5Char
        {mso-style-name:"Heading 5 Char";
        mso-style-unhide:no;
        mso-style-locked:yes;
        mso-style-link:"Heading 5";
        mso-ansi-font-size:13.0pt;
        mso-bidi-font-size:13.0pt;
        mso-bidi-language:AR-SA;
        font-weight:bold;
        font-style:italic;}
        span.BalloonTextChar
        {mso-style-name:"Balloon Text Char";
        mso-style-noshow:yes;
        mso-style-priority:99;
        mso-style-unhide:no;
        mso-style-locked:yes;
        mso-style-link:"Balloon Text";
        mso-ansi-font-size:8.0pt;
        mso-bidi-font-size:8.0pt;
        font-family:"Tahoma",sans-serif;
        mso-ascii-font-family:Tahoma;
        mso-hansi-font-family:Tahoma;
        mso-bidi-font-family:Tahoma;
        mso-bidi-language:AR-SA;}
        span.HeaderChar
        {mso-style-name:"Header Char";
        mso-style-priority:99;
        mso-style-unhide:no;
        mso-style-locked:yes;
        mso-style-link:Header;
        mso-bidi-language:AR-SA;}
        span.FooterChar
        {mso-style-name:"Footer Char";
        mso-style-priority:99;
        mso-style-unhide:no;
        mso-style-locked:yes;
        mso-style-link:Footer;
        mso-bidi-language:AR-SA;}
        .MsoChpDefault
        {mso-style-type:export-only;
        mso-default-props:yes;
        font-size:10.0pt;
        mso-ansi-font-size:10.0pt;
        mso-bidi-font-size:10.0pt;
        mso-fareast-language:EN-US;
        mso-bidi-language:KHM;}
        /* Page Definitions */
        @page
        {mso-footnote-separator:url("F01-Credit%20sale%20contract%20PHN_files/header.html") fs;
        mso-footnote-continuation-separator:url("F01-Credit%20sale%20contract%20PHN_files/header.html") fcs;
        mso-endnote-separator:url("F01-Credit%20sale%20contract%20PHN_files/header.html") es;
        mso-endnote-continuation-separator:url("F01-Credit%20sale%20contract%20PHN_files/header.html") ecs;}
        @page WordSection1
        {size:595.35pt 842.0pt;
        margin:.5in .5in .5in .5in;
        mso-header-margin:.2in;
        mso-footer-margin:5.75pt;
        mso-header:url("F01-Credit%20sale%20contract%20PHN_files/header.html") h1;
        mso-footer:url("F01-Credit%20sale%20contract%20PHN_files/header.html") f1;
        mso-paper-source:0;}
        div.WordSection1
        {page:WordSection1;}
        @page WordSection2
        {size:595.35pt 842.0pt;
        margin:28.35pt 28.05pt 28.35pt 28.05pt;
        mso-header-margin:.5in;
        mso-footer-margin:.5in;
        mso-columns:2 even .2in;
        mso-header:url("F01-Credit%20sale%20contract%20PHN_files/header.html") h1;
        mso-footer:url("F01-Credit%20sale%20contract%20PHN_files/header.html") f1;
        mso-paper-source:0;}
        div.WordSection2
        {page:WordSection2;}
        @page WordSection3
        {size:595.35pt 842.0pt;
        margin:.35in 28.1pt 19.45pt 28.1pt;
        mso-header-margin:.5in;
        mso-footer-margin:.5in;
        mso-header:url("F01-Credit%20sale%20contract%20PHN_files/header.html") h1;
        mso-footer:url("F01-Credit%20sale%20contract%20PHN_files/header.html") f1;
        mso-paper-source:0;}
        div.WordSection3
        {page:WordSection3;}
        /* List Definitions */
        .button {
                background-color: #4CAF50; /* Green */
                border: none;
                color: white;
                padding: 10px 25px;
                text-align: center;
                text-decoration: none;
                display: inline-block;
                font-size: 16px;
                margin: 4px 2px;
                -webkit-transition-duration: 0.4s; /* Safari */
                transition-duration: 0.4s;
                cursor: pointer;
            }

            .button1 {
                background-color: white; 
                color: black; 
                border: 2px solid #4CAF50;
            }

            .button1:hover {
                background-color: #4CAF50;
                color: white;
            }

            .button2 {
                background-color: white; 
                color: black; 
                border: 2px solid #008CBA;
            }

            .button2:hover {
                background-color: #008CBA;
                color: white;
            }
        </style>
    </head>
    <body id=print_me lang=EN-US link=blue vlink=purple style='tab-interval:.5in; margin-left:50px;'>
        <button id="print_hide" class="button button1" onclick="with_print()">print</button>
        <button id="go_back" class="button button2" onclick="go_back()">Go Back</button>
        <script>
            document.getElementById('print_me').style.width = "210mm";    
            function with_print(){
                document.getElementById('print_hide').style.display = "none";
                document.getElementById('go_back').style.display = "none";
                document.getElementById('print_me').removeAttribute("style");
                window.print();
                document.getElementById('print_me').style.width = "210mm"; 
                document.getElementById('print_me').style.marginLeft = "50px";  
                document.getElementById('print_hide').removeAttribute("style");
                document.getElementById('go_back').removeAttribute("style");
            }
            function go_back(){
                window.location = "{{ url('aprove_credit_sales/'.$data_id.'/show') }}";
            }
        </script>
        <div class=WordSection1>
            <p class=MsoNoSpacing align=center style='text-align:center'>
                <span
                style='font-size:12.0pt;font-family:"Moul";mso-bidi-language:KHM'>
                <o:p>&nbsp;</o:p>
                </span>
            </p>
            <p class=MsoNoSpacing align=center style='text-align:center'>
                <span lang=KHM
                style='font-size:14.0pt;font-family:"Moul";mso-bidi-language:KHM'>ព្រះរាជាណាចក្រកម្ពុជា</span>
                <span
                style='font-size:14.0pt;font-family:"Moul";mso-bidi-language:KHM'>
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing align=center style='text-align:center'>
                <span lang=KHM
                style='font-size:14.0pt;font-family:"Moul";mso-bidi-language:KHM'>ជាតិ
                សាសនា ព្រះមហាក្សត្រ</span>
                <span style='font-size:14.0pt;font-family:"Moul";
                mso-bidi-language:KHM'>
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing align=center style='text-align:center'>
                <b><span
                style='font-size:12.0pt;font-family:Wingdings;mso-ascii-font-family:"Battambang";
                mso-hansi-font-family:"Battambang";mso-bidi-font-family:"Battambang";
                mso-bidi-language:KHM;mso-char-type:symbol;mso-symbol-font-family:Wingdings'><span
                style='mso-char-type:symbol;mso-symbol-font-family:Wingdings'><span
                style='mso-char-type:symbol;mso-symbol-font-family:Wingdings'><span
                style='mso-char-type:symbol;mso-symbol-font-family:Wingdings'></span></span></span></span></b><b><span
                style='font-size:14.0pt;font-family:Wingdings;mso-ascii-font-family:"Battambang";
                mso-hansi-font-family:"Battambang";mso-bidi-font-family:"Battambang";
                mso-bidi-language:KHM;mso-char-type:symbol;mso-symbol-font-family:Wingdings'><span
                style='mso-char-type:symbol;mso-symbol-font-family:Wingdings'>®</span></span></b><b><span
                style='font-size:12.0pt;font-family:Wingdings;mso-ascii-font-family:"Battambang";
                mso-hansi-font-family:"Battambang";mso-bidi-font-family:"Battambang";
                mso-bidi-language:KHM;mso-char-type:symbol;mso-symbol-font-family:Wingdings'><span
                style='mso-char-type:symbol;mso-symbol-font-family:Wingdings'><span
                style='mso-char-type:symbol;mso-symbol-font-family:Wingdings'><span
                style='mso-char-type:symbol;mso-symbol-font-family:Wingdings'></span></span></span></span></b>
                <b>
                <span
                    style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                    KHM'>
                    <o:p></o:p>
                </span>
                </b>
            </p>
            <p class=MsoNoSpacing style='text-align:justify'>
                <b>
                <span style='font-size:
                    12.0pt;font-family:"Battambang";mso-bidi-language:KHM'>
                    <o:p>&nbsp;</o:p>
                </span>
                </b>
            </p>
            <p class=MsoNoSpacing style='text-align:justify'>
                <span lang=KHM
                style='font-size:12.0pt;font-family:"Moul";mso-bidi-language:KHM'>ខេ</span>
                <span
                style='font-size:12.0pt;font-family:"Moul";mso-bidi-language:KHM'>
                .<span
                    lang=KHM>អេ</span>.<span lang=KHM>អេស</span>.<span lang=KHM>ភី</span>.<span
                    lang=KHM> កសិករ ពាណិជ្ជ ឯ.ក</span>
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing style='text-align:justify'>
                <span lang=KHM
                style='font-family:"Moul";mso-bidi-language:KHM'>លេខកិច្ចសន្យាៈ </span><span
                style='font-size:9.0pt;font-family:"Battambang";mso-bidi-language:
                KHM' class="contract_id">....................................<span lang=KHM> </span></span>
                <span
                style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                KHM'>
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing style='text-align:justify'>
                <span lang=KHM
                style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                KHM'><span style='mso-spacerun:yes'>            </span></span>
                <span
                style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                KHM'>
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing align=center style='text-align:center'>
                <span lang=KHM
                style='font-size:12.0pt;font-family:"Moul";mso-bidi-language:KHM'>កិច្ចសន្យាទិញបង់រំលស់</span>
                <span
                style='font-size:12.0pt;font-family:"Moul";mso-bidi-language:KHM'>
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing align=center style='text-align:center'>
                <span lang=KHM
                style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                KHM'>ធ្វើនៅ</span><span style='font-family:"Battambang";mso-bidi-language:
                KHM' class="where_create_contract">........................</span><span lang=KHM style='font-size:12.0pt;
                font-family:"Battambang";mso-bidi-language:KHM'>ថ្ងៃទី</span><span
                style='font-family:"Battambang";mso-bidi-language:KHM' class="date_create_contract">...........</span><span
                lang=KHM style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                KHM'>ខែ</span><span style='font-family:"Battambang";mso-bidi-language:
                KHM' class="month_create_contract">...............</span><span lang=KHM style='font-size:12.0pt;font-family:
                "Battambang";mso-bidi-language:KHM'>ឆ្នាំ</span><span
                style='font-family:"Battambang";mso-bidi-language:KHM' class="year_create_contract">.................</span>
                <span
                style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                KHM'>
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing align=center style='text-align:center'>
                <b><span lang=KHM
                style='font-size:12.0pt;font-family:"Moul";mso-bidi-language:KHM'>រវាង</span></b>
                <b>
                <span
                    style='font-size:12.0pt;font-family:"Moul";mso-bidi-language:KHM'>
                    <o:p></o:p>
                </span>
                </b>
            </p>
            <p class=MsoNoSpacing style='text-align:justify'>
                <span lang=KHM
                style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                KHM'>ក្រុមហ៊ុន </span><span lang=KHM style='font-size:12.0pt;font-family:"Moul";
                mso-bidi-language:KHM'>ខេ.អេ.អេស.ភី. កសិករ ពាណិជ្ជ ឯ.ក</span><span lang=KHM
                style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                KHM'> ដែលមានលោក </span><span lang=KHM style='font-size:12.0pt;font-family:"Moul";
                mso-bidi-language:KHM' class="credit_committy">ផុន ភារម្យ</span><span lang=KHM style='font-size:12.0pt;
                font-family:"Battambang";mso-bidi-language:KHM' > តួនាទីជា </span><span
                lang=KHM style='font-size:12.0pt;font-family:"Moul";mso-bidi-language:
                KHM' class="credit_committy_position">ប្រធានសាខា</span><span lang=KHM style='font-size:12.0pt;font-family:"Battambang";
                mso-bidi-language:KHM'> ដែលមានអាសយដ្ឋាន ផ្ទះលេខ៦២ ផ្លូវលេខ៦២​</span>
                <span
                style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                KHM'>
                R<span lang=KHM> ភូមិទទួលគោក សង្កាត់ទួលសង្កែ ខណ្ឌឬស្សីកែវ រាជធានីភ្នំពេញ
                ជាតំណាងរបស់ក្រុមហ៊ុន តទៅនេះហៅកាត់ថាភាគី </span>(<span lang=KHM>ក</span>)
                <span
                    lang=KHM>
                    ។
                    <o:p></o:p>
                </span>
                </span>
            </p>
            <p class=MsoNoSpacing align=center style='text-align:center'>
                <b><span lang=KHM
                style='font-size:12.0pt;font-family:"Moul";mso-bidi-language:KHM'>និង</span></b>
                <b>
                <span
                    style='font-size:12.0pt;font-family:"Moul";mso-bidi-language:KHM'>
                    <o:p></o:p>
                </span>
                </b>
            </p>
            <p class=MsoNoSpacing style='text-align:left'>
                <span lang=KHM
                style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                KHM'>ឈ្មោះ</span>
                <span style='font-size:12.0pt;font-family:"Battambang";
                mso-bidi-language:KHM'><b class="kh_username" lang=KHM>.........................................</b><span lang=KHM>ភេទ</span >......<span
                    lang=KHM class="client_gender">....</span>....<span lang=KHM>ថ្ងៃខែឆ្នាំកំណើត</span>....<span class="client_dob">.............................</span><span
                    lang=KHM>សញ្ជាតិ</span><span class="nationality">.............</span>..<span lang=KHM>កាន់ប័ណ្ណ​សម្គាល់អត្តសញ្ញាណលេខ</span><span class="identify_num">..............................................</span><span lang=KHM>ប្រភេទប័ណ្ណ</span><span class="identify_type">.........................................</span>
                    <span lang=KHM>ចុះថ្ងៃទី</span><span class="date_create_identify_id">.....................</span><span lang=KHM>..</span>.........      <span
                    lang=KHM>ចេញដោយ</span>...<span lang=KHM class="identify_by">...</span>..................<span
                    lang=KHM>និងឈ្មោះ</span><span class="name_with_relate_client">........................................</span><span lang=KHM >ភេទ</span><span class="name_with_relate_client">...........</span>
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing style='text-align:justify'>
                <span lang=KHM
                style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                KHM'>ថ្ងៃខែឆ្នាំកំណើត</span>
                <span style='font-size:12.0pt;font-family:"Battambang";
                mso-bidi-language:KHM'>
                ......<span lang=KHM>........</span>..............<span
                    lang=KHM>សញ្ជាតិ</span>.<span lang=KHM>.........</span>..<span lang=KHM> កាន់ប័ណ្ណ​សម្គាល់អត្តសញ្ញាណលេខ</span>....<span
                    lang=KHM>..............</span>..............
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing style='text-align:justify'>
                <span lang=KHM
                style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                KHM'>ប្រភេទប័ណ្ណ</span>
                <span style='font-size:12.0pt;font-family:"Battambang";
                mso-bidi-language:KHM'>
                .....................................<span lang=KHM>ចុះថ្ងៃទី</span>..................<span
                    lang=KHM>.</span>...........<span lang=KHM>ចេញដោយ</span>...........................................<span
                    lang=KHM>។</span>
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing style='text-align:justify'>
                <span style='font-size:12.0pt;
                font-family:"Battambang";mso-bidi-language:KHM'>
                <span
                    style='mso-spacerun:yes'> </span><span lang=KHM>អាសយដ្ឋានផ្ទះលេខ</span><span class="home_num">...........</span><span
                    lang=KHM>ក្រុមទី</span><span class="group_num">...........</span><span lang=KHM>ផ្លូវលេខ</span><span class="street_num">...........</span><span
                    lang=KHM></span><span lang=KHM>ភូមិ</span><span class="villege">...........</span><span
                    lang=KHM>ឃុំ</span>/<span lang=KHM>សង្កាត់</span><span class="commune">..........................</span>
                <o:p></o:p>                                                            
                </span>
            </p>
            <p class=MsoNoSpacing style='text-align:justify'>
                <span lang=KHM
                style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                KHM'>ស្រុក</span>
                <span style='font-size:12.0pt;font-family:"Battambang";
                mso-bidi-language:KHM'>
                /<span lang=KHM>ខណ្ឌ</span><span class="district">..........................</span><span
                    lang=KHM>ខេត្ត</span>/<span lang=KHM>ក្រុង</span><span class="province" lang=KHM>..................</span><span
                    lang=KHM></span><span lang=KHM></span><span lang=KHM><span
                    style='mso-spacerun:yes'>  </span>តទៅហៅកាត់ថាភាគី </span>(<span lang=KHM>ខ</span>)<span
                    lang=KHM>។</span>
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing style='text-align:justify'>
                <span style='font-size:12.0pt;
                font-family:"Battambang";mso-bidi-language:KHM'>
                <o:p>&nbsp;</o:p>
                </span>
            </p>
            <p class=MsoNoSpacing align=center style='text-align:center'>
                <span lang=KHM
                style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                KHM'>ភាគីទាំងពីរបានព្រមព្រៀងចុះកិច្ចសន្យា ដោយអនុវត្តតាមប្រការដូចខាងក្រោម​៖</span>
                <span
                style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                KHM'>
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing style='text-align:justify'>
                <b><span lang=KHM
                style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                KHM'>ប្រការ១៖ អំពីលក្ខខណ្ឌរួម</span></b>
                <b>
                <span style='font-size:12.0pt;
                    font-family:"Battambang";mso-bidi-language:KHM'>
                    <o:p></o:p>
                </span>
                </b>
            </p>
            <p class=MsoNoSpacing style='text-align:justify'>
                <span lang=KHM
                style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                KHM'>១.១<span style='mso-tab-count:1'>    </span>ភាគី (ខ) យល់ព្រមទិញ <span class="item_name">.....................................................................</span>
                ចំនួន<span class="qty">..........................</span>គ្រឿង </span>
                <span style='font-size:12.0pt;
                font-family:"Battambang";mso-bidi-language:KHM'>
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing style='margin-left:.75in;text-align:justify;text-indent:
                -.25in'>
                <span lang=KHM style='font-size:12.0pt;font-family:"Battambang";
                mso-bidi-language:KHM'>ម៉ាក <span class="brand">.............................................................</span>
                លេខកូដ<span class="barcode">................................................................</span></span>
                <span
                style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                KHM'>
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing style='margin-left:.75in;text-align:justify;text-indent:
                -.25in'>
                <span lang=KHM style='font-size:12.0pt;font-family:"Battambang";
                mso-bidi-language:KHM'>ក្នុងតម្លៃសរុបជាលេខ <span
                style='mso-spacerun:yes'> </span><span class="total_price">................................</span> </span>
                <span
                style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                KHM'>
                (US$)<span lang=KHM> ជាអក្សរ</span><span class="price_word" lang=KHM>............................................</span>
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing style='margin-left:.75in;text-align:justify;text-indent:
                -.25in'>
                <span lang=KHM style='font-size:12.0pt;font-family:"Battambang";
                mso-bidi-language:KHM'>ដោយបង់ប្រាក់កក់មុនចំនួន<span class="deposit_precent">..........</span>ភាគរយ ស្មើនឹងចំនួនសរុបជាលេខ
                <span class="deposit_fixed">...................................... </span>
                <span style='font-size:12.0pt;
                font-family:"Battambang";mso-bidi-language:KHM'>
                (US$)
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing style='text-align:justify;text-indent:.5in'>
                <span
                lang=KHM style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                KHM'>ជាអក្សរ <span class="deposit_fixed_word">................................................................</span>។</span>
                <span
                style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                KHM'>
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing style='text-align:justify;text-indent:.5in'>
                <span
                lang=KHM style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                KHM'>១.២<span style='mso-tab-count:1'>    </span>ភាគី (ខ) យល់ព្រមបង់រំលស់ប្រាក់ដែលនៅសល់រួមទាំងការប្រាក់ចំនួនជាលេខ<span class="money_owne">.........</span></span>
                <span
                style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                KHM'>
                <span lang=KHM></span>US$
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing style='margin-left:.5in;text-align:justify'>
                <span
                lang=KHM style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                KHM'>ជាអក្សរ <span class="money_owne_word">................................................................</span></span>
                <span
                style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                KHM'>
                <span style='mso-spacerun:yes'>  </span><span lang=KHM>ក្នុងរយៈពេល<span class="duration_pay_money">........</span></span><span
                    lang=KHM>ខែ ទៅតាមដំណាក់កាលដូចខាងក្រោមៈ </span>
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing style='text-align:justify;text-indent:.5in'>
                <span
                style='font-size:12.0pt;font-family:Wingdings;mso-ascii-font-family:"Battambang";
                mso-hansi-font-family:"Battambang";mso-bidi-font-family:"Battambang";
                mso-bidi-language:KHM;mso-char-type:symbol;mso-symbol-font-family:Wingdings'><span class="week"
                style='mso-char-type:symbol;mso-symbol-font-family:Wingdings'>¨</span></span><span
                lang=KHM style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                KHM'> សបា្តហ៍ <span style='mso-tab-count:1'>     </span></span><span
                style='font-size:12.0pt;font-family:Wingdings;mso-ascii-font-family:"Battambang";
                mso-hansi-font-family:"Battambang";mso-bidi-font-family:"Battambang";
                mso-bidi-language:KHM;mso-char-type:symbol;mso-symbol-font-family:Wingdings'><span class="2week"
                style='mso-char-type:symbol;mso-symbol-font-family:Wingdings'>¨</span></span><span
                lang=KHM style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                KHM'> ពីរសបា្តហ៍ <span style='mso-spacerun:yes'>  </span><span
                style='mso-tab-count:1'>         </span></span><span style='font-size:12.0pt;
                font-family:Wingdings;mso-ascii-font-family:"Battambang";mso-hansi-font-family:
                "Battambang";mso-bidi-font-family:"Battambang";mso-bidi-language:
                KHM;mso-char-type:symbol;mso-symbol-font-family:Wingdings'><span class="month"
                style='mso-char-type:symbol;mso-symbol-font-family:Wingdings'>¨</span></span><span
                lang=KHM style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                KHM'> ខែ</span>
                <span style='font-size:12.0pt;font-family:"Battambang";
                mso-bidi-language:KHM'>
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing style='margin-left:70.9pt;text-align:justify;text-indent:
                -21.25pt;mso-list:l7 level1 lfo30'>
                <![if !supportLists]><span style='font-size:
                12.0pt;font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:
                Symbol;mso-bidi-language:KHM'><span style='mso-list:Ignore'>·<span
                style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                </span></span></span><![endif]><span lang=KHM style='font-size:12.0pt;
                font-family:"Battambang";mso-bidi-language:KHM'>ចំនួនជាលេខ </span>
                <span
                style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                KHM'>
                US$<span class="total_pay" lang=KHM>.....................</span> x <span class="pay_times">..........</span><span lang=KHM>ដង
                </span>
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing style='margin-left:70.9pt;text-align:justify;text-indent:
                -21.25pt;mso-list:l7 level1 lfo30'>
                <![if !supportLists]><span style='font-size:
                12.0pt;font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:
                Symbol;mso-bidi-language:KHM'><span style='mso-list:Ignore'>·<span
                style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                </span></span></span><![endif]><span lang=KHM style='font-size:12.0pt;
                font-family:"Battambang";mso-bidi-language:KHM'>ទូរទាត់ចាប់ពីថ្ងៃទី​<span class="start_date">................</span>ខែ<span class="start_month">................</span>ឆ្នាំ<span class="start_year">................</span></span>
                <span
                style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                KHM'>
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing style='margin-left:1.0in;text-align:justify'>
                <span
                lang=KHM style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                KHM'>នឹងបញ្ចប់ថ្ងៃទី​<span class="end_date">...............</span>ខែ<span class="end_month">...............</span>ឆ្នាំ<span class="end_year">...............</span> (តារាងកាលវិភាគសងប្រាក់)។</span>
                <span
                style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                KHM'>
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing style='text-align:justify'>
                <span lang=KHM
                style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                KHM'>១.៣<span style='mso-tab-count:1'>    </span>ភាគី (ខ) យល់ព្រមទូទាត់សំណងថ្លៃដឹកជញ្ជូន
                និងសេវារដ្ឋបាលផ្សេងៗ ដែលមានតម្លៃស្មើ១០% នៃតម្លៃទំ<span style='mso-tab-count:
                1'>          </span>និញ ក្នុងករណីភាគី(ខ) មិនទទួលយកទំនិញដែលបានដឹកជញ្ជូនទីកន្លែង។</span>
                <span
                style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                KHM'>
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing style='text-align:justify'>
                <span lang=KHM
                style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                KHM'>១.៤<span style='mso-tab-count:1'>    </span>ភាគី(ក) ធានាលើលក្ខនៃទំនិញ គុណភាពផលិតផលល្អ១០០%<span
                style='mso-spacerun:yes'>  </span>និងមានប័ណ្ណធានារយៈពេលមួយឆ្នាំ។</span>
                <span
                style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                KHM'>
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing style='text-align:justify'>
                <b><span lang=KHM
                style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                KHM'>ប្រការ២៖ អំពីលក្ខខណ្ឌពិសេស</span></b>
                <span style='font-size:12.0pt;
                font-family:"Battambang";mso-bidi-language:KHM'>
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing style='margin-left:.5in;text-align:justify;text-indent:
                -.5in'>
                <span lang=KHM style='font-size:12.0pt;font-family:"Battambang";
                mso-bidi-language:KHM'>២.១<span style='mso-tab-count:1'>    </span>ភាគី (ខ) មានកាតព្វកិច្ចបង់ប្រាក់ឲ្យបានទៀងទាត់ទៅតារាងកាលវិភាគសងប្រាក់ដែលបានកំណត់។
                ក្នុងករណីភាគី </span>
                <span style='font-size:12.0pt;font-family:"Battambang";
                mso-bidi-language:KHM'>
                (<span lang=KHM>ខ</span>)<span lang=KHM> មិនបានគោរពលក្ខខណ្ឌទាំងឡាយនៃកិច្ចសន្យានេះ
                ភាគី </span>(<span lang=KHM>ខ</span>)
                <span lang=KHM>
                    យល់ព្រមឲ្យភាគី (ក) <span
                        style='mso-spacerun:yes'> </span>រឹបអូសសម្ភារៈដែលបានទិញពីក្រុមហ៊ុន
                    ឬសម្ភារៈផ្សេងៗដែលមានតម្លៃប្រហាក់ប្រហែលគ្នា ដើម្បីលក់ឡាយឡុងសម្រាប់ទូទាត់ប្រាក់បំណុលដែលនៅខ្វះ។
                    <o:p></o:p>
                </span>
                </span>
            </p>
            <p class=MsoNoSpacing style='margin-left:.5in;text-align:justify;text-indent:
                -.5in'>
                <span lang=KHM style='font-size:12.0pt;font-family:"Battambang";
                mso-bidi-language:KHM'>២.២<span style='mso-tab-count:1'>    </span>ក្នុងករណី
                ភាគី </span>
                <span style='font-size:12.0pt;font-family:"Battambang";
                mso-bidi-language:KHM'>
                (<span lang=KHM>ខ</span>)<span lang=KHM> ខកខានមិនបានសងប្រាក់គ្រប់ចំនួនតាមថ្ងៃកំណត់ដូចមានក្នុងតារាងកាលវិភាគសងប្រាក់នោះ
                ភាគី​ </span>(<span lang=KHM>ខ</span>)<span lang=KHM> យល់ព្រមបង់ប្រាក់ពិន័យឲ្យ
                ភាគី (ក)។ ប្រាក់ពិន័យនេះត្រូវគណនាដោយយកចំនួនទឹកប្រាក់ដែលខកខានមិនបានសង
                គុណនឹងអត្រា ១០% ទៅតាមចំនួនថ្ងៃដែលបានយឺត។</span>
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing style='margin-left:.5in;text-align:justify;text-indent:
                -.5in'>
                <span lang=KHM style='font-size:12.0pt;font-family:"Battambang";
                mso-bidi-language:KHM'>២.៣<span style='mso-tab-count:1'>   </span>ក្នុងករណីមិនអាចអនុវត្តកិច្ចសន្យាបាន
                មុនចប់អាណត្តិដូចជា បាត់បង់លទ្ធភាពការងារ ឬមរណៈភាព អ្នកធានារបស់ភា​គី​ </span>
                <span
                style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                KHM'>
                (<span lang=KHM>ខ</span>) <span lang=KHM>ជាអ្នកទទួលខុសត្រូវទាំងស្រុងក្នុងការអនុវត្តកិច្ចសន្យាបន្ត។</span>
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing style='text-align:justify'>
                <span style='font-size:12.0pt;
                font-family:"Battambang";mso-bidi-language:KHM'>
                <o:p>&nbsp;</o:p>
                </span>
            </p>
            <p class=MsoNoSpacing style='text-align:justify'>
                <b><span lang=KHM
                style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                KHM'>ប្រការ៣៖ កាតព្វកិច្ចអ្នកធានា</span></b>
                <b>
                <span style='font-size:12.0pt;
                    font-family:"Battambang";mso-bidi-language:KHM'>
                    <o:p></o:p>
                </span>
                </b>
            </p>
            <p class=MsoNoSpacing style='text-align:justify'>
                <span lang=KHM
                style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                KHM'>៣.១<span style='mso-tab-count:1'>    </span>អ្នកធានាឈ្មោះ</span>
                <span
                style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                KHM'>
                <span class="sub_name1">...................................</span><span lang=KHM>ភេទ</span><span class="sub_gender1"
                    lang=KHM>......</span><span lang=KHM>ថ្ងៃខែឆ្នាំកំណើត</span><span class="sub_dob1"
                    lang=KHM>...........</span><span lang=KHM>សញ្ជាតិ</span><span class="sub_nation1">...........</span>
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing style='margin-left:.5in;text-align:justify'>
                <span
                lang=KHM style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                KHM'>កាន់ប័ណ្ណសម្គាល់អត្តសញ្ញាណលេខ</span><span style='font-size:12.0pt;
                font-family:"Battambang";mso-bidi-language:KHM'><span class="sub_id1" lang=KHM>.........................</span><span lang=KHM>ប្រភេទប័ណ្ណ</span><span class="sub_idt1">.......................................</span><span
                    lang=KHM>ចុះថ្ងៃទី</span><span class="approve_date1">............................</span><span lang=KHM>ចេញដោយ</span><span class="approve_by1">...........................</span><span
                    lang=KHM></span><span lang=KHM>និងឈ្មោះ</span><span class="sub_name2" lang=KHM>....................................</span>
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing style='margin-left:.5in;text-align:justify'>
                <span
                lang=KHM style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                KHM'>ភេទ</span>
                <span style='font-size:12.0pt;font-family:"Battambang";
                mso-bidi-language:KHM'>
                <span class="sub_gender2">.............</span><span lang=KHM> ថ្ងៃខែឆ្នាំកំណើត </span><span class="sub_dob2"
                    lang=KHM>.................................</span><span lang=KHM></span><span lang=KHM> សញ្ជាតិ</span><span class="sub_nation2"
                    lang=KHM>..............</span><span lang=KHM> </span>
                
                </span>
            </p>
            <p class=MsoNoSpacing style='margin-left:.5in;text-align:justify'>
                <span
                lang=KHM style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                KHM'>កាន់ប័ណ្ណ​សម្គាល់អត្តសញ្ញាណលេខ</span>
                <span class="sub_id2" style='font-size:12.0pt;font-family:"Battambang";
                mso-bidi-language:KHM'><span lang=KHM>ប្រភេទប័ណ្ណ</span>..................................<span
                    lang=KHM>ចុះថ្ងៃទី</span>............................<span lang=KHM>....</span>.......
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing style='margin-left:.5in;text-align:justify'>
                <span
                lang=KHM style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                KHM'>ចេញដោយ</span>
                <span style='font-size:12.0pt;font-family:"Battambang";
                mso-bidi-language:KHM'>
                <span class="approve_by2" lang=KHM>​....................................</span><span
                    lang=KHM>។ <span style='mso-spacerun:yes'> </span>អាសយដ្ឋាន ផ្ទះលេខ</span><span class="sub_home_num">.............</span><span
                    lang=KHM>ក្រុមទី</span><span class="sub_group_num" lang=KHM>.............</span><span lang=KHM>ផ្លូវលេខ</span><span class="sub_street_num"
                    lang=KHM>........</span>
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing style='margin-left:.5in;text-align:justify'>
                <span
                lang=KHM style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                KHM'>ភូមិ</span>
                <span style='font-size:12.0pt;font-family:"Battambang";
                mso-bidi-language:KHM'><span class="sub_villege" lang=KHM>.................</span><span
                    lang=KHM>ឃុំ</span>/<span lang=KHM>សង្កាត់</span><span class="sub_commune" lang=KHM>...............</span><span
                    lang=KHM>ស្រុក</span>/<span lang=KHM>ខណ្ឌ</span><span class="sub_district" lang=KHM>...................</span>
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing style='margin-left:.5in;text-align:justify'>
                <span
                lang=KHM style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                KHM'>ខេត្ត</span>
                <span style='font-size:12.0pt;font-family:"Battambang";
                mso-bidi-language:KHM'>
                /<span lang=KHM>ក្រុង</span><span class="sub_province">....................................................</span><span
                    lang=KHM>តទៅនេះហៅកាត់ថាភាគី </span>(<span lang=KHM>គ</span>)<span lang=KHM>។</span>
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing style='margin-left:.5in;text-align:justify;text-indent:
                -.5in'>
                <span lang=KHM style='font-size:12.0pt;font-family:"Battambang";
                mso-bidi-language:KHM'>៣.២<span style='letter-spacing:-1.0pt'> </span></span><span
                style='font-size:12.0pt;font-family:"Battambang";letter-spacing:-1.0pt;
                mso-bidi-language:KHM'><span style='mso-tab-count:1'>   </span></span><span
                lang=KHM style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                KHM'>ក្នុងករណីភាគី </span>
                <span style='font-size:12.0pt;font-family:"Battambang";
                mso-bidi-language:KHM'>
                (<span lang=KHM>ខ</span>)<span lang=KHM> គ្មានលទ្ធភាពសងប្រាក់ជូនភាគី
                </span>(<span lang=KHM>ក</span>) <span lang=KHM>ទេ នោះភាគី</span> (<span
                    lang=KHM>គ</span>)<span lang=KHM> នឹងមានកាតព្វកិច្ចសងប្រាក់ជំពាក់ទាំងអស់ ដែលមិនទាន់បានសងជំនួសភាគី
                </span>(<span lang=KHM>ខ</span>)<span lang=KHM> ឲ្យគ្រប់ចំនួន។</span>
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing style='margin-left:.75in;text-align:justify;text-indent:
                -.5in'>
                <span style='font-size:12.0pt;font-family:"Battambang";
                mso-bidi-language:KHM'>
                <o:p>&nbsp;</o:p>
                </span>
            </p>
            <p class=MsoNoSpacing style='text-align:justify'>
                <b><span lang=KHM
                style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                KHM'>ប្រការ៤៖​ អវសានបញ្ញត្តិ</span></b>
                <b>
                <span style='font-size:12.0pt;
                    font-family:"Battambang";mso-bidi-language:KHM'>
                    <o:p></o:p>
                </span>
                </b>    
            </p>
            <p class=MsoNoSpacing style='margin-left:.5in;text-align:justify;text-indent:
                -.5in'>
                <span lang=KHM style='font-size:12.0pt;font-family:"Battambang";
                mso-bidi-language:KHM'>៤.១<span style='mso-tab-count:1'>    </span>ភាគី</span>
                <span
                style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                KHM'>
                (<span lang=KHM>ក</span>)<span lang=KHM> ភាគី </span>(<span lang=KHM>ខ</span>)
                <span lang=KHM>និងភាគី</span> (<span lang=KHM>គ</span>)<span lang=KHM> សន្យាគោរពយ៉ាងមឺងម៉ាត់តាមរាល់ប្រការនៃកិច្ចសន្យានេះ។
                ក្នុងករណីមានការអនុវត្តផ្ទុយ ឬដោយរំលោភលើលក្ខខណ្ឌណាមួយនៃកិច្ចសន្យានេះ ភាគីដែលល្មើសត្រូវទទួលខុសត្រូវចំពោះមុខច្បាប់ជាធរមាន។
                រាល់សោហ៊ុយចំណាយទាក់ទងក្នុងការដោះស្រាយ វិវាទ ជាបន្ទុករបស់ភាគីដែលបង្ករការរំលោភបំពានលើកិច្ចសន្យា។</span>
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing style='margin-left:.5in;text-align:justify;text-indent:
                -.5in'>
                <span lang=KHM style='font-size:12.0pt;font-family:"Battambang";
                mso-bidi-language:KHM'>៤.២​ </span>
                <span style='font-size:12.0pt;font-family:
                "Battambang";mso-bidi-language:KHM'>
                <span style='mso-tab-count:1'>   </span><span
                    lang=KHM>ភាគីទាំងអស់បានអាន និងយល់យ៉ាងច្បាស់អំពីអត្ថន័យនៃកិច្ចសន្យា ហើយស្ម័គ្រចិត្តគោរពយ៉ាងម៉ឺងម៉ាត់រាល់ប្រការទាំងអស់ដែលមានចែងនៅក្នុងកិច្ចសន្យានេះដោយគ្មានកាបង្ខិតបង្ខំឡើយ។</span>
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing style='margin-left:.5in;text-align:justify;text-indent:
                -.5in'>
                <span lang=KHM style='font-size:12.0pt;font-family:"Battambang";
                mso-bidi-language:KHM'>៤.៣</span>
                <span style='font-size:12.0pt;font-family:
                "Battambang";mso-bidi-language:KHM'>
                <span style='mso-tab-count:1'>    </span><span
                    lang=KHM>កិច្ចសន្យា និងតារាងកាលវិភាគសងប្រាក់នេះមានសុពលភាពចាប់ពីថ្ងៃចុះហត្ថលេខា
                និងផ្តិតស្នាមមេដៃស្តាំតទៅ។</span>
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing style='margin-left:.5in;text-align:justify;text-indent:
                -.5in'>
                <span lang=KHM style='font-size:12.0pt;font-family:"Battambang";
                mso-bidi-language:KHM'>៤.៤ </span>
                <span style='font-size:12.0pt;font-family:
                "Battambang";mso-bidi-language:KHM'>
                <span style='mso-tab-count:1'>   </span><span
                    lang=KHM>កិច្ចសន្យានេះត្រូវបានធ្វើឡើងជា ០៣ច្បាប់ជាភាសាខ្មែរ ១ច្បាប់ដើមរក្សាទុកភាគី
                (ក) ១ច្បាប់រក្សាទុកនៅភាគី (ខ) និង១ច្បាប់ទៀតរក្សានៅភាគី (គ)។ </span>
                <o:p></o:p>
                </span>
            </p>
            <p class=MsoNoSpacing style='text-align:justify'>
                <span style='font-size:12.0pt;
                font-family:"Battambang";mso-bidi-language:KHM'>
                <o:p>&nbsp;</o:p>
                </span>
            </p>
        </div>
        <span style='font-size:12.0pt;font-family:"Battambang";mso-fareast-font-family:
            "Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:EN-US;
            mso-bidi-language:KHM'><br clear=all style='page-break-before:auto;mso-break-type:
            section-break'>
        </span>
        <div class=WordSection2>
            <p class=MsoNoSpacing style='text-align:justify'>
                <b>
                <span style='font-size:
                    12.0pt;font-family:"Battambang";mso-bidi-language:KHM'>
                    <o:p>&nbsp;</o:p>
                </span>
                </b>
            </p>
       <div class="" style="width:40%; display:inline-block ;">
            <p class=MsoNoSpacing align=center style='text-align:center'>
                <b><span lang=KHM
                style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                KHM'>ស្នាមមេដៃស្តាំភាគី</span></b>
                <b>
                <span style='font-size:12.0pt;font-family:
                    "Battambang";mso-bidi-language:KHM'>
                    (<span lang=KHM>ខ</span>)
                    <o:p></o:p>
                </span>
                </b>
            </p>
            <p class=MsoNoSpacing align=center style='text-align:center'>
                <b><span lang=KHM
                style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                KHM'>ប្តី</span></b>
                <b>
                <span style='font-size:12.0pt;font-family:"Battambang";
                    mso-bidi-language:KHM'>
                    / <span lang=KHM>ប្រពន្ធ</span>
                    <o:p></o:p>
                </span>
                </b>
            </p>
            <p class=MsoNoSpacing style='text-align:justify'>
                <b>
                <span style='font-size:
                    12.0pt;font-family:"Battambang";mso-bidi-language:KHM'>
                    <o:p>&nbsp;</o:p>
                </span>
                </b>
            </p>
            <p class=MsoNoSpacing style='text-align:justify'>
                <b>
                <span style='font-size:
                    12.0pt;font-family:"Battambang";mso-bidi-language:KHM'>
                    <o:p>&nbsp;</o:p>
                </span>
                </b>
            </p>
            <p class=MsoNoSpacing style='text-align:justify'>
                <b>
                <span style='font-size:
                    12.0pt;font-family:"Battambang";mso-bidi-language:KHM'>
                    <o:p>&nbsp;</o:p>
                </span> 
                </b>
            </p>
            
            
            <p class=MsoNoSpacing  align=center style='text-align:center'>
                <b><span lang=KHM
                style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                KHM'>ឈ្មោះ</span></b>
                <b>
                <span  style='font-size:12.0pt;font-family:"Battambang";
                    mso-bidi-language:KHM'>
                    ..............................<span
                        style='mso-spacerun:yes'>   
                </b>
                
            </p>
            <p class=MsoNoSpacing  align=center style='text-align:center'>
                <b><span lang=KHM
                style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                KHM'>ឈ្មោះ</span></b>
                <b>
                <span  style='font-size:12.0pt;font-family:"Battambang";
                    mso-bidi-language:KHM'>
                    ..............................<span
                        style='mso-spacerun:yes'>   
                </b>
                
            </p>
            <p class=MsoNoSpacing style='text-align:justify'>
                <b>
                <span style='font-size:
                    12.0pt;font-family:"Battambang";mso-bidi-language:KHM'>
                    <o:p>&nbsp;</o:p>
                </span>
                </b>
            </p>

        </div>

        <div class="" style="width:28%;  display:inline-block ;">
           
            <p class=MsoNoSpacing align=center style='text-align:center'>
                <b><span lang=KHM
                style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                KHM'>ស្នា​​មមេដៃស្តាំអ្នកធានា</span></b>
                <b>
                <span style='font-size:12.0pt;font-family:
                    "Battambang";mso-bidi-language:KHM'>
                   
                    <o:p></o:p>
                </span>
                </b>
            </p>
            <p class=MsoNoSpacing align=center style='text-align:center'>
                <b><span lang=KHM
                style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                KHM'>ភាគី</span></b>
                <b>
                <span style='font-size:12.0pt;font-family:
                    "Battambang";mso-bidi-language:KHM'>
                    (<span lang=KHM>គ</span>)
                    <o:p></o:p>
                </span>
                </b>
            </p>
            <p class=MsoNoSpacing style='text-align:justify'>
                <b>
                <span style='font-size:
                    12.0pt;font-family:"Battambang";mso-bidi-language:KHM'>
                    <o:p>&nbsp;</o:p>
                </span>
                </b>
            </p>
            <p class=MsoNoSpacing style='text-align:justify'>
                <b>
                <span style='font-size:
                    12.0pt;font-family:"Battambang";mso-bidi-language:KHM'>
                    <o:p>&nbsp;</o:p>
                </span>
                </b>
            </p>
            <p class=MsoNoSpacing style='text-align:justify'>
                <b>
                <span style='font-size:
                    12.0pt;font-family:"Battambang";mso-bidi-language:KHM'>
                    <o:p>&nbsp;</o:p>
                </span> 
                </b>
            </p>
            <p class=MsoNoSpacing style='text-align:justify'>
                <b>
                <span style='font-size:
                    12.0pt;font-family:"Battambang";mso-bidi-language:KHM'>
                    <o:p>&nbsp;</o:p>
                </span>
                </b>
            </p>
            <p class=MsoNoSpacing  align=center style='text-align:center'>
                <b><span lang=KHM
                style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                KHM'>ឈ្មោះ</span></b>
                <b>
                <span  style='font-size:12.0pt;font-family:"Battambang";
                    mso-bidi-language:KHM'>
                    .............................<span
                        style='mso-spacerun:yes'>   
                </b>
            </p>
            <p class=MsoNoSpacing style='text-align:justify'>
                <b>
                <span style='font-size:
                    12.0pt;font-family:"Battambang";mso-bidi-language:KHM'>
                    <o:p>&nbsp;</o:p>
                </span>
                </b>
            </p>
        </div>
        <div class="" style="width:28%;  display:inline-block ;">
           
            <p class=MsoNoSpacing align=center style='text-align:center'>
                <b><span lang=KHM
                style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                KHM'>តំណាងឲ្យភាគី(</span></b>
                <b>
                <span style='font-size:12.0pt;font-family:
                    "Battambang";mso-bidi-language:KHM'>
                   (<span lang=KHM>ក</span>)
                    <o:p></o:p>
                </span>
                </b>
            </p>
            <p class=MsoNoSpacing align=center style='text-align:center'>
                <b><span lang=KHM
                style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                KHM'>ហត្ថលេខា និងឈ្មោះ</span></b>
                <b>
                <span style='font-size:12.0pt;font-family:
                    "Battambang";mso-bidi-language:KHM'>
                    
                    <o:p></o:p>
                </span>
                </b>
            </p>
            <p class=MsoNoSpacing style='text-align:justify'>
                <b>
                <span style='font-size:
                    12.0pt;font-family:"Battambang";mso-bidi-language:KHM'>
                    <o:p>&nbsp;</o:p>
                </span>
                </b>
            </p>
            <p class=MsoNoSpacing style='text-align:justify'>
                <b>
                <span style='font-size:
                    12.0pt;font-family:"Battambang";mso-bidi-language:KHM'>
                    <o:p>&nbsp;</o:p>
                </span>
                </b>
            </p>
            <p class=MsoNoSpacing style='text-align:justify'>
                <b>
                <span style='font-size:
                    12.0pt;font-family:"Battambang";mso-bidi-language:KHM'>
                    <o:p>&nbsp;</o:p>
                </span> 
                </b>
            </p>
            <p class=MsoNoSpacing style='text-align:justify'>
                <b>
                <span style='font-size:
                    12.0pt;font-family:"Battambang";mso-bidi-language:KHM'>
                    <o:p>&nbsp;</o:p>
                </span>
                </b>
            </p>
            <p class=MsoNoSpacing  align=center style='text-align:center'>
                <b><span lang=KHM
                style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                KHM'>ឈ្មោះ</span></b>
                <b>
                <span  style='font-size:12.0pt;font-family:"Battambang";
                    mso-bidi-language:KHM'>
                    .............................<span
                        style='mso-spacerun:yes'>   
                </b>
            </p>
            <p class=MsoNoSpacing style='text-align:justify'>
                <b>
                <span style='font-size:
                    12.0pt;font-family:"Battambang";mso-bidi-language:KHM'>
                    <o:p>&nbsp;</o:p>
                </span>
                </b>
            </p>
        </div>
            
        <div class="" style="width:48%;  display:inline-block ;">
                <p class=MsoNoSpacing align=center style='text-align:center'>
                    <b><span lang=KHM
                    style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                    KHM'>បានឃើញ និងបញ្ជាក់ថាសេចក្តីបញ្ជាក់របស់</span></b>
                    <b>
                    <span style='font-size:
                        12.0pt;font-family:"Battambang";mso-bidi-language:KHM'>
                        <o:p></o:p>
                    </span>
                    </b>
                </p>
                <p class=MsoNoSpacing align=center style='text-align:center'>
                    <b><span lang=KHM
                    style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                    KHM'>មេភូមិ</span></b>
                    <span style='font-size:12.0pt;font-family:"Battambang";
                    mso-bidi-language:KHM'>
                    ...............................................
                    <o:p></o:p>
                    </span>
                </p>
                <p class=MsoNoSpacing align=center style='text-align:center'>
                    <span lang=KHM
                    style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                    KHM'>ថ្ងៃទី</span>
                    <span style='font-size:12.0pt;font-family:"Battambang";
                    mso-bidi-language:KHM'>
                    ..................<span lang=KHM>ខែ</span>.....................<span
                        lang=KHM>ឆ្នាំ</span>....................
                    <o:p></o:p>
                    </span>
                </p>
                <p class=MsoNoSpacing align=center style='text-align:center'>
                    <b><span lang=KHM
                    style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                    KHM'>មេឃុំ</span></b><b><span style='font-size:12.0pt;font-family:"Battambang";
                    mso-bidi-language:KHM'>/<span lang=KHM>ចៅសង្កាត់</span></span></b>
                    <span
                    style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                    KHM'>
                    .....................................
                    <o:p></o:p>
                    </span>
                </p>
                <p class=MsoNoSpacing align=center style='text-align:center'>
                    <b><span lang=KHM
                    style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                    KHM'>ហត្ថលេខា និងត្រា</span></b>
                    <b>
                    <span style='font-size:12.0pt;font-family:
                        "Battambang";mso-bidi-language:KHM'>
                        <o:p></o:p>
                    </span>
                    </b>
                </p>
                <p class=MsoNoSpacing style='text-align:justify'>
                    <b>
                    <span style='font-size:
                        12.0pt;font-family:"Battambang";mso-bidi-language:KHM'>
                        <o:p>&nbsp;</o:p>
                    </span>
                    </b>
                </p>


        </div>
        <div class="" style="width:48%;  display:inline-block ;">
                
                <p class=MsoNoSpacing align=center style='text-align:center'>
                    <b><span lang=KHM
                    style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                    KHM'>បានឃើញ និងទទួលស្គាល់ថា</span></b>
                    <b>
                    <span style='font-size:12.0pt;
                        font-family:"Battambang";mso-bidi-language:KHM'>
                        <o:p></o:p>
                    </span>
                    </b>
                </p>
                <p class=MsoNoSpacing align=center style='text-align:center'>
                    <span lang=KHM
                    style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                    KHM'>កម្មសិទ្ធិស្របច្បាប់របស់ភាគី(ខ)ប្រាកដមែន ហើយទ្រព្យទាំង​នេះពុំមាន<span
                    style='mso-spacerun:yes'>  </span>ពាក់ព័ន្ធនិងបញ្ហាអ្វីឡើយ ហើយភាគី(ខ)
                    បានយល់ព្រមដាក់បញ្ចាំដោយ ស្ម័គ្រចិត្តឲ្យភាគី(ក) ពិតប្រាកដមែន។</span>
                    <span
                    style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                    KHM'>
                    <o:p></o:p>
                    </span>
                </p>
                <p class=MsoNoSpacing align=center style='text-align:center'>
                    <span lang=KHM
                    style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                    KHM'>ថ្ងៃទី</span>
                    <span style='font-size:12.0pt;font-family:"Battambang";
                    mso-bidi-language:KHM'>
                    ..................<span lang=KHM>ខែ</span>.....................<span
                        lang=KHM>ឆ្នាំ</span>....................
                    <o:p></o:p>
                    </span>
                </p>
                <p class=MsoNoSpacing align=center style='text-align:center'>
                    <b><span lang=KHM
                    style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                    KHM'>មេភូមិ</span></b>
                    <span style='font-size:12.0pt;font-family:"Battambang";
                    mso-bidi-language:KHM'>
                    ................................................
                    <o:p></o:p>
                    </span>
                </p>
                <p class=MsoNoSpacing align=center style='text-align:center'>
                    <b><span lang=KHM
                    style='font-size:12.0pt;font-family:"Battambang";mso-bidi-language:
                    KHM'>ហត្ថលេខា និងឈ្មោះ</span></b>
                    <b>
                    <span style='font-size:12.0pt;font-family:
                        "Battambang";mso-bidi-language:KHM'>
                        <o:p></o:p>
                    </span>
                    </b>
                </p>
            </div>
        </div>
        <span lang=KHM style='font-size:12.0pt;font-family:"Battambang";
            mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:
            EN-US;mso-bidi-language:KHM'><br clear=all style='page-break-before:auto;
            mso-break-type:section-break'>
        </span>
        <div class=WordSection3>
            <p class=MsoNoSpacing style='text-align:justify'>
                <span style='font-size:12.0pt;
                font-family:"Battambang";mso-bidi-language:KHM'>
                <o:p>&nbsp;</o:p>
                </span>
            </p>
        </div>
    </body>
    
<meta name="_token" content="{{ csrf_token() }}" />
<!-- ១២៣៤៥៦៧៨៩០ -->
<script type="text/javascript">
$(document).ready(function(){
    $(window).load(function(){
        get_sale_credit_sale_show();
    });

    var count = 10;
        function str_pad_left(num,count){
            if(count > 0){
            var num_c = num.toString();
            var str = "0";
            var str_num = num_c.length;
            var count1 = count - str_num;
            var str1 = str.repeat(count1);
            return str1+""+num_c;
            }else{
                return console.log("error right count please it number and bigger then  0");
            }
        }
        function get_date(get_date){
            var d = new Date(get_date);
            var day_n = d.getDate();
            if(day_n > 10){
                day_n = day_n;
            }else{
                day_n = "0"+day_n; 
            }
            return day_n;
        }
       
        function get_month(get_month){
            var d = new Date(get_month);
            var month_n = d.getMonth() + 1;
            if(month_n >= 10){
                month_n = month_n;
            }else{
                month_n = "0"+month_n; 
            }
            return month_n;
        }
        function get_year(get_year){
            var d = new Date(get_year);
            var year_n = d.getFullYear();
            return year_n;
        }
        function get_sale_credit_sale_show(){
            var url_json =  "{{ url('aprove_credit_sales/'.$data_id.'/show_json') }}";
                    $.ajax({
                        type: "GET",
                        url: url_json, 
                        dataType: "json",
                        success: function(result){
                            console.log(result);
                            var text = "";
                            $.each(result.approval_item,function(i,da){
                                text += "<tr> " ;
                                // text += "<td>  <button class='btn btn-danger btn-mini remove_item' value='"+da.product_id+"'><i class='icon-remove icon-white ' style='padding-right: 0px;'></i></button>   "+da.item.name+" </td>";
                                text += "<td> "+da.item.name+" </td>";
                                text +=  "<td> "+da.item.item_bacode+"  </td>";
                                text +=  "<td> "+da.item.categorys.name+" </td>"
                                text +=  "<td> "+da.qty+" </td>";
                                text +=  "<td>  "+accounting.formatMoney(da.sell_price)+" </td>";
                                text +=  "<td>  "+accounting.formatMoney(da.total_price_payment)+" </td>";
                                text += "</tr> "; 
                               
                            });

                            $(".item_list").html(text); 
                            if(result.method == "sale_by_credit"){
                                var mothod = "បង់រំលស់";
                            }else{
                                var mothod = "បង់ជាសាច់ប្រាក់";   
                            }
                            var  dpm = result.date_for_payments;
                            var d = new Date(dpm);
                            var year_n = d.getFullYear();
                            var month_n = d.getMonth() + 1;
                            var day_n = d.getDate();
                            if(month_n > 10){
                                month_n = month_n;
                            }else{
                                month_n = "0"+month_n; 
                            }
                            if(day_n > 10){
                                day_n = day_n;
                            }else{
                                day_n = "0"+day_n; 
                            }
                           var  date_for_payments = day_n +"-"+month_n+"-"+year_n;
                           var dpmt = result.duration_pay_money_type;
                            if(dpmt == "month"){
                                dpmt = "ខែ";
                            }else if(dpmt == "2week"){
                                dpmt = "២ សប្តាហ៍";
                            }else if(dpmt == "week"){
                                dpmt = "សប្តាហ៍";
                            }else if(dpmt == "day"){
                                dpmt = "ថ្ងៃ";
                            }  
                            if(result.date_approval){
                                $(".date_create_contract").text("..."+get_date(result.date_approval)+"...");
                                $(".month_create_contract").html("..."+get_month(result.date_approval)+"...");
                                $(".year_create_contract").text("    "+get_year(result.date_approval));
                            }
                            if(result.cs_sales.branch.brand_name){
                                $(".where_create_contract").text("..."+result.cs_sales.branch.brand_name+"...");
                            }
                            
                            if(result.cs_sales.cs_client.kh_username){
                                $(".kh_username").text("  "+result.cs_sales.cs_client.kh_username+"   ");
                            }
                            if(result.cs_sales.cs_client.gender == 'F'){
                                    var gender = "ស្រី";
                            }else{
                                    var gender = "ប្រុស";
                            }
                            $(".client_gender").text("  "+gender+"   ");
                            
                            if(result.cs_sales.cs_client.dob){
                                  var dob = get_date(result.cs_sales.cs_client.dob)+"-"+get_month(result.cs_sales.cs_client.dob)+"-"+get_year(result.cs_sales.cs_client.dob);
                                  $(".client_dob").text("  "+dob+"   ");
                                  
                            }
                            if(result.cs_sales.cs_client.nationality == 1){
                                var national = "ខ្មែរ";
                            }else{nationality
                                var national = result.cs_sales.cs_client.nationality;
                            }
                            $(".nationality").text("  "+national+"   ");
                            
                            if(result.sale_id){
                                $(".contract_id").text(str_pad_left(result.sale_id,count));
                            }
                            if(result.cs_sales.cs_client.identify_num){
                                $('.identify_num').text("  "+result.cs_sales.cs_client.identify_num+"  ");
                            }
                            if(result.cs_sales.cs_client.identify_type){
                                if(result.cs_sales.cs_client.identify_type == 1){
                                    $(".identify_type").text(" អត្តសញ្ញាណប័ណ្ណ ");
                                }else if(result.cs_sales.cs_client.identify_type == 2){
                                    $(".identify_type").text("  លិខិនឆ្លង់ដែន ");
                                }else if(result.cs_sales.cs_client.identify_type == 3){
                                    $(".identify_type").text(" សំបុត្របញ្ជាក់កំណើត ");
                                }else if(result.cs_sales.cs_client.identify_type == 4){
                                    $(".identify_type").text(" សៀវភៅសា្នក់នៅ ");
                                }else if(result.cs_sales.cs_client.identify_type == 5){
                                    $(".identify_type").text(" សៀវភៅគ្រួសារ ");
                                }else{
                                    $(".identify_type").text(result.cs_sales.cs_client.identify_type);
                                }
                                
                            }
                            if(result.cs_sales.cs_client.identify_by){
                                if(result.cs_sales.cs_client.identify_by == 1){
                                   $(".identify_by").html("ក្រសួងមហាផ្ទៃ");
                                }else{
                                    $(".identify_by").html("  "+result.cs_sales.cs_client.identify_by+"  ");
                                }
                            }
                            if(result.cs_sales.cs_client.home_num){
                                $(".home_num").text("  "+result.cs_sales.cs_client.home_num+"  ");
                            }
                            if(result.cs_sales.cs_client.group_num){
                                $(".group_num").text("  "+result.cs_sales.cs_client.group_num+"  ");
                            }
                            if(result.cs_sales.cs_client.street_num){
                                $(".street_num").text("  "+result.cs_sales.cs_client.street_num+"  ");
                            }
                            if(result.cs_sales.cs_client.vilige){
                                $(".villege").text("  "+result.cs_sales.cs_client.vilige+"  ");
                            }
                            if(result.cs_sales.cs_client.commune){
                                $(".commune").text("  "+result.cs_sales.cs_client.commune+"  ");
                            }
                            if(result.cs_sales.cs_client.district){
                                $(".district").text("  "+result.cs_sales.cs_client.district+"  ");
                            }
                            if(result.cs_sales.cs_client.province){
                                $(".province").text("  "+result.cs_sales.cs_client.province+"  ");
                            }

                            var item_name = "";
                            var qty = 0;
                            var brand = "";
                            var barcode = "";
                            $.each(result.approval_item, function(ka, va){
                                if(ka == (result.approval_item.length-1)){
                                    item_name += va.item.name;
                                    brand += va.item.cs_brand.name;
                                    barcode += va.item.item_bacode;
                                }else{
                                    item_name += va.item.name + " , ";
                                    brand += va.item.cs_brand.name + " , ";
                                    barcode += va.item.item_bacode + " , ";
                                }
                                qty +=  va.qty;
                                
                            });
                            $(".item_name").text(item_name);
                            $(".qty").text("  "+qty+"  ");
                            $(".brand").text("  "+brand+"  ");
                            $(".barcode").text("  "+barcode+"  ");
                            $(".total_price").text("  "+result.prices_total_num+"  ");
                            $(".price_word").text("  "+result.prices_totalword+"  ");
                            $(".deposit_precent").text("  "+result.deposit_precent+"  ");
                            $(".deposit_fixed").text("  "+result.deposit_fixed+"  ");
                            $(".deposit_fixed_word").text("  "+result.deposit_fixed_word+"  ");
                            $(".money_owne").text("  "+result.money_owne+"  ");
                            $(".money_owne_word").text("  "+result.money_owne_word+"  ");
                            $(".duration_pay_money").text("  "+result.duration_pay_money+"  ");
                            
                            if(result.duration_pay_money_type == 'month'){
                                $(".month").text("þ");
                            }else if(result.duration_pay_money_type == '2week'){
                                $(".2week").text("þ");
                            }else{
                                $(".week").text("þ");
                            }
                            $.each(result.schedule.cs_schedule_timesheet, function(csk, csv){
                                var length = result.schedule.cs_schedule_timesheet.length;
                                if(csk == length-1){
                                    $(".total_pay").text("  "+csv.total_payment+"  ");
                                    $(".end_date").text("  "+get_date(csv.date_payment)+"  ");
                                    $(".end_month").text("  "+get_month(csv.date_payment)+"  ");
                                    $(".end_year").text("  "+get_year(csv.date_payment)+"  ");
                                }
                            });
                            $(".pay_times").text("  "+result.duration_pay_money+"  ");
                            $(".start_date").text("  "+get_date(result.date_for_payments)+"  ");
                            $(".start_month").text("  "+get_month(result.date_for_payments)+"  ");
                            $(".start_year").text("  "+get_year(result.date_for_payments)+"  ");
                            // Sub Clients
                            $.each(result.cs_client.sub_clients, function(sk, sv){
                                if(sk == 0){
                                    $(".sub_name1").text("  "+sv.client_name_kh+"  ");
                                    if(sv.client_gender == "M"){
                                        $(".sub_gender1").text("  ប្រុស  ");
                                    }else{
                                        $(".sub_gender1").text("  ស្រី  ");
                                    }
                                    $(".sub_dob1").text("  "+sv.client_dob+"  ");
                                    if(sv.client_nationality == 1){
                                        $(".sub_nation1").text("  ខ្មែរ  ");
                                    }else{
                                        $(".sub_nation1").text("  "+sv.client_nationality+"  ")
                                    }
                                    $(".sub_id1").text("  "+sv.client_idcard_no+"  ");

                                    if(sv.client_type_idcard == 1){
                                        $(".sub_idt1").text("  អត្តសញ្ញាណប័ណ្ណ  ");
                                    }else if(sv.client_type_idcard == 2){
                                        $(".sub_idt1").text("  លិខិនឆ្លង់ដែន  ");
                                    }else if(sv.client_type_idcard == 3){
                                        $(".sub_idt1").text("  សំបុត្របញ្ជាក់កំណើត  ");
                                    }else if(sv.client_type_idcard == 4){
                                        $(".sub_idt1").text("  សៀវភៅសា្នក់នៅ  ");
                                    }else if(sv.client_type_idcard == 5){
                                        $(".sub_idt1").text("  សៀវភៅគ្រួសារ  ");
                                    }else{
                                        $(".sub_idt1").text("  "+sv.client_type_idcard+"  ");
                                    }

                                    if(sv.client_aprovel_idcard_by == 1){
                                        $(".approve_by1").text("  ក្រសួងមហាផ្ទៃ  ");
                                    }else{
                                        $(".approve_by1").text("  "+sv.client_aprovel_idcard_by+"  ");
                                    }
                                    $(".sub_home_num").text("  "+sv.client_house_num+"  ");
                                    $(".sub_street_num").text("  "+sv.client_st_num+"  ");
                                    $(".sub_group_num").text("  "+sv.client_group_num+"  ");
                                    $(".sub_villege").text("  "+sv.client_village+"  ");
                                    $(".sub_commune").text("  "+sv.client_commune+"  ");
                                    $(".sub_district").text("  "+sv.client_district+"  ");
                                    $(".sub_province").text("  "+sv.client_province+"  ");                                }
                                if(sk == 1){
                                    $(".sub_name2").text("  "+sv.client_name_kh+"  ");
                                    if(sv.client_gender == "M"){
                                        $(".sub_gender2").text("  ប្រុស  ");
                                    }else{
                                        $(".sub_gender2").text("  ស្រី  ");
                                    }
                                    $(".sub_dob2").text("  "+sv.client_dob+"  ");
                                    if(sv.client_nationality == 1){
                                        $(".sub_nation2").text("  ខ្មែរ  ");
                                    }else{
                                        $(".sub_nation2").text("  "+sv.client_nationality+"  ")
                                    }
                                    $(".sub_id2").text("  "+sv.client_idcard_no+"  ");

                                    if(sv.client_type_idcard == 1){
                                        $(".sub_idt2").text("  អត្តសញ្ញាណប័ណ្ណ  ");
                                    }else if(sv.client_type_idcard == 2){
                                        $(".sub_idt2").text("  លិខិនឆ្លង់ដែន  ");
                                    }else if(sv.client_type_idcard == 3){
                                        $(".sub_idt2").text("  សំបុត្របញ្ជាក់កំណើត  ");
                                    }else if(sv.client_type_idcard == 4){
                                        $(".sub_idt2").text("  សៀវភៅសា្នក់នៅ  ");
                                    }else if(sv.client_type_idcard == 5){
                                        $(".sub_idt2").text("  សៀវភៅគ្រួសារ  ");
                                    }else{
                                        $(".sub_idt2").text("  "+sv.client_type_idcard+"  ");
                                    }
                                    if(sv.client_aprovel_idcard_by == 1){
                                        $(".approve_by2").text("  ក្រសួងមហាផ្ទៃ  ");
                                    }else{
                                        $(".approve_by2").text(sv.client_aprovel_idcard_by);
                                    }
                                }
                                
                            });
                            

                            $(".method").text(mothod); 
                            $(".prices_total_num_text").text(accounting.formatMoney(result.prices_total_num));
                            $(".deposit_fixed_text").text(accounting.formatMoney(result.deposit_fixed));
                            $(".duration_pay_money_text").text(result.duration_pay_money);
                            $(".duration_pay_money_type_text").text(dpmt);
                            $(".date_for_payments_text").text(date_for_payments );
                            $(".money_owne_text").text(accounting.formatMoney(result.money_owne));    
                            
                        },
                        error: function (result ,status, xhr) {
                            console.log(result.responseText);
                        }
                    
                    });
        }

 });       
</script> 
    </html>