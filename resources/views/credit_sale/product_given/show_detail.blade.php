@extends('credit_sale.layout.master')
@section('contend')
@include('credit_sale.autocompleted_script')
<style>
.table th, .table td{
    vertical-align: middle;
}
</style>
<div class="container-fluid">
            <div class="row-fluid">
                     <!-- validation -->
                    <div class="row-fluid">
                         <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                    <div class="muted pull-left"><a > {{$title}}</a> <span class="divider">|</span>បង្ហាញព័ត៌មានលម្អិត​ {{$title}}</div>					
                            </div>
                            <div class="block-content collapse in">      
                               <hr/>
                                 <center>
			                        <h3 class="cen_title text-center khmer_Moul">បង្ហាញព័ត៌មានលម្អិត​ {{$title}}​</h3>
			                    </center>
                            	<legend></legend>  
                            
                            @include('errors.error')
                            <table class="table table-bordered">
                                <thead style="background: rgb(251, 205, 205);">
                                    <tr class="header">
                                        <th width="20%">សេចក្ដីពិពណ៌នា</th>
                                        <th width="80%">ទិន្នន័យ</th>                                       
                                    </tr>
                                    </thead>
                                    <tbody class="body_show">

                                        <tr>
                                            <td>លេខតារាង</td>
                                            <td> <p class="schedule_id">  </p> </td>
                                        </tr>
                                        <tr>
                                            <td>ឈ្មោះអតិថិជន</td>
                                            <td> <p class="client_name">  </p> </td>
                                        </tr>
                                        <tr>
                                            <td>ផលិតផល</td>
                                            <td class="list_items">
                                                     <table class="table table-bordered">
                                                            <thead>
                                                                <tr>
                                                                    <th>ឈ្មោះទំនិញ</th>
                                                                    <th>លេខ​កូដ​</th>
                                                                    <th>ម៉ាក</th>
                                                                    <th>ចំនួន</th>
                                                                    <th>តម្លៃ​ឯកតា</th>
                                                                    <th>តម្លៃសរុប</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody class="item_list">

                                                            </tbody>
                                                        </table>    
                                            </td>
                                        </tr>
                                       
                                        <tr>
                                            <td> ចំនួនទឹកប្រាក់សរុបជាលេខ </td>
                                            <td> <p class="prices_total_num"></p> </td>
                                        </tr>
                                        <tr>
                                            <td> ចំនួនទឹកប្រាក់សរុបជាអក្សរ  </td>
                                            <td> <p class="money_owne_word"></p> </td>
                                        </tr>
                                        <tr>
                                            <td> ចំនួនទឹកប្រាក់ដែលបានកក់សរុបជាលេខ  </td>
                                            <td> <p class="deposit_fixed"></p> </td>
                                        </tr>
                                        <tr>
                                            <td> ចំនួនទឹកប្រាក់ដែលបានកក់សរុបជាអក្សរ  </td>
                                            <td> <p class="deposit_fixed_word"></p> </td>
                                        </tr>
                                        <tr>
                                            <td> ចំនួនទឹកប្រាក់ដែលនៅជំពាក់សរុបជាលេខ  </td>
                                            <td> <p class="money_owne"></p> </td>
                                        </tr>
                                        <tr>
                                            <td>  ចំនួនទឹកប្រាក់ដែលនៅជំពាក់សរុបជាអក្សរ   </td>
                                            <td> <p class="money_owne_word"></p> </td>
                                        </tr>
                                        
                                    </tbody>

                                    </table>
                             </div>   
						
                     	<!-- /block -->
		    		</div>
                     <!-- /validation -->
                </div>

            </div>

<meta name="_token" content="{{ csrf_token() }}" />




<script>
var item_id = "{{$item_id}}";
if(item_id != 0){
    show_data(item_id);
}
var redirect = "{{$redirect}}";
function show_data(item_id){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    var url_now = "{{url('show_give_product_to_co')}}/"+item_id;
            $.ajax({
                
                type: "POST",
                url: url_now,
                dataType: 'json',
                // async: false,
                //data: formData1,
                success: function(result){
                    console.log(result);
                    if(result.schedule_id){
                        $(".schedule_id").text(result.schedule_id);
                    }
                    if(result.client){
                        $(".client_name").text(result.client.kh_username);    
                    }
                    if(result.schedule.cs_approvalcredit.prices_total_num){
                        $(".prices_total_num").text(accounting.formatMoney(result.schedule.cs_approvalcredit.prices_total_num));
                    }
                    if(result.schedule.cs_approvalcredit.prices_totalword){
                        $(".prices_totalword").text(result.schedule.cs_approvalcredit.prices_totalword);
                    }

                    $(".money_owne").text(accounting.formatMoney(result.schedule.cs_approvalcredit.money_owne));
                    $(".money_owne_word").text(result.schedule.cs_approvalcredit.money_owne_word);

                    $(".deposit_fixed").text(accounting.formatMoney(result.schedule.cs_approvalcredit.deposit_fixed));
                    $(".deposit_fixed_word").text(result.schedule.cs_approvalcredit.deposit_fixed_word);
                    
                    if(result.schedule.currency){
                        $(".currency").text(result.schedule.currency.name);
                    }
                   
                    if(result.schedule.cs_approvalcredit.approval_item){
                        var text = "";
                            $.each(result.schedule.cs_approvalcredit.approval_item,function(i,da){
                                text += "<tr> " ;
                                // text += "<td>  <button class='btn btn-danger btn-mini remove_item' value='"+da.product_id+"'><i class='icon-remove icon-white ' style='padding-right: 0px;'></i></button>   "+da.item.name+" </td>";
                                text += "<td> "+da.item.name+" </td>";
                                text +=  "<td> "+da.item.item_bacode+"  </td>";
                                text +=  "<td> "+da.item.categorys.name+" </td>";
                                text +=  "<td> "+da.qty+" </td>";
                                text +=  "<td>  "+accounting.formatMoney(da.sell_price)+" </td>";
                                text +=  "<td>  "+accounting.formatMoney(da.total_price_payment)+" </td>";
                                text += "</tr> "; 
                               
                            });

                            $(".item_list").html(text); 
                    }
                    var text_co="";
                    if(result.status == 2){
                        var status = "<p style='color:#ef4153;'>ការដឹកជញ្ជូននៅរង់ចាំ</p>";
                    }else if(result.status == 1){
                        var status = "<p style='color:#1ed445;'>ការដឹកជញ្ជូនត្រូវបានបញ្ចប់</p>";
                    }
                    text_co += "<tr><td>ស្ថានភាព</td><td>"+status+"</td></tr>";
                            // redirect from give to co
                        if(redirect == "give_product_to_co"){  
                           
                            if(result.schedule.cs_approvalcredit.date_approval){
                                text_co += "<tr><td>កាលបរិច្ឆេទធ្វើកិច្ចសន្យា</td><td><p>"+day_format_show(result.schedule.cs_approvalcredit.date_approval)+"<p></td></tr>";
                            }
                            if(result.approve_by.id){
                                text_co += "<tr><td>​ ត្រួតពិនិត្យដោយ(មន្រ្តីហិរញ្ញវត្ថុ/ប្រធានសាខា)</td><td><p>"+result.approve_by.name_kh+"<p></td></tr>";
                            }
                            if(result.staff.id){
                                text_co += "<tr><td>​ ទទួលផលិតដោយ(បុគ្គលិក/មន្រ្តីឥទាន)</td><td><p>"+result.staff.name_kh+"<p></td></tr>";
                            }
                            if(result.give_by.id){
                                text_co += "<tr><td>​ ប្រគល់ផលិតដោយ(គណនេយ្យករ/បេឡាករ)</td><td><p>"+result.give_by.name_kh+"<p></td></tr>";
                            }

                            if(result.comment){
                                text_co += "<tr><td>​ បរិយាយ </td><td><p>"+result.comment+"<p></td></tr>";
                            }
                        }else if(redirect == "give_product_to_client"){    
                            // redirect from give to client 
                            if(result.date_give_to_client){
                                text_co += "<tr><td>​ ថ្ងៃខែបើកផលិតផល </td><td><p>"+day_format_show(result.date_give_to_client)+"<p></td></tr>";
                            }
                            
                            if(result.num_give_to_client){
                                text_co += "<tr><td>​ លេខបណ្ណ </td><td><p>"+result.num_give_to_client+"<p></td></tr>";
                            }
                            if(result.staff_give){
                                text_co += "<tr><td>​ ប្រគល់ផលិតផលដោយ </td><td><p>"+result.staff_give.name_kh+"<p></td></tr>";
                            }
                            
                            if(result.comment_give_to_client){
                                text_co += "<tr><td>​ បរិយាយ </td><td><p>"+result.comment_give_to_client+"<p></td></tr>";
                            }
                        } 

                    $(".body_show").append(text_co) ; 
                },
                error: function(){} 	          
            });

}
     
</script>            
@stop()
