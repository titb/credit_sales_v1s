@extends('credit_sale.layout.master')

@section('contend')

<?php

header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0, max-age=0');

?>

<div class="container-fluid">

    <div class="row-fluid">



                    <div class="row-fluid">

                         <!-- block -->

                        <div class="block">

                            <div class="navbar navbar-inner block-header">
                            		<form class="form-search" style="margin-bottom:2px;" action="{{URL::to('branch')}}" method="GET">

											<?php $branch = DB::table('mfi_branch')->where('deleted','=',0)->get(); ?>

											<input type="text" name="search" data-required="1" placeholder="ឈ្មោះ" class="span3 m-wrap" style="margin-top:5px;" id="typeahead" data-provide="typeahead" data-source='[@foreach($branch as $row)"{{$row->brand_name}}",@endforeach""]'>

											<button type="submit" class="btn btn-success"><i class="icon-search icon-white"></i> ស្វែងរក</button>

											<div class="muted span3 pull-right" style="padding-top: 0px;"><a href="{{ url('branch/create') }}" class="btn btn-success pull-right"><i class="icon-plus icon-white"></i> បង្កើតថ្មី</a></div>

                            		</form>

                            </div>

                            <div class="block-content collapse in">

                            @if (count($errors) > 0)

						          <div class="alert alert-danger">

						            <strong>Whoops!</strong> There were some problems with your input.<br><br>

						            <ul>

						              @foreach ($errors->all() as $error)

						                <li>{{ $error }}</li>

						              @endforeach

						            </ul>

						          </div>

						    @endif

                            @if ($message = Session::get('success'))

		                        <div class="alert alert-success">

		                            <p>{{ $message }}</p>

		                        </div>

		                    @endif

		                    @if ($message = Session::get('keyerror'))

		                        <div class="alert alert-danger">

		                            <p>{{ $message }}</p>

		                        </div>

		                    @endif

                            	<div class="12">



	                            	<center>

	                            		<h3 class="cen_title khmer_Moul"> ការគ្រប់គ្រងសាខា</h3>



	                            	</center>

	                            	<legend></legend>

	                            	</div>

	                            <style>

	                            	table.table.table-bordered thead tr th{

	                            		text-align: center;

	                            	}

	                            	table.table.table-bordered tbody tr td{

	                            		text-align: center;

	                            	}

	                            </style>

                            	<table class="table table-bordered">

						              <thead style="background: rgb(251, 205, 205);">

						                <tr>

						                  <th>លេខសម្គាល់</th>

						                  <th>ឈ្មោះសាខា</th>

						                  <th>អក្សរកាត់</th>

						                  <th>លេខទំនាក់ទំនង</th>

						                  <th>អីុម៉ែល</th>

						                  <th>សេចក្តីបរិយាយ</th>

						                  <th>អាសយដ្ឋាន</th>

						                  <th>គេហទំព័រ</th>

						                  <th>រូបភាព</th>

						                  <th>សកម្មភាព</th>

						                </tr>

						              </thead>

						              <tbody>

						        @foreach($data as $key => $d)

						                <tr>



						                  <td>
						                  	<?php
								                $invID = $d->id;
								                $invID = str_pad($invID, 6, '0', STR_PAD_LEFT);
								                echo $invID;
											?>
										  </td>

						                  <td>{{ $d->brand_name }}</td>

						                  <td>{{ $d->brand_name_short }}</td>

						                  <td>
						                  	<?php
												$length = strlen($d->brand_phone);
												  if ($length == 9) {
												    echo preg_replace("/^1?(\d{3})(\d{3})(\d{3})$/", "$1-$2-$3", $d->brand_phone);
												  }
												  else{
												    echo preg_replace("/^1?(\d{3})(\d{3})(\d{3})(\d{1})$/", "$1-$2-$3-$4", $d->brand_phone);
												  }
											?>
										  </td>

						                  <td>{{ $d->brand_email }}</td>

						                  <td>{{ $d->brand_dis }}</td>

						                  <td>{{ $d->brand_address }}</td>

						                  <td>{{ $d->website }}</td>

						                  <td>{{ $d->brand_upload_image }}</td>

						                  <td>

						                  		<a href="{!! url('branch/'.$d->id.'/edit') !!}" class="btn btn-primary">កែប្រែ</a>

						                  		{!! Form::open(['method' => 'get','url' => 'branch/'.$d->id.'/deleted','style'=>'display:inline']) !!}

									            {!! Form::submit('លុប', ['class' => 'btn btn-danger']) !!}

									        	{!! Form::close() !!}



						                  </td>

						                </tr>

						        @endforeach

						              </tbody>

						            </table>

			    			</div>

						</div>

                     	<!-- /block -->

		    		</div>

	</div>

</div>





@endsection
