@extends('credit_sale.layout.master')
@section('contend')
<div class="container-fluid">
    <div class="row-fluid">
                         <!-- block -->

                        <div class="block">

                            <div class="navbar navbar-inner block-header">

                                <div class="muted pull-left"><a href="{!! url('branch') !!}"> សាខា</a> <span class="divider">|</span> បង្កើតសាខាថ្មី </div>

                               <!--  <div class="muted pull-right"style="padding-top: 0px;"><a href="{{ url('accounts/create') }}" class="btn btn-success"><i class="icon-pencil icon-white"></i> New</a></div> -->

                           </div>

                            <div class="block-content collapse in">



                              @if (count($errors) > 0)

						          <div class="alert alert-danger">

						            <strong>Whoops!</strong> There were some problems with your input.<br><br>

						            <ul>

						              @foreach ($errors->all() as $error)

						                <li>{{ $error }}</li>

						              @endforeach

						            </ul>

						          </div>

						    @endif

                            @if ($message = Session::get('success'))

		                        <div class="alert alert-success">

		                            <p>{{ $message }}</p>

		                        </div>

		                    @endif

		                     @if ($message = Session::get('keyerror'))

		                        <div class="alert alert-danger">

		                            <p>{{ $message }}</p>

		                        </div>

		                    @endif

                            	<div class="span12">

	                            	<center>

	                            		<h3 class="cen_title"> បង្កើតសាខាថ្មី </h3>

	                            	</center>

                            	</div>



                            <div class="span12" style="margin-left:0;">

                            	<form action="{{ url('branch/create') }}" method="post" enctype="multipart/form-data">

	                                {{ csrf_field() }}



	                                <div class="span6" style="margin-left:0;">

										<label class="control-label">ឈ្មោះសាខា<span class="required">*</span></label>

					  					<div class="controls">

					  						<input type="text" id="brand_name" class="span12 m-wrap" name="brand_name" data-required="1" value="{{ old('brand_name') }}"/>

					  					</div>

									</div>

									<div class="span6">

										<label class="control-label">អក្សរកាត់<span class="required">*</span></label>

					  					<div class="controls">

					  						<input type="text" id="brand_name_short" class="span12 m-wrap" name="brand_name_short" data-required="1" value="{{ old('brand_phone') }}"/>

					  					</div>

									</div>

									<div class="span6" style="margin-left:0;">

										<label class="control-label">លេខទំនាក់ទំនង<span class="required">*</span></label>

					  					<div class="controls">

					  						<input type="text" id="brand_phone" class="span12 m-wrap" name="brand_phone" data-required="1" value="{{ old('brand_phone') }}"/>

					  					</div>

									</div>



									<div class="span6">

										<label class="control-label">អ៊ីម៉ែល<span class="required">*</span></label>

					  					<div class="controls">

					  						<input type="email" id="brand_email" class="span12 m-wrap" name="brand_email" data-required="1" value="{{ old('brand_email') }}"/>

					  					</div>

									</div>

									<div class="span6" style="margin-left:0;">

										<label class="control-label">សេចក្តីបរិយាយ<span class="required">*</span></label>

					  					<div class="controls">

					  						<input type="text" id="brand_dis" class="span12 m-wrap" name="brand_dis" data-required="1" value="{{ old('brand_dis') }}"/>

					  					</div>

									</div>



									<div class="span6">

										<label class="control-label">អាសយដ្ឋាន<span class="required">*</span></label>

					  					<div class="controls">

					  						<input type="text" id="brand_address" class="span12 m-wrap" name="brand_address" data-required="1" value="{{ old('brand_address') }}"/>

					  					</div>

									</div>

									<div class="span6" style="margin-left:0;">

										<label class="control-label">គេហទំព័រ<span class="required">*</span></label>

					  					<div class="controls">

					  						<input type="text" id="website" class="span12 m-wrap" name="website" data-required="1" value="{{ old('website') }}"/>

					  					</div>

									</div>



									<div class="span6">

										<label class="control-label">រូបភាព<span class="required">*</span></label>

				  						<div class="controls">

				  							<input type="file" name="brand_upload_image" class="input-file uniform_on" id="fileInput" multiple title="រូបភាពត្រូវតែជារូបភាពដែលថតចេញមកពីការបើកGPSថត"/>

				  						</div>

				  					</div>

				  					<br/>



									<div class="span12" style="margin-left:0; margin-top:15px;">

										<center>

											<button type="submit" class="btn btn-success">បញ្ចូល</button>

										</center>

									</div>

								</form>

							</div>



			    			</div>

						</div>
                     	<!-- /block -->
		    		</div>
	</div>

</div>



<script type="text/javascript">

	/*$(document).ready(function(){

		$('#add').click(function(){

			var img = '<div class="controls" id="addimage">' +

				  	'<input type="file" name="image_uploade_id_familly[]" class="input-file uniform_on" id="fileInput" title="រូបភាពត្រូវមានទំហំសមល្មម"/>' +

				  	'</div>';

			$('#addimage').append(img);

		});

	});*/

</script>



@endsection
