@extends('credit_sale.layout.master')

@section('contend')

	<div class="container-fluid dash-fluid">

        <div class="row-fluid">
                <div class="block dash_back">
                    <div class="navbar navbar-inner block-header">
                        <div class="muted pull-left">Dashboard</div>
                        <!-- <div class="pull-right"><span class="badge badge-warning">View More</span> -->

                        </div>
                    </div>
                    <div class="block-content mar_span3 collapse in ">
                <!-- start row 1 -->
                        <div class="span3">
                            <div class="switch-right-grid">  
                                <div class="switch-right-grid1">
                                    <h3>TOTAL ITEM SALE</h3>
                                   
                                    <ul>
                                        <li>Total Payment Sold: <b>{{ number_format($data['data_payment'], 2) }}</b> USD</li>
                                        <li>Total Items Sold: <b>{{$data['data_total_item'] }} </b>Items</li>
                                    </ul>
                                </div>
                            </div>  
                        </div>
                        <div class="span3">
                            <div class="switch-right-grid">  
                                <div class="switch-right-grid1">
                                    <h3>TOTAL SALE DIRECT</h3>
                                   
                                    <ul>
                                        <li>Total Payment Sold: <b>{{ number_format($data['data_payment_sale_chash'], 2) }}</b> USD</li>
                                        <li>Total Items Sold: <b>{{$data['data_total_item_sale_chash'] }} </b>Items</li>
                                    </ul>
                                </div>
                            </div>  
                        </div>
                        <div class="span3">
                            <div class="switch-right-grid">  
                                <div class="switch-right-grid1">
                                    <h3>TOTAL SALE CREDIT</h3>
                                   
                                    <ul>
                                        <li>Total Payment Sold: <b>{{ number_format($data['data_payment_sale_credit'], 2) }}</b> USD</li>
                                        <li>Total Items Sold: <b>{{$data['data_total_item_sale_credit'] }} </b>Items</li>
                                    </ul>
                                </div>
                            </div>  
                        </div>
                        <div class="span3">
                            <div class="switch-right-grid">  
                                <div class="switch-right-grid1">
                                    <h3>TOTAL ITEM IN STOCK</h3>
                                   
                                    <ul>
                                        <li>Total Payment Sold: <b>{{ number_format($data['total_item_in_stock_payment'], 2) }}</b> USD</li>
                                        <li>Total Items Sold: <b>{{$data['total_item_in_stock'] }} </b>Items</li>
                                    </ul>
                                </div>
                            </div>  
                        </div>

                <!-- end row 1 -->
               <!-- start row 1 -->
                        <!-- <div class="span3" style="margin-left:0px;">
                            <div class="switch-right-grid">  
                                <div class="switch-right-grid1">
                                    <h3>TOTAL CUSTOMER</h3>
                                   
                                    <ul>
                                        <li>Earning: $400 USD</li>
                                        <li>Items Sold: 20 Items</li>
                                        <li>Last Hour Sales: $34 USD</li>
                                    </ul>
                                </div>
                            </div>  
                        </div>
                        <div class="span3">
                            <div class="switch-right-grid">  
                                <div class="switch-right-grid1">
                                    <h3>PAYMENT BY CREDIT</h3>
                                   
                                    <ul>
                                        <li>Earning: $400 USD</li>
                                        <li>Items Sold: 20 Items</li>
                                        <li>Last Hour Sales: $34 USD</li>
                                    </ul>
                                </div>
                            </div>  
                        </div>
                        <div class="span3">
                            <div class="switch-right-grid">  
                                <div class="switch-right-grid1">
                                    <h3>TOTAL SALE CREDIT</h3>
                                   
                                    <ul>
                                        <li>Earning: $400 USD</li>
                                        <li>Items Sold: 20 Items</li>
                                        <li>Last Hour Sales: $34 USD</li>
                                    </ul>
                                </div>
                            </div>  
                        </div>
                        <div class="span3">
                            <div class="switch-right-grid">  
                                <div class="switch-right-grid1">
                                    <h3>TOTAL ITEM</h3>
                                   
                                    <ul>
                                        <li>Earning: $400 USD</li>
                                        <li>Items Sold: 20 Items</li>
                                        <li>Last Hour Sales: $34 USD</li>
                                    </ul>
                                </div>
                            </div>  
                        </div> -->

                <!-- end row 1 -->
                    </div>
                </div>


        </div>

    </div>
@stop()