@extends('credit_sale.layout.master')

@section('contend')

	

<div class="container-fluid">

            <div class="row-fluid">

                		

                     <!-- validation -->

                    <div class="row-fluid">

                         <!-- block -->

                        <div class="block">

                            <div class="navbar navbar-inner block-header">

                                <div class="muted pull-left"><a href="{!! url('interest_rate') !!}">Interest Rate</a> <span class="divider">/</span>Create Iterest Rate</div>

                            	<div class="muted pull-right" style="padding-top: 0px;"><a href="{{ url('interest_rate/create') }}" class="btn btn-success pull-right"><i class="icon-plus icon-white"></i> New</a></div>

                            </div>

                            <div class="block-content collapse in">

                            @if (count($errors) > 0)

						          <div class="alert alert-danger">

						            <strong>Whoops!</strong> There were some problems with your input.<br><br>

						            <ul>

						              @foreach ($errors->all() as $error)

						                <li>{{ $error }}</li>

						              @endforeach

						            </ul>

						          </div>

						    @endif

                            @if ($message = Session::get('success'))

		                        <div class="alert alert-success">

		                            <p>{{ $message }}</p>

		                        </div>

		                    @endif

		                     @if($message = Session::get('keyerror'))

		                        <div class="alert alert-danger">

		                            <p>{{ $message }}</p>

		                        </div>

		                    @endif

                            <div class="span12">

                            		<h3 class="cen_title"> Interest Rate</h3>

                            	<legend></legend>

                            </div>

                            	<form action="{{ url('interest_rate/create') }}" method="post" enctype="multipart/form-data">

	                                {{ csrf_field() }}

	                                <div class="span12" style="padding-left: 50px;">

	             

										  <div class="control-group">

			  								<label class="control-label" >Method<span class="required">*</span></label>

			  								<div class="controls">

			  									<select name="module_interest_id" class="span9 m-wrap">
													@foreach($modules as $mo)
			  											<option value="{{ $mo->id }}" >{{ $mo->display_name_kh }}</option>
													@endforeach
			  					  				</select>

			  								</div>

			  								<label class="control-label">ចំនួនលុយចាប់ពី<span class="required">*</span></label>

			  								<div class="controls">

			  									<input type="text" name="size_money_from" data-required="1" class="span9 m-wrap"/>

			  								</div>

			  								<label class="control-label">រហូតដល់ចំនួនលុយ<span class="required">*</span></label>

			  								<div class="controls">

			  									<input type="text" name="size_money_to" data-required="1" class="span9 m-wrap"/>

			  								</div>

			  								<label class="control-label">អត្រាកាប្រាក់ប្រចាំថ្ងៃនៅភួមិ(%)<span class="required">*</span></label>

			  								<div class="controls">

			  									<input type="text" name="day_rate_villige" data-required="1" class="span9 m-wrap"/>

			  								</div>

			  								<label class="control-label">អត្រាកាប្រាក់ប្រចាំថ្ងៃនៅសាខា(%)<span class="required">*</span></label>

			  								<div class="controls">

			  									<input type="text" name="day_rate_brand" data-required="1" class="span9 m-wrap"/>

			  								</div>

			  								<label class="control-label">អត្រាកាប្រាក់ប្រចាំសប្តាហ៍នៅភួមិ(%)<span class="required">*</span></label>

			  								<div class="controls">

			  									<input type="text" name="weekly_rate_villige" data-required="1" class="span9 m-wrap"/>

			  								</div>

											<label class="control-label">អត្រាកាប្រាក់ប្រចាំសប្តាហ៍នៅសាខា(%)<span class="required">*</span></label>

			  								<div class="controls">

			  									<input type="text" name="weekly_rate_brand" data-required="1" class="span9 m-wrap"/>

			  								</div>

											<label class="control-label">អត្រាកាប្រាក់ប្រចាំ២សប្តាហ៍នៅភួមិ(%)<span class="required">*</span></label>

			  								<div class="controls">

			  									<input type="text" name="two_weekly_rate_villige" data-required="1" class="span9 m-wrap"/>

			  								</div>

			  								<label class="control-label">អត្រាកាប្រាក់ប្រចាំ២សប្តាហ៍នៅសាខា(%)<span class="required">*</span></label>

			  								<div class="controls">

			  									<input type="text" name="two_weekly_brand" data-required="1" class="span9 m-wrap"/>

			  								</div>

											<label class="control-label">អត្រាកាប្រាក់ប្រចាំខែនៅភូមិ(%)<span class="required">*</span></label>

			  								<div class="controls">

			  									<input type="text" name="monthly_rate_villige" data-required="1" class="span9 m-wrap"/>

			  								</div>

											<label class="control-label">អត្រាកាប្រាក់ប្រចាំខែនៅសាខា(%)<span class="required">*</span></label>

			  								<div class="controls">

			  									<input type="text" name="monthly_rate_brand" data-required="1" class="span9 m-wrap"/>

			  								</div>

			  								<label class="control-label">របៀបសង់​<span class="required">*</span></label>

			  								<div class="controls">

			  									<input type="text" name="repayment_disction" data-required="1" class="span9 m-wrap"/>

			  								</div>

											<label class="control-label">រយះពេលសង់(ខែ)<span class="required">*</span></label>

			  								<div class="controls">

			  									<input type="text" name="duration" data-required="1" class="span9 m-wrap"/>

			  								</div>

											<label class="control-label">Status</label>

			  								<div class="controls">

			  									<select name="status" class="span9 m-wrap">

			  										<option value="1">Active</option>

			  										<option value="2">Not Active</option>

			  									</select>

			  								</div>

											<label class="control-label">Note</label>

			  								<div class="controls">

			  									<input type="text" name="note" data-required="1" class="span9 m-wrap"/>

			  								</div>

									</div>



									<div class="span9">

										<center>

											<button type="submit" class="btn btn-success">Save</button>

										</center>

									</div>

								</form>

			    			</div>

						</div>

                     	<!-- /block -->

		    		</div>

                     <!-- /validation -->

                </div>

            </div>	





	

@stop()