@extends('credit_sale.layout.master')
@section('contend')

	

<div class="container-fluid">

            <div class="row-fluid">

               

                     <!-- validation -->

                    <div class="row-fluid">

                         <!-- block -->

                        <div class="block">

                            <div class="navbar navbar-inner block-header">

                            </div>

                            <div class="block-content collapse in">

                            @if (count($errors) > 0)

						          <div class="alert alert-danger">

						            <strong>Whoops!</strong> There were some problems with your input.<br><br>

						            <ul>

						              @foreach ($errors->all() as $error)

						                <li>{{ $error }}</li>

						              @endforeach

						            </ul>

						          </div>

						    @endif

                            @if ($message = Session::get('success'))

		                        <div class="alert alert-success">

		                            <p>{{ $message }}</p>

		                        </div>

		                    @endif

		                    @if($message = Session::get('keyerror'))

		                        <div class="alert alert-danger">

		                            <p>{{ $message }}</p>

		                        </div>

		                    @endif



                             	<center>

			                        <h3 class="cen_title text-center khmer_Moul">ការកំណត់អត្រាការប្រាក់ថ្មី</h3> 
			                    @if(count($data) == null)   
			                    	<div class="muted span3 pull-right" style="margin-bottom:5px;"><a href="{{ url('new_interest_rate/create') }}" class="btn btn-success pull-right"><i class="icon-plus icon-white"></i> បង្កើតថ្មី</a></div>
			                    @else
			                        <a href="{{url('service_interest_rate')}}" class="btn btn-primary pull-right" style="margin-left: 5px;">អត្រាសេវាផ្សេងៗ</a>
			                    	<div class="muted span3 pull-right" style="padding-top: 0px;margin-bottom:5px;pointer-events: none;"><a href="#" class="btn btn-success pull-right" disabled="disabled"><i class="icon-plus icon-white"></i> បង្កើតថ្មី</a></div>
			                    @endif
			                    </center>

                            	<legend></legend>



                            <style>

                            	

                            </style>



                            

							    <table class="table table-bordered">

							      <thead>
							        <tr class="header">
							            <th>ល.រ</th>
							            <th>របៀបសង</th>
							            <th>អត្រាការប្រាក់ ភូមិ</th>
							            <th>អត្រាការប្រាក់ សាខា</th>
							            <th>ស្ថានភាព</th>
							        </tr>
							      </thead>

							      <tbody>
							      @foreach($data as $key => $d)
							      	<tr>
							      		<td>{{ $key+1 }}</td>
							      		<td>
							      			@if($d->type_dura == 1)
							      				ខែ
							      			@endif
							      		</td>
							      		<td>{{ $d->villige }} %</td>
							      		<td>{{ $d->branch }} %</td>
							      		<td>
							      		@if(Auth::user()->groups->first()->hasPermission(['create-user-groups']))
							      			<a href="{!! url('new_interest_rate/'.$d->id.'/edit') !!}" class="btn btn-info">កែប្រែ</a>
							      		
							      			@if(Auth::user()->groups->first()->hasPermission(['list-user-groups']))

						                  		{!! Form::open(['method' => 'post','url' => 'new_interest_rate/'.$d->id.'/delete','style'=>'display:inline']) !!}

									            	{!! Form::submit('លុប', ['class' => 'btn btn-danger']) !!}

									        	{!! Form::close() !!}

								        	@endif

								        @endif
							      		</td>
							      	</tr>
							      @endforeach
							      </tbody>

							    </table>
                            	<!-- Pagination -->
								<div class="pagination text-right">{{ $data->links() }}</div>

			    			</div>

						</div>

                     	<!-- /block -->

		    		</div>

                     <!-- /validation -->

                </div>

            </div>	



	

@stop()