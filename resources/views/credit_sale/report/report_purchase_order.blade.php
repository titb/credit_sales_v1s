@extends('credit_sale.layout.master')
@section('contend')
@include('credit_sale.autocompleted_script')
<style>
    p#payment_back {
        color: #08c;
        margin-top: 10px;
    }

</style>
<meta name="csrf-token" content="{{ csrf_token() }}">
<script src="{{ URL::to('assets/accounting.min.js')}}"></script>
<!-- container-fluid  --> 	
<div class="container-fluid content_top_mar" >

<!-- validation -->
<div class="row-fluid">
    <!-- block -->
    <div class="block">
    @include('errors.error')
        <div class="navbar navbar-inner block-header">
            <div class="span12" style="margin-bottom: 3px;">
                <div class="muted pull-left" style="padding-top: 5px;">Report Purchase Order Product</div>
                <a href="{{url('export_report_po')}}" class="btn btn-primary b_search pull-right"  value="b_search">បញ្ចូលទៅកាន់ Excel</a>
                <a href="{{url('export_report')}}" class="btn btn-danger pull-right"  value="b_search" style="margin-right: 4px;">ព្រីន</a>
            </div>
        </div>
        <div class="block-content collapse in">
            <?php 
                $url = "{{url('report/report_po_product')}}";
            
            ?>
            @include('credit_sale.report.search_form_receiving')
                <div class="span12"  style="margin-left: 0px;">
                    <table class="table table-bordered">
                        <thead>
                        <tr> 
                            <th>#</th>
                            <th>លេខកូដ</th>
                            <th>ឈ្មោះអ្នកផ្គត់ផ្គង់</th>
                            <th>ចំនួនផលិតផលPO</th>
                            <th>ប្រាក់កក់</th>
                            <th>ប្រាក់ដែលនៅជំពាក់</th>
                            <th>ចំនួនប្រាក់សរុប</th>
                            <th>ថ្ងៃបញ្ជាទិញ</th>
                            <th>បុគ្គលិកទទួល</th>  
                        </tr>
                        </thead>
                        <tbody id="list_item">
                            
                        </tbody>
                    </table>
                    <div class="pagination text-right"></div>
                </div>
            </div>

        </div>
    <!-- /block -->
</div>
<!-- /validation -->

</div>
<script type="text/javascript">

	$(document).ready(function(){

        function day_format_show(date_format){
                    var d = new Date(date_format);

                    var year_n = d.getFullYear();
                    var month_n = d.getMonth() + 1;
                    var day_n = d.getDate();
                    if(month_n > 10){
                        month_n = month_n;
                    }else{
                        month_n = "0"+month_n; 
                    }
                
                    if(day_n > 10){
                        day_n = day_n;
                    }else{
                        day_n = "0"+day_n; 
                    }

            return  day_n +"-"+month_n+"-"+year_n;
        }

        var numpage = 1;
        var url_edit = "{{route('report/report_po_product_json')}}";
        get_page(url_edit.numpage);
        // $(document).ajaxComplete(function(){
            $(".b_search").click(function(){
                var submit_search = $(this).val();
                var n = 1;
                
                var url_index2 = submit_search; 		
                get_page(url_index2,numpage = n);
            });
            $(document).on('click','.pag',function(){
                var numpage = $(this).text();   
                get_page(url_edit,numpage);
            });

            $(".pre").click(function(){
                var numpage = $(this).find(".pre_in").val();
                get_page(url_edit,numpage);
            });
        // });

        function get_page(url,n){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })

            var from_date = $(".from_date").val();
            var to_date = $(".to_date").val(); 
            var brand_name = $(".brand_name").val(); 
            var currency = $(".currency").val(); 
            var supplier_name = $(".supplier_name").val(); 
            var purches_id = $(".purches_id").val(); 
            var staff_name = $(".staff_name").val();

            if(url === "b_search" || (from_date !== "" && to_date !== "") || brand_name !== "" || currency !== "" || supplier_name !== "" || purches_id !== "" || staff_name !== ""){
                var url_i = "{{route('report/report_po_product_json')}}";
                var forData = {
                    from_date: from_date,
                    to_date: to_date,
                    brand_name: brand_name,
                    currency: currency,
                    supplier_name: supplier_name,
                    purches_id: purches_id,
                    staff_name: staff_name,
                    submit_search: $(".b_search").val()
                }                
                var url_index = url_i+"?from_date="+from_date+"&to_date="+to_date+"&brand_name="+brand_name+"&currency="+currency+"&supplier_name="+supplier_name+"&purches_id="+purches_id+"&staff_name="+staff_name+"&submit_search=b_search&page="+n;     
            }else{
                var forData = {};
                var url_index = url_edit+"?page="+n;
            }


            // alert(url_edit);
            var client;
            // var forData = {};
            var out = "";
            $.ajax({
                    type: "GET",
                    url: url_edit, 
                    dataType: "json",
                    data: forData,
                    success: function(result ,xhr){
                        console.log(result);
                        var myJSON = JSON.stringify(result); 
                        var text ="";
                        // var i = 0;
                        $.each(result.data.data , function(index, field){
                            var il = result.data.from  + index;
                            var url_show_re = "{{url('credit_sales_purchas_order')}}/"+field.id+"/invoice";
                            text +=  "<tr>";
                            text += " <td>"+ il +"</td>";
                            text += " <td> <a href='"+url_show_re+"'>"+ field.invoice_num +"</a></td>";
                            text += " <td>"+ field.suppliers.name +"</td>";
                            text += " <td>"+ field.qty_order +"</td>"; 
                            text += " <td>"+ accounting.formatMoney(field.deposit_price) +"</td>";
                            text += " <td>"+ accounting.formatMoney(field.remain_price) +"</td>";
                            text += " <td>"+ accounting.formatMoney(field.dute_total_price) +"</td>";
                            text += " <td>"+ day_format_show(field.order_date) +"</td>";
                            text += " <td>"+ field.staff_order.name_kh +"</td>";
                            text += "</tr>";
                        });

                        $("#list_item").html(text);    
  
                        var page = "";
                        if(result.data.prev_page_url === null){
                            var pr_url = result.data.current_page;
                        }else{
                            var pr_url = result.data.current_page -1;
                        }
                        page += "<a class='pre'>&laquo;<input type='hidden' class='pre_in' value='"+pr_url+"' ></a>";
                        for(var x = 1; x <= result.data.last_page; x ++  ) {
                            if(result.data.current_page === x){
                                page += "<a class='pag active' >"+x+"</a>";
                            }else{
                                page += "<a class='pag' >"+x+"</a>";
                            }
                        }
                        if(result.data.next_page_url === null){
                            var ne_url = result.data.current_page;
                        }else{
                            var ne_url = result.data.current_page +1;
                        }
                        page += "<a class='pre'>&raquo;<input type='hidden' class='pre_in' value='"+ne_url+"' ></a>";
                        $(".pagination").html(page );
                    },
                    error: function (result ) {
                        console.log(result.stutus);
                    }

            });

        }
						      
				
	});
</script>
@endsection
