@extends('credit_sale.layout.master')
@section('contend')
@include('credit_sale.autocompleted_script')
<style>
    p#payment_back {
        color: #08c;
        margin-top: 10px;
    }

</style>
<!-- container-fluid  --> 	
<div class="container-fluid content_top_mar" >

<!-- validation -->
<div class="row-fluid">
    <!-- block -->
    <div class="block">
    @include('errors.error')
        <div class="navbar navbar-inner block-header">
            <div class="span12" style="margin-bottom: 10px;">
                <div class="muted pull-left" style="padding-top: 5px;"><span> {{$title}} </span></div>
                <a  class="btn btn-info export_excel pull-right" style="margin-top:-5px;">Export to Excel</a>
                <a href="#" class="btn btn-danger print_excel pull-right" onclick="myFunction('report-incom')" style="margin-right: 4px; margin-top:-5px;" >Print</a>
            </div>    
        </div>
        <div class="block-content collapse in">
        <?php 
            $url = url('report/payment_back/report_summery_payment_back?reset=reset');
        ?>
            @include('credit_sale.report.search_form')
                <div class="span12"  style="margin-left: 0px;" id="report-incom"> 
                <style type="text/css">
                            body {
                                -webkit-print-color-adjust: exact;
                            }
                        @media print
                        {    
                            .no-print, .no-print *{display: none !important;}
                            .color{color:red !important;}
                            tr th.row-background{background:#438eb9 !important;}
                        }
                        #co{
                            text-align: center;
                        }
                        .no_show{
                            display: none;
                        }
                    </style>
                    <style type="text/css" media="print">

                        @page { size: landscape; margin: 10px; margin-top: 10px;margin-bottom: 20px;}

                        .table-print{

                        width: 100%;

                        font-family: 'Battambang';

                        }

                        table {

                        width: 100%;

                        border-left: 0;

                        -webkit-border-radius: 4px;

                        -moz-border-radius: 4px;

                        border-radius: 4px;

                        }

                        table tr th, table tr td {

                            border: 1px solid #000;

                            padding: 5px;

                            line-height: 20px;

                            vertical-align: center;

                            font-size: 10px;

                        }

                        .no_print{
                                display: none;
                        }

                        .title_print{
                            font-family: 'Moul';
                        }

                        a{
                            text-decoration: none;
                            color: #000;
                        }
                        .font-weight{
                            font-weight: bold;
                        }
                        .print_show{
                            display: block;
                        }

                    </style>
                    <table class="table table-bordered table-print"  cellpadding="0" cellspacing="0" >
                        <thead  style="background: rgb(251, 205, 205);">
                        <tr class="header"> 
                            
                            <th></th>
                            <th>សរុបប្រាក់ដើមបានសង់</th>
                            <th>សរុបប្រាក់ការបានសង់</th>
                            <th>សរុបប្រាក់ពិន័យ</th>
                            <th>សរុបប្រាក់ផ្សេងៗ</th>
                            <th>សរុបចំនួនទឹកសង់ប្រាក់</th>
                        </tr>
                        </thead>
                        <tbody id="list_item" class="list_item">
                            
                        </tbody>
                    </table>
                </div>    
            </div>

        </div>
    <!-- /block -->
</div>
<!-- /validation -->

</div>

<script type="text/javascript">

	$(document).ready(function(){

        var numpage = 1;
        var url_edit = "{{route('report/payment_back/get_summery_total_json')}}";
        get_page(url_edit.numpage);
        // $(document).ajaxComplete(function(){
            $(".b_search").click(function(){
                var submit_search = $(this).val();
                var n = 1;
                
                var url_index2 = submit_search; 		
                get_page(url_index2,numpage = n);
            });

        function get_page(url,n){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })
            
            var from_date = $(".from_date").val();
            var to_date = $(".to_date").val(); 
            var brand_name = $(".brand_name").val(); 
            var currency = $(".currency").val(); 
            var client_name = $(".client_name").val(); 
            var sale_id = $(".sale_id").val(); 
            var staff_name = $(".staff_name").val();  

            //alert(item_search);
            if(url === "b_search" || (from_date !== "" && to_date !== "") || brand_name !== "" || currency !== "" || client_name !== "" || sale_id !== "" || staff_name !== ""){
                var url_i = "{{route('report/payment_back/get_summery_total_json')}}";
                var forData = {
                    from_date: from_date,
                    to_date: to_date,
                    brand_name: brand_name,
                    currency: currency,
                    client_name: client_name,
                    sale_id: sale_id,
                    staff_name: staff_name,
                    submit_search: $(".b_search").val()
                }                
                var url_index = url_i+"?from_date="+from_date+"&to_date="+to_date+"&brand_name="+brand_name+"&currency="+currency+"&client_name="+client_name+"&sale_id="+sale_id+"&staff_name="+staff_name+"&submit_search=b_search&page="+n;     
                var url_excel = "{{route('report/payment_back/report_summery_payment_back')}}?from_date="+from_date+"&to_date="+to_date+"&brand_name="+brand_name+"&currency="+currency+"&client_name="+client_name+"&sale_id="+sale_id+"&staff_name="+staff_name+"&submit_search="+url+"&export=excel";  

            }else{
                var forData = {};
                var url_index = url_edit+"?page="+n;
                var url_excel = "{{route('report/payment_back/report_summery_payment_back')}}?export=excel";  
            }
        
            var client;
            // var forData = {};
            var out = "";
            $.ajax({
                    type: "GET",
                    url: url_index, 
                    dataType: "json",
                    data: forData,
                    success: function(result ,xhr){
                        console.log(result);

                        var text = "";
                        $(".export_excel").attr('href',url_excel);
                        if(result){
                                text +="<tr>";
                                    text +="<td style='background: rgb(251, 205, 205);'>សរុបប្រាក់ដុល្លា</td>";
                                    // text +="<td>Date</td>";
                                    text +="<td>"+accounting.formatMoney(result.data_query.total_available_total_pay_cost)+"</td>";
                                    text +="<td>"+accounting.formatMoney(result.data_query.total_available_total_pay_interest)+"</td>";
                                    text +="<td>"+accounting.formatMoney(result.data_query.total_panalty)+"</td>";
                                    text +="<td>"+accounting.formatMoney(result.data_query.total_other_payment)+"</td>";
                                    text +="<td>"+accounting.formatMoney(result.data_query.total_available_total_payment)+"</td>";
                                text +="</tr>";
                                text +="<tr >";
                                    text +="<td style='background: rgb(251, 205, 205);' >សរុបប្រាក់រៀល</td>";
                                    // text +="<td>Date</td>";
                                    text +="<td>"+accounting.formatMoney(result.data_query2.total_available_total_pay_cost)+"</td>";
                                    text +="<td>"+accounting.formatMoney(result.data_query2.total_available_total_pay_interest)+"</td>";
                                    text +="<td>"+accounting.formatMoney(result.data_query2.total_panalty)+"</td>";
                                    text +="<td>"+accounting.formatMoney(result.data_query2.total_other_payment)+"</td>";
                                    text +="<td>"+accounting.formatMoney(result.data_query2.total_available_total_payment)+"</td>";
                                text +="</tr>";
                            // $.each(result, function(k,sale){
                            //     var il = result.data.from  + k; 
                            //     var status;
                                // text +="<tr>";
                                //     text +="<td>"+il+"</td>";
                                //     text +="<td>"+sale.cs_schedule.schedule_number+"</td>";
                                //     text +="<td>"+sale.cs_schedule.cs_client.kh_username+"</td>";
                                //     text +="<td>"+accounting.formatMoney(sale.available_total_pay_cost)+"</td>";
                                //     text +="<td>"+accounting.formatMoney(sale.available_total_pay_interest)+"</td>";
                                //     text +="<td>"+accounting.formatMoney(sale.panalty)+"</td>";
                                //     text +="<td>"+accounting.formatMoney(sale.other_payment)+"</td>";
                                //     text +="<td>"+accounting.formatMoney(sale.available_total_payment)+"</td>";
                                //     text +="<td>"+day_format_show(sale.available_date_payment)+"</td>";
                                //     text +="<td>"+sale.cs_staff.name_kh+"</td>";
                                //     text +="<td>"+sale.cs_schedule.currency.name_kh+"</td>";
                                // text +="</td>";
                           // });

                            $('#list_item').html(text);
                        }else{
                            window.location = "{{url('report/clients/report_sale_by_credit')}}";
                        }
                        
                    },
                    error: function (result ) {
                        console.log(result.stutus);
                    }

            });
        }

	});
function myFunction(printpage)

    {

    var headstr = "<html><head><title></title></head><body>";

    var footstr = "</body>";

    var newstr = document.all.item(printpage).innerHTML;

    var oldstr = document.body.innerHTML;

    document.body.innerHTML = headstr+newstr+footstr;

    window.print();

    document.body.innerHTML = oldstr;

    return false;

    }


</script>

@endsection
