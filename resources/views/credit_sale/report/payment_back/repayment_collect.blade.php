@extends('credit_sale.layout.master')
@section('contend')
@include('credit_sale.autocompleted_script')
<style>
    p#payment_back {
        color: #08c;
        margin-top: 10px;
    }

</style>
<meta name="csrf-token" content="{{ csrf_token() }}">
<script src="{{ URL::to('assets/accounting.min.js')}}"></script>
<!-- container-fluid  --> 	
<div class="container-fluid content_top_mar" >
    
<!-- validation -->
<div class="row-fluid">
    
    <!-- block -->
    <div class="block">

        @include('errors.error')
        <div class="navbar navbar-inner block-header">
            <div class="span12" style="margin-bottom: 3px;">
                <div class="muted pull-left" style="padding-top: 5px;">{{$title}}</div>
                <a href="{{url('export_report')}}" class="btn btn-primary b_search pull-right"  value="b_search">បញ្ចូលទៅកាន់ Excel</a>
                <a href="{{url('export_report')}}" class="btn btn-danger pull-right"  value="b_search" style="margin-right: 4px;">ព្រីន</a>
            </div>
            
            
        </div>
        <div class="block-content collapse in">
        <?php 
            $url = "url('report/report_schedules_json')";
        ?>
            @include('credit_sale.report.search_form')
                <div class="span12" style="margin-left: 0px;">
                    

                    <table class="table table-bordered">
                        <thead>
                        <tr> 
                            <th>#</th>
                            <th>លេខកូដតារា</th>
                            <th>ឈ្មោះអតិថិជន</th>
                            <th>កាលបរិច្ឆេទសង</th>
                            <th>ប្រាក់ដើម</th>
                            <th>ប្រាក់ការ</th>
                            <th>ប្រាក់ដែលត្រូវបង់</th>
                            <th>ប្រាក់ពិន័យ</th>
                            <th>បុគ្គលិក</th>
                            <th>រូបិយប័ណ្ណ</th>  
                        </tr>
                        </thead>
                        <tbody id="list_item" class="list_item">
                            
                        </tbody>
                        <tfoot>
                            <tr style="color:red;">
                                <td colspan="2"> សរុបប្រាក់ដុល្លា </td>
                                <td colspan="2" class="total_count_collect_time"></td>
                                <td class="total_money_owne_cost"></td>
                                <td class="total_money_owne_interest"></td>
                                <td class="total_money_owne_total_pay"></td>
                                <td class="total_money_owne_total_panalty"></td>
                                <td colspan="2" style="background-color:#ccc;"></td>
                            </tr>
                            <tr style="color:red;">
                                <td colspan="2" > សរុបប្រាក់រៀល </td>
                                <td colspan="2" class="total_count_collect_time_kh"></td>
                                <td class="total_money_owne_cost_kh"></td>
                                <td class="total_money_owne_interest_kh"></td>
                                <td class="total_money_owne_total_pay_kh"></td>
                                <td class="total_money_owne_total_panalty_kh"></td>
                                <td colspan="2" style="background-color:#ccc;"></td>
                            </tr>
                        </tfoot>
                    </table>
                    <div class="pagination text-right"></div>
                </div>
            </div>

        </div>
    <!-- /block -->
</div>
<!-- /validation -->

</div>

<script type="text/javascript">

	$(document).ready(function(){
        function day_format_show(date_format){
            var d = new Date(date_format);

            var year_n = d.getFullYear();
            var month_n = d.getMonth() + 1;
            var day_n = d.getDate();
            if(month_n > 10){
                month_n = month_n;
            }else{
                month_n = "0"+month_n; 
            }
            
            if(day_n > 10){
                day_n = day_n;
            }else{
                day_n = "0"+day_n; 
            }

            return  day_n +"-"+month_n+"-"+year_n;
        }
        
        
        var numpage = 1;
        var url_edit = "{{route('report/report_repayment_collect_json')}}";
        get_page(url_edit,numpage);
        // $(document).ajaxComplete(function(){
            $(".b_search").click(function(){
                
                var submit_search = $(this).val();
              //  alert(submit_search);
                var n = 1;
                
                var url_index2 = submit_search; 		
                get_page(url_index2,numpage = n);
            });
            $(document).on('click','.pag',function(){
                var numpage = $(this).text();   
                get_page(url_edit,numpage);
            });

            $(document).on('click','.pre',function(){
                var numpage = $(this).find(".pre_in").val();
                get_page(url_edit,numpage);
            });
        // });


        function get_page(url,n){  
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })
            
            var from_date = $(".from_date").val();
            var to_date = $(".to_date").val(); 
            var brand_name = $(".brand_name").val(); 
            var currency = $(".currency").val(); 
            var client_name = $(".client_name").val(); 
            var schedule_id = $(".sale_id").val(); 
            var staff_name = $(".staff_name").val();  

            //alert(item_search);
            if(url === "b_search" || (from_date !== "" && to_date !== "") || brand_name !== "" || currency !== "" || client_name !== "" || schedule_id !== "" || staff_name !== ""){
                var url_i = "{{route('report/report_repayment_collect_json')}}";
                var forData = {
                    from_date: from_date,
                    to_date: to_date,
                    brand_name: brand_name,
                    currency: currency,
                    client_name: client_name,
                    schedule_id: schedule_id,
                    staff_name: staff_name,
                    submit_search: url
                }   

                ///alert(JSON.stringify(forData));        
                var url_index = url_i+"?from_date="+from_date+"&to_date="+to_date+"&brand_name="+brand_name+"&currency="+currency+"&client_name="+client_name+"&schedule_id="+schedule_id+"&staff_name="+staff_name+"&submit_search=b_search&page="+n;     
// alert(url_index);
  }else{
                var forData = {};
                var url_index = url_edit+"?page="+n;
            }

            // alert(url_edit);
            var client;
            // var forData = {};
            var out = "";
            $.ajax({
                    type: "GET",
                    url: url_index, 
                    dataType: "json",
                    data: forData,
                    success: function(result ,xhr){
                        console.log(result);

                        var text = "";
                       // var i = 0;   
                        if(result.data.data){
                            $.each(result.data.data, function(k,sche){
                                var sche_id = sche.cs_schedule.id;
                                var url_sch = "{{ url('aprove_credit_sales') }}/"+sche_id+"/generate_credit_sale";
                              
                                var il = result.data.from  + k; 
                                text +="<tr>";
                                    text +="<td>"+il+"</td>";
                                    text +="<td><a href='"+url_sch+"'>"+sche.cs_schedule.schedule_number+"</a></td>";
                                    text +="<td>"+sche.cs_schedule.cs_client.kh_username+"</td>";
                                    text +="<td>"+day_format_show(sche.date_payment)+"</td>";
                                    text +="<td>"+accounting.formatMoney(sche.total_pay_cost)+"</td>";
                                    text +="<td>"+accounting.formatMoney(sche.total_pay_interest)+"</td>";
                                    text +="<td>"+accounting.formatMoney(sche.total_payment)+"</td>";
                                    text +="<td>0</td>";
                                    text +="<td>"+sche.cs_schedule.cs_staff.name_kh+"</td>";
                                    text +="<td>"+sche.cs_schedule.currency.value_option+"</td>";
                                text +="</tr>";
                            });
                        }
                        $('#list_item').html(text);

                        var page = "";
                                    if(result.data.prev_page_url === null){
                                        var pr_url = result.data.current_page;
                                    }else{
                                        var pr_url = result.data.current_page -1;
                                    }
                                    page += "<a class='pre'>&laquo;<input type='hidden' class='pre_in' value='"+pr_url+"' ></a>";
                                    for(var x = 1; x <= result.data.last_page; x ++  ) {
                                        if(result.data.current_page === x){
                                            page += "<a class='pag active' >"+x+"</a>";
                                        }else{
                                            page += "<a class='pag' >"+x+"</a>";
                                        }
                                    }
                                    if(result.data.next_page_url === null){
                                        var ne_url = result.data.current_page;
                                    }else{
                                        var ne_url = result.data.current_page +1;
                                    }
                                    page += "<a class='pre'>&raquo;<input type='hidden' class='pre_in' value='"+ne_url+"' ></a>";
                                    $(".pagination").html(page );

                        $('.total_count_collect_time').text(result.total_count_collect_time);
                        $('.total_money_owne_cost').text(accounting.formatMoney(result.total_money_owne_cost));            
                        $('.total_money_owne_interest').text(accounting.formatMoney(result.total_money_owne_interest));
                        $('.total_money_owne_total_pay').text(accounting.formatMoney(result.total_money_owne_total_pay));

                        $('.total_count_collect_time_kh').text(result.total_count_collect_time_kh);
                        $('.total_money_owne_cost_kh').text(accounting.formatMoney(result.total_money_owne_cost_kh));            
                        $('.total_money_owne_interest_kh').text(accounting.formatMoney(result.total_money_owne_interest_kh));
                        $('.total_money_owne_total_pay_kh').text(accounting.formatMoney(result.total_money_owne_total_pay_kh));
                    },
                    error: function (result ) {
                        console.log(result.stutus);
                    }

            });
        }
	});
</script>
@endsection
