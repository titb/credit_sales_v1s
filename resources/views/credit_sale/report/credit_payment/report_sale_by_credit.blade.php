@extends('credit_sale.layout.master')
@section('contend')
@include('credit_sale.autocompleted_script')
<style>
    p#payment_back {
        color: #08c;
        margin-top: 10px;
    }
    .list_item > tr > td {
        text-align: center;
        vertical-align: middle;
    }
    .header th {
        text-align: center;
    }

</style>
<!-- container-fluid  --> 	
<div class="container-fluid content_top_mar" >

<!-- validation -->
<div class="row-fluid">
    <!-- block -->
    <div class="block">
    @include('errors.error')
        <div class="navbar navbar-inner block-header">
            <div class="span12" style="margin-bottom: 10px;">
                <div class="muted pull-left" style="padding-top: 5px;"><span> {{$title}} </span></div>
                <a  class="btn btn-info export_excel pull-right" style="margin-top:-5px;">បញ្ចូលទៅកាន់ Excel</a>
                <a href="#" class="btn btn-danger print_excel pull-right" onclick="myFunction('report-incom')" style="margin-right: 4px; margin-top:-5px;" >ព្រីន</a>
            </div>    
        </div>
        <div class="block-content collapse in">
        <?php 
            $url = url('report/sales/report_sale_by_credit?reset=reset');
        ?>
            @include('credit_sale.report.search_form_sale')
                <div class="span12"  style="margin-left: 0px;" id="report-incom"> 
                <style type="text/css">
                            body {
                                -webkit-print-color-adjust: exact;
                            }
                        @media print
                        {    
                            .no-print, .no-print *{display: none !important;}
                            .color{color:red !important;}
                            tr th.row-background{background:#438eb9 !important;}
                        }
                        #co{
                            text-align: center;
                        }
                        .no_show{
                            display: none;
                        }
                    </style>
                    <style type="text/css" media="print">

                        @page { size: landscape; margin: 10px; margin-top: 10px;margin-bottom: 20px;}

                        .table-print{

                        width: 100%;

                        font-family: 'Battambang';

                        }

                        table {

                        width: 100%;

                        border-left: 0;

                        -webkit-border-radius: 4px;

                        -moz-border-radius: 4px;

                        border-radius: 4px;

                        }

                        table tr th, table tr td {

                            border: 1px solid #000;

                            padding: 5px;

                            line-height: 20px;

                            vertical-align: center;

                            font-size: 10px;

                        }

                        .no_print{
                                display: none;
                        }

                        .title_print{
                            font-family: 'Moul';
                        }

                        a{
                            text-decoration: none;
                            color: #000;
                        }
                        .font-weight{
                            font-weight: bold;
                        }
                        .print_show{
                            display: block;
                        }

                    </style>
                    <table class="table table-bordered table-print"  cellpadding="0" cellspacing="0" >
                        <thead  style="background: rgb(251, 205, 205);">
                        <tr class="header"> 
                            <th>#</th>
                            <th>លេខកូដ</th>
                            <th>ឈ្មោះអតិថិជន</th>
                            <th>កាលបរិច្ឆេទបង្កើត</th>
                            <th>រយៈពេលសង</th>
                            <th>បរិមាណទំនិញ</th>
                            <th>ចំនួនទឹកប្រាក់</th>
                            <th>ប្រាក់បង់តំបូង</th>
                            <th>ប្រាក់ដែលនៅជំពាក់</th>
                            <!-- <th>ស្ថានភាព</th> -->
                            <th>ថ្ងៃសង</th>
                            <th>ប្រាក់ដើម</th>
                            <th>ប្រាក់ការ</th>
                            <th>ប្រាក់បង់</th>
                            <th>នៅសល់</th>
                        </tr>
                        </thead>
                        <tbody id="list_item" class="list_item">
                            
                        </tbody>
                    </table>
                </div>
                <div class="pagination text-right"></div>
            </div>

        </div>
    <!-- /block -->
</div>
<!-- /validation -->

</div>

<script type="text/javascript">

	$(document).ready(function(){
        function day_format_show(date_format){
            var d = new Date(date_format);

            var year_n = d.getFullYear();
            var month_n = d.getMonth() + 1;
            var day_n = d.getDate();
            if(month_n > 10){
                month_n = month_n;
            }else{
                month_n = "0"+month_n; 
            }
            
            if(day_n > 10){
                day_n = day_n;
            }else{
                day_n = "0"+day_n; 
            }
            return day_n +"-"+month_n+"-"+year_n;
        }
        function get_kh_day(date){
            var d = new Date(date);
            var day = d.getDay();
            var arr = ["អាទិត្យ","ចន្ទ័","អង្គារ","ពុធ","ព្រហស្បតិ៍","សុក្រ","សៅរ៍"];
            var arr_key = arr.keys();
            for(i = 0; i <= arr.length; i++){
                if(i == day){
                    return arr[i] + " ";
                }
            }
        }

        var numpage = 1;
        var url_edit = "{{route('report/sales/report_sale_by_credit_json')}}";
        get_page(url_edit.numpage);
        // $(document).ajaxComplete(function(){
            $(".b_search").click(function(){
                var submit_search = $(this).val();
                var n = 1;
                
                var url_index2 = submit_search; 		
                get_page(url_index2,numpage = n);
            });
            $(document).on('click','.pag',function(){
                var numpage = $(this).text();   
                get_page(url_edit,numpage);
            });

            $(".pre").click(function(){
                var numpage = $(this).find(".pre_in").val();
                get_page(url_edit,numpage);
            });
        // });

        function get_page(url,n){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })
            
            var from_date = $(".from_date").val();
            var to_date = $(".to_date").val(); 
            var brand_name = $(".brand_name").val(); 
            var currency = $(".currency").val(); 
            var client_name = $(".client_name").val(); 
            var sale_id = $(".sale_id").val(); 
            var staff_name = $(".staff_name").val();  

            //alert(item_search);
            if(url === "b_search" || (from_date !== "" && to_date !== "") || brand_name !== "" || currency !== "" || client_name !== "" || sale_id !== "" || staff_name !== ""){
                var url_i = "{{route('report/sales/report_sale_by_credit_json')}}";
                var forData = {
                    from_date: from_date,
                    to_date: to_date,
                    brand_name: brand_name,
                    currency: currency,
                    client_name: client_name,
                    sale_id: sale_id,
                    staff_name: staff_name,
                    submit_search: $(".b_search").val()
                }                
                var url_index = url_i+"?from_date="+from_date+"&to_date="+to_date+"&brand_name="+brand_name+"&currency="+currency+"&client_name="+client_name+"&sale_id="+sale_id+"&staff_name="+staff_name+"&submit_search=b_search&page="+n;     
                var url_excel = "{{route('report/sales/report_sale_by_credit')}}?from_date="+from_date+"&to_date="+to_date+"&brand_name="+brand_name+"&currency="+currency+"&client_name="+client_name+"&sale_id="+sale_id+"&staff_name="+staff_name+"&submit_search="+url+"&export=excel";  

            }else{
                var forData = {};
                var url_index = url_edit+"?page="+n;
                var url_excel = "{{route('report/sales/report_sale_by_credit')}}?export=excel";  
            }
        
            var client;
            // var forData = {};
            var out = "";
            $.ajax({
                    type: "GET",
                    url: url_index, 
                    dataType: "json",
                    data: forData,
                    success: function(result ,xhr){
                        console.log(result);

                        var text = "";
                        
                        $(".export_excel").attr('href',url_excel);
                        if(result.data.data){
                            var total_ammount = 0;
                            $.each(result.data.data, function(k,sale){
                                var url_show_in = "{{url('credit_sales')}}/"+sale.id+"/invoice";
                                var il = result.data.from  + k; 
                                var status;
                                if(sale.cs_approval_credit_sale){
                                    if(sale.is_approve == 1){
                                        status = 'អនុម័ត';     
                                    }else if(sale.is_approve == 2) {
                                        status = 'មិនទាន់អនុម័ត';   
                                    }else if(sale.is_approve == 3) {
                                        status = 'មិនអនុម័ត';   
                                    }
                                }else {
                                    status = 'រង់ចាំការអនុម័ត';   
                                }
                                var dpm = sale.cs_request_form.duration_pay_money;
                                var dpmt = sale.cs_request_form.duration_pay_money_type;
                                if(dpmt == "month"){
                                    dpmt = "ខែ";
                                }else if(dpmt == "2week"){
                                    dpmt = "២ សប្តាហ៍";
                                }else if(dpmt == "week"){
                                    dpmt = "សប្តាហ៍";
                                }else if(dpmt == "day"){
                                    dpmt = "ថ្ងៃ";
                                }  
                                var dpm_d = dpm+"  "+dpmt;

                                text +="<tr>";
                                    text +="<td>"+il+"</td>";
                                    text +="<td>"+sale.num_invoice+"</td>";
                                    text +="<td>"+sale.cs_client.kh_username+"</td>";
                                    text +="<td>"+day_format_show(sale.cs_request_form.date_create_request)+"</td>";
                                    text +="<td>"+dpm_d+"</td>";
                                    text +="<td>"+sale.cs_request_form.total_qty+"</td>";
                                    text +="<td>"+accounting.formatMoney(sale.cs_request_form.prices_total_num)+"</td>";
                                    text +="<td>"+accounting.formatMoney(sale.cs_request_form.money_owne)+"</td>";
                                    text +="<td>"+accounting.formatMoney(sale.cs_request_form.deposit_fixed)+"</td>";
                                    text += "<td colspan='5'></td>"; // <a href='#' class='print_sale_by_cash'>Print</a>  | <a href='"+url_show_in+"' class='show_sale_by_cash'>Show</a> | <a href='#' class='deleted_sale_by_cash'>Delete</a> 
                                text += "</tr>";
                                if(sale.schedule){
                                    $.each(sale.schedule.cs_schedule_timesheet, function(kst,st){
                                        if(kst != 0){
                                            text += "<tr>";
                                                text += "<td colspan='9'></td>";
                                                text += "<td><b>"+ "ថ្ងៃ " + get_kh_day(st.date_payment)+ "</b>" + day_format_show(st.date_payment) +"</td>";
                                                text += "<td>"+ st.total_pay_cost +" $</td>";
                                                text += "<td>"+ st.total_pay_interest +" $</td>";
                                                text += "<td>"+ st.total_payment +" $</td>";
                                                text += "<td>"+ st.total_pay_cost_owe +" $</td>";
                                            text += "</tr>";
                                        }
                                    });
                                }
                                text += "<tr>";
                                text += "<td colspan='9'><b>សរុប "+ sale.cs_client.kh_username +"</b></td>";
                                text += "<td colspan='5'><b>"+ (sale.schedule?sale.schedule.money_owne_total_pay:'') +" $</b></td>";
                                text += "</tr>";

                                total_ammount += sale.schedule?sale.schedule.money_owne_total_pay:0;
                            });
                                text += "<tr style=' background: rgba(255, 219, 219, 0.64);'>";
                                text += "<td colspan='9'><strong>សរុប "+ result.data.data.length + " / " + result.data.total +" អតិថិជន</strong></td>";
                                text += "<td colspan='5'><strong>"+ total_ammount + " $ / "+ result.total_sum_amm +" $</strong></td>";
                                text += "</tr>";
                            $('#list_item').html(text);
                        var page = "";   
                                if(result.data.prev_page_url === null){
                                    var pr_url = result.data.current_page;
                                }else{
                                    var pr_url = result.data.current_page -1;
                                }
                                page += "<a class='pre'>&laquo;<input type='hidden' class='pre_in' value='"+pr_url+"' ></a>";
                                for(var x = 1; x <= result.data.last_page; x ++  ) {
                                    if(result.data.current_page === x){
                                        page += "<a class='pag active' >"+x+"</a>";
                                    }else{
                                        page += "<a class='pag' >"+x+"</a>";
                                    }
                                }
                                if(result.data.next_page_url === null){
                                    var ne_url = result.data.current_page; 
                                }else{
                                    var ne_url = result.data.current_page +1;
                                }
                                page += "<a class='pre'>&raquo;<input type='hidden' class='pre_in' value='"+ne_url+"' ></a>";
                                $(".pagination").html(page );
                        }else{
                            window.location = "{{url('report/clients/report_sale_by_credit')}}";
                        }
                        
                    },
                    error: function (result ) {
                        console.log(result.stutus);
                    }

            });
        }

	});
function myFunction(printpage)

    {

    var headstr = "<html><head><title></title></head><body>";

    var footstr = "</body>";

    var newstr = document.all.item(printpage).innerHTML;

    var oldstr = document.body.innerHTML;

    document.body.innerHTML = headstr+newstr+footstr;

    window.print();

    document.body.innerHTML = oldstr;

    return false;

    }


</script>

@endsection
