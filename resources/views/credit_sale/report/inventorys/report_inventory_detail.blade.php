@extends('credit_sale.layout.master')
@section('contend')
<div class="container-fluid">
            <div class="row-fluid">
            @include('credit_sale.report.menu_report_left')
                
                <!--/span-->
                <div class="span9" id="content">
                    <div class="row-fluid">
                        <!-- block -->
                        <div class="block"style="margin-top:0em;">
                        @include('errors.error')
                            <div class="navbar navbar-inner block-header">
                                <div class="span12" style="margin-bottom: 10px;">
                                    <div class="muted pull-left" style="padding-top: 5px;"><span> {{$title}} </span></div>
                                    <a  class="btn btn-info export_excel pull-right" style="margin-top:-5px;">Export to Excel</a>
                                    <a href="#" class="btn btn-danger print_excel pull-right" onclick="myFunction('report-incom')" style="margin-right: 4px; margin-top:-5px;" >Print</a>
                                </div>    
                            </div>
                            <div class="block-content collapse in">
                            <?php 
                                $url = url('report/payment_back/report_summery_payment_back?reset=reset');
                            ?>
                                @include('credit_sale.report.search_form')
                                    <div class="span12"  style="margin-left: 0px;" id="report-incom"> 
                                    <style type="text/css">
                                                body {
                                                    -webkit-print-color-adjust: exact;
                                                }
                                            @media print
                                            {    
                                                .no-print, .no-print *{display: none !important;}
                                                .color{color:red !important;}
                                                tr th.row-background{background:#438eb9 !important;}
                                            }
                                            #co{
                                                text-align: center;
                                            }
                                            .no_show{
                                                display: none;
                                            }
                                        </style>
                                        <style type="text/css" media="print">

                                            @page { size: landscape; margin: 10px; margin-top: 10px;margin-bottom: 20px;}

                                            .table-print{

                                            width: 100%;

                                            font-family: 'Battambang';

                                            }

                                            table {

                                            width: 100%;

                                            border-left: 0;

                                            -webkit-border-radius: 4px;

                                            -moz-border-radius: 4px;

                                            border-radius: 4px;

                                            }

                                            table tr th, table tr td {

                                                border: 1px solid #000;

                                                padding: 5px;

                                                line-height: 20px;

                                                vertical-align: center;

                                                font-size: 10px;

                                            }

                                            .no_print{
                                                    display: none;
                                            }

                                            .title_print{
                                                font-family: 'Moul';
                                            }

                                            a{
                                                text-decoration: none;
                                                color: #000;
                                            }
                                            .font-weight{
                                                font-weight: bold;
                                            }
                                            .print_show{
                                                display: block;
                                            }

                                        </style>
                                        <table class="table table-bordered table-print"  cellpadding="0" cellspacing="0" >
                                            <thead  style="background: rgb(251, 205, 205);">
                                            <tr class="header"> 
                                                <th>ល.រទំនិញ</th>
                                                <th>កាលបរិច្ឆេទ</th>
                                                <th>ឈ្មោះ​ទំនិញ</th>
                                                <th>អតិថិជន</th>
                                                <th>បុគ្គលិក</th>
                                                <th>ជំពូក</th>
                                                <th>ល.ស ផលិតផល</th>
                                                <th>ទំហំ</th>
                                                <th>បរិមាណ​ចេញ​ចូល</th>
                                                <th>សេចក្ដី​អធិប្បាយ</th>
                                            </tr>
                                            </thead>
                                            <tbody id="list_item" class="list_item">
                                                
                                            </tbody>
                                        </table>
                                    </div>    
                                </div>
                        </div>
                        <!-- /block -->
                    </div>
                </div>
            </div>

        </div>

@endsection
