<div class="span12">
        
    <div class="span2">
        <lable>&nbsp;</lable>
        <input type="text" name="from_date" placeholder="ពីថ្ងៃទី" data-required="1" class="input-xlarge datepicker span12 m-wrap from_date"id="typeahead" data-provide="typeahead" autocomplete="off">
    </div>
    <div class="span2">
        <lable>&nbsp;</lable>
        <input type="text" name="to_date" placeholder="ដល់ថ្ងៃទី" data-required="1" class="input-xlarge datepicker span12 m-wrap to_date"id="typeahead" data-provide="typeahead" autocomplete="off">
    </div>
    <div class="span2">
        <lable>&nbsp;</lable>
        <?php $brand_id = App\Branch::where('deleted','=',0)->get(); ?>
        <!-- <input type="text" name="brand_name" placeholder="Brand Name" data-required="1" class="span12 m-wrap brand_name"id="typeahead" data-provide="typeahead" autocomplete="off"> -->
        <select id="select01" class="span12 m-wrap brand_name" name="brand_name" >
            <option value="">ជ្រើសរើសសាខា</option>
            @foreach($brand_id as $br)
            <option value="{{$br->id}}">{{$br->brand_name}}</option>
            @endforeach
        </select>
    </div>
    <div class="span2">
        <lable>&nbsp;</lable>
        <?php $currency = App\Currency::where('deleted','=',0)->get(); ?>
        <!-- <input type="text" name="brand_name" placeholder="Brand Name" data-required="1" class="span12 m-wrap brand_name"id="typeahead" data-provide="typeahead" autocomplete="off"> -->
        <select id="select01" class="span12 m-wrap currency" name="currency" >
            <option value="">ជ្រើសរើសរូបិយប័ណ្ណ</option>
            @foreach($currency as $br)
            <option value="{{$br->id}}">{{$br->name}}</option>
            @endforeach
        </select>
    </div>
    <div class="span2">
        <lable>&nbsp;</lable>
        <?php $supplier_name = App\Cs_suppliers::where('deleted','=',0)->get(); ?>
        <!-- <input type="text" name="brand_name" placeholder="Brand Name" data-required="1" class="span12 m-wrap brand_name"id="typeahead" data-provide="typeahead" autocomplete="off"> -->
        <select id="select01" class="span12 m-wrap supplier_name" name="supplier_name" >
            <option value="">ជ្រើសរើសអ្នកផ្គត់ផ្គង់</option>
            @foreach($supplier_name as $br)
            <option value="{{$br->id}}">{{$br->name}}</option>
            @endforeach
        </select>
    </div>
    <div class="span2">
        <lable>&nbsp;</lable>
        <?php $purches_id = App\Cs_Purchase_Order::where('deleted','=',1)->get(); ?>
        <!-- <input type="text" name="brand_name" placeholder="Brand Name" data-required="1" class="span12 m-wrap brand_name"id="typeahead" data-provide="typeahead" autocomplete="off"> -->
        <select id="select01" class="span12 m-wrap purches_id" name="purches_id" >
            <option value="">ជ្រើសរើសលេខកូដ</option>
            @foreach($purches_id as $br)
            <option value="{{$br->invoice_num}}">{{$br->invoice_num}}</option>
            @endforeach
        </select>
    </div>
    <!-- <div class="row" style="margin-left: 0px;"> -->
        <div class="span10"  style="margin-left: 0px;">
            <div class="span2"  style="margin-left: 0px; width: 200px;">
                <lable>&nbsp;</lable>
                <?php $staff_name = App\User::where('deteted','=',0)->get(); ?>
                <!-- <input type="text" name="brand_name" placeholder="Brand Name" data-required="1" class="span12 m-wrap brand_name"id="typeahead" data-provide="typeahead" autocomplete="off"> -->
                <select id="select01" class="span12 m-wrap staff_name" name="staff_name" >
                    <option value="">ជ្រើសរើសបុគ្គលិក</option>
                    @foreach($staff_name as $br)
                    <option value="{{$br->id}}">{{$br->name_kh}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="span2 pull-right" style="margin-right: -123px;">
            <lable>&nbsp;</lable><br/>
            <button type="submit" class="btn btn-primary b_search" id="b_search" name="submit_search" value="b_search">ស្វែងរក</button>
        </div>
    <!-- </div> -->
</div>
    