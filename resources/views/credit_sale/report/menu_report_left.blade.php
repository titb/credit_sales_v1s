<div class="span3" id="sidebar">
    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
        <!-- <li class="active">
            <a href="#"><i class="icon-chevron-right"></i> Dashboard</a>
        </li> -->
        
        <li class="{{ Request::segment(2) == 'item' ? 'active' : null }}">
            <a href="{{ url('report/item/report_item_list') }}"> <i class="icon-chevron-right"></i> របាយការណ៍លម្អិតពីទំនិញ </a>
        </li>
        <li class="{{ Request::segment(2) == 'clients' ? 'active' : null }}">
            <a href="{{ url('report/clients/report_client_list') }}"><i class="icon-chevron-right"></i>  របាយការណ៍លម្អិតពីអតិថិជន </a>
        </li>
        <li class="{{ Request::segment(3) == 'report_sale_by_credit' ? 'active' : null }}">
            <a href="{{ url('report/sales/report_sale_by_credit') }}"><i class="icon-chevron-right"></i> របាយការណ៍លម្អិតពីការលក់ដោយឥណទាន </a>
        </li>
        <li class="{{ Request::segment(3) == 'report_direct_payment' ? 'active' : null }}">
            <a href="{{ url('report/sales/report_direct_payment') }}"><i class="icon-chevron-right"></i> របាយការណ៍លម្អិតពីការលក់ដោយរការទូទាត់ដោយផ្ទាល់  </a>
        </li>
        <li class="{{ Request::segment(3) == 'report_payment_back' ? 'active' : null }}">
            <a href="{{ url('report/payment_back/report_payment_back') }}"><i class="icon-chevron-right"></i> របាយការណ៍លម្អិតពីសង់ប្រាក់ត្រលប់  </a>
        </li>
        <li class="{{ Request::segment(3) == 'report/payment_back/report_summery_payment_back' ? 'active' : null }}">
            <a href="{{ url('report/payment_back/report_summery_payment_back') }}"><i class="icon-chevron-right"></i> របាយការណ៍សរុបពីសង់ប្រាក់ត្រលប់  </a>
        </li>
        <li class="{{ Request::segment(2) == 'report_schedules' ? 'active' : null }}">
            <a href="{{ url('report/report_schedules') }}"><i class="icon-chevron-right"></i> របាយការណ៍លម្អិតពីកាលវិភាគសងប្រាក់ </a>
        </li>
        <li class="{{ Request::segment(2) == 'report_repayment_collect' ? 'active' : null }}">
            <a href="{{ url('report/report_repayment_collect') }}"><i class="icon-chevron-right"></i> របាយការណ៍លម្អិតពីការប្រមូលសាច់ប្រាក់ </a>
        </li>
        <li class="{{ Request::segment(2) == 'report_sale' ? 'active' : null }}">
            <a href="{{ url('report/report_sale') }}"><i class="icon-chevron-right"></i> របាយការណ៍លម្អិតពីការទិញជាសាច់ប្រាក់</a>
        </li> 
        <li class="{{ Request::segment(2) == 'report_sale_by_credit' ? 'active' : null }}">
            <a href="{{ url('report/report_sale_by_credit') }}"><i class="icon-chevron-right"></i> របាយការណ៍លម្អិតពីការទិញបង់រំលស់</a>
        </li>
        <li class="{{ Request::segment(2) == 'report_receiving_product' ? 'active' : null }}">
            <a href="{{ url('report/report_receiving_product') }}"><i class="icon-chevron-right"></i> របាយការណ៍លម្អិតពីការទទួលទំនិញ </a>
        </li> 
        <li class="{{ Request::segment(2) == 'report_po_product' ? 'active' : null }}">
            <a href="{{ url('report/report_po_product') }}"><i class="icon-chevron-right"></i> របាយការណ៍លម្អិតពីការបញ្ជារទិញ </a>
        </li>

        <li class="{{ Request::segment(3) == 'inventory_details' ? 'active' : null }}">
            <a href="{{ url('report/inventorys/inventory_details') }}"><i class="icon-chevron-right"></i> របាយការណ៍លម្អិតពីសារពើភ័ណ្ឌ </a>
        </li>

        
    </ul>
</div>