@extends('credit_sale.layout.master')
@section('contend')
@include('credit_sale.autocompleted_script')
<style>
    p#payment_back {
        color: #08c;
        margin-top: 10px;
    }

</style>
<meta name="csrf-token" content="{{ csrf_token() }}">
<script src="{{ URL::to('assets/accounting.min.js')}}"></script>
<!-- container-fluid  --> 	
<div class="container-fluid content_top_mar" >
    
<!-- validation -->
<div class="row-fluid">
    
    <!-- block -->
    <div class="block">

        @include('errors.error')
        <div class="navbar navbar-inner block-header">
            <div class="span12" style="margin-bottom: 3px;">
                <div class="muted pull-left" style="padding-top: 5px;">Report Sale By Cash</div>
                <a href="{{url('export_report')}}" class="btn btn-primary b_search pull-right"  value="b_search">Export to Report</a>
                <a href="{{url('export_report')}}" class="btn btn-danger pull-right"  value="b_search" style="margin-right: 4px;">Print</a>
            </div>    
        </div>
        <div class="block-content collapse in">
        <?php 
            $url = "{{url('report/report_sale')}}";
        ?>
            @include('credit_sale.report.search_form_sale')
                <div class="span12" style="margin-left: 0px;">
                    <table class="table table-bordered">
                        <thead>
                        <tr> 
                            <th>#</th>
                            <th>លេខកូដ</th>
                            <th>ឈ្មោះអតិថិជន</th>
                            <th>ចំនួនទឹកប្រាក់</th>
                            <th>បញ្ចុះតម្លៃ</th>
                            <!-- <th>ប្រាក់ដែលនៅជំពាក់</th> -->
                            <th>ចំនួនផលិតផល</th>
                            <th>ប្រភេទបង់ប្រាក់</th>
                            <th>បុគ្គលិកប្រមូលប្រាក់</th>
                            <th>រូបិយប័ណ្ណ</th>
                            <th>ផ្ដល់មតិ</th>  
                        </tr>
                        </thead>
                        <tbody id="list_item" class="list_item">
                            
                        </tbody>
                    </table>
                    <div class="pagination text-right"></div>
                </div>
            </div>

        </div>
    <!-- /block -->
</div>
<!-- /validation -->

</div>

<script type="text/javascript">

	$(document).ready(function(){
        function day_format_show(date_format){
            var d = new Date(date_format);

            var year_n = d.getFullYear();
            var month_n = d.getMonth() + 1;
            var day_n = d.getDate();
            if(month_n > 10){
                month_n = month_n;
            }else{
                month_n = "0"+month_n; 
            }
            
            if(day_n > 10){
                day_n = day_n;
            }else{
                day_n = "0"+day_n; 
            }

            return  day_n +"-"+month_n+"-"+year_n;
        }

        var numpage = 1;
        var url_edit = "{{route('report/report_sale_json')}}";
        get_page(url_edit.numpage);
        // $(document).ajaxComplete(function(){
            $(".b_search").click(function(){
                var submit_search = $(this).val();
                var n = 1;
                
                var url_index2 = submit_search; 		
                get_page(url_index2,numpage = n);
            });
            $(document).on('click','.pag',function(){
                var numpage = $(this).text();   
                get_page(url_edit,numpage);
            });

            $(".pre").click(function(){
                var numpage = $(this).find(".pre_in").val();
                get_page(url_edit,numpage);
            });
        // });

        function get_page(url,n){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })
            
            var from_date = $(".from_date").val();
            var to_date = $(".to_date").val(); 
            var brand_name = $(".brand_name").val(); 
            var currency = $(".currency").val(); 
            var client_name = $(".client_name").val(); 
            var sale_id = $(".sale_id").val(); 
            var staff_name = $(".staff_name").val();  

            //alert(item_search);
            if(url === "b_search" || (from_date !== "" && to_date !== "") || brand_name !== "" || currency !== "" || client_name !== "" || sale_id !== "" || staff_name !== ""){
                var url_i = "{{route('report/report_sale_json')}}";
                var forData = {
                    from_date: from_date,
                    to_date: to_date,
                    brand_name: brand_name,
                    currency: currency,
                    client_name: client_name,
                    schedule_id: schedule_id,
                    staff_name: staff_name,
                    submit_search: $(".b_search").val()
                }                
                var url_index = url_i+"?from_date="+from_date+"&to_date="+to_date+"&brand_name="+brand_name+"&currency="+currency+"&client_name="+client_name+"&sale_id="+sale_id+"&staff_name="+staff_name+"&submit_search=b_search&page="+n;     
            }else{
                var forData = {};
                var url_index = url_edit+"?page="+n;
            }
        
            var client;
            // var forData = {};
            var out = "";
            $.ajax({
                    type: "GET",
                    url: url_index, 
                    dataType: "json",
                    data: forData,  
                    success: function(result ,xhr){
                        console.log(result);

                        var text = "";
                        if(result.data.data){
                            $.each(result.data.data, function(k, sale){
                                var il = result.data.from  + k; 
                                var url_show_in = "{{url('credit_sales')}}/"+sale.id+"/invoice";
                                text +="<tr>";
                                    text +="<td>"+il+"</td>";
                                    text +="<td> <a href='#' class='print_sale_by_cash'>Print</a>  | <a href='"+url_show_in+"' class='show_sale_by_cash'>Show</a> |<a href='#' class='deleted_sale_by_cash'>Delete</a> | "+sale.num_invoice+"</td>";
                                    text +="<td>"+sale.cs_client.kh_username+"</td>";
                                    text +="<td>"+accounting.formatMoney(sale.sale_payment.dute_payment_amount)+"</td>";
                                    text +="<td>"+sale.sale_payment.discount_payment+"</td>";
                                    text +="<td>"+sale.sale_payment.total_qty+"</td>";
                                    text +="<td>"+sale.method+"</td>";
                                    text +="<td>"+sale.user.name_kh+"</td>";
                                    text +="<td>"+sale.currency.value_option+"</td>";
                                    text +="<td>"+sale.sale_payment.comment+"</td>";
                                text +="</tr>";
                            });
                        }
                        $('#list_item').html(text);
                        var page = "";
                        if(result.data.prev_page_url === null){
                            var pr_url = result.data.current_page;
                        }else{
                            var pr_url = result.data.current_page -1;
                        }
                        page += "<a class='pre'>&laquo;<input type='hidden' class='pre_in' value='"+pr_url+"' ></a>";
                        for(var x = 1; x <= result.data.last_page; x ++  ) {
                            if(result.data.current_page === x){
                                page += "<a class='pag active' >"+x+"</a>";
                            }else{
                                page += "<a class='pag' >"+x+"</a>";
                            }
                        }
                        if(result.data.next_page_url === null){
                            var ne_url = result.data.current_page;
                        }else{
                            var ne_url = result.data.current_page +1;
                        }
                        page += "<a class='pre'>&raquo;<input type='hidden' class='pre_in' value='"+ne_url+"' ></a>";
                        $(".pagination").html(page );
                       
                        
                    },
                    error: function (result ) {
                        console.log(result.stutus);
                    }

            });
        }    

    });
        

    
</script>
@endsection
