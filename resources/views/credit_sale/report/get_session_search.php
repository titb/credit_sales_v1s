<?php  
    if(Session::has('from_date')){
        $from_date  = Session::get('from_date');
    }else{
        $from_date  = '';
    }

    if(Session::has('to_date')){
        $to_date  = Session::get('to_date');
    }else{
        $to_date  = '';
    }

    if(Session::has('brand_name')){
        $brand_name  = Session::get('brand_name');
    }else{
        $brand_name  = '';
    }

    if(Session::has('currency')){
        $currency  = Session::get('currency');
    }else{
        $currency  = '';
    }

    if(Session::has('client_name')){
        $client_name  = Session::get('client_name');
    }else{
        $client_name  = '';
    }

    if(Session::has('sale_id')){
        $sale_id  = Session::get('sale_id');
    }else{
        $sale_id  = '';
    }

    if(Session::has('staff_name')){
        $staff_name  = Session::get('staff_name');
    }else{
        $staff_name  = '';
    }

?>