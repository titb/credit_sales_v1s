@extends('credit_sale.layout.master')
@section('contend')
@include('credit_sale.autocompleted_script')
<style>
    p#payment_back {
        color: #08c;
        margin-top: 10px;
    }
    .list_item > tr > td {
        text-align: center;
        vertical-align: middle;
    }
    .header th {
        text-align: center;
    }

</style>
<!-- container-fluid  --> 	
<div class="container-fluid content_top_mar" >

<!-- validation -->
<div class="row-fluid">
    <!-- block -->
    <div class="block">
    @include('errors.error')
        <div class="navbar navbar-inner block-header">
            <div class="span12" style="margin-bottom: 10px;">
                <div class="muted pull-left" style="padding-top: 5px;"><span> {{$title}} </span></div>
                <a  class="btn btn-info export_excel pull-right" style="margin-top:-5px;">បញ្ចូលទៅកាន់ Excel</a>
                <a href="#" class="btn btn-danger print_excel pull-right" onclick="myFunction('report-incom')" style="margin-right: 4px; margin-top:-5px;" >ព្រីន</a>
            </div>    
        </div>
        <div class="block-content collapse in">
        <?php 
            $url = url('report/sales/report_direct_payment?reset=reset');
        ?>
            @include('credit_sale.report.search_form_sale')
                <div class="span12"  style="margin-left: 0px;" id="report-incom"> 
                <style type="text/css">
                            body {
                                -webkit-print-color-adjust: exact;
                            }
                        @media print
                        {    
                            .no-print, .no-print *{display: none !important;}
                            .color{color:red !important;}
                            tr th.row-background{background:#438eb9 !important;}
                        }
                        #co{
                            text-align: center;
                        }
                        .no_show{
                            display: none;
                        }
                    </style>
                    <style type="text/css" media="print">

                        @page { size: landscape; margin: 10px; margin-top: 10px;margin-bottom: 20px;}

                        .table-print{

                        width: 100%;

                        font-family: 'Battambang';

                        }

                        table {

                        width: 100%;

                        border-left: 0;

                        -webkit-border-radius: 4px;

                        -moz-border-radius: 4px;

                        border-radius: 4px;

                        }

                        table tr th, table tr td {

                            border: 1px solid #000;

                            padding: 5px;

                            line-height: 20px;

                            vertical-align: center;

                            font-size: 10px;

                        }

                        .no_print{
                                display: none;
                        }

                        .title_print{
                            font-family: 'Moul';
                        }

                        a{
                            text-decoration: none;
                            color: #000;
                        }
                        .font-weight{
                            font-weight: bold;
                        }
                        .print_show{
                            display: block;
                        }

                    </style>
                    <table class="table table-bordered table-print"  cellpadding="0" cellspacing="0" >
                        <thead  style="background: rgb(251, 205, 205);">
                        <tr class="header"> 
                            <th>#</th>
                            <th>លេខកូដ</th>
                            <th>ឈ្មោះអតិថិជន</th>
                            <th>កាលបរិច្ឆេទបង្កើត</th>
                            <th>បរិមាណទំនិញ</th>
                            <th>ការទូទាត់សរុបទាំងអស់</th>
                            <th>បញ្ចុះតម្លៃ</th>
                            <th>ការទូទាត់សរុប</th>
                            <th>តម្លៃនៃទំនិញដែលបានលក់</th>
                            <!-- <th>ប្រាក់ចំណេញ</th>    -->
                            <th>ការបង់ប្រាក់</th>
                            <th>ប្រភេទបង់</th>
                            <th>មូលហេតុបញ្ចុះតម្លៃ</th>
                            <th width="15%">ឈ្មោះទំនិញ</th>
                            <th>ចំនួន</th>
                            <th>តម្លៃ</th>
                            <th>សរុប</th>
                        </tr>
                        </thead>
                        <tbody id="list_item" class="list_item">
                            
                        </tbody>
                    </table>
                </div>
                <div class="pagination text-right"></div>
            </div>

        </div>
    <!-- /block -->
</div>
<!-- /validation -->

</div>

<script type="text/javascript">
	$(document).ready(function(){

        var numpage = 1;
        var url_edit = "{{route('report/sales/report_direct_payment_json')}}";
        get_page(url_edit.numpage);
        // $(document).ajaxComplete(function(){
            $(".b_search").click(function(){
                var submit_search = $(this).val();
                var n = 1;
                
                var url_index2 = submit_search; 		
                get_page(url_index2,numpage = n);
            });
            $(document).on('click','.pag',function(){
                var numpage = $(this).text();   
                get_page(url_edit,numpage);
            });

            $(".pre").click(function(){
                var numpage = $(this).find(".pre_in").val();
                get_page(url_edit,numpage);
            });
        // });

        function get_page(url,n){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })
            
            var from_date = $(".from_date").val();
            var to_date = $(".to_date").val(); 
            var brand_name = $(".brand_name").val(); 
            var currency = $(".currency").val(); 
            var client_name = $(".client_name").val(); 
            var sale_id = $(".sale_id").val(); 
            var staff_name = $(".staff_name").val();  

            //alert(item_search);
            if(url === "b_search" || (from_date !== "" && to_date !== "") || brand_name !== "" || currency !== "" || client_name !== "" || sale_id !== "" || staff_name !== ""){
                var url_i = "{{route('report/sales/report_direct_payment_json')}}";
                var forData = {
                    from_date: from_date,
                    to_date: to_date,
                    brand_name: brand_name,
                    currency: currency,
                    client_name: client_name,
                    sale_id: sale_id,
                    staff_name: staff_name,
                    submit_search: $(".b_search").val()
                }                
                var url_index = url_i+"?from_date="+from_date+"&to_date="+to_date+"&brand_name="+brand_name+"&currency="+currency+"&client_name="+client_name+"&sale_id="+sale_id+"&staff_name="+staff_name+"&submit_search=b_search&page="+n;     
                var url_excel = "{{route('report/sales/report_direct_payment')}}?from_date="+from_date+"&to_date="+to_date+"&brand_name="+brand_name+"&currency="+currency+"&client_name="+client_name+"&sale_id="+sale_id+"&staff_name="+staff_name+"&submit_search="+url+"&export=excel";  

            }else{
                var forData = {};
                var url_index = url_edit+"?page="+n;
                var url_excel = "{{route('report/sales/report_direct_payment')}}?export=excel";  
            }
        
            var client;
            // var forData = {};
            var out = "";
            $.ajax({
                    type: "GET",
                    url: url_index, 
                    dataType: "json",
                    data: forData,
                    success: function(result ,xhr){
                        console.log(result);

                        var text = "";
                        $(".export_excel").attr('href',url_excel);
                        if(result.data.data){
                            $.each(result.data.data, function(k,sale){
                                var url_show_in = "{{url('credit_sales')}}/"+sale.id+"/invoice";
                                var il = result.data.from  + k;  
                                var st_p = ''; 
                                if(sale.sale_transaction_pay){
                                    $.each(sale.sale_transaction_pay, function(i,b){
                                        st_p += Math.abs(b.account_mount)+"  "+b.currency.name_kh+"<br/>";
                                    });
                                }
                                var cl = "";
                                if(sale.cs_client){
                                    cl = sale.cs_client.kh_username;
                                }else{
                                    cl = "No Customer";
                                }
                                if(sale.sale_payment.remaining == 'pay_on_direct_sale'){
                                    var stat_pay = "ទូទាត់";
                                }else{
                                    var stat_pay = "ជំពាក់";
                                }
                                var profit = 0;
                                text +="<tr>";
                                    text +="<td>"+il+"</td>";
                                    text +="<td>"+sale.num_invoice+"</td>";
                                    text +="<td>"+cl+"</td>";
                                    text +="<td>"+day_format_show(sale.sale_payment.payment_date)+"</td>";
                                    text +="<td>"+Math.abs(sale.sale_payment.total_qty)+"</td>";
                                    text +="<td>"+accounting.formatMoney(Math.abs(sale.sale_payment.sub_payment_amount))+"</td>";
                                    text +="<td>"+accounting.formatMoney(Math.abs(sale.sale_payment.discount_payment))+"</td>";
                                    text +="<td>"+accounting.formatMoney(Math.abs(sale.sale_payment.total_payment))+"</td>";
                                    text +="<td>"+accounting.formatMoney(Math.abs(sale.sale_payment.dute_payment_amount))+"</td>";
                                    // text +="<td>"+profit+"</td>";
                                    text +="<td>"+st_p+"</td>";
                                    text +="<td><b>"+stat_pay+"</b></td>";
                                    text +="<td>"+(sale.sale_payment.discount_reason!==null?sale.sale_payment.discount_reason:'')+"</td>";
                                    text +="<td colspan='4'></td>";
                                text +="</tr>";
                                if(sale.sale_item){
                                    $.each(sale.sale_item, function(kst,st){
                                        text += "<tr>";
                                            text += "<td colspan='12'></td>";
                                            text += "<td style='background: rgba(255, 219, 219, 0.64);'>"+ (st.item?st.item.name:'') +"</td>";
                                            text += "<td style='background: rgba(255, 219, 219, 0.64);'>"+ Math.abs(st.qty) +"</td>";
                                            text += "<td style='background: rgba(255, 219, 219, 0.64);'>"+ st.sell_price +" $</td>";
                                            text += "<td style='background: rgba(255, 219, 219, 0.64);'>"+ Math.abs(st.total_price) +" $</td>";
                                        text += "</tr>";
                                    });
                                }
                            });
                            text += "<tr style=' background: rgba(255, 219, 219, 0.64);'>";
                            text += "<td colspan='4'><strong>សរុប "+ result.data.data.length + " / " + result.data.total +" អតិថិជន</strong></td>";
                            text += "<td><strong>"+result.all_qty+"</strong></td>";
                            text += "<td><strong>"+result.sub_total+" ដុល្លា</strong></td>";
                            text += "<td></td>";
                            text += "<td><strong>"+result.total+" ដុល្លា</strong></td>";
                            text += "<td><strong>"+result.paid_amm+" ដុល្លា</strong></td>";
                            text += "<td><strong>"+result.payment_us+" ដុល្លា / "+result.payment_kh+" រៀល</strong></td>";
                            text += "<td colspan='6'></td>";
                            text += "</tr>";
                            $('#list_item').html(text);
                        var page = "";
                                if(result.data.prev_page_url === null){
                                    var pr_url = result.data.current_page;
                                }else{
                                    var pr_url = result.data.current_page -1;
                                }
                                page += "<a class='pre'>&laquo;<input type='hidden' class='pre_in' value='"+pr_url+"' ></a>";
                                for(var x = 1; x <= result.data.last_page; x ++  ) {
                                    if(result.data.current_page === x){
                                        page += "<a class='pag active' >"+x+"</a>";
                                    }else{
                                        page += "<a class='pag' >"+x+"</a>";
                                    }
                                }
                                if(result.data.next_page_url === null){
                                    var ne_url = result.data.current_page; 
                                }else{
                                    var ne_url = result.data.current_page +1;
                                }
                                page += "<a class='pre'>&raquo;<input type='hidden' class='pre_in' value='"+ne_url+"' ></a>";
                                $(".pagination").html(page );
                        }else{
                            window.location = "{{url('report/clients/report_direct_payment')}}";
                        } 
                       
                    },
                    error: function (result ) {
                        console.log(result.stutus);
                    }

            });
        }

	});
function myFunction(printpage)

    {

    var headstr = "<html><head><title></title></head><body>";

    var footstr = "</body>";

    var newstr = document.all.item(printpage).innerHTML;

    var oldstr = document.body.innerHTML;

    document.body.innerHTML = headstr+newstr+footstr;

    window.print();

    document.body.innerHTML = oldstr;

    return false;

    }


</script>

@endsection
