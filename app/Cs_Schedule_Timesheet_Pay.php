<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cs_Schedule_Timesheet_Pay extends Model
{

    protected $table = "cs-schedules-repayment-timesheet";

    public function cs_sch_ts(){
        return $this->belongsTo('App\Cs_Schedule_Timesheet','schedules_titmesheet_id');
    }

    public function cs_schedule(){
        return $this->belongsTo('App\Cs_Schedule','cs_schedules_id');
    }
    public function cs_client(){
        return $this->belongsTo('App\Cs_Client','client_id');
    }
    public function user(){
        return $this->belongsTo('App\User','user_id');
    }
    public function branch(){
        return $this->belongsTo('App\Branch','branch_id');
    }
    public function cs_staff(){
        return $this->belongsTo('App\User','staff_id');
    }
    public function currency(){
        return $this->belongsTo('App\Currency','currency_id');
    }
}
