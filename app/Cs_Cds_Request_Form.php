<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cs_Cds_Request_Form extends Model
{
    //
    protected $table = "cs_request_form";

    public function staff_id(){
        return $this->belongsTo('App\User','staff_id');
    }
    public function cs_clients(){
        return $this->belongsTo('App\Cs_Client', 'client_id');
    }
    public function cs_sales(){
        return $this->belongsTo('App\Cs_Sale','sale_id');
    }
}
