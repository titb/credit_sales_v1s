<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mfi_Position extends Model
{
  protected $table = "mfi_positions";

  public function user(){

      return $this->hasMany('App\User','position_id');

  }
}
