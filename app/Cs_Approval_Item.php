<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cs_Approval_Item extends Model
{
    //
    protected $table = "cs-item-approval";
    public function item(){
        return $this->belongsTo('App\Cs_Items','product_id');
    }
}
