<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cs_Schedule_Timesheet extends Model
{
    //
    protected $table = "cs-schedules-timesheet";

    public function cs_sch_ts_pay(){
        return $this->hasMany('App\Cs_Schedule_Timesheet_Pay','schedules_titmesheet_id')->orderBy('updated_at', 'DESC');
    }

    public function cs_schedule(){
        return $this->belongsTo('App\Cs_Schedule','cs_schedules_id');
    }
}
