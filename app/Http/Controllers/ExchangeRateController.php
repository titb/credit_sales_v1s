<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Mfi_Exchange_Rate;
use DB;
use Auth;

class ExchangeRateController extends Controller
{

	public function show_list(){
		$title = "Exchange Rate";
		$data = Mfi_Exchange_Rate::all();
    	return view('credit_sale.exchange_rate.show_exchange_rate')->with('title',$title)->with('data',$data);
	}

    public function get_edit($id){
    	$title = "Edit Exchange Rate";
    	$data = Mfi_Exchange_Rate::find($id);
    	return view('credit_sale.exchange_rate.edit_exchange_rate')->with('title',$title)->with('data',$data);
    }

    public function post_edit(Request $request, $id){
    	$title = "Edit Exchange Rate";
    	$data = array(
    					'real' => str_replace(',','',$request->real), 
    					'us' => $request->us
    			);
    	DB::table('cs_exchange_rate')->where('id','=',$id)->update($data);
      $data = [
                      'ip_log'=> $request->ip(),
                      'active'=> "កែប្រែកំណត់ការប្តូរប្រាក់",
                      'user_id'=> Auth::user()->id,
                      'status'=> '3',
                      'what_id' => $id,
                      'method' => 'exchange rate',
                      'create_date' => date('Y-m-d h:m:s')
                  ];
                DB::table('cs-history-logs')->insert($data); 
    	return redirect()->to('exchange_rate')->with('success','Save Seccessfull');
    }

     public function post_deleted(Request $request , $id){

      $data = Mfi_Exchange_Rate::find($id);

      $data->delete();
      $data = [
                      'ip_log'=> $request->ip(),
                      'active'=> "លុបកំណត់ការប្តូរប្រាក់",
                      'user_id'=> Auth::user()->id,
                      'status'=> '3',
                      'what_id' => $id,
                      'method' => 'exchange rate',
                      'create_date' => date('Y-m-d h:m:s')
                  ];
                DB::table('cs-history-logs')->insert($data); 
      return redirect()->to('exchange_rate')->with('success','Delete Seccessfull'); 

  } 


}
