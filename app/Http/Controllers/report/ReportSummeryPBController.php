<?php

namespace App\Http\Controllers\report;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Cs_Schedule_Timesheet_Pay;
use App\lib\Session_Search;
use App\lib\SchedulePaymentBack;
use App\lib\Schedule;
use App\Cs_Sale;
use DB;
use Auth;
use Session;
use DateTime;
use Excel;
class ReportSummeryPBController extends Controller
{
    protected $schedule;
    public function __construct()     
    {
        $this->schedule = new Schedule;
        $this->time_sheet_pay = new SchedulePaymentBack;
        $this->middleware('auth');
    }
        //
    public function ShowData(Request $request){
            $data = Schedule::getDatabyGet($request);
            print_r($data);
    }

    public function getDataJson(Request $request){
       
    }
     
    public function get_summery_total_json(Request $request){
        $data_query1 = $this->time_sheet_pay->collect_total_summery($request,1);
        $data_query2 = $this->time_sheet_pay->collect_total_summery($request,2);
        $data_query = [
            'data_query' => $data_query1,
            'data_query2' => $data_query2,
        ];
        return response()->json($data_query);
    }

    public function get_report_total_payment_back(Request $request){

            $title ="Report Summary Payment Back";
            // $data = $this->time_sheet_pay->collect_total_summery($request,1);
            
            // print_r($data);
            // die();    
            return view('credit_sale.report.payment_back.get_report_total_payment_back',compact('title'));

    }   
}
