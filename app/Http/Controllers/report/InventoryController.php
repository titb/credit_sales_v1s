<?php

namespace App\Http\Controllers\report;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Cs_Client;
use App\Cs_Image_Data;
use App\Cs_brand;
use App\Cs_categorys;
use App\Cs_suppliers;
use App\Cs_items;
use App\Cs_inventorys;
use App\Cs_Purchase_Order;
use App\Cs_Purchase_Order_Item;
use App\Cs_Purchase_Order_Payment;
use Auth;
use Session;
use DB;


class InventoryController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
        $this->inv = new Cs_inventorys;
    }

    public function report_inventory_details(Request $request){
        $title = "របាយការណ៍លម្អិតពីសារពើភ័ណ្ឌ";
        return view('credit_sale.report.inventorys.report_inventory_detail',compact('title'));
    }


    public function get_search_repayment_collect(Request $request, $money_type){
       
        $data = $this->inv->with(['item.categorys','cs_brand','user']);
            if($request->submit_search == "b_search"){
                // from date
                if($request->from_date != ""){
                    $from_date = date('Y-m-d',strtotime($request->from_date));
                    Session::flash('from_date',$request->from_date);

                    $data = $data->where('cs-schedules-timesheet.date_payment','>=',$from_date);
                }
                // To date
                if($request->to_date != "") {
                    $to_date = date('Y-m-d',strtotime($request->to_date));
                    Session::flash('to_date',$request->to_date);

                    $data = $data->where('cs-schedules-timesheet.date_payment','<=',$to_date);
                }
                // Search Brand
                if($request->brand_name != ""){
                    $brand_name = $request->brand_name;
                    Session::flash('brand_name',$request->brand_name);

                    $data = $data->where('css.branch_id','=',$brand_name);
                }
                // Search Currency
                if($request->currency != ""){
                    $currency = $request->currency;
                    Session::flash('currency',$request->currency);

                    $data = $data->where('css.currency_id','=',$currency);
                }
                // Search Client Name
                if($request->client_name != ""){
                    $client_name = $request->client_name;
                    Session::flash('client_name',$request->client_name);
                    $data = $data->where('css.client_id','=',$client_name);
                }

                // Search Schedule ID
                if($request->schedule_id != ""){
                    $schedule_id = $request->schedule_id;
                    Session::flash('schedule_id',$request->schedule_id);
                    $data = $data->where('css.id','=',$schedule_id);
                }

                // Search Staff Name
                if($request->staff_name != ""){
                    $staff_name = $request->staff_name;
                    Session::flash('staff_name',$request->staff_name);
                    $data = $data->where('css.staff_id','=',$staff_name);
                }
            }else{
                Session::forget('from_date');
                Session::forget('to_date');
                Session::forget('brand_name');
                Session::forget('currency');
                Session::forget('client_name');
                Session::forget('schedule_id');
                Session::forget('staff_name');
            }

            if($money_type != null){
                $data = $data->where('css.currency_id','=',$money_type);
            }   
            
            return $data; 
    
    }

}
