<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cs_Schedule;
use App\lib\Session_Search;
use Auth;
use DB;
use Session;
use Excel;

class ReportScheduleController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
        $this->session_search = new Session_Search; 
    }
    public function get_report_schedule(Request $request){
        $title = "Report Schedule Back";
        return view('credit_sale.report.report_schedule',compact('title'));
    }

    public function get_search_schedul(Request $request, $money_type){
       
            $data = Cs_Schedule::with(['branch','request_form','cs_client','user','cs_staff','cs_approvalcredit','cs_schedule_timesheet','cs_schedule_timesheet_pay','currency'])
            ->join('cs-manager-branch-approval as cmba','cmba.id','=','cs-schedules.approval_id')
            ->select('cs-schedules.*')
            ->where('cs-schedules.deleted','=',1);

            if($request->submit_search == "b_search"){
                // from date
                if($request->from_date != ""){
                    $from_date = date('Y-m-d',strtotime($request->from_date));
                    Session::flash('from_date',$request->from_date);

                    $data = $data->where('cmba.date_give_product','>=',$from_date);
                }
                // To date
                if($request->to_date != "") {
                    $to_date = date('Y-m-d',strtotime($request->to_date));
                    Session::flash('to_date',$request->to_date);

                    $data = $data->where('cmba.date_give_product','<=',$to_date);
                }
                // Search Brand
                if($request->brand_name != ""){
                    $brand_name = $request->brand_name;
                    Session::flash('brand_name',$request->brand_name);

                    $data = $data->where('cs-schedules.branch_id','=',$brand_name);
                }
                // Search Currency
                if($request->currency != ""){
                    $currency = $request->currency;
                    Session::flash('currency',$request->currency);

                    $data = $data->where('cs-schedules.currency_id','=',$currency);
                }
                // Search Client Name
                if($request->client_name != ""){
                    $client_name = $request->client_name;
                    Session::flash('client_name',$request->client_name);
                    $data = $data->where('cs-schedules.client_id','=',$client_name);
                }

                // Search Schedule ID
                if($request->schedule_id != ""){
                    $schedule_id = $request->schedule_id;
                    Session::flash('schedule_id',$request->schedule_id);
                    $data = $data->where('cs-schedules.id','=',$schedule_id);
                }

                // Search Staff Name
                if($request->staff_name != ""){
                    $staff_name = $request->staff_name;
                    Session::flash('staff_name',$request->staff_name);
                    $data = $data->where('cs-schedules.staff_id','=',$staff_name);
                }
            }else{
                Session::forget('from_date');
                Session::forget('to_date');
                Session::forget('brand_name');
                Session::forget('currency');
                Session::forget('client_name');
                Session::forget('schedule_id');
                Session::forget('staff_name');
            }

            if($money_type != null){
                $data = $data->where('cs-schedules.currency_id','=',$money_type);
            }   
            
            return $data;
       
    }

    public function get_report_schedule_json(Request $request){
        
        $data_query = $this->get_search_schedul($request,null);        
        $data = $data_query->paginate(30);
        $data->setPath('report/report_schedules_json?from_date='.$request->from_date.'&to_date='.$request->to_date.'&brand_name='.$request->brand_name.'&currency='.$request->currency.'&client_name='.$request->client_name.'&schedule_id='.$request->schedule_id.'&staff_name='.$request->staff_name.'&submit_search=b_search');
      
        $data_query2 = $this->get_search_schedul($request,1);  
        $total_count_schedule = $data_query2->count();  
        $total_money_owne_cost = $data_query2->sum('cs-schedules.money_owne_cost'); 
        $total_money_owne_interest = $data_query2->sum('cs-schedules.money_owne_interest'); 
        $total_money_owne_total_pay = $data_query2->sum('cs-schedules.money_owne_total_pay');

        $data_query_kh = $this->get_search_schedul($request,2);  
        $total_count_schedule_kh = $data_query_kh->count();  
        $total_money_owne_cost_kh = $data_query_kh->sum('cs-schedules.money_owne_cost'); 
        $total_money_owne_interest_kh = $data_query_kh->sum('cs-schedules.money_owne_interest'); 
        $total_money_owne_total_pay_kh = $data_query_kh->sum('cs-schedules.money_owne_total_pay');


        $data = [
            'data' =>$data,
            'total_count_schedule'=> $total_count_schedule,
            'total_money_owne_cost' => $total_money_owne_cost,
            'total_money_owne_interest' => $total_money_owne_interest,
            'total_money_owne_total_pay' => $total_money_owne_total_pay,
            'total_count_schedule_kh'=> $total_count_schedule_kh,
            'total_money_owne_cost_kh' => $total_money_owne_cost_kh,
            'total_money_owne_interest_kh' => $total_money_owne_interest_kh,
            'total_money_owne_total_pay_kh' => $total_money_owne_total_pay_kh
        ];
        return response()->json($data);
    }

    

    public function export_report_to_excel(Request $request){
        $date_sche = date('Y-m-d h:m:s');
        Excel::create('Report Schedule-'.$date_sche.'', function($excel) use ($request) {

            $excel->sheet('Report Schedule', function($sheet) use ($request) {

                $data_query = $this->get_search_schedul($request,null);
                $data = $data_query->get();

                $sheet->row(1, array(
                    'Report Schedule',  
                ));
                $sheet->row(2, array(
                    '#', 'លេខកូដតារា','ឈ្មោះអតិថិជន','ប្រាក់ដើមសរុប','ប្រាក់ការសរុប','ប្រាក់ដែលត្រូវបង់សរុប','រយៈពេលសង','ថ្ងៃសុំសង','បុគ្គលិក','រូបិយប័ណ្ណ'
                ));
                $i = 3;
                $data_count = ($data_query->count()) + 2;
                foreach($data as $key=>$d){

                    $dura_type = "";
                    if($d->cs_approvalcredit->duration_pay_money_type == 'month'){
                        $dura_type = "ខែ";
                    }elseif($d->cs_approvalcredit->duration_pay_money_type == '2week'){
                        $dura_type = "២សប្តាហ៍";
                    }elseif($d->cs_approvalcredit->duration_pay_money_type == 'week'){
                        $dura_type = "សប្តាហ៍";
                    }elseif($d->cs_approvalcredit->duration_pay_money_type == 'day'){
                        $dura_type = "ថ្ងៃ";
                    }
                    $sheet->row($key+$i, array(
                        $key+1, $d->id, $d->cs_client->kh_username, $d->cs_approvalcredit->money_owne, $d->money_owne_interest, $d->money_owne_total_pay, $d->cs_approvalcredit->duration_pay_money .' '.$dura_type  , $d->cs_approvalcredit->date_approval, $d->cs_staff->name_kh, $d->currency->name
                    ));
                    $sheet->row($key+$i, function($row){
                        $row->setFontFamily('Battambang');
                    });
                    
                }

                // Style
                $sheet->setBorder('A1:J10', 'thin');
                $sheet->mergeCells('A1:J1');
                $sheet->setHeight(1,70);
                $sheet->setHeight(2,40);

                
                $sheet->setStyle(array(
                    'font' => array(
                        'name'      =>  'Battambang',
                        'size'      =>  10,
                    )
                ));

                $sheet->row(1, function($row){
                    $row->setBackground('#d74a4a');
                    $row->setFontFamily('Battambang');
                
                });
                $sheet->row(2, function($row){
                    $row->setBackground('#fab8b8');
                    $row->setFontFamily('Battambang');
                
                });
                
                $sheet->cell('A1:J'.$data_count.'', function($cell){
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                    $cell->setFont(array(
                        'family' => 'Battambang',
                        'size' => '12',
                        'bold' => false
                    ));
                });

                $sheet->cell('A1:J1', function($cells){
                    $cells->setFont(array(
                        'size' => '15',
                    ));
                });

            });
        })->export('xlsx');
    }
    
}