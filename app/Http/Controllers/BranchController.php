<?php



namespace App\Http\Controllers;



use Illuminate\Http\Request;



use App\Http\Requests;

use App\Http\Controllers\Controller;

use App\Branch;

use DB;

use Auth;

class BranchController extends Controller

{

    public function __construct()
    {
        $this->middleware('auth');
    }    
    public function show_brand(Request $request){
        if (Auth::check()) {
                $title = 'Branch';

                $search = $request->search;

                $data = Branch::where('deleted','=',0)->where('brand_name','LIKE','%'.$search.'%')->get();

                return view('credit_sale.branch.show_list_brand')->with('title',$title)->with('data',$data);
        }else{
            return redirect()->route('login');
        }

    }



    public function show_brand_create(){

        $title = "Create Branch";
    
        return view('credit_sale.branch.create_brand')->with('title',$title);

    }



    public function save_brand_create(Request $request){

        $brand = array(



                    'brand_name' => $request->brand_name,

                    'brand_phone' => $request->brand_phone,

                    'brand_email' => $request->brand_email,

                    // 'brand_upload_image' => $request->brand_upload_image,

                    'brand_dis' => $request->brand_dis,

                    'brand_address' => $request->brand_address,

                    'website' => $request->website



                );



        $b = DB::table('mfi_branch')->insertGetId($brand);

        $brand = [
                'ip_log'=> $request->ip(),
                'active'=> "បង្កើតការកំណត់សាខា",
                'user_id'=> Auth::user()->id,
                'status'=> '2',
                'what_id' => $b,
                'method' => 'brand',
                'create_date' => date('Y-m-d h:m:s')
            ];
          DB::table('cs-history-logs')->insert($brand);

        return redirect()->to('branch')->with('success','Save Seccessfull');

    }



    public function show_brand_edit($id){

        $title = "Edit Branch";

        $brand = Branch::find($id);

        return view('credit_sale.branch.edit_brand')->with('title',$title)->with('brand',$brand);

    }



    public function brand_edit(Request $request, $id){

        $brand = array(
                    'brand_name' => $request->brand_name,

                    'brand_phone' => $request->brand_phone,

                    'brand_email' => $request->brand_email,

                    // 'brand_upload_image' => $request->brand_upload_image,

                    'brand_dis' => $request->brand_dis,

                    'brand_address' => $request->brand_address,

                    'website' => $request->website



                );

        DB::table('mfi_branch')->where('id','=',$id)->update($brand);

        $brand = [
                      'ip_log'=> $request->ip(),
                      'active'=> "កែប្រែការកំណត់សាខា",
                      'user_id'=> Auth::user()->id,
                      'status'=> '3',
                      'what_id' => $id,
                      'method' => 'brand',
                      'create_date' => date('Y-m-d h:m:s')
                  ];
                DB::table('cs-history-logs')->insert($brand);

        return redirect()->to('branch')->with('success','Update Seccessfull');

    }



    public function deleted_brand($id){

        $data = Brand::find($id);

        $data->deleted = 1 ;

        if ($data->save()) {

            return redirect('branch')->with('success','Success Delete');

        }else{

            return redirect('branch')->with('keyerror','Error Data Delete');

        }

        $data = [
                'ip_log'=> $request->ip(),
                'active'=> "លុបការកំណត់សាខា",
                'user_id'=> Auth::user()->id,
                'status'=> '4',
                'what_id' => $id,
                'method' => 'brand',
                'create_date' => date('Y-m-d h:m:s')
            ];
        DB::table('cs-history-logs')->insert($data);

    }
}
