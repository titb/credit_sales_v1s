<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cs_categorys;
use App\Cs_suppliers;
use Auth;
use DB;
class SupplierController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    //
    public function get_list_suppliers(Request $request){

        $title = "ការគ្រប់គ្រង់អ្នកផ្គត់ផ្គង់";
        return view('credit_sale.supplier.suppliers_index',compact('title'));
        
    }
    public function get_list_json_suppliers(Request $request){
        if($request->has('submit_search')){   
            $data = Cs_suppliers::where('deleted','=',0)->where('status','=',1)
                    ->where('name','LIKE','%'.$request->item_search.'%')
                    ->Orwhere('company_name','LIKE','%'.$request->item_search.'%')
                    ->paginate(10);
            $data->setPath('suppliers_get_js?item_search='.$request->item_search.'&submit_search=b_search');
        }else{
            $data = Cs_suppliers::where('deleted','=',0)->where('status','=',1)->paginate(10);
        } 
          
        return response()->json($data);
        
    }
   
    public function get_form_create_suppliers(Request $request){

        $title = "ការបង្កើតអ្នកផ្គត់ផ្គង់ថ្មី";
        return view('credit_sale.supplier.suppliers_create',compact('title'));
        
    }
    // Create New Supplier
    public function post_create_suppliers(Request $request){

        if($request->hasFile('images')){
        
            $destinationPath = "Account/images";
            $im_cl_up = $request->file('images');
            $fileName = $im_cl_up->getClientOriginalName();
            $up_cl_im = $im_cl_up->move($destinationPath,$fileName);
            $adimage = $request->get('Account/images',$fileName);
            
          }else{

              $adimage ="";
          }
        $data = [
                    'company_name'=> $request->company_name,
                    'name'=> $request->name,
                    'email'=> $request->email,
                    'phone'=> $request->phone,
                    'address1'=> $request->address1,
                    'address2'=> $request->address2,
                    'city'=> $request->city,
                    'state_or_province'=> $request->state_or_province,
                    'zip_code'=> $request->zip_code,
                    'country'=> $request->country,
                    'account_number'=> $request->account_number,
                    'images'=> $adimage,
                    'description' => $request->description,
                    'user_id' => Auth::user()->id,
                    'status'=> 1,
                    'created_at' => date('Y-m-d h:m:s')
                ];

            $cs_supplier_id = Cs_suppliers::insertGetId($data);
            $datas = Cs_suppliers::find($cs_supplier_id);
        if($cs_supplier_id){
                    $us_gr = [
                        'ip_log'=> $request->ip(),
                        'active'=> "ការបង្កើតអ្នកផ្គត់ផ្គង់",
                        'user_id'=> Auth::user()->id,
                        'status'=> '2',
                        'what_id' => $cs_supplier_id,
                        'method' => 'Suppliers',
                        'create_date' => date('Y-m-d h:m:s')
                    ];
                DB::table('cs-history-logs')->insert($us_gr);

                $mess = [
                    'datas' => $datas,
                    'redirect' => $request->redirect,
                    'msg_show' => '<div class="alert alert-success"><p>Success! Create Supplier</p></div>'
                ];
            }else{
                $mess = [
                    'datas' =>$datas,
                    'redirect' => $request->redirect,
                    'msg_show' => '<div class="alert alert-danger"><p>Error! Please check input again</p></div>'
                ];
            }

              return response()->json($mess);
  
        
    }

    public function get_form_edit_suppliers(Request $request,$id){
        $title = "ការកែប្រែរព័ត៌មានអ្នកផ្គត់ផ្គង់";
        $data_id = $id;
        return view('credit_sale.supplier.suppliers_create',compact('title','data_id'));
        
    }

    public function get_json_edit_suppliers(Request $request,$id){
        $data = Cs_suppliers::find($id);
        return response()->json($data); 
    }

    // Edit Supplier
    public function post_edit_suppliers(Request $request ,$id){

        $data = Cs_suppliers::find($id);
        if($request->hasFile('images')){
        
            $destinationPath = "Account/images";
            $im_cl_up = $request->file('images');
            $fileName = $im_cl_up->getClientOriginalName();
            $up_cl_im = $im_cl_up->move($destinationPath,$fileName);
            $adimage = $request->get('Account/images',$fileName);
            
          }else{

              $adimage = $data->images;
          }
        $data = [
                    'company_name'=> $request->company_name,
                    'name'=> $request->name,
                    'email'=> $request->email,
                    'phone'=> $request->phone,
                    'address1'=> $request->address1,
                    'address2'=> $request->address2,
                    'city'=> $request->city,
                    'state_or_province'=> $request->state_or_province,
                    'zip_code'=> $request->zip_code,
                    'country'=> $request->country,
                    'account_number'=> $request->account_number,
                    'images'=> $adimage,
                    'description' => $request->description,
                    'user_id' => Auth::user()->id,
                    'status'=> 1,
                    'created_at' => date('Y-m-d h:m:s')
                ];

            $cs_supplier_id = Cs_suppliers::where('id','=',$id)->update($data);
            if($cs_supplier_id){
                        $us_gr = [
                            'ip_log'=> $request->ip(),
                            'active'=> "ការកែប្រែរព័ត៌មានអ្នកផ្គត់ផ្គង់",
                            'user_id'=> Auth::user()->id,
                            'status'=> '2',
                            'what_id' => $id,
                            'method' => 'Suppliers',
                            'create_date' => date('Y-m-d h:m:s')
                        ];
                    DB::table('cs-history-logs')->insert($us_gr);

                    $mess = [
                        'msg_show' => '<div class="alert alert-success"><p>Success! edit Supplier</p></div>'
                    ];
                }else{
                    $mess = [
                        'msg_show' => '<div class="alert alert-danger"><p>Error! Please check input again</p></div>'
                    ];
                }

            return response()->json($mess);
    }

    // Deleted  Supplier 
 
     public function post_delete_suppliers(Request $request,$id){
        $cs_supplier_id = Cs_suppliers::where('id','=',$id)->delete();
            if($cs_supplier_id){
                    $us_gr = [
                        'ip_log'=> $request->ip(),
                        'active'=> "លុបព័ត៌មានអ្នកផ្គត់ផ្គង់",
                        'user_id'=> Auth::user()->id,
                        'status'=> '2',
                        'what_id' => $id,
                        'method' => 'Suppliers',
                        'create_date' => date('Y-m-d h:m:s')
                    ];
                DB::table('cs-history-logs')->insert($us_gr);

                $mess = [
                    'msg_show' => '<div class="alert alert-success"><p>Success! Delete Supplier</p></div>'
                ];
            }else{
                $mess = [
                    'msg_show' => '<div class="alert alert-danger"><p>Error! Please check input again</p></div>'
                ];
            }
        return response()->json($mess);
    }
    
    public function suppliers_get_ajax_serach(Request $request){

        $iterm = $request->term;   
                $data = Cs_suppliers::where('deleted','=',0)->where('status','=',1)
                        ->where('name','LIKE','%'.$iterm.'%')
                        ->Orwhere('company_name','LIKE','%'.$iterm.'%')
                        ->Orwhere('phone','LIKE','%'.$iterm.'%')
                        ->Orwhere('email','LIKE','%'.$iterm.'%')
                        ->Orwhere('account_number','LIKE','%'.$iterm.'%')->take(10)->get();
           
                        $results = [] ;
                        foreach($data as $key => $v){
                             if($v->images){
                                 $img = $v->images;
                             }else{
                                 $img = 'avatar2.png';
                             }
            
                            $results[] = ['id'=> $v->id, 'value' => $v->name ,'image_g'=>$img ];
                        }
            
                        return response()->json($results);
    }
 
}
