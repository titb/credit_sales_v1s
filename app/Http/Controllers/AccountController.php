<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;
use App\Cs_Client;
use App\Cs_Image_Data;
use App\Cs_Schedule;
use App\Cs_ApprovalCredit;
use App\Cs_CommentFinish;
use Session;
use DB;

class AccountController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function get_list_client_accout(Request $request){
        
            $title = "ការគ្រប់គ្រងអតិថិជនជាលក្ខណៈបុគ្គល";

            return view('credit_sale.client_management.client_list',compact('title'));
    }

    public function get_list_json_client_accout(Request $request){
      
          if($request->has('submit_search')){
              //Name Search  
                if($request->search_name != ""){
                  $search_name = $request->search_name;
                  Session::flash('search_name',$request->get('search_name'));
                }else{
                  $search_name = '%_%';
                  Session::forget('search_name');
                }
              //Phone Number   
                if($request->search_phone  != ""){
                  $search_phone = $request->search_phone;
                  Session::flash('search_phone',$request->get('search_phone'));
                }else{
                  $search_phone = '%_%';
                  Session::forget('search_phone');
                }
                //Gender 
                if($request->search_gender != ""){
                  $search_gender = $request->search_gender;
                  Session::flash('search_gender',$request->get('search_gender'));
                }else{
                  $search_gender = '%_%';
                  Session::forget('search_gender');
                }
              //Id Card    
              if($request->search_id_card != ""){
                $search_id_card = $request->search_id_card;
                Session::flash('search_id_card',$request->get('search_id_card'));
              }else{
                $search_id_card = '%_%';
                Session::forget('search_id_card');
              }

              //DOB  
              if($request->search_dob != ""){
                $search_dob = date('Y-m-d',strtotime($request->search_dob));
                Session::flash('search_dob',$request->get('search_dob'));
              }else{
                $search_dob = '%_%';
                Session::forget('search_dob');
              }
            // Brand
            if($request->brand_name != ""){
              $brand_name = $request->brand_name;
              Session::flash('brand_name',$request->get('brand_name'));
            }else{
                $brand_name = '%_%';
                Session::forget('brand_name');
            }
            
                $data = Cs_Client::Where('deleted','=',0)
                ->where(function ($query) use ($search_name){
                  $query->where('kh_username','LIKE','%'.$search_name.'%')
                        ->Orwhere('en_username','LIKE','%'.$search_name.'%');
                })
                ->Where('phone','LIKE','%'.$search_phone.'%')
                ->Where('gender','LIKE','%'.$search_gender.'%')
                ->Where('identify_num','LIKE','%'.$search_id_card.'%')
                ->Where('dob','LIKE','%'.$search_dob.'%')
                ->Where('branch_id','LIKE','%'.$brand_name.'%')
                ->orderBy('id','desc')
                ->paginate(30);
                $data->setPath('accounts_get_js?search_name='.$search_name.'&search_phone='.$search_phone.'&search_id_card='.$search_id_card.'&search_dob='.$search_dob.'&search_gender='.$search_gender.'&search_brand='.$brand_name);
          }else{

            if(Auth::user()->groups->first()->hasPermission(['create-user-groups'])){
                  
                  $data = Cs_Client::Where('deleted','=',0)
                          ->orderBy('id','desc')
                          ->paginate(30);
                
            }else{
                  $data = Cs_Client::Where('deleted','=',0)
                        ->orderBy('id','desc')
                        ->where('branch_id','=',Auth::user()->branch_id)
                        ->paginate(30);
                
            }
            Session::forget('search_name');
            Session::forget('search_phone');
            Session::forget('search_gender');
            Session::forget('search_id_card');
            Session::forget('search_dob'); 
            Session::forget('brand_name');
          }
        return response()->json($data);
        
    }
    
    public function get_create_client_account(Request $request){
        
        $title = "ការគ្រប់គ្រងអតិថិជនជាលក្ខណៈបុគ្គល";
        
        return view('credit_sale.client_management.client_create',compact('title'));
   
    }
    public function post_create_client_account(Request $request){
       $this->validate($request,[
            'client_first_name_kh' => 'required',
            'client_second_name_kh' => 'required',
            'client_gender'  => 'required',
            'day' => 'required', 
            'client_nationality1' => 'required',
            'client_type_idcard1' => 'required',
            'phone'  => 'required|unique:cs_clients|max:60',
            'identify_num' => 'required|unique:cs_clients|max:60',
            
          ]);
       
          if($request->hasFile('client_upload_image')){
        
            $destinationPath = "Account/images";
            $im_cl_up = $request->file('client_upload_image');
            $fileName = $im_cl_up->getClientOriginalName();
            $up_cl_im = $im_cl_up->move($destinationPath,$fileName);
            $adimage = $request->get('Account/images',$fileName);
            
          }else{
              $up_cl_im =" ";
              $adimage ="";
          }
          $address = $adimage;
          $result['longitude'] = $request->client_longitidute;
          $result['latitude'] = $request->client_lutidued;

          $client_first_name_en = strtolower($request->client_first_name_en);
          $client_second_name_en = strtolower($request->client_second_name_en);
          if ($request->client_nationality1 == 1) {
            $cnation = $request->client_nationality1;
          }elseif ($request->client_nationality1 == 2) {
            $cnation = $request->client_nationality2;
          }

          if ($request->client_type_idcard1 < 6) {
            $ctype_idcard = $request->client_type_idcard1;
          }elseif ($request->client_type_idcard1 == 6) {
            $ctype_idcard = $request->client_type_idcard2;
          }

          if ($request->client_aprovel_idcard_by1 == 1) {
            $caprovel_idcard_by = $request->client_aprovel_idcard_by1;
          }elseif ($request->client_aprovel_idcard_by1 == 2) {
            $caprovel_idcard_by = $request->client_aprovel_idcard_by2;
          }
          
          //date dob
          $day = $request->day;
          $month = $request->month;
          $year = $request->year;
          $date_dob = $year.'-'.$month.'-'.$day ;
         
          // district other 
          if($request->client_district == "ផ្សេងៗ"){

            $client_district = $request->other_district;
          }else{
            $client_district = $request->client_district;
          }

          if($request->client_commune == "ផ្សេងៗ"){
              $client_commune = $request->other_commune;
          }else{
              $client_commune = $request->client_commune;
          }

          if($request->client_village == "ផ្សេងៗ"){
              $client_village = $request->other_village;
          }else{
              $client_village = $request->client_village;
          }
          if($request->client_create_date_idcard){
              $client_create_date = date('Y-m-d' ,strtotime($request->client_create_date_idcard));
          }else{
            $client_create_date = " ";
          }
          $acount_p = [

              'kh_name_first' => $request->client_first_name_kh,
              'kh_name_last' => $request->client_second_name_kh,
              'en_name_first' => ucwords($client_first_name_en),
              'en_name_last' => ucwords($client_second_name_en),
              'kh_username' => $request->client_first_name_kh.' '.$request->client_second_name_kh,
              'en_username' => ucwords($client_first_name_en).' '.ucwords($client_second_name_en),
              'gender'  => $request->client_gender,
              'dob'  => $date_dob,
              'nationality' => $cnation,
              'identify_num' => $request->identify_num,
              'identify_type' => $ctype_idcard,
              'identify_by' => $caprovel_idcard_by,
              'phone'    => $request->phone,
              'home_num'=> $request->client_house_num,
              'group_num'=> $request->client_group_num,
              'street_num'=> $request->client_st_num, 
              'vilige'=> $client_village,
              'commune' => $client_commune,
              'district' => $client_district,
              'province' => $request->client_province,
              'job' => $request->client_job,
              'place_job' => $request->client_address_job,
              'upload_relate_document' => $adimage,
              'description' => $request->client_note,
              'client_status' => 0,
              'larvel_client' => 0,
              'latitude' =>  $result['latitude'] ,
              'longitude' => $result['longitude'],
              'user_id'  => Auth::id(),
              'branch_id' => $request->branch_id,
              'client_type_id' => $request->client_type,
              'created_at' => date('Y-m-d h:m:s'),
           ];

           $acc_pp = Cs_Client::insertGetId($acount_p);
          if($acc_pp != 0){
            $data_client = Cs_Client::find($acc_pp);
            $brand_code = $data_client->branch->brand_name_short;
            $date_y = date('Y');
            $date_m = date('m');
            $date_d = date('d');
            $client_code = $brand_code."".$date_y."".$date_m."".$date_d."".$acc_pp;
            $client_code = Cs_Client::where('id',$acc_pp)
                            ->update(['client_code'=>$client_code]);
          }
         
          

           $acount_p = [
            'ip_log'=> $request->ip(),
            'active'=> "បង្កើតអតិថិជនជាលក្ខណៈបុគ្គល",
            'user_id'=> Auth::user()->id,
            'status'=> '2',
            'what_id' => $acc_pp,
            'method' => 'customer personal',
            'create_date' => date('Y-m-d h:m:s')
        ];
        DB::table('cs-history-logs')->insert($acount_p); 
        $data_cli = Cs_Client::find($acc_pp);
        if(!empty($acc_pp)){
          if($request->hasFile('image_uploade_id_familly')){
            $imagp = $request->file('image_uploade_id_familly');
            foreach($imagp as $key => $value) {
                $fileName = $value->getClientOriginalName();  
                $uplades = $value->move($destinationPath, $fileName);
                $dat_image = [
                  'image_value' =>$request->get('Account/images',$fileName),
                  'meta_value' => 'cs_client_id',
                  'dis' => $acc_pp
              ];
            
                DB::table('cs_image_data')->insert($dat_image);
              }
          }
         
          
                  $mess = [
                      'datas'=>$data_cli,
                      'msg_show' => '<div class="alert alert-success"><p>Success! Create Client</p></div>',
                      'redirect'=> $request->redirect,
                      'item' => $request->item
                  ];
        }else{
                $mess = [
                    'datas'=>$data_cli,
                    'msg_show' => '<div class="alert alert-danger"><p>Error! Please check input again</p></div>',
                    'redirect'=> $request->redirect,
                      'item' => $request->item
                  ];
        }

        return response()->json($mess);
    }
    public function get_edit_client_account(Request $request){
        
        $title = "ការកែប្រែអតិថិជន";
        $client_id = $request->client_id;
        return view('credit_sale.client_management.client_edit',compact('title', 'client_id'));
  
    }
    public function get_json_edit_client_account(Request $request ,$id){

      $data = Cs_Client::find($id);
     // $title = $data->client_name_kh." !Edit Account ";
      $relate_file = DB::table('cs_image_data')->where("meta_value","=","cs_client_id")->where('dis','=',$id)->get();
     
        $data_source = [
          'client'=> $data,
          'relate_file' => $relate_file
        ];  
      return response()->json($data_source);
    }  
    // public function post_edit_client_account(Request $request ,$id){
    //     $mess = $request->all();
    //     return response()->json($mess);
    // }
    public function post_edit_client_account(Request $request ,$id){
      // $this->validate($request,[
      //      'client_first_name_kh' => 'required',
      //      'client_second_name_kh' => 'required',
      //      'client_gender'  => 'required',
      //      'day' => 'required', 
      //      'client_nationality1' => 'required',
      //      'client_type_idcard1' => 'required',
      //      'phone'  => 'required',
      //      'identify_num' => 'required',
           
      //    ]);
         $destinationPath = "Account/images";
        $da = Cs_Client::find($id);
         if($request->hasFile('client_upload_image')){       
           $im_cl_up = $request->file('client_upload_image');
           $fileName = $im_cl_up->getClientOriginalName();
           $up_cl_im = $im_cl_up->move($destinationPath,$fileName);
           $adimage = $request->get('Account/images',$fileName);
           
         }else{
             $up_cl_im ="";
             $adimage =$da->upload_relate_document;
         }
         $address = $adimage;
         $result['longitude'] = $request->client_longitidute;
         $result['latitude'] = $request->client_lutidued;

         $client_first_name_en = strtolower($request->client_first_name_en);
         $client_second_name_en = strtolower($request->client_second_name_en);
         if ($request->client_nationality1 == 1) {
           $cnation = $request->client_nationality1;
         }elseif ($request->client_nationality1 == 2) {
           $cnation = $request->client_nationality2;
         }

         if ($request->client_type_idcard1 < 6) {
           $ctype_idcard = $request->client_type_idcard1;
         }elseif ($request->client_type_idcard1 == 6) {
           $ctype_idcard = $request->client_type_idcard2;
         }

         if ($request->client_aprovel_idcard_by1 == 1) {
           $caprovel_idcard_by = $request->client_aprovel_idcard_by1;
         }elseif ($request->client_aprovel_idcard_by1 == 2) {
           $caprovel_idcard_by = $request->client_aprovel_idcard_by2;
         }
         
         //date dob
         $day = $request->day;
         $month = $request->month;
         $year = $request->year;
         $date_dob = $year.'-'.$month.'-'.$day ;
        
         // district other 
         if($request->client_district == "ផ្សេងៗ"){

           $client_district = $request->other_district;
         }else{
           $client_district = $request->client_district;
         }

         if($request->client_commune == "ផ្សេងៗ"){
             $client_commune = $request->other_commune;
         }else{
             $client_commune = $request->client_commune;
         }

         if($request->client_village == "ផ្សេងៗ"){
             $client_village = $request->other_village;
         }else{
             $client_village = $request->client_village;
         }
         if($request->client_create_date_idcard){
             $client_create_date = date('Y-m-d' ,strtotime($request->client_create_date_idcard));
         }else{
           $client_create_date = " ";
         }
         $acount_p = [

             'kh_name_first' => $request->client_first_name_kh,
             'kh_name_last' => $request->client_second_name_kh,
             'en_name_first' => ucwords($client_first_name_en),
             'en_name_last' => ucwords($client_second_name_en),
             'kh_username' => $request->client_first_name_kh.' '.$request->client_second_name_kh,
             'en_username' => ucwords($client_first_name_en).' '.ucwords($client_second_name_en),
             'gender'  => $request->client_gender,
             'dob'  => $date_dob,
             'nationality' => $cnation,
             'identify_num' => $request->identify_num,
             'identify_type' => $ctype_idcard,
             'identify_by' => $caprovel_idcard_by,
             'phone'    => $request->phone,
             'home_num'=> $request->client_house_num,
             'group_num'=> $request->client_group_num,
             'street_num'=> $request->client_st_num, 
             'vilige'=> $client_village,
             'commune' => $client_commune,
             'district' => $client_district,
             'province' => $request->client_province,
             'job' => $request->client_job,
             'place_job' => $request->client_address_job,
             'upload_relate_document' => $adimage,
             'description' => $request->client_note,
             'client_status' => 0,
             'larvel_client' => 0,
             'latitude' =>  $result['latitude'] ,
             'longitude' => $result['longitude'],
             'user_id'  => Auth::id(),
             'branch_id' => $request->branch_id,
             'client_type_id' => $request->client_type,
             'created_at' => date('Y-m-d h:m:s'),
          ];

          $acc_pp = Cs_Client::where('id','=',$id)->update($acount_p);


      if($acc_pp){    

        $acount_p = [
           'ip_log'=> $request->ip(),
           'active'=> "កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល",
           'user_id'=> Auth::user()->id,
           'status'=> '2',
           'what_id' => $id,
           'method' => 'customer personal',
           'create_date' => date('Y-m-d h:m:s')
       ];
       DB::table('cs-history-logs')->insert($acount_p); 
      
         if($request->hasFile('image_uploade_id_familly')){
              DB::table('cs_image_data')->where('meta_value','=','cs_client_id')->where('dis','=',$id)->delete();
              $imagp = $request->file('image_uploade_id_familly');
              foreach($imagp as $key => $value) {
                  $fileName = $value->getClientOriginalName();  
                  $uplades = $value->move($destinationPath, $fileName);
                  $dat_image = [
                    'image_value' =>$request->get('Account/images',$fileName),
                    'meta_value' => 'cs_client_id',
                    'dis' => $id
                ];
              
                  DB::table('cs_image_data')->insert($dat_image);
              }
            }

         
                 $mess = [
                     'msg_show' => '<div class="alert alert-success"><p>Success! Edit Client</p></div>'
                 ];
       }else{
               $mess = [
                   'msg_show' => '<div class="alert alert-danger"><p>Error! Please check input again</p></div>'
               ];
       }
  
       return response()->json($mess);
   }
    public function get_show_client_accout(Request $request ,$id){
        
      $title = "ការបង្ហាញព័ត៌មានអតិថិជន";
      $datas = Cs_Client::find($id);
      $client_id = $id;
      return view('credit_sale.client_management.client_show_detail',compact('title','client_id','datas'));
    }
  // Delete Client
   public function post_delete_client_account(Request $request, $id){
        $acc_pp = Cs_Client::where('id','=',$id)->delete();
        if($acc_pp ){
             DB::table('cs_image_data')->where('meta_value','=','cs_client_id')->where('dis','=',$id)->delete();
              $acount_p = [
                'ip_log'=> $request->ip(),
                'active'=> "កែរប្រែរអតិថិជនជាលក្ខណៈបុគ្គល",
                'user_id'=> Auth::user()->id,
                'status'=> '2',
                'what_id' => $id,
                'method' => 'customer personal',
                'create_date' => date('Y-m-d h:m:s')
            ];
            DB::table('cs-history-logs')->insert($acount_p); 

            $mess = [
              'msg_show' => '<div class="alert alert-success"><p>Success! Deleted Client . Client ID '.$id.' has delete.</p></div>'
            ];
        }else{
            $mess = [
              'msg_show' => '<div class="alert alert-danger"><p>Error! Please check input again</p></div>'
            ];
        }

        return response()->json($mess);
   }
   // Show Client's Schedule
   public function show_generate_schedule(Request $request, $client_id){
      $title = "កាលវិភាគសងប្រាក់អតិថិជន";
      $client_id = $request->client_id;
      $cli_dat = Cs_Client::where('id',$client_id)->first();
      if(count($cli_dat->sub_clients) > 0){
        return view('credit_sale.client_management.generate_schedule',compact('title','client_id'));
      }else{
        $title = "Create Sub Client";
        return Redirect('accounts/sub_client/create_sub_client'.'?client_id='.$client_id);
      }
  }
  public function schedule_show_json(Request $request, $client_id){
      $data = Cs_Schedule::with(['cs_approvalcredit.approval_item.item','cs_schedule_timesheet','cs_client','user','cs_staff'])
      ->where('client_id','=',$client_id)
      ->where('is_finish','=',0)
      ->first();   
      return response()->json($data);
  }
  public function list_aprove_credit_sales(Request $request ,$id){
      $title = "ការបង្ហាញកិច្ចសន្យាទិញបង់រំលស់";
      $data_id = $id;
      $client_id = $id;
      $cli_dat = Cs_Client::where('id',$client_id)->first();
      if(count($cli_dat->sub_clients) > 0){
        return view('credit_sale.client_management.list_approval_credit_sale',compact('title','data_id','client_id'));
      }else{
        $title = "Create Sub Client";
        return Redirect('accounts/sub_client/create_sub_client'.'?client_id='.$client_id);
      }
  }
  public function show_aprove_credit_sales(Request $request,$client_id ,$id){
    $title = "ការបង្ហាញកិច្ចសន្យាទិញបង់រំលស់";
    $data_id = $id;
    $request_id = $id;
    return view('credit_sale.client_management.show_approval_credit_sale',compact('title','data_id','request_id','client_id'));
}
  public function show_aprove_credit_sales_json(Request $request ,$id){
      $datas = Cs_ApprovalCredit::with(['approval_item.item','cs_sales.branch','approval_item.item.categorys','cs_sales.cs_client','schedule'])
              ->where('client_id','=',$id)
              ->get();
      return response()->json($datas);
  }
  public function show_detail_aprove_credit_sales_json(Request $request ,$id){
    $datas = Cs_ApprovalCredit::with(['approval_item.item','cs_sales.branch','approval_item.item.categorys','cs_sales.cs_client','schedule'])
            ->where('cds_request_id','=',$id)
            ->first();
    return response()->json($datas);
}
  public function show_contract_credit_sale($id){
    $title = "ការបង្ហាញការអនុម័តឥណទាន";
    $data_id = $id;
    $cl_data = Cs_ApprovalCredit::with(['approval_item.item','cs_client.sub_clients','cs_sales.branch','approval_item.item.categorys','cs_sales.cs_client','schedule'])
              ->where('cds_request_id','=',$id)
              ->first();
    // dd($cl_data);
    return view('credit_sale.client_management.contract_credit_sale',compact('title','data_id','cl_data'));
  }
  public function show_cred_history($id){
    $title = "ការបង្ហាញប្រវត្តិនៃការទិញ";
    $client_id = $id;
    // 
    // dd($cl_data);
    return view('credit_sale.client_management.history.history_cred_payment', compact('data','title','client_id'));
    // return response()->json($cre_data);
  }
  public function show_cred_history_json($client_id){
    $cre_data = Cs_ApprovalCredit::with(['approval_item.item','cs_sales.branch','approval_item.item.categorys','cs_sales.cs_client','schedule'])
              ->where('client_id',$client_id)->get();
    return response()->json($cre_data);
  }
  // ========= Client Comment Finish ========= //
  public function index_client_comment($client_id){
    $title = "ការបញ្ចប់ឥណទាន";
    return view('credit_sale.client_management.cl_comment_finish.cl_comment', compact('title','client_id'));
  }
  public function client_comment_json($client_id){
    $comment = Cs_Schedule::where('client_id',$client_id)
                            ->with(['comment_finish','cs_client','cs_approvalcredit.approval_item'])
                            ->get();
    return response()->json($comment);
  }
}
