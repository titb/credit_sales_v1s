<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Penaty_date;
use App\User;
use DB;
use Auth;
class PenatyController extends Controller
{
   	public function __construct()
   	{
   		$this->middleware('auth');
   	}

   	public function index(Request $request){
   		$title = "Penaty Date";
         $users = User::where('deteted', '=', 0)->get();
         $data  = Penaty_date::all();
   		return view('credit_sale.penaty.index', compact('title','data','users'));
   	}
   	public function create(Request $request){
   		$title = "Penaty Date | Create";
   		return view('credit_sale.penaty.create', compact('title'));
   	}
   	public function store(Request $request){
   		$this->validate($request,[
   			'title' => 'required',
            'start_date' => 'required',
            'end_date'  => 'required',
            'color'     => 'required'
   			]);
         $data = array(
            'name'        => $request->title,
            'late_form'   => $request->start_date,
            'at_late'     => $request->end_date,
            'percent_of_payment' => $request->percent_of_payment,
            'color'        => $request->color,
            'note'         => $request->discription,
            'user_id'      =>  Auth::id(),
            'created_at'   => date('Y-m-d h:m:s'),
            );
         $p = DB::table('cs_penaty')->insertGetId($data);
          $data = [
                'ip_log'=> $request->ip(),
                'active'=> "បង្កើតការកំណត់ពិន័យ",
                'user_id'=> Auth::user()->id,
                'status'=> '2',
                'what_id' => $p,
                'method' => 'penaty',
                'create_date' => date('Y-m-d h:m:s')
            ];
          DB::table('cs-history-logs')->insert($data); 
         return redirect('penaty_date/create')->with('success', 'You are Create successfull');
   	}
      public function edit(Request $request, $id){
         $title = "Penaty | Edit";
         $data = Penaty_date::find($id);
         return view('credit_sale.penaty.edit')->with('data', $data)->with('title', $title);
      }
      public function update(Request $request, $id){
         $this->validate($request,[
            'title' => 'required',
            'start_date' => 'required',
            'end_date'  => 'required',
            'color'     => 'required'
            ]);
         $update = [
                     'name'        => $request->title,
                     'late_form'   => $request->start_date,
                     'at_late'     => $request->end_date,
                     'percent_of_payment' => $request->percent_of_payment,
                     'color'        => $request->color,
                     'note'  => $request->discription,
                     'user_id'      =>  Auth::id(),
                     'updated_at'   => date('Y-m-d h:m:s'),
                  ];
         Penaty_date::where('id','=',$id)->update($update);
         $update = [
                      'ip_log'=> $request->ip(),
                      'active'=> "កែប្រែការកំណត់ពិន័យ",
                      'user_id'=> Auth::user()->id,
                      'status'=> '3',
                      'what_id' => $id,
                      'method' => 'penaty',
                      'create_date' => date('Y-m-d h:m:s')
                  ];
                DB::table('cs-history-logs')->insert($update);
          return redirect()->to('penaty_date')->with('success', 'You are Update successfull');

      }
      Public function delete(Request $request, $id){
         $data = Penaty_date::find($id);
         $data->delete();
         $data = [
                'ip_log'=> $request->ip(),
                'active'=> "លុបការកំណត់ពិន័យ",
                'user_id'=> Auth::user()->id,
                'status'=> '4',
                'what_id' => $id,
                'method' => 'penaty',
                'create_date' => date('Y-m-d h:m:s')
            ];
         DB::table('cs-history-logs')->insert($data);  
         return redirect()->to('penaty_date')->with('success', 'You Delete successfull');
      }
}
