<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cs_Schedule;
use App\Cs_Client;
use App\User;
use App\Cs_ApprovalCredit;
use Auth;
use DB;
use Session;
class SearchajaxController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    // search with ajax
    public function search_Client_ajax(Request $request){
            $client = $request->term;
            $data = Cs_Client::where('deleted','=',0)
            ->where('kh_username','LIKE','%'.$client.'%')
            ->where('id','LIKE','%'.$client.'%')
            ->where('en_username','LIKE','%'.$client.'%')
            ->take(10)->get();
        $result = [];
        foreach($data as $key => $v){
                $result[] = ['id'=>$v->id,'value'=>	$v->kh_username];
        }
        return response()->json($result);  
    }


    // search schedule with ajax not yet give 
    public function search_schedule_ajax(Request $request){
        $schedule = $request->term;
        $is_give = 0;
        $result =  $this->schedule_search($schedule,$is_give);
        return response()->json($result);  
    }
    
    // search schedule with ajax gice to co
    public function search_schedule_give_to_co_ajax(Request $request){
        $schedule = $request->term;
        $is_give = 2;
        $result =  $this->schedule_search($schedule,$is_give);
        return response()->json($result);  
    }

    // search schedule with ajax gice to Client
    public function search_schedule_id_ajax(Request $request){
        $schedule = $request->term;    
        $is_give =1;
        $result =  $this->schedule_search($schedule,$is_give);
        return response()->json($result);  
    }

    public function schedule_search($term , $is_give){
                $data = Cs_Schedule::where('deleted','=',1)
                ->where('is_give','=',$is_give)
                ->where('id','LIKE','%'.$term.'%')
                ->take(10)->get();
            $result = [];
            foreach($data as $key => $v){
                    $result[] = ['id'=>$v->id,'value'=>$v->id,'client_id'=>$v->client_id,'sale_id'=>$v->sale_id];
            }
            return  $result;
    }
    public function schedule_search_all(Request $request){
        $term = $request->term;    
        $data = Cs_Schedule::where('deleted','=',1)
            ->where('id','LIKE','%'.$term.'%')
            ->take(10)->get();
            $result = [];
            foreach($data as $key => $v){
                    $result[] = ['id'=>$v->id,'value'=>$v->id,'client_id'=>$v->client_id,'sale_id'=>$v->sale_id];
            }
        return response()->json($result);  

    }

    // schedule payment 

    public function search_schedule_payment(Request $request){
        $term = $request->term;
        $data = [
            'deleted' => 1,
            'is_give' => 1,
            'is_finish' => 0,
            'status_for_pay' => 1,
            'status' => 1,
            'active' => 1,
        ];
        $data_sch = $this->schedule_for_payment($term, $data);
        return response()->json($data_sch);

    }

    //Search Schedule For Payment 
    public function schedule_for_payment($term, $data){
        $data = Cs_Schedule::where('id','LIKE','%'.$term.'%')
                ->where('deleted','=',$data['deleted'])
                ->where('is_give','=',$data['is_give'])
                ->where('is_finish','=',$data['is_finish'])
                ->where('status_for_pay','<>',$data['status_for_pay'])
                ->where('status','=',$data['status'])
                ->where('active','=',$data['active'])
                ->take(10)->get();
                $result = [];
                foreach($data as $key => $v){
                        $result[] = ['id'=>$v->id,'value'=>$v->id,'client_id'=>$v->client_id,'sale_id'=>$v->sale_id];
                }
            return  $result;
    }


    // 

    





}
