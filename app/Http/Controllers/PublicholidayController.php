<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\PublicHoliday;
use DB;
use Auth;

class PublicholidayController extends Controller

{

    public function __construct()

      {

          $this->middleware('auth');

      }

      

    public function get_public_holiday(){

        $title = "Public Holiday";

        $list = PublicHoliday::all();

        $data = PublicHoliday::where('deleted','=',0)->paginate(15);

        return view('credit_sale.payment_setting.holidy_setting',compact('title' ,'data','list'));

    }

     public function get_public_holiday_create(){

        $title = "Create Public Holiday";

        return view('credit_sale.payment_setting.holidy_create',compact('title'));
    }

    public function post_public_holiday_create(Request $request){



        $this->validate($request,[

                'title'=> 'required',

                'duration' => 'required',

                'start_date' => 'required|unique:mfi_public_holiday',

                'end_date' => 'required|unique:mfi_public_holiday'



            ]);

        $data = array(

            'title' => $request->title,

            'duration' => $request->duration,

            'start_date' => date('Y-m-d',strtotime($request->start_date)),

            'end_date' => date('Y-m-d',strtotime($request->end_date)),

            'discription'=> $request->discription,

            'user_id' => Auth::user()->id,

            'created_at' => date('Y-m-d h:m:s')

        );
        $dat =  DB::table('mfi_public_holiday')->insertGetId($data);
        
        $start_date1 = strtotime($request->start_date);    
        $end_date = strtotime($request->end_date);

        for($start_date = $start_date1; $start_date <= $end_date; $start_date = strtotime("+1 days", $start_date)) {
                $arr = array(
                    'mfi_public_holiday_id' => $dat,
                    'date_for_holiday' => date ("Y-m-d", $start_date)
                );
              DB::table('mfi_public_holiday_reset')->insert($arr);  
        }

        return Redirect()->to('public_holiday')->with('success','Success Create');
        $data = [
                'ip_log'=> $request->ip(),
                'active'=> "បង្កើតការកំណត់ថ្ងៃឈប់សំរាក់",
                'user_id'=> Auth::user()->id,
                'status'=> '2',
                'what_id' => $dat,
                'method' => 'public holiday',
                'create_date' => date('Y-m-d h:m:s')
            ];
          DB::table('cs_history_log')->insert($data); 
    }



    public function get_public_holiday_edit($id){

        $title = "Edit Public Holiday";

        $data = PublicHoliday::find($id);

        return view('credit_sale.payment_setting.holidy_edit',compact('title','data'));

    }



    public function post_public_holiday_edit(Request $request , $id){

            $this->validate($request,[

                'title'=> 'required',

                'duration' => 'required',

                'start_date' => 'required',

                'end_date' => 'required'



            ]);



        $data = array(

            'title' => $request->title,

            'duration' => $request->duration,

            'start_date' => date('Y-m-d',strtotime($request->start_date)),

            'end_date' => date('Y-m-d',strtotime($request->end_date)),

            'discription'=> $request->discription,

            'user_id' => Auth::user()->id,

            'updated_at' => date('Y-m-d h:m:s')

        );



        DB::table('mfi_public_holiday')->where('id','=',$id)->update($data);
        DB::table('mfi_public_holiday_reset')->where('mfi_public_holiday_id','=',$id)->delete();
        $start_date1 = strtotime($request->start_date);    
        $end_date = strtotime($request->end_date);
        for($start_date = $start_date1; $start_date <= $end_date; $start_date = strtotime("+1 days", $start_date)) {
                $arr = array(
                    'mfi_public_holiday_id' => $id,
                    'date_for_holiday' => date ("Y-m-d", $start_date)
                );
              DB::table('mfi_public_holiday_reset')->insert($arr);  
}
        return redirect('public_holiday')->with('success','Success Update');
        $data = [
                      'ip_log'=> $request->ip(),
                      'active'=> "កែប្រែការកំណត់ថ្ងៃឈប់សំរាក់",
                      'user_id'=> Auth::user()->id,
                      'status'=> '3',
                      'what_id' => $id,
                      'method' => 'public holiday',
                      'create_date' => date('Y-m-d h:m:s')
                  ];
                DB::table('cs_history_log')->insert($data); 
    }



    public function get_public_holiday_deleted(Request $request, $id){

        $data = PublicHoliday::find($id);
        $data->delete();

        DB::table('mfi_public_holiday_reset')->where('mfi_public_holiday_id','=',$id)->delete();
        $data = [
                'ip_log'=> $request->ip(),
                'active'=> "លុបការកំណត់ថ្ងៃឈប់សំរាក់",
                'user_id'=> Auth::user()->id,
                'status'=> '4',
                'what_id' => $id,
                'method' => 'public holiday',
                'create_date' => date('Y-m-d h:m:s')
            ];
        DB::table('cs_history_log')->insert($data);
        return redirect('public_holiday')->with('success','Success Delete');
          
    }

   
}

