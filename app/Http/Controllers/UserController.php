<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

use App\Http\Requests;

use App\Http\Controllers\Controller;

use App\User;

use App\Role;

use App\Group;

use App\Permission;

use App\Mfi_Position;

use DB;

class UserController extends Controller

{
     public function __construct()
      {
          $this->middleware('auth');
      }

  public function get_user_index(Request $request){
     $title = "User Management";

      if($request->has('search')){
      $search = $request->search;

       $data = User::orderBy('id','DESC')->where('name_kh','LIKE','%'.$search.'%')->paginate(10);
      }else{
         $data = User::orderBy('id','DESC')->paginate(10);
      }
        return view('credit_sale.user_management.list_users',compact('data','title'));

    }



    public function get_user_create(){

      $title = "Create Management";

      $groups = Group::all();

      return view('credit_sale.user_management.create_user',compact('groups'))->with('title',$title);

    }



    public function post_user_create(Request $request){



      $this->validate($request, [

            'username' => 'required',

            'user_email' => 'required',

            'password' => 'required|same:confirm-password',

            'group_id' => 'required'

        ]);

      if($request->hasFile('photo')){

          $destinationPath = "User/images";
          $im_cl_up = $request->file('photo');
          $fileName = $im_cl_up->getClientOriginalName();
          $up_cl_im = $im_cl_up->move($destinationPath,$fileName);

          $userdata = [

            'username' => $request->username ,

            'branch_id' => $request->branch_id,

            'name_kh' => $request->name_kh ,

            'user_email' => $request->user_email,

            'user_phone' => $request->user_phone,

            'user_gender'  => $request->user_sex,

            'password'   => bcrypt($request->password),

            'photo'     => $fileName,

            'position_id' => $request->position_id,

            'user_status' => 1,
            'deteted' => 1,

            'created_at'  => date('Y-m-d h:m:s'),

          ];

          $user_id =User::insertGetId($userdata);

          if($user_id != 0){

              $us_gr = [

                'user_id' => $user_id,

                'group_id' => $request->group_id,

              ];

              DB::table('user_groups')->insert($us_gr);
              $us_gr = [
                'ip_log'=> $request->ip(),
                'active'=> "បង្កើតអ្នកប្រើប្រាស់",
                'user_id'=> Auth::user()->id,
                'status'=> '2',
                'what_id' => $user_id,
                'method' => 'user',
                'create_date' => date('Y-m-d h:m:s')
            ];
          DB::table('cs-history-logs')->insert($us_gr);
              return redirect()->to('users')->with('success','Save Seccessfull');

          }
      }
      else{
          $userdata = [

            'username' => $request->username ,

            'branch_id' => $request->branch_id,

            'name_kh' => $request->name_kh ,

            'user_email' => $request->user_email,

            'user_phone' => $request->user_phone,

            'user_gender'  => $request->user_sex,

            'password'   => bcrypt($request->password),

            //'photo'     => $fileName,

            'user_status' => 1,
            'deteted' => 1,

            'position_id' => $request->position_id,

            'created_at'  => date('Y-m-d h:m:s'),

          ];

          $user_id =User::insertGetId($userdata);

          if($user_id != 0){



              $us_gr = [

                'user_id' => $user_id,

                'group_id' => $request->group_id,

              ];

              DB::table('user_groups')->insert($us_gr);
        $us_gr = [
                'ip_log'=> $request->ip(),
                'active'=> "បង្កើតអ្នកប្រើប្រាស់",
                'user_id'=> Auth::user()->id,
                'status'=> '2',
                'what_id' => $user_id,
                'method' => 'user',
                'create_date' => date('Y-m-d h:m:s')
            ];
          DB::table('cs-history-logs')->insert($us_gr);

              return redirect()->to('users')->with('success','Save Seccessfull');

      }

}
}

    public function show_user_profile(){

            $id = Auth::id();

            $title = Auth::user()->username." ! Profile";

            $data = User::where('id' ,'=', $id)->first();

            return view('credit_sale.user_management.showuser_detail',compact('data','title'));

    }

    public function get_show_user(Request $request ,$id){

      $data = User::find($id);
      $title = $data->client_name_kh." ! Show Profile User";
      return view('credit_sale.user_management.showuser_detail',compact('title' , 'data','acount'));

    }

    public function show_user_status_active($id){

            $data = User::find($id);

            $data->user_status = 1;

            $data->save();

           return redirect()->to('users')->with('success','Active Seccessfull');

    }

    public function show_user_status_not_active($id){

            $data = User::find($id);

            $data->user_status = 0;

            $data->save();

           return redirect()->to('users')->with('success','Not Active Seccessfull');

    }



    public function get_user_edit(Request $request,$id){

             $data = User::find($id);

             $title = Auth::user()->username."! Edit User";

             $groups = Group::pluck('display_name','id');

             $user_group = $data->groups->pluck('id','id')->toArray();

              return view('credit_sale.user_management.edit_user',compact('data','title','groups','user_group'));



    }



    public function post_user_edit(Request $request,$id){

            $this->validate($request, [

            'username' => 'required',

            'user_email' => 'required',

            // 'password' => 'required|same:confirm-password',

            'group_id' => 'required'

        ]);


        if($request->hasFile('photo')){

            $destinationPath = "User/images";
            $im_cl_up = $request->file('photo');
            $fileName = $im_cl_up->getClientOriginalName();
            $up_cl_im = $im_cl_up->move($destinationPath,$fileName);

          $userdata = [

            'username' => $request->username ,

            'branch_id' => $request->branch_id,

            'name_kh' => $request->name_kh ,

            'user_email' => $request->user_email,

            'user_phone' => $request->user_phone,

            'user_gender'  => $request->user_sex,

            //'password'   => bcrypt($request->password),

            'photo'       => $fileName,

            'user_status' => 1,
            'deteted' => 1,

            'position_id' => $request->position_id,

            'updated_at'  => date('Y-m-d h:m:s'),

        ];

          User::where('id','=',$id)->update($userdata);

              $us_gr = [

                'user_id' => $id,

                'group_id' => $request->group_id

              ];

              DB::table('user_groups')->where('user_id','=',$id)->update($us_gr);
              $us_gr = [
                'ip_log'=> $request->ip(),
                'active'=> "កែប្រែរអ្នកប្រើប្រាស់",
                'user_id'=> Auth::user()->id,
                'status'=> '3',
                'what_id' => $id,
                'method' => 'user',
                'create_date' => date('Y-m-d h:m:s')
              ];
              DB::table('cs-history-logs')->insert($us_gr);
              return redirect()->to('users')->with('success','Update Seccessfull');
      }
      else{

        $userdata = [

            'username' => $request->username ,

            'branch_id' => $request->branch_id,

            'name_kh' => $request->name_kh ,

            'user_email' => $request->user_email,

            'user_phone' => $request->user_phone,

            'user_gender'  => $request->user_sex,

            //'password'   => bcrypt($request->password),

            //'photo'       => $fileName,

            'user_status' => 1,
            'deteted' => 1,

            'position_id' => $request->position_id,

            'updated_at'  => date('Y-m-d h:m:s'),

        ];

          User::where('id','=',$id)->update($userdata);

              $us_gr = [

                'user_id' => $id,

                'group_id' => $request->group_id

              ];

              DB::table('user_groups')->where('user_id','=',$id)->update($us_gr);
               $us_gr = [
                'ip_log'=> $request->ip(),
                'active'=> "កែប្រែរអ្នកប្រើប្រាស់",
                'user_id'=> Auth::user()->id,
                'status'=> '3',
                'what_id' => $id,
                'method' => 'user',
                'create_date' => date('Y-m-d h:m:s')
            ];
          DB::table('cs-history-logs')->insert($us_gr);
              return redirect()->to('users')->with('success','Update Seccessfull');

      }



    }

  public function post_user_deleted(Request $request , $id){

      $data = User::find($id);

      $data->delete();
       $us_gr = [
                'ip_log'=> $request->ip(),
                'active'=> "លុបអ្នកប្រើប្រាស់",
                'user_id'=> Auth::user()->id,
                'status'=> '4',
                'what_id' => $id,
                'method' => 'user',
                'create_date' => date('Y-m-d h:m:s')
            ];
          DB::table('cs-history-logs')->insert($us_gr);
      return redirect()->to('users')->with('success','Delete Seccessfull');

  }

    /// Group



    //show list create edit delete

    public function get_list_user_group(Request $request){

      $title = "User Group Management";

      $search = $request->search;

      $data = Group::orderBy('id','DESC')->where('name','LIKE','%'.$search.'%')->paginate(10);

      return view('credit_sale.user_management.user_group_list',compact('data','title'))

      ->with('i', ($request->input('page', 1) - 1) * 10);

    }



    // show form Create

    public function get_create_user_group(){

        $permission = Permission::all();
        $groups = $permission->groupBy('module');
        $title = "Create Group";
          return view('credit_sale.user_management.user_group_create',compact('groups','title'));
    }

    public function post_create_user_group(Request $request){

          $this->validate($request ,[
              'name' => 'required',

              'display_name' => 'required',

              'description' => 'required',

              'permission_id' => 'required'
          ]);
          $gr_us = [

              'name' => $request->name,

              'display_name' => $request->display_name,

              'description' => $request->description

          ];
          $gr_us_id = Group::insertGetId($gr_us);
              if($gr_us_id != 0 ){
                  foreach ($request->permission_id as $key => $value) {
                          $per_gr = [
                                'group_id' =>$gr_us_id,
                                'permission_id' => $request->permission_id[$key]
                          ];
                    DB::table('mfi_group_permission')->insert($per_gr);
                  }
                $per_gr = [
                      'ip_log'=> $request->ip(),
                      'active'=> "បង្កើតសិទ្ធិអ្នកប្រើប្រាស់",
                      'user_id'=> Auth::user()->id,
                      'status'=> '2',
                      'what_id' => $gr_us_id,
                      'method' => 'user group',
                      'create_date' => date('Y-m-d h:m:s')
                  ];
                DB::table('cs-history-logs')->insert($per_gr);
               return redirect()->to('users-groups')->with('success' , 'Save Seccessfull !');

              }
    }

  public function get_edit_user_group(Request $request , $id){

       $title = "Edit Group";

       $data =  Group::find($id);

       $permission = Permission::all();

       $groups = $permission->groupBy('module');

      return view('credit_sale.user_management.user_group_edit',compact('data','groups_per','groups','title'));

  }



  public function post_edit_user_group(Request $request ,$id){

          $this->validate($request ,[

              'display_name' => 'required',

              'description' => 'required',

              'permission_id' => 'required'
          ]);
          $gr_us = [

              'display_name' => $request->display_name,

              'description' => $request->description


          ];
          Group::where('id','=',$id)->update($gr_us);

           DB::table("mfi_group_permission")->where("mfi_group_permission.group_id",$id)->delete();

                  foreach ($request->permission_id as $key => $value) {
                          $per_gr = [
                                'group_id' =>$id,
                                'permission_id' => $request->permission_id[$key]
                          ];
                    DB::table('mfi_group_permission')->insert($per_gr);
                  }
                  $per_gr = [
                      'ip_log'=> $request->ip(),
                      'active'=> "កែប្រែសិទ្ធិអ្នកប្រើប្រាស់",
                      'user_id'=> Auth::user()->id,
                      'status'=> '3',
                      'what_id' => $id,
                      'method' => 'user group',
                      'create_date' => date('Y-m-d h:m:s')
                  ];
                DB::table('cs-history-logs')->insert($per_gr);
               return redirect()->to('users-groups')->with('success' , 'Update Seccessfull !');



        }

      public function post_user_group_deleted(Request $request , $id){



          $gro=Group::find($id);

          $gro->delete();

           DB::table("mfi_group_permission")->where("mfi_group_permission.group_id",$id)->delete();
           $gro = [
                      'ip_log'=> $request->ip(),
                      'active'=> "លុបសិទ្ធិអ្នកប្រើប្រាស់",
                      'user_id'=> Auth::user()->id,
                      'status'=> '2',
                      'what_id' => $id,
                      'method' => 'user group',
                      'create_date' => date('Y-m-d h:m:s')
                  ];
                DB::table('cs-history-logs')->insert($gro);
          return redirect()->to('users-groups')->with('success' , 'Delete Seccessfull !');

      }

// ======================= Management Position =================================
      //list
      public function list_position(){
        $title = "Management Position";
        $position = Mfi_Position::where('deleted','=',0)->paginate(5);
        return view('credit_sale.user_management.list_position',compact('title','position'));
      }
      //create
      public function position_create(){

        $title = "Create Position";
        return view('credit_sale.user_management.create_position',compact('title'));

      }
      public function post_position_create(Request $request){

        $data = array('name' => $request->name,'deleted' => 0 );
        $d = DB::table('mfi_positions')->insertGetId($data);
         $data = [
                'ip_log'=> $request->ip(),
                'active'=> "បង្កើតមុខតំណែង",
                'user_id'=> Auth::user()->id,
                'status'=> '2',
                'what_id' => $d,
                'method' => 'position',
                'create_date' => date('Y-m-d h:m:s')
            ];
          DB::table('cs-history-logs')->insert($data);
        return redirect()->to('list_position')->with('success','Save Seccessfull');

      }
      //Edit
      public function position_edit($id){
        $title = "Edit Position";
        $position = Mfi_Position::find($id);
        return view('credit_sale.user_management.edit_position',compact('title','position'));
      }
      public function post_position_edit(Request $request, $id){
        $data = array('name' => $request->name );
        DB::table('mfi_positions')->where('id','=',$id)->update($data);
        $data = [
                      'ip_log'=> $request->ip(),
                      'active'=> "កែប្រែមុខតំណែង",
                      'user_id'=> Auth::user()->id,
                      'status'=> '3',
                      'what_id' => $id,
                      'method' => 'position',
                      'create_date' => date('Y-m-d h:m:s')
                  ];
                DB::table('cs-history-logs')->insert($data);
        return redirect()->to('list_position')->with('success','Update Seccessfull');
      }
      //delete
      public function post_position_deleted($id,Request $request){

        $data = array('deleted' => 1);
        DB::table('mfi_positions')->where('id','=',$id)->update($data);
        $data = [
                'ip_log'=> $request->ip(),
                'active'=> "លុបមុខតំណែង",
                'user_id'=> Auth::user()->id,
                'status'=> '4',
                'what_id' => $id,
                'method' => 'position',
                'create_date' => date('Y-m-d h:m:s')
            ];
        DB::table('cs-history-logs')->insert($data);
        return redirect()->to('list_position')->with('success','Delete Seccessfull');

      }
// ======================= Management Permission =================================

    public function get_list_user_permission(Request $request){

      $title = "User Permission";
      $search = $request->search;
      $data = Permission::orderBy('id','DESC')->where('name','LIKE','%'.$search.'%')->paginate(30);
      return view('credit_sale.user_management.permission.per_index',compact('data','title'))
      ->with('i', ($request->input('page', 1) - 1) * 10);

    }
    // show form Create
    public function get_create_user_permission(){
        $title = "Create User Permission";
          return view('credit_sale.user_management.permission.per_create',compact('groups','title'));
    }
    public function post_create_user_permission(Request $request){
          $this->validate($request ,[
              'name' => 'required',
              'display_name' => 'required',
              'module' => 'required',
          ]);
          $gr_us = [
               'name' => $request->name,
              'display_name' => $request->display_name,
              'module' => $request->module,
              'description' => $request->description,
              'app_setting' => "credit_sale"
          ];
          $gr_us_id = Permission::insertGetId($gr_us);
        return redirect()->to('users-permission')->with('success' , 'Save Seccessfull !');
    }

    public function get_edit_user_permission(Request $request , $id){

         $title = "Edit User Permission";

         $data =  Permission::find($id);

        return view('credit_sale.user_management.permission.per_edit',compact('data','title'));

    }



      public function post_edit_user_permission(Request $request ,$id){

            $this->validate($request ,[
                'display_name' => 'required',
            ]);
            $gr_us = [
                'name' => $request->name,
                'display_name' => $request->display_name,
                'module' => $request->module,
                'description' => $request->description,
                'app_setting' => "credit_sale"
            ];
                Permission::where('id','=',$id)->update($gr_us);
                 return redirect()->to('users-permission')->with('success' , 'Update Seccessfull !');
        }



        public function post_user_group_permission_deleted(Request $request , $id){
            $gro=Permission::find($id);
            $gro->delete();
            return redirect()->to('users-permission')->with('success' , 'Delete Seccessfull !');

        }
       public function change_password_user(Request $request , $id){
         $title = "Change Password";
         $data = User::find($id);
         return view('credit_sale.user_management.user_changepassword',compact('title','data'));
       }
      public function post_change_password_user(Request $request , $id){
         $this->validate($request ,[
            'password' => 'required|same:confirm-password',
         ]);
         $chage = [
           'password' => bcrypt($request->password)
         ];

          User::where('id','=',$id)->update($chage);

         return redirect()->to('users')->with('success','Password has Change');

     }

     public function change_my_password_user(Request $request){
       $title = "Change Password";
       $data = User::find(Auth::user()->id);
       return view('credit_sale.user_management.change_mypassword',compact('title','data'));

     }
    public function post_change_my_password_user(Request $request){
       $this->validate($request ,[
          'password' => 'required|same:confirm-password',
       ]);
       $chage = [
         'password' => bcrypt($request->password)
       ];

        User::where('id','=',Auth::user()->id)->update($chage);

       return redirect()->to('logout')->with('success','Password has Change');

   }


}
