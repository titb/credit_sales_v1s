<?php



namespace App\Http\Controllers;

use Illuminate\Http\Request;



use App\Http\Requests;

use App\Http\Controllers\Controller;

use Auth;

use DB;

use Session;

use App\Cs_Client;

use App\User;

use App\Mfi_Interest_Loan;

use App\Mfi_New_Interest_Rate;

use App\Module_Interest;
use DateTime;

class IterestController extends Controller

{

    

       public function __construct()

      {

          $this->middleware('auth');

      }

    public function get_interest_rate_list(){

        $title = "Interest Rate List";

        $data = Mfi_Interest_Loan::orderBy('id','asc')->get();

        $groups = $data->groupBy('rate_name');

        return view('credit_sale.payment_setting.interest_rate_list',compact('title','data','groups'));

    }

    public function get_iterest_rate_create(){

        $title = "Create Iterest Rate";
        $modules = Module_Interest::where('active','1')->orderBy('id','asc')->get();
        return view('credit_sale.payment_setting.interest_rate_create',compact('title','modules'));

    }

    public function post_iterest_rate_create(Request $request){

        $this->validate($request,[
                'module_interest_id' => 'required',
            ]);
            $data = Module_Interest::find($request->module_interest_id);    
        $data_detail = [

            'user_id' => Auth::user()->id,
            'module_interest_id' => $request->module_interest_id,
            'rate_name' => $data->name,

            'size_money_from' => $request->size_money_from,

            'size_money_to' => $request->size_money_to,

            'day_rate_villige' => $request->day_rate_villige,

            'weekly_rate_villige'=>$request->weekly_rate_villige,

            'two_weekly_rate_villige'=>$request->two_weekly_rate_villige,

            'monthly_rate_villige'=> $request->monthly_rate_villige,

            'day_rate_brand' => $request->day_rate_brand,

            'weekly_rate_brand' => $request->weekly_rate_brand,

            'two_weekly_brand' => $request->two_weekly_brand,

            'monthly_rate_brand' => $request->monthly_rate_brand,

            'duration' => $request->duration,

            'repayment_disction' => $request->repayment_disction,

            'note'=>$request->note,

            'status' => $request->status,

            'created_at' => date('Y-m-d h:m:s')



        ];

        Mfi_Interest_Loan::insert($data_detail);

        return redirect()->to('interest_rate/create')->with('success','Save Seccessfull');
        $dd = collect(Mfi_Interest_Loan::all())->last();

        $data_detail = [
                'ip_log'=> $request->ip(),
                'active'=> "បង្កើតការកំណត់អត្រាការប្រាក់",
                'user_id'=> Auth::user()->id,
                'status'=> '2',
                'what_id' => $dd->id,
                'method' => 'interest rate',
                'create_date' => date('Y-m-d h:m:s')
            ];
          DB::table('	cs_history_log')->insert($data_detail);
    }



    public function get_iterest_rate_edit($id){

        $title ="Edit Interest Rate";
        $modules = Module_Interest::where('active','1')->orderBy('id','asc')->get();
        $data = Mfi_Interest_Loan::find($id);

        return view('credit_sale.payment_setting.interest_rate_edit',compact('data','title','modules'));

    }

     public function post_iterest_rate_edit(Request $request , $id){

        $this->validate($request,[

                'module_interest_id' => 'required',

            ]);
            $data = Module_Interest::find($request->module_interest_id);    
        $data_detail = [

            'user_id' => Auth::user()->id,
            'module_interest_id' => $request->module_interest_id,
            'rate_name' => $data->name,

            'size_money_from' => $request->size_money_from,

            'size_money_to' => $request->size_money_to,

            'day_rate_villige' => $request->day_rate_villige,

            'weekly_rate_villige'=>$request->weekly_rate_villige,

            'two_weekly_rate_villige'=>$request->two_weekly_rate_villige,

            'monthly_rate_villige'=> $request->monthly_rate_villige,

            'day_rate_brand' => $request->day_rate_brand,

            'weekly_rate_brand' => $request->weekly_rate_brand,

            'two_weekly_brand' => $request->two_weekly_brand,

            'monthly_rate_brand' => $request->monthly_rate_brand,

            'duration' => $request->duration,

            'repayment_disction' => $request->repayment_disction,

            'note'=>$request->note,

            'status' => $request->status,

            'created_at' => date('Y-m-d h:m:s'),

            'updated_at' => date('Y-m-d h:m:s') 

        ];

        Mfi_Interest_Loan::where('id','=',$id)->update($data_detail);

        return redirect()->to('interest_rate')->with('success','Save Seccessfull');
        $data_detail = [
                      'ip_log'=> $request->ip(),
                      'active'=> "កែប្រែការកំណត់អត្រាការប្រាក់",
                      'user_id'=> Auth::user()->id,
                      'status'=> '3',
                      'what_id' => $id,
                      'method' => 'interest rate',
                      'create_date' => date('Y-m-d h:m:s')
                  ];
                DB::table('scs_history_log')->insert($data_detail); 
    }
    
//New Interest Rate
    //List
    public function get_new_interest_rate_list(){
        $title = "Interest Rate List";
        $data = Mfi_New_Interest_Rate::paginate(10);
        return view('credit_sale.payment_setting.new_interest_rate_list',compact('title','data'));
    }
    //Create
    public function get_new_interest_rate_create(){
        $title = "Interest Rate Create";
        return view('credit_sale.payment_setting.new_interest_rate_create',compact('title'));
    }
    public function post_new_interest_rate_create(Request $request){
        $data = [
                        'type_dura' => $request->type_dura, 
                        'villige' => $request->villige, 
                        'branch' => $request->branch, 
                        'created_at' => date('Y-m-d') 
                    ];
        DB::table('mfi_new_interest_rate')->insert($data);
        Session::flash('success','Data insert Successful...');

        return redirect('new_interest_rate');
    }

    //Edit
    public function get_new_interest_rate_edit($id){
        $title = 'Interest Rate Edit';
        $data = Mfi_New_Interest_Rate::find($id);
        return view('credit_sale.payment_setting.new_interest_rate_edit',compact('title','data'));
    }
    public function post_new_interest_rate_edit(Request $request){
        $id = $request->id;
        $data = [
                    'type_dura' => $request->type_dura,
                    'villige' => $request->villige,
                    'branch' => $request->branch,
                    'updated_at' => date('Y-m-d')
                ];
        DB::table('mfi_new_interest_rate')->where('id','=',$id)->update($data);
        Session::flash('success','Data Updated Successful...');

        return redirect('new_interest_rate');
    }
    //Delete
    public function post_new_interest_rate_delete($id){
        DB::table('mfi_new_interest_rate')->where('id','=',$id)->delete();
        Session::flash('success','Data Deleted Successful...');

        return redirect('new_interest_rate');
    }
//Service Interest Rate
    public function get_service_interest_rate_list(){
        $title = "Service Interest Rate List";

        $data = Mfi_Interest_Loan::orderBy('id','asc')->get();

        $data1 = Mfi_New_Interest_Rate::first();

        $groups = $data->groupBy('rate_name');

        return view('credit_sale.payment_setting.service_interest_rate',compact('title','data','data1','groups'));
    }


}

