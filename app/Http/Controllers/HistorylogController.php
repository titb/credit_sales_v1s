<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\Mfi_Client;
use App\Mfi_SubClient;
use App\Mfi_Repayment_Schedul;
use App\Mfi_Load_Request;
use App\Cs_brand;
use App\User;
use App\Cs_History_log;
use App\Mfi_Repayment_Scheduls_Timesheet_History;
use DB;
use Cache;


class HistorylogController extends Controller
{

     public function __construct()
      {
          $this->middleware('auth');
      }

      public function history_log(Request $request){
            $title = "History log User";
            $brand_name = $request->brand_name;
            if($request->start_form != Null && $request->to_date != Null && $request->brand_name != Null){
              $start_from = date('Y-m-d',strtotime($request->start_form));
              $to_date = date('Y-m-d',strtotime($request->to_date));
              
              $data = Cs_History_log::join('users as u','cs_history_log.user_id','=','u.id')
                      ->whereBetween('create_date',[$start_from,$to_date])
                      ->where('u.brand_id','=',$brand_name)
                      ->orderBy('cs_history_log.id','desc')
                      ->paginate(30);
              
                      $data->setPath('history_log?start_form='.$start_from.'&to_date='.$to_date.'&brand_name='.$brand_name);
            }
            else if($request->start_form != Null && $request->to_date != Null){
              $start_from = date('Y-m-d',strtotime($request->start_form));
              $to_date = date('Y-m-d',strtotime($request->to_date));

              $data = Cs_History_log::whereBetween('create_date',[$start_from,$to_date])
                      ->orderBy('cs_history_log.id','desc')
                      ->paginate(30);

              $data->setPath('history_log?start_form='.$start_from.'&to_date='.$to_date.'&brand_name=');
            }
            else if($request->brand_name != Null){
              $data = Cs_History_log::join('users as u','cs_history_log.user_id','=','u.id')
                      ->where('u.brand_id','=',$brand_name)
                      ->orderBy('cs_history_log.id','desc')
                      ->paginate(30);

              $data->setPath('history_log?start_form=&to_date=&brand_name='.$brand_name);
            }
            else{
              $data = Cs_History_log::orderBy('id','desc')->paginate(30);
            }

              $user = User::all();
              $brand = Cs_brand::all();
            return view('credit_sale.report.report_history_log',compact('title','data','brand','user'));
      }
} 