<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cs_brand;
use App\Cs_categorys;
use Auth;
use DB;
class RelateItemController extends Controller
{

    //Brand
        //list Brand

            public function get_list_brands(Request $request){
                $title = "ការគ្រប់គ្រង់ម៉ាករបស់ផលិតផល";
                return view('credit_sale.items.brand',compact('title'));
            }

        //  Get Data with json

            public function get_create_json_brands(Request $request){
                if($request->has('submit_search')){   
                    $data = Cs_brand::where('deleted','=',0)->where('name','LIKE','%'.$request->brand_products.'%')->paginate(5);
                    $data->setPath('products/brands_json?brand_products='.$request->brand_products);
                }else{
                    $data = Cs_brand::where('deleted','=',0)->paginate(5);
                }    
                return response()->json($data);
            }
        // Create New Brand
            public function post_create_brands(Request $request){
                $data = [
                            'name'=> $request->name_brand,
                            'description' => $request->discription_get
                        ];

                    $cs_brand_id = Cs_brand::insertGetId($data);
                if($cs_brand_id){
                            $us_gr = [
                                'ip_log'=> $request->ip(),
                                'active'=> "បង្កើតម៉ាករបស់ផលិតផលថ្មី",
                                'user_id'=> Auth::user()->id,
                                'status'=> '2',
                                'what_id' => $cs_brand_id,
                                'method' => 'brand',
                                'create_date' => date('Y-m-d h:m:s')
                            ];
                        DB::table('cs-history-logs')->insert($us_gr);

                        $mess = [
                            'msg_show' => '<div class="alert alert-success"><p>Success! Create Brand</p></div>'
                        ];
                    }else{
                        $mess = [
                            'msg_show' => '<div class="alert alert-danger"><p>Error! Please check input again</p></div>'
                        ];
                    }

                    return response()->json($mess);
            }
        // Edit Brand         
            public function post_edit_brands(Request $request,$id){
                $data = [
                            'name'=> $request->name_brand,
                            'description' => $request->discription_get
                        ];

                    $cs_brand_id = Cs_brand::where('id','=',$id)->update($data);
                if($cs_brand_id){
                            $us_gr = [
                                'ip_log'=> $request->ip(),
                                'active'=> "កែរប្រែរព័ត៌មានម៉ាករបស់ផលិតផល",
                                'user_id'=> Auth::user()->id,
                                'status'=> '2',
                                'what_id' => $id,
                                'method' => 'brand',
                                'create_date' => date('Y-m-d h:m:s')
                            ];
                        DB::table('cs-history-logs')->insert($us_gr);

                        $mess = [
                            'msg_show' => '<div class="alert alert-success"><p>Success!  Brand</p></div>'
                        ];
                    }else{
                        $mess = [
                            'msg_show' => '<div class="alert alert-danger"><p>Error! Please check input again</p></div>'
                        ];
                    }

                    return response()->json($mess);
            }
        // Deleted Brand    
            public function get_deleted_brands(Request $request,$id){
                $cs_brand_id = Cs_brand::where('id','=',$id)->delete();
                    if($cs_brand_id){
                            $us_gr = [
                                'ip_log'=> $request->ip(),
                                'active'=> "លុបព័ត៌មានម៉ាករបស់ផលិតផល",
                                'user_id'=> Auth::user()->id,
                                'status'=> '2',
                                'what_id' => $id,
                                'method' => 'brand',
                                'create_date' => date('Y-m-d h:m:s')
                            ];
                        DB::table('cs-history-logs')->insert($us_gr);

                        $mess = [
                            'msg_show' => '<div class="alert alert-success"><p>Success! Delete Brand</p></div>'
                        ];
                    }else{
                        $mess = [
                            'msg_show' => '<div class="alert alert-danger"><p>Error! Please check input again</p></div>'
                        ];
                    }
                return response()->json($mess);
            }
    // End Brand
    
    // Category
        //View Category 

            public function get_list_categorys(Request $request){
                $title = "ការគ្រប់គ្រង់ប្រភេទរបស់ផលិតផល";
               
                return view('credit_sale.items.category',compact('title'));
            }

        // Get List json  category

            public function get_create_json_categorys(Request $request){
                if($request->has('submit_search')){   
                    $data = Cs_categorys::where('deleted','=',0)->where('name','LIKE','%'.$request->item_search.'%')->paginate(5);
                    $data->setPath('products/categorys_json?item_search='.$request->item_search);
                }else{
                    $data = Cs_categorys::where('deleted','=',0)->paginate(5);
                }    
                return response()->json($data);
            }

        // Create New Category
            public function post_create_categorys(Request $request){
                $data = [
                            'name'=> $request->name,
                            'description' => $request->description_get,
                            'user_id' => Auth::user()->id,
                            'status'=> 1,
                            'created_at' => date('Y-m-d h:m:s')
                        ];

                    $cs_category_id = Cs_categorys::insertGetId($data);
                if($cs_category_id){
                            $us_gr = [
                                'ip_log'=> $request->ip(),
                                'active'=> "បង្កើតប្រភេទរបស់ផលិតផល",
                                'user_id'=> Auth::user()->id,
                                'status'=> '2',
                                'what_id' => $cs_category_id,
                                'method' => 'Category',
                                'create_date' => date('Y-m-d h:m:s')
                            ];
                        DB::table('cs-history-logs')->insert($us_gr);

                        $mess = [
                            'msg_show' => '<div class="alert alert-success"><p>Success! Create Category</p></div>'
                        ];
                    }else{
                        $mess = [
                            'msg_show' => '<div class="alert alert-danger"><p>Error! Please check input again</p></div>'
                        ];
                    }

                    return response()->json($mess);
            }
        //Edit Category 
            public function post_edit_categorys(Request $request,$id){
                $data = [
                            'name'=> $request->name,
                            'description' => $request->description_get,
                            'user_id' => Auth::user()->id,
                            'status'=> 1,
                            'created_at' => date('Y-m-d h:m:s')
                        ];

                    $cs_category_id = Cs_categorys::where('id','=',$id)->update($data);
                if($cs_category_id){
                            $us_gr = [
                                'ip_log'=> $request->ip(),
                                'active'=> "កែរប្រែរព័ត៌មានប្រភេទរបស់ផលិតផល",
                                'user_id'=> Auth::user()->id,
                                'status'=> '2',
                                'what_id' => $id,
                                'method' => 'Category',
                                'create_date' => date('Y-m-d h:m:s')
                            ];
                        DB::table('cs-history-logs')->insert($us_gr);

                        $mess = [
                            'msg_show' => '<div class="alert alert-success"><p>Success!  Category</p></div>'
                        ];
                    }else{
                        $mess = [
                            'msg_show' => '<div class="alert alert-danger"><p>Error! Please check input again</p></div>'
                        ];
                    }

                    return response()->json($mess);
            }
        // Deleted Catedgory 
            public function get_deleted_categorys(Request $request,$id){
                $cs_category_id = Cs_categorys::where('id','=',$id)->delete();
                    if($cs_category_id){
                            $us_gr = [
                                'ip_log'=> $request->ip(),
                                'active'=> "លុបព័ត៌មានប្រភេទរបស់ផលិតផល",
                                'user_id'=> Auth::user()->id,
                                'status'=> '2',
                                'what_id' => $id,
                                'method' => 'Category',
                                'create_date' => date('Y-m-d h:m:s')
                            ];
                        DB::table('cs-history-logs')->insert($us_gr);

                        $mess = [
                            'msg_show' => '<div class="alert alert-success"><p>Success! Delete Category</p></div>'
                        ];
                    }else{
                        $mess = [
                            'msg_show' => '<div class="alert alert-danger"><p>Error! Please check input again</p></div>'
                        ];
                    }
                return response()->json($mess);
            }

    //End Category
}
