<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\User;
use App\Branch;
use App\Cs_Client;
use App\Cs_Schedule;
use App\Cs_Schedule_Timesheet_Pay;
use App\Cs_Schedule_Timesheet;
use App\Cs_Approval_Item;
use App\Cs_Sale_Payment_Tran;
use App\Cs_ApprovalCredit;
use App\Penaty_date;
use App\Cs_Settings;
use App\Currency;
use Cookie;
use View;
use Auth;
use DateTime;
use DB;
class PaymentBackController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function date_count_date($stare_date = null ,$end_date = null){
		$dat_payf = new DateTime(date("Y-m-d",strtotime($stare_date)));
		$date_generat = new DateTime(date("Y-m-d",strtotime($end_date)));
		$dat_pay1 = $date_generat->getTimestamp() - $dat_payf->getTimestamp();

		$date_generat1 = $dat_pay1 / (60 * 60 *24);
		return $date_generat1;
	} 
	public function kh_currency($number = null){
		if ($number != 0){
		$places = -2;
		$mult = pow(10, abs($places));
		$kh_con = ceil($number/$mult)*$mult;
		}else{
			$kh_con = 0;
		}
		return $kh_con;
	}
	public function us_currency($number = null){
		$places = 2;
		$us_con = round($number ,$places);	
		return $us_con;
    }   
    
    public function get_payment_back(Request $request){
        $title = "Payment Back";
        $user= User::where('user_status','=',1)->where('deteted','=',1)->get();
        return view('credit_sale.payment_back.payment_back',compact('title','user'));
    }
    public function list_schedule_timesheet_pay($item){
        if($item != 0){
           $item_id = $item;
        }else{
            $item_id = "%_%";
        }
            if(Auth::user()->groups->first()->name == 'administractor'){
                $data = Cs_Schedule_Timesheet_Pay::with('cs_schedule','cs_schedule.cs_client','cs_schedule.cs_staff','user','cs_staff')->where('cs_schedules_id','LIKE',$item_id)->paginate(15); 
            }else{
                $data = Cs_Schedule_Timesheet_Pay::with('cs_schedule','cs_schedule.cs_client','cs_schedule.cs_staff','user','cs_staff')->where('user_id','=',Auth::user()->id)->where('cs_schedules_id','LIKE',$item_id)->paginate(15); 
            }
        
        return $data;
    }
    public function list_schedule_timesheet_pay_json(Request $request){
        if($request->submit_search){
            if($request->payment_client_id){
                $item = $request->payment_client_id;
            }else{
                $item = 0;
            }
                
        }else{
            $item = 0;
        }
         $data = $this->list_schedule_timesheet_pay($item);

        return response()->json($data);
    }
    public function list_payment_back(Request $request){ 
        $title = "List Payment Back";
        return view('credit_sale.payment_back.list_payment_back',compact('title'));
    }

        
    // check schedule finish
    public function schedule_completed(Request $request , $id){
        $data = [
            'deleted' => 1,
            'is_give' => 1,
            'is_finish' => 1,
            'status_for_pay' => 1,
            'status' => 1,
            'active' => 1,
        ];
         $data_sch = check_schedule($id, $data);
         return response()->json($data_sch);
    }

    // check payment completed 
    public function data_pay_complete($data){
        $total_payment = $data['total_pay_cost'] + $data['total_pay_cost_owe'] + $data['total_pay_interest'];
        return response()->json($data);
    }

    // check schedule finish
    public function check_schedule($id, $data){
        $data = Cs_Schedule::where('id','=',$id)
                ->where('deleted','=',$data['deleted'])
                ->where('is_give','=',$data['is_give'])
                ->where('is_finish','=',$data['is_finish'])
                ->where('status_for_pay','=',$data['status_for_pay'])
                ->where('status','=',$data['status'])
                ->where('active','=',$data['active'])
                ->first();
        if(!isset($data)){
            return false;
        }else{
            return $data;
        }
    }


    public function schedule_check_timesheet_pay($id){
        if(count($this->check_schedule_pay_not_yet_payment($id)) > 0){
            $data =  $this->check_schedule_pay_not_yet_payment($id);
        }else{
            $data = $this->get_payment_not_yet_pay($id);   
        }
        return response()->json($data);
    }
    // check Schedule timesheet not payment   
    public function get_payment_not_yet_pay($id) {
        $schedule = Cs_Schedule_Timesheet::with(['cs_sch_ts_pay','cs_schedule','cs_schedule.cs_client','cs_schedule.cs_staff'])
                    ->where('cs_schedules_id','=',$id)
                    ->where('status','=',0)
                    ->where('status_for_pay','=',0)
                    ->first();
        $sch_time_id = $schedule->id;
        $data = [
            'schedule' => $schedule, 
            'available_total_pay_cos' =>0,
            'panalty' => 0,
            'num_of_payment'=> 1,
            'date_last_payment' =>0,
            'payment_own' => 0,
            'available_total_pay_interest' =>0,
            'old_payment' => 0,
            'status_for_pay' => 0,
        ];
        return $data; 
    } 


    // check schedule payment not yet complete  
    public function check_schedule_pay_not_yet_payment($id) {
        $schedule = Cs_Schedule_Timesheet::with(['cs_sch_ts_pay','cs_schedule','cs_schedule.cs_client','cs_schedule.cs_staff'])
                    ->where('cs_schedules_id','=',$id)
                    ->where('status_for_pay','=',2)
                    ->where('status','=',2)
                    ->first();
    if(!empty($schedule)){
        $sch_time_id = $schedule->id;
        $data_sch_payment = Cs_Schedule_Timesheet_Pay::where('schedules_titmesheet_id','=',$sch_time_id)->get();
        $available_total_pay_cost = 0 ; $panalty = 0 ;  $available_total_pay_interest = 0 ; $available_total_payment = 0; $available_date_payment = '';
        foreach ($data_sch_payment as $dsch){
            $available_total_pay_cost += $dsch->available_total_pay_cost ;
            $available_total_pay_interest += $dsch->available_total_pay_interest;
            $available_total_payment += $dsch->available_total_payment;
            $panalty += $dsch->panalty;
        }
        $al_date_payment = collect($data_sch_payment)->last()->available_date_payment;
        $payment_own = collect($data_sch_payment)->first()->available_total_due_payment - $available_total_payment ;
        if($payment_own > 0){
            $payment_own = $payment_own;
        }else{
            $payment_own = 0;
        }

        $data = [
            'schedule' => $schedule,
            'available_total_pay_cos' =>  $available_total_pay_cost,
            'panalty' => $panalty,
            'num_of_payment'=> 2,
            'date_last_payment' => date('d-m-Y',strtotime($al_date_payment)),
            'payment_own' => $this->us_currency($payment_own),
            'available_total_pay_interest' =>  $available_total_pay_interest,
            'old_payment' =>  $available_total_payment,
            'status_for_pay' => 2,
        ];
    }else{
        $data = [];
    }
        return $data;
    }

    // get all payment of schedule 
    public function getScheduleTimesheetPay($id){
        $data_sch_payment = Cs_Schedule_Timesheet_Pay::where('cs_schedules_id','=',$id)->get();
        return $data_sch_payment;
    }

    // get payment pay by schedule time sheet  

    public function payment_pay_by_timesheet($sc_timesheet_id){
        $data_sch_payment = Cs_Schedule_Timesheet_Pay::where('schedules_titmesheet_id','=',$sc_timesheet_id)->get();
        return $data_sch_payment;
    }
    
    // add session payment 
    public function add_session_payment(Request $request){
        if($request->schedule_id){
            $request->session()->put('schedule_id', $request->schedule_id);
        }
        
    }

    //timesheet payment 
    public function schedule_time_sheet($id){
        $data = Cs_Schedule_Timesheet::find($id);
        return $data;
    }

    //Schedule  payment 
    public function schedule_payment($id){
        $data = Cs_Schedule::find($id);
        return $data;
    }
    //validate payment 
    public function validate_payment(Request $request){
        $validator = Validator::make($request->all(), [
            'payment_client_id' => 'required',
            'num_invoice_payment' => 'required',
            'staff_id' => 'required',
            'available_date_payment' => 'required',
            'lavel_payment' => 'required'
        ]);
        return $validator;
    }



    // check date payment 
    public function check_date_payment($date_pay , $al_date_pay){
        
        if(strtotime($date_pay) >= strtotime($al_date_pay)){
            $late_date = 0;
        }else{
            $late_date = $this->date_count_date($date_pay , $al_date_pay);
        }
        return $late_date;
    }

    // set payment panalty 
    public function set_payment_late($date_late){
        $pan = Penaty_date::where('status','=',1)->get();
        $panalty = 0;
        foreach ($pan as $p){
            if(($p->late_form <= $date_late) && ($p->at_late >= $date_late)){
                $panalty = $p->percent_of_payment;
                break;
            }
        }
        $panalty = $panalty;

        return  $panalty;
    }

    //get payment panalty
    public function panalty($num , $pan){
        $pan = $pan /100 ;
        $num1 = $num * $pan;
        return $num1;
    }

    // payment number 
    public function num_panalty($num_pay , $late_date){
        $pan = $this->set_payment_late($late_date);
        $panalty = $this->panalty($num_pay, $pan);
        $num_pay_late = $panalty * $late_date;
        $conv = $this->us_currency($num_pay_late);
        return $conv;
    }


   // get interest use bye date 
   public function get_interest_use_date($interest){
        $date_in_month = 30 ;
        $interest_full_month = $interest;
        $interest_bye_date =  $interest_full_month /  $date_in_month;
        return  $interest_bye_date;
   }
    //Complete Payment 
    public function payment_complete(array $data){
        $al_payment = str_replace(',','',$data['available_total_payment']);
        $status_for_pay = $data['status_for_pay'];
        $sc_timesheet_id_prev = $data['sc_timesheet_id'] -1;
        $old_total_pay_cos = $data['old_total_pay_cos'];
        $old_total_pay_interest = $data['old_total_pay_interest'];
        $old_total_panalty = $data['old_total_panalty'];
        $total_pay_cost_owe = $data['total_pay_cost_owe'];
        $date_last_payment = $data['date_last_payment']; 
        
         
        $data_schedule = $this->schedule_payment($data['payment_client_id']);
        $data_payment =  $this->getScheduleTimesheetPay($data['payment_client_id']);
        $total_paid_cost = collect($data_payment)->sum('available_total_pay_cost');

        $data_timesheet =  $this->schedule_time_sheet($data['sc_timesheet_id']);
        $data_timesheet_prev =  $this->schedule_time_sheet($sc_timesheet_id_prev);
        $al_payment1 = $al_payment;
        
            $money_owne_cost = $data_schedule->money_owne_cost;
            $total_paid_cost = $total_paid_cost;
            //interest use by date , check date use , if avalible date less then date payemnt 
                $date_payment = $data['date_payment'];
                $available_date_payment = $data['available_date_payment'];
                $date_last_payment = $data['date_last_payment'];
                if($date_payment > $available_date_payment){
                    $date_last_month = $data_timesheet_prev['date_payment'];
                    $date_of_use = $this->check_date_payment($date_last_month ,$available_date_payment );
                    $interest_use = $this->get_interest_use_date($data_timesheet->total_pay_interest);
                    $total_pay_interest =  $interest_use * $date_of_use;
                }else{
                    $total_pay_interest = $data_timesheet->total_pay_interest;
                }
            // end

            // check has this have payment or not 
            $total_payment_cost_own = $data_timesheet->total_pay_cost_owe;
            $total_payment_cost = $data_timesheet->total_pay_cost;
           

            if($total_payment_cost >= $old_total_pay_cos){
                $total_payment_cost = $total_payment_cost - $old_total_pay_cos;
            }else{
                $total_payment_cost = 0 ;
            }
    
            if($total_pay_interest >= $old_total_pay_interest){
                $total_pay_interest = $total_pay_interest - $old_total_pay_interest;
            }else{
                $total_pay_interest = 0 ;
            }
            // end 

        


            // check have panalty or not 
            if(isset($data['is_penalty_pay'])){
                $num_late_payment = $this->num_panalty($payment,$data['late_date']);
            }else{
                $num_late_payment = 0;
            }
           // end

            // interest use bydate 

            $payment_on_date =  $total_payment_cost_own + $total_payment_cost + $total_pay_interest + $num_late_payment;
            $total_cost_payment = $total_payment_cost + $total_payment_cost_own;

            if($al_payment >= $payment_on_date ){
                $num_late_payment = $num_late_payment;
                $total_pay_interest =  $total_pay_interest;
                $total_payment_cost = $total_cost_payment; 
                $payment_remaining = $al_payment - $payment_on_date;
            }else{
                return false;
            }
    

        $data = [
            'available_total_due_payment' => $this->us_currency($payment_on_date),
            'available_total_payment' => $al_payment1,
            'available_remain' => $this->us_currency($payment_remaining), 
            'total_pay_cost' => $this->us_currency($total_payment_cost),
            'total_pay_cost_owe' => 0, 
            'total_pay_interest' => $this->us_currency($total_pay_interest),
            'num_late_payment' => $this->us_currency($num_late_payment),
            'status' => 1,
            
        ];
        return $data;
    }
    // Complete Payment 

    // price if payment 
    public function check_price_of_payment(array $data){
        $payment = str_replace(',','',$data['total_payment']);
        $payment_own = str_replace(',','',$data['payment_own']);
        $al_payment = str_replace(',','',$data['available_total_payment']);
        $total_pay_cost = $data['total_pay_cost'];
        $total_pay_interest = $data['total_pay_interest'];
        $old_total_pay_cos = $data['old_total_pay_cos'];
        $old_total_pay_interest = $data['old_total_pay_interest'];
        $old_total_panalty = $data['old_total_panalty'];
        $total_pay_cost_owe = $data['total_pay_cost_owe'];
        $date_last_payment = $data['date_last_payment']; 
        $al_payment1 = $al_payment;
        if($payment_own > 0){
            $payment = $payment_own ;
        }

        if($total_pay_cost >= $old_total_pay_cos){
            $total_pay_cost = $total_pay_cost - $old_total_pay_cos;
        }else{
            $total_pay_cost = 0 ;
        }

        if($total_pay_interest >= $old_total_pay_cos){
            $total_pay_interest = $total_pay_interest - $old_total_pay_interest;
        }else{
            $total_pay_interest = 0 ;
        }

        if(isset($data['is_penalty_pay'])){
            $num_late_payment = $this->num_panalty($payment,$data['late_date']);
        }else{
            $num_late_payment = 0;
        }

        $num_late_payment = $num_late_payment;

        $payment = $payment +  $num_late_payment;
        $due_payment = $payment;

        if($al_payment >= $payment){
            $available_remain = $al_payment - $payment ;
            $num_late_payment = $num_late_payment;
            $pay_al = $payment;
            $total_pay_cost = $total_pay_cost;
            
            $total_pay_interest = $total_pay_interest; 
            $status = 1;
        }else{
            
            $available_remain = 0;
            $status = 2;

            if($al_payment >= $num_late_payment){
                 $num_late_payment = $num_late_payment;
                 $al_payment = $al_payment - $num_late_payment;            
            }else{
                $num_late_payment = $al_payment;
                $al_payment = 0;        
            }
            if($al_payment >= $total_pay_interest){
                $total_pay_interest = $total_pay_interest;
                $al_payment = $al_payment - $total_pay_interest;
            }else{
                $total_pay_interest = $al_payment;
                $al_payment = 0;
            }
            if($al_payment >= $total_pay_cost){
                $total_pay_cost = $total_pay_cost;
                $al_payment = $al_payment - $total_pay_cost;
            }else{

                $total_pay_cost = $al_payment;
                $al_payment = 0;
            }
            

            $pay_al = $payment;
 
        }
       

        $data = [
            'available_total_due_payment' => $due_payment,
            'available_total_payment' => $al_payment1,
            'available_remain' =>  $this->us_currency($available_remain),
            'total_pay_cost' => $this->us_currency($total_pay_cost),
            'total_pay_cost_owe' => $this->us_currency($total_pay_cost_owe),
            'total_pay_interest' => $this->us_currency($total_pay_interest),
            'num_late_payment' => $this->us_currency($num_late_payment),
            'status' => $status,
            
        ];
        return $data;

    }

    // Payment Back 
    public function client_payment_back(Request $request){
        $validator = $this->validate_payment($request);
        if ($validator->fails()) {
            $errors = $validator->errors();
           return response()->json($errors);
        }
        try {
            DB::beginTransaction();
                
                $sc_timesheet_id = $request->sc_timesheet_id;
                // $total_payment = $request->total_payment;
                $total_payment = str_replace(',','',$request->total_payment);
                $available_total_payment = str_replace(',','',$request->available_total_payment);
                $data1 = $request->all();
                // check date for panalty 
                if($request->has('date_last_payment')){
                    if($request->date_last_payment >=  $request->date_payment){
                        $date_pay = $request->date_last_payment;
                    }else{
                        $date_pay = $request->date_payment;
                    }
                }else{
                    $date_pay = $request->date_payment;
                }
                    
                    $al_date_pay = $request->available_date_payment;
                    $late_date = $this->check_date_payment($date_pay , $al_date_pay);
                // end check date for panalty 
                //panalty   
                if($request->has('is_penalty_pay')){
                    $is_penalty_pay = $request->is_penalty_pay;
                    $pan = $this->set_payment_late($late_date);
                    $num_pay_nul = $this->panalty($total_payment, $pan);
                    $num_late_payment = $this->num_panalty($total_payment,$late_date);
                }else{
                    $pan  = 0;
                    $num_pay_nul = 0;
                    $num_late_payment = 0;
                    $is_penalty_pay = 0;
                }
                $data_all = array_add($request->all(),'late_date', $late_date) ;
                // $data_all = array_add($data_all) ;
         if($request->pay_all == 1 || $request->total_pay_cost_owe == 0){ 
             
            $data = $this->payment_complete($data_all);
            if($data != false){
                $datapay = $data;
            }else{
                $datapay = $this->check_price_of_payment($data_all);
            }
                
         }else{
            $datapay = $this->check_price_of_payment($data_all);
         }
                // end panalty
                
            // Payment 
                
               $data_payment = [
                    'cs_schedules_id' => $request->payment_client_id,
                    // 'client_id' => $request->client_id,
                    'branch_id' => Auth::user()->branch_id,
                    'user_id' => Auth::user()->id,
                    'staff_id' => $request->staff_id,
                    'schedules_titmesheet_id' => $request->sc_timesheet_id,
                    'num_invoice_payment' => $request->num_invoice_payment,
                    'available_total_pay_cost' =>  $datapay['total_pay_cost'],
                    'available_total_pay_interest' =>  $datapay['total_pay_interest'],
                    'available_total_payment' =>  $datapay['available_total_payment'],
                    'available_total_due_payment' =>  $datapay['available_total_due_payment'],
                    'available_total_pay_cost_owe' =>  $datapay['total_pay_cost_owe'],
                    'other_pay_reason' => null,
                    'other_payment' => 0,
                    'panalty' => $datapay['num_late_payment'], 
                    'date_payment' =>  date('Y-m-d',strtotime($date_pay)),
                    'available_date_payment' =>  date('Y-m-d',strtotime($al_date_pay)),
                    'lavel_payment' =>  $request->lavel_payment,
                    'num_of_payment' =>  $request->num_of_payment,
                    'date_late_pay' =>  $late_date,
                    'is_penalty_pay' =>  $is_penalty_pay,
                    'available_remain' => $datapay['available_remain'],
                    'status' => $datapay['status'],
                    'status_for_pay' => $request->pay_all,
                    'active' => 1,
                    'note' => $request->comment,
                    'deleted' => 1,
                    'created_at' => date('Y-m-d h:m:s'),
                    'updated_at' => date('Y-m-d h:m:s'),

               ];

               $cs_cshedule_pay = Cs_Schedule_Timesheet_Pay::insertGetId($data_payment);
               if($cs_cshedule_pay != 0){

                   Cs_Schedule_Timesheet::where('id','=',$request->sc_timesheet_id)->update(['status'=> $datapay['status'],'status_for_pay'=> $request->pay_all]);
                   Cs_Schedule::where('id','=',$request->payment_client_id)->update(['status_for_pay'=> $request->pay_all]);
                    $data_tran = [
                        'sale_id' => $request->payment_client_id,
                        'schedule_timesheet_pay_id' => $cs_cshedule_pay,
                        'account_mount' => $datapay['available_total_payment'],
                        'currecy_type' => 1,
                        'payment_pay' => 'cash'
                    ] ;
                   Cs_Sale_Payment_Tran::insert($data_tran);
                }    
            // Payment

        

            

                DB::commit();    
                    $data = [
                        'pan' => $pan,
                        'num_pay_nul' =>  $num_pay_nul,
                        'num_late_payment' =>  $num_late_payment,
                        'data' => $data_payment,
                        'late_date' => $late_date,
                        'data1' =>$data1
                    ];
                    return response()->json($data);
               

        } catch (\Exception $e) {
            DB::rollBack();

            $data = [
                'message'    => 'Error Create Payment Please Check: '.$e->getMessage(),
                'alert-type' => 'error',
            ];
            return response()->json($data);

        }
    }

    // deleted credit sale payment timesheet payment 
    public function delete_payment_timesheet($id){
        $data = Cs_Schedule_Timesheet_Pay::find($id);
        $data_sche_pay = Cs_Schedule_Timesheet_Pay::where('cs_schedules_id','=',$data->cs_schedules_id)->get();

        $data_ts_pay = Cs_Schedule_Timesheet_Pay::where('id','>',$id)->where('cs_schedules_id','=',$data->cs_schedules_id)->get();
        foreach ($data_ts_pay as $key => $ks){
            Cs_Schedule_Timesheet::where('id','=',$ks->schedules_titmesheet_id)->update(['status'=>0 , 'status_for_pay'=>0]);
        }
        if($data->num_of_payment == 1){
            $status = 0;
            $status_for_pay = 0;
        }else{
            $status = 2;
            $status_for_pay = 2;
        }
        if(count($data_sche_pay)== 1){
            $status_pay = 0;
        }else{
            $status_pay = 2;
        }
        Cs_Schedule_Timesheet::where('id','=',$data->schedules_titmesheet_id)->update(['status'=>$status , 'status_for_pay'=>$status_for_pay]);
        Cs_ApprovalCredit::where('id','=',$data->cs_schedules_id)->update(['is_finish'=>0]);
        Cs_Schedule::where('id','=',$data->cs_schedules_id)->update(['status_for_pay'=>$status_pay,'is_finish'=>0]);
        Cs_Schedule_Timesheet_Pay::where('id','>=',$id)->where('cs_schedules_id','=',$data->cs_schedules_id)->delete();
       
        return response()->json(['msg_show'=>'អ្នកបានលុបដោយជោគជ័យ']);
    }   
}

