<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\lib\Total_sale;
use App\lib\LibInventory;
use DB;
use Auth;
use Session;
use DateTime;
class HomeController extends Controller
{
    protected $total_sale;
    public function __construct()
    {
        $this->total_item_inv = new LibInventory;
        $this->total_sale = new Total_sale;
        $this->middleware('auth');
    }
    public function get_welcome(){
        $title = "Welcome Dashboard";
        $data = $this->get_data_total();
        // print_r($data);
        // die();
        return view('credit_sale.welcome',compact('title','data'));
    }

    public function get_data_total(){
      
        $local = false;
        $paginate = false;
        $numofpage = 10;
        $data_payment = $this->total_sale->get_total_sale_item_payment($method= null, $local, $paginate, $numofpage);
        $data_total_item = $this->total_sale->get_total_sale_item($method= null, $local, $paginate, $numofpage);
        $data_payment_sale_chash = $this->total_sale->get_total_sale_item_payment($method= 'sale_by_cash', $local, $paginate, $numofpage);
        $data_total_item_sale_chash = $this->total_sale->get_total_sale_item($method= 'sale_by_cash', $local, $paginate, $numofpage);
        $data_payment_sale_credit = $this->total_sale->get_total_sale_item_payment($method= 'sale_by_credit', $local, $paginate, $numofpage);
        $data_total_item_sale_credit = $this->total_sale->get_total_sale_item($method= 'sale_by_credit', $local, $paginate, $numofpage);
        $total_item_in_stock = $this->total_item_inv->get_total_inventory_item($deleted=1, $branch=null, $paginate=null);
        $total_item_in_stock_payment = $this->total_item_inv->get_total_price_inventory_item($deleted=1, $branch=null, $paginate=null);

        
        $data = [
            'data_payment' => $data_payment,
            'data_total_item' => $data_total_item,
            'data_payment_sale_chash' => $data_payment_sale_chash,
            'data_total_item_sale_chash' => $data_total_item_sale_chash,
            'data_payment_sale_credit' => $data_payment_sale_credit,
            'data_total_item_sale_credit' => $data_total_item_sale_credit,
            'total_item_in_stock_payment' => $total_item_in_stock_payment,
            'total_item_in_stock' => $total_item_in_stock

        ];
        // echo "Total item Payment :".$data_payment."<br/>";
        // echo "Total item  :".$data_total_item."<br/>";
        // echo "Total item Payment Sale By Chash:".$data_payment_sale_chash."<br/>";
        // echo "Total item  Sale By Chash:".$data_total_item_sale_chash."<br/>";
        // echo "Total item Payment Sale By Credit:".$data_payment_sale_credit."<br/>";
        // echo "Total item Sale By Credit:".$data_total_item_sale_credit."<br/>";
        return  $data;
    }
    
    // public function get_login(){
    //     $title = "Login";
    //     return view('credit_sale.login',compact('title'));
    // }
     public function get_test(){
        $title = "Welcome Dashboard";
        return view('page',compact('title'));
    }
    // Route::get('/test', function () {
    //   $title ="text";
    //     return view('page',compact('title'));
    // });
}
