<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Cs_ApprovalCredit;
use App\Cs_CommentFinish;
use App\Cs_Schedule;

class CommentFinishController extends Controller
{
    public function index_comment_finish(){
        $cre_data = Cs_Schedule::where('status_for_pay',1)->get();
        // dd($cre_data);
        return view('credit_sale.comment_finish.index_comment_finish', compact('cre_data'));
    }
    // public function index_comment_finish_json(){
    //     $cre_data = Cs_Schedule::where('status_for_pay',1)->with(['comment_finish'])->get();
    //     return response()->json($cre_data);
    // }
    public function create_comment_finish(Request $request){
        $comment_id = $request->request_id;
        $title = "ការបង្ហាញព័ត៌មានបញ្ចប់ឥណទាន";
        return view('credit_sale.comment_finish.create_comment_finish', compact('title','comment_id'));
    }
    public function create_comment_json(Request $request){
        $id = $request->request_id;
        $sched_data = Cs_Schedule::where('approval_id',$id)
                                ->with(['comment_finish','cs_approvalcredit','cs_approvalcredit.approval_item.item','cs_schedule_timesheet_pay'])
                                ->get();
        return response()->json($sched_data);
    }
    public function post_comment_finish_json(Request $request){
        $request_id = $request->request_id;
        $date_last = date('Y-m-d', strtotime($request->date_last_payment));
        $data = [
                    'command_finish'=>$request->command_finish,
                    'date_last_payment'=> $date_last,
                    'status_of_finish'=>$request->status_of_finish,
                    'note'=>$request->note,
                    'cs_schedules_id'=>$request_id,
                    'user_id'=>Auth::id(),
                ];

        if($request->comment_id != 0){
            $data_finish = Cs_CommentFinish::where('id','=',$request->comment_id)->update($data);
        }else{
            $data_finish = Cs_CommentFinish::insert($data,['created_at'=>date('Y-m-d')]);
        }

        if($request->status_of_finish == 1){
            $is_finish = 1;
        }else{
            $is_finish = 0;
        }
        
        Cs_ApprovalCredit::where('cds_request_id',$request_id)->update(['is_finish'=>$is_finish]);
        Cs_Schedule::where('approval_id',$request_id)->update(['is_finish'=>$is_finish]);
        return response()->json($data_finish);
    }
    public function delete_comment($id){
        $data = Cs_CommentFinish::where('cs_schedules_id',$id)->delete();
        Cs_ApprovalCredit::where('cds_request_id',$id)->update(['is_finish'=>0]);
        Cs_Schedule::where('approval_id',$id)->update(['is_finish'=>0]);
        return response()->json($data);
    }
    // public function create_json_comment_finish(Request $request){
    //     $id = $request->comment_id;
    //     $json_data = Cs_ApprovalCredit::where('id',$id)->get();
    //     return response()->json($json_data);
    // }

    // ============> Comment Finish Error <===============
    public function comment_error(){
        $title = "ការបញ្ចប់មិនត្រឹមត្រូវ";
        $com_error = Cs_Schedule::where('status_for_pay',1)->get();
        return view('credit_sale.comment_finish.comment_finish_error.index_comment_error', compact('title','com_error'));
    }
    public function comment_error_json(){
        $com_error = Cs_Schedule::where('status_for_pay',1)->where('is_finish', 0)
                                ->with(['comment_finish','cs_client','cs_approvalcredit','cs_approvalcredit.approval_item'])
                                ->get();
        return response()->json($com_error);
    }
}
