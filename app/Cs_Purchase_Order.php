<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cs_Purchase_Order extends Model
{
    //
    protected $table = "cs_purchase_order"; 
    

    public function cs_purchase_order_item(){
        return $this->hasMany('App\Cs_Purchase_Order_Item','cs_purchase_order_id');
    }
    public function cs_purchase_order_payment(){
        return $this->hasMany('App\Cs_Purchase_Order_Payment','cs_purchase_order_id');
    }

    public function suppliers(){
        return $this->belongsTo('App\Cs_suppliers','supplier_id');
    }

    public function users(){
        return $this->belongsTo('App\User','user_id');
    }

    public function staff_receive(){
        return $this->belongsTo('App\User','stuff_receiving_id');
    }
    public function staff_order(){
        return $this->belongsTo('App\User','stuff_order_id');
    }


    
}
