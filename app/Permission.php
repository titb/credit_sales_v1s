<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
  protected $table ="mfi_permission";

    public function groups()

    {
        return $this->belongsToMany('App\Group','mfi_group_permission','group_id','permission_id');
    }
}
