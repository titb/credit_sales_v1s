<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cs_ApprovalCredit extends Model
{
    //
    protected $table = "cs-manager-branch-approval";

    public function approval_item(){
        return $this->hasMany('App\Cs_Approval_Item','cs_approval_id');
    }
    public function cs_sales(){
        return $this->belongsTo('App\Cs_Sale','sale_id');
    }
    public function module_interest(){
        return $this->belongsTo('App\Module_Interest','interest_type');
    } 
    public function schedule(){
        return $this->hasOne('App\Cs_Schedule','approval_id');
    }

    public function currency(){
        return $this->belongsTo('App\Currency','currency');
    }
    public function cs_client(){
        return $this->belongsTo('App\Cs_Client','client_id');
    }

    public function manager_id1(){
        return $this->belongsTo('App\User','manager_id1');
    }
    public function manager_id2(){
        return $this->belongsTo('App\User','manager_id2');
    }
    public function manager_id3(){
        return $this->belongsTo('App\User','manager_id3');
    }


}
