<?php
namespace App\lib;
use App\lib\Session_Search;
use App\Cs_Client;
use App\Cs_Items;
use App\Cs_inventorys;
use DB;
use Auth;
use Session;

class LibInventory  {	
    // Query data Inventory 
    static function get_inventory_query($deleted = 1){
        if($deleted){
            $data_query = Cs_inventorys::where('deleted','=',$deleted);
        }else{
            $data_query = Cs_inventorys::where('deleted','=',1);
        }
        return $data_query;
    }

    static function get_inventory_query_branch($deleted = 1, $branch=null){
        $data_query = self::get_inventory_query($deleted);
        if($branch){
            $data_query = $data_query->where('branch_id','=',$branch);
        }else{
            $data_query = $data_query;
        }

        return $data_query;
    }

    static function get_inventory_info($deleted = 1, $branch=null, $paginate =null){
        $data_query = self::get_inventory_query_branch($deleted, $branch);
        if($paginate){
            $data_query = $data_query->paginate($paginate);
        }else{
            $data_query = $data_query->get();
        }
        return $data_query;
    }

    static function get_total_inventory_item($deleted = 1, $branch=null, $paginate = null){
        $data_query = self::get_inventory_info($deleted, $branch, $paginate);
        $data_qty = $data_query->sum('qty');
        $data_count = $data_qty ;
        return $data_count;
    }

    public function get_total_price_inventory_item($deleted = 1, $branch=null, $paginate = null)
    {
        $data_query = self::get_inventory_info($deleted, $branch, $paginate);
        $total_payment = 0 ;
        $total_payment_free = 0;
        foreach ( $data_query as $data ){
            $total_payment += $data->qty * $data->item->sell_price;   
            $total_payment_free += $data->qty_free * $data->item->sell_price;   
        }  
        $total_p = $total_payment + $total_payment_free;
        return $total_p;
    }
    
}