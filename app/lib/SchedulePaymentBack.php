<?php
namespace App\lib;
use App\lib\Session_Search;
use App\Cs_Schedule;
use App\Cs_Schedule_Timesheet_Pay;
use DB;
use Auth;
use Session;
   
class  SchedulePaymentBack {
    protected $request;
    static function getData(){
             $data_query = Cs_Schedule_Timesheet_Pay::with(['cs_schedule.cs_client','cs_schedule.currency','branch','user','cs_staff'])
                            ->join('cs-schedules as css','css.id','=','cs-schedules-repayment-timesheet.cs_schedules_id')
                            ->select('cs-schedules-repayment-timesheet.*')
                     ->where('cs-schedules-repayment-timesheet.deleted','=',1);
                     
         return  $data_query; 
    }

    // static function getDataPay(){
    //     $data_query = Cs_Schedule_Timesheet_Pay::join('')
    //      return  $data_query; 
    // }

    static function GetDataSearch($request){
        $data_query = self::getData();
        if($request->has('submit_search')){
          $data_search = Session_Search::search_form($request);  

            if($data_search['from_date'] != ''){
                $data_query = $data_query->where('cs-schedules-repayment-timesheet.available_date_payment','>=',$data_search['from_date']);
            }

            if($data_search['to_date'] != ''){
                $data_query = $data_query->where('cs-schedules-repayment-timesheet.available_date_payment','<=',$data_search['to_date']);
            }

            if($data_search['brand_name'] != ''){
                $data_query = $data_query->where('cs-schedules-repayment-timesheet.branch_id','=',$data_search['brand_name']);
            }

            if($data_search['client_name'] != ''){
                $data_query = $data_query->where('cs-schedules-repayment-timesheet.client_id','=',$data_search['client_name']);
            }

            if($data_search['sale_id'] != ''){
                $data_query = $data_query->where('cs-schedules-repayment-timesheet.cs_schedules_id','=',$data_search['sale_id']);
            }

            if($data_search['currency'] != ''){
                $data_query = $data_query->where('css.currency_id','=',$data_search['currency']);
            }
            
            if($data_search['staff_name'] != ''){
                $data_query = $data_query->where('cs-schedules-repayment-timesheet.staff_id','=',$data_search['staff_name']);
            }

        }else{
            $data_query = $data_query;
        }
        return $data_query;
    }
    
    static function getDataAll($request){
        $query = self::GetDataSearch($request);
        $data_query = $query->get();
        return $data_query;
    }
    static function getDatabyPagination($request,$num = 15){
        $data_query = self::GetDataSearch($request);
        $data = $data_query->paginate($num);
        return $data;
    }
// Group Bye 
    public function getDataGroupBySchedule(){
        $query = self::GetDataSearch($request);
        $data_query = $query->groupBy('cs-schedules-repayment-timesheet.cs_schedules_id')->get();
        return $data_query;
    }


    public function total_cost_own_of_client($data){
        
    }

    public function collect_total_summery($request,$money_type){
        $query = self::GetDataSearch($request);
        $query = $query->where('css.currency_id','=',$money_type);
        $total_available_total_pay_cost = $query->sum('cs-schedules-repayment-timesheet.available_total_pay_cost');
        $total_available_total_pay_interest = $query->sum('cs-schedules-repayment-timesheet.available_total_pay_interest');
        $total_other_payment = $query->sum('cs-schedules-repayment-timesheet.other_payment');
        $total_panalty = $query->sum('cs-schedules-repayment-timesheet.panalty');
        $total_available_total_payment  = $query->sum('cs-schedules-repayment-timesheet.available_total_payment');  

        $total_cost_own = [
            'total_available_total_pay_cost' => $total_available_total_pay_cost,
            'total_available_total_pay_interest' => $total_available_total_pay_interest,
            'total_other_payment' => $total_other_payment,
            'total_panalty' => $total_panalty,
            'total_available_total_payment' => $total_available_total_payment
        ];

        return   $total_cost_own;
    }




}