<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mfi_Credit_Committy extends Model
{
      protected $table ="mfi_credit_committies";

    public function mfi_load_request()

    {

        return $this->belongsTo('App\Mfi_Load_Request','mfi_load_request_id');

    }

    public function mfi_interest_loan(){

        return $this->belongsTo('App\Mfi_Interest_Loan','mfi_interest_rate_id');

    }

    public function users(){



    	return $this->belongsToMany('App\User','mfi_users_credit_committies','user_id','mfi_credit_committie_id');



    }



    public function mfi_credit_committies_groups(){

    	return $this->hasMany('App\Mfi_Credit_Committies_Group','mfi_credit_committies_id');

    }



    public function mfi_repayment_schedul_type(){

        return $this->belongsTo('App\Mfi_Repayment_Schedul_Type','mfi_repayment_schedul_type_id');

    }



    public function mfi_repayment_scheduls(){

        return $this->hasMany('App\Mfi_Repayment_Schedul','mfi_credit_committies_id');

    }

    public function mfi_users_credit_committies(){
        return $this->hasMany('App\Mfi_User_Commutity','mfi_credit_committie_id');
    }
}
