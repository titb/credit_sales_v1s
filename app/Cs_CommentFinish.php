<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cs_CommentFinish extends Model
{
    protected $table = 'cs_command_finish_credit_sale';

    public function scedules(){
        return $this->belongsTo('App\Cs_Schedule', 'cs_schedules_id');
    }
}
