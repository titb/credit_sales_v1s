<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sub_client extends Model
{
    protected $table = "cs_sub_clients";

    public function clients()
    {
        return $this->belongsToMany('App\Cs_Client','cs_client_andsub_clients','client_id','sub_client_id');
    }
    public function third_clients(){
        return $this->hasOne('App\ThirdClient', 'sub_client_id');
    }
}
