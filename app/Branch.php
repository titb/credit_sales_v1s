<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    protected $table = "mfi_branch";
    public function user(){

    	return $this->hasOne('App\User','branch_id');
    }
}
