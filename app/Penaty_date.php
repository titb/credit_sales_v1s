<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Penaty_date extends Model
{
     protected $table = "cs_penaty";

     public function user(){
          return $this->belongsTo('App\User','user_id');
     }
}
