<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
  protected $table = 'groups';

   public function users()
    {
        return $this->belongsToMany('App\User','user_groups','user_id','group_id');
    }

    public function permissions()
      {
          return $this->belongsToMany('App\Permission','mfi_group_permission','group_id','permission_id');
      }

      public function hasAnyPermission($permissions){

          if(is_array($permissions)){

              foreach ($permissions as  $permission) {

                  if($this->hasPermission($permission)){

                      return true;

                  }

              }

          }else{

              if($this->hasPermission($permissions)){

                      return true;

                  }

          }

          return false;

      }

      public function hasPermission($permission){

            if($this->permissions()->where('name',$permission)->first()){

                return true;

            }

            return false;

        }
    
}
