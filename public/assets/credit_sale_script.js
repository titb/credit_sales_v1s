
function date_month_date(dat_format = null){
    var d = new Date(dat_format);
    var year_n = d.getFullYear();
    var month_n = d.getMonth() + 1;
    var day_n = d.getDate();
    if(month_n >= 10){
        month_n = month_n;
    }else{
        month_n = "0"+month_n; 
    }

    if(day_n >= 10){
        day_n = day_n;
    }else{
        day_n = "0"+day_n; 
    }

    var data = {"year_n": year_n , "month_n": month_n ,"day_n" : day_n };
    return data;
}

function convert_back_date(date_format){
    var data =  date_month_date(date_format);
      return  data.year_n +"-"+data.day_n+"-"+data.month_n;
}
function day_format_show(date_format = null){
   var data =  date_month_date(date_format);
  // var data = JSON.stringify(data);
    return  data.day_n +"-"+data.month_n+"-"+data.year_n;
}

function stand_formate_date(date_format = null){
    var data =  date_month_date(date_format);
    return  data.year_n +"-"+data.month_n+"-"+data.day_n;
 
}

function date_between( start_date = null, end_date = null ){
    var start_date = stand_formate_date(start_date);
    var end_date = stand_formate_date(end_date);
    var dt1 = new Date(start_date);
    var dt2 = new Date(end_date);
    var diff = dt2.getTime() - dt1.getTime();
    var days_d = diff/(1000 * 60 * 60 * 24);
    return days_d;
}



function DateInMonth(dat_format){
    var d = new Date(dat_format);
    var year_n = d.getFullYear();
    var month_n = d.getMonth();
    return new Date(year_n, month_n, 0).getDate();
}
